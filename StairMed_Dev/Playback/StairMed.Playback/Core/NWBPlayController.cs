using Newtonsoft.Json;
using StairMed.ChipConst.Intan;
using StairMed.Core.Consts;
using StairMed.DataCenter;
using StairMed.DataCenter.Recorder;
using StairMed.DataFile.NWB;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.Probe;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StairMed.Playback.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class NWBPlayController : ViewModelBase
    {
        #region 单例

        private static readonly NWBPlayController _instance = new NWBPlayController();
        public static NWBPlayController Instance => _instance;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public const int SEND_INTERVAL_MS = 50;

        /// <summary>
        /// 
        /// </summary>
        public bool ImportedProbeMap { get; set; } = false;


        /// <summary>
        /// 
        /// </summary>
        private long _deviceId = 10086;
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        protected bool _isReading = false;
        private readonly AutoResetEvent _resetEvent = new AutoResetEvent(false);

        /// <summary>
        /// 已经播放的时长
        /// </summary>
        private TimeSpan _playedTime = TimeSpan.Zero;
        public TimeSpan PlayedTime
        {
            get { return _playedTime; }
            set
            {
                value = value < TimeSpan.Zero ? TimeSpan.Zero : value > Duration ? Duration : value;
                Set(ref _playedTime, value);
                if (_playedTime.TotalSeconds <= 0 || Duration.TotalSeconds <= 0)
                {
                    PlayedProgress = 0;
                }
                else
                {
                    PlayedProgress = _playedTime.TotalSeconds / Duration.TotalSeconds;
                }
            }
        }

        /// <summary>
        /// 已播放的进度
        /// </summary>
        private double _playedProgress = 0;
        public double PlayedProgress
        {
            get { return _playedProgress; }
            set
            {
                if (Set(ref _playedProgress, value))
                {
                    if (Duration.TotalSeconds > 0)
                    {
                        PlayedTime = TimeSpan.FromSeconds(value * Duration.TotalSeconds);
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private double _speedX = 1.0;
        public double SpeedX
        {
            get { return _speedX; }
            set { Set(ref _speedX, value); }
        }

        /// <summary>
        /// 源文件路径
        /// </summary>
        private string _sourcePath = string.Empty;
        public string SourcePath
        {
            get { return _sourcePath; }
            set
            {
                if (Set(ref _sourcePath, value))
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        try
                        {
                            FileName = Path.GetFileNameWithoutExtension(value);
                        }
                        catch { }
                    }
                }
            }
        }

        /// <summary>
        /// 文件名
        /// </summary>
        private string _fileName = string.Empty;
        public string FileName
        {
            get { return _fileName; }
            set { Set(ref _fileName, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private NWBFileReader _nwbReader = null;

        /// <summary>
        /// 
        /// </summary>
        private bool _loadSucceed = false;
        public bool LoadSucceed
        {
            get { return _loadSucceed; }
            set { Set(ref _loadSucceed, value); }
        }

        /// <summary>
        /// 数据的采样率
        /// </summary>
        private int _sampleRate;
        public int SampleRate
        {
            get { return _sampleRate; }
            set { Set(ref _sampleRate, value); }
        }

        /// <summary>
        /// 数据的时间长度
        /// </summary>
        private TimeSpan _duration = TimeSpan.Zero;
        public TimeSpan Duration
        {
            get { return _duration; }
            set { Set(ref _duration, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private long _totalRow = 0;
        private List<int> _physicsChannels = new List<int>();
        private int _channelCount = 0;

        /// <summary>
        /// 通道映射关系
        /// </summary>
        private NTProbeMap _probeMap = new NTProbeMap();
        public NTProbeMap ProbeMap
        {
            get { return _probeMap; }
            private set { Set(ref _probeMap, value); }
        }

        /// <summary>
        /// 设备类型
        /// </summary>
        private DeviceType _deviceType = DeviceType.CS36;
        public DeviceType DeviceType
        {
            get { return _deviceType; }
            private set { Set(ref _deviceType, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private int _deviceChannelCount = 64;
        public int DeviceChannelCount
        {
            get { return _deviceChannelCount; }
            set { Set(ref _deviceChannelCount, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly Stopwatch _watch = new Stopwatch();
        private long _readedRowIndex = 0;
        private long _sentChunkTimes = 0;
        private int _readChunkRowsOnce = 0;
        private int _dataChunkRows = 0;

        /// <summary>
        /// 
        /// </summary>
        private CollectMode _collectMode = CollectMode.RawAll;
        public CollectMode CollectMode
        {
            get { return _collectMode; }
            set { Set(ref _collectMode, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly object _chunkSizeLock = new object();

        /// <summary>
        /// 
        /// </summary>
        private NWBDataProcess _process = new NWBDataProcess();

        /// <summary>
        /// 
        /// </summary>
        private NWBPlayController()
        {
            Task.Factory.StartNew(PullDataLoop, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// 开始播放文件
        /// </summary>
        /// <returns></returns>
        public bool StartReadFile(NWBCollectParamInfo collectParam, double speedX)
        {
            NeuralDataCollector.Instance.StartNewCollect(collectParam);

            //
            _watch.Reset();
            _readedRowIndex = Convert.ToInt64(PlayedTime.TotalSeconds * _sampleRate);
            if (_readedRowIndex >= _totalRow)
            {
                _readedRowIndex = 0;
            }
            _sentChunkTimes = 0;
            _readChunkRowsOnce = Convert.ToInt32(_dataChunkRows * speedX);
            SpeedX = speedX;

            //新的播放
            if (!_isReading)
            {
                _process.ReInit(_channelCount, _sampleRate, DeviceType == DeviceType.HInSX);
            }
            _isReading = true;
            _resetEvent.Set();
            return true;
        }

        /// <summary>
        /// 调整播放速度
        /// </summary>
        /// <param name="speedX"></param>
        public void SpeedUp(double speedX)
        {
            lock (_chunkSizeLock)
            {
                _readChunkRowsOnce = Convert.ToInt32(_dataChunkRows * speedX);
                SpeedX = speedX;
            }
        }

        /// <summary>
        /// 快进 or 快退
        /// </summary>
        /// <param name="seconds"></param>
        public void MovePlayedLine(int seconds)
        {
            PlayedTime = PlayedTime.Add(TimeSpan.FromSeconds(seconds));
        }

        /// <summary>
        /// 停止文件播放
        /// </summary>
        /// <returns></returns>
        public bool StopReadFile()
        {
            _isReading = false;

            Thread.Sleep(100);

            //
            NeuralDataCollector.Instance.StopCollect();
            NeuralDataRecorder.Instance.StopRecording();

            //
            return true;
        }

        /// <summary>
        /// 设置播放的文件
        /// </summary>
        /// <param name="sourcePath"></param>
        public bool SetSourcePath(string sourcePath)
        {
            //
            if (_nwbReader != null)
            {
                _nwbReader.Close();
            }

            //
            _readedRowIndex = 0;
            _sentChunkTimes = 0;
            LoadSucceed = false;

            //
            try
            {
                //
                SourcePath = sourcePath;

                //
                _nwbReader = new NWBFileReader(SourcePath);
                CollectMode = _nwbReader.GetDataTypeDescription().Contains("Spike") ? CollectMode.Spike : CollectMode.RawAll;
                if (CollectMode == CollectMode.Spike)
                {
                    _totalRow = _nwbReader.GetSpikeLength();
                }
                else
                {
                    _totalRow = _nwbReader.GetFloatRows();
                }
                Duration = TimeSpan.FromMilliseconds(_totalRow * 1000.0 / _nwbReader.GetSampleRate());
                SampleRate = _nwbReader.GetSampleRate();
                _physicsChannels = _nwbReader.GetChannels();

                //
                ProbeMap = (NTProbeMap)JsonConvert.DeserializeObject<NTProbeMap>(_nwbReader.GetProbeMap());
                if (ImportedProbeMap)
                {
                    ProbeMap = ReadonlyCONST.ProbeMap;
                }
                if (System.Enum.TryParse(_nwbReader.GetDeviceType(), out DeviceType deviceType))
                {
                    DeviceType = deviceType;
                }
                else
                {
                    DeviceType = DeviceType.HInSX;
                }
                DeviceChannelCount = _nwbReader.GetDeviceChannelCount();

                //
                if (DeviceType == DeviceType.SS64)
                {
                    ReadonlyCONST.GetInputIndexInReport = (int inputChannel) =>
                    {
                        if (inputChannel < IntanConst.RHD2164_ONE_CHIP_CHANNELS)
                        {
                            return inputChannel % (IntanConst.RHD2164_ONE_CHIP_CHANNELS / 2) * 2 + (inputChannel / (IntanConst.RHD2164_ONE_CHIP_CHANNELS / 2));
                        }
                        else
                        {
                            inputChannel -= IntanConst.RHD2164_ONE_CHIP_CHANNELS;
                            return inputChannel % IntanConst.RHS2116_ONE_CHIP_CHANNELS * 4 + inputChannel / IntanConst.RHS2116_ONE_CHIP_CHANNELS + IntanConst.RHD2164_ONE_CHIP_CHANNELS;
                        }
                    };
                }
                else
                {
                    ReadonlyCONST.GetInputIndexInReport = null;
                }

                //
                _channelCount = _physicsChannels.Count;
                _dataChunkRows = SampleRate * SEND_INTERVAL_MS / 1000;
                _readChunkRowsOnce = _dataChunkRows;
                SpeedX = 1.0;
                PlayedTime = TimeSpan.Zero;

                //
                LoadSucceed = true;
                return true;
            }
            catch
            {
                SourcePath = string.Empty;
                return false;
            }
        }

        /// <summary>
        /// 采集的通道
        /// </summary>
        /// <returns></returns>
        public List<int> GetInputChannels()
        {
            var inputChannels = new List<int>();
            foreach (var item in _physicsChannels)
            {
                inputChannels.Add(ProbeMap.GetInputIndex(item));
            }
            return inputChannels;
        }

        /// <summary>
        /// 采集的物理通道顺序
        /// </summary>
        /// <returns></returns>
        public List<int> GetPhysicsChannels()
        {
            return _physicsChannels;
        }

        /// <summary>
        /// 提取数据
        /// </summary>
        private void PullDataLoop()
        {
            //
            Thread.CurrentThread.Priority = ThreadPriority.Highest;

            //
            while (true)
            {
                Thread.Sleep(20);

                //
                if (_isReading)
                {
                    PullDataOnce();
                }
                else
                {
                    _resetEvent.WaitOne();
                }
            }
        }

        /// <summary>
        /// 提取一次数据
        /// </summary>
        private void PullDataOnce()
        {
            _watch.Start();

            //
            if (_watch.ElapsedMilliseconds / SEND_INTERVAL_MS <= _sentChunkTimes)
            {
                return;
            }

            //
            lock (_chunkSizeLock)
            {
                //
                var readRows = Math.Min(_totalRow - _readedRowIndex, _readChunkRowsOnce);
                if (readRows <= 0)
                {
                    EventHelper.Publish<FileReadCompletedEvent>();
                    _isReading = false; //先停掉PullDataLoop循环
                    return;
                }

                //
                if (CollectMode == CollectMode.Spike)
                {
                    var chunk = _nwbReader.GetSpikeDatas(_readedRowIndex, readRows);

                    //spikes:ch1 ch2 ... chn ch1 ch2 ... chn ch1 ch2 ... chn
                    var spks = ArrayPool<float>.Shared.Rent((int)(readRows * _channelCount));
                    for (int i = 0; i < readRows; i++)
                    {
                        for (int j = 0; j < _channelCount; j++)
                        {
                            spks[i * _channelCount + j] = chunk[i, j];
                        }
                    }

                    //
                    _process.HandleSPKDataFrame(spks, (int)readRows);
                }
                else
                {
                    var chunk = _nwbReader.GetFloatDatas(_readedRowIndex, readRows);
                    //wpfs:ch1 ch2 ... chn ch1 ch2 ... chn ch1 ch2 ... chn
                    var wfps = ArrayPool<float>.Shared.Rent((int)(readRows * _channelCount));
                    for (int i = 0; i < readRows; i++)
                    {
                        for (int j = 0; j < _channelCount; j++)
                        {
                            wfps[i * _channelCount + j] = chunk[i, j];
                        }
                    }

                    //
                    _process.HandleWPFDataFrame(wfps, (int)readRows);
                }

                //
                _readedRowIndex += readRows;

                //
                PlayedTime = TimeSpan.FromMilliseconds(1000.0 * _readedRowIndex / _sampleRate);
            }

            //
            _sentChunkTimes++;
        }
    }
}
