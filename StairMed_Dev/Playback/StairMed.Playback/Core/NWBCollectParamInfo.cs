﻿using StairMed.Entity.Infos.Bases;
using System;
using System.Collections.Generic;

namespace StairMed.Playback.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class NWBCollectParamInfo : CollectParamInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public NWBCollectParamInfo(int channelCount) : base(channelCount)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        private TimeSpan _dataDuration;
        public TimeSpan DataDuration
        {
            get { return _dataDuration; }
            set { Set(ref _dataDuration, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        public void ChooseChannels(List<int> channels)
        {
            InitChannels();
            foreach (var item in Channels)
            {
                if (channels.Contains(item.InputIndex))
                {
                    item.Enable = true;
                }
                else
                {
                    item.Enable = false;
                }
            }
        }
    }
}
