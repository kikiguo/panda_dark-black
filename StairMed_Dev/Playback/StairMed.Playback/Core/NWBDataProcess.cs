﻿using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.ChipConst.Intan;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers;
using StairMed.Entity.DataPools;
using StairMed.Enum;
using StairMed.FilterProcess;
using StairMed.FilterProcess.ParamMonitor;
using System;
using System.Buffers;
using System.Collections.Generic;

namespace StairMed.Playback.Core
{
    /// <summary>
    /// 
    /// </summary>
    internal class NWBDataProcess
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _notchFilterSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.NotchFilterType),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _lowFilterSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.LowPassFilterCutoff),
            nameof(StairMedSettings.Instance.LowPassFilterOrder),
            nameof(StairMedSettings.Instance.LowPassFilterType),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _highFilterSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.HighPassFilterCutoff),
            nameof(StairMedSettings.Instance.HighPassFilterOrder),
            nameof(StairMedSettings.Instance.HighPassFilterType),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _careSettings = new HashSet<string>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IAsyncFIFO _wpfsFIFO = null; //数据chunk处理线程
        private readonly IAsyncFIFO _spksFIFO = null; //数据chunk处理线程
        private readonly IAsyncFIFO _wpfChunkFIFO = null; //数据chunk处理线程
        private readonly IAsyncFIFO _spkChunkFIFO = null; //数据chunk处理线程

        /// <summary>
        /// 
        /// </summary>
        private IXPU _xpu = null; //数据处理
        private int _channelCount = 0;
        private int _sampleRate = 0;
        private readonly ExternalTrigMonitor _externalTrigMonitor;
        /// <summary>
        /// 
        /// </summary>
        public NWBDataProcess()
        {
            //
            _wpfsFIFO = new BlockFIFO { Name = nameof(_wpfsFIFO), ConsumerAction = WPFConsume };
            _spksFIFO = new BlockFIFO { Name = nameof(_spksFIFO), ConsumerAction = SPKConsume };
            _wpfChunkFIFO = new BlockFIFO { Name = nameof(_wpfChunkFIFO), ConsumerAction = WPFChunkConsume };
            _spkChunkFIFO = new BlockFIFO { Name = nameof(_spkChunkFIFO), ConsumerAction = SPKChunkConsume };

            //
            _xpu = XPUFactory.Instance.GetXPU();
            _xpu.ParamCenter.SetRawKB(IntanConst.CoefK, IntanConst.CoefB);
            

            //
            _careSettings.UnionWith(_notchFilterSettings);
            _careSettings.UnionWith(_lowFilterSettings);
            _careSettings.UnionWith(_highFilterSettings);

            //
            StairMedSettings.Instance.OnSettingChanged += (HashSet<string> changes) =>
            {
                if (_careSettings.Overlaps(changes))
                {
                    UpdateFilterParams();
                }
            };
            SpikeThresholdCenter.Instance.OnThresholdChanged += () =>
            {
                UpdateFilterParams();
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelCount"></param>
        /// <param name="sampleRate"></param>
        internal void ReInit(int channelCount, int sampleRate, bool useGpu)
        {
            _channelCount = channelCount;
            _sampleRate = sampleRate;

            //
            _xpu.Free();
            _xpu = XPUFactory.Instance.GetXPU();

            //
            UpdateFilterParams();
        }

        #region WPF

        /// <summary>
        /// wpfs:ch1 ch2 ... chn ch1 ch2 ... chn ch1 ch2 ... chn
        /// </summary>
        /// <param name="wfps"></param>
        /// <param name="sampleCount"></param>
        internal void HandleWPFDataFrame(float[] wfps, int sampleCount)
        {
            _wpfsFIFO.Add(sampleCount, wfps);
        }

        /// <summary>
        /// 数据块处理
        /// </summary>
        /// <param name="objs"></param>
        private void WPFConsume(object[] objs)
        {
            var sampleCount = (int)objs[0];
            var chunk = (float[])objs[1];

            //
            _xpu.ParamCenter.SetCommonParam(_channelCount, sampleCount, _sampleRate);
            _xpu.ProcessWFPChunk(chunk, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs);
            ArrayPool<float>.Shared.Return(chunk);

            //
            _wpfChunkFIFO.Add(_channelCount, sampleCount, wfps, hfps, lfps, spks,trigs);
        }

     

        /// <summary>
        /// 数据块处理
        /// </summary>
        /// <param name="objs"></param>
        private void WPFChunkConsume(object[] objs)
        {
            var channelCount = (int)objs[0];
            var sampleCount = (int)objs[1];
            var wfps = (PooledArray<float>)objs[2];
            var hfps = (PooledArray<float>)objs[3];
            var lfps = (PooledArray<float>)objs[4];
            var spks = (PooledArray<float>)objs[5];
            var trigs= (PooledArray<int>)objs[6];
            //
            NeuralDataCollector.Instance.BufferChunk(new Chunk
            {
                InputChannelInReports = NeuralDataCollector.Instance.GetInputChannelInReports(),
                ChannelCount = channelCount,
                OneChannelSamples = sampleCount,
                SampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate(),
                SpikeScopeTimeMS = StairMedSettings.Instance.SpikeScopeTimeMS,
                SpikeSumStepMS = StairMedSettings.Instance.SpikeSumStepMS,
                SpikeSumWindowMS = StairMedSettings.Instance.SpikeSumWindowMS,
                Raws = wfps,
                Hfps = hfps,
                Lfps = lfps,
                Spks = spks,
                Trigs = trigs,
            });
        }

        #endregion



        #region Spike

        /// <summary>
        /// spks:ch1 ch2 ... chn ch1 ch2 ... chn ch1 ch2 ... chn
        /// </summary>
        /// <param name="spks"></param>
        /// <param name="sampleCount"></param>
        internal void HandleSPKDataFrame(float[] spks, int sampleCount)
        {
            _spksFIFO.Add(sampleCount, spks);
        }

        /// <summary>
        /// 数据块处理
        /// </summary>
        /// <param name="objs"></param>
        private void SPKConsume(object[] objs)
        {
            var sampleCount = (int)objs[0];
            var chunk = (float[])objs[1];

            //
            _xpu.ParamCenter.SetCommonParam(_channelCount, sampleCount, _sampleRate);
            _xpu.ProcessSpkChunk(chunk, out PooledArray<float> spks);
            ArrayPool<float>.Shared.Return(chunk);

            //
            _spkChunkFIFO.Add(_channelCount, sampleCount, spks);
        }

        /// <summary>
        /// 数据块处理
        /// </summary>
        /// <param name="objs"></param>
        private void SPKChunkConsume(object[] objs)
        {
            var channelCount = (int)objs[0];
            var sampleCount = (int)objs[1];
            var spks = (PooledArray<float>)objs[2];
            var channelSpikeCount = (PooledArray<int>)objs[3];
            var channelHfpSquareSum = (PooledArray<float>)objs[4];

            //
            NeuralDataCollector.Instance.BufferChunk(new Chunk
            {
                InputChannelInReports = NeuralDataCollector.Instance.GetInputChannelInReports(),
                ChannelCount = channelCount,
                OneChannelSamples = sampleCount,
                SampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate(),
                SpikeScopeTimeMS = StairMedSettings.Instance.SpikeScopeTimeMS,
                SpikeSumStepMS = StairMedSettings.Instance.SpikeSumStepMS,
                SpikeSumWindowMS = StairMedSettings.Instance.SpikeSumWindowMS,
                Spks = spks,
            });
        }


        #endregion


        /// <summary>
        /// 
        /// </summary>
        private void UpdateFilterParams()
        {
            var notchHz = StairMedSettings.Instance.NotchFilterType == NotchFilterType._60Hz ? 60 : StairMedSettings.Instance.NotchFilterType == NotchFilterType._50Hz ? 50 : 0;
            UpdateNotchFilter(notchHz);

            //            
            UpdateLowPassFilter(StairMedSettings.Instance.LowPassFilterCutoff, StairMedSettings.Instance.LowPassFilterOrder, StairMedSettings.Instance.LowPassFilterType);

            //
            UpdateHighPassFilter(StairMedSettings.Instance.HighPassFilterCutoff, StairMedSettings.Instance.HighPassFilterOrder, StairMedSettings.Instance.HighPassFilterType);

            //
            var hashset = new HashSet<float>();
            var thresholds = new List<float>();
            var inputChannels = NeuralDataCollector.Instance.GetInputChannelInReports();
            foreach (var channel in inputChannels)
            {
                var threshold = Convert.ToSingle(SpikeThresholdCenter.Instance.GetThreshold(channel));
                thresholds.Add(threshold);
                hashset.Add(threshold);
            }
            if (hashset.Count > 1)
            {
                UpdateSpikeParam(thresholds);
            }
            else
            {
                UpdateSpikeParam(thresholds[0]);
            }


        }



        #region 数据处理参数

        /// <summary>
        /// 高通滤波
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        private void UpdateHighPassFilter(int cutoff, int order, FilterType filterType)
        {
            _xpu.ParamCenter.SetHighPassFilter(cutoff, order, filterType);
        }

        /// <summary>
        /// 低通滤波
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="filterType"></param>
        private void UpdateLowPassFilter(int cutoff, int order, FilterType filterType)
        {
            _xpu.ParamCenter.SetLowPassFilter(cutoff, order, filterType);
        }

        /// <summary>
        /// 陷波滤波
        /// </summary>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        private void UpdateNotchFilter(int notchHz, int notchBandwidth = 10)
        {
            _xpu.ParamCenter.SetNotchFilter(notchHz, notchBandwidth);
        }

        /// <summary>
        /// spike检测阈值
        /// </summary>
        /// <param name="thresholds"></param>
        private void UpdateSpikeParam(List<float> thresholds)
        {
            _xpu.ParamCenter.SetSpikeParam(thresholds);
        }

        /// <summary>
        /// spike检测阈值
        /// </summary>
        /// <param name="threshold"></param>
        private void UpdateSpikeParam(float threshold)
        {
            _xpu.ParamCenter.SetSpikeParam(threshold);
        }


     
        #endregion
    }
}
