﻿using Prism.Regions;
using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.DataCenter;
using StairMed.MVVM.Helper;
using StairMed.Playback.Logger;
using StairMed.Playback.Views.Regions;
using StairMed.Playback.Views.Regions.Bodies;
using System;
using System.Windows;
using System.Windows.Input;

namespace StairMed.Playback.Views.Windows
{
    /// <summary>
    /// MainstayWnd.xaml 的交互逻辑
    /// </summary>
    public partial class MainstayWnd 
    {
        public static RoutedCommand ShortCutCommand = new RoutedCommand();

        /// <summary>
        /// 
        /// </summary>
        public MainstayWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            RegionManager.SetRegionManager(this, RegionHelper.RegionManager);

            //
            InitializeComponent();
            this.WindowState = WindowState.Normal;
            this.btnRestore.Visibility = Visibility.Collapsed;

            //
            Application.Current.MainWindow = this;

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                RegionHelper.RequestNavigate(RegionNames.HeadRegion, nameof(HeadView));
                RegionHelper.RequestNavigate(RegionNames.NeckRegion, nameof(NeckView));
                RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(HistoryView));
                //RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(BodyDefaultView));
            };

            //
            this.BindingDragMove();

            //
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };

            //
            this.Closing += Window_Closing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (NeuralDataCollector.Instance.IsCollecting)
            {
                Toast.Warn("请停止数据回放，再做关闭");
                e.Cancel = true;
                return;
            }

            //
            if (MsgBox.AskAnxious("Sure you want out？", "Exit", this) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
                return;
            }

            //
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
