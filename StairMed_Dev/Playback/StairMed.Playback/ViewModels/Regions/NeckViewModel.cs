﻿using StairMed.Core.Settings;
using StairMed.MVVM.Base;
using StairMed.Playback.Core;

namespace StairMed.Playback.ViewModels.Regions
{
    /// <summary>
    /// 
    /// </summary>
    internal class NeckViewModel : RegionViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public NWBPlayController PlayController { get; set; } = NWBPlayController.Instance;
    }
}
