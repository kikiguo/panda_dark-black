﻿using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.DataCenter;
using StairMed.Enum;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.Playback.Views.Regions.Bodies;
using StairMed.Playback.Views.Windows;
using StairMed.UI.Views.Windows.Settings;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;

namespace StairMed.Playback.ViewModels.Regions
{
    /// <summary>
    /// 
    /// </summary>
    internal class HeadViewModel : RegionViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public NeuralDataCollector NeuralDataCollector { get; set; } = NeuralDataCollector.Instance;

        /// <summary>
        /// 
        /// </summary>
        public ICommand SwitchBody => new DelegateCommand<object>((obj) =>
        {
            var bodyType = (BodyType)obj;
            switch (bodyType)
            {
                case BodyType.History:
                    {
                        RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(HistoryView));
                    }
                    break;
                default:
                    break;
            }
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand ShowAbout => new DelegateCommand<object>((obj) =>
        {
            AboutWnd.PopUp();
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand Setting => new DelegateCommand<object>((obj) =>
        {
            SettingWnd.PopUp();
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand Log => new DelegateCommand<object>((obj) =>
        {
            try
            {
                Process.Start("explorer.exe", Path.Combine(CONST.AppInstallPath, "Log"));
            }
            catch { }
        });
    }
}
