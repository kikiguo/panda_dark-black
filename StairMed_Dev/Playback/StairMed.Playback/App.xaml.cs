﻿using Prism.DryIoc;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using StairMed.Logger;
using StairMed.MVVM.Helper;
using StairMed.Playback.Views.Regions;
using StairMed.Playback.Views.Regions.Bodies;
using StairMed.Playback.Views.Windows;
using StairMed.Tools;
using StairMed.UI.Views.Windows;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.Playback
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        /// <summary>
        /// 
        /// </summary>
        private static Mutex _singletonMutex = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            //单实例运行
            var name = this.GetType().FullName;
            _singletonMutex = new Mutex(true, this.GetType().FullName, out bool createdNew);
            if (!createdNew)
            {
                Thread.Sleep(500);
                this.Shutdown();
                return;
            }

            //
            StairMed.Playback.Logger.LogTool.InitLogger(this.GetType().FullName.Split(".")[1]);
            Logger.LogTool.Logger.LogF($">>>>>>>>>>>>>>>>>>>>>>>>>> enter app, version:{VersionInfo.SoftVersion}, build:{VersionInfo.BuildDate}");

            //
            Initializer.Init();

            //
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
            base.OnStartup(e);
            this.Exit += (object sender, ExitEventArgs e) =>
            {
                Logger.LogTool.Logger.LogF($"<<<<<<<<<<<<<<<<<<<<<<<<<< leave app, version:{VersionInfo.SoftVersion}, build:{VersionInfo.BuildDate}");
                UIHelper.Skip = true;
                StairMedOutput.ApplicationExit = true;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskScheduler_UnobservedTaskException(object? sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception != null && e.Exception.InnerExceptions.Count > 0)
            {
                Logger.LogTool.Logger.Exp($"", e.Exception.InnerExceptions[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Window CreateShell()
        {
            return new WelcomeWnd(() =>
            {
                new MainstayWnd().ShowDialog();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        protected override void RegisterTypes(IContainerRegistry container)
        {
            ContainerHelper.Container = Container.Resolve<IContainerExtension>();
            EventHelper.EventAggregator = ContainerHelper.Resolve<IEventAggregator>();
            RegionHelper.RegionManager = ContainerHelper.Resolve<IRegionManager>();

            //
            container.RegisterForNavigation<HeadView>();
            container.RegisterForNavigation<NeckView>();
            container.RegisterForNavigation<BodyDefaultView>();
            container.RegisterForNavigation<HistoryView>();
        }
    }
}
