﻿using StairMed.Logger;
using System.Collections.Generic;

namespace StairMed.Playback.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public static class LogTool
    {
        /// <summary>
        /// 
        /// </summary>
        public static StairMed.Logger.ILogger Logger = new StairMed.Logger.Logger();

        /// <summary>
        /// 
        /// </summary>
        private static readonly List<ILogger> _loggers = new List<ILogger>
        {
            StairMed.Playback.Logger.LogTool.Logger,
            StairMed.FilterProcess.Logger.LogTool.Logger,
            StairMed.UI.Logger.LogTool.Logger,
            StairMed.Controls.Logger.LogTool.Logger,
            StairMed.DataCenter.Logger.LogTool.Logger,
            StairMed.Core.Logger.LogTool.Logger,
        };

        /// <summary>
        /// 
        /// </summary>
        public static void InitLogger(string fileName)
        {
            var output = new StairMedOutput { FileName = fileName };
            foreach (var logger in _loggers)
            {
                logger.Output = output;
                logger.LogLevel = LogLevel.Trace;
            }
        }
    }
}
