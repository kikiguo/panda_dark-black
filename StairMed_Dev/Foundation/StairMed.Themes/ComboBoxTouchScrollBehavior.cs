﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace StairMed.Themes.Behaviors
{
    /// <summary>
    /// Add touch support to a combobox: 
    /// Supports touch-based drag-and-drop.
    /// Prevents the ManipulationBoundaryFeedback flowing to the parent.
    /// </summary>
    public class ComboBoxTouchScrollBehavior
    {
        /// <summary>
        /// The property which should be set to attach the behavior.
        /// </summary>
        public static readonly DependencyProperty IsAttachedProperty =
            DependencyProperty.RegisterAttached("IsAttached", typeof(bool), typeof(ComboBoxTouchScrollBehavior), new UIPropertyMetadata(false, AttachToElement));

        /// <summary>
        /// Backing field for actual open combo box.
        /// </summary>
        private static ComboBox openBox;

        /// <summary>
        /// Gets the is attached.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <returns>True if behavior is attached</returns>
        public static bool GetIsAttached(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsAttachedProperty);
        }

        /// <summary>
        /// Sets the is attached.
        /// </summary>
        /// <param name="obj">The dependency object.</param>
        /// <param name="value">If set to <c>true</c> [value].</param>
        public static void SetIsAttached(DependencyObject obj, bool value)
        {
            obj.SetValue(IsAttachedProperty, value);
        }

        /// <summary>
        /// Attaches to element.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <param name="eventArgs">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AttachToElement(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs eventArgs)
        {
            var comboBox = dependencyObject as ComboBox;
            if (comboBox != null)
            {
                comboBox.Loaded += OnLoaded;
                comboBox.Unloaded += OnUnloaded;
            }
        }

        /// <summary>
        /// Handles the drop down closed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private static void HandleDropDownClosed(object sender, EventArgs e)
        {
            openBox = null;

            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.TouchMove -= MainWindow_TouchMove;
            }
        }

        /// <summary>
        /// Handles the drop down opened.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private static void HandleDropDownOpened(object sender, EventArgs e)
        {
            openBox = sender as ComboBox;

            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.TouchMove += MainWindow_TouchMove;
            }
        }

        /// <summary>
        /// Handles the manipulation boundary feedback.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ManipulationBoundaryFeedbackEventArgs"/> instance containing the event data.</param>
        private static void HandleManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Handles the TouchMove event of the MainWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.TouchEventArgs"/> instance containing the event data.</param>
        private static void MainWindow_TouchMove(object sender, TouchEventArgs e)
        {
            if (openBox == null)
            {
                return;
            }

            if (!openBox.AreAnyTouchesOver)
            {
                openBox.IsDropDownOpen = false;
            }
        }

        /// <summary>
        /// Called when [loaded].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void OnLoaded(object sender, RoutedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null)
            {
                return;
            }

            comboBox.ManipulationBoundaryFeedback += HandleManipulationBoundaryFeedback;
            comboBox.DropDownOpened += HandleDropDownOpened;
            comboBox.DropDownClosed += HandleDropDownClosed;
        }

        /// <summary>
        /// Called when [unloaded].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private static void OnUnloaded(object sender, RoutedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null)
            {
                return;
            }

            comboBox.ManipulationBoundaryFeedback -= HandleManipulationBoundaryFeedback;
            comboBox.DropDownOpened -= HandleDropDownOpened;
            comboBox.DropDownClosed -= HandleDropDownClosed;
        }
    }
}
