﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StairMed.Themes.converters
{
    /// <summary>
    /// 
    /// </summary>
    public class IfEqualElse : IMultiValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var count = (values.Length - 1) / 2;
            for (int i = 0; i < count; i++)
            {
                if (values[0].Equals(values[1 + i * 2]))
                {
                    return values[2 + i * 2];
                }
            }

            //
            return values[^1];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SpikeScopeHeight : IMultiValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double minHeight = 80;
            try
            {
                var count = values.Length;
                if (count == 0)
                {
                    return minHeight;
                }
                if (count == 1)
                {
                    return values[^1];
                }
                else
                {
                    double v0 = 0.0;
                    double.TryParse(values[^1]?.ToString(), out v0);//parent height

                    double v1 = 1.0;
                    double.TryParse(values[^2]?.ToString(), out v1);//cols

                    double v2 = 128 / v1;//rows

                    var retvalue = (double)(v0 / v2);
                    if (retvalue < minHeight)
                        return minHeight;
                    return Math.Floor(retvalue);
                }
            }
            catch (Exception ex)
            {
                return minHeight;
            }
            //

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class EnumToBoolenConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                string val = value?.ToString();
                string param = parameter?.ToString();
                if (val == param)
                    return true;
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                bool val = System.Convert.ToBoolean(value);
                if (val)
                    return parameter?.ToString();
                return "";
            }
            catch (Exception)
            {

                return "";
            }
        }
    }
}
