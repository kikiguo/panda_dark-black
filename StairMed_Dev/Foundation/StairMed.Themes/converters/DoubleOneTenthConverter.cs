﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StairMed.Themes.converters
{
    /// <summary>
    /// 
    /// </summary>
    public class DoubleOneTenthConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (double.TryParse(value.ToString(), out double result))
                {
                    if (result >= 10)
                    {
                        return Math.Round(result / 10);
                    }
                }
            }
            catch { }
            return 1.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
