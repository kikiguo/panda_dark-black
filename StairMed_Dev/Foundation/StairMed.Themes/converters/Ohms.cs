﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StairMed.Themes.converters
{
    /// <summary>
    /// 
    /// </summary>
    public class Ohms : IValueConverter
    {
        const int M = 1000 * 1000;
        const int K = 1000;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var ohms = (double)value;
                if (ohms >= M)
                {
                    return $"{ohms / M:F2}MΩ";
                }
                if (ohms >= K)
                {
                    return $"{ohms / K:F2}kΩ";
                }
                return $"{ohms:F2}Ω";
            }
            catch { }
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
