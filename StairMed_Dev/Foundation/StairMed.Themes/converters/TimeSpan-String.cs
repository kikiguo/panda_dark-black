﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StairMed.Themes.converters
{
    /// <summary>
    /// 
    /// </summary>
    public class TimeSpan_String : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var timespan = (TimeSpan)value;
            var formatStr = @"hh\:mm\:ss";
            if (timespan.Days > 0)
            {
                formatStr = @"dd\:hh\:mm\:ss";
            }
            return timespan.ToString(formatStr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
