﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Markup;

namespace StairMed.Themes
{
    public class EnumBindingSourceExtension : MarkupExtension
    {
        private Type _enumType;

        public Type EnumType
        {
            get { return _enumType; }
            set
            {
                if (_enumType != value)
                {
                    if (value != null) {
                        var enumType = Nullable.GetUnderlyingType(value) ?? value;
                        if(!enumType.IsEnum) {
                            throw new ArgumentException("Type must be for an enum");
                        }
                    }

                    _enumType = value;
                }
            }
        }

        public EnumBindingSourceExtension()
        {
            
        }

        public EnumBindingSourceExtension(Type enumType)
        {
            EnumType=enumType;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if(null== _enumType)
            {
                throw new InvalidOperationException("The enumtype must be specified.");
            }
            var actualEnumType = Nullable.GetUnderlyingType(_enumType) ?? _enumType;
            var enumValues = System.Enum.GetValues(actualEnumType);
            if (actualEnumType == _enumType) {
                return enumValues;
            }
            var tempArray = System.Array.CreateInstance(actualEnumType, enumValues.Length + 1);
            enumValues.CopyTo(tempArray, 1);

            return tempArray;
        }
    }
}
