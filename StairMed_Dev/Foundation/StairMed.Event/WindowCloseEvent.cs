﻿using Prism.Events;

namespace StairMed.Event
{
    /// <summary>
    /// 分析窗口关闭事件通知viewmodel
    /// </summary>
    public class WindowCloseEvent : PubSubEvent<string>
    {

    }
    public class GenericWndCloseEvent : PubSubEvent<string>
    {

    }

    public class HeadstageInsertedEvent : PubSubEvent
    {

    }
}
