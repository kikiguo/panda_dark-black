﻿using Prism.Events;

namespace StairMed.Event
{
    /// <summary>
    /// CS36 设备页 自适应设备个数 宽度
    /// </summary>
    public class HardwareViewWidthChangedEvent : PubSubEvent<double>
    {
    }
}
