﻿using Prism.Events;

namespace StairMed.Event
{
    /// <summary>
    /// 数据回放：文件播放完毕
    /// </summary>
    public class FileReadCompletedEvent : PubSubEvent
    {
    }
}
