﻿using Prism.Events;
using System.Collections.Generic;

namespace StairMed.Event
{
    /// <summary>
    /// 通知viewmode显示哪个通道/隐藏哪个通道（通过itemsoure的filter）
    /// </summary>
    public class FilterDisplayChannelsEvent : PubSubEvent<List<int>>
    {

    }
}
