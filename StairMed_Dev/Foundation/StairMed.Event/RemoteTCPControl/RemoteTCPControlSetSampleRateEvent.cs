﻿using Prism.Events;
using System.Collections.Generic;

namespace StairMed.Event.RemoteTCPControl
{
    /// <summary>
    /// tcp远程控制：设置采样率
    /// </summary>
    public class RemoteTCPControlSetSampleRateEvent : PubSubEvent<List<string>>
    {
    }
}
