﻿using Prism.Events;
using System.Collections.Generic;

namespace StairMed.Event.RemoteTCPControl
{
    /// <summary>
    /// tcp远程控制：选择通道
    /// </summary>
    public class RemoteTCPControlSelectCommandEvent : PubSubEvent<List<string>>
    {
    }
}
