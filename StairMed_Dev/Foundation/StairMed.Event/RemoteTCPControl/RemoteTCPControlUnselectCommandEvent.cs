﻿using Prism.Events;
using System.Collections.Generic;

namespace StairMed.Event.RemoteTCPControl
{
    /// <summary>
    /// tcp远程控制：取消选择某些通道
    /// </summary>
    public class RemoteTCPControlUnselectCommandEvent : PubSubEvent<List<string>>
    {
    }
}
