﻿using Prism.Events;
using System.Collections.Generic;

namespace StairMed.Event.RemoteTCPControl
{
    /// <summary>
    /// tcp远程控制：启动和停止采集
    /// </summary>
    public class RemoteTCPControlRunModeCommandEvent : PubSubEvent<List<string>>
    {
    }
}
