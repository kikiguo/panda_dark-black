﻿using Prism.Events;

namespace StairMed.Event
{
    /// <summary>
    /// 分析窗口打开事件通知viewmodel
    /// </summary>
    public class WindowLoadEvent : PubSubEvent<string>
    {

    }

    public class ChannelChangedEvent : PubSubEvent<int>
    { 
    
    }

    public class StartCollectEvent : PubSubEvent<int>
    { 
    
    }

    public class StopCollectEvent : PubSubEvent<int>
    { 
    
    }

    public class LayoutChangedEvent : PubSubEvent<int>
    { 
    
    }
    public class ChannelChangedEventFromSub : PubSubEvent<int>
    {

    }
    public class StartRecordEvent : PubSubEvent<int>
    {

    }

    public class StopRecordEvent : PubSubEvent<int>
    {

    }

    public class SelectNWBFileEvent : PubSubEvent<int>
    {

    }

    public class PlayNWBFileEvent : PubSubEvent<double>
    {

    }
    public class StopPlayNWBFileEvent : PubSubEvent<int>
    {

    }

    public class OpenPlaybackPanelEvent : PubSubEvent<bool>
    { 
    
    }
}
