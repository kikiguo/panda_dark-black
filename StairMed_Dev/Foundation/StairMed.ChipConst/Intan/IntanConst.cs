﻿using System.Collections.Generic;

namespace StairMed.ChipConst.Intan
{
    /// <summary>
    /// 
    /// </summary>
    public static class IntanConst
    {
        /// <summary>
        /// uV = k * (adc + b)
        /// </summary>
        public const float CoefK = 0.195f;

        /// <summary>
        /// uV = k * (adc + b)
        /// </summary>
        public const int CoefB = -0x8000;

        /// <summary>
        /// 每片2116的通道数
        /// </summary>
        public const int RHS2116_ONE_CHIP_CHANNELS = 16;

        /// <summary>
        /// 每片2164的通道数
        /// </summary>
        public const int RHD2164_ONE_CHIP_CHANNELS = 64;

        /// <summary>
        /// DSP cutoff kfreqs
        /// </summary>
        public static readonly List<double> DSPCutoffKFreqs = new List<double>
        {
            0, //0
            0.1103, //1
            0.04579, //2
            0.02125, //3
            0.01027, //4
            0.005053, //5
            0.002506, //6
            0.001248, //7
            0.0006229, //8
            0.0003112, //9
            0.0001555, //10
            0.00007773, //11
            0.00003886, //12
            0.00001943, //13
            0.000009714, //14
            0.000004857, //15
        };

        /// <summary>
        /// Setting Lower Bandwidth: 
        /// On-Chip Register Values:
        /// The following settings for variables in Registers 12-13 are 
        /// used to configure the lower bandwidth(fL) of the amplifiers.
        /// </summary>
        public static readonly Dictionary<double, object> AmplifierLowerBandwidthDict = new Dictionary<double, object>
        {
            {500, (13, 0, 0)},
            {300, (15, 0, 0)},
            {250, (17, 0, 0)},
            {200, (18, 0, 0)},
            {150, (21, 0, 0)},
            {100, (25, 0, 0)},
            {75, (28, 0, 0)},
            {50, (34, 0, 0)},
            {30, (44, 0, 0)},
            {25, (48, 0, 0)},
            {20, (54, 0, 0)},
            {15, (62, 0, 0)},
            {10, (5, 1, 0)},
            {7.5, (18, 1, 0)},
            {5.0, (40, 1, 0)},
            {3.0, (20, 2, 0)},
            {2.5, (42, 2, 0)},
            {2.0, (8, 3, 0)},
            {1.5, (9, 4, 0)},
            {1.0, (44, 6, 0)},
            {0.75, (49, 9, 0)},
            {0.50, (35, 17, 0)},
            {0.30, (1, 40, 0)},
            {0.25, (56, 54, 0)},
            {0.10, (16, 60, 1)},
        };

        /// <summary>
        /// Setting Upper Bandwidth: 
        /// On-Chip Register Values
        /// The following settings for variables in Registers 8-11 are
        /// used to configure the upper bandwidth(fH) of the amplifiers.
        /// </summary>
        public static readonly Dictionary<double, object> AmplifierUpperBandwidthDict = new Dictionary<double, object>
        {
            {20000, (8, 0, 4, 0)},
            {15000, (11, 0, 8, 0)},
            {10000, (17, 0, 16, 0)},
            {7500, (22, 0, 23, 0)},
            {5000, (33, 0, 37, 0)},
            {3000, (3, 1, 13, 1)},
            {2500, (13, 1, 25, 1)},
            {2000, (27, 1, 44, 1)},
            {1500, (1, 2, 23, 2)},
            {1000, (46, 2, 30, 3)},
            {750, (41, 3, 36, 4)},
            {500, (30, 5, 43, 6)},
            {300, (6, 9, 2, 11)},
            {250, (42, 10, 5, 13)},
            {200, (24, 13, 7, 16)},
            {150, (44, 17, 8, 21)},
            {100, (38, 26, 5, 31)},
        };

        /// <summary>
        /// The following step DAC settings are used to configure the 
        /// step size of the stimulators.For values not listed on this
        /// table, contact Intan Technologies for recommended values.
        /// </summary>
        public static readonly Dictionary<double, object> StimulationCurrentStepSizeDict = new Dictionary<double, object>
        {
            {10, (64, 19, 3)},
            {20, (40, 40, 1)},
            {50, (64, 40, 0)},
            {100, (30, 20, 0)},
            {200, (25, 10, 0)},
            {500, (101, 3, 0)},
            {1000, (98, 1, 0)},
            {2000, (94, 0, 0)},
            {5000, (38, 0, 0)},
            {10000, (15, 0, 0)},
        };


        /// <summary>
        /// These variables configure internal bias voltages that optimize the compliance range of the 
        /// stimulator circuits.The optimum values for these variables are a function of stimulation step size (set in Register 34) and are listed 
        /// in a table earlier in the datasheet.This register is typically set once shortly after the chip is powered up.
        /// </summary>
        public static readonly Dictionary<double, object> StimulationBiasVoltagesDict = new Dictionary<double, object>
        {
            {10, (6, 6)},
            {20, (7, 7)},
            {50, (7, 7)},
            {100, (7, 7)},
            {200, (8, 8)},
            {500, (9, 9)},
            {1000, (10, 10)},
            {2000, (11, 11)},
            {5000, (14, 14)},
            {10000, (15, 15)},
        };
    }
}
