﻿using StairMed.ChipConst.Intan;
using StairMed.ChipConst.Logger;
using System;
using System.Collections.Generic;

namespace StairMed.ChipConst
{
    /// <summary>
    /// 
    /// </summary>
    public class IntanImpedenceTest
    {
        /// <summary>
        /// 
        /// </summary>
        private const double TwoPi = Math.PI * 2;
        private const double RadiansToDegrees = 180 / Math.PI; // 57.2957795132;
        private const double DegreesToRadians = Math.PI / 180; // 0.0174532925199;

        /// <summary>
        /// 
        /// </summary>
        public int Channel { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        private readonly double _frequency = 1000;
        private readonly double _sampleRate = 20000;
        private readonly double _amplitude = 128;
        private readonly bool _applyNotch = false;
        private readonly double _fNotch = 50;
        private readonly double _bandwidth = 10;
        private readonly double _highCutoff = 7500; //7.5k

        private readonly int _numPeriods = 10; //周期个数

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frequency"></param>
        /// <param name="sampleRate"></param>
        /// <param name="applyNotch"></param>
        /// <param name="fNotch"></param>
        /// <param name="bandwidth"></param>
        /// <param name="highCutoff"></param>
        public IntanImpedenceTest(double frequency, double sampleRate, double amplitude, bool applyNotch = false, double fNotch = 60, double bandwidth = 10, double highCutoff = 7500)
        {
            _frequency = frequency;
            _sampleRate = sampleRate;
            _amplitude = amplitude;
            _applyNotch = applyNotch;
            _fNotch = fNotch;
            _bandwidth = bandwidth;
            _highCutoff = highCutoff;

            //
            _numPeriods = (int)Math.Round(0.02 * _frequency); // Test each channel for at least 20 msec...
            _numPeriods = Math.Max(_numPeriods, 5); // ...but always measure across no fewer than 5 complete periods
            _numPeriods += 2; // + 2 periods to give time to settle initially
        }

        /// <summary>
        /// 生成测试波形，含N个周期
        /// </summary>
        /// <param name="frequency"></param>
        /// <param name="sampleRate"></param>
        /// <param name="amplitude"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        public bool CreateZcheckDac(out byte[] period, out int numPeriods)
        {
            period = new byte[0];
            numPeriods = _numPeriods;

            //
            if (!CreatePeriodZcheckDac(out byte[] periods))
            {
                return false;
            }

            //
            period = periods;

            //
            //var dacNumPeriods = numPeriods + 2; // + 2 periods to give time to settle initially
            //dacs = new byte[dacNumPeriods * periods.Length];
            //for (int i = 0; i < dacNumPeriods; i++)
            //{
            //    Array.Copy(periods, 0, dacs, i * periods.Length, periods.Length);
            //}

            //
            return true;
        }

        /// <summary>
        /// 计算阻抗
        /// </summary>
        /// <param name="waveformBytes"></param>
        /// <returns></returns>
        public ComplexPolar MeasureComplexAmplitude(List<byte[]> waveformBytes)
        {
            if (waveformBytes.Count != 3)
            {
                return new ComplexPolar
                {
                    Magnitude = 0,
                    Phase = 0,
                };
            }

            var waveforms = new List<List<double>>();
            for (int i = 0; i < waveformBytes.Count; i++)
            {
                var waveform = new List<double>();

                //
                var waveformByte = waveformBytes[i];
                var dataLen = waveformByte.Length / 2;
                for (int j = 0; j < dataLen; j++)
                {
                    var uv = IntanHelper.Adc2Uv(BitConverter.ToUInt16(waveformByte, j * 2));
                    waveform.Add(uv);
                }

                //
                waveforms.Add(waveform);
            }

            //
            return MeasureComplexAmplitude(waveforms[0].ToArray(), waveforms[1].ToArray(), waveforms[2].ToArray());
        }

        /// <summary>
        /// 计算阻抗：输入三种电容对应的波形
        /// </summary>
        /// <param name="waveform100fF"></param>
        /// <param name="waveform1pF"></param>
        /// <param name="waveform10pF"></param>
        /// <param name="applyNotch"></param>
        /// <param name="fNotch"></param>
        /// <param name="bandwidth"></param>
        /// <param name="sampleRate"></param>
        /// <param name="frequency"></param>
        /// <param name="numPeriods"></param>
        /// <param name="highCutoff">actualUpperBandwidth</param>
        /// <returns></returns>
        public ComplexPolar MeasureComplexAmplitude(double[] waveform100fF, double[] waveform1pF, double[] waveform10pF)
        {
            if (waveform100fF.Length != waveform1pF.Length || waveform100fF.Length != waveform10pF.Length)
            {
                return new ComplexPolar
                {
                    Magnitude = 0,
                    Phase = 0,
                };
            }

            //
            //if (true)
            //{
            //    LogTool.Logger.LogT($"{nameof(IntanImpedenceTest)}, {nameof(MeasureComplexAmplitude)}, 3 waveforms, channel={Channel} begin");
            //    for (int i = 0; i < waveform100fF.Length; i++)
            //    {
            //        LogTool.Logger.LogT($"channel={Channel}, i={i}, cap=0.1pf, μV={waveform100fF[i]:F2}, cap=1pf, μV={waveform1pF[i]:F2}, cap=10pf, μV={waveform10pF[i]:F2}");
            //    }
            //    LogTool.Logger.LogT($"{nameof(IntanImpedenceTest)}, {nameof(MeasureComplexAmplitude)}, 3 waveforms, channel={Channel} end");
            //}

            //
            var complex100fF = MeasureComplexAmplitude(waveform100fF, _applyNotch, _fNotch, _bandwidth, _sampleRate, _frequency, _numPeriods - 2);
            var complex1pF = MeasureComplexAmplitude(waveform1pF, _applyNotch, _fNotch, _bandwidth, _sampleRate, _frequency, _numPeriods - 2);
            var complex10pF = MeasureComplexAmplitude(waveform10pF, _applyNotch, _fNotch, _bandwidth, _sampleRate, _frequency, _numPeriods - 2);

            //
            var measuredImpedance = new List<ComplexPolar> { complex100fF, complex1pF, complex10pF };

            //
            const double DacVoltageAmplitude = 128.0 * (1.225 / 256.0); // this assumes the DAC amplitude was set to 128
            //const double parasiticCapacitance = 12.0e-12; // 15 pF; // Estimate of on-chip parasitic capicitance, including effective amplifier input capacitance.
            const double parasiticCapacitance = 15.0e-12; // 15 pF; // Estimate of on-chip parasitic capicitance, including effective amplifier input capacitance.

            double relativeFreq = _frequency / _sampleRate;
            double saturationVoltage = ApproximateSaturationVoltage(_frequency, _highCutoff);

            // Make sure chosen capacitor is below saturation voltage
            int bestAmplitudeIndex;
            if (measuredImpedance[2].Magnitude < saturationVoltage)
            {
                bestAmplitudeIndex = 2;
            }
            else if (measuredImpedance[1].Magnitude < saturationVoltage)
            {
                bestAmplitudeIndex = 1;
            }
            else
            {
                bestAmplitudeIndex = 0;
            }

            // If C2 and C3 are too close, C3 is probably saturated. Ignore C3.
            double capRatio = measuredImpedance[1].Magnitude / measuredImpedance[2].Magnitude;
            if (capRatio > 0.2)
            {
                if (bestAmplitudeIndex == 2)
                {
                    bestAmplitudeIndex = 1;
                }
            }

            double cSeries = 0.0;
            switch (bestAmplitudeIndex)
            {
                case 0:
                    cSeries = 0.1e-12;
                    break;
                case 1:
                    cSeries = 1.0e-12;
                    break;
                case 2:
                    cSeries = 10.0e-12;
                    break;
            }

            // Calculate current amplitude produced by on-chip voltage DAC
            double current = TwoPi * _frequency * DacVoltageAmplitude * cSeries;

            ComplexPolar impedance = new ComplexPolar();

            // Calculate impedance magnitude from calculated current and measured voltage.
            impedance.Magnitude = 1.0e-6 * (measuredImpedance[bestAmplitudeIndex].Magnitude / current) *
                                  (18.0 * relativeFreq * relativeFreq + 1.0);

            // Calculate impedance phase, with small correction factor accounting for the 3-command SPI pipeline delay.
            var period = _sampleRate / _frequency;
            impedance.Phase = measuredImpedance[bestAmplitudeIndex].Phase + 360.0 * (3.0 / period);

            // Factor out on-chip parasitic capacitance from impedance measurement.
            impedance = FactorOutParallelCapacitance(impedance, _frequency, parasiticCapacitance);

            //
            return impedance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freq"></param>
        /// <param name="channelNames"></param>
        /// <param name="impedances"></param>
        /// <param name="phases"></param>
        /// <returns></returns>
        public static string ForamtToCSV(double freq, List<string> channelNames, List<double> impedances, List<double> phases)
        {
            //
            var lines = new List<string>();

            //headLine
            lines.Add(string.Join(",", new List<string>
            {
                "Channel Number",
                "Channel Name",
                $"Impedance Magnitude at {freq} Hz (ohms)",
                $"Impedance Phase at {freq} Hz (degrees)",
                $"Series RC equivalent R (Ohms)",
                $"Series RC equivalent C (Farads)",
            }));

            //datas
            for (int i = 0; i < impedances.Count; i++)
            {
                var line = new List<string>
                {
                    $"{i+1}",
                    $"{channelNames[i]}",
                    $"{impedances[i]:F2}",
                    $"{phases[i]}",
                    $"{impedances[i]*Math.Cos(phases[i] * DegreesToRadians):F2}",
                    $"{impedances[i]*Math.Sin(phases[i] * DegreesToRadians):F2}",
                };
                lines.Add(string.Join(",", line));
            }

            //
            return string.Join(Environment.NewLine, lines);
        }

        #region 私有函数

        /// <summary>
        /// 生成单周期波形
        /// </summary>
        /// <param name="frequency"></param>
        /// <param name="sampleRate"></param>
        /// <param name="amplitude"></param>
        /// <returns></returns>
        private bool CreatePeriodZcheckDac(out byte[] periods)
        {
            periods = new byte[0];
            if (_amplitude < 0.0 || _amplitude > 128.0)
            {
                LogTool.Logger.LogE("amplitude out of range");
                return false;
            }
            if (_frequency < 0.0)
            {
                LogTool.Logger.LogE("negative frequency not allowed");
                return false;
            }
            if (_frequency > _sampleRate / 4.0)
            {
                LogTool.Logger.LogE("frequency too high relative to sample rate");
                return false;
            }

            //
            if (_frequency <= 0.0)
            {
                LogTool.Logger.LogE("frequency not positive");
                return false;
            }

            //
            if (_frequency <= 0.0)
            {
                LogTool.Logger.LogE("frequency not positive");
                return false;
            }

            //
            int period = (int)Math.Floor(_sampleRate / _frequency + 0.5);

            //
            double t = 0.0;
            periods = new byte[period];
            for (int i = 0; i < period; ++i)
            {
                int val = (int)Math.Floor(_amplitude * Math.Sin(TwoPi * _frequency * t) + 128.0 + 0.5);
                if (val < 0)
                {
                    val = 0;
                }
                else if (val > 255)
                {
                    val = 255;
                }
                periods[i] = (byte)val;
                t += 1.0 / _sampleRate;
            }

            //
            return true;
        }

        // Given a measured complex impedance that is the result of an electrode impedance in parallel
        // with a parasitic capacitance (i.e., due to the amplifier input capacitance and other
        // capacitances associated with the chip bondpads), this function factors out the effect of the
        // parasitic capacitance to return the acutal electrode impedance.
        private ComplexPolar FactorOutParallelCapacitance(ComplexPolar impedance, double frequency, double parasiticCapacitance)
        {
            // First, convert from polar coordinates to rectangular coordinates.
            double measuredR = impedance.Magnitude * Math.Cos(DegreesToRadians * impedance.Phase);
            double measuredX = impedance.Magnitude * Math.Sin(DegreesToRadians * impedance.Phase);

            double capTerm = TwoPi * frequency * parasiticCapacitance;
            double xTerm = capTerm * (measuredR * measuredR + measuredX * measuredX);
            double denominator = capTerm * xTerm + 2.0 * capTerm * measuredX + 1.0;
            double trueR = measuredR / denominator;
            double trueX = (measuredX + xTerm) / denominator;

            // Now, convert from rectangular coordinates back to polar coordinates.
            ComplexPolar result = new ComplexPolar();
            result.Magnitude = Math.Sqrt(trueR * trueR + trueX * trueX);
            result.Phase = RadiansToDegrees * Math.Atan2(trueX, trueR);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actualZFreq"></param>
        /// <param name="highCutoff"></param>
        /// <returns></returns>
        private double ApproximateSaturationVoltage(double actualZFreq, double highCutoff)
        {
            if (actualZFreq < 0.2 * highCutoff)
            {
                return 5000.0;
            }
            else
            {
                return 5000.0 * Math.Sqrt(1.0 / (1.0 + Math.Pow(3.3333 * actualZFreq / highCutoff, 4.0)));
            }
        }

        /// <summary>
        /// 计算单个电容下的阻抗幅值和相位
        /// </summary>
        /// <param name="waveform"></param>
        /// <param name="sampleRate"></param>
        /// <param name="frequency"></param>
        /// <param name="numPeriods"></param>
        private ComplexPolar MeasureComplexAmplitude(double[] waveform, bool applyNotch, double fNotch, double bandwidth, double sampleRate, double frequency, int numPeriods)
        {
            //
            if (applyNotch)
            {
                ApplyNotchFilter(waveform, fNotch, bandwidth, sampleRate);
            }

            //
            int period = (int)Math.Round(sampleRate / frequency);
            int startIndex = 0;
            int endIndex = startIndex + numPeriods * period - 1;

            // Move the measurement window to the end of the waveform to ignore start-up transient.
            while (endIndex < waveform.Length - period)
            {
                startIndex += period;
                endIndex += period;
            }

            return AmplitudeOfFreqComponent(waveform, startIndex, endIndex, sampleRate, frequency);
        }

        /// <summary>
        /// 陷波滤波
        /// </summary>
        /// <param name="waveform"></param>
        /// <param name="fNotch"></param>
        /// <param name="bandwidth"></param>
        /// <param name="sampleRate"></param>
        private void ApplyNotchFilter(double[] waveform, double fNotch, double bandwidth, double sampleRate)
        {
            var d = Math.Exp(-1.0 * Math.PI * bandwidth / sampleRate);
            var b = (1.0 + d * d) * Math.Cos(TwoPi * fNotch / sampleRate);
            var b0 = (1.0 + d * d) / 2.0;
            var b1 = -b;
            var b2 = b0;
            var a1 = b1;
            var a2 = d * d;
            int length = waveform.Length;

            //
            var prevPrevIn = waveform[0];
            var prevIn = waveform[1];
            for (int t = 2; t < length; ++t)
            {
                var input = waveform[t];
                waveform[t] = b0 * input + b1 * prevIn + b2 * prevPrevIn - a1 * waveform[t - 1] - a2 * waveform[t - 2]; // Direct Form 1 implementation
                prevPrevIn = prevIn;
                prevIn = input;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="waveform"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="sampleRate"></param>
        /// <param name="frequency"></param>
        private ComplexPolar AmplitudeOfFreqComponent(double[] waveform, int startIndex, int endIndex, double sampleRate, double frequency)
        {
            var K = TwoPi * frequency / sampleRate; // precalculate for speed

            // Perform correlation with sine and cosine waveforms.
            var meanI = 0.0;
            var meanQ = 0.0;
            for (int t = startIndex; t <= endIndex; ++t)
            {
                meanI += waveform[t] * Math.Cos(K * t);
                meanQ += waveform[t] * -1.0 * Math.Sin(K * t);
            }
            var length = (double)(endIndex - startIndex + 1);
            meanI /= length;
            meanQ /= length;

            //
            var realComponent = 2.0 * meanI;
            var imagComponent = 2.0 * meanQ;

            //
            ComplexPolar result = new ComplexPolar
            {
                Magnitude = Math.Sqrt(realComponent * realComponent + imagComponent * imagComponent),
                Phase = RadiansToDegrees * Math.Atan2(imagComponent, realComponent)
            };
            return result;
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ComplexPolar
    {
        public double Magnitude { get; set; } = 0;
        public double Phase { get; set; } = 0;
    };
}
