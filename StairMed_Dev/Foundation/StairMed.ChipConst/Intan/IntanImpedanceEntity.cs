﻿using System;

namespace StairMed.ChipConst.Intan
{
    /// <summary>
    /// 
    /// </summary>
    public class IntanImpedanceEntity
    {
        /// <summary>
        /// 
        /// </summary>
        private const double TwoPi = Math.PI * 2;
        private const double RadiansToDegrees = 180 / Math.PI; // 57.2957795132;
        private const double DegreesToRadians = Math.PI / 180; // 0.0174532925199;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="magnitude"></param>
        /// <param name="phase"></param>
        public IntanImpedanceEntity(double magnitude, double phase)
        {
            Magnitude = magnitude;
            Phase = phase;
            R = magnitude * Math.Cos(Phase * DegreesToRadians);
            C = magnitude * Math.Sin(Phase * DegreesToRadians);
        }

        /// <summary>
        /// 
        /// </summary>
        private double _magnitude = 0;
        public double Magnitude
        {
            get { return _magnitude; }
            set
            {
                if (_magnitude != value)
                {
                    _magnitude = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _phase = 0;
        public double Phase
        {
            get { return _phase; }
            set
            {
                if (_phase != value)
                {
                    _phase = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _r = 0;
        public double R
        {
            get { return _r; }
            set
            {
                if (_r != value)
                {
                    _r = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _c = 0;
        public double C
        {
            get { return _c; }
            set
            {
                if (_c != value)
                {
                    _c = value;
                }
            }
        }

    }
}
