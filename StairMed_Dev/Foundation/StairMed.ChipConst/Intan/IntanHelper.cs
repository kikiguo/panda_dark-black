﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace StairMed.ChipConst.Intan
{
    /// <summary>
    /// 
    /// </summary>
    public class IntanHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="adc"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Adc2Uv(ushort adc)
        {
            return IntanConst.CoefK * (adc + IntanConst.CoefB);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adc"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ushort Uv2Adc(double uv)
        {
            return (ushort)(uv / IntanConst.CoefK - IntanConst.CoefB);
        }

        /// <summary>
        /// 获取DSP cutoff设置值
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        /// <returns></returns>
        public static void GetDSPCutoffN(double cutoff, double sampleRate, out byte N)
        {
            //
            var cutoffs = new List<double>();
            for (int i = 0; i < IntanConst.DSPCutoffKFreqs.Count; i++)
            {
                cutoffs.Add(IntanConst.DSPCutoffKFreqs[i] * sampleRate);
            }

            //
            var sortedCutoffs = new List<double>(cutoffs);
            sortedCutoffs.Sort();

            //
            N = 0;
            if (cutoff <= sortedCutoffs[0])
            {
                N = 0;
            }
            else if (cutoff >= sortedCutoffs[^1])
            {
                N = (byte)(sortedCutoffs.Count - 1);
            }
            else
            {
                for (int i = 0; i < sortedCutoffs.Count - 1; i++)
                {
                    if (cutoff >= sortedCutoffs[i] && cutoff <= sortedCutoffs[i + 1])
                    {
                        if (sortedCutoffs[i + 1] - cutoff > cutoff - sortedCutoffs[i])
                        {
                            N = (byte)i;
                        }
                        else
                        {
                            N = (byte)(i + 1);
                        }
                        break;
                    }
                }
            }

            //
            N = (byte)cutoffs.IndexOf(sortedCutoffs[N]);

            //
            //for (int i = 0; i < cutoffs.Count; i++)
            //{
            //    Logger.LogTool.Logger.LogT($"[{i:N2}] = {cutoffs[i]} {(N != i ? $"" : $"++++++++++++++{cutoff}")}");
            //}
        }

        /// <summary>
        /// 2116 Register 34: Stimulation Current Step Size
        /// </summary>
        /// <param name="step"></param>
        /// <returns></returns>
        public static void Get2116Register34(int step, out ushort register34)
        {
            var tupleStep = (ValueTuple<int, int, int>)IntanConst.StimulationCurrentStepSizeDict[step];
            var sel1 = tupleStep.Item1;
            var sel2 = tupleStep.Item2;
            var sel3 = tupleStep.Item3;
            register34 = (ushort)(sel1 + (sel2 << 7) + (sel3 << 13));
        }

        /// <summary>
        /// 2116 Register 35: Stimulation Bias Voltages
        /// </summary>
        /// <param name="step"></param>
        /// <returns></returns>
        public static void Get2116Register35(int step, out ushort register35)
        {
            var tupleBias = (ValueTuple<int, int>)IntanConst.StimulationBiasVoltagesDict[step];
            var pbias = tupleBias.Item1;
            var nbias = tupleBias.Item2;
            register35 = (ushort)(nbias + (pbias << 4));
        }

        /// <summary>
        /// 2164: Registers 8-13: On-Chip Amplifier Bandwidth Select
        /// </summary>
        /// <param name="hz"></param>
        /// <returns></returns>
        public static void Get2164Register_12_13(double hz, out byte register12, out byte register13)
        {
            var tuple = (ValueTuple<int, int, int>)IntanConst.AmplifierLowerBandwidthDict[hz];
            var RL_DAC1 = (byte)tuple.Item1;
            var RL_DAC2 = (byte)tuple.Item2;
            var RL_DAC3 = (byte)tuple.Item3;

            //
            register12 = RL_DAC1;
            register13 = Convert.ToByte((RL_DAC3 << 6) + RL_DAC2);
        }

        /// <summary>
        /// 2164: Registers 8-13: On-Chip Amplifier Bandwidth Select
        /// </summary>
        /// <param name="hz"></param>
        /// <returns></returns>
        public static void Get2164Register_8_9_10_11(double hz, out byte register8, out byte register9, out byte register10, out byte register11)
        {
            var tuple = (ValueTuple<int, int, int, int>)IntanConst.AmplifierUpperBandwidthDict[hz];
            var RH1_DAC1 = (byte)tuple.Item1;
            var RH1_DAC2 = (byte)tuple.Item2;
            var RH2_DAC1 = (byte)tuple.Item3;
            var RH2_DAC2 = (byte)tuple.Item4;

            register8 = RH1_DAC1;
            register9 = RH1_DAC2;
            register10 = RH2_DAC1;
            register11 = RH2_DAC2;
        }

        /// <summary>
        /// 2116: Registers 4-7: On-Chip Amplifier Bandwidth Select
        /// </summary>
        /// <param name="hz"></param>
        /// <returns></returns>
        public static void Get2116Register_4_5(double hz, out ushort register4, out ushort register5)
        {
            var tuple = (ValueTuple<int, int, int, int>)IntanConst.AmplifierUpperBandwidthDict[hz];
            var RH1_SEL1 = (byte)tuple.Item1;
            var RH1_SEL2 = (byte)tuple.Item2;
            var RH2_SEL1 = (byte)tuple.Item3;
            var RH2_SEL2 = (byte)tuple.Item4;

            //
            register4 = (ushort)((RH1_SEL2 << 6) + (RH1_SEL1));
            register5 = (ushort)((RH2_SEL2 << 6) + (RH2_SEL1));
        }

        /// <summary>
        /// 2116: Registers 4-7: On-Chip Amplifier Bandwidth Select
        /// </summary>
        /// <param name="hz"></param>
        /// <returns></returns>
        public static void Get2116Register_6_7(double hz, out ushort register6, out ushort register7)
        {
            var tuple = (ValueTuple<int, int, int>)IntanConst.AmplifierLowerBandwidthDict[hz];
            var RL_SEL1 = (byte)tuple.Item1;
            var RL_SEL2 = (byte)tuple.Item2;
            var RL_SEL3 = (byte)tuple.Item3;
            register6 = (ushort)((RL_SEL3 << 13) + (RL_SEL2 << 7) + (RL_SEL1));
            register7 = register6;
        }
    }
}
