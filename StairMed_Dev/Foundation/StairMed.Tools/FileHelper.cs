﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace StairMed.Tools
{
    public static class FileHelper
    {
        /// <summary>
        /// 加载文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string LoadFile(string fileName, Encoding? encoding = null)
        {
            try
            {
                if (encoding == null)
                {
                    encoding = Encoding.UTF8;
                }
                var builder = new StringBuilder();
                using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(fileStream, encoding))
                    {
                        var line = reader.ReadLine();
                        while (line != null)
                        {
                            builder.AppendLine(line);
                            line = reader.ReadLine();
                        }
                        reader.Close();
                    }
                    fileStream.Close();
                }
                return builder.ToString();
            }
            catch { }
            return string.Empty;
        }

        /// <summary>
        /// 加载文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static List<string> LoadFileLines(string fileName)
        {
            var encoding = Encoding.UTF8;
            var lines = new List<string>();

            //
            try
            {
                var builder = new StringBuilder();
                using (var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(fileStream, encoding))
                    {
                        var line = reader.ReadLine();
                        while (line != null)
                        {
                            lines.Add(line.Trim());
                            line = reader.ReadLine();
                        }
                        reader.Close();
                    }
                    fileStream.Close();
                }
            }
            catch { }

            //
            return lines;
        }


        /// <summary>
        /// 保存至文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        public static void SaveToFileWithBom(string fileName, string content)
        {
            var encoding = Encoding.UTF8;
            fileName = Path.GetFullPath(fileName);
            try
            {
                var dir = Path.GetDirectoryName(fileName);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
            catch { }

            try
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
            catch { }

            try
            {
                using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    using (var writer = new StreamWriter(fileStream, encoding))
                    {
                        writer.Write(content);
                        writer.Flush();
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 保存至文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        /// <param name="encoding"></param>
        public static bool SaveToFileNoBom(string fileName, string content, out string errMsg)
        {
            var encoding = Encoding.UTF8;
            errMsg = string.Empty;
            fileName = Path.GetFullPath(fileName);

            //
            try
            {
                var dir = Path.GetDirectoryName(fileName);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }

            //
            try
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }

            //
            try
            {
                var bytes = encoding.GetBytes(content);
                using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fileStream.Write(bytes, 0, bytes.Length);
                }
                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
        }


        /// <summary>
        /// 将控件保存为图片
        /// </summary>
        /// <param name="control"></param>
        /// <param name="encoder"></param>
        /// <param name="savePath"></param>
        public static void SnapshotUIControl(FrameworkElement control, BitmapEncoder encoder, string savePath)
        {
            try
            {
                //
                var drawingVisual = new DrawingVisual();
                using (var context = drawingVisual.RenderOpen())
                {
                    var brush = new VisualBrush(control)
                    {
                        Stretch = Stretch.None
                    };
                    context.DrawRectangle(brush, null, new Rect(0, 0, control.ActualWidth, control.ActualHeight));
                    context.Close();
                }

                //
                var targetBitmap = new RenderTargetBitmap((int)control.ActualWidth, (int)control.ActualHeight, 96d, 96d, PixelFormats.Default);
                targetBitmap.Render(drawingVisual);

                //
                encoder.Frames.Add(BitmapFrame.Create(targetBitmap));
                using (var fs = System.IO.File.Open(savePath, System.IO.FileMode.OpenOrCreate))
                {
                    encoder.Save(fs);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{nameof(SnapshotUIControl)}: {ex.Message}");
            }
        }
    }
}
