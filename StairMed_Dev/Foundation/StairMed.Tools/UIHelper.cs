using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;

namespace StairMed.Tools
{
    public static class UIHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static bool Skip { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="priority"></param>
        public static void Invoke(Action callback, DispatcherPriority priority = DispatcherPriority.Normal)
        {
            if (Skip)
            {
                return;
            }

            //
            try
            {
                Application.Current.Dispatcher.Invoke(callback, priority);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{nameof(Invoke)}: {ex.Message}");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="priority"></param>
        public static void BeginInvoke(Action callback, DispatcherPriority priority = DispatcherPriority.Normal)
        {
            if (Skip)
            {
                return;
            }

            //
            try
            {
                Application.Current.Dispatcher.BeginInvoke(callback, priority);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{nameof(BeginInvoke)}: {ex.Message}");
            }
        }
    }
}
