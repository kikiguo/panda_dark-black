﻿using System;
using System.Windows.Media;

namespace StairMed.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public static class RGBHSB
    {
        //
        public const int HSBColorRingStep = 55;

        /// <summary>
        /// 色相，饱和度，亮度转换成rgb值
        /// </summary>
        /// <returns></returns>
        public static float[] HSB2RGB(float[] hsb)
        {
            if (hsb[0] == 360)
            {
                hsb[0] = 0;
            }
            float[] rgb = new float[3];
            float r = 0;
            float g = 0;
            float b = 0;

            if (hsb[1] == 0)
            {
                r = g = b = hsb[2];
            }
            else
            {
                float sectorPos = hsb[0] / 60f;
                int sectorNum = (int)Math.Floor(sectorPos);
                float fractionalSector = sectorPos - sectorNum;
                float p = hsb[2] * (1 - hsb[1]);
                float q = hsb[2] * (1 - (hsb[1] * fractionalSector));
                float t = hsb[2] * (1 - (hsb[1] * (1 - fractionalSector)));
                switch (sectorNum)
                {
                    case 0:
                        r = hsb[2];
                        g = t;
                        b = p;
                        break;
                    case 1:
                        r = q;
                        g = hsb[2];
                        b = p;
                        break;
                    case 2:
                        r = p;
                        g = hsb[2];
                        b = t;
                        break;
                    case 3:
                        r = p;
                        g = q;
                        b = hsb[2];
                        break;
                    case 4:
                        r = t;
                        g = p;
                        b = hsb[2];
                        break;
                    case 5:
                        r = hsb[2];
                        g = p;
                        b = q;
                        break;
                }

            }
            return new float[] { r * 255, g * 255, b * 255 };
        }

        /// <summary>
        /// 将rgb类型的颜色转换为hsb
        /// </summary>
        /// <param name="rgb"></param>
        /// <returns></returns>
        public static float[] RGB2HSB(float[] rgb)
        {
            float[] hsb = new float[3];
            float r = rgb[0];
            float g = rgb[1];
            float b = rgb[2];

            float max = Math.Max(r, Math.Max(g, b));
            if (max <= 0)
            {
                return hsb;
            }
            float min = Math.Min(r, Math.Min(g, b));
            float dif = max - min;
            if (max > min)
            {
                if (g == max)
                {
                    hsb[0] = (b - r) / dif * 60f + 120f;
                }
                else if (b == max)
                {
                    hsb[0] = (r - g) / dif * 60f + 240f;
                }
                else if (b > g)
                {
                    hsb[0] = (g - b) / dif * 60f + 360f;
                }
                else
                {
                    hsb[0] = (g - b) / dif * 60f;
                }
                if (hsb[0] < 0)
                {
                    hsb[0] = hsb[0] + 360f;
                }
            }
            else
            {
                hsb[0] = 0;
            }
            hsb[1] = dif / max;
            hsb[2] = max / 255f;
            return hsb;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static Color GetColor(int index)
        {
            var rgbs = HSB2RGB(new float[] { index * HSBColorRingStep % 360, 1, 1 });
            return Color.FromRgb(Convert.ToByte(rgbs[0]), Convert.ToByte(rgbs[1]), Convert.ToByte(rgbs[2]));
        }

    }
}
