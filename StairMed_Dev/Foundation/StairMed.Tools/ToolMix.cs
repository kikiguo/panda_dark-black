﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace StairMed.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public static class ToolMix
    {
        /// <summary>
        /// 坐标轴均分:如Y轴范围0-1
        /// </summary>
        /// <param name="val"></param>
        /// <param name="step"></param>
        /// <param name="power"></param>
        /// <param name="n"></param>
        public static void CoordinateAxisEqualDivide(double val, out float step, out int power, out int n, int minN = 4)
        {
            if (val.Equals(double.NaN) || val.Equals(double.NegativeInfinity) || val.Equals(double.PositiveInfinity) || val <= 0)
            {
                val = 0.5;
            }

            //
            power = 0;
            val = val * 1.05;
            var pow = (int)Math.Floor(Math.Log10(Math.Abs(val) / 10));

            //
            var unit = (float)Math.Pow(10, pow);
            var multis = new List<int> { 80, 70, 60, 50, 40, 30, 20, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            //
            step = unit;
            foreach (var multi in multis)
            {
                step = unit * multi;
                if (val / step > minN)
                {
                    break;
                }
            }

            //
            power = (int)Math.Floor(Math.Log10(step));
            n = (int)Math.Ceiling(val / step);
        }

        /// <summary>
        /// 是否含有汉字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool HasChinese(string str)
        {
            return Regex.IsMatch(str, @"[\u4e00-\u9fa5]");
        }

        /// <summary>
        /// 使用md5进行加密
        /// </summary>
        /// <param name="words"></param>
        /// <returns></returns>
        public static string MD5Encrypt(string words)
        {
            string pwd = "";

            MD5 md5 = MD5.Create();
            byte[] bytes = md5.ComputeHash(Encoding.UTF8.GetBytes(words));

            for (int i = 0; i < bytes.Length; i++)
            {
                pwd += bytes[i].ToString("X2");
            }
            return pwd;
        }

        /// <summary>
        /// 将控件提取图片
        /// </summary>
        /// <param name="control"></param>
        public static BitmapSource SnapshotUIControl(FrameworkElement control)
        {
            try
            {
                //
                var drawingVisual = new DrawingVisual();
                using (var context = drawingVisual.RenderOpen())
                {
                    var brush = new VisualBrush(control)
                    {
                        Stretch = Stretch.None
                    };
                    context.DrawRectangle(brush, null, new Rect(0, 0, control.ActualWidth, control.ActualHeight));
                    context.Close();
                }

                //
                var targetBitmap = new RenderTargetBitmap((int)control.ActualWidth, (int)control.ActualHeight, 96d, 96d, PixelFormats.Default);
                targetBitmap.Render(drawingVisual);

                //
                return BitmapFrame.Create(targetBitmap);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"{nameof(SnapshotUIControl)}: {ex.Message}");
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<string> GetLocalIp()
        {
            //获取本地的IP地址
            var list = new List<string>();

            //
            var nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var adapter in nics)
            {
                //
                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet || adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                {
                    var ipAddrs = adapter.GetIPProperties().UnicastAddresses;
                    foreach (var ipAddr in ipAddrs)
                    {
                        //
                        if (ipAddr.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            list.Add(ipAddr.Address.ToString());
                        }
                    }
                }
            }

            //
            return list;
        }
    }
}
