﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace StairMed.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public class ColorScale
    {
        /// <summary>
        /// 
        /// </summary>
        private const int ColorMapSize = 256;

        //
        private float _minValue;
        private float _maxValue;
        private float _valueRange;
        private List<Color> _colorMap;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="minValue_"></param>
        /// <param name="maxValue_"></param>
        public ColorScale(float minValue_ = 0.0f, float maxValue_ = 1.0f)
        {
            _minValue = minValue_;
            _maxValue = maxValue_;
            _valueRange = _maxValue - _minValue;
            _colorMap = new List<Color>(ColorMapSize);
            CalculateColorMap();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Color> GetColorMap()
        {
            return _colorMap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minValue_"></param>
        /// <param name="maxValue_"></param>
        public void SetRange(float minValue_, float maxValue_)
        {
            _minValue = minValue_;
            _maxValue = maxValue_;
            _valueRange = _maxValue - _minValue;
            CalculateColorMap();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Color GetColor(double value)
        {
            int index = Convert.ToInt32(Math.Round((ColorMapSize - 1) * (value - _minValue) / _valueRange));
            if (index < 0)
            {
                index = 0;
            }
            else if (index >= ColorMapSize)
            {
                index = ColorMapSize - 1;
            }
            return _colorMap[index];
        }



        /// <summary>
        /// 
        /// </summary>
        private void CalculateColorMap()
        {
            int c1 = Convert.ToInt32(Math.Round(ColorMapSize * 0.20));
            int c2 = Convert.ToInt32(Math.Round(ColorMapSize * 0.75));
            double iNorm, hue, value, saturation;

            // Dark violet fading to bright blue
            for (int i = 0; i < c1; ++i)
            {
                iNorm = (double)i / (double)c1;
                hue = 290.0 - 40.0 * iNorm;
                saturation = 255.0 / 256;
                value = 255.0 * (0.7 * iNorm + 0.3) / 256;
                HsvToRgb(hue, saturation, value, out int r, out int g, out int b);
                _colorMap.Add(Color.FromRgb((byte)r, (byte)g, (byte)b));
            }
            // Hue sweeping from blue through green, yellow, orange, to red
            for (int i = c1; i < c2; ++i)
            {
                iNorm = (double)(i - c1) / (double)(c2 - c1 + 1);
                hue = 250.0 - 255.0 * iNorm;
                saturation = 255.0 / 256;
                value = 255.0 / 256;
                HsvToRgb(hue, saturation, value, out int r, out int g, out int b);
                _colorMap.Add(Color.FromRgb((byte)r, (byte)g, (byte)b));
            }
            // Red fading into white
            for (int i = c2; i < ColorMapSize; ++i)
            {
                iNorm = (double)(i - c2) / (double)(ColorMapSize - c2 - 1);
                saturation = 255.0 * (1.0 - iNorm) / 256;
                value = 255.0 / 256;
                hue = 355.0;
                HsvToRgb(hue, saturation, value, out int r, out int g, out int b);
                _colorMap.Add(Color.FromRgb((byte)r, (byte)g, (byte)b));
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="h"></param>
        /// <param name="S"></param>
        /// <param name="V"></param>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        void HsvToRgb(double h, double S, double V, out int r, out int g, out int b)
        {
            // ######################################################################
            // T. Nathan Mundhenk
            // mundhenk@usc.edu
            // C/C++ Macro HSV to RGB

            double H = h;
            while (H < 0) { H += 360; };
            while (H >= 360) { H -= 360; };
            double R, G, B;
            if (V <= 0)
            { R = G = B = 0; }
            else if (S <= 0)
            {
                R = G = B = V;
            }
            else
            {
                double hf = H / 60.0;
                int i = (int)Math.Floor(hf);
                double f = hf - i;
                double pv = V * (1 - S);
                double qv = V * (1 - S * f);
                double tv = V * (1 - S * (1 - f));
                switch (i)
                {

                    // Red is the dominant color

                    case 0:
                        R = V;
                        G = tv;
                        B = pv;
                        break;

                    // Green is the dominant color

                    case 1:
                        R = qv;
                        G = V;
                        B = pv;
                        break;
                    case 2:
                        R = pv;
                        G = V;
                        B = tv;
                        break;

                    // Blue is the dominant color

                    case 3:
                        R = pv;
                        G = qv;
                        B = V;
                        break;
                    case 4:
                        R = tv;
                        G = pv;
                        B = V;
                        break;

                    // Red is the dominant color

                    case 5:
                        R = V;
                        G = pv;
                        B = qv;
                        break;

                    // Just in case we overshoot on our math by a little, we put these here. Since its a switch it won't slow us down at all to put these here.

                    case 6:
                        R = V;
                        G = tv;
                        B = pv;
                        break;
                    case -1:
                        R = V;
                        G = pv;
                        B = qv;
                        break;

                    // The color is not defined, we should throw an error.

                    default:
                        //LFATAL("i Value error in Pixel conversion, Value is %d", i);
                        R = G = B = V; // Just pretend its black/white
                        break;
                }
            }
            r = Clamp((int)(R * 255.0));
            g = Clamp((int)(G * 255.0));
            b = Clamp((int)(B * 255.0));
        }

        /// <summary>
        /// Clamp a value to 0-255
        /// </summary>
        int Clamp(int i)
        {
            if (i < 0) return 0;
            if (i > 255) return 255;
            return i;
        }

    }
}
