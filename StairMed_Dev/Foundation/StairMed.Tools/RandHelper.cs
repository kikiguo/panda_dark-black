﻿using System;

namespace StairMed.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public static class RandHelper
    {
        private static readonly Random Random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static int Next(int maxValue = int.MaxValue, int minValue = 0)
        {
            return Random.Next(minValue, maxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static double NextDouble()
        {
            return Random.NextDouble();
        }
    }
}
