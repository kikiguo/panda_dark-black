﻿using System;

namespace StairMed.Tools
{
    public enum WindowFunction
    {
        WindowRectangular,
        WindowTriangular,
        WindowHann,
        WindowHamming
    };

    public class FastFourierTransform
    {
        public static readonly float TwoPi = (float)(Math.PI * 2);
        public static readonly float Pi = (float)(Math.PI);

        public int SampleRate;
        public uint Length;
        private WindowFunction function;

        private float[] window;
        private float[] logPsd;
        private float[] frequency;

        public FastFourierTransform(int sampleRate_, int length_ = 1024, WindowFunction function_ = WindowFunction.WindowHamming)
        {
            SampleRate = sampleRate_;
            Length = (uint)length_;
            function = function_;

            //
            setLength(Length);
        }


        private void setLength(uint length_)
        {
            Length = length_;
            createWindow();
            createPsdVector();
            createFrequencyVector();
        }


        // Perform an FFT of an array of n complex numbers, where n must be a power of two.
        // The complex numbers are stored in data, an array of length 2n, where
        // data[0] = input_real[t]
        // data[1] = input_imaginary[t]
        // data[2] = input_real[t+1]
        // data[3] = input_imaginary[t+1]
        // etc...
        // The complex FFT is returned in the same format, overwriting data.
        public static void complexInputFft(float[] data, uint n)
        {
            // Reverse-binary reindexing
            uint m;
            uint nTimes2 = n << 1;
            uint j = 1;
            for (uint i = 1; i < nTimes2; i += 2)
            {
                if (j > i)
                {
                    (data[i - 1], data[j - 1]) = (data[j - 1], data[i - 1]);
                    (data[i], data[j]) = (data[j], data[i]);
                }
                m = n;
                while (m >= 2 && j > m)
                {
                    j -= m;
                    m >>= 1;
                }
                j += m;
            }

            // Danielson-Lanczos section
            double wTemp, wReal, wpReal, wImag, wpImag, theta;
            float tempReal, tempImag;
            uint iStep;
            uint mMax = 2;
            while (mMax < nTimes2)
            {
                iStep = mMax << 1;
                theta = -TwoPi / (double)mMax;     // we flip sign here to match MATLAB fft()
                wTemp = Math.Sin(0.5 * theta);
                wpReal = -2.0 * wTemp * wTemp;
                wpImag = Math.Sin(theta);
                wReal = 1.0;
                wImag = 0.0;
                for (m = 1; m < mMax; m += 2)
                {
                    for (uint i = m; i <= nTimes2; i += iStep)
                    {
                        j = i + mMax;
                        tempReal = (float)(wReal * (double)data[j - 1] - wImag * (double)data[j]);
                        tempImag = (float)(wReal * (double)data[j] + wImag * (double)data[j - 1]);
                        data[j - 1] = data[i - 1] - tempReal;
                        data[j] = data[i] - tempImag;
                        data[i - 1] += tempReal;
                        data[i] += tempImag;
                    }
                    wTemp = wReal;
                    wReal += wReal * wpReal - wImag * wpImag;
                    wImag += wImag * wpReal + wTemp * wpImag;
                }
                mMax = iStep;
            }
        }






        // Perform an FFT of an array of n real numbers, where n must be a power of two.
        // The real numbers are stored in data, an array of length n, and the complex FFT
        // (for positive frequencies only) is overwritten on the same array in the following
        // format:
        // data[0] = real component of frequency 0
        // data[1] = real component of frequency n/2
        // data[2] = real component of frequency 1
        // data[3] = imaginary component of frequency 1
        // data[4] = real component of frequency 2
        // data[5] = imaginary component of frequency 2
        // etc...
        // Note the special use of data[1] to return the real component of the maximum
        // frequency n/2.  The imaginary component of frequency 0 and n/2 are always zero
        // for real-valued inputs.
        public static void realInputFft(float[] data, uint n)
        {
            complexInputFft(data, n >> 1);

            double theta = -Pi / (double)(n >> 1);      // we flip sign here to match MATLAB fft()
            double wTemp = Math.Sin(0.5 * theta);
            double wpReal = -2.0 * wTemp * wTemp;
            double wpImag = Math.Sin(theta);
            double wReal = 1.0 + wpReal;
            double wImag = wpImag;
            uint nPlus1 = n + 1;
            uint i1, i2, i3, i4;
            float h1Real, h1Imag, h2Real, h2Imag;
            for (uint i = 2; i <= (n >> 2); ++i)
            {
                i1 = (i << 1) - 2;
                i2 = i1 + 1;
                i3 = nPlus1 - i2;
                i4 = i3 + 1;
                h1Real = 0.5F * (data[i1] + data[i3]);
                h1Imag = 0.5F * (data[i2] - data[i4]);
                h2Real = 0.5F * (data[i2] + data[i4]);
                h2Imag = 0.5F * (data[i3] - data[i1]);
                data[i1] = (float)(h1Real + wReal * h2Real - wImag * h2Imag);
                data[i2] = (float)(h1Imag + wReal * h2Imag + wImag * h2Real);
                data[i3] = (float)(h1Real - wReal * h2Real + wImag * h2Imag);
                data[i4] = (float)(-h1Imag + wReal * h2Imag + wImag * h2Real);
                wTemp = wReal;
                wReal += wReal * wpReal - wImag * wpImag;
                wImag += wImag * wpReal + wTemp * wpImag;
            }
            data[(n >> 1) + 1] *= -1.0F;    // we flip this imaginary value sign to match MATLAB fft()

            // Convey first real frequency value in data[0] and last real frequency value in data[1].
            // These have no imaginary components.
            h1Real = data[0];
            data[0] += data[1];
            data[1] = h1Real - data[1];
        }


        // Calculate the logarithm of the square root of the PSD of data and normalizes values to facilitate calculation
        // of signal amplitude from PSD.  The values in data are overwritten with intermediate results.
        // Returns a pointer to the results, an array (length/2 + 1) long.
        public float[] logSqrtPowerSpectralDensity(float[] data)
        {
            // Apply window.
            for (int k = 0; k < Length; ++k)
            {
                data[k] *= window[k];
            }

            // Calculate FFT.
            realInputFft(data, Length);

            float normalizationFactor = (float)Math.Log10(2.0F / (float)Length); // add this to facilitate estimate of narrowband signal amplitude
                                                                                 // from PSD.
            const float windowCorrectionFactor = 0.267789F; // empirical correction factor; only valid for Hamming window!
            normalizationFactor += windowCorrectionFactor;

            float epsilon = float.Epsilon;   // add tiny number to PSD results before
                                             // calculating log to avoid log(0) = -inf.
            logPsd[0] = (float)(0.5F * Math.Log10(0.25F * data[0] * data[0] + epsilon) + normalizationFactor);    // no imaginary component here
            uint i = 1;
            uint j = 2;
            for (; i < (Length >> 1); ++i)
            {
                // PSD = |FFT|^2 = real^2 + imaginary^2
                // Then take the square root (moved outside the logarithm as a factor of 1/2) to go from uV^2/Hz to uV/sqrt(Hz).
                // Then take logarithm to compress wide dynamic range for viewing.  And add normalization factor to normalize to
                // the number of samples in the FFT and to compensate for weighting of FFT window function.
                logPsd[i] = (float)(0.5F * Math.Log10(data[j] * data[j] + data[j + 1] * data[j + 1] + epsilon) + normalizationFactor);
                j += 2;
            }
            logPsd[i] = (float)(0.5F * Math.Log10(0.25F * data[1] * data[1] + epsilon) + normalizationFactor);    // no imaginary component here
            return logPsd;
        }




        // Return frequency for an index ranging from zero to (length/2).
        private float getFrequency(int index)
        {
            if (index > (int)(Length >> 1))
            {
                return -1.0F;
            }
            else
            {
                return frequency[index];
            }
        }




        private void createWindow()
        {
            window = new float[Length];

            uint i, j;
            float nDiv2 = (float)(Length >> 1);
            float nMinus1DivTwoPi = (Length - 1) / TwoPi;
            float value;

            switch (function)
            {
                case WindowFunction.WindowRectangular:
                    {
                        for (i = 0; i < Length; ++i)
                        {
                            window[i] = 1.0F;
                        }
                    }
                    break;
                case WindowFunction.WindowTriangular:
                    {
                        for (i = 0, j = Length - 1; i < (Length >> 1);)
                        {
                            value = (float)(i + 1) / nDiv2;
                            window[i++] = value;
                            window[j--] = value;
                        }
                    }
                    break;
                case WindowFunction.WindowHann:
                    {
                        for (i = 0; i < Length; ++i)
                        {
                            window[i] = (float)(0.5F * (1.0F - Math.Cos((float)i / nMinus1DivTwoPi)));
                        }
                    }
                    break;
                case WindowFunction.WindowHamming:
                    {
                        for (i = 0; i < Length; ++i)
                        {
                            window[i] = (float)(0.54F - 0.46F * Math.Cos((float)i / nMinus1DivTwoPi));
                        }
                    }
                    break;
                default:
                    break;
            }
        }


        private void createPsdVector()
        {
            logPsd = new float[(Length >> 1) + 1];
        }


        private void createFrequencyVector()
        {
            uint n = (Length >> 1) + 1;
            frequency = new float[n];

            float deltaF = SampleRate / (float)Length;
            float f = 0.0F;
            for (uint i = 0; i < n; ++i)
            {
                frequency[i] = f;
                f += deltaF;
            }
        }
    };
}
