﻿using System.Security.Cryptography;

namespace StairMed.Tools
{
    /// <summary>
    /// 
    /// </summary>
    public static class AES
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plaintext"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static byte[] AESCBCEncrypt(byte[] plaintext, byte[] key, byte[] iv)
        {
            using (var aes = new RijndaelManaged())
            {
                aes.Padding =  PaddingMode.Zeros;
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.IV = iv;

                //
                using (var encryptor = aes.CreateEncryptor())
                {
                    return encryptor.TransformFinalBlock(plaintext, 0, plaintext.Length);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ciphertext"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static byte[] AESCBCDecrypt(byte[] ciphertext, byte[] key, byte[] iv)
        {
            using (var aes = new RijndaelManaged())
            {
                aes.Padding = PaddingMode.Zeros;
                aes.Mode = CipherMode.CBC;
                aes.Key = key;
                aes.IV = iv;

                //
                using (var decryptor = aes.CreateDecryptor())
                {
                    return decryptor.TransformFinalBlock(ciphertext, 0, ciphertext.Length);
                }
            }
        }
    }
}
