﻿using StairMed.AsyncFIFO.Logger;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StairMed.AsyncFIFO
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseFIFO : IAsyncFIFO
    {
        /// <summary>
        /// 
        /// </summary>
        private bool _running = true;

        /// <summary>
        /// 
        /// </summary>
        protected const int MaxBuffer = 200;

        /// <summary>
        /// 处理函数
        /// </summary>
        protected Action<object[]> _consumerAction;

        /// <summary>
        /// 名称描述
        /// </summary>
        protected string _name = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            set
            {
                _name = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Action<object[]> ConsumerAction
        {
            set
            {
                _consumerAction = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BaseFIFO()
        {
            Task.Factory.StartNew(ConsumingLoop, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// 
        /// </summary>
        ~BaseFIFO()
        {
            _running = false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        public abstract void Add(params object[] objs);

        /// <summary>
        /// 
        /// </summary>
        protected abstract void Consume();

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _running = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ConsumingLoop()
        {
            while (_running)
            {
#if DEBUG
                Consume();
#else
                try
                {
                    Consume();
                }
                catch (Exception ex)
                {
                    Thread.Sleep(30);
                    LogTool.Logger.Exp($"{this.GetType()}, Consume", ex);
                }
#endif


            }
        }

    }
}
