﻿using StairMed.AsyncFIFO.Logger;
using System.Collections.Concurrent;

namespace StairMed.AsyncFIFO
{
    /// <summary>
    /// 生产者-消费者 模式
    /// </summary>
    public class BlockFIFO : BaseFIFO
    {
        /// <summary>
        /// 
        /// </summary>
        public const string TAG = nameof(BlockFIFO);

        /// <summary>
        /// 线程安全队列
        /// </summary>
        private readonly BlockingCollection<object[]> _blockingQueue = new BlockingCollection<object[]>(short.MaxValue);


        /// <summary>
        /// 生产
        /// </summary>
        /// <param name="objs"></param>
        public override void Add(params object[] objs)
        {
            _blockingQueue.Add(objs);
            if (_blockingQueue.Count > MaxBuffer)
            {
                LogTool.Logger.LogW($"{TAG} - {_name} - {_blockingQueue.Count}");
            }
        }

        /// <summary>
        /// 消费
        /// </summary>
        protected override void Consume()
        {
            var item = _blockingQueue.Take();
            _consumerAction.Invoke(item);
        }

    }
}
