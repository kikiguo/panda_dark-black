﻿using StairMed.AsyncFIFO.Logger;
using System.Collections.Generic;
using System.Threading;

namespace StairMed.AsyncFIFO
{
    /// <summary>
    /// 生产者-消费者 模式
    /// </summary>
    public class LockFIFO : BaseFIFO
    {
        /// <summary>
        /// 
        /// </summary>
        public const string TAG = nameof(LockFIFO);

        /// <summary>
        /// 
        /// </summary>
        private readonly List<object[]> _batchList = new List<object[]>();
        private readonly object _batchMutex = new object();


        /// <summary>
        /// 生产
        /// </summary>
        /// <param name="objs"></param>
        public override void Add(params object[] objs)
        {
            Monitor.Enter(_batchMutex);
            {
                _batchList.Add(objs);
                Monitor.Pulse(_batchMutex);
            }
            Monitor.Exit(_batchMutex);

            //
            if (_batchList.Count > MaxBuffer)
            {
                LogTool.Logger.LogW($"{TAG} - {_name} - {_batchList.Count}");
            }
        }


        /// <summary>
        /// 消费
        /// </summary>
        protected override void Consume()
        {
            var items = new List<object[]>();

            //
            Monitor.Enter(_batchMutex);
            {
                if (_batchList.Count == 0)
                {
                    Monitor.Wait(_batchMutex, 5000);
                }
                else
                {
                    items.AddRange(_batchList);
                    _batchList.Clear();
                }
            }
            Monitor.Exit(_batchMutex);

            //
            if (items.Count <= 0)
            {
                return;
            }

            //
            for (int i = 0; i < items.Count; i++)
            {
                _consumerAction?.Invoke(items[i]);
            }
        }

    }
}
