﻿using System;

namespace StairMed.AsyncFIFO
{
    /// <summary>
    /// 生产者-消费者 模式
    /// </summary>
    public interface IAsyncFIFO : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        void Add(params object[] objs);

        /// <summary>
        /// 
        /// </summary>
        string Name { set; }

        /// <summary>
        /// 
        /// </summary>
        Action<object[]> ConsumerAction { set; }
    }
}
