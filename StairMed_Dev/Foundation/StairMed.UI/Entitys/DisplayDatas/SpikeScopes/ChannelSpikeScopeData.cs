﻿using HDF5CSharp;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers;
using StairMed.MVVM.Base;
using System;

namespace StairMed.UI.Entitys.DisplayDatas.SpikeScopes
{
    /// <summary>
    /// 
    /// </summary>
    public class ChannelSpikeScopeData : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicsIndex"></param>
        public ChannelSpikeScopeData(long deviceId, int physicsIndex)
        {
            DeviceId = deviceId;
            PhysicsIndex = physicsIndex;
            InputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
        }
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="physicsIndex"></param>
        public void Update(long deviceId, int physicsIndex)
        {
            DeviceId = deviceId;
            PhysicsIndex = physicsIndex;
            InputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
            TimeScaleMS = Settings.SpikeScopeTimeMS;

        }

        /// <summary>
        /// 
        /// </summary>
        private long _deviceId = 0;
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }


        private bool _enable= false;
        public bool Enable
        {
            get { return _enable; }
            set { Set(ref _enable, value); }
        }


        private int _timeScaleMS = 2;
        public  int TimeScaleMS
        {
            get { return _timeScaleMS; }
            set { Set(ref _timeScaleMS, value); }
        }


        private double _thresholdUV = -70;
        public double ThresholdUV
        {
            get { return _thresholdUV; }
            set
            {
                Set(ref _thresholdUV, value);
            }
        }

        /// <summary>
        /// spike scope图:保留多少个spike,25
        /// </summary>
        /// private int _spikeScopeReserved = 25;
        private int _spikeScopeReserved = 10;
        public int SpikeScopeReserved
        {
            get { return _spikeScopeReserved; }
            set { Set(ref _spikeScopeReserved, Math.Max(1, value)); }
        }

        /// <summary>
        /// spike scope图:电压范围
        /// </summary>
        private int _spikeScopeVoltageRange = 400;
        public int SpikeScopeVoltageRange
        {
            get { return _spikeScopeVoltageRange; }
            set { Set(ref _spikeScopeVoltageRange, value); }
        }


        /// <summary>
        /// spike scope图:电压范围
        /// </summary>
        private double _gain = 1;
        public double Gain
        {
            get { return _gain; }
            set { Set(ref _gain, value); }
        }


        /// <summary>
        /// spike scope图:电压范围
        /// </summary>
        private bool _grid = false;
        public bool Grid
        {
            get { return _grid; }
            set { Set(ref _grid, value); }
        }



        /// <summary>
        /// 通道号
        /// </summary>
        private int _inputIndex = 0;
        public int InputIndex
        {
            get { return _inputIndex; }
            set { Set(ref _inputIndex, value); }
        }

        /// <summary>
        /// 虚假的通道号
        /// </summary>
        private int _physicsIndex = -1;
        public int PhysicsIndex
        {
            get { return _physicsIndex; }
            set { Set(ref _physicsIndex, value); }
        }

        /// <summary>
        /// 信号均方差
        /// </summary>
        private double _rms = 0.0;
        public double RMS
        {
            get { return _rms; }
            set { Set(ref _rms, value); }
        }

        /// <summary>
        /// spike发放频率
        /// </summary>
        private double _spikeRate = 0.0;
        public double SpikeRate
        {
            get { return _spikeRate; }
            set { Set(ref _spikeRate, value); }
        }

        /// <summary>
        /// 是否快照
        /// </summary>
        private bool _takeSnapshot = false;
        public bool TakeSnapshot
        {
            get { return _takeSnapshot; }
            set { Set(ref _takeSnapshot, value); }
        }

        /// <summary>
        /// 清理快照
        /// </summary>
        private bool _clearSnapshot = false;
        public bool ClearSnapshot
        {
            get { return _clearSnapshot; }
            set { Set(ref _clearSnapshot, value); }
        }

        /// <summary>
        /// 清理scope
        /// </summary>
        private bool _clearScope = false;
        public bool ClearScope
        {
            get { return _clearScope; }
            set { Set(ref _clearScope, value); }
        }

        /// <summary>
        /// 是否可见，不可见就没必要加载了
        /// </summary>
        private bool _isVisibleToUser = false;
        public bool IsVisibleToUser
        {
            get { return _isVisibleToUser; }
            set { Set(ref _isVisibleToUser, value); }
        }

    }
}
