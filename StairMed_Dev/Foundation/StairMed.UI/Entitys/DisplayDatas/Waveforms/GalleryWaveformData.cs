﻿using StairMed.Enum;
using StairMed.MVVM.Base;

namespace StairMed.UI.Entitys.DisplayDatas
{
    /// <summary>
    /// 单个画廊的数据
    /// </summary>
    public class GalleryWaveformData : ViewModelBase
    {
        public GalleryWaveformData(NeuralDataType dataType)
        {

        }

        /// <summary>
        /// 是否可见，不可见就没必要加载了
        /// </summary>
        private bool _isVisibleToUser = false;
        public bool IsVisibleToUser
        {
            get { return _isVisibleToUser; }
            set { Set(ref _isVisibleToUser, value); }
        }

        /// <summary>
        /// 控件宽度：决定横向容纳多少个点
        /// </summary>
        private double _viewWidth = 1920;
        public double ViewWidth
        {
            get { return _viewWidth; }
            set { Set(ref _viewWidth, value); }
        }


        /// <summary>
        /// 视窗长度
        /// </summary>
        private int _displayWindowMS = 4000;
        public int DisplayWindowMS
        {
            get { return _displayWindowMS; }
            set { Set(ref _displayWindowMS, value); }
        }
    }
}
