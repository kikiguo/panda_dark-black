﻿using StairMed.DataCenter;
using StairMed.DataCenter.Centers;
using StairMed.Enum;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace StairMed.UI.Entitys.DisplayDatas
{
    /// <summary>
    /// 视窗展示的每个通道上的数据：多个画廊的集合
    /// </summary>
    public class ChannelWaveformData : ViewModelBase, IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        public ChannelWaveformData(long deviceId, int inputIndex, int physicsIndex, Color color)
        {
            Update(deviceId, inputIndex, physicsIndex, color);
            
            //
            ChannelWaveforms = new Dictionary<NeuralDataType, GalleryWaveformData>();
            var types = System.Enum.GetValues(typeof(NeuralDataType));
            foreach (var item in types)
            {
                var type = (NeuralDataType)item;
                ChannelWaveforms[type] = new GalleryWaveformData(type);
            }

            //
            SpikeThresholdCenter.Instance.OnThresholdChanged += Instance_OnThresholdChanged;

            //
            ImpedanceCenter.Instance.OnDeviceChannelImpedanceChanged += Instance_OnDeviceChannelImpedanceChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="inputIndex"></param>
        /// <param name="physicsIndex"></param>
        /// <param name="color"></param>
        public void Update(long deviceId, int inputIndex, int physicsIndex, Color color)
        {
            DeviceId = deviceId;
            InputIndex = inputIndex;
            PhysicsIndex = physicsIndex;

            //
            DeviceId = deviceId;
            InputIndex = inputIndex;
            PhysicsIndex = physicsIndex;
            LineBrush = new SolidColorBrush(color);

            //
            var collectParam = NeuralDataCollector.Instance.GetCollectingDataParam();
            NativeName = collectParam == null ? $"{PhysicsIndex}" : collectParam.GetNativeName(PhysicsIndex);
            //
            Threshold = SpikeThresholdCenter.Instance.GetThreshold(inputIndex);

            //
            ImpedanceCenter.Instance.GetImpedance(DeviceId, PhysicsIndex, out double magnitude, out double phase);
            ImpedanceMagnitude = magnitude;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="physicChannel"></param>
        /// <param name="magnitude"></param>
        /// <param name="phase"></param>
        private void Instance_OnDeviceChannelImpedanceChanged(long deviceId, int physicChannel, double magnitude, double phase)
        {
            if (DeviceId == deviceId && PhysicsIndex == physicChannel)
            {
                ImpedanceMagnitude = magnitude;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Instance_OnThresholdChanged()
        {
            Threshold = SpikeThresholdCenter.Instance.GetThreshold(InputIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        private long _deviceId = 0;
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _impedanceMagnitude = 0.0;
        public double ImpedanceMagnitude
        {
            get { return _impedanceMagnitude; }
            set { Set(ref _impedanceMagnitude, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private Brush _lineBrush = Brushes.White;
        public Brush LineBrush
        {
            get { return _lineBrush; }
            set { Set(ref _lineBrush, value); }
        }

        /// <summary>
        /// 通道序号：对应data chunk中的16bit（字节*2）序号
        /// </summary>
        private int _inputIndex = 0;
        public int InputIndex
        {
            get { return _inputIndex; }
            set { Set(ref _inputIndex, value); }
        }

        /// <summary>
        /// 电极的物理位置顺序
        /// </summary>
        private int _physicsIndex = 0;
        public int PhysicsIndex
        {
            get { return _physicsIndex; }
            set { Set(ref _physicsIndex, value); }
        }

        /// <summary>
        /// NativeName
        /// </summary>
        private string _nativeName = "NName";
        public string NativeName
        {
            get { return _nativeName; }
            set { Set(ref _nativeName, value); }
        }

        /// <summary>
        /// 通道数据：包括原始数据、spike数据、spike组合数据
        /// </summary>
        private Dictionary<NeuralDataType, GalleryWaveformData> _channelWaveforms = new Dictionary<NeuralDataType, GalleryWaveformData>();
        public Dictionary<NeuralDataType, GalleryWaveformData> ChannelWaveforms
        {
            get { return _channelWaveforms; }
            set
            {
                Set(ref _channelWaveforms, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _threshold = -70;
        public double Threshold
        {
            get { return _threshold; }
            set { Set(ref _threshold, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        public void ChangeLineColor(Color color)
        {
            LineBrush = new SolidColorBrush(color);
        }

        /// <summary>
        /// 控件是否可见
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public bool IsVisibleToUser(NeuralDataType dataType)
        {
            var types = System.Enum.GetValues(typeof(NeuralDataType));
            foreach (var item in types)
            {
                var type = (NeuralDataType)item;
                ChannelWaveforms[type].IsVisibleToUser = false;
            }
            return ChannelWaveforms[dataType].IsVisibleToUser =true;
        }


        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
            ImpedanceCenter.Instance.OnDeviceChannelImpedanceChanged -= Instance_OnDeviceChannelImpedanceChanged;
        }
    }
}
