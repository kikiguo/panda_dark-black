﻿using StairMed.Core.Consts;
using StairMed.Enum;
using StairMed.MVVM.Base;
using StairMed.Tools;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Channels;
using System.Windows.Media;

namespace StairMed.UI.Entitys.DisplayDatas
{
    /// <summary>
    /// 视窗展示的设备所有通道的数据
    /// </summary>
    public class DeviceWaveformData : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly ColorScale ColorScale = new ColorScale(0, 100);

        /// <summary>
        /// 
        /// </summary>
        public DeviceWaveformData()
        {
            AllChannels = new ObservableCollection<ChannelWaveformData>();

        }

        /// <summary>
        /// 设备ID
        /// </summary>
        private long _deviceId = 0;
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }

        /// <summary>
        /// 所有通道的数据
        /// </summary>
        private ObservableCollection<ChannelWaveformData> _allChannels = new ObservableCollection<ChannelWaveformData>();
        private ObservableCollection<ChannelWaveformData> _trigChannels = new ObservableCollection<ChannelWaveformData>();
        public ObservableCollection<ChannelWaveformData> AllChannels
        {
            get { return _allChannels; }
            set { Set(ref _allChannels, value); }
        }


        public ObservableCollection<ChannelWaveformData> TRIGChannels
        {
            get { return _trigChannels; }
            set { Set(ref _trigChannels, value); }
        }
        /// <summary>
        /// 当前控件是否可见
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public bool IsVisibleToUser(int channel, NeuralDataType dataType)
        {
            
                TRIGChannels.FirstOrDefault(r => r.InputIndex == channel).IsVisibleToUser(dataType);
         
           
            return AllChannels.FirstOrDefault(r => r.InputIndex == channel).IsVisibleToUser(dataType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicsIndex"></param>
        /// <returns></returns>
        public static Color GetWaveformColor(int physicsIndex)
        {
            return ColorScale.GetColor((physicsIndex + 3) * 18 % 85 + 15);
        }

     

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="physicsChannels"></param>
        public void Update(long deviceId, List<int> physicsChannels)
        {
            if (TRIGChannels.Count == 9)
            {
                for (int i = 0; i < 1; i++)
                {
                    var physicsIndex = physicsChannels[i+1];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);

                    TRIGChannels[i].Update(deviceId, inputIndex, physicsIndex, Color.FromRgb(255,255,255));
                }
            }
            else if (TRIGChannels.Count < 9)
            {
                for (int i = 0; i < TRIGChannels.Count; i++)
                {
                    var physicsIndex = physicsChannels[i+1];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
                    TRIGChannels[i].Update(deviceId, inputIndex, physicsIndex, Color.FromRgb(255, 255, 255));
                }

                //
                for (int i = TRIGChannels.Count; i < 9; i++)
                {
                    var physicsIndex = physicsChannels[i+1];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
            
                    TRIGChannels.Add(new ChannelWaveformData(deviceId, inputIndex, physicsIndex, Color.FromRgb(255, 255, 255)));
                }

            }
            else
            {
                for (int i = 0; i < 9; i++)
                {
                    var physicsIndex = physicsChannels[i+1];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
                    TRIGChannels[i].Update(deviceId, inputIndex, physicsIndex, Color.FromRgb(255, 255, 255));
                }

                //
                while (TRIGChannels.Count >9)
                {
                    TRIGChannels.RemoveAt(2);
                }
            }
            




            if (AllChannels.Count == physicsChannels.Count)
            {
                for (int i = 0; i < physicsChannels.Count; i++)
                {
                    var physicsIndex = physicsChannels[i];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
                    AllChannels[i].Update(deviceId, inputIndex, physicsIndex, GetWaveformColor(physicsIndex));
                }
            }
            else if (AllChannels.Count < physicsChannels.Count)
            {
                for (int i = 0; i < AllChannels.Count; i++)
                {
                    var physicsIndex = physicsChannels[i];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
                    AllChannels[i].Update(deviceId, inputIndex, physicsIndex, GetWaveformColor(physicsIndex));
                }

                //
                for (int i = AllChannels.Count; i < physicsChannels.Count; i++)
                {
                    var physicsIndex = physicsChannels[i];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
                    AllChannels.Add(new ChannelWaveformData(deviceId, inputIndex, physicsIndex, GetWaveformColor(physicsIndex)));
                }
            }
            else
            {
                for (int i = 0; i < physicsChannels.Count; i++)
                {
                    var physicsIndex = physicsChannels[i];
                    var inputIndex = ReadonlyCONST.ConvertToInputChannel(physicsIndex);
                    AllChannels[i].Update(deviceId, inputIndex, physicsIndex, GetWaveformColor(physicsIndex));
                }

                //
                while (AllChannels.Count > physicsChannels.Count)
                {
                    AllChannels.RemoveAt(physicsChannels.Count);
                }
            }
        }
    }
}
