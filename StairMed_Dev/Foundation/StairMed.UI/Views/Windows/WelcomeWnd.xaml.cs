﻿using StairMed.Controls;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.UI.Views.Windows
{
    /// <summary>
    /// WelcomeWnd.xaml 的交互逻辑
    /// </summary>
    public partial class WelcomeWnd
    {
        /// <summary>
        /// 
        /// </summary>
        public WelcomeWnd(Action WndOnClosing)
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            InitializeComponent();
            this.BindingDragMove();
            this.MaxHeight = SystemParameters.WorkArea.Height;
            this.WindowState = WindowState.Maximized;
            //
            Task.Run(() =>
            {
                Thread.Sleep(30000);

                //所有检查工作结束，进入主题
                UIHelper.BeginInvoke(() =>
                {
                    LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                    this.DelayClose();
                    WndOnClosing.Invoke();
                });
            });
        }
    }
}
