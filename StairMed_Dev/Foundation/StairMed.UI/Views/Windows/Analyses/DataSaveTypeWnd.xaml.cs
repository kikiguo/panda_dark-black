﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.UI.Logger;
using System.Windows;

namespace StairMed.UI.Views.Windows.Analyses
{
    /// <summary>
    /// DataSaveTypeWnd.xaml 的交互逻辑
    /// </summary>
    public partial class DataSaveTypeWnd
    {
        /// <summary>
        /// 
        /// </summary>
        public DataSaveTypeWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            this.Close();
        }
    }
}
