﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.UI.Entitys.DisplayDatas.SpikeScopes;
using StairMed.UI.Logger;
using StairMed.UI.Views.Windows.Analyses;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;

namespace StairMed.UI.Views.UserControls.Waveforms
{
    /// <summary>
    /// SpikeScopeView.xaml 的交互逻辑
    /// </summary>
    public partial class SpikeScopeView : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public List<int> _displayChannels = new List<int>();

        /// <summary>
        /// 
        /// </summary>
        public SpikeScopeView()
        {
            InitializeComponent();

            //
            EventHelper.Subscribe<FilterDisplayChannelsEvent, List<int>>((channels) =>
            {
                _displayChannels = channels;
                itemControl.Items.Filter = (obj) =>
                {
                    return _displayChannels.Contains(((ChannelSpikeScopeData)obj).InputIndex);
                };
            }, Prism.Events.ThreadOption.UIThread);

          
            
        }

       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NeuralChart_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is NeuralChartBase neuralChart)
            {
                try
                {
              
                    var inputChannel = neuralChart.Channel;
                    var physicsChannel = ReadonlyCONST.ConvertToPhysicsChannel(inputChannel);
                    if (StairMedSettings.Instance.ISIChannelLockToClick)
                    {
                        StairMedSettings.Instance.ISIChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.PSTHChannelLockToClick)
                    {
                        StairMedSettings.Instance.PSTHChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.WaveformChannelLockToClick)
                    {
                        StairMedSettings.Instance.WaveformChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.SpectrumChannelLockToClick)
                    {
                        StairMedSettings.Instance.SpecturmChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.SpikeScopeChannelLockToClick)
                    {
                        StairMedSettings.Instance.SpikeScopeChannel = physicsChannel;
                        
                    }
                }
                catch(Exception ex)
                {
                    LogTool.Logger.Exp($"{nameof(SpikeScopeView)}, {nameof(NeuralChart_MouseLeftButtonDown)}", ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NeuralChart_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
           
            if (sender is NeuralChartBase neuralChart)
            {
                try
                {
                    var inputChannel = neuralChart.Channel;
                    var physicsChannel = ReadonlyCONST.ConvertToPhysicsChannel(inputChannel);
                    StairMedSettings.Instance.SpikeScopeChannel = physicsChannel;
                    SpikeScopeWnd.PopUp();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{nameof(SpikeScopeView)}, {nameof(NeuralChart_MouseDoubleClick)}", ex);
                }
            }
        }
    }
}
