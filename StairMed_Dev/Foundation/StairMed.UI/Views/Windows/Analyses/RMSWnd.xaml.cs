﻿using StairMed.Controls;
using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Controls.Controls.NeuralCharts.RMSCharts;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Windows;

namespace StairMed.UI.Views.Windows.Analyses
{
    /// <summary>
    /// RMSWnd.xaml 的交互逻辑
    /// </summary>
    public partial class RMSWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal RMSWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.btnRestore.Visibility = Visibility.Collapsed;
            //
            this.cmbTimeSpan.ItemsSource = CONST.ISITimeSpanMSOptions;
            this.cmbBinSize.ItemsSource = CONST.ISIBinSizeMSOptions;
            this.ChannelName.ItemsSource = CONST.RMSChannels;
            this.RMSValue.ItemsSource = CONST.RMSValue;

            //this.numChannels.Maximum = ReadonlyCONST.DeviceChannelCount;

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                EventHelper.Publish<WindowLoadEvent, string>(this.GetType().Name);
            };
            this.Closed += (object sender, EventArgs e) =>
            {
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);
                _wnd = null;
                Application.Current.MainWindow.TakeToFront();
            };

            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new RMSWnd().Show();
                }
            });
        }

        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NeuralChart_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            if (sender is RMSChart neuralChart)
            {
                try
                {
                    StairMedSettings.Instance.RMSMouseDouble= StairMedSettings.Instance.RMSMouseDouble?false : true;
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{nameof(RMSWnd)}, {nameof(NeuralChart_MouseDoubleClick)}", ex);
                }
            }
        }
    }
}
