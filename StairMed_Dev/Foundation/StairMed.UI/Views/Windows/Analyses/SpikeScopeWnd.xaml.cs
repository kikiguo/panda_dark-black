﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Windows;
using System.Windows.Input;

namespace StairMed.UI.Views.Windows.Analyses
{
    /// <summary>
    /// SpikeScopeWnd.xaml 的交互逻辑
    /// </summary>
    public partial class SpikeScopeWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal SpikeScopeWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.btnRestore.Visibility = Visibility.Collapsed;
            //
            this.cmbKMeansK.ItemsSource = CONST.SpikeScopeTypeOptions;
            this.cmbTimeScale.ItemsSource = CONST.SpikeScopeTimeScaleMSOptions;
            this.cmbVoltageScale.ItemsSource = CONST.SpikeScopeVoltageScaleMSOptions;
            this.cmbReserved.ItemsSource = CONST.SpikeScopeGlowOptions;
            this.numChannels.Maximum = ReadonlyCONST.DeviceChannelCount;

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                EventHelper.Publish<WindowLoadEvent, string>(this.GetType().Name);
            };
            this.Closed += (object sender, EventArgs e) =>
            {
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);
                _wnd = null;
                Application.Current.MainWindow.TakeToFront();
            };
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new SpikeScopeWnd().Show();
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpikeScopeChart_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateThreshold(e);
            }
            else
            {
                UpdateUVScale(e);
            }

            //
            e.Handled = true;
        }

        /// <summary>
        /// 调整电压范围
        /// </summary>
        /// <param name="e"></param>
        private void UpdateUVScale(MouseWheelEventArgs e)
        {
            var index = CONST.SpikeScopeVoltageScaleMSOptions.IndexOf(StairMedSettings.Instance.SpikeScopeVoltageRange);
            if (index < 0)
            {
                StairMedSettings.Instance.SpikeScopeVoltageRange = CONST.SpikeScopeVoltageScaleMSOptions[0];
            }
            else
            {
                index += (e.Delta < 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.SpikeScopeVoltageScaleMSOptions.Count - 1, index));
                StairMedSettings.Instance.SpikeScopeVoltageRange = CONST.SpikeScopeVoltageScaleMSOptions[index];
            }
        }

        /// <summary>
        /// 调整阈值
        /// </summary>
        /// <param name="e"></param>
        private void UpdateThreshold(MouseWheelEventArgs e)
        {
            var threshold = this.numThreshold.Value;
            threshold += (e.Delta > 0 ? 1 : -1);
            this.numThreshold.Value = threshold;
        }

        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
