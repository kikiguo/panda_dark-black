﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Timers;
using System.Windows;
using System.Windows.Input;

namespace StairMed.UI.Views.Windows.Analyses
{
    /// <summary>
    /// WaveformWnd.xaml 的交互逻辑
    /// </summary>
    public partial class WaveformWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal WaveformWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.WindowState = WindowState.Normal;
            this.btnRestore.Visibility = Visibility.Collapsed;

            //
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };

            //
            this.cmbDisplayWindow.ItemsSource = CONST.DisplayWindowMSOptions;
            this.cmbWaveHeight.ItemsSource = CONST.WaveHeightOptions;

            //
            this.numChannels.Maximum = ReadonlyCONST.DeviceChannelCount;

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                EventHelper.Publish<WindowLoadEvent, string>(this.GetType().Name);
            };
            this.Closed += (object sender, EventArgs e) =>
            {
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);
                _wnd = null;
                Application.Current.MainWindow.TakeToFront();
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// 波形高度变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WaveChart_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateWaveformHeight(e);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateWaveMax(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 时间范围变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimeAxis_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateDisplayWindowMS(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpikeSumChart_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateWaveformHeight(e);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateSpikeSumMax(e);
                e.Handled = true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateDisplayWindowMS(MouseWheelEventArgs e)
        {
            //调试视窗长度
            var index = CONST.DisplayWindowMSOptions.IndexOf(StairMedSettings.Instance.DisplayWindowMS);
            if (index < 0)
            {
                StairMedSettings.Instance.DisplayWindowMS = CONST.DisplayWindowMSOptions[0];
            }
            else
            {
                index += (e.Delta < 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.DisplayWindowMSOptions.Count - 1, index));
                StairMedSettings.Instance.DisplayWindowMS = CONST.DisplayWindowMSOptions[index];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateWaveMax(MouseWheelEventArgs e)
        {
            //调整电压范围
            var ratioA = e.Delta < 0 ? 11 : 10;
            var ratioB = e.Delta < 0 ? 10 : 11;
            StairMedSettings.Instance.WaveMax = Convert.ToSingle(Math.Round(StairMedSettings.Instance.WaveMax * ratioA / ratioB));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateWaveformHeight(MouseWheelEventArgs e)
        {
            //调整高度
            var index = CONST.WaveHeightOptions.IndexOf(StairMedSettings.Instance.WaveformHeight);
            if (index < 0)
            {
                StairMedSettings.Instance.WaveformHeight = CONST.WaveHeightOptions[0];
            }
            else
            {
                index += (e.Delta > 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.WaveHeightOptions.Count - 1, index));
                StairMedSettings.Instance.WaveformHeight = CONST.WaveHeightOptions[index];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateSpikeSumMax(MouseWheelEventArgs e)
        {
            //调整电压范围
            var ratioA = e.Delta < 0 ? 11 : 10;
            var ratioB = e.Delta < 0 ? 10 : 11;
            StairMedSettings.Instance.SpikeSumMax = Convert.ToSingle(Math.Round(StairMedSettings.Instance.SpikeSumMax * ratioA / ratioB));
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new WaveformWnd().Show();
                }
            });
        }
    }
}
