﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Collections.Generic;
using System.Windows;

namespace StairMed.UI.Views.Windows.Analyses
{
    /// <summary>
    /// PSTHWnd.xaml 的交互逻辑
    /// </summary>
    public partial class PSTHWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static System.Windows.Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal PSTHWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.btnRestore.Visibility = Visibility.Collapsed;
            //
            this.cmbTriggerChannels.ItemsSource = ReadonlyCONST.TriggerChannels;
            this.cmbTrials.ItemsSource = CONST.PSTHTrialOptions;
            this.cmbPreTrigger.ItemsSource = new List<int> { 50, 100, 200, 500, 1000, 2000 };
            this.cmbPostTrigger.ItemsSource = new List<int> { 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, };
            this.cmbBinSize.ItemsSource = new List<int> { 1, 2, 5, 10, 20, 50, 100 };
            //this.numChannels.Maximum = ReadonlyCONST.DeviceChannelCount;

            //
            this.Loaded += (object sender, RoutedEventArgs e)=>
            {
                EventHelper.Publish<WindowLoadEvent, string>(this.GetType().Name);
            };
            this.Closed += (object sender, EventArgs e)=>
            {
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);
                _wnd = null;
                Application.Current.MainWindow.TakeToFront();
            };
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new PSTHWnd().Show();
                }
            });
        }
        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
