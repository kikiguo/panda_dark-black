﻿using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.Core.Helpers;
using StairMed.Core.Settings;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Collections.Generic;

namespace StairMed.UI.Views.Windows
{
    /// <summary>
    /// MonkeySelectionWnd.xaml 的交互逻辑
    /// </summary>
    public partial class MonkeySelectionWnd
    {
        /// <summary>
        /// 
        /// </summary>
        public Action WndOnClosing;

        /// <summary>
        /// 
        /// </summary>
        public MonkeySelectionWnd(StairMedSolidSettings settings, Action OnClosing)
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            this.WndOnClosing = OnClosing;
            InitializeComponent();
            this.BindingDragMove();

            //
            this.cmbType.ItemsSource = settings.HistoryTypes;
            if (settings.HistoryTypes != null && settings.HistoryTypes.Count > 0)
            {
                this.cmbType.SelectedIndex = 0;
            }

            //
            this.cmbCode.ItemsSource = settings.HistoryCodes;
            if (settings.HistoryCodes != null && settings.HistoryCodes.Count > 0)
            {
                this.cmbCode.SelectedIndex = 0;
            }

            this.cmbHospital.ItemsSource = settings.HistoryHospital;
            if (settings.HistoryHospital != null && settings.HistoryHospital.Count > 0)
            {
                this.cmbHospital.SelectedIndex = 0;
            }

            this.cmbElecode.ItemsSource = settings.HistoryEleCode;
            if (settings.HistoryEleCode != null && settings.HistoryEleCode.Count > 0)
            {
                this.cmbElecode.SelectedIndex = 0;
            }

            
            if (settings.HistoryCreateTime != null && settings.HistoryCreateTime.Count > 0)
            {
                this.txtTime.Text = settings.HistoryCreateTime[0];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            //
            if (string.IsNullOrWhiteSpace(cmbType.Text) || string.IsNullOrWhiteSpace(cmbCode.Text))
            {
                Toast.Warn("请输入实验对象和实验目的");
                return;
            }

            //
            UIHelper.BeginInvoke(() =>
            {
                this.DelayClose();
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");

                //
                var testType = cmbType.Text.Trim();
                var testCode = cmbCode.Text.Trim();
                var testHospital = cmbHospital.Text.Trim();
                var testElecode = cmbElecode.Text.Trim();

                //
                StairMedSolidSettings.Instance.TestType = testType;
                {
                    StairMedSolidSettings.Instance.HistoryTypes.Remove(StairMedSolidSettings.Instance.TestType);
                    var list = new List<string>(StairMedSolidSettings.Instance.HistoryTypes);
                    list.Insert(0, StairMedSolidSettings.Instance.TestType);
                    StairMedSolidSettings.Instance.HistoryTypes = list;
                }

                //
                StairMedSolidSettings.Instance.TestCode = testCode;
                {
                    StairMedSolidSettings.Instance.HistoryCodes.Remove(StairMedSolidSettings.Instance.TestCode);
                    var list = new List<string>(StairMedSolidSettings.Instance.HistoryCodes);
                    list.Insert(0, StairMedSolidSettings.Instance.TestCode);
                    StairMedSolidSettings.Instance.HistoryCodes = list;
                }


                StairMedSolidSettings.Instance.TestHosptial = testHospital;
                {
                    StairMedSolidSettings.Instance.HistoryHospital.Remove(StairMedSolidSettings.Instance.TestHosptial);
                    var list = new List<string>(StairMedSolidSettings.Instance.HistoryHospital);
                    list.Insert(0, StairMedSolidSettings.Instance.TestHosptial);
                    StairMedSolidSettings.Instance.HistoryHospital = list;
                }

                StairMedSolidSettings.Instance.TestEleCode = testElecode;
                {
                    StairMedSolidSettings.Instance.HistoryEleCode.Remove(StairMedSolidSettings.Instance.TestEleCode);
                    var list = new List<string>(StairMedSolidSettings.Instance.HistoryEleCode);
                    list.Insert(0, StairMedSolidSettings.Instance.TestEleCode);
                    StairMedSolidSettings.Instance.HistoryEleCode = list;
                }

                if (string.IsNullOrWhiteSpace(txtTime.Text))
                {
                    var testCreateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
                    StairMedSolidSettings.Instance.CreateTime = testCreateTime;
                    {
                        StairMedSolidSettings.Instance.HistoryCreateTime.Remove(StairMedSolidSettings.Instance.CreateTime);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryCreateTime);
                        list.Insert(0, StairMedSolidSettings.Instance.CreateTime);
                        StairMedSolidSettings.Instance.HistoryCreateTime = list;
                    }
                }
 
                this.WndOnClosing.Invoke();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Close();
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
        }
    }
}
