﻿using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Probe;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace StairMed.UI.Views.Windows.Settings
{
    /// <summary>
    /// ElectrodeMapWnd.xaml 的交互逻辑
    /// </summary>
    public partial class ElectrodeMapWnd
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(ElectrodeMapWnd);

        /// <summary>
        /// 
        /// </summary>
        private bool _isImported = false;

        /// <summary>
        /// 
        /// </summary>
        public Action<NTProbeMap> NewNTProbeMapImported;

        /// <summary>
        /// 
        /// </summary>
        public ElectrodeMapWnd(NTProbeMap probeMap, Action<NTProbeMap> importedNew)
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            NewNTProbeMapImported = importedNew;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();

            //
            if (NewNTProbeMapImported == null)
            {
                btnImportNewProbeMap.Visibility = Visibility.Collapsed;
            }

            //
            this.txtRebootTip.Visibility = Visibility.Collapsed;
            this.btnRestore.Visibility = Visibility.Collapsed;
            this.probeMapChart.ProbeMap = probeMap;

            //
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };

            //
            this.Closing += Window_Closing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImportNewProbeMap_Click(object sender, RoutedEventArgs e)
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new Microsoft.Win32.OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //
                try
                {
                    //
                    var xml = FileHelper.LoadFile(dialog.FileName, Encoding.UTF8);
                    if (string.IsNullOrWhiteSpace(xml))
                    {
                        Toast.Warn($"文件为空，无法导入");
                        return;
                    }

                    //
                    using (var reader = new StringReader(xml))
                    {
                        NTProbeMap newProbeMap = null;
                        try
                        {
                            newProbeMap = (NTProbeMap)new XmlSerializer(typeof(NTProbeMap)).Deserialize(reader);
                        }
                        catch (Exception ex)
                        {
                            LogTool.Logger.Exp($"{TAG}, Deserialize", ex);
                        }

                        //
                        if (newProbeMap == null)
                        {
                            Toast.Warn($"文件有误，无法导入");
                            return;
                        }

                        //
                        if (newProbeMap.IsInValid())
                        {
                            Toast.Warn($"文件无效，导入失败");
                            return;
                        }

                        //
                        if (this.probeMapChart.ProbeMap.ChannelCount != newProbeMap.ChannelCount)
                        {
                            Toast.Warn($"通道个数不匹配");
                            return;
                        }

                        //
                        this.probeMapChart.ProbeMap = newProbeMap;

                        //
                        if (!_isImported)
                        {
                            _isImported = true;
                            this.txtRebootTip.Visibility = Visibility.Visible;
                        }

                        //
                        NewNTProbeMapImported.Invoke(newProbeMap);

                        //
                        Toast.Info($"新电极阵列已导入，重启软件后生效");
                    }
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, load", ex);
                    Toast.Error($"文件有误，无法导入");
                    return;
                }
            });
        }
    }
}
