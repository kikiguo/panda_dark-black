﻿using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.UI.Views.Windows.Settings
{
    /// <summary>
    /// AutoSpikeThresholdWnd.xaml 的交互逻辑
    /// </summary>
    public partial class AutoSpikeThresholdWnd
    {
        const string TAG = nameof(AutoSpikeThresholdWnd);

        /// <summary>
        /// 
        /// </summary>
        private Func<double, bool> RelativeThresholdCallback;

        /// <summary>
        /// 
        /// </summary>
        public AutoSpikeThresholdWnd(Func<double, bool> _relativeThresholdCallback)
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            this.RelativeThresholdCallback = _relativeThresholdCallback;

            //
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();

            //
            rbtnAbsoluteThreshold.IsChecked = true;
            numAbsoluteThreshold.Value = -70;
            cmbRMSSign.SelectedIndex = 1;
            numRMSMultiple.Value = 3.0;
            loading.Visibility = Visibility.Collapsed;

            //
            this.Closing += Window_Closing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (rbtnAbsoluteThreshold.IsChecked.HasValue && rbtnAbsoluteThreshold.IsChecked.Value)
            {
                StairMedSettings.Instance.Threshold = numAbsoluteThreshold.Value;
                SpikeThresholdCenter.Instance.SetThreshold(numAbsoluteThreshold.Value);
                Toast.Info("设置成功");
                this.Close();
            }
            else
            {
                loading.Visibility = Visibility.Visible;
                var multi = numRMSMultiple.Value * (cmbRMSSign.SelectedIndex == 0 ? 1 : -1);

                //
                Task.Run(() =>
                {
                    try
                    {
                        if (RelativeThresholdCallback.Invoke(multi))
                        {
                            UIHelper.BeginInvoke(() =>
                            {
                                this.Close();
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        LogTool.Logger.Exp($"{TAG}, {nameof(RelativeThresholdCallback)}", ex);
                    }

                    //
                    UIHelper.BeginInvoke(() =>
                    {
                        loading.Visibility = Visibility.Collapsed;
                    });
                });
            }
        }
    }
}
