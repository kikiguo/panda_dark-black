﻿using StairMed.Core.Settings;
using System.Windows.Controls;

namespace StairMed.UI.Views.Windows.Settings
{
    /// <summary>
    /// AdvanceSettingPage.xaml 的交互逻辑
    /// </summary>
    public partial class AdvanceSettingPage : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public AdvanceSettings AdvanceSettings { get; set; } = AdvanceSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public AdvanceSettingPage()
        {
            InitializeComponent();
            this.DataContext = this;
        }
    }
}
