﻿using StairMed.Core.Consts;
using System.Windows.Controls;

namespace StairMed.UI.Views.Windows.Settings
{
    /// <summary>
    /// BasicSettingPage.xaml 的交互逻辑
    /// </summary>
    public partial class BasicSettingPage : UserControl
    {
        public BasicSettingPage()
        {
            InitializeComponent();

            //
            this.cmbSpikeRateStatisticMS.ItemsSource = CONST.SpikeRateStatisticMSOptions;
            this.cmbRMSStatisticMS.ItemsSource = CONST.SpikeRateStatisticMSOptions;
        }
    }
}
