﻿using StairMed.Core.Settings;
using StairMed.UI.Views.Windows;
using StairMed.UI.Views.Windows.Analyses;
using System.Collections.Generic;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// WaveSettings.xaml 的交互逻辑
    /// </summary>
    public partial class WaveSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public WaveSettings()
        {
            InitializeComponent();

            //this.DataContext = this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWaveform_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            WaveformWnd.PopUp();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSpikeScope_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SpikeScopeWnd.PopUp();
        }
    }
}
