﻿using StairMed.ChipConst.Intan;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Enum;
using System.Collections.Generic;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// FilterSettings.xaml 的交互逻辑
    /// </summary>
    public partial class FilterSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public FilterSettings()
        {
            InitializeComponent();

            //
            this.DataContext = this;

            //
            this.cmbNotchFilterType.ItemsSource = new List<NotchFilterType> { NotchFilterType.None, NotchFilterType._50Hz, NotchFilterType._60Hz };
            this.cmbLowPassFilterType.ItemsSource = CONST.FilterTypes;
            this.cmbHighPassFilterType.ItemsSource = CONST.FilterTypes;
            this.cmbLowPassFilterOrder.ItemsSource = CONST.FilterOrders;
            this.cmbHighPassFilterOrder.ItemsSource = CONST.FilterOrders;

            //
            var lowerBandwidths = new List<double>();
            foreach (var kvp in IntanConst.AmplifierLowerBandwidthDict)
            {
                lowerBandwidths.Add(kvp.Key);
            }
            this.cmbLowerBandwidths.ItemsSource = lowerBandwidths;

            //
            var upperBandwidths = new List<double>();
            foreach (var kvp in IntanConst.AmplifierUpperBandwidthDict)
            {
                upperBandwidths.Add(kvp.Key);
            }
            this.cmbUpperBandwidths.ItemsSource = upperBandwidths;
        }
    }
}
