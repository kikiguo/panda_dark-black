﻿using StairMed.Core.Consts;
using StairMed.Core.Settings;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// WaveformSettings.xaml 的交互逻辑
    /// </summary>
    public partial class WaveformSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public WaveformSettings()
        {
            InitializeComponent();

            this.DataContext = this;

            //
            this.cmbDisplayWindow.ItemsSource = CONST.DisplayWindowMSOptions;
            this.cmbWaveformDisplayColumn.ItemsSource = CONST.DisplayColumnOptions;
            this.cmbWaveHeight.ItemsSource = CONST.WaveHeightOptions;
        }
    }
}
