﻿using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.UI.ViewModels.UserControls.SideBars;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// Interaction logic for SampleSettings.xaml
    /// </summary>
    public partial class SampleSettings : UserControl
    {
        SampleSettingsViewModel vm=null;
        public SampleSettings()
        {
            InitializeComponent();
            vm = (SampleSettingsViewModel)this.DataContext;
        }

        private void btnListAdd_Click(object sender, RoutedEventArgs e)
        {
            var noneMembers = ListView_NoneMembers.SelectedItems;
            if (noneMembers.Count > 0 && vm!=null)
            {
                vm.NoneMemberSelected.Clear();
                foreach (ChanMember member in noneMembers)
                {
                    vm.NoneMemberSelected.Add(member);
                }
                vm.MemberAddCommand.Execute(null);
            }
        }

        private void btnListRemove_Click(object sender, RoutedEventArgs e)
        {
            var members = ListView_GroupMember.SelectedItems;
            if (members.Count>0 && vm!=null)
            {
                vm.GroupMemberSelected.Clear();
                foreach (ChanMember member in members)
                {
                    vm.GroupMemberSelected.Add(member);
                }
                vm.MemberRemoveCommand.Execute(null);
            }
        }
    }

    public class IntToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                int data = System.Convert.ToInt32(value);
                int param = System.Convert.ToInt32(parameter);
                return data == param;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var data = (bool)value;
                if (data)
                {
                    return System.Convert.ToInt32(parameter);
                }
                return -1;
            }
            catch (Exception)
            {

                return -1;
            }
        }
    }

    public class BoolToForeground : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                bool val = System.Convert.ToBoolean(value);
                if (val)
                {
                    return "#FFFFFFFF";
                }
                else
                {
                    return "#FFA9A9A9";
                }
            }
            catch (Exception)
            {

                return "#FFA9A9A9";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
