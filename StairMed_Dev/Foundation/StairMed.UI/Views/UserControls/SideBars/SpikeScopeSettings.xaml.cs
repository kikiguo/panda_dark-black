﻿using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using System.Windows.Input;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// SpikeScopeSettings.xaml 的交互逻辑
    /// </summary>
    public partial class SpikeScopeSettings
    {
        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public SpikeScopeSettings()
        {
            InitializeComponent();

            this.DataContext = this;

            //
            this.cmbKMeansK.ItemsSource = CONST.SpikeScopeTypeOptions;
            this.cmbTimeScale.ItemsSource = CONST.SpikeScopeTimeScaleMSOptions;
            this.cmbVoltageScale.ItemsSource = CONST.SpikeScopeVoltageScaleMSOptions;
            this.cmbReserved.ItemsSource = CONST.SpikeScopeGlowOptions;
            this.cmbSpikeScopeDisplayColumnn.ItemsSource = CONST.DisplayColumnOptions;
        }
    }
}
