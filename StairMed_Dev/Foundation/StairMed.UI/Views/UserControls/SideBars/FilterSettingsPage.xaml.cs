﻿using StairMed.ChipConst.Intan;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// Interaction logic for FilterSettingsPage.xaml
    /// </summary>
    public partial class FilterSettingsPage : UserControl
    {
        
        public FilterSettingsPage()
        {
            InitializeComponent();
            //this.DataContext = this;

            ////
            //this.cmbNotchFilterType.ItemsSource = new List<NotchFilterType> { NotchFilterType.None, NotchFilterType._50Hz, NotchFilterType._60Hz };
            //this.cmbLowPassFilterType.ItemsSource = CONST.FilterTypes;
            //this.cmbHighPassFilterType.ItemsSource = CONST.FilterTypes;
            this.cmbLowPassFilterOrder.ItemsSource = CONST.FilterOrders;
            //this.cmbHighPassFilterOrder.ItemsSource = CONST.FilterOrders;

            ////
            //var lowerBandwidths = new List<double>();
            //foreach (var kvp in IntanConst.AmplifierLowerBandwidthDict)
            //{
            //    lowerBandwidths.Add(kvp.Key);
            //}
            //this.cmbLowerBandwidths.ItemsSource = lowerBandwidths;

            ////
            //var upperBandwidths = new List<double>();
            //foreach (var kvp in IntanConst.AmplifierUpperBandwidthDict)
            //{
            //    upperBandwidths.Add(kvp.Key);
            //}
            //this.cmbUpperBandwidths.ItemsSource = upperBandwidths;
        }
    }
}
