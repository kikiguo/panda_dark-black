﻿using StairMed.Core.Consts;
using StairMed.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// Interaction logic for AudioSettings.xaml
    /// </summary>
    public partial class AudioSettings : UserControl
    {
        public AudioSettings()
        {
            InitializeComponent();

            //this.cb_OutputType.ItemsSource = CONST.OutputType;
            //this.cb_OutputType2.ItemsSource = CONST.OutputType;

            List<string> listChannel = new List<string>();
            for (int i = 1; i <= 1024; i++)
            {
                listChannel.Add(i.ToString().PadLeft(4, '0'));
            }
            this.cb_ChannelName.ItemsSource = listChannel;
            this.cb_ChannelName2.ItemsSource = listChannel;
        }
    }
}
