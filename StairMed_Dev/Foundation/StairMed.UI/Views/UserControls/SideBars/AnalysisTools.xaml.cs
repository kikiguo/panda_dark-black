﻿using StairMed.Core.Consts;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers;
using StairMed.DataCenter.Centers.Trigger;
using StairMed.Entity.DataPools;
using StairMed.UI.Views.Windows.Analyses;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StairMed.UI.Views.UserControls.SideBars
{
    /// <summary>
    /// AnalysisTools.xaml 的交互逻辑
    /// </summary>
    public partial class AnalysisTools
    {
        /// <summary>
        /// 
        /// </summary>
        public AnalysisTools()
        {
            InitializeComponent();

            this.DataContext = this;

            //
            Task.Factory.StartNew(GenerateTriggerLoop, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWaveform_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            WaveformWnd.PopUp();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSpikeScope_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SpikeScopeWnd.PopUp();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnISI_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ISIWnd.PopUp();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPSTH_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PSTHWnd.PopUp();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSpectrogram_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SpectrumWnd.PopUp();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProbeMapSpike_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SpikeMapWnd.PopUp();
        }




        #region 伪造trigger信号

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTrigger_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _autoGenerate = !_autoGenerate;
        }

        /// <summary>
        /// 
        /// </summary>
        private int _index = 0;
        private readonly Dictionary<int, bool> _levelDict = new Dictionary<int, bool>();
        private long _lastTriggerIndex = 0;
        private bool _autoGenerate = false;

        /// <summary>
        /// 
        /// </summary>
        public void GenerateTriggerLoop()
        {
            while (true)
            {
                try
                {
                    while (_autoGenerate)
                    {
                        Thread.Sleep(200);
                        //Thread.Sleep(50);

                        //
                        _index++;
                        if (!NeuralDataCollector.Instance.IsCollecting)
                        {
                            break;
                        }

                        //
                        var triggerChannels = new HashSet<int>();
                        for (int i = 0; i < ReadonlyCONST.TriggerChannels.Count; i++)
                        {
                            if (_index % (i * 2 + 2) == 0)
                            {
                                triggerChannels.Add(i);
                            }
                        }

                        //
                        if (triggerChannels.Count <= 0)
                        {
                            continue;
                        }

                        //
                        var pack = new TriggerPack
                        {
                            SampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate(),
                            Triggers = new Dictionary<int, List<TriggerEdge>>()
                        };

                        //
                        var offset = NeuralDataCollector.Instance.RecvdLength;

                        //
                        if (offset <= _lastTriggerIndex)
                        {
                            offset = _lastTriggerIndex + 300;
                        }
                        _lastTriggerIndex = offset;

                        //
                        foreach (var channel in triggerChannels)
                        {
                            var oldLevel = _levelDict.ContainsKey(channel) ? _levelDict[channel] : false;

                            //
                            pack.Triggers[channel] = new List<TriggerEdge>
                            {
                                new TriggerEdge { ToHigh = oldLevel, Index = offset}
                            };

                            //
                            _levelDict[channel] = !oldLevel;
                        }

                        //
                        TriggerDataPool.Instance.BufferTrigger(pack);
                    }
                }
                catch { }
                Thread.Sleep(1000);
                _lastTriggerIndex = 0;
            }
        }

        #endregion
    }
}
