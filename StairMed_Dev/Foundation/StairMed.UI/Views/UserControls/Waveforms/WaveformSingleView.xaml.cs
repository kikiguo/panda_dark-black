﻿using StairMed.Core.Consts;
using StairMed.Core.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StairMed.UI.Views.UserControls.Waveforms
{
    /// <summary>
    /// Interaction logic for SpikeScopeSingleView.xaml
    /// </summary>
    public partial class WaveformSingleView : UserControl
    {
        public WaveformSingleView()
        {
            InitializeComponent();
        }

        private void WaveChart_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateWaveformHeight(e);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateWaveMax(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 时间范围变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimeAxis_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateDisplayWindowMS(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpikeSumChart_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateWaveformHeight(e);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateSpikeSumMax(e);
                e.Handled = true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateDisplayWindowMS(MouseWheelEventArgs e)
        {
            //调试视窗长度
            var index = CONST.DisplayWindowMSOptions.IndexOf(StairMedSettings.Instance.DisplayWindowMS);
            if (index < 0)
            {
                StairMedSettings.Instance.DisplayWindowMS = CONST.DisplayWindowMSOptions[0];
            }
            else
            {
                index += (e.Delta < 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.DisplayWindowMSOptions.Count - 1, index));
                StairMedSettings.Instance.DisplayWindowMS = CONST.DisplayWindowMSOptions[index];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateWaveMax(MouseWheelEventArgs e)
        {
#if true
            ReadOnlyCollection<float> waveformVoltages = CONST.WaveformVoltageScaleMSOptions;
            var curWaveformVol = StairMedSettings.Instance.WaveMax;
            var curWaveformVolIndex = waveformVoltages.IndexOf(StairMedSettings.Instance.WaveMax);
            if (e.Delta < 0)
            {
                
                if (curWaveformVolIndex - 1 >= 0)
                {
                    curWaveformVol = waveformVoltages[curWaveformVolIndex - 1];
                }
            }
            else
            {
                if (curWaveformVolIndex + 1 < waveformVoltages.Count)
                {
                    curWaveformVol = waveformVoltages[curWaveformVolIndex + 1];
                }
            }
            StairMedSettings.Instance.WaveMax = curWaveformVol;
#else
            //调整电压范围
            var ratioA = e.Delta < 0 ? 11 : 10;
            var ratioB = e.Delta < 0 ? 10 : 11;
            StairMedSettings.Instance.WaveMax = Convert.ToSingle(Math.Round(StairMedSettings.Instance.WaveMax * ratioA / ratioB));
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateWaveformHeight(MouseWheelEventArgs e)
        {
            //调整高度
            var index = CONST.WaveHeightOptions.IndexOf(StairMedSettings.Instance.WaveformHeight);
            if (index < 0)
            {
                StairMedSettings.Instance.WaveformHeight = CONST.WaveHeightOptions[0];
            }
            else
            {
                index += (e.Delta > 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.WaveHeightOptions.Count - 1, index));
                StairMedSettings.Instance.WaveformHeight = CONST.WaveHeightOptions[index];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateSpikeSumMax(MouseWheelEventArgs e)
        {
            //调整电压范围
            var ratioA = e.Delta < 0 ? 11 : 10;
            var ratioB = e.Delta < 0 ? 10 : 11;
            StairMedSettings.Instance.SpikeSumMax = Convert.ToSingle(Math.Round(StairMedSettings.Instance.SpikeSumMax * ratioA / ratioB));
        }
    }
}
