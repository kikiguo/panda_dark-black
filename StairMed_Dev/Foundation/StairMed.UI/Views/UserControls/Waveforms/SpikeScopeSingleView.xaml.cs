﻿using StairMed.Controls.Controls.NeuralCharts.SpikeScopeCharts;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StairMed.UI.Views.UserControls.Waveforms
{
    /// <summary>
    /// Interaction logic for SpikeScopeSingleView.xaml
    /// </summary>
    public partial class SpikeScopeSingleView : UserControl
    {
        public SpikeScopeSingleView()
        {
            InitializeComponent();
            test = this.spcha;
        }

        public SpikeScopeChart test;

        private void SpikeScopeChart_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateThreshold(e);
            }
            else
            {
                UpdateUVScale(e);
            }

            //
            e.Handled = true;
      
        }

        /// <summary>
        /// 调整电压范围
        /// </summary>
        /// <param name="e"></param>
        private void UpdateUVScale(MouseWheelEventArgs e)
        {
            var index = CONST.SpikeScopeVoltageScaleMSOptions.IndexOf(StairMedSettings.Instance.SpikeScopeVoltageRange);
            if (index < 0)
            {
                StairMedSettings.Instance.SpikeScopeVoltageRange = CONST.SpikeScopeVoltageScaleMSOptions[0];
            }
            else
            {
                index += (e.Delta < 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.SpikeScopeVoltageScaleMSOptions.Count - 1, index));
                StairMedSettings.Instance.SpikeScopeVoltageRange = CONST.SpikeScopeVoltageScaleMSOptions[index];
            }
        }

        /// <summary>
        /// 调整阈值
        /// </summary>
        /// <param name="e"></param>
        private void UpdateThreshold(MouseWheelEventArgs e)
        {
            //var threshold = this.numThreshold.Value;
            //threshold += (e.Delta > 0 ? 1 : -1);
            //this.numThreshold.Value = threshold;
        }
    }
}
