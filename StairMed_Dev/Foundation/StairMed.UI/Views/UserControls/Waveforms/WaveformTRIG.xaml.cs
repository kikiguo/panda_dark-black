﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Entitys.DisplayDatas;
using StairMed.UI.Logger;
using StairMed.UI.Views.Windows.Analyses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Windows.Controls;
using System.Windows.Input;

namespace StairMed.UI.Views.UserControls.Waveforms
{
    /// <summary>
    /// WaveformTRIG.xaml 的交互逻辑
    /// </summary>
    public partial class WaveformTRIG : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        private HashSet<int> _displayChannels = new HashSet<int>();
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        /// <summary>
        /// 
        /// </summary>
        public WaveformTRIG()
        {
            InitializeComponent();

         
            Settings.OnSettingChanged -= Settings_OnSettingChanged;
            Settings.OnSettingChanged += Settings_OnSettingChanged;
            //
            EventHelper.Subscribe<FilterDisplayChannelsEvent, List<int>>((channels) =>
            {
                var oldChannels = _displayChannels.ToHashSet();
                var newChannels = channels.ToHashSet();
                if (oldChannels.SetEquals(newChannels))
                {
                    return;
                }

                //
                _displayChannels = newChannels;
           
            }, Prism.Events.ThreadOption.UIThread);

        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
        }


        protected void Settings_OnSettingChanged(HashSet<string> changes)
        {

            UIHelper.Invoke(() =>
            {

                this.container.ScrollToVerticalOffset(Settings.WaveHeight * (Settings.WaveformDisplayTRIGColumn-2));

            });


        }


        NeuralChartBase test;
        private void InitializeChartFramework(NeuralChartBase chart)
        {
            test= chart;
            chart.Height = 50;
            chart.Background = System.Windows.Media.Brushes.LightGray;
        }


        /// <summary>
        /// 波形高度变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WaveChart_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateWaveHeight(e);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateWaveMax(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 时间范围变化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimeAxis_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl) || Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateDisplayWindowMS(e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpikeSumChart_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateWaveHeight(e);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                UpdateSpikeSumMax(e);
                e.Handled = true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateDisplayWindowMS(MouseWheelEventArgs e)
        {
            //调试视窗长度
            var index = CONST.DisplayWindowMSOptions.IndexOf(StairMedSettings.Instance.DisplayWindowMS);
            if (index < 0)
            {
                StairMedSettings.Instance.DisplayWindowMS = CONST.DisplayWindowMSOptions[0];
            }
            else
            {
                index += (e.Delta < 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.DisplayWindowMSOptions.Count - 1, index));
                StairMedSettings.Instance.DisplayWindowMS = CONST.DisplayWindowMSOptions[index];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateWaveMax(MouseWheelEventArgs e)
        {
            //调整电压范围
#if true
            ReadOnlyCollection<float> waveformVoltages = CONST.WaveformVoltageScaleMSOptions;
            var curWaveformVol = StairMedSettings.Instance.WaveMax;
            var curWaveformVolIndex = waveformVoltages.IndexOf(StairMedSettings.Instance.WaveMax);
            if (e.Delta < 0)
            {

                if (curWaveformVolIndex - 1 >= 0)
                {
                    curWaveformVol = waveformVoltages[curWaveformVolIndex - 1];
                }
            }
            else
            {
                if (curWaveformVolIndex + 1 < waveformVoltages.Count)
                {
                    curWaveformVol = waveformVoltages[curWaveformVolIndex + 1];
                }
            }
            StairMedSettings.Instance.WaveMax = curWaveformVol;
#else
            var ratioA = e.Delta < 0 ? 11 : 10;
            var ratioB = e.Delta < 0 ? 10 : 11;
            StairMedSettings.Instance.WaveMax = Convert.ToSingle(Math.Round(StairMedSettings.Instance.WaveMax * ratioA / ratioB));
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateWaveHeight(MouseWheelEventArgs e)
        {
            //调整高度
            var index = CONST.WaveHeightOptions.IndexOf(StairMedSettings.Instance.WaveHeight);
            if (index < 0)
            {
                StairMedSettings.Instance.WaveHeight = CONST.WaveHeightOptions[0];
            }
            else
            {
                index += (e.Delta > 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.WaveHeightOptions.Count - 1, index));
                StairMedSettings.Instance.WaveHeight = CONST.WaveHeightOptions[index];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdateSpikeSumMax(MouseWheelEventArgs e)
        {
            //调整电压范围
            var ratioA = e.Delta < 0 ? 11 : 10;
            var ratioB = e.Delta < 0 ? 10 : 11;
            StairMedSettings.Instance.SpikeSumMax = Convert.ToSingle(Math.Round(StairMedSettings.Instance.SpikeSumMax * ratioA / ratioB));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NeuralChart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is NeuralChartBase neuralChart)
            {
                try
                {
                    var inputChannel = neuralChart.Channel;
                    var physicsChannel = ReadonlyCONST.ConvertToPhysicsChannel(inputChannel);
                    if (StairMedSettings.Instance.ISIChannelLockToClick)
                    {
                        StairMedSettings.Instance.ISIChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.PSTHChannelLockToClick)
                    {
                        StairMedSettings.Instance.PSTHChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.WaveformChannelLockToClick)
                    {
                        StairMedSettings.Instance.WaveformChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.SpectrumChannelLockToClick)
                    {
                        StairMedSettings.Instance.SpecturmChannel = physicsChannel;
                    }
                    if (StairMedSettings.Instance.SpikeScopeChannelLockToClick)
                    {
                        StairMedSettings.Instance.SpikeScopeChannel = physicsChannel;
                    }
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{nameof(WaveformTRIG)}, {nameof(NeuralChart_MouseLeftButtonDown)}", ex);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NeuralChart_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender is NeuralChartBase neuralChart)
            {
                try
                {
                    var inputChannel = neuralChart.Channel;
                    var physicsChannel = ReadonlyCONST.ConvertToPhysicsChannel(inputChannel);
                    StairMedSettings.Instance.WaveformChannel = physicsChannel;
                    WaveformWnd.PopUp();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{nameof(WaveformTRIG)}, {nameof(NeuralChart_MouseDoubleClick)}", ex);
                }
            }
        }
    }
}

