﻿using Microsoft.VisualBasic;
using Prism.Commands;
using StairMed.Array;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Recorder;
using StairMed.Entity.Infos.Bases;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Views.Windows.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StairMed.UI.ViewModels.UserControls.Waveforms
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class WaveformVMBase : RegionViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public const string TAG = nameof(WaveformVMBase);

        /// <summary>
        /// 
        /// </summary>
        private readonly object lockObj = new object();

        /// <summary>
        /// 采样率
        /// </summary>
        private ReadOnlyCollection<int> _sampleRates = ReadonlyCONST.SampleRates;
        public ReadOnlyCollection<int> SampleRates
        {
            get { return _sampleRates; }
            set { Set(ref _sampleRates, value); }
        }

        /// <summary>
        /// 所选设备的ID
        /// </summary>
        private long _deviceId = -1;
        public long DeviceId
        {
            get { return _deviceId; }
            set
            {
                Set(ref _deviceId, value);
                WaveformViewModel.DeviceId = value;
            }
        }

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        public abstract CollectParamInfo ChartDataParam { get; set; }

        /// <summary>
        /// 设备设置的参数
        /// </summary>
        public abstract CollectParamInfo DeviceParam { get; set; }

        /// <summary>
        /// 是否需要在开始采集后，对界面中显示的通道进行过滤刷新
        /// </summary>
        public virtual bool NeedFilter => true;

        /// <summary>
        /// 参数配置
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 参数配置
        /// </summary>
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;

        /// <summary>
        /// 数据采集器
        /// </summary>
        public NeuralDataCollector NeuralDataCollector { get; set; } = NeuralDataCollector.Instance;

        /// <summary>
        /// 数据录制器
        /// </summary>
        public NeuralDataRecorder NeuralDataRecorder { get; set; } = NeuralDataRecorder.Instance;

        /// <summary>
        /// 光栅数据控制器
        /// </summary>
        protected WaveformViewModel _waveformViewModel = new WaveformViewModel();
        public WaveformViewModel WaveformViewModel
        {
            get { return _waveformViewModel; }
            set { Set(ref _waveformViewModel, value); }
        }

        protected WaveformTRIGModel _waveformTRIGModel = new WaveformTRIGModel();
        public WaveformTRIGModel WaveformTRIGModel
        {
            get { return _waveformTRIGModel; }
            set { Set(ref _waveformTRIGModel, value); }
        }


        /// <summary>
        /// spike scope 数据
        /// </summary>
        protected SpikeScopeViewModel _spikeScopeViewModel = new SpikeScopeViewModel();
        public SpikeScopeViewModel SpikeScopeViewModel
        {
            get { return _spikeScopeViewModel; }
            set { Set(ref _spikeScopeViewModel, value); }
        }




        private WaveformSingleViewModel _waveformSingleViewModel = new WaveformSingleViewModel();

        public WaveformSingleViewModel WaveformSingleViewModel
        {
            get { return _waveformSingleViewModel; }
            set { Set(ref _waveformSingleViewModel, value); }
        }

        private SpikeScopeSingleViewModel _spikeScopeSingleViewModel = new SpikeScopeSingleViewModel();

        public SpikeScopeSingleViewModel SpikeScopeSingleViewModel
        {
            get { return _spikeScopeSingleViewModel; }
            set { Set(ref _spikeScopeSingleViewModel, value); }
        }


        /// <summary>
        /// 数据显示列数
        /// </summary>
        private List<int> _displayColumns;
        public List<int> DisplayColumns
        {
            get { return _displayColumns; }
            set { Set(ref _displayColumns, value); }
        }


       
        /// <summary>
        /// 调整配置时，动态刷新波形
        /// </summary>
        private readonly HashSet<string> CareSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.WaveformDisplayColumn),
        };


        /// <summary>
        /// 调整配置时，TRIG动态刷新波形
        /// </summary>
        private readonly HashSet<string> TRIGCareSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.WaveformDisplayTRIGColumn),
        };


        /* PooledArray<float> trigs = new PooledArray<float>(sampleCount); // 假设sampleCount是trigs的长度*/


        /// <summary>
        /// 
        /// </summary>
        public WaveformVMBase()
        {
            DisplayColumns = Enumerable.Range(0, Settings.WaveformDisplayColumn).ToList();
            Settings.OnSettingChanged += (changes) =>
            {
                if (CareSettings.Overlaps(changes))
                {

                    DisplayColumns = Enumerable.Range(0, Settings.WaveformDisplayColumn).ToList(); 

                    /* T02.DataContext = trigs;
                     T03.DataContext = trigs;*/
                }

               
            };

        }

        /// <summary>
        /// 初始化波形显示界面
        /// </summary>
        protected void InitWaveformUI(long deviceId, List<int> physicsChannels)
        {
            UIHelper.BeginInvoke(() =>
            {
                Task.Run(() =>
                {
                    _waveformViewModel.InitUIDataAsync(deviceId, physicsChannels);
                    _spikeScopeViewModel.InitUIDataAsync(deviceId, physicsChannels);
                    _spikeScopeSingleViewModel.InitUIDataAsync(deviceId, physicsChannels);
                });
            });
        }

        #region 命令绑定

        /// <summary>
        /// 开始采集
        /// </summary>
        public ICommand StartCollect => new DelegateCommand(() =>
        {
            StartCollectHandler();
        });

        protected virtual void CollectPreParation()
        { 
        
        }

        protected void StartCollectHandler(int startIndex = 0)
        {
            CollectPreParation();

            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);



            if (DeviceId < 0)
            {
                return;
            }

            //
            if (DeviceParam.CollectChannelCount <= 0)
            {
                Toast.Warn($"未选择通道");
                return;
            }
            //
            var succeed = false;
            Loading.Show(() =>
            {
                succeed = StartCollectImpl();
            }, () =>
            {
                if (!succeed)
                {
                    return;
                }

                //
                if (DeviceParam.CollectType == CollectMode.Spike)
                {
                    Settings.DisplayRaw = false;
                    Settings.DisplayWFWave = false;
                    Settings.DisplayHFWave = false;
                    Settings.DisplayLFWave = false;
                    Settings.DisplaySpike = true;
                }
                else
                {
                    if (!Settings.DisplayRaw && !Settings.DisplayWFWave && !Settings.DisplayHFWave && !Settings.DisplayLFWave)
                    {
                        Settings.DisplayRaw = true;
                        Settings.DisplayWFWave = false;
                        Settings.DisplayHFWave = false;
                        Settings.DisplayLFWave = false;
                        Settings.DisplaySpike = false;
                        Settings.DisplaySpikeSum = false;
                    }
                }

                //
                if (NeedFilter)
                {
                    EventHelper.Publish<FilterDisplayChannelsEvent, List<int>>(DeviceParam.OrderedInputChannels.ToList());
                }
                ChartDataParam = NeuralDataCollector.GetCollectingDataParam();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected abstract bool StartCollectImpl();

        /// <summary>
        /// 
        /// </summary>
        protected abstract bool StopCollectImpl();

        /// <summary>
        /// 停止采集
        /// </summary>
        public ICommand StopCollect => new DelegateCommand(() =>
        {
            var succeed = false;
            Loading.Show(() =>
            {
                succeed = StopCollectImpl();
            }, () =>
            {
                if (succeed)
                {
                    Task.Run(() =>
                    {
                        Thread.Sleep(200);
                        Toast.Info($"采集数据个数：{NeuralDataCollector.RecvdLength:N0}");

                    });
                }
                else
                {
                    Toast.Error("停止采样失败");
                }
            });
        });

        /// <summary>
        /// 录制：保存数据至本地文件
        /// </summary>
        public ICommand StartRecord => new DelegateCommand(() =>
        {
            if (ToolMix.HasChinese(SolidSettings.NeuralDataSaveFolder))
            {
                Toast.Warn("采集数据存储路径中不能包含中文");
                UIHelper.BeginInvoke(() =>
                {
                    SettingWnd.PopUp();
                });
                return;
            }

            //
            if (!Directory.Exists(SolidSettings.NeuralDataSaveFolder))
            {
                try
                {
                    Directory.CreateDirectory(SolidSettings.NeuralDataSaveFolder);
                }
                catch (Exception ex)
                {
                    Logger.LogTool.Logger.Exp(nameof(StartRecord), ex);
                    Toast.Error($"{ex.Message}[{SolidSettings.NeuralDataSaveFolder}]");
                    return;
                }
            }

            //
            NeuralDataRecorder.Instance.StartRecording();
        });

        /// <summary>
        /// 录制：保存数据至本地文件
        /// </summary>
        public ICommand StopRecord => new DelegateCommand(() =>
        {
            NeuralDataRecorder.Instance.StopRecording();
        });

        /// <summary>
        /// 切换采样类型
        /// </summary>
        public virtual ICommand SwitchCollectType => new DelegateCommand<object>((obj) =>
        {
            DeviceParam.CollectType = (CollectMode)obj;
        });

        #endregion

    }
}
