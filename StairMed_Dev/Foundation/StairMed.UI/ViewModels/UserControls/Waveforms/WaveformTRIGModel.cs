﻿using StairMed.Core.Settings;
using StairMed.MVVM.Base;
using StairMed.Tools;
using StairMed.UI.Entitys.DisplayDatas;
using System;
using System.Collections.Generic;

namespace StairMed.UI.ViewModels.UserControls.Waveforms
{
    /// <summary>
    /// 
    /// </summary>
    public class WaveformTRIGModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public const string TAG = nameof(WaveformTRIGModel);

        #region 绑定数据

        /// <summary>
        /// 开始采集时间
        /// </summary>
        private DateTime _startCollectTime = DateTime.Now.AddDays(-10);
        public DateTime StartCollectTime
        {
            get { return _startCollectTime; }
            set { Set(ref _startCollectTime, value); }
        }

        /// <summary>
        /// 波形数据
        /// </summary>
        private DeviceWaveformData _waveformData = new DeviceWaveformData();
        public DeviceWaveformData WaveformDatas
        {
            get { return _waveformData; }
            set { Set(ref _waveformData, value); }
        }

        /// <summary>
        /// sweep模式下x的位置
        /// </summary>
        private float _sweepX = -1;
        public float SweepX
        {
            get { return _sweepX; }
            set { Set(ref _sweepX, value); }
        }

        #endregion

        /// <summary>
        /// 公共配置：显示设置、参数设置
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 公共配置：显示设置、参数设置
        /// </summary>
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public long DeviceId { get; set; } = -1;

        #region 公开接口

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicsChannels"></param>
        public void InitUIDataAsync(long deviceId, List<int> physicsChannels)
        {
            UIHelper.Invoke(() =>
            {
                WaveformDatas.Update(deviceId, physicsChannels);
            });
        }

        #endregion
    }
}
