﻿using HDF5CSharp;
using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.Tools;
using StairMed.UI.Entitys.DisplayDatas.SpikeScopes;
using StairMed.UI.Logger;
using StairMed.UI.ViewModels.Windows.Analyses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Channels;
using System.Windows.Input;

namespace StairMed.UI.ViewModels.UserControls.Waveforms
{
    public class SpikeScopeSingleViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private double _thresholdUV = -70;
        public double ThresholdUV
        {
            get
            {
                if (SpikeScopes.Count == 0)
                    return _thresholdUV;
                else
                    return SpikeScopes[InputChannel].ThresholdUV;
            }

            set
            {
                lock (SpikeScopes)
                {
                    SpikeScopes[InputChannel].ThresholdUV = value;
                    if (Set(ref _thresholdUV, value))
                    {
                        SpikeThresholdCenter.Instance.SetThreshold(InputChannel, SpikeScopes[InputChannel].ThresholdUV);
                        updataSpikeScopes();
                    }
                }

            }
        }

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = 0;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 信号均方差
        /// </summary>
        private double _rms = 0.0;
        public double RMS
        {
            get { return _rms; }
            set { Set(ref _rms, value); }
        }

        /// <summary>
        /// spike发放频率
        /// </summary>
        private double _spikeRate = 0.0;
        public double SpikeRate
        {
            get { return _spikeRate; }
            set { Set(ref _spikeRate, value); }
        }

        /// <summary>
        /// 是否快照
        /// </summary>
        private bool _takeSnapshot = false;
        public bool TakeSnapshot
        {
            get { return _takeSnapshot; }
            set { Set(ref _takeSnapshot, value); }
        }
        /// <summary>
        /// 所有通道的数据
        /// </summary>
        private ObservableCollection<ChannelSpikeScopeData> _spikeScopes = new ObservableCollection<ChannelSpikeScopeData>();
        public ObservableCollection<ChannelSpikeScopeData> SpikeScopes
        {
            get { return _spikeScopes; }
            set { Set(ref _spikeScopes, value); }
        }
        /// <summary>
        /// 清理快照
        /// </summary>
        private bool _clearSnapshot = false;
        public bool ClearSnapshot
        {
            get { return _clearSnapshot; }
            set { Set(ref _clearSnapshot, value); }
        }

        /// <summary>
        /// 清理scope
        /// </summary>
        private bool _clearScope = false;
        public bool ClearScope
        {
            get { return _clearScope; }
            set { Set(ref _clearScope, value); }
        }


        /// <summary>
        /// 打印日志用于记录个数
        /// 
        /// </summary>
        private static int _index = 0;

        /// <summary>
        /// 
        /// </summary>

        public SpikeScopeSingleViewModel()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name}, index:{_index++}");

            //
            CareSettings.UnionWith(new HashSet<string>
            {
                nameof(StairMedSettings.Instance.SpikeScopeChannel),
            });
            Settings.OnSettingChanged -= Settings_OnSettingChanged;
            Settings.OnSettingChanged += Settings_OnSettingChanged;
            //
            RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
            RMSCenter.Instance.HfpRmsUpdated += Instance_HfpRmsUpdated;
            RMSCenter.Instance.RegisterInputChannel(InputChannel);

            //
            SpikeRateCenter.Instance.SpikeRateUpdated -= Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.SpikeRateUpdated += Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.RegisterSpikeRate(InputChannel);

            //
            //   SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
            //  SpikeThresholdCenter.Instance.OnThresholdChanged += Instance_OnThresholdChanged;
        }

        /// <summary>
        /// 阈值发生变化
        /// </summary>
        private void Instance_OnThresholdChanged()
        {
            ThresholdUV = SpikeThresholdCenter.Instance.GetThreshold(InputChannel);
        }

        /// <summary>
        /// Spike发放率更新
        /// </summary>
        /// <param name="dict"></param>
        private void Instance_SpikeRateUpdated(int channel, double rate)
        {

            if (channel == InputChannel)
            {
                SpikeRate = rate;
            }
        }

        /// <summary>
        /// HFP信号的RMS更新
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="rms"></param>
        private void Instance_HfpRmsUpdated(int channel, double rms)
        {
            if (StairMedSettings.Instance.SpikeScopeEnable)
            {
                OnViewClose();
                return;
            }

            if (channel == InputChannel)
            {
                RMS = rms;
            }

        }


        public List<bool> _spikeDisableds = new List<bool>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        protected void Settings_OnSettingChanged(HashSet<string> changes)
        {
            if (InputChannel >= 0 && InputChannel < SpikeScopes.Count)
            {
                RMSCenter.Instance.UnregisterInputChannel(InputChannel);
                SpikeRateCenter.Instance.UnregisterSpikeRate(InputChannel);
            }

            // 检查是否改变了当前的输入通道
            if (InputChannel != Settings.SpikeScopeChannel &&
                Settings.SpikeScopeChannel >= 0 &&
                Settings.SpikeScopeChannel < SpikeScopes.Count)
            {
                InputChannel = Settings.SpikeScopeChannel;
                ReloadSettings(changes);
                return;
            }

            // 如果新的输入通道与当前的不同
            if (InputChannel != Settings.SpikeScopeChannel)
            {
                InputChannel = Settings.SpikeScopeChannel;

                if (InputChannel >= 0 && InputChannel < SpikeScopes.Count)
                {
                    UpdateChannelFromSettings(SpikeScopes[InputChannel]);
                    return;
                }
            }

            // 根据是否选择了所有通道来更新设置
            if (Settings.SpikeScopeAllChannel)
            {
                UpdateAllChannels(); // 更新所有通道
            }
            else
            {
                UpdateSingleChannel(InputChannel); // 只更新当前选定的通道
            }

            // 再次注册输入通道
            RMSCenter.Instance.RegisterInputChannel(InputChannel);
            SpikeRateCenter.Instance.RegisterSpikeRate(InputChannel);
        
        }

        private void UpdateChannelFromSettings(ChannelSpikeScopeData scope)
        {
            // 更新通道的各种设置
            scope.Enable = Settings.SpikeScopeEnable;
            scope.TimeScaleMS = Settings.SpikeScopeTimeMS;
            scope.ThresholdUV = Settings.Threshold;
            scope.SpikeScopeReserved = Settings.SpikeScopeReserved;
            scope.SpikeScopeVoltageRange = Settings.SpikeScopeVoltageRange;
            scope.Gain = Settings.SpikeScopeGain;
            scope.Grid = Settings.IsShowSpikeScopeGrid;
        }

        private void updataSpikeScopes()
        {
            if (Settings.SpikeScopeAllChannel)
            {
                UpdateAllChannels();
            }
            else
            {
                UpdateSingleChannel(InputChannel);
            }
      


        }

        private void UpdateAllChannels()
        {
            foreach (var scope in SpikeScopes)
            {
                UpdateChannelSettings(scope);
            }
        }
        private void UpdateSingleChannel(int channelIndex)
        {
            if (channelIndex >= 0 && channelIndex < SpikeScopes.Count)
            {
                UpdateChannelSettings(SpikeScopes[channelIndex]);
            }
        }

        private void UpdateChannelSettings(ChannelSpikeScopeData scope)
        {
          
            scope.ThresholdUV = _thresholdUV;
            SpikeThresholdCenter.Instance.SetThreshold(scope.InputIndex, _thresholdUV);
        }


        private void EnableCleanChannel()
        {
            StairMedSettings.Instance.IsClean = true;
            DateTime now = DateTime.Now;
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long timestamp = (long)(now.ToUniversalTime() - epoch).TotalMilliseconds;
            StairMedSettings.Instance.cleanTiem = timestamp + 2000;
        }


        /// <summary>
        /// 
        /// </summary>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            OnViewClose();
            SpikeRateCenter.Instance.UnregisterSpikeRate(InputChannel);
            RMSCenter.Instance.UnregisterInputChannel(InputChannel);
            InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.SpikeScopeChannel);
            SpikeRateCenter.Instance.RegisterSpikeRate(InputChannel);
            RMSCenter.Instance.RegisterInputChannel(InputChannel);

            //
            if (InputChannel < 128)
                ThresholdUV = SpikeScopes[InputChannel].ThresholdUV;


            Settings.OnSettingChanged -= Settings_OnSettingChanged;
            Settings.OnSettingChanged += Settings_OnSettingChanged;
            //
            RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
            RMSCenter.Instance.HfpRmsUpdated += Instance_HfpRmsUpdated;
            RMSCenter.Instance.RegisterInputChannel(InputChannel);

            //
            SpikeRateCenter.Instance.SpikeRateUpdated -= Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.SpikeRateUpdated += Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.RegisterSpikeRate(InputChannel);

            //
            //SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
            //SpikeThresholdCenter.Instance.OnThresholdChanged += Instance_OnThresholdChanged;
        }

        protected void ReloadAllchannels(int changes)
        {
            OnViewClose();
            SpikeRateCenter.Instance.UnregisterSpikeRate(changes);
            RMSCenter.Instance.UnregisterInputChannel(changes);
            SpikeRateCenter.Instance.RegisterSpikeRate(changes);
            RMSCenter.Instance.RegisterInputChannel(changes);

            //

            Settings.OnSettingChanged -= Settings_OnSettingChanged;
            Settings.OnSettingChanged += Settings_OnSettingChanged;
            //
            RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
            RMSCenter.Instance.HfpRmsUpdated += Instance_HfpRmsUpdated;
            RMSCenter.Instance.RegisterInputChannel(changes);

            //
            SpikeRateCenter.Instance.SpikeRateUpdated -= Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.SpikeRateUpdated += Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.RegisterSpikeRate(changes);

            //
            //SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
            //SpikeThresholdCenter.Instance.OnThresholdChanged += Instance_OnThresholdChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
            RMSCenter.Instance.UnregisterInputChannel(InputChannel);

            //
            SpikeRateCenter.Instance.SpikeRateUpdated -= Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.UnregisterSpikeRate(InputChannel);

            //
            //  SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
        }

        /// <summary>
        /// 清空
        /// </summary>
        public ICommand ClearScopeCommand => new DelegateCommand(() =>
        {
            ClearScope = true;
            ClearScope = false;
        });

        /// <summary>
        /// 快照
        /// </summary>
        public ICommand TakeSnapshotCommand => new DelegateCommand(() =>
        {
            TakeSnapshot = true;
            TakeSnapshot = false;
        });

        /// <summary>
        /// 清理快照
        /// </summary>
        public ICommand ClearSnapshotCommand => new DelegateCommand(() =>
        {
            ClearSnapshot = true;
            ClearSnapshot = false;
        });

        /// <summary>
        /// 初始化界面数据
        /// </summary>
        /// <param name="physicsChannels"></param>
        public void InitUIDataAsync(long deviceId, List<int> physicsChannels)
        {
            UIHelper.Invoke(() =>
            {
                if (SpikeScopes.Count > 1) return;
                for (int i = 0; i < 1200; i++)
                {
                    _spikeDisableds.Add(false);
                    SpikeScopes.Add(new ChannelSpikeScopeData(deviceId, i));

                }


            });
        }
    }
}
