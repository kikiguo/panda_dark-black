﻿using StairMed.Core.Settings;
using StairMed.MVVM.Base;
using StairMed.Tools;
using StairMed.UI.Entitys.DisplayDatas.SpikeScopes;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace StairMed.UI.ViewModels.UserControls.Waveforms
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeScopeViewModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public const string TAG = nameof(SpikeScopeViewModel);

        /// <summary>
        /// 所有通道的数据
        /// </summary>
        private ObservableCollection<ChannelSpikeScopeData> _spikeScopes = new ObservableCollection<ChannelSpikeScopeData>();
        public ObservableCollection<ChannelSpikeScopeData> SpikeScopes
        {
            get { return _spikeScopes; }
            set { Set(ref _spikeScopes, value); }
        }

        /// <summary>
        /// 公共配置：显示设置、参数设置
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 初始化界面数据
        /// </summary>
        /// <param name="physicsChannels"></param>
        public void InitUIDataAsync(long deviceId, List<int> physicsChannels)
        {
            UIHelper.Invoke(() =>
            {
                if (SpikeScopes.Count == physicsChannels.Count)
                {
                    for (int i = 0; i < physicsChannels.Count; i++)
                    {
                        SpikeScopes[i].Update(deviceId, physicsChannels[i]);
                    }
                }
                else if (SpikeScopes.Count < physicsChannels.Count)
                {
                    for (int i = 0; i < SpikeScopes.Count; i++)
                    {
                        SpikeScopes[i].Update(deviceId, physicsChannels[i]);
                    }

                    //
                    for (int i = SpikeScopes.Count; i < physicsChannels.Count; i++)
                    {
                        SpikeScopes.Add(new ChannelSpikeScopeData(deviceId, physicsChannels[i]));
                    }
                }
                else
                {
                    for (int i = 0; i < physicsChannels.Count; i++)
                    {
                        SpikeScopes[i].Update(deviceId, physicsChannels[i]);
                    }

                    //
                    while (SpikeScopes.Count > physicsChannels.Count)
                    {
                        SpikeScopes.RemoveAt(physicsChannels.Count);
                    }
                }
            });
        }
    }
}
