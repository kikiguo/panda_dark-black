﻿using Prism.Commands;
using StairMed.Core.Settings;
using StairMed.Event;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace StairMed.UI.ViewModels.UserControls.SideBars
{
    internal class FilterSettingsPageViewModel: ViewModelBase
    {
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        public FilterSettingsPageViewModel()
        {
            FilterData = new double[,] {
                { 0.03, 0.012 },
                { 0.04, 0.019 },
                { 0.05, 0.025 },
                { 0.06, 0.043 },
                { 0.07, 0.058 },
                { 0.08, 0.072 },
                { 0.09, 0.085 },
                { 0.10, 0.097 },
                { 0.20, 0.23 },
                { 0.30, 0.34 },
                { 0.40, 0.44 },
                { 0.50, 0.53 },
                { 0.60, 0.60 },
                { 0.70, 0.66 },
                { 0.80, 0.71 },
                { 0.90, 0.76 },
                { 1.00, 0.79 },
                { 2.00, 0.93 },
                { 3.00, 0.97 },
                { 4.00, 0.98 },
                { 5.00, 0.99 },
                { 6.00, 0.99 },
                { 7.00, 0.99 },
                { 9.00, 1.00 },
                { 10.00, 1.00 },
                { 20.00, 1.00 },
                { 30.00, 1.00 },

            }; 

            FilterData2 = new double[,] {
                { 0.03, 0.017 },
                { 0.04, 0.024 },
                { 0.05, 0.03 },
                { 0.06, 0.048 },
                { 0.07, 0.063 },
                { 0.08, 0.077 },
                { 0.09, 0.09 },
                { 0.10, 0.14 },
                { 0.20, 0.27 },
                { 0.30, 0.39 },
                { 0.40, 0.49 },
                { 0.50, 0.58 },
                { 0.60, 0.65 },
                { 0.70, 0.69 },
                { 0.80, 0.76 },
                { 0.90, 0.78 },
                { 1.00, 0.79 },
                { 2.00, 0.93 },
                { 3.00, 0.97 },
                { 4.00, 0.98 },
                { 5.00, 0.99 },
                { 6.00, 0.99 },
                { 7.00, 0.99 },
                { 9.00, 1.00 },
                { 10.00, 1.00 },
                { 20.00, 1.00 },
                { 30.00, 1.00 },

            };
        }
        private double[,] _filterData;

		public double[,] FilterData
		{
			get { return _filterData; }
			set { Set(ref _filterData, value); }
        }
        private double[,] _filterData2;

        public double[,] FilterData2
        {
            get { return _filterData2; }
            set { Set(ref _filterData2, value); }
        }

        private int _value=2;

        public int AA
        {
            get { return _value; }
            set { Set(ref _value, value); }
        }
        public ICommand SampleSaveCommand => new DelegateCommand(() => {
            

        });
        public ICommand SampleApplyCommand => new DelegateCommand(() => {

            
            //close dialog
            EventHelper.Publish<GenericWndCloseEvent, string>("Filter Parameter");
        });
        public ICommand SampleCancelCommand => new DelegateCommand(() => {

            //close dialog
            EventHelper.Publish<GenericWndCloseEvent, string>("Filter Parameter");
        });
    }
}
