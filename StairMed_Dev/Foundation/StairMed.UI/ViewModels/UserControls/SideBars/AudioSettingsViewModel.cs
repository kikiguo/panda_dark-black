﻿using Prism.Commands;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace StairMed.UI.ViewModels.UserControls.SideBars
{
    internal class AudioSettingsViewModel : ViewModelBase
    {
        private string _label="Audio Port";

        public string Label
        {
            get { return _label; }  
            set { Set(ref _label, value); }
        }

        private EnumOutputType _ouputType = EnumOutputType.Spike;

        public EnumOutputType OutputType
        {
            get { return _ouputType; }
            set { Set(ref _ouputType, value); }
        }

        private int _selectedChannel = 0;   

        public int SelectedChannel
        {
            get { return _selectedChannel; }
            set { Set(ref _selectedChannel, value); }
        }


        private int _audioInputStart;

        public int AudioInputStart
        {
            get { return _audioInputStart; }
            set { Set(ref _audioInputStart, value); }
        }

        private int _audioInputEnd;

        public int AudioInputEnd
        {
            get { return _audioInputEnd; }
            set { Set(ref _audioInputEnd, value); }
        }

        private int _audioOutputStart;

        public int AudioOutputStart
        {
            get { return _audioOutputStart; }
            set { Set(ref _audioOutputStart, value); }
        }

        private int _audioOutputEnd;

        public int AudioOutputEnd
        {
            get { return _audioOutputEnd; }
            set { Set(ref _audioOutputEnd, value); }
        }






        private string _label2="Audio Port";

        public string Label2
        {
            get { return _label2; }
            set { Set(ref _label2, value); }
        }

        private EnumOutputType _ouputType2 = EnumOutputType.Spike;

        public EnumOutputType OutputType2
        {
            get { return _ouputType2; }
            set { Set(ref _ouputType2, value); }
        }


        private int _selectedChannel2 = 0;

        public int SelectedChannel2
        {
            get { return _selectedChannel2; }
            set { Set(ref _selectedChannel2, value); }
        }

        private int _audioInputStart2;

        public int AudioInputStart2
        {
            get { return _audioInputStart2; }
            set { Set(ref _audioInputStart2, value); }
        }

        private int _audioInputEnd2;

        public int AudioInputEnd2
        {
            get { return _audioInputEnd2; }
            set { Set(ref _audioInputEnd2, value); }
        }


        private int _audioOutputStart2;

        public int AudioOutputStart2
        {
            get { return _audioOutputStart2; }
            set { Set(ref _audioOutputStart2, value); }
        }

        private int _audioOutputEnd2;

        public int AudioOutputEnd2
        {
            get { return _audioOutputEnd2; }
            set { Set(ref _audioOutputEnd2, value); }
        }


        public ICommand SampleSaveCommand => new DelegateCommand(() => {


        });
        public ICommand SampleApplyCommand => new DelegateCommand(() => {

            
            //close dialog
            EventHelper.Publish<GenericWndCloseEvent, string>("Audio Properties");
        });
        public ICommand SampleCancelCommand => new DelegateCommand(() => {

            //close dialog
            EventHelper.Publish<GenericWndCloseEvent, string>("Audio Properties");
        });
    }
}
