﻿using HDF5CSharp;
using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Xml;

namespace StairMed.UI.ViewModels.UserControls.SideBars
{
    internal class SampleSettingsViewModel: ViewModelBase
    {
        const int GroupChannelCount = 1024;
        Dictionary<int,List<ChanMember>> List_NoneMember;
        Dictionary<int, List<ChanMember>> List_GroupMember;
        public SampleSettingsViewModel()
        {
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;
            UpdateTableControlHeadstages();
            SelectHeadstageIndex = 0;

            EventHelper.Subscribe<HeadstageInsertedEvent>(() => {
                for (int i = 0; i < Headstages.Count; i++)
                {
                    Headstages[i].IsEnable = StairMedSettings.Instance.InsertedHeadstages.Exists(_ => _.StartsWith(Headstages[i].GroupName));
                }
            }, Prism.Events.ThreadOption.UIThread);
        }

        //private ObservableCollection<HeadStage> m_Headstages = new ObservableCollection<HeadStage>();

        //public ObservableCollection<HeadStage> Headstages
        //{
        //    get { return m_Headstages; }
        //    set { m_Headstages = value; }
        //}
        private ObservableCollection<HeadStage> _headstages = new ObservableCollection<HeadStage>();
        public ObservableCollection<HeadStage> Headstages
        {
            get { return _headstages; }
            set { Set(ref _headstages, value); }
        }

        /// <summary>
        /// 单个headstage上多少个通道
        /// </summary>
        private int _singleHeadstageChannels = 10;
        public int SingleHeadstageChannels
        {
            get { return _singleHeadstageChannels; }
            set { Set(ref _singleHeadstageChannels, value); }
        }

        private ObservableCollection<string> _headstageGroupsSerial = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroupsSerial
        {
            get { return _headstageGroupsSerial; }
            set { Set(ref _headstageGroupsSerial, value); }
        }

        /// <summary>
        /// 选择第几个headstage显示
        /// </summary>
        private int _selectHeadstageIndex = -1;
        public int SelectHeadstageIndex
        {
            get { return _selectHeadstageIndex; }
            set
            {
                if (_selectHeadstageIndex != value && (Headstages.Count > value && Headstages[value].IsEnable))
                {
                    Set(ref _selectHeadstageIndex, value);
                    UpdateNonMember(_selectHeadstageIndex);
                }
            }
        }

        private int _selectHeadstageIndexGroup = -1;
        public int SelectHeadstageIndexGroup
        {
            get { return _selectHeadstageIndexGroup; }
            set
            {
                if (_selectHeadstageIndexGroup != value)
                {
                    Set(ref _selectHeadstageIndexGroup, value);
                    UpdateGroupMember(_selectHeadstageIndexGroup);
                }
            }
        }

        private ObservableCollection<ChanMember> _noneMembers=new ObservableCollection<ChanMember>();

        public ObservableCollection<ChanMember> NonMembers
        {
            get { return _noneMembers; }
            set { Set(ref _noneMembers, value); }
        }

        private ObservableCollection<ChanMember> _noneMemberSelected=new ObservableCollection<ChanMember>();

        public ObservableCollection<ChanMember> NoneMemberSelected
        {   
            get { return _noneMemberSelected; }
            set { Set(ref _noneMemberSelected, value); }
        }

        private ObservableCollection<ChanMember> _groupMemberSelected = new ObservableCollection<ChanMember>();

        public ObservableCollection<ChanMember> GroupMemberSelected
        {
            get { return _groupMemberSelected; }
            set { Set(ref _groupMemberSelected, value); }
        }

        private ObservableCollection<ChanMember> _groupMembers = new ObservableCollection<ChanMember>();

        public ObservableCollection<ChanMember> GroupMembers
        {
            get { return _groupMembers; }
            set { Set(ref _groupMembers, value); }
        }

        private ObservableCollection<ChanMember> _groupMembersOptions = new ObservableCollection<ChanMember>();

        public ObservableCollection<ChanMember> GroupMembersOptions
        {
            get { return _groupMembersOptions; }    
            set { Set(ref _groupMembersOptions, value); }
        }


        private EnumSampleType _sampleType;

        public EnumSampleType SampleType
        {
            get { return _sampleType; }
            set { Set(ref _sampleType, value); }
        }
        private int _sampleTypeData = 0;
            
        public int SampleTypeData
        {
            get { return _sampleTypeData; }
            set { Set(ref _sampleTypeData, value); }
        }

        public ICommand MemberAddCommand => new DelegateCommand(() =>
        {
            for (int i = 0; i < NoneMemberSelected.Count; i++)
            {
                NonMembers.Remove(NoneMemberSelected[i]);

                var item = NoneMemberSelected[i];
                item.Status = typeof(EnumSampleType).GetEnumName(SampleTypeData);
                GroupMembers.Add(item);
            }
            if (SelectHeadstageIndexGroup != SelectHeadstageIndex)
                SelectHeadstageIndexGroup = SelectHeadstageIndex;
            else
                UpdateGroupMember(SelectHeadstageIndexGroup);
        });
        public ICommand MemberAddAllCommand => new DelegateCommand(() => {
            NoneMemberSelected.Clear();
            for (int i = 0; i < NonMembers.Count; i++)
            {
                NoneMemberSelected.Add(NonMembers[i]);
            }
            if (NoneMemberSelected.Count > 0)
                MemberAddCommand.Execute(null);
        });
        public ICommand MemberRemoveCommand => new DelegateCommand(() => 
        {
            for (int i = 0; i < GroupMemberSelected.Count; i++)
            {
                GroupMembers.Remove(GroupMemberSelected[i]);
                GroupMembersOptions.Remove(GroupMemberSelected[i]);
            }
            if (SelectHeadstageIndex != SelectHeadstageIndexGroup)
                SelectHeadstageIndex = SelectHeadstageIndexGroup;
            else
                UpdateNonMember(SelectHeadstageIndex);
        });
        public ICommand MemberRemoveAllCommand => new DelegateCommand(() => {
            int groupStartIndex = SelectHeadstageIndexGroup * SingleHeadstageChannels;
            int groupEndIndex = groupStartIndex + SingleHeadstageChannels;
            GroupMemberSelected.Clear();
            GroupMembersOptions.Clear();
            for (int i = 0; i < GroupMembers.Count; i++)
            {
                //if (GroupMembers.Max(_ => _.ID) <= groupStartIndex || GroupMembers.Min(_ => _.ID) > groupEndIndex + 1)
                //{
                //    break;
                //}
                var item = GroupMembers[i];
                if (item.ID > groupStartIndex && item.ID <= groupEndIndex)
                {
                    GroupMemberSelected.Add(item);
                }
            }
            if (GroupMemberSelected.Count > 0)
            {
                MemberRemoveCommand.Execute(null);
            }
        });
        public ICommand SampleSaveCommand => new DelegateCommand(() => {
            for (int i = 0; i < GroupMembers.Count; i++)
            {

            }
        
        });
        public ICommand SampleApplyCommand => new DelegateCommand(() => {

            for (int i = 0; i < GroupMembers.Count; i++)
            {

            }
            //close dialog
            EventHelper.Publish<GenericWndCloseEvent, string>("Sample Properties");
        });
        public ICommand SampleCancelCommand=> new DelegateCommand(() => {

            //close dialog
            EventHelper.Publish<GenericWndCloseEvent, string>("Sample Properties");
        });

        private void UpdateTableControlHeadstages()
        {
            if (Headstages.Count == 0)
            {
                for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                {
                    //Headstages.Add($"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                    Headstages.Add(new HeadStage
                    {
                        GroupName=$"{(char)('A' + i)}",
                        Name = $"{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}",
                        //IsEnable = StairMedSettings.Instance.InsertedHeadstages.Exists(_=>_.StartsWith((char)('A' + i))),
                        IsEnable=true,
                    });
                    HeadstageGroupsSerial.Add($"{(i + 1).ToString()}");
                }
            }
        }

        private void UpdateNonMember(int selectedHeadstageIndex)
        {
            int startChanIndex = selectedHeadstageIndex * SingleHeadstageChannels;
            if (startChanIndex > -1)
            {
                NonMembers.Clear();
                for (int i = startChanIndex; i < startChanIndex + SingleHeadstageChannels; i++)
                {
                    if (!GroupMembers.ToList().Exists(_ => _.ID.Equals(i + 1)))
                        NonMembers.Add(new ChanMember { ID = i + 1, Name = $"Chan{(i + 1).ToString()}", Status = "None" });
                }
            }

        }

        private void UpdateGroupMember(int selectedHeadstageIndex)
        {
            int groupStartIndex= selectedHeadstageIndex * SingleHeadstageChannels;
            int groupEndIndex = groupStartIndex + SingleHeadstageChannels;
            GroupMembersOptions.Clear();
            for (int i = 0; i < GroupMembers.Count; i++)
            {
                //if (GroupMembers.Max(_ => _.ID) <= groupStartIndex || GroupMembers.Min(_ => _.ID) > groupEndIndex + 1)
                //{
                //    break;
                //}
                var item = GroupMembers[i];
                if (item.ID > groupStartIndex && item.ID <= groupEndIndex)
                {
                    GroupMembersOptions.Add(item);
                }
            }
        }

    }

    internal class ChanMember : ViewModelBase
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { Set(ref _id, value); }
        }

        private string _name = string.Empty;

        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }

        private string _status="None";
            
        public string Status
        {
            get { return _status; }
            set { Set(ref _status, value); }
        }

    }

    internal class HeadStage : ViewModelBase
    {
        private string _groupName;

        public string GroupName
        {
            get { return _groupName; }
            set { Set(ref _groupName, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }

        private bool _isEnable = false;

        public bool IsEnable
        {
            get { return _isEnable; }
            set { Set(ref _isEnable, value); }  
        }

    }
}
