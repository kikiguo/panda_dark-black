﻿using MatFileHandler;
using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers.PSTH;
using StairMed.Tools;
using StairMed.UI.Views.Windows.Analyses;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    internal class PSTHWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly object _lock = new object();

        /// <summary>
        /// 视窗长度
        /// </summary>
        private int _displayWindowMS;
        public int DisplayWindowMS
        {
            get { return _displayWindowMS; }
            set { Set(ref _displayWindowMS, value); }
        }

        /// <summary>
        /// 直方图数据
        /// </summary>
        private List<float> _histograms = new List<float>();
        public List<float> Histograms
        {
            get { return _histograms; }
            set { Set(ref _histograms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<PSTHRaster> _rasters = new ObservableCollection<PSTHRaster>();
        public ObservableCollection<PSTHRaster> Rasters
        {
            get { return _rasters; }
            set { Set(ref _rasters, value); }
        }

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = -1;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 采样率
        /// </summary>
        private int _sampleRate = 10000;
        public int SampleRate
        {
            get { return _sampleRate; }
            set { Set(ref _sampleRate, value); }
        }

        /// <summary>
        /// 触发通道
        /// </summary>
        private int _triggerChannel = -1;
        public int TriggerChannel
        {
            get { return _triggerChannel; }
            set { Set(ref _triggerChannel, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _mouseX = -1;
        public double MouseX
        {
            get { return _mouseX; }
            set { Set(ref _mouseX, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _preMS = 0;
        private int _postMS = 0;

        /// <summary>
        /// 
        /// </summary>
        private bool _saveing = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> DisplayWindowMSSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.PSTHPostTriggerSpanMS),
            nameof(StairMedSettings.Instance.PSTHPreTriggerSpanMS),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> PSTHChannelSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.PSTHChannel),
            nameof(StairMedSettings.Instance.PSTHTriggerChannel),
        };

        /// <summary>
        /// 
        /// </summary>
        public PSTHWndViewModel()
        {
            //
            CareSettings.UnionWith(DisplayWindowMSSettings);
            CareSettings.UnionWith(PSTHChannelSettings);

            //
            PSTHCenter.Instance.HandleNewPSTH -= Instance_HandleNewPSTH;
            PSTHCenter.Instance.HandleNewPSTH += Instance_HandleNewPSTH;

            //
            NeuralDataCollector.Instance.RunningStateChanged -= Instance_RunningStateChanged;
            NeuralDataCollector.Instance.RunningStateChanged += Instance_RunningStateChanged;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iscollecting"></param>
        private void Instance_RunningStateChanged(bool iscollecting)
        {
            //true表示重新开始采集
            if (iscollecting)
            {
                lock (_lock)
                {
                    Histograms = new List<float>();
                    Rasters.Clear();
                }
            }
        }

        /// <summary>
        /// 接收到新的psth
        /// </summary>
        /// <param name="psthchannel"></param>
        /// <param name="triggerChannel"></param>
        /// <param name="samplerate"></param>
        /// <param name="preMS"></param>
        /// <param name="postMS"></param>
        /// <param name="newRasters"></param>
        private void Instance_HandleNewPSTH(int psthchannel, int triggerChannel, int samplerate, List<PSTHRaster> newRasters)
        {
            if (_saveing)
            {
                return;
            }

            //
            if (psthchannel != InputChannel || triggerChannel != TriggerChannel)
            {
                return;
            }

            lock (_lock)
            {
                //
                SampleRate = samplerate;

                //
                Rasters.AddRange(newRasters);

                //
                var trials = Settings.PSTHTrials;
                while (Rasters.Count > trials)
                {
                    Rasters.RemoveAt(0);
                }

                //
                UpdateHistogram();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="changes"></param>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            base.ReloadSettings(changes);

            //
            if (changes == null || DisplayWindowMSSettings.Overlaps(changes))
            {
                lock (_lock)
                {
                    _preMS = Settings.PSTHPreTriggerSpanMS;
                    _postMS = Settings.PSTHPostTriggerSpanMS;

                    //
                    DisplayWindowMS = _preMS + _postMS;

                    //
                    Histograms = new List<float>();
                    Rasters.Clear();
                }
            }

            //
            if (changes == null || PSTHChannelSettings.Overlaps(changes))
            {
                lock (_lock)
                {
                    //
                    PSTHCenter.Instance.UnregisterPSTHChannel(InputChannel, TriggerChannel);
                    InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.PSTHChannel);
                    TriggerChannel = Settings.PSTHTriggerChannel;
                    PSTHCenter.Instance.RegisterPSTHChannel(InputChannel, TriggerChannel);

                    //
                    Histograms = new List<float>();
                    Rasters.Clear();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            PSTHCenter.Instance.HandleNewPSTH -= Instance_HandleNewPSTH;
            PSTHCenter.Instance.UnregisterPSTHChannel(InputChannel, TriggerChannel);
            NeuralDataCollector.Instance.RunningStateChanged -= Instance_RunningStateChanged;
        }

        /// <summary>
        /// 清空
        /// </summary>
        public ICommand Clean => new DelegateCommand(() =>
        {
            lock (_lock)
            {
                Histograms = new List<float>();
                Rasters.Clear();
            }
        });

        /// <summary>
        /// 清空最后一轮
        /// </summary>
        public ICommand CleanLastTrial => new DelegateCommand(() =>
        {
            lock (_lock)
            {
                if (Rasters.Count > 0)
                {
                    Rasters.RemoveAt(Rasters.Count - 1);
                    UpdateHistogram();
                }
            }
        });

        /// <summary>
        /// 更新直方图
        /// </summary>
        private void UpdateHistogram()
        {
            var binMS = Settings.PSTHBinSizeMS;
            var histDict = new Dictionary<int, int>();

            //
            var columns = DisplayWindowMS / binMS;
            for (int i = 0; i < columns; i++)
            {
                histDict[i] = 0;
            }

            //
            var binStep = binMS * _sampleRate / 1000;
            foreach (var raster in Rasters)
            {
                foreach (var index in raster.Raster)
                {
                    if (index / binStep < columns)
                    {
                        histDict[index / binStep]++;
                    }
                }
            }

            //
            var hist = new List<float>();
            var scaleFactor = 1000.0 / (binMS * Rasters.Count);
            for (int i = 0; i < columns; i++)
            {
                hist.Add((float)scaleFactor * histDict[i]);
            }

            //
            Histograms = hist;
        }

        /// <summary>
        /// 配置保存文件格式
        /// </summary>
        public ICommand ConfigDataSaveFileType => new DelegateCommand(() =>
        {
            UIHelper.Invoke(() => { new DataSaveTypeWnd().ShowDialog(); });
        });

        /// <summary>
        /// 保存
        /// </summary>
        public ICommand Save => new DelegateCommand<object>((obj) =>
        {
            _saveing = true;

            //
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new FolderBrowserDialog
                {
                    Description = "请选择保存文件路径",
                    SelectedPath = CONST.AppInstallPath,
                };

                //
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var destFolder = dialog.SelectedPath;
                    var savePath = Path.Combine(destFolder, $"PSTH_{InputChannel + 1}_{DateTime.Now:yyyyMMdd_HHmmss}");

                    //
                    Loading.Show(() =>
                    {
                        //CSV
                        if (StairMedSettings.Instance.SaveDataToCSVType && Histograms.Count > 0)
                        {
                            SaveToCSV(savePath);
                        }

                        //Matlab
                        if (StairMedSettings.Instance.SaveDataToMatlabType && Rasters != null && Rasters.Count > 0)
                        {
                            SaveToMatlab(savePath);
                        }
                    }, () =>
                    {
                        //涉及到ui控件，只能放在ui线程中调用 //PNG
                        if (StairMedSettings.Instance.SaveDataToPNGType)
                        {
                            SaveToPNG(obj, savePath);
                        }
                        Toast.Info("保存成功");
                    });
                }

                //
                _saveing = false;
            });
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        private static void SaveToPNG(object obj, string path)
        {
            FileHelper.SnapshotUIControl((FrameworkElement)obj, new PngBitmapEncoder(), $"{path}.png");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        private void SaveToMatlab(string path)
        {
            using (var fs = new FileStream($"{path}.mat", FileMode.OpenOrCreate, FileAccess.Write))
            {
                var writer = new MatFileWriter(fs);
                var builder = new DataBuilder();
                var bin_size = builder.NewVariable("bin_size", builder.NewArray(new[] { StairMedSettings.Instance.PSTHBinSizeMS }, 1, 1));
                var num_trials = builder.NewVariable("num_trials", builder.NewArray(new[] { Rasters.Count }, 1, 1));
                var avg_firing_rate = builder.NewVariable("avg_firing_rate", builder.NewArray(Histograms.ToArray(), Histograms.Count, 1));
                var hz = NeuralDataCollector.Instance.GetCollectingSampleRate();
                var sample_rate = builder.NewVariable("sample_rate", builder.NewArray(new[] { hz }, 1, 1));
                var waveform_name = builder.NewVariable("waveform_name", builder.NewCharArray($"PSTH-{StairMedSettings.Instance.ISIChannel}"));

                //
                var t_bins_datas = new int[Histograms.Count];
                for (int i = 0; i < t_bins_datas.Length; i++)
                {
                    t_bins_datas[i] = i * StairMedSettings.Instance.PSTHBinSizeMS - StairMedSettings.Instance.PSTHPreTriggerSpanMS;
                }
                var t_bins = builder.NewVariable("t_bins", builder.NewArray(t_bins_datas, t_bins_datas.Length, 1));

                //
                var spikeCount = Rasters.Sum(r => r == null ? 0 : r.Raster.Count);
                var waveformData = new int[spikeCount * 3];
                var index = 0;
                for (int i = 0; i < Rasters.Count; i++)
                {
                    var raster = Rasters[i];
                    for (int j = 0; j < raster.Raster.Count; j++)
                    {
                        waveformData[index] = i;
                        waveformData[index + spikeCount] = raster.Raster[j];
                        waveformData[index + spikeCount * 2] = 1;

                        //
                        index += 1;
                    }
                }
                var waveforms = builder.NewVariable("waveforms", builder.NewArray(waveformData, waveformData.Length / 3, 3));

                //                            
                var min = -StairMedSettings.Instance.PSTHPreTriggerSpanMS * hz / 1000;
                var max = StairMedSettings.Instance.PSTHPostTriggerSpanMS * hz / 1000;
                var t_waveform_datas = Enumerable.Range(min, max - min).ToArray();
                var t_waveform = builder.NewVariable("t_waveforms", builder.NewArray(t_waveform_datas, t_waveform_datas.Length, 1));

                //
                var trails_datas = Enumerable.Range(1, Rasters.Count).ToArray();
                var trails = builder.NewVariable("trails", builder.NewArray(trails_datas, trails_datas.Length, 1));

                //
                var mainData = builder.NewFile(new[] { bin_size, num_trials, avg_firing_rate, sample_rate, waveform_name, t_bins, waveforms, t_waveform, trails });
                writer.Write(mainData);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        private void SaveToCSV(string path)
        {
            //
            var lines = new List<string>();

            //headLine
            {
                var line = new List<string> { "PSTH(ms)", "spikes/s" };
                lines.Add(string.Join(",", line));
            }

            //每个时间点的统计数据
            {
                for (int i = 0; i < Histograms.Count; i++)
                {
                    var line = new List<string> { $"{StairMedSettings.Instance.PSTHBinSizeMS * i - StairMedSettings.Instance.PSTHPreTriggerSpanMS}", $"{Histograms[i]}" };
                    lines.Add(string.Join(",", line));
                }
            }

            //
            var content = string.Join(Environment.NewLine, lines);
            FileHelper.SaveToFileWithBom($"{path}.csv", content);
        }
    }
}
