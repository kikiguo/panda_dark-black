﻿using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.UI.Logger;
using System.Collections.Generic;
using System.Windows.Input;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeScopeWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private double _thresholdUV = 0;
        public double ThresholdUV
        {
            get { return _thresholdUV; }
            set
            {
                if (Set(ref _thresholdUV, value))
                {
                    SpikeThresholdCenter.Instance.SetThreshold(InputChannel, value);
                }
            }
        }

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = 0;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 信号均方差
        /// </summary>
        private double _rms = 0.0;
        public double RMS
        {
            get { return _rms; }
            set { Set(ref _rms, value); }
        }

        /// <summary>
        /// spike发放频率
        /// </summary>
        private double _spikeRate = 0.0;
        public double SpikeRate
        {
            get { return _spikeRate; }
            set { Set(ref _spikeRate, value); }
        }

        /// <summary>
        /// 是否快照
        /// </summary>
        private bool _takeSnapshot = false;
        public bool TakeSnapshot
        {
            get { return _takeSnapshot; }
            set { Set(ref _takeSnapshot, value); }
        }

        /// <summary>
        /// 清理快照
        /// </summary>
        private bool _clearSnapshot = false;
        public bool ClearSnapshot
        {
            get { return _clearSnapshot; }
            set { Set(ref _clearSnapshot, value); }
        }

        /// <summary>
        /// 清理scope
        /// </summary>
        private bool _clearScope = false;
        public bool ClearScope
        {
            get { return _clearScope; }
            set { Set(ref _clearScope, value); }
        }

        /// <summary>
        /// 打印日志用于记录个数
        /// </summary>
        private static int _index = 0;

        /// <summary>
        /// 
        /// </summary>
        public SpikeScopeWndViewModel()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name}, index:{_index++}");

            //
            CareSettings.UnionWith(new HashSet<string>
            {
                nameof(StairMedSettings.Instance.SpikeScopeChannel),
            });

            //
            RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
            RMSCenter.Instance.HfpRmsUpdated += Instance_HfpRmsUpdated;
            RMSCenter.Instance.RegisterInputChannel(InputChannel);

            //
            SpikeRateCenter.Instance.SpikeRateUpdated -= Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.SpikeRateUpdated += Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.RegisterSpikeRate(InputChannel);

            //
            SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
            SpikeThresholdCenter.Instance.OnThresholdChanged += Instance_OnThresholdChanged;
        }

        /// <summary>
        /// 阈值发生变化
        /// </summary>
        private void Instance_OnThresholdChanged()
        {
            ThresholdUV = SpikeThresholdCenter.Instance.GetThreshold(InputChannel);
        }

        /// <summary>
        /// Spike发放率更新
        /// </summary>
        /// <param name="dict"></param>
        private void Instance_SpikeRateUpdated(int channel, double rate)
        {
            if (channel == InputChannel)
            {
                SpikeRate = rate;
            }
        }

        /// <summary>
        /// HFP信号的RMS更新
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="rms"></param>
        private void Instance_HfpRmsUpdated(int channel, double rms)
        {
            if (channel == InputChannel)
            {
                RMS = rms;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            SpikeRateCenter.Instance.UnregisterSpikeRate(InputChannel);
            RMSCenter.Instance.UnregisterInputChannel(InputChannel);
            InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.SpikeScopeChannel);
            SpikeRateCenter.Instance.RegisterSpikeRate(InputChannel);
            RMSCenter.Instance.RegisterInputChannel(InputChannel);

            //
            ThresholdUV = SpikeThresholdCenter.Instance.GetThreshold(InputChannel);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
            RMSCenter.Instance.UnregisterInputChannel(InputChannel);

            //
            SpikeRateCenter.Instance.SpikeRateUpdated -= Instance_SpikeRateUpdated;
            SpikeRateCenter.Instance.UnregisterSpikeRate(InputChannel);

            //
            SpikeThresholdCenter.Instance.OnThresholdChanged -= Instance_OnThresholdChanged;
        }

        /// <summary>
        /// 清空
        /// </summary>
        public ICommand ClearScopeCommand => new DelegateCommand(() =>
        {
            ClearScope = true;
            ClearScope = false;
        });

        /// <summary>
        /// 快照
        /// </summary>
        public ICommand TakeSnapshotCommand => new DelegateCommand(() =>
        {
            TakeSnapshot = true;
            TakeSnapshot = false;
        });

        /// <summary>
        /// 清理快照
        /// </summary>
        public ICommand ClearSnapshotCommand => new DelegateCommand(() =>
        {
            ClearSnapshot = true;
            ClearSnapshot = false;
        });

    }
}
