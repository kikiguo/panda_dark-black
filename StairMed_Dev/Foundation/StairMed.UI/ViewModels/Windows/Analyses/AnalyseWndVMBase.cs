﻿using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Event;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.UI.Logger;
using System.Collections.Generic;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    public class AnalyseWndVMBase : ViewModelBase
    {
        /// <summary>
        /// view名称，用于view弹窗匹配
        /// </summary>
        protected readonly string WMD_NAME = string.Empty;

        /// <summary>
        /// 公共配置：显示设置、参数设置
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 公共配置：显示设置、参数设置
        /// </summary>
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        protected HashSet<string> CareSettings = new HashSet<string>();

        /// <summary>
        /// 
        /// </summary>
        public AnalyseWndVMBase()
        {
            //
            WMD_NAME = GetType().Name[..^CONST.STR_VIEWMODEL_LEN];

            //
            EventHelper.Subscribe<WindowLoadEvent, string>(OnWindowPopup, Prism.Events.ThreadOption.BackgroundThread, true, (obj) => WMD_NAME.Equals(obj));
            EventHelper.Subscribe<WindowCloseEvent, string>(OnWindowClosed, Prism.Events.ThreadOption.BackgroundThread, true, (obj) => WMD_NAME.Equals(obj));

            //
            Settings.OnSettingChanged -= Settings_OnSettingChanged;
            Settings.OnSettingChanged += Settings_OnSettingChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void OnWindowPopup(string obj)
        {
            ReloadSettings(null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void OnWindowClosed(string obj)
        {
            OnViewClose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void Settings_OnSettingChanged(HashSet<string> changes)
        {
            if (CareSettings.Overlaps(changes))
            {
                ReloadSettings(changes);
            }
        }

        /// <summary>
        /// 重新加载配置
        /// </summary>
        protected virtual void ReloadSettings(HashSet<string> changes) { }

        /// <summary>
        /// 视图关闭，viewmodel被销毁
        /// </summary>
        protected virtual void OnViewClose()
        {
            EventHelper.Unsubscribe<WindowLoadEvent, string>(OnWindowPopup);
            EventHelper.Unsubscribe<WindowCloseEvent, string>(OnWindowClosed);
            Settings.OnSettingChanged -= Settings_OnSettingChanged;
        }
    }
}
