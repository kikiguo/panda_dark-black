﻿using MatFileHandler;
using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers.Spectrum;
using StairMed.Tools;
using StairMed.UI.Views.Windows.Analyses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpectrumWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly object lockObj = new object();

        #region spectrum

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = 0;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _displayWindowMS = 0;
        public int DisplayWindowMS
        {
            get { return _displayWindowMS; }
            set { Set(ref _displayWindowMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _sampleRate = 30000;
        public int SampleRate
        {
            get { return _sampleRate; }
            set
            {
                if (Set(ref _sampleRate, value))
                {
                    DeltaF = SampleRate * 1.0 / Settings.FFTSize;
                    DeltaT = Settings.FFTSize * 500.0 / SampleRate;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _deltaF = 0.0;
        public double DeltaF
        {
            get { return _deltaF; }
            set { Set(ref _deltaF, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _deltaT = 0.0;
        public double DeltaT
        {
            get { return _deltaT; }
            set { Set(ref _deltaT, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _spectrumReserved = 1;
        public int SpectrumReserved
        {
            get { return _spectrumReserved; }
            set { Set(ref _spectrumReserved, value); }
        }

        /// <summary>
        /// 功率谱集合
        /// </summary>
        private List<PSDInfo> _psds = new List<PSDInfo>();
        public List<PSDInfo> PSDs
        {
            get { return _psds; }
            set { Set(ref _psds, value); }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private bool _saveing = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> SpecturmChannelSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.SpecturmChannel),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> SpecturmFFTSizeSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.FFTSize),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> DisplayWindowMSSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.SpectrumTimeScaleS),
        };

        /// <summary>
        /// 
        /// </summary>
        public SpectrumWndViewModel()
        {
            CareSettings.UnionWith(SpecturmChannelSettings);
            CareSettings.UnionWith(SpecturmFFTSizeSettings);
            CareSettings.UnionWith(DisplayWindowMSSettings);

            //
            SpectrumCenter.Instance.HandleNewPSDs -= Instance_HandleNewPSDs;
            SpectrumCenter.Instance.HandleNewPSDs += Instance_HandleNewPSDs;
            NeuralDataCollector.Instance.RunningStateChanged -= Instance_RunningStateChanged;
            NeuralDataCollector.Instance.RunningStateChanged += Instance_RunningStateChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iscollecting"></param>
        private void Instance_RunningStateChanged(bool iscollecting)
        {
            //true表示重新开始采集
            if (iscollecting)
            {
                lock (lockObj)
                {
                    PSDs = new List<PSDInfo>();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="changes"></param>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            base.ReloadSettings(changes);

            //
            if (changes == null || SpecturmChannelSettings.Overlaps(changes))
            {
                SpectrumCenter.Instance.UnregisterSpectrumChannel(InputChannel);
                InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.SpecturmChannel);
                SpectrumCenter.Instance.RegisterSpectrumChannel(InputChannel);

                //
                lock (lockObj)
                {
                    PSDs = new List<PSDInfo>();
                }
            }

            //
            if (changes == null || SpecturmFFTSizeSettings.Overlaps(changes))
            {
                DeltaF = 1.0 * SampleRate / StairMedSettings.Instance.FFTSize;
                DeltaT = 500.0 * StairMedSettings.Instance.FFTSize / SampleRate;
            }

            //
            if (changes == null || DisplayWindowMSSettings.Overlaps(changes))
            {
                lock (lockObj)
                {
                    DisplayWindowMS = Settings.SpectrumTimeScaleS * 1000;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();
            SpectrumCenter.Instance.HandleNewPSDs -= Instance_HandleNewPSDs;
            SpectrumCenter.Instance.UnregisterSpectrumChannel(InputChannel);
            NeuralDataCollector.Instance.RunningStateChanged -= Instance_RunningStateChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="spectrumInfo"></param>
        private void Instance_HandleNewPSDs(int channel, int sampleRate, int fftSize, List<PSDInfo> psds)
        {
            if (_saveing)
            {
                return;
            }

            //
            if (channel == InputChannel)
            {
                lock (lockObj)
                {
                    var psdReserved = Convert.ToInt32(Math.Ceiling(1.0 * StairMedSettings.Instance.SpectrumTimeScaleS * sampleRate * 2 / fftSize));
                    SpectrumReserved = psdReserved;
                    SampleRate = sampleRate;
                    DeltaT = 500.0 * fftSize / SampleRate;
                    DeltaF = 1.0 * SampleRate / fftSize;

                    //
                    var tmpPsds = new List<PSDInfo>();
                    if (PSDs != null && PSDs.Count > 0 && PSDs[0].PSD.Length == psds[0].PSD.Length)
                    {
                        tmpPsds.AddRange(PSDs);
                    }
                    tmpPsds.AddRange(psds);
                    if (tmpPsds.Count > psdReserved)
                    {
                        tmpPsds.RemoveRange(0, tmpPsds.Count - psdReserved);
                    }
                    PSDs = tmpPsds;
                }
            }
        }


        /// <summary>
        /// 配置保存文件格式
        /// </summary>
        public ICommand ConfigDataSaveFileType => new DelegateCommand(() =>
        {
            UIHelper.Invoke(() => { new DataSaveTypeWnd().ShowDialog(); });
        });

        /// <summary>
        /// 保存
        /// </summary>
        public ICommand Save => new DelegateCommand<object>((obj) =>
        {
            _saveing = true;

            UIHelper.BeginInvoke(() =>
            {
                var dialog = new FolderBrowserDialog
                {
                    Description = "请选择保存文件路径",
                    SelectedPath = CONST.AppInstallPath,
                };

                //
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var destFolder = dialog.SelectedPath;
                    var spectrum = StairMedSettings.Instance.SpectrumDisplayMode ? "Spectrogram" : "Spectrum";
                    var savePath = Path.Combine(destFolder, $"{spectrum}_{InputChannel + 1}_{DateTime.Now:yyyyMMdd_HHmmss}");

                    //
                    Loading.Show(() =>
                    {
                        //CSV
                        if (StairMedSettings.Instance.SaveDataToCSVType && PSDs != null && PSDs.Count > 0)
                        {
                            SaveToCSV(savePath);
                        }

                        //Matlab
                        if (StairMedSettings.Instance.SaveDataToMatlabType && PSDs != null && PSDs.Count > 0)
                        {
                            SaveToMatlab(savePath);
                        }
                    }, () =>
                    {
                        //涉及到ui控件，只能放在ui线程中调用 //PNG
                        if (StairMedSettings.Instance.SaveDataToPNGType)
                        {
                            SaveToPNG(obj, savePath);
                        }
                        Toast.Info("保存成功");
                    });
                }

                //
                _saveing = false;
            });
        });

        /// <summary>
        /// 保存为png
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        private static void SaveToPNG(object obj, string path)
        {
            FileHelper.SnapshotUIControl((FrameworkElement)obj, new PngBitmapEncoder(), $"{path}.png");
        }

        /// <summary>
        /// 保存为matlab
        /// </summary>
        /// <param name="path"></param>
        private void SaveToMatlab(string path)
        {
            using (var fs = new FileStream($"{path}.mat", FileMode.OpenOrCreate, FileAccess.Write))
            {
                var writer = new MatFileWriter(fs);
                var builder = new DataBuilder();
                var waveform_name = builder.NewVariable("waveform_name", builder.NewCharArray($"{(StairMedSettings.Instance.SpectrumDisplayMode ? "Spectrogram" : "Spectrum")}-{StairMedSettings.Instance.SpecturmChannel}"));
                var nfft = builder.NewVariable("nfft", builder.NewArray(new[] { StairMedSettings.Instance.FFTSize }, 1, 1));
                var sample_rate = builder.NewVariable("sample_rate", builder.NewArray(new[] { SampleRate }, 1, 1));

                //
                var freqCount = PSDs[0].PSD.Length;
                var freqdata = new double[freqCount];
                for (int i = 0; i < freqCount; i++)
                {
                    freqdata[i] = i * DeltaF;
                }
                var f = builder.NewVariable("f", builder.NewArray(freqdata, freqCount, 1));

                //
                if (StairMedSettings.Instance.SpectrumDisplayMode)
                {
                    var tCount = PSDs.Count;
                    var tdata = new double[tCount];
                    for (int i = 0; i < tCount; i++)
                    {
                        tdata[i] = i * DeltaT / 1000;
                    }
                    var ts = builder.NewVariable("t", builder.NewArray(tdata, tCount, 1));

                    //
                    var datas = new double[freqCount * tCount];
                    for (int i = 0; i < tCount; i++)
                    {
                        var psd = PSDs[i];
                        for (int j = 0; j < freqCount; j++)
                        {
                            datas[i * freqCount + j] = psd.PSD[j];
                        }
                    }
                    var specgram = builder.NewVariable("specgram", builder.NewArray(datas, freqCount, tCount));

                    //
                    var mainData = builder.NewFile(new[] { waveform_name, nfft, sample_rate, f, ts, specgram });
                    writer.Write(mainData);
                }
                else
                {
                    var datas = new double[freqCount];
                    var psd = PSDs[^1];
                    for (int j = 0; j < freqCount; j++)
                    {
                        datas[j] = psd.PSD[j];
                    }
                    var specgram = builder.NewVariable("specgram", builder.NewArray(datas, freqCount, 1));

                    //
                    var mainData = builder.NewFile(new[] { waveform_name, nfft, sample_rate, f, specgram });
                    writer.Write(mainData);
                }
            }
        }

        /// <summary>
        /// 保存为csv
        /// </summary>
        /// <param name="path"></param>
        private void SaveToCSV(string path)
        {
            if (StairMedSettings.Instance.SpectrumDisplayMode)
            {
                //Spectrogram
                var lines = new List<string>();

                //headLine
                {
                    var line = new List<string> { "Time(s)/Freq(Hz)" };
                    for (int i = 0; i < PSDs[0].PSD.Length; i++)
                    {
                        line.Add($"{i * DeltaF}");
                    }
                    lines.Add(string.Join(",", line));
                }

                //每个时间点的频谱
                {
                    for (int psdIndex = 0; psdIndex < PSDs.Count; psdIndex++)
                    {
                        var line = new List<string> { $"{psdIndex * DeltaT / 1000}" };
                        for (int freqIndex = 0; freqIndex < PSDs[psdIndex].PSD.Length; freqIndex++)
                        {
                            line.Add($"{PSDs[psdIndex].PSD[freqIndex]}");
                        }
                        lines.Add(string.Join(",", line));
                    }
                }

                //
                var content = string.Join(Environment.NewLine, lines);
                FileHelper.SaveToFileWithBom($"{path}.csv", content);
            }
            else
            {
                //Spectrum
                var lines = new List<string>();

                //headLine
                {
                    var line = new List<string> { "Freq(Hz)", "Log Spectrum" };
                    lines.Add(string.Join(",", line));
                }

                //每个时间点的频谱
                {
                    var psd = PSDs[^1];
                    for (int freqIndex = 0; freqIndex < psd.PSD.Length; freqIndex++)
                    {
                        var line = new List<string> { $"{DeltaF * freqIndex}", $"{psd.PSD[freqIndex]}" };
                        lines.Add(string.Join(",", line));
                    }
                }

                //
                var content = string.Join(Environment.NewLine, lines);
                FileHelper.SaveToFileWithBom($"{path}.csv", content);
            }
        }
    }
}
