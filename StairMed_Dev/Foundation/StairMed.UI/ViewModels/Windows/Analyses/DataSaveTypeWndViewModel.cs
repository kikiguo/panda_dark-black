﻿using StairMed.Core.Settings;
using StairMed.MVVM.Base;
using StairMed.UI.Logger;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    internal class DataSaveTypeWndViewModel : ViewModelBase
    {
        /// <summary>
        /// 公共配置：显示设置、参数设置
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
    }
}
