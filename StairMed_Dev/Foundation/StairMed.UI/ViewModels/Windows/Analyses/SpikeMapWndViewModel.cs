﻿using StairMed.Core.Consts;
using StairMed.DataCenter.Centers.SpikeMap;
using StairMed.Probe;
using System.Collections.Generic;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeMapWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private NTProbeMap _probeMap = ReadonlyCONST.ProbeMap;
        public NTProbeMap ProbeMap
        {
            get { return _probeMap; }
            set { Set(ref _probeMap, value); }
        }

        /// <summary>
        /// 电极个数：pd2201支持8个：8*128
        /// </summary>
        private int _electrodeCount = 1;
        public int ElectrodeCount
        {
            get { return _electrodeCount; }
            set 
            {
                Set(ref _electrodeCount, value);
                ShowElectrodeIndex = value > 1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _showElectrodeIndex = false;
        public bool ShowElectrodeIndex
        {
            get { return _showElectrodeIndex; }
            set { Set(ref _showElectrodeIndex, value); }
        }


        /// <summary>
        /// 电极序号
        /// </summary>
        private int _electrodeIndex = 0;
        public int ElectrodeIndex
        {
            get { return _electrodeIndex; }
            set
            {
                if (_electrodeIndex >= 0 && _electrodeIndex < ElectrodeCount)
                {
                    UnregisterChannels(_electrodeIndex);
                }
                Set(ref _electrodeIndex, value);
                if (_electrodeIndex >= 0 && _electrodeIndex < ElectrodeCount)
                {
                    RegisterChannels(_electrodeIndex);
                }
            }
        }

        /// <summary>
        /// spike已过去多久:key:physics channel
        /// </summary>
        private Dictionary<int, double> _spikePassedMsDict = new Dictionary<int, double>();
        public Dictionary<int, double> SpikePassedMsDict
        {
            get { return _spikePassedMsDict; }
            set { Set(ref _spikePassedMsDict, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public SpikeMapWndViewModel()
        {
            ElectrodeCount = ReadonlyCONST.DeviceChannelCount / _probeMap.ChannelCount;

            //
            SpikeMapCenter.Instance.NewestSpikeIndexUpdated -= Instance_NewestSpikeIndexUpdated;
            SpikeMapCenter.Instance.NewestSpikeIndexUpdated += Instance_NewestSpikeIndexUpdated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="recvdMaxIndex"></param>
        /// <param name="newestSpikeIndexDict"></param>
        private void Instance_NewestSpikeIndexUpdated(int sampleRate, long recvdMaxIndex, Dictionary<int, long> newestSpikeIndexDict)
        {
            var spikePassedMsDict = new Dictionary<int, double>();
            foreach (var kvp in newestSpikeIndexDict)
            {
                var physicsChannel = ReadonlyCONST.ConvertToPhysicsChannel(kvp.Key) - _probeMap.ChannelCount * ElectrodeIndex;
                spikePassedMsDict[physicsChannel] = 1000.0 * (recvdMaxIndex - kvp.Value) / sampleRate;
            }
            SpikePassedMsDict = spikePassedMsDict;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RegisterChannels(int electrodeIndex)
        {
            var begin = _probeMap.ChannelCount * electrodeIndex;
            var end = _probeMap.ChannelCount * (electrodeIndex + 1) - 1;

            //
            var channels = new List<int>();
            for (int i = begin; i <= end; i++)
            {
                channels.Add(i);
            }
            SpikeMapCenter.Instance.RegisterChannels(channels);
        }

        /// <summary>
        /// 
        /// </summary>
        private void UnregisterChannels(int electrodeIndex)
        {
            var begin = _probeMap.ChannelCount * electrodeIndex;
            var end = _probeMap.ChannelCount * (electrodeIndex + 1) - 1;
            //
            var channels = new List<int>();
            for (int i = begin; i <= end; i++)
            {
                channels.Add(i);
            }
            SpikeMapCenter.Instance.UnregisterChannels(channels);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            SpikeMapCenter.Instance.NewestSpikeIndexUpdated -= Instance_NewestSpikeIndexUpdated;
            UnregisterChannels(ElectrodeIndex);
        }
    }
}
