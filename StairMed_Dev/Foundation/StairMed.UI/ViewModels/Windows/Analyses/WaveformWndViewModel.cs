﻿using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Tools;
using StairMed.UI.Entitys.DisplayDatas;
using StairMed.UI.Logger;
using System.Collections.Generic;
using System.Windows.Media;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    /// <summary>
    /// 
    /// </summary>
    internal class WaveformWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private static System.Windows.Media.Color LineColor = System.Windows.Media.Color.FromRgb(0xff, 0, 0);

        /// <summary>
        /// 
        /// </summary>
        private ChannelWaveformData _waveformData = new ChannelWaveformData(-1, ReadonlyCONST.ConvertToInputChannel(0), 0, LineColor);
        public ChannelWaveformData WaveformData
        {
            get { return _waveformData; }
            set { Set(ref _waveformData, value); }
        }

        /// <summary>
        /// 线条显示颜色：阶梯色
        /// </summary>
        private bool _lineColorSM = false;
        public bool LineColorSM
        {
            get { return _lineColorSM; }
            set
            {
                Set(ref _lineColorSM, value);
                if (value)
                {
                    WaveformData.ChangeLineColor(Color.FromRgb(0x00, 0xff, 0xa3));
                }
            }
        }

        /// <summary>
        /// 线条显示颜色：白色
        /// </summary>
        private bool _lineColorWhite = false;
        public bool LineColorWhite
        {
            get { return _lineColorWhite; }
            set
            {
                Set(ref _lineColorWhite, value);
                if (value)
                {
                    WaveformData.ChangeLineColor(Colors.White);
                }
            }
        }

        /// <summary>
        /// 线条显示颜色：原色
        /// </summary>
        private bool _lineColorOrign = true;
        public bool LineColorOrign
        {
            get { return _lineColorOrign; }
            set
            {
                Set(ref _lineColorOrign, value);
                if (value)
                {
                    var color = DeviceWaveformData.GetWaveformColor(StairMedSettings.Instance.WaveformChannel);
                    WaveformData.ChangeLineColor(color);
                }
            }
        }

        /// <summary>
        /// sweep or roll
        /// </summary>
        private bool _sweepOrRoll = true;
        public bool SweepOrRoll
        {
            get { return _sweepOrRoll; }
            set { Set(ref _sweepOrRoll, value); }
        }

        /// <summary>
        /// 显示原始波形
        /// </summary>
        private bool _displayRaw = true;
        public bool DisplayRaw
        {
            get { return _displayRaw; }
            set { Set(ref _displayRaw, value); }
        }

        /// <summary>
        /// 显示spike
        /// </summary>
        private bool _displaySpike = false;
        public bool DisplaySpike
        {
            get { return _displaySpike; }
            set { Set(ref _displaySpike, value); }
        }

        /// <summary>
        /// 显示spike组合
        /// </summary>
        private bool _displaySpikeSum = false;
        public bool DisplaySpikeSum
        {
            get { return _displaySpikeSum; }
            set { Set(ref _displaySpikeSum, value); }
        }

        /// <summary>
        /// 显示高频波形
        /// </summary>
        private bool _displayHFWave = true;
        public bool DisplayHFWave
        {
            get { return _displayHFWave; }
            set { Set(ref _displayHFWave, value); }
        }

        /// <summary>
        /// 显示低频信号
        /// </summary>
        private bool _displayLFWave = true;
        public bool DisplayLFWave
        {
            get { return _displayLFWave; }
            set { Set(ref _displayLFWave, value); }
        }

        /// <summary>
        /// 显示外触发信号
        /// </summary>
        private bool _displayTRIG = true;
        public bool DisplayTRIG
        {
            get { return _displayTRIG; }
            set { Set(ref _displayTRIG, value); }
        }

        /// <summary>
        /// 显示全通道数据
        /// </summary>
        private bool _displayWFWave = false;
        public bool DisplayWFWave
        {
            get { return _displayWFWave; }
            set { Set(ref _displayWFWave, value); }
        }

        /// <summary>
        /// sweep模式下x的位置
        /// </summary>
        private float _sweepX = -1;
        public float SweepX
        {
            get { return _sweepX; }
            set { Set(ref _sweepX, value); }
        }

        /// <summary>
        /// 打印日志用于记录个数
        /// </summary>
        private static int _index = 0;

        /// <summary>
        /// 
        /// </summary>
        public WaveformWndViewModel()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name}, index:{_index++}");
            CareSettings.UnionWith(new HashSet<string> { nameof(StairMedSettings.Instance.WaveformChannel) });
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            UIHelper.BeginInvoke(() =>
            {
                var inputChannel = ReadonlyCONST.ConvertToInputChannel(StairMedSettings.Instance.WaveformChannel);

                //
                LineColorOrign = true;
                Color color = DeviceWaveformData.GetWaveformColor(StairMedSettings.Instance.WaveformChannel);

                //
                WaveformData.Update(-1, inputChannel, StairMedSettings.Instance.WaveformChannel, color);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();
            WaveformData.Dispose();
        }
    }
}
