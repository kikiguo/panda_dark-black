﻿using MatFileHandler;
using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers.RMS;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.FilterProcess;
using StairMed.Tools;
using StairMed.UI.Views.Windows.Analyses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    internal class RMSWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly object _lock = new object();

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = 0;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<float> _histograms = new List<float>();
        public List<float> Histograms
        {
            get { return _histograms; }
            set { Set(ref _histograms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<float> _oldhistograms = new List<float>();
        public List<float> OldHistograms
        {
            get { return _oldhistograms; }
            set { Set(ref _oldhistograms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _mean = 0;
        public double Mean
        {
            get { return _mean; }
            set { Set(ref _mean, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _rmsFreq = 0;
        public double RMSFreq
        {
            get { return _rmsFreq; }
            set { Set(ref _rmsFreq, value); }
        }


        private double _rms = 0;
        public double Rms
        {
            get { return _rms; }
            set { Set(ref _rms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _stdDev = 0;
        public double StdDev
        {
            get { return _stdDev; }
            set { Set(ref _stdDev, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private long _rmsCount = 0;
        public long RMSCount
        {
            get { return _rmsCount; }
            set { Set(ref _rmsCount, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _binSizeMS = 10;
        public int BinSizeMS
        {
            get { return _binSizeMS; }
            set { Set(ref _binSizeMS, value); }
        }

        private int _rmsValue = 33;
        public int RMSValue
        {
            get { return _rmsValue; }
            set { Set(ref _rmsValue, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private int _timeSpan = 1000;
        public int TimeSpan
        {
            get { return _timeSpan; }
            set { Set(ref _timeSpan, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private int _staRms = 0;
        public int StaRms
        {
            get { return _staRms; }
            set { Set(ref _staRms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _endRms = 0;
        public int EndRms
        {
            get { return _endRms; }
            set { Set(ref _endRms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _saveing = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> RMSChannelSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.RMSChannel),
        };
       
        /// <summary>
        /// 
        /// </summary>
        public RMSWndViewModel()
        {
            //
            CareSettings.UnionWith(RMSChannelSettings);



            //
            RMSCenter.Instance.RMSUpdated -= Instance_RMSUpdated;
            RMSCenter.Instance.RMSUpdated += Instance_RMSUpdated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rmsInfo"></param>
        private void Instance_RMSUpdated(int channel, RMSInfo rmsInfo)
        {
            if (_saveing || channel != InputChannel)
            {
                return;
            }

            //
            if (channel == InputChannel)
            {
                lock (_lock)
                {
                    try
                    {
                        //
                        RMSCount = rmsInfo.RMSCount;
                        Mean = rmsInfo.Mean;
                        StdDev = rmsInfo.StdDev;
                        RMSFreq = rmsInfo.RMSFreq;
                        Rms = rmsInfo.Rms;
                        BinSizeMS = StairMedSettings.Instance.RMSBinSizeMS;
                        TimeSpan = StairMedSettings.Instance.RMSTimeScaleMS;
                        OldHistograms.Add(rmsInfo.Histograms[0]);
                        if (!StairMedSettings.Instance.RMSMouseDouble)
                        {
                            StaRms = OldHistograms.Count < TimeSpan / BinSizeMS ? 0 : OldHistograms.Count - TimeSpan / BinSizeMS;
                            EndRms = OldHistograms.Count;
                            var hist = OldHistograms.Skip(StaRms).Take(EndRms - StaRms).ToList();

                            Histograms = hist;
                        }
                       
                        
                       
                      
                        RMSValue = StairMedSettings.Instance.RMSValue;
                    }
                    catch {

                        string boo = "faa";
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="changes"></param>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            base.ReloadSettings(changes);

            //
            if (changes == null || RMSChannelSettings.Overlaps(changes))
            {
                RMSCenter.Instance.UnregisterRMSChannel(InputChannel);
                InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.RMSChannel);
                RMSCenter.Instance.RegisterRMSChannel(InputChannel);

                //
                lock (_lock)
                {
                    RMSCount = 0;
                    Mean = 0.0;
                    StdDev = 0.0;
                    RMSFreq = 0.0;
                    Rms = 0.0;
                    Histograms = new List<float>();
                    BinSizeMS = StairMedSettings.Instance.RMSBinSizeMS;
                    TimeSpan = StairMedSettings.Instance.RMSTimeScaleMS;
                    RMSValue = StairMedSettings.Instance.RMSValue;
                }
            }
        }

        /// <summary>
        /// 窗口关闭时，释放所有监听
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            RMSCenter.Instance.RMSUpdated -= Instance_RMSUpdated;
            RMSCenter.Instance.UnregisterRMSChannel(InputChannel);
        }

        /// <summary>
        /// 清空
        /// </summary>
        public ICommand Clean => new DelegateCommand(() =>
        {
            RMSCenter.Instance.CleanChannel(InputChannel);
            lock (_lock)
            {
                RMSCount = 0;
                Mean = 0.0;
                StdDev = 0.0;
                RMSFreq = 0.0;
                Rms = 0.0;
                Histograms = new List<float>();
                BinSizeMS = StairMedSettings.Instance.RMSBinSizeMS;
                TimeSpan = StairMedSettings.Instance.RMSTimeScaleMS;
                RMSValue = StairMedSettings.Instance.RMSValue;
            }
        });

        /// <summary>
        /// 配置保存文件格式
        /// </summary>
        public ICommand ConfigDataSaveFileType => new DelegateCommand(() =>
        {
            UIHelper.Invoke(() => { new DataSaveTypeWnd().ShowDialog(); });
        });

        /// <summary>
        /// 保存
        /// </summary>
        public ICommand Save => new DelegateCommand<object>((obj) =>
        {
            _saveing = true;
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new FolderBrowserDialog
                {
                    Description = "请选择保存文件路径",
                    SelectedPath = CONST.AppInstallPath,
                };

                //
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var destFolder = dialog.SelectedPath;
                    var savePath = Path.Combine(destFolder, $"RMS_{InputChannel + 1}_{DateTime.Now:yyyyMMdd_HHmmss}");

                    //
                    Loading.Show(() =>
                    {
                        //CSV
                        if (StairMedSettings.Instance.SaveDataToCSVType && Histograms != null && Histograms.Count > 0)
                        {
                            SaveToCSV(savePath);
                        }

                        //Matlab
                        if (StairMedSettings.Instance.SaveDataToMatlabType && Histograms != null && Histograms.Count > 0)
                        {
                            SaveToMatlab(savePath);
                        }
                    }, () =>
                    {
                        //涉及到ui控件，只能放在ui线程中调用 //PNG
                        if (StairMedSettings.Instance.SaveDataToPNGType)
                        {
                            SaveToPNG(obj, savePath);
                        }
                        Toast.Info("保存成功");
                    });
                }

                //
                _saveing = false;
            });
        });

        /// <summary>
        /// 保存为图片
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        private void SaveToPNG(object obj, string path)
        {
            FileHelper.SnapshotUIControl((FrameworkElement)obj, new PngBitmapEncoder(), $"{path}.png");
        }

        /// <summary>
        /// 保存为matlab文件
        /// </summary>
        /// <param name="path"></param>
        private void SaveToMatlab(string path)
        {
            using (var fs = new FileStream($"{path}.mat", FileMode.OpenOrCreate, FileAccess.Write))
            {
                var writer = new MatFileWriter(fs);
                var builder = new DataBuilder();
                var bin_size = builder.NewVariable("bin_size", builder.NewArray(new[] { BinSizeMS }, 1, 1));
                var waveform_name = builder.NewVariable("waveform_name", builder.NewCharArray($"RMS-{StairMedSettings.Instance.RMSChannel}"));
                var mean_RMS = builder.NewVariable("mean_RMS", builder.NewArray(new[] { Mean }, 1, 1));
                var num_RMSs_recorded = builder.NewVariable("num_RMSs_recorded", builder.NewArray(new[] { RMSCount }, 1, 1));
                var std_dev_RMS = builder.NewVariable("std_dev_RMS", builder.NewArray(new[] { StdDev }, 1, 1));
                var sample_rate = builder.NewVariable("sample_rate", builder.NewArray(new[] { NeuralDataCollector.Instance.GetCollectingSampleRate() }, 1, 1));

                //
                var t_bins_datas = new int[Histograms.Count];
                for (int i = 0; i < t_bins_datas.Length; i++)
                {
                    t_bins_datas[i] = i * BinSizeMS;
                }
                var t_bins = builder.NewVariable("t_bins", builder.NewArray(t_bins_datas, t_bins_datas.Length, 1));

                //
                var probability_datas = new float[Histograms.Count];
                for (int i = 0; i < probability_datas.Length; i++)
                {
                    probability_datas[i] = Histograms[i];
                }
                var probability = builder.NewVariable("probability", builder.NewArray(probability_datas, probability_datas.Length, 1));

                //
                var mainData = builder.NewFile(new[] { bin_size, waveform_name, mean_RMS, sample_rate, num_RMSs_recorded, std_dev_RMS, t_bins, probability });
                writer.Write(mainData);
            }
        }

        /// <summary>
        /// 保存为csv文件
        /// </summary>
        /// <param name="path"></param>
        private void SaveToCSV(string path)
        {
            var lines = new List<string>();

            //headLine
            {
                var line = new List<string> { "RMS(ms)", "probability" };
                lines.Add(string.Join(",", line));
            }

            //每个时间点的统计数据
            {
                for (int i = 0; i < Histograms.Count; i++)
                {
                    var line = new List<string> { $"{BinSizeMS * i}", $"{Histograms[i]}" };
                    lines.Add(string.Join(",", line));
                }
            }

            //
            var content = string.Join(Environment.NewLine, lines);
            FileHelper.SaveToFileWithBom($"{path}.csv", content);
        }
    }
}
