﻿using MatFileHandler;
using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.Filter;
using StairMed.DataCenter;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows;
using StairMed.UI.Views.Windows.Analyses;
using System.Windows.Forms;
using StairMed.Controls.Windows;
using StairMed.FilterProcess;

namespace StairMed.UI.ViewModels.Windows.Analyses
{
    internal class FilterWndViewModel : AnalyseWndVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly object _lock = new object();

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = 0;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<float> _lowhistograms = new List<float>();
        public List<float> LowHistograms
        {
            get { return _lowhistograms; }
            set { Set(ref _lowhistograms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<float> _highistograms = new List<float>();
        public List<float> HigHistograms
        {
            get { return _highistograms; }
            set { Set(ref _highistograms, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private double _mean = 0;
        public double Mean
        {
            get { return _mean; }
            set { Set(ref _mean, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _rmsFreq = 0;
        public double FilterFreq
        {
            get { return _rmsFreq; }
            set { Set(ref _rmsFreq, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _stdDev = 0;
        public double StdDev
        {
            get { return _stdDev; }
            set { Set(ref _stdDev, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private long _rmsCount = 0;
        public long FilterCount
        {
            get { return _rmsCount; }
            set { Set(ref _rmsCount, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _binSizeMS = 10;
        public int BinSizeMS
        {
            get { return _binSizeMS; }
            set { Set(ref _binSizeMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _timeSpan = 3000;
        public int TimeSpan
        {
            get { return _timeSpan; }
            set { Set(ref _timeSpan, value); }
        }



        /// <summary>
        /// 
        /// </summary>
        private bool _saveing = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> FilterChannelSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.WaveformChannel),
        };
        ParamCenter ParamCenter { get; }
        /// <summary>
        /// 
        /// </summary>
        public FilterWndViewModel()
        {
            //
            CareSettings.UnionWith(FilterChannelSettings);
            ParamCenter.SetHighPassFilter(50, 1, FilterType.Butterworth);
            //
            FilterCenter.Instance.FilterUpdated -= Instance_FilterUpdated;
            FilterCenter.Instance.FilterUpdated += Instance_FilterUpdated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rmsInfo"></param>
        private void Instance_FilterUpdated(int channel, FilterInfo rmsInfo)
        {
            if (_saveing || channel != InputChannel)
            {
                return;
            }

            //
            if (channel == InputChannel)
            {
                lock (_lock)
                {
                    try
                    {
                        //
                        FilterCount = rmsInfo.FilterCount;
                        Mean = rmsInfo.Mean;
                        StdDev = rmsInfo.StdDev;
                        BinSizeMS = StairMedSettings.Instance.FilterBinSizeMS;
                        TimeSpan = StairMedSettings.Instance.FilterTimeScaleMS;
                        FilterFreq = rmsInfo.FilterFreq;
                        if(TimeSpan / BinSizeMS<=LowHistograms.Count)
                        {
                            LowHistograms.RemoveAt(0);
                            HigHistograms.RemoveAt(0);
                        }
                        LowHistograms.Add(rmsInfo.LowHistograms[0]);
                        HigHistograms.Add(rmsInfo.HigHistograms[0]);
                    
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="changes"></param>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            base.ReloadSettings(changes);

            //
            if (changes == null || FilterChannelSettings.Overlaps(changes))
            {
                FilterCenter.Instance.UnregisterFilterChannel(InputChannel);
                InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.WaveformChannel);
                FilterCenter.Instance.RegisterFilterChannel(InputChannel);

                //
                lock (_lock)
                {
                    FilterCount = 0;
                    Mean = 0.0;
                    StdDev = 0.0;
                    FilterFreq = 0.0;
                    LowHistograms = new List<float>();
                    HigHistograms = new List<float>();
                    BinSizeMS = StairMedSettings.Instance.FilterBinSizeMS;
                    TimeSpan = StairMedSettings.Instance.FilterTimeScaleMS;
                }
            }
        }

        /// <summary>
        /// 窗口关闭时，释放所有监听
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            FilterCenter.Instance.FilterUpdated -= Instance_FilterUpdated;
            FilterCenter.Instance.UnregisterFilterChannel(InputChannel);
        }

        /// <summary>
        /// 清空
        /// </summary>
        public ICommand Clean => new DelegateCommand(() =>
        {
            FilterCenter.Instance.CleanChannel(InputChannel);
            lock (_lock)
            {
                FilterCount = 0;
                Mean = 0.0;
                StdDev = 0.0;
                FilterFreq = 0.0;
                LowHistograms = new List<float>();
                HigHistograms = new List<float>();
                BinSizeMS = StairMedSettings.Instance.FilterBinSizeMS;
                TimeSpan = StairMedSettings.Instance.FilterTimeScaleMS;
            }
        });

       


        /// <summary>
        /// 配置保存文件格式
        /// </summary>
        public ICommand ConfigDataSaveFileType => new DelegateCommand(() =>
        {
            UIHelper.Invoke(() => { new DataSaveTypeWnd().ShowDialog(); });
        });

        /// <summary>
        /// 保存
        /// </summary>
        public ICommand Save => new DelegateCommand<object>((obj) =>
        {
            _saveing = true;
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new FolderBrowserDialog
                {
                    Description = "请选择保存文件路径",
                    SelectedPath = CONST.AppInstallPath,
                };

                //
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var destFolder = dialog.SelectedPath;
                    var savePath = Path.Combine(destFolder, $"Filter_{InputChannel + 1}_{DateTime.Now:yyyyMMdd_HHmmss}");

                    //
                    Loading.Show(() =>
                    {
                        //CSV
                        if (StairMedSettings.Instance.SaveDataToCSVType && LowHistograms != null && LowHistograms.Count > 0)
                        {
                            SaveToCSV(savePath);
                        }

                        //Matlab
                        if (StairMedSettings.Instance.SaveDataToMatlabType && LowHistograms != null && LowHistograms.Count > 0)
                        {
                            SaveToMatlab(savePath);
                        }
                    }, () =>
                    {
                        //涉及到ui控件，只能放在ui线程中调用 //PNG
                        if (StairMedSettings.Instance.SaveDataToPNGType)
                        {
                            SaveToPNG(obj, savePath);
                        }
                        Toast.Info("保存成功");
                    });
                }

                //
                _saveing = false;
            });
        });

        /// <summary>
        /// 保存为图片
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        private void SaveToPNG(object obj, string path)
        {
            FileHelper.SnapshotUIControl((FrameworkElement)obj, new PngBitmapEncoder(), $"{path}.png");
        }

        /// <summary>
        /// 保存为matlab文件
        /// </summary>
        /// <param name="path"></param>
        private void SaveToMatlab(string path)
        {
            using (var fs = new FileStream($"{path}.mat", FileMode.OpenOrCreate, FileAccess.Write))
            {
                var writer = new MatFileWriter(fs);
                var builder = new DataBuilder();
                var bin_size = builder.NewVariable("bin_size", builder.NewArray(new[] { BinSizeMS }, 1, 1));
                var waveform_name = builder.NewVariable("waveform_name", builder.NewCharArray($"Filter-{StairMedSettings.Instance.WaveformChannel}"));
                var mean_Filter = builder.NewVariable("mean_Filter", builder.NewArray(new[] { Mean }, 1, 1));
                var num_Filters_recorded = builder.NewVariable("num_Filters_recorded", builder.NewArray(new[] { FilterCount }, 1, 1));
                var std_dev_Filter = builder.NewVariable("std_dev_Filter", builder.NewArray(new[] { StdDev }, 1, 1));
                var sample_rate = builder.NewVariable("sample_rate", builder.NewArray(new[] { NeuralDataCollector.Instance.GetCollectingSampleRate() }, 1, 1));

                //
                var t_bins_datas = new int[LowHistograms.Count];
                for (int i = 0; i < t_bins_datas.Length; i++)
                {
                    t_bins_datas[i] = i * BinSizeMS;
                }
                var t_bins = builder.NewVariable("t_bins", builder.NewArray(t_bins_datas, t_bins_datas.Length, 1));

                //
                var probability_datas = new float[LowHistograms.Count];
                for (int i = 0; i < probability_datas.Length; i++)
                {
                    probability_datas[i] = LowHistograms[i];
                }
                var probability = builder.NewVariable("probability", builder.NewArray(probability_datas, probability_datas.Length, 1));

                //
                var mainData = builder.NewFile(new[] { bin_size, waveform_name, mean_Filter, sample_rate, num_Filters_recorded, std_dev_Filter, t_bins, probability });
                writer.Write(mainData);
            }
        }

        /// <summary>
        /// 保存为csv文件
        /// </summary>
        /// <param name="path"></param>
        private void SaveToCSV(string path)
        {
            var lines = new List<string>();

            //headLine
            {
                var line = new List<string> { "Filter(ms)", "probability" };
                lines.Add(string.Join(",", line));
            }

            //每个时间点的统计数据
            {
                for (int i = 0; i < LowHistograms.Count; i++)
                {
                    var line = new List<string> { $"{BinSizeMS * i}", $"{LowHistograms[i]}" };
                    lines.Add(string.Join(",", line));
                }
            }

            //
            var content = string.Join(Environment.NewLine, lines);
            FileHelper.SaveToFileWithBom($"{path}.csv", content);
        }
    }
}
