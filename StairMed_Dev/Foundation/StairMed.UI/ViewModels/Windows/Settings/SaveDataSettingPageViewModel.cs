﻿using Microsoft.Win32;
using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.Core.Settings;
using StairMed.Extension;
using StairMed.MVVM.Base;
using StairMed.Tools;
using System.Diagnostics;
using System.Windows.Input;

namespace StairMed.UI.ViewModels.Windows.Settings
{
    /// <summary>
    /// 
    /// </summary>
    internal class SaveDataSettingPageViewModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(SaveDataSettingPageViewModel);

        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;

        /// <summary>
        /// 采集数据存储位置设置
        /// </summary>
        public ICommand ChangeDataSaveFolder => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new System.Windows.Forms.FolderBrowserDialog
                {
                    SelectedPath = SolidSettings.NeuralDataSaveFolder,
                };
                var ret = dialog.ShowDialog();
                if (ret == System.Windows.Forms.DialogResult.OK)
                {
                    var path = dialog.SelectedPath;
                    if (!ToolMix.HasChinese(path))
                    {
                        SolidSettings.NeuralDataSaveFolder = dialog.SelectedPath;
                    }
                    else
                    {
                        Toast.Warn("采集数据存储路径中不能包含中文");
                    }
                }
            });
        });

        /// <summary>
        /// 打开采集数据保存位置
        /// </summary>
        public ICommand OpenDataSaveFolder => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                Process.Start("explorer.exe", SolidSettings.NeuralDataSaveFolder);
            });
        });

        /// <summary>
        /// Load Settings
        /// </summary>
        public ICommand LoadSettings => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                    InitialDirectory = CONST.AppInstallPath,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //
                var persistence = new PersistenceHelper(dialog.FileName);
                var instance = persistence.Load<StairMedSettings>();
                if (instance == null)
                {
                    Toast.Error("加载失败");
                    return;
                }

                //
                StairMedSettings.Instance.DeepCopy(instance);
            });
        });

        /// <summary>
        /// Save Settings
        /// </summary>
        public ICommand SaveSettings => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new SaveFileDialog
                {
                    Filter = "All files (*.xml)|*.xml",
                    OverwritePrompt = true,
                };
                if (dialog.ShowDialog() == true)
                {
                    new PersistenceHelper(dialog.FileName).Save(StairMedSettings.Instance);
                }
            });
        });

        /// <summary>
        /// Set Default Settings
        /// </summary>
        public ICommand SetDefaultSettings => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                    InitialDirectory = CONST.AppInstallPath,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //             
                var persistence = new PersistenceHelper(dialog.FileName);
                var instance = persistence.Load<StairMedSettings>();
                if (instance == null)
                {
                    Toast.Error("加载失败");
                    return;
                }

                //
                StairMedSettings.Instance.DeepCopy(instance);
                StairMedSolidSettings.Instance.DefaultSettingPath = dialog.FileName;
            });
        });

    }
}
