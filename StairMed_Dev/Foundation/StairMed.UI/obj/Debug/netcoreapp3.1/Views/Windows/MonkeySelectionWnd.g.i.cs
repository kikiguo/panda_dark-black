﻿#pragma checksum "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "82EC2A0010A93ABFABF7A37F182FB931ADDCAED5"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using HandyControl.Controls;
using HandyControl.Data;
using HandyControl.Expression.Media;
using HandyControl.Expression.Shapes;
using HandyControl.Interactivity;
using HandyControl.Media.Animation;
using HandyControl.Media.Effects;
using HandyControl.Properties.Langs;
using HandyControl.Themes;
using HandyControl.Tools;
using HandyControl.Tools.Converter;
using HandyControl.Tools.Extension;
using StairMed.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace StairMed.UI.Views.Windows {
    
    
    /// <summary>
    /// MonkeySelectionWnd
    /// </summary>
    public partial class MonkeySelectionWnd : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal StairMed.UI.Views.Windows.MonkeySelectionWnd thisWnd;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblCode;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbCode;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblType;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbType;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblDesp;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbHospital;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTarget;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbElecode;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTime;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtTime;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOK;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClose;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.10.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/StairMed.UI;component/views/windows/monkeyselectionwnd.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.10.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.thisWnd = ((StairMed.UI.Views.Windows.MonkeySelectionWnd)(target));
            return;
            case 2:
            this.lblCode = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.cmbCode = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.lblType = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.cmbType = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.lblDesp = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.cmbHospital = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.lblTarget = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.cmbElecode = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.lblTime = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.txtTime = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.btnOK = ((System.Windows.Controls.Button)(target));
            
            #line 85 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
            this.btnOK.Click += new System.Windows.RoutedEventHandler(this.btnOK_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btnClose = ((System.Windows.Controls.Button)(target));
            
            #line 91 "..\..\..\..\..\Views\Windows\MonkeySelectionWnd.xaml"
            this.btnClose.Click += new System.Windows.RoutedEventHandler(this.btnClose_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

