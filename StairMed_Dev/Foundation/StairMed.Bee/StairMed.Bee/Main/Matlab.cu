#include "Exports.h"
#include "Globals.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder1.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder2.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder3.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder4.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder5.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder6.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder7.cuh"
#include "../Kernels/MatlabKernels/MatlabFilterOrder8.cuh"


//
int matlabNotchFilterOrder = 0;
int matlabHfpFilterOrder = 0; //matlab�˲� b1 b2 ...bn  a1 a2 ... an
int matlabLfpFilterOrder = 0; //matlab�˲� b1 b2 ...bn  a1 a2 ... an

double *dev_matlabWfpPrevs = 0;
double *dev_matlabLfpPrevs = 0;
double *dev_matlabHfpPrevs = 0;

double *dev_matlabWfpFilter = 0;
double *dev_matlabLfpFilter = 0;
double *dev_matlabHfpFilter = 0;

//
void InitMatlabFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[], double** d_prevs, double** d_filter, float** d_datas, float ** d_transpose)
{
	if (*d_prevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_prevs));
		*d_prevs = 0;
	}
	if (*d_filter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_filter));
		*d_filter = 0;
	}
	if (*d_datas != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_datas));
		*d_datas = 0;
	}
	if (*d_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_transpose));
		*d_transpose = 0;
	}

	//
	int prevByteCount = channels * (filterParamCount - 2) * sizeof(double);
	cudaMalloc((void**)d_prevs, prevByteCount);
	cudaMemset(*d_prevs, 0, prevByteCount);

	//
	cudaMalloc((void**)d_filter, filterParamCount * sizeof(double));
	cudaMemcpy(*d_filter, filterParam, filterParamCount * sizeof(double), cudaMemcpyHostToDevice);

	//
	InitBuffer(channels, oneChannelSamples, d_datas, d_transpose);
}


//
void InitMatlabNotchFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[])
{
	matlabNotchFilterOrder = filterParamCount / 2 - 1;
	InitMatlabFilterParam(channels, oneChannelSamples, filterParamCount, filterParam, &dev_matlabWfpPrevs, &dev_matlabWfpFilter, &dev_wfps, &dev_wfps_transpose);
}

//
void InitMatlabLfpFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[])
{
	matlabLfpFilterOrder = filterParamCount / 2 - 1;
	InitMatlabFilterParam(channels, oneChannelSamples, filterParamCount, filterParam, &dev_matlabLfpPrevs, &dev_matlabLfpFilter, &dev_lfps, &dev_lfps_transpose);
}

//
void InitMatlabHfpFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[])
{
	matlabHfpFilterOrder = filterParamCount / 2 - 1;
	InitMatlabFilterParam(channels, oneChannelSamples, filterParamCount, filterParam, &dev_matlabHfpPrevs, &dev_matlabHfpFilter, &dev_hfps, &dev_hfps_transpose);
}

//
void FreeMatlabBuffer()
{
	if (dev_matlabWfpPrevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_matlabWfpPrevs));
		dev_matlabWfpPrevs = 0;
	}
	if (dev_matlabLfpPrevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_matlabLfpPrevs));
		dev_matlabLfpPrevs = 0;
	}
	if (dev_matlabHfpPrevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_matlabHfpPrevs));
		dev_matlabHfpPrevs = 0;
	}

	if (dev_matlabWfpFilter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_matlabWfpFilter));
		dev_matlabWfpFilter = 0;
	}
	if (dev_matlabLfpFilter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_matlabLfpFilter));
		dev_matlabLfpFilter = 0;
	}
	if (dev_matlabHfpFilter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_matlabHfpFilter));
		dev_matlabHfpFilter = 0;
	}
}


//
void FilterByMatlab(cudaStream_t stream, int channels, int oneChannelSamples, const float* srcs, int filterOrder, double* prevs, double* filter, float* fps)
{
	int blockSize = 64;
	int gridSize = (channels + blockSize - 1) / blockSize;
	switch (filterOrder)
	{
	case 1:
	{
		MatlabFilterOrder1Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 2:
	{
		MatlabFilterOrder2Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 3:
	{
		MatlabFilterOrder3Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 4:
	{
		MatlabFilterOrder4Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 5:
	{
		MatlabFilterOrder5Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 6:
	{
		MatlabFilterOrder6Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 7:
	{
		MatlabFilterOrder7Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	case 8:
	{
		MatlabFilterOrder8Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	break;
	default:
		break;
	}
}


//
void ProcessADChunkByMatlab(int adoffset, unsigned char adbytes[], int channels, int oneChannelSamples, float raws[], float wfps[], float hfps[], float lfps[], float spikes[])
{
	//
	int sampleCount = channels * oneChannelSamples;

	//
	CUDA_CHECK_ERROR(cudaMemcpy(dev_ads, adbytes + adoffset, sampleCount * sizeof(unsigned short), cudaMemcpyHostToDevice));

	//
	cudaStream_t rawStream;
	CUDA_CHECK_ERROR(cudaStreamCreate(&(rawStream)));

	//ad -> raw -> wfp
	{
		//ad to raw
		AD2Raw(rawStream, (unsigned short*)dev_ads, sampleCount, raw_k, raw_b, dev_raws);

		//raw to wfp
		FilterByMatlab(rawStream, channels, oneChannelSamples, dev_raws, matlabNotchFilterOrder, dev_matlabWfpPrevs, dev_matlabWfpFilter, dev_wfps);
	}

	//wfp to hfp
	FilterByMatlab(rawStream, channels, oneChannelSamples, dev_wfps, matlabHfpFilterOrder, dev_matlabHfpPrevs, dev_matlabHfpFilter, dev_hfps);

	//wfp to lfp
	FilterByMatlab(rawStream, channels, oneChannelSamples, dev_wfps, matlabLfpFilterOrder, dev_matlabLfpPrevs, dev_matlabLfpFilter, dev_lfps);

	//
	Threshold(rawStream, channels, oneChannelSamples);

	//transpose	
	Transpose(rawStream, dev_raws, dev_raws_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_wfps, dev_wfps_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_lfps, dev_lfps_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_hfps, dev_hfps_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_spikes, dev_spikes_transpose, oneChannelSamples, channels);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(rawStream));

	//
	CUDA_CHECK_ERROR(cudaStreamDestroy(rawStream));

	//
	CUDA_CHECK_ERROR(cudaDeviceSynchronize());

	//
	CUDA_CHECK_ERROR(cudaMemcpy(raws, dev_raws_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(wfps, dev_wfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(hfps, dev_hfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(lfps, dev_lfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(spikes, dev_spikes_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
}

//
void ProcessADChunkByMatlab2(int adoffset, unsigned char adbytes[], int channels, int oneChannelSamples, float wfps[], float hfps[], float lfps[], float spikes[])
{
	//
	int sampleCount = channels * oneChannelSamples;

	//
	CUDA_CHECK_ERROR(cudaMemcpy(dev_ads, adbytes + adoffset, sampleCount * sizeof(unsigned short), cudaMemcpyHostToDevice));

	//
	cudaStream_t rawStream;
	CUDA_CHECK_ERROR(cudaStreamCreate(&(rawStream)));

	//ad -> raw -> wfp
	{
		//ad to raw
		AD2Raw(rawStream, (unsigned short*)dev_ads, sampleCount, raw_k, raw_b, dev_raws);

		//raw to wfp
		FilterByMatlab(rawStream, channels, oneChannelSamples, dev_raws, matlabNotchFilterOrder, dev_matlabWfpPrevs, dev_matlabWfpFilter, dev_wfps);
	}

	//wfp to hfp
	FilterByMatlab(rawStream, channels, oneChannelSamples, dev_wfps, matlabHfpFilterOrder, dev_matlabHfpPrevs, dev_matlabHfpFilter, dev_hfps);

	//wfp to lfp
	FilterByMatlab(rawStream, channels, oneChannelSamples, dev_wfps, matlabLfpFilterOrder, dev_matlabLfpPrevs, dev_matlabLfpFilter, dev_lfps);

	//
	Threshold(rawStream, channels, oneChannelSamples);

	//transpose	
	Transpose(rawStream, dev_wfps, dev_wfps_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_lfps, dev_lfps_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_hfps, dev_hfps_transpose, oneChannelSamples, channels);
	Transpose(rawStream, dev_spikes, dev_spikes_transpose, oneChannelSamples, channels);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(rawStream));

	//
	CUDA_CHECK_ERROR(cudaStreamDestroy(rawStream));

	//
	CUDA_CHECK_ERROR(cudaDeviceSynchronize());

	//
	CUDA_CHECK_ERROR(cudaMemcpy(wfps, dev_wfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(hfps, dev_hfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(lfps, dev_lfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(spikes, dev_spikes_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
}

void ProcessWFPChunkByMatlab(int channels, int oneChannelSamples, float wfps[], float hfps[], float lfps[], float spikes[])
{
	//
	int sampleCount = channels * oneChannelSamples;

	//
	CUDA_CHECK_ERROR(cudaMemcpy(dev_wfps, wfps, sampleCount * sizeof(float), cudaMemcpyHostToDevice));

	//
	cudaStream_t rawStream, wfpStream, hfpStream, lfpStream, spikeStream;
	CUDA_CHECK_ERROR(cudaStreamCreate(&(rawStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(wfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(hfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(lfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(spikeStream)));

	//wfp to hfp
	FilterByMatlab(hfpStream, channels, oneChannelSamples, dev_wfps, matlabHfpFilterOrder, dev_matlabHfpPrevs, dev_matlabHfpFilter, dev_hfps);

	//wfp to lfp
	FilterByMatlab(lfpStream, channels, oneChannelSamples, dev_wfps, matlabLfpFilterOrder, dev_matlabLfpPrevs, dev_matlabLfpFilter, dev_lfps);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(lfpStream));

	//
	Threshold(hfpStream, channels, oneChannelSamples);
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));

	//transpose	
	Transpose(wfpStream, dev_wfps, dev_wfps_transpose, oneChannelSamples, channels);
	Transpose(lfpStream, dev_lfps, dev_lfps_transpose, oneChannelSamples, channels);
	Transpose(hfpStream, dev_hfps, dev_hfps_transpose, oneChannelSamples, channels);
	Transpose(spikeStream, dev_spikes, dev_spikes_transpose, oneChannelSamples, channels);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(rawStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(wfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(lfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(spikeStream));

	//
	CUDA_CHECK_ERROR(cudaStreamDestroy(rawStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(wfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(lfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(spikeStream));

	//
	CUDA_CHECK_ERROR(cudaDeviceSynchronize());

	//
	CUDA_CHECK_ERROR(cudaMemcpy(wfps, dev_wfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(hfps, dev_hfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(lfps, dev_lfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(spikes, dev_spikes_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
}


