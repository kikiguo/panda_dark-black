﻿#include "Exports.h"
#include "Globals.cuh"

#include "../Kernels/GlobalKernels/AD2Raw.cuh"
#include "../Kernels/GlobalKernels/HFP_Square_Sum.cuh"
#include "../Kernels/GlobalKernels/Spike.cuh"
#include "../Kernels/GlobalKernels/Spike_Count.cuh"
#include "../Kernels/GlobalKernels/Transpose.cuh"


float raw_k = 0;
int raw_b = 0;

float *dev_raws = 0;
unsigned short *dev_ads = 0;
float *dev_spikes = 0;

float *dev_raws_transpose = 0;
float *dev_spikes_transpose = 0;

int *dev_prevSpikeIndex = 0;
float *dev_thresholds = 0;
int spike_gap = 0;
bool all_threshold_same = true;
float unified_threshold = -1;
bool threshold_positive = false;

float *dev_wfps = 0;
float *dev_hfps = 0;
float *dev_lfps = 0;

float *dev_wfps_transpose = 0;
float *dev_hfps_transpose = 0;
float *dev_lfps_transpose = 0;

//初始化cuda
int Init(int deviceIndex)
{
	return cudaSetDevice(deviceIndex); // 选择用于运行的GPU
}

//释放cuda
void Reset()
{
	cudaDeviceReset();
}

//
void InitRawParam(int channels, int oneChannelSamples, float k, int b)
{
	raw_k = k;
	raw_b = b;
	if (dev_ads != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_ads));
		dev_ads = 0;
	}
	if (dev_raws != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_raws));
		dev_raws = 0;
	}
	if (dev_raws_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_raws_transpose));
		dev_raws_transpose = 0;
	}

	cudaMalloc((void**)&dev_ads, channels * oneChannelSamples * sizeof(unsigned short));
	cudaMalloc((void**)&dev_raws, channels * oneChannelSamples * sizeof(float));
	cudaMalloc((void**)&dev_raws_transpose, channels * oneChannelSamples * sizeof(float));
}

//
void InitSpikeParams(int channels, int oneChannelSamples, int gap, float thresholds[])
{
	spike_gap = gap;
	all_threshold_same = false;

	//
	if (dev_spikes != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_spikes));
		dev_spikes = 0;
	}
	if (dev_spikes_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_spikes_transpose));
		dev_spikes_transpose = 0;
	}
	if (dev_prevSpikeIndex != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_prevSpikeIndex));
		dev_prevSpikeIndex = 0;
	}
	if (dev_thresholds != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_thresholds));
		dev_thresholds = 0;
	}

	//
	cudaMalloc((void**)&dev_spikes, channels * oneChannelSamples * sizeof(float));
	cudaMalloc((void**)&dev_spikes_transpose, channels * oneChannelSamples * sizeof(float));

	//
	cudaMalloc((void**)&dev_prevSpikeIndex, channels * sizeof(int));
	cudaMemset(dev_prevSpikeIndex, -gap * 2, channels * sizeof(int));

	//
	cudaMalloc((void**)&dev_thresholds, channels * sizeof(float));
	cudaMemcpy(dev_thresholds, thresholds, channels * sizeof(float), cudaMemcpyHostToDevice);
}

//
void InitSpikeParam(int channels, int oneChannelSamples, int gap, float threshold)
{
	spike_gap = gap;
	unified_threshold = threshold;
	all_threshold_same = true;
	threshold_positive = threshold > 0;

	//
	if (dev_spikes != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_spikes));
		dev_spikes = 0;
	}
	if (dev_spikes_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_spikes_transpose));
		dev_spikes_transpose = 0;
	}
	if (dev_prevSpikeIndex != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_prevSpikeIndex));
		dev_prevSpikeIndex = 0;
	}
	if (dev_thresholds != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_thresholds));
		dev_thresholds = 0;
	}

	//
	cudaMalloc((void**)&dev_spikes, channels * oneChannelSamples * sizeof(float));
	cudaMalloc((void**)&dev_spikes_transpose, channels * oneChannelSamples * sizeof(float));

	//
	cudaMalloc((void**)&dev_prevSpikeIndex, channels * sizeof(int));
	cudaMemset(dev_prevSpikeIndex, -gap * 2, channels * sizeof(int));
}

//
void InitBuffer(int channels, int oneChannelSamples, float** d_datas, float ** d_transpose)
{
	//
	cudaMalloc((void**)d_datas, channels * oneChannelSamples * sizeof(float));
	cudaMalloc((void**)d_transpose, channels * oneChannelSamples * sizeof(float));
}

//
void FreeBee()
{
	FreeIntanBuffer();
	FreeMatlabBuffer();

	if (dev_ads != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_ads));
		dev_ads = 0;
	}
	if (dev_raws != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_raws));
		dev_raws = 0;
	}
	if (dev_wfps != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_wfps));
		dev_wfps = 0;
	}
	if (dev_lfps != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_lfps));
		dev_lfps = 0;
	}
	if (dev_hfps != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_hfps));
		dev_hfps = 0;
	}


	if (dev_raws_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_raws_transpose));
		dev_raws_transpose = 0;
	}
	if (dev_wfps_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_wfps_transpose));
		dev_wfps_transpose = 0;
	}
	if (dev_lfps_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_lfps_transpose));
		dev_lfps_transpose = 0;
	}
	if (dev_hfps_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_hfps_transpose));
		dev_hfps_transpose = 0;
	}


	if (dev_prevSpikeIndex != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_prevSpikeIndex));
		dev_prevSpikeIndex = 0;
	}
	if (dev_thresholds != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_thresholds));
		dev_thresholds = 0;
	}
	if (dev_spikes != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_spikes));
		dev_spikes = 0;
	}
	if (dev_spikes_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_spikes_transpose));
		dev_spikes_transpose = 0;
	}
}


//
void AD2Raw(cudaStream_t stream, const unsigned short *adshort, int N, float k, int b, float* raws)
{
	int blockSize = 1024;
	int gridSize = (N + blockSize - 1) / blockSize;
	AD2RawKernel << <gridSize, blockSize, 0, stream >> > (adshort, N, k, b, raws);
}

//
void Transpose(cudaStream_t stream, float* src, float* dst, int m, int n)
{
	dim3 block = { BLOCK_SIZE, BLOCK_SIZE,1 };
	dim3 grid = { (unsigned int)(n - 1 + BLOCK_SIZE) / BLOCK_SIZE, (unsigned int)(m - 1 + BLOCK_SIZE) / BLOCK_SIZE,1 };
	Transpose << <grid, block, BLOCK_SIZE*(BLOCK_SIZE + 1), stream >> > (src, dst, m, n);
}

//spike
void Threshold(cudaStream_t stream, int channels, int oneChannelSamples)
{
	int blockSize = 64;
	int gridSize = (channels + blockSize - 1) / blockSize;
	cudaMemset(dev_spikes, 0, channels *oneChannelSamples * sizeof(float));

	//
	if (all_threshold_same)
	{
		if (threshold_positive)
		{
			ThresholdPositiveAllSame << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, dev_hfps, dev_prevSpikeIndex, unified_threshold, spike_gap, dev_spikes);
		}
		else
		{
			ThresholdNegativeAllSame << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, dev_hfps, dev_prevSpikeIndex, unified_threshold, spike_gap, dev_spikes);
		}
	}
	else
	{
		ThresholdSeparately << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, dev_hfps, dev_prevSpikeIndex, dev_thresholds, spike_gap, dev_spikes);
	}
}
