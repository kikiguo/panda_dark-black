#include "Exports.h"
#include "Globals.cuh"
#include "../Kernels/IntanKernels/IntanFilterLayer0.cuh"
#include "../Kernels/IntanKernels/IntanFilterLayer1.cuh"
#include "../Kernels/IntanKernels/IntanFilterLayer2.cuh"
#include "../Kernels/IntanKernels/IntanFilterLayer3.cuh"
#include "../Kernels/IntanKernels/IntanFilterLayer4.cuh"

//
int intanNotchFilterCount = 0;
int intanHfpFilterCount = 0;
int intanLfpFilterCount = 0;

float *dev_intanWfpPrevs = 0;
float *dev_intanLfpPrevs = 0;
float *dev_intanHfpPrevs = 0;

float *dev_intanWfpFilter = 0;
float *dev_intanLfpFilter = 0;
float *dev_intanHfpFilter = 0;


//
void InitIntanFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[], float** d_prevs, float** d_filter, float** d_datas, float ** d_transpose)
{
	if (*d_prevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_prevs));
		*d_prevs = 0;
	}
	if (*d_filter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_filter));
		*d_filter = 0;
	}
	if (*d_datas != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_datas));
		*d_datas = 0;
	}
	if (*d_transpose != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(*d_transpose));
		*d_transpose = 0;
	}

	//
	int prevByteCount = channels * (filterCount + 1) * 2 * sizeof(float); //
	cudaMalloc((void**)d_prevs, prevByteCount);
	cudaMemset(*d_prevs, 0, prevByteCount);

	//
	cudaMalloc((void**)d_filter, filterCount * 5 * sizeof(float)); //each filter has five args：b0, b1, b2, a2, a1
	cudaMemcpy(*d_filter, filterParam, filterCount * 5 * sizeof(float), cudaMemcpyHostToDevice);

	//
	InitBuffer(channels, oneChannelSamples, d_datas, d_transpose);
}

//
void InitIntanNotchFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[])
{
	intanNotchFilterCount = filterCount;
	InitIntanFilterParam(channels, oneChannelSamples, filterCount, filterParam, &dev_intanWfpPrevs, &dev_intanWfpFilter, &dev_wfps, &dev_wfps_transpose);
}

//
void InitIntanHfpFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[])
{
	intanHfpFilterCount = filterCount;
	InitIntanFilterParam(channels, oneChannelSamples, filterCount, filterParam, &dev_intanHfpPrevs, &dev_intanHfpFilter, &dev_hfps, &dev_hfps_transpose);
}

//
void InitIntanLfpFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[])
{
	intanLfpFilterCount = filterCount;
	InitIntanFilterParam(channels, oneChannelSamples, filterCount, filterParam, &dev_intanLfpPrevs, &dev_intanLfpFilter, &dev_lfps, &dev_lfps_transpose);
}

//
void FreeIntanBuffer()
{
	if (dev_intanWfpPrevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_intanWfpPrevs));
		dev_intanWfpPrevs = 0;
	}
	if (dev_intanLfpPrevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_intanLfpPrevs));
		dev_intanLfpPrevs = 0;
	}
	if (dev_intanHfpPrevs != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_intanHfpPrevs));
		dev_intanHfpPrevs = 0;
	}

	if (dev_intanWfpFilter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_intanWfpFilter));
		dev_intanWfpFilter = 0;
	}
	if (dev_intanLfpFilter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_intanLfpFilter));
		dev_intanLfpFilter = 0;
	}
	if (dev_intanHfpFilter != 0)
	{
		CUDA_CHECK_ERROR(cudaFree(dev_intanHfpFilter));
		dev_intanHfpFilter = 0;
	}
}



//
void FilterByIntan(cudaStream_t stream, int channels, int oneChannelSamples, const float* srcs, int filterCount, float* prevs, float* filter, float* fps)
{
	int blockSize = 64;
	int gridSize = (channels + blockSize - 1) / blockSize;
	if (filterCount == 1)
	{
		IntanFilter1Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	else if (filterCount == 2)
	{
		IntanFilter2Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	else if (filterCount == 3)
	{
		IntanFilter3Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	else if (filterCount == 4)
	{
		IntanFilter4Kernel << <gridSize, blockSize, 0, stream >> > (channels, oneChannelSamples, srcs, prevs, filter, fps);
	}
	else if (filterCount == 0)
	{
		blockSize = 1024;
		gridSize = (channels*oneChannelSamples + blockSize - 1) / blockSize;
		IntanFilter0Kernel << <gridSize, blockSize, 0, stream >> > (channels*oneChannelSamples, srcs, fps);
	}
}

//处理数据块
void ProcessADChunkByIntan(int adoffset, unsigned char adbytes[], int channels, int oneChannelSamples, float raws[], float wfps[], float hfps[], float lfps[], float spikes[])
{
	//
	int sampleCount = channels * oneChannelSamples;

	//
	CUDA_CHECK_ERROR(cudaMemcpy(dev_ads, adbytes + adoffset, sampleCount * sizeof(unsigned short), cudaMemcpyHostToDevice));

	//
	cudaStream_t rawStream, wfpStream, hfpStream, lfpStream, spikeStream;
	CUDA_CHECK_ERROR(cudaStreamCreate(&(rawStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(wfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(hfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(lfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(spikeStream)));

	//ad -> raw -> wfp
	{
		//ad to raw
		AD2Raw(rawStream, (unsigned short*)dev_ads, sampleCount, raw_k, raw_b, dev_raws);

		//raw to wfp
		FilterByIntan(rawStream, channels, oneChannelSamples, dev_raws, intanNotchFilterCount, dev_intanWfpPrevs, dev_intanWfpFilter, dev_wfps);
	}

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(rawStream));

	//wfp to hfp
	FilterByIntan(hfpStream, channels, oneChannelSamples, dev_wfps, intanHfpFilterCount, dev_intanHfpPrevs, dev_intanHfpFilter, dev_hfps);

	//wfp to lfp
	FilterByIntan(lfpStream, channels, oneChannelSamples, dev_wfps, intanLfpFilterCount, dev_intanLfpPrevs, dev_intanLfpFilter, dev_lfps);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(lfpStream));

	//
	Threshold(hfpStream, channels, oneChannelSamples);
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));

	//transpose	
	Transpose(rawStream, dev_raws, dev_raws_transpose, oneChannelSamples, channels);
	Transpose(wfpStream, dev_wfps, dev_wfps_transpose, oneChannelSamples, channels);
	Transpose(lfpStream, dev_lfps, dev_lfps_transpose, oneChannelSamples, channels);
	Transpose(hfpStream, dev_hfps, dev_hfps_transpose, oneChannelSamples, channels);
	Transpose(spikeStream, dev_spikes, dev_spikes_transpose, oneChannelSamples, channels);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(rawStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(wfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(lfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(spikeStream));

	//
	CUDA_CHECK_ERROR(cudaStreamDestroy(rawStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(wfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(lfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(spikeStream));

	//
	CUDA_CHECK_ERROR(cudaDeviceSynchronize());

	//
	CUDA_CHECK_ERROR(cudaMemcpy(raws, dev_raws_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(wfps, dev_wfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(hfps, dev_hfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(lfps, dev_lfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(spikes, dev_spikes_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
}


//处理数据块
void ProcessWFPChunkByIntan(int channels, int oneChannelSamples, float wfps[], float hfps[], float lfps[], float spikes[])
{
	//
	int sampleCount = channels * oneChannelSamples;

	//
	CUDA_CHECK_ERROR(cudaMemcpy(dev_wfps, wfps, sampleCount * sizeof(float), cudaMemcpyHostToDevice));

	//
	cudaStream_t rawStream, wfpStream, hfpStream, lfpStream, spikeStream;
	CUDA_CHECK_ERROR(cudaStreamCreate(&(rawStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(wfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(hfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(lfpStream)));
	CUDA_CHECK_ERROR(cudaStreamCreate(&(spikeStream)));

	//wfp to hfp
	FilterByIntan(hfpStream, channels, oneChannelSamples, dev_wfps, intanHfpFilterCount, dev_intanHfpPrevs, dev_intanHfpFilter, dev_hfps);

	//wfp to lfp
	FilterByIntan(lfpStream, channels, oneChannelSamples, dev_wfps, intanLfpFilterCount, dev_intanLfpPrevs, dev_intanLfpFilter, dev_lfps);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(lfpStream));

	//
	Threshold(hfpStream, channels, oneChannelSamples);
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));

	//transpose	
	Transpose(wfpStream, dev_wfps, dev_wfps_transpose, oneChannelSamples, channels);
	Transpose(lfpStream, dev_lfps, dev_lfps_transpose, oneChannelSamples, channels);
	Transpose(hfpStream, dev_hfps, dev_hfps_transpose, oneChannelSamples, channels);
	Transpose(spikeStream, dev_spikes, dev_spikes_transpose, oneChannelSamples, channels);

	//
	CUDA_CHECK_ERROR(cudaStreamSynchronize(rawStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(wfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(lfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamSynchronize(spikeStream));

	//
	CUDA_CHECK_ERROR(cudaStreamDestroy(rawStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(wfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(hfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(lfpStream));
	CUDA_CHECK_ERROR(cudaStreamDestroy(spikeStream));

	//
	CUDA_CHECK_ERROR(cudaDeviceSynchronize());

	//
	CUDA_CHECK_ERROR(cudaMemcpy(wfps, dev_wfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(hfps, dev_hfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(lfps, dev_lfps_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
	CUDA_CHECK_ERROR(cudaMemcpy(spikes, dev_spikes_transpose, sampleCount * sizeof(float), cudaMemcpyDeviceToHost));
}


