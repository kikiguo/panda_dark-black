#pragma once

#include <stdio.h>
#include "cuda_runtime.h"  
#include "device_launch_parameters.h"

#define CUDA_CHECK_ERROR(call)\
do{\
	cudaError_t _error = (cudaError_t)(call);\
	if(_error != cudaSuccess)\
	{\
		printf("*** CUDA Error *** at [%s:%d] error=%d, reason:%s \n",\
			__FILE__, __LINE__, _error, cudaGetErrorString(_error));\
		return;\
	}\
}while(0)


extern float raw_k;
extern int raw_b;

//Intan
extern int intanNotchFilterCount;
extern int intanHfpFilterCount;
extern int intanLfpFilterCount;
//Intan
extern float *dev_intanWfpPrevs;
extern float *dev_intanHfpPrevs;
extern float *dev_intanLfpPrevs;
//Intan
extern float *dev_intanWfpFilter;
extern float *dev_intanLfpFilter;
extern float *dev_intanHfpFilter;

//Matlab
extern int matlabNotchFilterOrder;
extern int matlabHfpFilterOrder; //matlab�˲� b1 b2 ...bn  a1 a2 ... an
extern int matlabLfpFilterOrder; //matlab�˲� b1 b2 ...bn  a1 a2 ... an
//Matlab
extern double *dev_matlabWfpPrevs;
extern double *dev_matlabLfpPrevs;
extern double *dev_matlabHfpPrevs;
//Matlab	   
extern double *dev_matlabWfpFilter;
extern double *dev_matlabLfpFilter;
extern double *dev_matlabHfpFilter;

//public
extern int *dev_prevSpikeIndex;
extern float *dev_thresholds;
extern int spike_gap;
extern bool all_threshold_same;
extern float unified_threshold;
extern bool threshold_positive;

//public
extern unsigned short *dev_ads;
extern float *dev_raws;
extern float *dev_wfps;
extern float *dev_hfps;
extern float *dev_lfps;
extern float *dev_spikes;

//public
extern float *dev_wfps_transpose;
extern float *dev_hfps_transpose;
extern float *dev_lfps_transpose;
extern float *dev_raws_transpose;
extern float *dev_spikes_transpose;


extern void InitBuffer(int channels, int oneChannelSamples, float** d_datas, float ** d_transpose);
extern void FreeIntanBuffer();
extern void FreeMatlabBuffer();
extern void AD2Raw(cudaStream_t stream, const unsigned short *adshort, int N, float k, int b, float* raws);
extern void Transpose(cudaStream_t stream, float* src, float* dst, int m, int n);
extern void Threshold(cudaStream_t stream, int channels, int oneChannelSamples);