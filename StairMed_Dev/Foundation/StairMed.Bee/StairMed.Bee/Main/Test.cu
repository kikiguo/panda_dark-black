#include "Exports.h"
#include "Globals.cuh"


//AddTestKernel
__global__ void AddTestKernel(int N, const int *a, const int *b, int *c)
{
	int n = threadIdx.x + blockDim.x * blockIdx.x;
	if (n < N)
	{
		c[n] = a[n] + b[n];
	}
}

bool IsSupportCUDA()
{
	int dev_num = 0;
	cudaError_t error_id = cudaGetDeviceCount(&dev_num);
	if (error_id != cudaSuccess)
	{
		return false;
	}

	// This function call returns 0 if there are no CUDA capable devices.
	if (dev_num == 0)
	{
		return false;
	}
	else
	{
		return true;
	}

}

void AddTest(int len, int a[], int b[], int c[])
{
	//
	int *dev_a = 0;
	cudaMalloc((void**)&dev_a, len * sizeof(int));
	CUDA_CHECK_ERROR(cudaMemcpy(dev_a, a, len * sizeof(int), cudaMemcpyHostToDevice));

	//
	int *dev_b = 0;
	cudaMalloc((void**)&dev_b, len * sizeof(int));
	CUDA_CHECK_ERROR(cudaMemcpy(dev_b, b, len * sizeof(int), cudaMemcpyHostToDevice));

	//
	int *dev_c = 0;
	cudaMalloc((void**)&dev_c, len * sizeof(int));

	//
	int blockSize = 1024;
	int gridSize = (len + blockSize - 1) / blockSize;
	AddTestKernel << <gridSize, blockSize >> > (len, dev_a, dev_b, dev_c);

	//
	CUDA_CHECK_ERROR(cudaMemcpy(c, dev_c, len * sizeof(int), cudaMemcpyDeviceToHost));

	//
	CUDA_CHECK_ERROR(cudaFree(dev_a));
	CUDA_CHECK_ERROR(cudaFree(dev_b));
	CUDA_CHECK_ERROR(cudaFree(dev_c));
}