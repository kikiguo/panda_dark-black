﻿#pragma once

#ifdef __DLLEXPORT
#define __DLL_EXP _declspec(dllexport)    // 导出函数 - 生成dll文件时使用
#else
#define __DLL_EXP _declspec(dllimport)    // 导入函数 -使用dll是使用
#endif



#if defined(__cplusplus) || defined(c_plusplus)
extern "C"
{
#endif

	__DLL_EXP int Init(int deviceIndex);
	__DLL_EXP void Reset();


	__DLL_EXP void InitRawParam(int channels, int oneChannelSamples, float k, int b);

	__DLL_EXP void InitSpikeParams(int channels, int oneChannelSamples, int gap, float thresholds[]);
	__DLL_EXP void InitSpikeParam(int channels, int oneChannelSamples, int gap, float threshold);

	__DLL_EXP void InitIntanNotchFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[]);
	__DLL_EXP void InitIntanLfpFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[]);
	__DLL_EXP void InitIntanHfpFilterParam(int channels, int oneChannelSamples, int filterCount, float filterParam[]);

	__DLL_EXP void ProcessADChunkByIntan(int adoffset, unsigned char adbytes[], int channels, int oneChannelSamples, float raws[], float wfps[], float hfps[], float lfps[], float spikes[]);
	__DLL_EXP void ProcessWFPChunkByIntan(int channels, int oneChannelSamples, float wfps[], float hfps[], float lfps[], float spikes[]);


	__DLL_EXP void InitMatlabNotchFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[]);
	__DLL_EXP void InitMatlabLfpFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[]);
	__DLL_EXP void InitMatlabHfpFilterParam(int channels, int oneChannelSamples, int filterParamCount, double filterParam[]);

	__DLL_EXP void ProcessADChunkByMatlab(int adoffset, unsigned char adbytes[], int channels, int oneChannelSamples, float raws[], float wfps[], float hfps[], float lfps[], float spikes[]);
	__DLL_EXP void ProcessADChunkByMatlab2(int adoffset, unsigned char adbytes[], int channels, int oneChannelSamples, float wfps[], float hfps[], float lfps[], float spikes[]);
	__DLL_EXP void ProcessWFPChunkByMatlab(int channels, int oneChannelSamples, float wfps[], float hfps[], float lfps[], float spikes[]);

	__DLL_EXP bool IsSupportCUDA();

	__DLL_EXP void AddTest(int len, int a[], int b[], int c[]);

	__DLL_EXP void FreeBee();

#if defined(__cplusplus)||defined(c_plusplus)
}
#endif

