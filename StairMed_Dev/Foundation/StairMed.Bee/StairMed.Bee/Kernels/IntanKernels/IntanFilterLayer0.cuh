#pragma once

#include "cuda_runtime.h"  
#include "device_launch_parameters.h"  

__global__ void IntanFilter0Kernel(int N, const float* srcs, float* fps);