#pragma once

#include "cuda_runtime.h"  
#include "device_launch_parameters.h"  

__global__ void IntanFilter3Kernel(int channels, int oneChannelSamples, const float* srcs, float* prevs, const float* filter, float* fps);