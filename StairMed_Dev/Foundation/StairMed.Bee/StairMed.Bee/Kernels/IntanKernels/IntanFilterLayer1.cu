#include "IntanFilterLayer1.cuh"


#define Filter2_N \
 \
inVal = srcs[idx]; \
outVal = b01 * prev00000; \
outVal += b02 * prevprev0; \
outVal -= a01 * prev11111; \
outVal -= a02 * prevprev1; \
prevprev0 = prev00000; \
prevprev1 = prev11111; \
 \
outVal += b00 * inVal; \
prev00000 = inVal; \
 \
prev11111 = outVal; \
 \
fps[idx] = outVal; 

//filter
__global__ void IntanFilter1Kernel(int channels, int oneChannelSamples, const float* srcs, float* prevs, const float* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		float b00 = filter[0];
		float b01 = filter[1];
		float b02 = filter[2];
		float a02 = filter[3];
		float a01 = filter[4];

		//prev param
		float prevprev0 = (prevs[channel + 0 * channels]);
		float prev00000 = (prevs[channel + 1 * channels]);
		float prevprev1 = (prevs[channel + 2 * channels]);
		float prev11111 = (prevs[channel + 3 * channels]);

		//loop
		{
			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					int idx = ((repeatIndex * channels) << 5);
					idx += channel;
					float inVal = 0;
					float outVal = 0;

					Filter2_N //1
						idx += channels; Filter2_N //2
						idx += channels; Filter2_N //3
						idx += channels; Filter2_N //4
						idx += channels; Filter2_N //5
						idx += channels; Filter2_N //6
						idx += channels; Filter2_N //7
						idx += channels; Filter2_N //8
						idx += channels; Filter2_N //9
						idx += channels; Filter2_N //10
						idx += channels; Filter2_N //11
						idx += channels; Filter2_N //12
						idx += channels; Filter2_N //13
						idx += channels; Filter2_N //14
						idx += channels; Filter2_N //15
						idx += channels; Filter2_N //16
						idx += channels; Filter2_N //17
						idx += channels; Filter2_N //18
						idx += channels; Filter2_N //19
						idx += channels; Filter2_N //20
						idx += channels; Filter2_N //21
						idx += channels; Filter2_N //22
						idx += channels; Filter2_N //23
						idx += channels; Filter2_N //24
						idx += channels; Filter2_N //25
						idx += channels; Filter2_N //26
						idx += channels; Filter2_N //27
						idx += channels; Filter2_N //28
						idx += channels; Filter2_N //29
						idx += channels; Filter2_N //30
						idx += channels; Filter2_N //31
						idx += channels; Filter2_N //32
				}
			}
			else if ((oneChannelSamples & 0x0F) == 0)
			{
				int repeat = oneChannelSamples >> 4;
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					int idx = ((repeatIndex * channels) << 4);
					idx += channel;
					float inVal = 0;
					float outVal = 0;

					Filter2_N //1
						idx += channels; Filter2_N //2
						idx += channels; Filter2_N //3
						idx += channels; Filter2_N //4
						idx += channels; Filter2_N //5
						idx += channels; Filter2_N //6
						idx += channels; Filter2_N //7
						idx += channels; Filter2_N //8
						idx += channels; Filter2_N //9
						idx += channels; Filter2_N //10
						idx += channels; Filter2_N //11
						idx += channels; Filter2_N //12
						idx += channels; Filter2_N //13
						idx += channels; Filter2_N //14
						idx += channels; Filter2_N //15
						idx += channels; Filter2_N //16
				}
			}
			else
			{
				int repeat = oneChannelSamples;
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					int idx = repeatIndex * channels + channel;
					float inVal = 0;
					float outVal = 0;

					Filter2_N
				}
			}
		}

		//save prev data back
		prevs[channel + 0 * channels] = prevprev0;
		prevs[channel + 1 * channels] = prev00000;
		prevs[channel + 2 * channels] = prevprev1;
		prevs[channel + 3 * channels] = prev11111;
	}
}

