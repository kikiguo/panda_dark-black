#include "IntanFilterLayer4.cuh"

#define Filter4_N\
 \
inVal = srcs[idx]; \
 \
outVal = b00 * inVal; \
outVal += b01 * prev00000; \
outVal += b02 * prevprev0; \
outVal -= a01 * prev11111; \
outVal -= a02 * prevprev1; \
prevprev0 = prev00000; \
prev00000 = inVal; \
 \
inVal = outVal; \
outVal = b10 * inVal; \
outVal += b11 * prev11111; \
outVal += b12 * prevprev1; \
outVal -= a11 * prev22222; \
outVal -=a12 * prevprev2; \
prevprev1 = prev11111; \
prev11111 = inVal; \
 \
inVal = outVal; \
outVal = b20 * inVal; \
outVal += b21 * prev22222; \
outVal += b22 * prevprev2; \
outVal -= a21 * prev33333; \
outVal -= a22 * prevprev3; \
prevprev2 = prev22222; \
prev22222 = inVal; \
 \
inVal = outVal; \
outVal = b30 * inVal; \
outVal += b31 * prev33333; \
outVal += b32 * prevprev3; \
outVal -= a31 * prev44444; \
outVal -=a32 * prevprev4; \
prevprev3 = prev33333; \
prev33333 = inVal; \
 \
prevprev4 = prev44444; \
prev44444 = outVal; \
 \
fps[idx] = outVal; 


//1.#pragma unroll not work(nsight compute)
//2.__device__ __forceinline__ lead used register increase
//3.manual unroll can speed up
//4.

//filter
__global__ void IntanFilter4Kernel(int channels, int oneChannelSamples, const float* srcs, float* prevs, const float* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		float b00 = filter[0];
		float b01 = filter[1];
		float b02 = filter[2];
		float a02 = filter[3];
		float a01 = filter[4];
		float b10 = filter[5];
		float b11 = filter[6];
		float b12 = filter[7];
		float a12 = filter[8];
		float a11 = filter[9];
		float b20 = filter[10];
		float b21 = filter[11];
		float b22 = filter[12];
		float a22 = filter[13];
		float a21 = filter[14];
		float b30 = filter[15];
		float b31 = filter[16];
		float b32 = filter[17];
		float a32 = filter[18];
		float a31 = filter[19];

		//prev param
		float prevprev0 = (prevs[channel + 0 * channels]);
		float prev00000 = (prevs[channel + 1 * channels]);
		float prevprev1 = (prevs[channel + 2 * channels]);
		float prev11111 = (prevs[channel + 3 * channels]);
		float prevprev2 = (prevs[channel + 4 * channels]);
		float prev22222 = (prevs[channel + 5 * channels]);
		float prevprev3 = (prevs[channel + 6 * channels]);
		float prev33333 = (prevs[channel + 7 * channels]);
		float prevprev4 = (prevs[channel + 8 * channels]);
		float prev44444 = (prevs[channel + 9 * channels]);

		//loop
		{
			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					int idx = ((repeatIndex * channels) << 5);
					idx += channel;
					float inVal = 0;
					float outVal = 0;

					Filter4_N //1
						idx += channels; Filter4_N //2
						idx += channels; Filter4_N //3
						idx += channels; Filter4_N //4
						idx += channels; Filter4_N //5
						idx += channels; Filter4_N //6
						idx += channels; Filter4_N //7
						idx += channels; Filter4_N //8
						idx += channels; Filter4_N //9
						idx += channels; Filter4_N //10
						idx += channels; Filter4_N //11
						idx += channels; Filter4_N //12
						idx += channels; Filter4_N //13
						idx += channels; Filter4_N //14
						idx += channels; Filter4_N //15
						idx += channels; Filter4_N //16
						idx += channels; Filter4_N //17
						idx += channels; Filter4_N //18
						idx += channels; Filter4_N //19
						idx += channels; Filter4_N //20
						idx += channels; Filter4_N //21
						idx += channels; Filter4_N //22
						idx += channels; Filter4_N //23
						idx += channels; Filter4_N //24
						idx += channels; Filter4_N //25
						idx += channels; Filter4_N //26
						idx += channels; Filter4_N //27
						idx += channels; Filter4_N //28
						idx += channels; Filter4_N //29
						idx += channels; Filter4_N //30
						idx += channels; Filter4_N //31
						idx += channels; Filter4_N //32
				}
			}
			else if ((oneChannelSamples & 0x0F) == 0)
			{
				int repeat = oneChannelSamples >> 4;
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					int idx = ((repeatIndex * channels) << 4);
					idx += channel;
					float inVal = 0;
					float outVal = 0;

					Filter4_N //1
						idx += channels; Filter4_N //2
						idx += channels; Filter4_N //3
						idx += channels; Filter4_N //4
						idx += channels; Filter4_N //5
						idx += channels; Filter4_N //6
						idx += channels; Filter4_N //7
						idx += channels; Filter4_N //8
						idx += channels; Filter4_N //9
						idx += channels; Filter4_N //10
						idx += channels; Filter4_N //11
						idx += channels; Filter4_N //12
						idx += channels; Filter4_N //13
						idx += channels; Filter4_N //14
						idx += channels; Filter4_N //15
						idx += channels; Filter4_N //16
				}
			}
			else
			{
				int repeat = oneChannelSamples;
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					int idx = repeatIndex * channels + channel;
					float inVal = 0;
					float outVal = 0;

					Filter4_N //1
				}
			}
		}

		//save prev data back
		prevs[channel + 0 * channels] = prevprev0;
		prevs[channel + 1 * channels] = prev00000;
		prevs[channel + 2 * channels] = prevprev1;
		prevs[channel + 3 * channels] = prev11111;
		prevs[channel + 4 * channels] = prevprev2;
		prevs[channel + 5 * channels] = prev22222;
		prevs[channel + 6 * channels] = prevprev3;
		prevs[channel + 7 * channels] = prev33333;
		prevs[channel + 8 * channels] = prevprev4;
		prevs[channel + 9 * channels] = prev44444;
	}
}

