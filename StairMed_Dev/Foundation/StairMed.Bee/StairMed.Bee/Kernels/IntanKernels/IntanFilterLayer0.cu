#include "IntanFilterLayer0.cuh"

//filter
__global__ void IntanFilter0Kernel(int N, const float* srcs, float* fps)
{
	int idx = threadIdx.x + blockDim.x * blockIdx.x;
	if (idx < N)
	{
		fps[idx] = srcs[idx];
	}
}