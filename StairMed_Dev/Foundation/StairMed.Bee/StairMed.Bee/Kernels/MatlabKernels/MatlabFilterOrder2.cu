#include "MatlabFilterOrder2.cuh"

#define FilterOrder2 \
 \
inVal = srcs[idx]; \
outVal = b0 * inVal; \
 \
outVal += b1 * preX0; \
outVal += b2 * preX1; \
outVal -= a1 * preY0; \
outVal -= a2 * preY1; \
 \
preX1 = preX0; \
preX0 = inVal; \
preY1 = preY0; \
preY0 = outVal; \
 \
fps[idx] = outVal; \

//filter
__global__ void MatlabFilterOrder2Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		double b0 = filter[0];
		double b1 = filter[1];
		double b2 = filter[2];
		//double a0 = filter[3];
		double a1 = filter[4];
		double a2 = filter[5];

		double preX0 = prevs[channel + 0 * channels];
		double preX1 = prevs[channel + 1 * channels];
		double preY0 = prevs[channel + 2 * channels];
		double preY1 = prevs[channel + 3 * channels];

		//loop
		{
			float inVal = 0;
			double outVal = 0;

			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder2; idx += channels;	//1
					FilterOrder2; idx += channels;	//2
					FilterOrder2; idx += channels;	//3
					FilterOrder2; idx += channels;	//4
					FilterOrder2; idx += channels;	//5
					FilterOrder2; idx += channels;	//6
					FilterOrder2; idx += channels;	//7
					FilterOrder2; idx += channels;	//8
					FilterOrder2; idx += channels;	//9
					FilterOrder2; idx += channels;	//10
					FilterOrder2; idx += channels;	//11
					FilterOrder2; idx += channels;	//12
					FilterOrder2; idx += channels;	//13
					FilterOrder2; idx += channels;	//14
					FilterOrder2; idx += channels;	//15
					FilterOrder2; idx += channels;	//16
					FilterOrder2; idx += channels;	//17
					FilterOrder2; idx += channels;	//18
					FilterOrder2; idx += channels;	//19
					FilterOrder2; idx += channels;	//20
					FilterOrder2; idx += channels;	//21
					FilterOrder2; idx += channels;	//22
					FilterOrder2; idx += channels;	//23
					FilterOrder2; idx += channels;	//24
					FilterOrder2; idx += channels;	//25
					FilterOrder2; idx += channels;	//26
					FilterOrder2; idx += channels;	//27
					FilterOrder2; idx += channels;	//28
					FilterOrder2; idx += channels;	//29
					FilterOrder2; idx += channels;	//30
					FilterOrder2; idx += channels;	//31
					FilterOrder2; idx += channels;	//32
				}
			}
			else
			{
				//
				int repeat = oneChannelSamples;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder2; idx += channels;
				}
			}
		}

		//
		prevs[channel + 0 * channels] = preX0;
		prevs[channel + 1 * channels] = preX1;
		prevs[channel + 2 * channels] = preY0;
		prevs[channel + 3 * channels] = preY1;
	}
}





