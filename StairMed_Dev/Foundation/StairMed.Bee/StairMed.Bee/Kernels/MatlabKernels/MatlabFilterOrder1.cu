#include "MatlabFilterOrder1.cuh"

#define FilterOrder1 \
 \
inVal = srcs[idx]; \
outVal = b0 * inVal; \
 \
outVal += b1 * preX0; \
outVal -= a1 * preY0; \
 \
preX0 = inVal; \
preY0 = outVal; \
 \
fps[idx] = outVal; \

//filter
__global__ void MatlabFilterOrder1Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		double b0 = filter[0];
		double b1 = filter[1];
		//double a0 = filter[2];
		double a1 = filter[3];

		double preX0 = prevs[channel + 0 * channels];
		double preY0 = prevs[channel + 1 * channels];

		//loop
		{
			float inVal = 0;
			double outVal = 0;

			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder1; idx += channels;	//1
					FilterOrder1; idx += channels;	//2
					FilterOrder1; idx += channels;	//3
					FilterOrder1; idx += channels;	//4
					FilterOrder1; idx += channels;	//5
					FilterOrder1; idx += channels;	//6
					FilterOrder1; idx += channels;	//7
					FilterOrder1; idx += channels;	//8
					FilterOrder1; idx += channels;	//9
					FilterOrder1; idx += channels;	//10
					FilterOrder1; idx += channels;	//11
					FilterOrder1; idx += channels;	//12
					FilterOrder1; idx += channels;	//13
					FilterOrder1; idx += channels;	//14
					FilterOrder1; idx += channels;	//15
					FilterOrder1; idx += channels;	//16
					FilterOrder1; idx += channels;	//17
					FilterOrder1; idx += channels;	//18
					FilterOrder1; idx += channels;	//19
					FilterOrder1; idx += channels;	//20
					FilterOrder1; idx += channels;	//21
					FilterOrder1; idx += channels;	//22
					FilterOrder1; idx += channels;	//23
					FilterOrder1; idx += channels;	//24
					FilterOrder1; idx += channels;	//25
					FilterOrder1; idx += channels;	//26
					FilterOrder1; idx += channels;	//27
					FilterOrder1; idx += channels;	//28
					FilterOrder1; idx += channels;	//29
					FilterOrder1; idx += channels;	//30
					FilterOrder1; idx += channels;	//31
					FilterOrder1; idx += channels;	//32
				}
			}
			else
			{
				//
				int repeat = oneChannelSamples;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder1; idx += channels;
				}
			}
		}

		//
		prevs[channel + 0 * channels] = preX0;
		prevs[channel + 1 * channels] = preY0;
	}
}





