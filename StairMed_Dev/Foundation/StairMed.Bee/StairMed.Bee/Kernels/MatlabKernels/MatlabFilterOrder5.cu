#include "MatlabFilterOrder5.cuh"

#define FilterOrder5 \
 \
inVal = srcs[idx]; \
outVal = b0 * inVal; \
 \
outVal += b1 * preX0; \
outVal += b2 * preX1; \
outVal += b3 * preX2; \
outVal += b4 * preX3; \
outVal += b5 * preX4; \
outVal -= a1 * preY0; \
outVal -= a2 * preY1; \
outVal -= a3 * preY2; \
outVal -= a4 * preY3; \
outVal -= a5 * preY4; \
 \
preX4 = preX3; \
preX3 = preX2; \
preX2 = preX1; \
preX1 = preX0; \
preX0 = inVal; \
preY4 = preY3; \
preY3 = preY2; \
preY2 = preY1; \
preY1 = preY0; \
preY0 = outVal; \
 \
fps[idx] = outVal; \

//filter
__global__ void MatlabFilterOrder5Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		double b0 = filter[0];
		double b1 = filter[1];
		double b2 = filter[2];
		double b3 = filter[3];
		double b4 = filter[4];
		double b5 = filter[5];
		//double a0 = filter[6];
		double a1 = filter[7];
		double a2 = filter[8];
		double a3 = filter[9];
		double a4 = filter[10];
		double a5 = filter[11];

		double preX0 = prevs[channel + 0 * channels];
		double preX1 = prevs[channel + 1 * channels];
		double preX2 = prevs[channel + 2 * channels];
		double preX3 = prevs[channel + 3 * channels];
		double preX4 = prevs[channel + 4 * channels];
		double preY0 = prevs[channel + 5 * channels];
		double preY1 = prevs[channel + 6 * channels];
		double preY2 = prevs[channel + 7 * channels];
		double preY3 = prevs[channel + 8 * channels];
		double preY4 = prevs[channel + 9 * channels];

		//loop
		{
			float inVal = 0;
			double outVal = 0;

			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder5; idx += channels;	//1
					FilterOrder5; idx += channels;	//2
					FilterOrder5; idx += channels;	//3
					FilterOrder5; idx += channels;	//4
					FilterOrder5; idx += channels;	//5
					FilterOrder5; idx += channels;	//6
					FilterOrder5; idx += channels;	//7
					FilterOrder5; idx += channels;	//8
					FilterOrder5; idx += channels;	//9
					FilterOrder5; idx += channels;	//10
					FilterOrder5; idx += channels;	//11
					FilterOrder5; idx += channels;	//12
					FilterOrder5; idx += channels;	//13
					FilterOrder5; idx += channels;	//14
					FilterOrder5; idx += channels;	//15
					FilterOrder5; idx += channels;	//16
					FilterOrder5; idx += channels;	//17
					FilterOrder5; idx += channels;	//18
					FilterOrder5; idx += channels;	//19
					FilterOrder5; idx += channels;	//20
					FilterOrder5; idx += channels;	//21
					FilterOrder5; idx += channels;	//22
					FilterOrder5; idx += channels;	//23
					FilterOrder5; idx += channels;	//24
					FilterOrder5; idx += channels;	//25
					FilterOrder5; idx += channels;	//26
					FilterOrder5; idx += channels;	//27
					FilterOrder5; idx += channels;	//28
					FilterOrder5; idx += channels;	//29
					FilterOrder5; idx += channels;	//30
					FilterOrder5; idx += channels;	//31
					FilterOrder5; idx += channels;	//32
				}
			}
			else
			{
				//
				int repeat = oneChannelSamples;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder5; idx += channels;
				}
			}
		}

		//
		prevs[channel + 0 * channels] = preX0;
		prevs[channel + 1 * channels] = preX1;
		prevs[channel + 2 * channels] = preX2;
		prevs[channel + 3 * channels] = preX3;
		prevs[channel + 4 * channels] = preX4;
		prevs[channel + 5 * channels] = preY0;
		prevs[channel + 6 * channels] = preY1;
		prevs[channel + 7 * channels] = preY2;
		prevs[channel + 8 * channels] = preY3;
		prevs[channel + 9 * channels] = preY4;
	}
}





