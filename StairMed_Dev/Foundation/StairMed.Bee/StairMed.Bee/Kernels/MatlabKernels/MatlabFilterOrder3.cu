#include "MatlabFilterOrder3.cuh"

#define FilterOrder3 \
 \
inVal = srcs[idx]; \
outVal = b0 * inVal; \
 \
outVal += b1 * preX0; \
outVal += b2 * preX1; \
outVal += b3 * preX2; \
outVal -= a1 * preY0; \
outVal -= a2 * preY1; \
outVal -= a3 * preY2; \
 \
preX2 = preX1; \
preX1 = preX0; \
preX0 = inVal; \
preY2 = preY1; \
preY1 = preY0; \
preY0 = outVal; \
 \
fps[idx] = outVal; \

//filter
__global__ void MatlabFilterOrder3Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		double b0 = filter[0];
		double b1 = filter[1];
		double b2 = filter[2];
		double b3 = filter[3];
		//double a0 = filter[4];
		double a1 = filter[5];
		double a2 = filter[6];
		double a3 = filter[7];

		double preX0 = prevs[channel + 0 * channels];
		double preX1 = prevs[channel + 1 * channels];
		double preX2 = prevs[channel + 2 * channels];
		double preY0 = prevs[channel + 3 * channels];
		double preY1 = prevs[channel + 4 * channels];
		double preY2 = prevs[channel + 5 * channels];

		//loop
		{
			float inVal = 0;
			double outVal = 0;

			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder3; idx += channels;	//1
					FilterOrder3; idx += channels;	//2
					FilterOrder3; idx += channels;	//3
					FilterOrder3; idx += channels;	//4
					FilterOrder3; idx += channels;	//5
					FilterOrder3; idx += channels;	//6
					FilterOrder3; idx += channels;	//7
					FilterOrder3; idx += channels;	//8
					FilterOrder3; idx += channels;	//9
					FilterOrder3; idx += channels;	//10
					FilterOrder3; idx += channels;	//11
					FilterOrder3; idx += channels;	//12
					FilterOrder3; idx += channels;	//13
					FilterOrder3; idx += channels;	//14
					FilterOrder3; idx += channels;	//15
					FilterOrder3; idx += channels;	//16
					FilterOrder3; idx += channels;	//17
					FilterOrder3; idx += channels;	//18
					FilterOrder3; idx += channels;	//19
					FilterOrder3; idx += channels;	//20
					FilterOrder3; idx += channels;	//21
					FilterOrder3; idx += channels;	//22
					FilterOrder3; idx += channels;	//23
					FilterOrder3; idx += channels;	//24
					FilterOrder3; idx += channels;	//25
					FilterOrder3; idx += channels;	//26
					FilterOrder3; idx += channels;	//27
					FilterOrder3; idx += channels;	//28
					FilterOrder3; idx += channels;	//29
					FilterOrder3; idx += channels;	//30
					FilterOrder3; idx += channels;	//31
					FilterOrder3; idx += channels;	//32
				}
			}
			else
			{
				//
				int repeat = oneChannelSamples;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder3; idx += channels;
				}
			}
		}

		//
		prevs[channel + 0 * channels] = preX0;
		prevs[channel + 1 * channels] = preX1;
		prevs[channel + 2 * channels] = preX2;
		prevs[channel + 3 * channels] = preY0;
		prevs[channel + 4 * channels] = preY1;
		prevs[channel + 5 * channels] = preY2;
	}
}





