#include "MatlabFilterOrder4.cuh"

#define FilterOrder4 \
 \
inVal = srcs[idx]; \
outVal = b0 * inVal; \
 \
outVal += b1 * preX0; \
outVal += b2 * preX1; \
outVal += b3 * preX2; \
outVal += b4 * preX3; \
outVal -= a1 * preY0; \
outVal -= a2 * preY1; \
outVal -= a3 * preY2; \
outVal -= a4 * preY3; \
 \
preX3 = preX2; \
preX2 = preX1; \
preX1 = preX0; \
preX0 = inVal; \
preY3 = preY2; \
preY2 = preY1; \
preY1 = preY0; \
preY0 = outVal; \
 \
fps[idx] = outVal; \

//filter
__global__ void MatlabFilterOrder4Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		double b0 = filter[0];
		double b1 = filter[1];
		double b2 = filter[2];
		double b3 = filter[3];
		double b4 = filter[4];
		//double a0 = filter[5];
		double a1 = filter[6];
		double a2 = filter[7];
		double a3 = filter[8];
		double a4 = filter[9];

		double preX0 = prevs[channel + 0 * channels];
		double preX1 = prevs[channel + 1 * channels];
		double preX2 = prevs[channel + 2 * channels];
		double preX3 = prevs[channel + 3 * channels];
		double preY0 = prevs[channel + 4 * channels];
		double preY1 = prevs[channel + 5 * channels];
		double preY2 = prevs[channel + 6 * channels];
		double preY3 = prevs[channel + 7 * channels];

		//loop
		{
			float inVal = 0;
			double outVal = 0;

			if ((oneChannelSamples & 0x1F) == 0)
			{
				int repeat = oneChannelSamples >> 5;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder4; idx += channels;	//1
					FilterOrder4; idx += channels;	//2
					FilterOrder4; idx += channels;	//3
					FilterOrder4; idx += channels;	//4
					FilterOrder4; idx += channels;	//5
					FilterOrder4; idx += channels;	//6
					FilterOrder4; idx += channels;	//7
					FilterOrder4; idx += channels;	//8
					FilterOrder4; idx += channels;	//9
					FilterOrder4; idx += channels;	//10
					FilterOrder4; idx += channels;	//11
					FilterOrder4; idx += channels;	//12
					FilterOrder4; idx += channels;	//13
					FilterOrder4; idx += channels;	//14
					FilterOrder4; idx += channels;	//15
					FilterOrder4; idx += channels;	//16
					FilterOrder4; idx += channels;	//17
					FilterOrder4; idx += channels;	//18
					FilterOrder4; idx += channels;	//19
					FilterOrder4; idx += channels;	//20
					FilterOrder4; idx += channels;	//21
					FilterOrder4; idx += channels;	//22
					FilterOrder4; idx += channels;	//23
					FilterOrder4; idx += channels;	//24
					FilterOrder4; idx += channels;	//25
					FilterOrder4; idx += channels;	//26
					FilterOrder4; idx += channels;	//27
					FilterOrder4; idx += channels;	//28
					FilterOrder4; idx += channels;	//29
					FilterOrder4; idx += channels;	//30
					FilterOrder4; idx += channels;	//31
					FilterOrder4; idx += channels;	//32
				}
			}
			else
			{
				//
				int repeat = oneChannelSamples;
				int idx = channel;

				//
				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)
				{
					FilterOrder4; idx += channels;
				}
			}
		}

		//
		prevs[channel + 0 * channels] = preX0;
		prevs[channel + 1 * channels] = preX1;
		prevs[channel + 2 * channels] = preX2;
		prevs[channel + 3 * channels] = preX3;
		prevs[channel + 4 * channels] = preY0;
		prevs[channel + 5 * channels] = preY1;
		prevs[channel + 6 * channels] = preY2;
		prevs[channel + 7 * channels] = preY3;
	}
}





