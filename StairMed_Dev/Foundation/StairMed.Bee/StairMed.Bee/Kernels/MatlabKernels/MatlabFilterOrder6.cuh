#pragma once

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__global__ void MatlabFilterOrder6Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps);



