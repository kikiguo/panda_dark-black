#pragma once

#include "cuda_runtime.h"  
#include "device_launch_parameters.h"    

//
__device__ void ThresholdNegative(int * prevs, int channel, int oneChannelSamples, int gap, int channels, const float * srcs, float threshold, float * spikes);


//所有阈值都不一样，需对每个通道单独处理
__global__ void ThresholdSeparately(int channels, int oneChannelSamples, const float* srcs, int* prevs, float* thresholds, int gap, float* spikes);


//所有阈值一样：阈值为正值
__global__ void ThresholdPositiveAllSame(int channels, int oneChannelSamples, const float* srcs, int* prevs, float threshold, int gap, float* spikes);

//所有阈值一样：阈值为负值
__global__ void ThresholdNegativeAllSame(int channels, int oneChannelSamples, const float* srcs, int* prevs, float threshold, int gap, float* spikes);


