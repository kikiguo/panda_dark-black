#include "Spike.cuh"

//
__device__ void ThresholdNegative(int * prevs, int channel, int oneChannelSamples, int gap, int channels, const float * srcs, float threshold, float * spikes)
{
	int prevSpikeIndex = prevs[channel];
	prevSpikeIndex -= oneChannelSamples;

	//loop
	{
		int sampleIndex = prevSpikeIndex + gap;
		if (sampleIndex < 0)
		{
			sampleIndex = 0;
		}

		//
		for (; sampleIndex < oneChannelSamples; ++sampleIndex)
		{
			int idx = sampleIndex * channels + channel;

			//
			float inVal = srcs[idx];
			if (inVal < threshold)
			{
				spikes[idx] = 1;
				prevSpikeIndex = sampleIndex;
				sampleIndex += gap;
			}
		}
	}

	//save prev data back
	prevs[channel] = prevSpikeIndex;
}




//所有阈值都不一样，需对每个通道单独处理
__global__ void ThresholdSeparately(int channels, int oneChannelSamples, const float* srcs, int* prevs, float* thresholds, int gap, float* spikes)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		float threshold = thresholds[channel];
		int prevSpikeIndex = prevs[channel];
		prevSpikeIndex -= oneChannelSamples;

		//loop
		{
			int sampleIndex = prevSpikeIndex + gap;
			if (sampleIndex < 0)
			{
				sampleIndex = 0;
			}

			//
			for (; sampleIndex < oneChannelSamples; ++sampleIndex)
			{
				int idx = sampleIndex * channels + channel;

				//
				float inVal = srcs[idx];
				bool isSpike = false;
				if (threshold > 0)
				{
					isSpike = inVal > threshold;
				}
				else
				{
					isSpike = inVal < threshold;
				}

				//
				if (isSpike)
				{
					spikes[idx] = 1;
					prevSpikeIndex = sampleIndex;
					sampleIndex += gap;
				}
			}
		}

		//save prev data back
		prevs[channel] = prevSpikeIndex;
	}
}


//所有阈值一样：阈值为正值
__global__ void ThresholdPositiveAllSame(int channels, int oneChannelSamples, const float* srcs, int* prevs, float threshold, int gap, float* spikes)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		int prevSpikeIndex = prevs[channel];
		prevSpikeIndex -= oneChannelSamples;

		//loop
		{
			int sampleIndex = prevSpikeIndex + gap;
			if (sampleIndex < 0)
			{
				sampleIndex = 0;
			}

			//
			for (; sampleIndex < oneChannelSamples; ++sampleIndex)
			{
				int idx = sampleIndex * channels + channel;

				//
				float inVal = srcs[idx];
				if (inVal > threshold)
				{
					spikes[idx] = 1;
					prevSpikeIndex = sampleIndex;
					sampleIndex += gap;
				}
			}
		}

		//save prev data back
		prevs[channel] = prevSpikeIndex;
	}
}

//所有阈值一样：阈值为负值
__global__ void ThresholdNegativeAllSame(int channels, int oneChannelSamples, const float* srcs, int* prevs, float threshold, int gap, float* spikes)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;

	//
	if (channel < channels)
	{
		int prevSpikeIndex = prevs[channel];
		prevSpikeIndex -= oneChannelSamples;

		//loop
		{
			int sampleIndex = prevSpikeIndex + gap;
			if (sampleIndex < 0)
			{
				sampleIndex = 0;
			}

			//
			for (; sampleIndex < oneChannelSamples; ++sampleIndex)
			{
				int idx = sampleIndex * channels + channel;

				//
				float inVal = srcs[idx];
				if (inVal < threshold)
				{
					spikes[idx] = 1;
					prevSpikeIndex = sampleIndex;
					sampleIndex += gap;
				}
			}
		}

		//save prev data back
		prevs[channel] = prevSpikeIndex;
	}
}


