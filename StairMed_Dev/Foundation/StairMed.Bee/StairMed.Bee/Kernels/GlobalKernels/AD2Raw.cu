#include "AD2Raw.cuh"

//ad to raws
__global__ void AD2RawKernel(const unsigned short *adshort, int N, float k, int b, float* raws)
{
	int n = threadIdx.x + blockDim.x * blockIdx.x;
	if (n < N)
	{
		raws[n] = (adshort[n] + b) * k;
	}
}