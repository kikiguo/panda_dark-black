#include "HFP_Square_Sum.cuh"

__global__ void SumHfpSquare(int channels, int oneChannelSamples, const float* srcs, float* hfpSquareSum)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;
	float sum = 0;
	for (int sampleIndex = 0; sampleIndex < oneChannelSamples; ++sampleIndex)
	{
		int idx = sampleIndex * channels + channel;

		//
		float inVal = srcs[idx];
		sum += inVal * inVal;
	}
	hfpSquareSum[channel] = sum;
}