#include "Transpose.cuh"

//transpose
__global__ void Transpose(float* src, float* dst, int m, int n)
{
	int col = blockDim.x * blockIdx.x + threadIdx.x;
	int row = blockDim.y * blockIdx.y + threadIdx.y;

	//
	__shared__ float S[BLOCK_SIZE][BLOCK_SIZE + 1];
	S[threadIdx.y][threadIdx.x] = row < m && col < n ? src[row*n + col] : 0;

	//
	__syncthreads();

	//
	//int nx = blockIdx.x * blockDim.x + threadIdx.y;
	//int ny = threadIdx.x + blockIdx.y * blockDim.x;
	//if (nx < n && ny < m)
	//	dst[ny + m * nx] = S[threadIdx.x][threadIdx.y];


	if (blockIdx.x * blockDim.x + threadIdx.y < n && threadIdx.x + blockIdx.y * blockDim.x < m)
		dst[threadIdx.x + blockIdx.y*blockDim.x + m * (blockIdx.x*blockDim.x + threadIdx.y)] = S[threadIdx.x][threadIdx.y];
	//return;
}