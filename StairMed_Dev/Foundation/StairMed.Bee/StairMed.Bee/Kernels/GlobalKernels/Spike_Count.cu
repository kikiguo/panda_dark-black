#include "Spike_Count.cuh"

__global__ void CountChannelSpike(int channels, int oneChannelSamples, const float * srcs, int * spikeCounts)
{
	int channel = threadIdx.x + blockDim.x * blockIdx.x;
	int count = 0;
	for (int sampleIndex = 0; sampleIndex < oneChannelSamples; ++sampleIndex)
	{
		int idx = sampleIndex * channels + channel;

		//
		float inVal = srcs[idx];
		if (inVal > 0)
		{
			count += 1;
		}
	}
	spikeCounts[channel] = count;
}