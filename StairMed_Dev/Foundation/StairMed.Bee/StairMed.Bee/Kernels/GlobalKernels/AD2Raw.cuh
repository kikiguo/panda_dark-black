#pragma once

#include "cuda_runtime.h"  
#include "device_launch_parameters.h"    

//ad to raws
__global__ void AD2RawKernel(const unsigned short *adshort, int N, float k, int b, float* raws);
