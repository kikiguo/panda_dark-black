#pragma once

#include "cuda_runtime.h"  
#include "device_launch_parameters.h"   

#define BLOCK_SIZE 32

//transpose
__global__ void Transpose(float* src, float* dst, int m, int n);