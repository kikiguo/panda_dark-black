﻿using StairMed.BLE.Logger;
using StairMed.Enum;
using System;
using System.Diagnostics;
using System.Threading;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Foundation;
using Windows.Security.Cryptography;

namespace StairMed.BLE
{
    /// <summary>
    /// 
    /// </summary>
    public class BLEConnection
    {
        //
        protected const string TAG = nameof(BLEConnection);

        //
        private readonly Guid SERVICE_UUID = Guid.Parse("00002760-08c2-11e1-9073-0e8ac72e5450");
        private readonly Guid WRITE_UUID = Guid.Parse("00002760-08c2-11e1-9073-0e8ac72e5401");
        private readonly Guid READ_UUID = Guid.Parse("00002760-08c2-11e1-9073-0e8ac72e5402");
        private readonly ulong ADDRESS = 0;
        private readonly int CONNECT_TIMEOUT_MS = 15 * 1000;
        private readonly int SEND_WAIT_MS = 1 * 1000;

        //
        private BluetoothLEDevice _ble = null;
        private GattDeviceService _service = null;
        private GattCharacteristic _writeCharacter = null;
        private GattCharacteristic _readCharacter = null;
        private bool _configReadCharacterSucceed = false;

        //
        private bool _connectFailed = true;
        private bool _tryingGetWriteCharacter = true;
        private bool _tryingGetReadCharacter = true;
        private bool _tryingConfigReadCharacter = true;

        //
        private readonly Stopwatch _connectStopwatch = new Stopwatch();
        private readonly Stopwatch _sendStopwatch = new Stopwatch();

        //
        //private int _failedCount = 0;

        /// <summary>
        /// 连接状态
        /// </summary>
        private ConnectState _state = ConnectState.Disconnected;
        public ConnectState State
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _state = value;
                    ConnectionStateChanged?.Invoke(this, value);
                }
            }
        }

        /// <summary>
        /// 连接状态变化
        /// </summary>
        public event Action<BLEConnection, ConnectState> ConnectionStateChanged;

        /// <summary>
        /// 接收到数据帧
        /// </summary>
        public event Action<BLEConnection, byte[]> FrameReceived;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="address"></param>
        /// <param name="service"></param>
        /// <param name="write"></param>
        /// <param name="read"></param>
        /// <param name="connectTimeoutMS"></param>
        /// <param name="sendWaitMS"></param>
        public BLEConnection(ulong address, Guid service, Guid write, Guid read, int connectTimeoutMS = 30 * 1000, int sendWaitMS = 1000)
        {
            ADDRESS = address;
            SERVICE_UUID = service;
            WRITE_UUID = write;
            READ_UUID = read;
            CONNECT_TIMEOUT_MS = connectTimeoutMS;
            SEND_WAIT_MS = sendWaitMS;
        }

        /// <summary>
        /// 连接:阻塞式调用
        /// </summary>
        /// <returns></returns>
        public void Connect()
        {
            //
            if (State != ConnectState.Disconnected)
            {
                return;
            }

            //
            try
            {
                LogTool.Logger.LogT($"{TAG}, start connect");
                State = ConnectState.Connecting;

                //
                _ble = null;
                _writeCharacter = null;
                _readCharacter = null;
                _configReadCharacterSucceed = false;

                //
                _connectFailed = false;
                _tryingGetWriteCharacter = true;
                _tryingGetReadCharacter = true;
                _tryingConfigReadCharacter = true;

                //连接
                BluetoothLEDevice.FromBluetoothAddressAsync(ADDRESS).Completed = (asyncInfo, asyncStatus) =>
                {
                    LogTool.Logger.LogT($"{TAG}, got device");

                    //连接返回
                    if (asyncStatus != AsyncStatus.Completed)
                    {
                        _connectFailed = true;
                        return;
                    }

                    //得到连接
                    _ble = asyncInfo.GetResults();
                    if (_ble == null)
                    {
                        _connectFailed = true;
                        return;
                    }

                    //获取服务
                    _ble.GetGattServicesAsync().Completed = (asyncInfo, asyncStatus) =>
                    {
                        LogTool.Logger.LogT($"{TAG}, get all services returned");

                        //获取服务返回
                        if (asyncStatus != AsyncStatus.Completed)
                        {
                            _connectFailed = true;
                            return;
                        }

                        //得到服务
                        var services = asyncInfo.GetResults().Services;
                        if (services.Count <= 0)
                        {
                            LogTool.Logger.LogT($"{TAG}, got none services");
                        }
                        foreach (var service in services)
                        {
                            LogTool.Logger.LogT($"{TAG}, service uuid:{service.Uuid}");

                            //
                            if (service.Uuid.Equals(SERVICE_UUID))
                            {
                                LogTool.Logger.LogT($"{TAG}, got target service, PDU:{service.Session.MaxPduSize}");
                                _service = service;

                                //获取特征
                                _service.GetCharacteristicsAsync().Completed = (asyncInfo, asyncStatus) =>
                                {
                                    LogTool.Logger.LogT($"{TAG}, get all characters returned");

                                    //获取特征返回
                                    if (asyncStatus != AsyncStatus.Completed)
                                    {
                                        _connectFailed = true;
                                        return;
                                    }

                                    //获得特征
                                    var characters = asyncInfo.GetResults().Characteristics;
                                    if (characters.Count <= 0)
                                    {
                                        LogTool.Logger.LogT($"{TAG}, got none characters");
                                    }
                                    foreach (var character in characters)
                                    {
                                        LogTool.Logger.LogT($"{TAG}, character uuid:{character.Uuid}");

                                        //通过uuid获取类型
                                        if (character.Uuid.Equals(READ_UUID))
                                        {
                                            LogTool.Logger.LogT($"{TAG}, got read character");

                                            _readCharacter = character;
                                            _readCharacter.ProtectionLevel = GattProtectionLevel.Plain;
                                            _readCharacter.ValueChanged -= ReadCharacter_ValueChanged;
                                            _readCharacter.ValueChanged += ReadCharacter_ValueChanged;
                                            _readCharacter.WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue.Notify).Completed = (asyncInfo, asyncStatus) =>
                                            {
                                                LogTool.Logger.LogT($"{TAG}, modified read character");

                                                //
                                                if (asyncStatus == AsyncStatus.Completed)
                                                {
                                                    _configReadCharacterSucceed = asyncInfo.GetResults() == GattCommunicationStatus.Success;
                                                }
                                                _tryingConfigReadCharacter = false;
                                            };
                                        }
                                        else if (character.Uuid.Equals(WRITE_UUID))
                                        {
                                            LogTool.Logger.LogT($"{TAG}, got write character");
                                            _writeCharacter = character;
                                        }
                                    }

                                    //
                                    _tryingGetWriteCharacter = false;
                                    _tryingGetReadCharacter = false;
                                };
                            }
                        }
                    };
                };

                //等待
                _connectStopwatch.Restart();
                while (_connectStopwatch.ElapsedMilliseconds < CONNECT_TIMEOUT_MS)
                {
                    Thread.Sleep(50);

                    //直接失败
                    if (_connectFailed)
                    {
                        State = ConnectState.Disconnected;
                        LogTool.Logger.LogT($"{TAG}, connect failed");
                        return;
                    }

                    //正在获取读写特征码
                    if (_tryingGetWriteCharacter || _tryingGetReadCharacter)
                    {
                        continue;
                    }

                    //
                    if (_writeCharacter == null || _readCharacter == null)
                    {
                        State = ConnectState.Disconnected;
                        LogTool.Logger.LogT($"{TAG}, read or write character not got");
                        return;
                    }

                    //正在对读取进行配置
                    if (_tryingConfigReadCharacter)
                    {
                        continue;
                    }

                    //一切顺利，单看：配置读特性
                    State = _configReadCharacterSucceed ? ConnectState.Connected : ConnectState.Disconnected;
                    LogTool.Logger.LogT($"{TAG}, connect end, result:{State}");
                    if (_configReadCharacterSucceed)
                    {
                        _ble.ConnectionStatusChanged += (BluetoothLEDevice sender, object args) =>
                        {
                            LogTool.Logger.LogT($"{TAG}, BLE connect state changed, state:{sender.ConnectionStatus}");
                            if (sender.ConnectionStatus == BluetoothConnectionStatus.Disconnected)
                            {
                                Disconnect();
                            }
                        };
                    }
                    return;
                }

                //超时
                State = ConnectState.Disconnected;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Connect)}", ex);
                State = ConnectState.Disconnected;
            }
        }

        /// <summary>
        /// 收到消息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void ReadCharacter_ValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            try
            {
                CryptographicBuffer.CopyToByteArray(args.CharacteristicValue, out byte[] bytes);
                if (bytes != null)
                {
                    FrameReceived?.Invoke(this, bytes);
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ReadCharacter_ValueChanged)}", ex);
            }
        }

        /// <summary>
        /// 断开蓝牙连接
        /// </summary>
        public void Disconnect()
        {
            if (State == ConnectState.Connected)
            {
                try
                {
                    State = ConnectState.Disconnecting;

                    //
                    _readCharacter.ValueChanged -= ReadCharacter_ValueChanged;
                    _readCharacter = null;
                    _writeCharacter = null;
                    _service.Dispose();
                    _service = null;
                    if (_ble != null)
                    {
                        _ble.Dispose();
                        _ble = null;
                    }
                    GC.Collect();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.LogT($"{TAG}, Disconnect: Exception:{ex}");
                }

                //TODO:安全阈值，可能因计算机而异
                Thread.Sleep(3000);
                State = ConnectState.Disconnected;
            }
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public bool Send(byte[] bytes)
        {
            try
            {
                if (_writeCharacter == null || State != ConnectState.Connected)
                {
                    return false;
                }

                //
                bool isSucceed = false;
                bool gotResult = false;

                //
                _writeCharacter.WriteValueAsync(CryptographicBuffer.CreateFromByteArray(bytes), GattWriteOption.WriteWithoutResponse).Completed = (asyncInfo, asyncStatus) =>
                {
                    if (asyncStatus == AsyncStatus.Completed)
                    {
                        isSucceed = asyncInfo.GetResults() == GattCommunicationStatus.Success;
                    }
                    gotResult = true;
                };

                //
                _sendStopwatch.Restart();
                while (!gotResult && _sendStopwatch.ElapsedMilliseconds < SEND_WAIT_MS)
                {
                    Thread.Sleep(1);
                }

                //
                //if (!isSucceed)
                //{
                //    _failedCount++;
                //    if (_failedCount >= 3)
                //    {
                //        State = ConnectState.Disconnected;
                //    }
                //}
                //else
                //{
                //    _failedCount = 0;
                //}

                //
                return isSucceed;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Send)}", ex);
                return false;
            }
        }
    }
}
