﻿using StairMed.BLE.Logger;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Windows.Devices.Bluetooth.Advertisement;

namespace StairMed.BLE
{
    /// <summary>
    /// 
    /// </summary>
    public class BLESearcher
    {
        const string TAG = nameof(BLESearcher);

        /// <summary>
        /// 蓝牙搜素者
        /// </summary>
        private readonly BluetoothLEAdvertisementWatcher _watcher = new BluetoothLEAdvertisementWatcher
        {
            ScanningMode = BluetoothLEScanningMode.Active,
            AllowExtendedAdvertisements = true,
        };

        /// <summary>
        /// 搜索到设备，将设备名称和地址传出去
        /// </summary>
        public event Action<BLESearcher, string, ulong> BluetoothSearched;

        /// <summary>
        /// 
        /// </summary>
        private Regex _regex = new Regex(@".*");

        /// <summary>
        /// 记录某设备上次进行上报的时间
        /// </summary>
        private Dictionary<ulong, DateTime> _dict = new Dictionary<ulong, DateTime>();

        /// <summary>
        /// 上报间隔，没必要频繁上报
        /// </summary>
        private int _reportIntervalMS = 3000;

        /// <summary>
        /// 名称匹配字符串
        /// </summary>
        private string _nameRegex = string.Empty;
        public string NameRegex
        {
            get { return _nameRegex; }
            set
            {
                if (_nameRegex == null || !_regex.Equals(value))
                {
                    _nameRegex = value;
                    _regex = new Regex(value ?? string.Empty);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public BLESearcher(string nameRegex, int reportIntervalMS = 3000)
        {
            NameRegex = nameRegex;
            _reportIntervalMS = reportIntervalMS;

            //
            _watcher.Received += (sender, eventArgs) =>
            {
                HandleBLEAdvertisementEvent(eventArgs);
            };
        }

        /// <summary>
        /// 处理蓝牙广播信息
        /// </summary>
        /// <param name="advertisementInfo"></param>
        private void HandleBLEAdvertisementEvent(BluetoothLEAdvertisementReceivedEventArgs advertisementInfo)
        {
            //
            if (advertisementInfo == null)
            {
                return;
            }

            //
            if (advertisementInfo.Advertisement == null)
            {
                return;
            }

            //通过正则表达式屏蔽非本设备蓝牙
            var localName = advertisementInfo.Advertisement.LocalName;
            if (string.IsNullOrWhiteSpace(localName) || !_regex.IsMatch(localName))
            {
                return;
            }

            //蓝牙广播地址
            var address = advertisementInfo.BluetoothAddress;

            //降低同一设备上报频率
            if (!_dict.ContainsKey(address) || Math.Abs((_dict[address] - DateTime.Now).TotalMilliseconds) > _reportIntervalMS)
            {
                _dict[address] = DateTime.Now;

                //上报
                BluetoothSearched?.Invoke(this, localName, advertisementInfo.BluetoothAddress);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Start()
        {
            try
            {
                //启动搜索
                _watcher.Start();
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Start)}", ex);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Stop()
        {
            try
            {
                //停止搜索
                _watcher.Stop();
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Stop)}", ex);
            }
        }
    }
}
