﻿namespace StairMed.Core.Consts
{
    /// <summary>
    /// 
    /// </summary>
    public static class RegionNames
    {
        public const string HeadRegion = "Head";
        public const string NeckRegion = "Neck";
        public const string BodyRegion = "Body";
    }
}
