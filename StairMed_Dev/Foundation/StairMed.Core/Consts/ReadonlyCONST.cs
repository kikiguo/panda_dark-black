﻿using StairMed.Enum;
using StairMed.Probe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace StairMed.Core.Consts
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadonlyCONST
    {
        /// <summary>
        /// 设备类型
        /// </summary>
        public static DeviceType DeviceType = DeviceType.HInSX;

        /// <summary>
        /// 通道总数
        /// </summary>
        public static int DeviceChannelCount = 100;

        /// <summary>
        /// 触发通道们
        /// </summary>
        public static ReadOnlyCollection<int> TriggerChannels = new List<int> { 1, 2, 3, 4, 5, }.AsReadOnly();

        /// <summary>
        /// 电极Map图
        /// </summary>
        private static NTProbeMap _probeMap = NTProbeMap.GetDefault();
        public static NTProbeMap ProbeMap
        {
            get
            {
                return _probeMap;
            }
            set
            {
                _probeMap = value;
                ReadonlyCONST.ConvertToPhysicsChannel = _probeMap.GetPhysicsIndex;
                ReadonlyCONST.ConvertToInputChannel = _probeMap.GetInputIndex;
            }
        }

        /// <summary>
        /// 采样率
        /// </summary>
        public static ReadOnlyCollection<int> SampleRates = new List<int> { 5000, 5500, 11000, 22000, 30000 }.AsReadOnly();

        /// <summary>
        /// 电极上的物理顺序
        /// </summary>
        public static Func<int, int> ConvertToPhysicsChannel;

        /// <summary>
        /// 下位机认为的顺序
        /// </summary>
        public static Func<int, int> ConvertToInputChannel;

        /// <summary>
        /// 下位机上报数据时重组了顺序
        /// </summary>
        public static Func<int, int> GetInputIndexInReport;
    }
}
