using StairMed.FilterProcess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace StairMed.Core.Consts
{
    /// <summary>
    /// 
    /// </summary>
    public static class CONST
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SoftVersion = "1.0.0";
        public const string BuildDate = "221228";

        /// <summary>
        /// 多长时间搜索不到蓝牙认为蓝牙已不再广播
        /// </summary>
        public const int BLEBroadcastingCheckMS = 20 * 1000;

        /// <summary>
        /// socket的读写缓冲区大小
        /// </summary>
        public const int SocketBufferSize = 1024 * 1024 * 16;

        /// <summary>
        /// nwb版本
        /// </summary>
        public const int StairMedNWBVersion = 1;

        /// <summary>
        /// 公司网址
        /// </summary>
        public const string StariMedURL = "https://www.stairmed.com";

        //“VIEWMODEL”字符串的长度
        public const int STR_VIEWMODEL_LEN = 9;

        /// <summary>
        /// 波形最大缓冲时长
        /// </summary>
        public const int WAVEFORM_BUFFERED_S = 8;

        /// <summary>
        /// 最大缓存spike秒数
        /// </summary>
        public const int SPIKE_BUFFERED_S = 25;

        /// <summary>
        /// 最大缓存spike scope个数
        /// </summary>
        public const int SPIKE_SCOPE_BUFFERED_MAX = 40;

        /// <summary>
        /// 单个spike最大时长
        /// </summary>
        public const double SPIKE_TIME_LEN_S = 0.002;

        /// <summary>
        /// 
        /// </summary>
        public const bool LockSampleRate = false;

        //ISI
        public static readonly ReadOnlyCollection<int> ISITimeSpanMSOptions = new List<int> { 50, 100, 200, 500, 1000 }.AsReadOnly();
        public static readonly ReadOnlyCollection<string> SpikeRate = new List<string> { "None", "15KHz", "20KHz", "25KHz", "30Khz" }.AsReadOnly();
        public static readonly ReadOnlyCollection<int> ISIBinSizeMSOptions = new List<int> { 1, 2, 5, 10, 20 }.AsReadOnly();

        private static  List<int> channel()
        {
            List<int> result = new List<int>();
            for (int i = 0; i < 128; i++)
            {
                result.Add(i);
            }

            return result;
        }

        private static List<int> rmsvalue()
        {
            List<int> result = new List<int>();
            for (int i = 1; i < 51; i++)
            {
                result.Add(i);
            }

            return result;
        }



        private static List<int> Channelvalue()
        {
            List<int> result = new List<int>();
            for (int i = 1; i < 2048; i++)
            {
                result.Add(i);
            }

            return result;
        }


        private static List<int> Channelvalue1024()
        {
            List<int> result = new List<int>();
            for (int i = 1; i < 1024; i++)
            {
                result.Add(i);
            }

            return result;
        }

        private static List<double> ChannelGainvalue()
        {
            List<double> result = new List<double>();
            int dcount = 31 - 10;
            for (int i = 0; i < dcount; i++)
            {
                result.Add(Math.Round(1 + (i * 0.1), 1));
            }

            return result;
        }


        public static readonly ReadOnlyCollection<int> RMSChannels = channel().AsReadOnly();
        public static readonly ReadOnlyCollection<int> RMSValue = rmsvalue().AsReadOnly();
        public static readonly ReadOnlyCollection<int> SpikeScopeChannel = Channelvalue().AsReadOnly();
        public static readonly ReadOnlyCollection<double> SpikeScopecmbGain = ChannelGainvalue().AsReadOnly();
        public static readonly ReadOnlyCollection<int> ChannelName = Channelvalue1024().AsReadOnly();
        public static readonly ReadOnlyCollection<string> OutPutType = new List<string> { "spk", "lfp", "trigger" }.AsReadOnly();




        //SpikeScope
        public static readonly ReadOnlyCollection<int> SpikeScopeTypeOptions = Enumerable.Range(1, 20).ToList().AsReadOnly();
        public static readonly ReadOnlyCollection<int> SpikeScopeTimeScaleMSOptions = new List<int> { 2,3, 4, 6 }.AsReadOnly();
        public static readonly ReadOnlyCollection<int> SpikeScopeVoltageScaleMSOptions = new List<int> { 50, 100, 200, 300, 400, 500, 800, 1000, 3000, 6000 }.AsReadOnly();
        public static readonly ReadOnlyCollection<int> SpikeScopeGlowOptions = new List<int> { 5, 10, 25, 50, 75, 100, 150, 200 }.AsReadOnly();

        //Spectrum
        public static readonly ReadOnlyCollection<int> SpectrumTimeScaleMSOptions = new List<int> { 2, 5, 10 }.AsReadOnly();

        //
        public static readonly ReadOnlyCollection<FilterType> FilterTypes = new List<FilterType> { FilterType.Butterworth, FilterType.Bessel }.AsReadOnly();
        public static readonly ReadOnlyCollection<int> FilterOrders = Enumerable.Range(1, 8).ToList().AsReadOnly();

        //
        public static readonly ReadOnlyCollection<int> RenderIntervalMSOptions = new List<int> { 10, 20, 40, 50, 100, 200, 500, 1000, 2000, 5000 }.AsReadOnly();

        //
        public static readonly ReadOnlyCollection<int> SpikeRateStatisticMSOptions = new List<int> { 1000, 2000, 3000 }.AsReadOnly();

        //PSTH
        public static readonly ReadOnlyCollection<int> PSTHTrialOptions = new List<int> { 10, 20, 50, 100, 200, 500 }.AsReadOnly();
        public static readonly Dictionary<int, int> PSTHTrialStepDict = new Dictionary<int, int>
        {
            { 10, 5 },
            { 20, 5 },
            { 50, 10 },
            { 100, 20 },
            { 200, 50 },
            { 500, 100 },
        };
        public static ReadOnlyCollection<int> PSTHPostTriggerOptions = new List<int> { 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000}.AsReadOnly();
        public static Dictionary<int, int> PSTHPostTriggerStepDict = new Dictionary<int, int>
        {
            { 50, 50 },
            { 100, 100 },
            { 200, 100 },
            { 500, 100 },
            { 1000, 500 },
            { 2000, 500 },
            { 5000, 1000 },
            { 10000, 1000 },
            { 20000, 2000 },
        };
        public static ReadOnlyCollection<int> PSTHPreTriggerOptions = new List<int> { 50, 100, 200, 500, 1000, 2000 }.AsReadOnly();
        public static Dictionary<int, int> PSTHPreTriggerStepDict = new Dictionary<int, int>
        {
            { 50, 50 },
            { 100, 100 },
            { 200, 100 },
            { 500, 100 },
            { 1000, 500 },
            { 2000, 500 },
        };


        //
        public static ReadOnlyCollection<int> DisplayWindowMSOptions = new List<int> { 50, 100, 200, 300, 500, 1000, 2000, 4000,5000, 10000 }.AsReadOnly();
        public static Dictionary<int, int> DisplayWindowScaleBarDict = new Dictionary<int, int>
        {
            { 5, 1 },
            { 10, 1 },
            { 20, 2 },
            { 40, 4 },
            { 100, 10 },
            { 200, 20 },
            { 400, 40 },
            { 1000, 100 },
            { 2000, 200 },
            { 4000, 400 },
            { 10000, 1000 },
        };
        public static Dictionary<int, int> DisplayWindowTickStepDict = new Dictionary<int, int>
        {
            { 10, 1 },
            { 20, 2 },
            { 50, 5 },
            { 100, 10 },
            { 200, 20 },
            { 500, 50 },
            { 1000, 100 },
            { 2000, 200 },
            { 5000, 500 },
            { 10000, 1000 },
            { 15000, 2000 },
            { 20000, 4000 },
            { 30000, 5000 },
        };

        /// <summary>
        /// 波形列数
        /// </summary>
        public static ReadOnlyCollection<int> DisplayColumnOptions = new List<int> { 4, 8, 16 }.AsReadOnly();

        /// <summary>
        /// 波形高度
        /// </summary>
        public static ReadOnlyCollection<float> WaveHeightOptions = new List<float> { 25, 35, 45, 60, 80, 105, 135, 175, 225, 295, 350 }.AsReadOnly();

        /// <summary>
        /// 
        /// </summary>
        public static string AppInstallPath => Helper._installPath;
        public static class Helper
        {
            public static string _installPath = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        }

        public static readonly ReadOnlyCollection<float> WaveformVoltageScaleMSOptions = new List<float> { 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 6000}.AsReadOnly();

        public static ReadOnlyCollection<int> WaveformDisplayColumnOptions = new List<int> { 1, 2, 4, 8 }.AsReadOnly();

        public static ReadOnlyCollection<int> WaveformDisplayTRIGColumnOptions = new List<int> { 1, 2,3, 4,5,6,7, 8 }.AsReadOnly();

        public static ReadOnlyCollection<double> PlaySpeedOptions = new List<double> { 0.5, 1.0, 2.0, 5.0 }.AsReadOnly();
    }
}
