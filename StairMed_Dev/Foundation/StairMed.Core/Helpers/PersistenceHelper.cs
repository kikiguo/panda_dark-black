﻿using StairMed.Core.Consts;
using StairMed.Tools;
using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace StairMed.Core.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class PersistenceHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private const string TAG = "Persistence";

        /// <summary>
        /// 
        /// </summary>
        public static readonly Encoding Encoder = Encoding.UTF8;

        /// <summary>
        /// 
        /// </summary>
        private string _file = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public PersistenceHelper(string fileName)
        {
            _file = Path.Combine(CONST.AppInstallPath, fileName);
        }

        /// <summary>
        /// 加载
        /// </summary>
        /// <returns></returns>
        public T? Load<T>() where T : class
        {
            try
            {
                //
                if (!File.Exists(_file))
                {
                    return null;
                }

                //
                var xml = FileHelper.LoadFile(_file, Encoder);
                if (string.IsNullOrWhiteSpace(xml))
                {
                    return null;
                }

                //
                using (var reader = new StringReader(xml))
                {
                    return (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(Load)}", ex);
            }

            //
            return null;
        }

        /// <summary>
        /// 保存
        /// </summary>
        public void Save(object obj)
        {
            try
            {
                using (var fs = new FileStream(_file, FileMode.Create, FileAccess.ReadWrite))
                {
                    using (var writer = new StreamWriter(fs, Encoder))
                    {
                        new XmlSerializer(obj.GetType()).Serialize(writer, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(Save)}", ex);
            }
        }
    }
}
