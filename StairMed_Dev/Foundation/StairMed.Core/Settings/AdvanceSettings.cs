﻿using StairMed.MVVM.Base;

namespace StairMed.Core.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class AdvanceSettings : ViewModelBase
    {
        #region 单例

        private static readonly AdvanceSettings _instance = new AdvanceSettings();
        public static AdvanceSettings Instance => _instance;
        private AdvanceSettings() { }

        #endregion

        /// <summary>
        /// 绑定采集和录制按钮
        /// </summary>
        private bool _bindCollectRecordButton = false;
        public bool BindCollectRecordButton
        {
            get { return _bindCollectRecordButton; }
            set { Set(ref _bindCollectRecordButton, value); }
        }

        /// <summary>
        /// 是否录制raw数据
        /// </summary>
        private bool _isRecordRaw = false;
        public bool IsRecordRaw
        {
            get { return _isRecordRaw; }
            set { Set(ref _isRecordRaw, value); }
        }

        /// <summary>
        /// 是否录制wfp数据
        /// </summary>
        private bool _isRecordWFP = true;
        public bool IsRecordWFP
        {
            get { return _isRecordWFP; }
            set { Set(ref _isRecordWFP, value); }
        }

        /// <summary>
        /// 是否录制LFP数据
        /// </summary>
        private bool _isRecordLFP = false;
        public bool IsRecordLFP
        {
            get { return _isRecordLFP; }
            set { Set(ref _isRecordLFP, value); }
        }

        /// <summary>
        /// 是否录制HFP
        /// </summary>
        private bool _isRecordHFP = false;
        public bool IsRecordHFP
        {
            get { return _isRecordHFP; }
            set { Set(ref _isRecordHFP, value); }
        }

        /// <summary>
        /// 是否录制Spike数据
        /// </summary>
        private bool _isRecordSpike = false;
        public bool IsRecordSpike
        {
            get { return _isRecordSpike; }
            set { Set(ref _isRecordSpike, value); }
        }

        /// <summary>
        /// 是否录制TRIG数据
        /// </summary>
        private bool _isRecordTRIG = false;
        public bool IsRecordTRIG
        {
            get { return _isRecordTRIG; }
            set { Set(ref _isRecordTRIG, value); }
        }
    }
}
