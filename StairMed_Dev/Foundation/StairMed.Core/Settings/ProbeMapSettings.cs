﻿using StairMed.Core.Helpers;
using StairMed.MVVM.Base;
using StairMed.Probe;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Xml.Linq;
using System.IO;
using StairMed.Core.Consts;
using System.Linq;

namespace StairMed.Core.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class ProbeMapSettings : ViewModelBase
    {
        const string TAG = nameof(ProbeMapSettings);
        private readonly Timer _saveTimer = new Timer
        {
            Interval = 100,
            AutoReset = false,
        };

        #region 单例

        private class Helper
        {
            internal static PersistenceHelper _persistence = new PersistenceHelper("probemap.xml");
            internal static ProbeMapSettings _instance = new ProbeMapSettings();
            internal static bool _loaded = false;
            static Helper()
            {
                NTProbeMap newProbeMap = _persistence.Load<NTProbeMap>();
                if (newProbeMap != null)
                {
                    _probeMap = newProbeMap;
                }
                ProbeMapSettings.channelMap = LoadChannelMapping("A");
                _loaded = true;
            }
        }
        public static ProbeMapSettings Instance => Helper._instance;
        private ProbeMapSettings()
        {
            _saveTimer.Elapsed += (sender, e) =>
            {
                Helper._persistence.Save(this);
            };
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected override bool Set<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            var changed = base.Set(ref storage, value, propertyName);
            if (changed && Helper._loaded)
            {
                _saveTimer.Stop();
                _saveTimer.Start();
            }
            return changed;
        }


        /// <summary>
        /// 
        /// </summary>
        private static NTProbeMap _probeMap = null;
        public NTProbeMap ProbeMap
        {
            get { return _probeMap; }
            set { Set(ref _probeMap, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private static Dictionary<int, int> channelMap = new Dictionary<int, int>();
        public Dictionary<int, int> ChannelMap
        {
            get { return channelMap; }
            set { Set(ref channelMap, value); } // Change the accessibility to public
        }

        public static Dictionary<int, int> LoadChannelMapping(string group)
        {
            Dictionary<int, int> localChannelMap = new Dictionary<int, int>();

            string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string filePath = Path.Combine(currentDirectory, "probemap1.xml");

            try
            {
                XDocument doc = XDocument.Load(filePath);
                XElement monkeyElement = doc.Descendants("ChannelMappings")
                                    .FirstOrDefault(e => e.Attribute("id")?.Value == "Monkey" &&
                                                            e.Attribute("Group")?.Value == group);

                IEnumerable<XElement> mappings;
                if (monkeyElement != null)
                {
                    mappings = monkeyElement.Elements("Mapping");
                }
                else
                {
                    mappings = Enumerable.Empty<XElement>();
                }

                foreach (var mapping in mappings)
                {
                    int dsChannel = int.Parse(mapping.Attribute("DSChannel").Value);
                    int pdChannel = int.Parse(mapping.Attribute("PDChannel").Value);

                    localChannelMap[dsChannel] = pdChannel;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading channel map for group {group}: {ex.Message}");
            }

            return localChannelMap;
        }

    }

}
