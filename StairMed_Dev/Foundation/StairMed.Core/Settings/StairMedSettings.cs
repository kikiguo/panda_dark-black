﻿using StairMed.Core.Helpers;
using StairMed.FilterProcess;
using StairMed.Enum;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Collections.ObjectModel;

namespace StairMed.Core.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class StairMedSettings : ViewModelBase
    {
        const string TAG = nameof(StairMedSettings);
        private readonly Timer _timer = new Timer
        {
            Interval = 100,
            AutoReset = false,
        };

        #region 单例

        private class Helper
        {
            internal static PersistenceHelper _persistence = new PersistenceHelper("settings.xml");
            internal static StairMedSettings _instance = new StairMedSettings();
            internal static bool _loaded = false;
            static Helper()
            {
                _persistence = new PersistenceHelper(StairMedSolidSettings.Instance.DefaultSettingPath);
                var instance = _persistence.Load<StairMedSettings>();
                if (instance != null)
                {
                    _instance = instance;
                }
                else
                {
                    _instance = new StairMedSettings();
                }
                _loaded = true;
            }
        }
        public static StairMedSettings Instance => Helper._instance;
        private StairMedSettings()
        {
            _timer.Elapsed += (sender, e) =>
            {
                //Helper._persistence.Save(this);
                var changes = new HashSet<string>();
                lock (_changedParams)
                {
                    foreach (var item in _changedParams)
                    {
                        changes.Add(item);
                    }
                    _changedParams.Clear();
                }
                OnSettingChanged?.Invoke(changes);
            };
        }

        #endregion



        /// <summary>
        /// 
        /// </summary>
        public event Action<HashSet<string>> OnSettingChanged;


        /// <summary>
        /// 
        /// </summary>
        private HashSet<string> _changedParams = new HashSet<string>();

        private bool _isClean = false;

        public bool IsClean
        {
            get { return _isClean; }
            set { Set(ref _isClean, value); }
        }

        private long _leanTiem = 0;
        public long cleanTiem
        {
            get { return _leanTiem; }
            set { Set(ref _leanTiem, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected override bool Set<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            var changed = base.Set(ref storage, value, propertyName);
            if (changed && Helper._loaded)
            {
                lock (_changedParams)
                {
                    _changedParams.Add(propertyName);
                }

                //
                _timer.Stop();
                _timer.Start();
            }
            return changed;
        }


        /// <summary>
        /// 正在采集中
        /// </summary>
        private bool _isCollecting = false;
        public bool IsCollecting
        {
            get { return _isCollecting; }
            set { Set(ref _isCollecting, value); }
        }


        private long _fileTimeSpan = 0;
        public long FileTimeSpan
        {
            get { return _fileTimeSpan; }
            set { Set(ref _fileTimeSpan, value); }
        }


        private string _fileTime = "00:00:00";
        public string FileTime
        {
            get { return _fileTime; }
            set { Set(ref _fileTime, value); }
        }


        private string _fileDate = "2023-10-13";
        public string FileDate
        {
            get { return _fileDate; }
            set { Set(ref _fileDate, value); }
        }

        /// <summary>
        /// Waveform下Waveform显示列数
        /// </summary>
        private int _waveformDisplayColumn = 1;
        public int WaveformDisplayColumn
        {
            get { return _waveformDisplayColumn; }
            set { Set(ref _waveformDisplayColumn, value); }
        }

        private int _waveformDisplayTRIG= 2;
        public int WaveformDisplayColumnTRIG
        {
            get { return _waveformDisplayTRIG; }
            set { Set(ref _waveformDisplayTRIG, value); }
        }


        private int _waveformDisplayTRIGColumn = 1;
        public int WaveformDisplayTRIGColumn
        {
            get { return _waveformDisplayTRIGColumn; }
            set { Set(ref _waveformDisplayTRIGColumn, value);
                WaveformDisplayTRIGColumnHeight = 60 * value>120?120: 60 * value;
                WaveformDisplayColumnTRIG = value+1;
            }
        }


        private int _waveformDisplayTRIGColumnHeight = 0;
        public int WaveformDisplayTRIGColumnHeight
        {
            get { return _waveformDisplayTRIGColumnHeight; }
            set { Set(ref _waveformDisplayTRIGColumnHeight, value); } 
        }

        /// <summary>
        /// Waveform下SpikeScope显示列数
        /// </summary>
        private int _spikeScopeDisplayColumn = 8;
        public int SpikeScopeDisplayColumn
        {
            get { return _spikeScopeDisplayColumn; }
            set { Set(ref _spikeScopeDisplayColumn, value); }
        }

        /// <summary>
        /// 视窗长度
        /// </summary>
        private int _displayWindowMS = 2000;
        public int DisplayWindowMS
        {
            get { return _displayWindowMS; }
            set { Set(ref _displayWindowMS, value); }
        }


        private bool _rmsMouseDouble = false;
        public bool RMSMouseDouble
        {
            get { return _rmsMouseDouble; }
            set { Set(ref _rmsMouseDouble, value); }
        }

        /// <summary>
        /// 采集窗口
        /// </summary>
        private int _spikeSumWindowMS = 50;
        public int SpikeSumWindowMS
        {
            get { return _spikeSumWindowMS; }
            set { Set(ref _spikeSumWindowMS, value); }
        }

        /// <summary>
        /// 采集步长
        /// </summary>
        private int _spikeSumStepMS = 10;
        public int SpikeSumStepMS
        {
            get { return _spikeSumStepMS; }
            set { Set(ref _spikeSumStepMS, value); }
        }

        /// <summary>
        /// waveform or spike显示方式：true:waveform , false:spikescope
        /// </summary>
        private bool _waveformOrSpike = true;
        public bool WaveformOrSpike
        {
            get { return _waveformOrSpike; }
            set { Set(ref _waveformOrSpike, value); }
        }

        /// <summary>
        /// sweep or roll
        /// </summary>
        private bool _sweepOrRoll = true;
        public bool SweepOrRoll
        {
            get { return _sweepOrRoll; }
            set { Set(ref _sweepOrRoll, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _rollOffsetTimeMs = 0;
        public int RollOffsetTimeMs
        {
            get { return _rollOffsetTimeMs; }
            set { Set(ref _rollOffsetTimeMs, value); }
        }
        
        private int _audioOutPutType = 0;
        public int AudioOutPutType
        {
            get { return _audioOutPutType; }
            set { Set(ref _audioOutPutType, value); }
        }
            
        private int _audioOuRight = 100;
        public int AudioOuRight
        {
            get { return _audioOuRight; }
            set { Set(ref _audioOuRight, value); }
        }
                 
        private int _audioOuLeft = 10;
        public int AudioOuLeft
        {
            get { return _audioOuLeft; }
            set { Set(ref _audioOuLeft, value); }
        }
        
                 
        private int _audioInLeft = 10;
        public int AudioInLeft
        {
            get { return _audioInLeft; }
            set { Set(ref _audioInLeft, value); }
        }
        
                 
        private int _audioInRight= 100;
        public int AudioInRight
        {
            get { return _audioInRight; }
            set { Set(ref _audioInRight, value); }
        }

        private string _audioOutPutTypeValue = "spk";
        public string AudioOutPutTypeValue
        {
            get { return _audioOutPutTypeValue; }
            set { Set(ref _audioOutPutTypeValue, value); }
        }

        private int _audioChannelName1 = 1;
        public int AudioChannelName1
        {
            get { return _audioChannelName1; }
            set { Set(ref _audioChannelName1, value); }
        }

        /// <summary>
        /// 显示原始波形
        /// </summary>
        private bool _displayRaw = false;
        public bool DisplayRaw
        {
            get { return _displayRaw; }
            set { Set(ref _displayRaw, value); }
        }

        /// <summary>
        /// 显示spike
        /// </summary>
        private bool _displaySpike = false;
        public bool DisplaySpike
        {
            get { return _displaySpike; }
            set { Set(ref _displaySpike, value); }
        }

        /// <summary>
        /// 显示spike组合
        /// </summary>
        private bool _displaySpikeSum = false;
        public bool DisplaySpikeSum
        {
            get { return _displaySpikeSum; }
            set { Set(ref _displaySpikeSum, value); }
        }

        /// <summary>
        /// 显示高频波形
        /// </summary>
        private bool _displayHFWave = false;
        public bool DisplayHFWave
        {
            get { return _displayHFWave; }
            set { Set(ref _displayHFWave, value); }
        }

        /// <summary>
        /// 显示低频信号
        /// </summary>
        private bool _displayLFWave = true;
        public bool DisplayLFWave
        {
            get { return _displayLFWave; }
            set { Set(ref _displayLFWave, value); }
        }

        /// <summary>
        /// 显示外触发信号
        /// </summary>
        private bool _displayTRIG = false;
        public bool DisplayTRIG
        {
            get { return _displayTRIG; }
            set { Set(ref _displayTRIG, value);
                WaveformDisplayTRIGColumn = 1;
                if (value)
                    WaveformDisplayTRIGColumnHeight = 60;
                else
                    WaveformDisplayTRIGColumnHeight = 0;
            }
        }

        /// <summary>
        /// 显示全通道数据
        /// </summary>
        private bool _displayWFWave = false;
        public bool DisplayWFWave
        {
            get { return _displayWFWave; }
            set { Set(ref _displayWFWave, value); }
        }

        /// <summary>
        /// 原始数据波形上限
        /// </summary>
        private float _waveMax = 500;
        public float WaveMax
        {
            get { return _waveMax; }
            set
            {
                value = Math.Min(Math.Max(value, 5), 9999);
                Set(ref _waveMax, value);
                WaveMin = -value;
            }
        }

        /// <summary>
        /// 原始数据波形下限
        /// </summary>
        private float _waveMin = -500;
        public float WaveMin
        {
            get { return _waveMin; }
            set
            {
                if (Set(ref _waveMin, value))
                {
                    WaveMax = -value;
                }
            }
        }

        /// <summary>
        /// Spike组合数据波形上限
        /// </summary>
        private float _spikeSumMax = 50;
        public float SpikeSumMax
        {
            get { return _spikeSumMax; }
            set
            {
                value = Math.Min(Math.Max(value, 1), 1500);
                Set(ref _spikeSumMax, value);
            }
        }

        /// <summary>
        /// 原始波形高度
        /// </summary>
        private float _waveHeight = 60;
        public float WaveHeight
        {
            get { return _waveHeight; }
            set
            {
                value = Math.Min(Math.Max(value, 5), 999);
                Set(ref _waveHeight, value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _showSideBarSetting = true;
        public bool ShowSideBarSetting
        {
            get { return _showSideBarSetting; }
            set { Set(ref _showSideBarSetting, value); }
        }

        /// <summary>
        /// spike阈值
        /// </summary>
        private double _threshold = -70;
        public double Threshold
        {
            get { return _threshold; }
            set
            {
                value = Math.Min(Math.Max(value, -6390), 6390);
                Set(ref _threshold, value);
            }
        }


        private string _spikeRate = "None";
        public string SpikeRate
        {
            get { return _spikeRate; }
            set { Set(ref _spikeRate, value); }
        }


        private string _lfpRate = "None";
        public string LfpRate
        {
            get { return _lfpRate; }
            set { Set(ref _lfpRate, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _halfSampleRate = 15000;
        public int HalfSampleRate
        {
            get { return _halfSampleRate; }
            set { Set(ref _halfSampleRate, value); }
        }

        #region

        /// <summary>
        /// 通道 physics
        /// </summary>
        private int _waveformChannel = 0;
        public int WaveformChannel
        {
            get { return _waveformChannel; }
            set { Set(ref _waveformChannel, value); }
        }

        /// <summary>
        /// 绑定点选
        /// </summary>
        private bool _waveformChannelLockToClick = true;
        public bool WaveformChannelLockToClick
        {
            get { return _waveformChannelLockToClick; }
            set { Set(ref _waveformChannelLockToClick, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private float _waveformHeight = 175;
        public float WaveformHeight
        {
            get { return _waveformHeight; }
            set { Set(ref _waveformHeight, value); }
        }


        #endregion


        #region Spike Scope

        /// <summary>
        /// spike scope图:保留多少个spike,25
        /// </summary>
        /// private int _spikeScopeReserved = 25;
        private int _spikeScopeReserved = 10;
        public int SpikeScopeReserved
        {
            get { return _spikeScopeReserved; }
            set { Set(ref _spikeScopeReserved, Math.Max(1, value)); }
        }


        /// <summary>
        /// spike scope图:电压范围
        /// </summary>
        private int _spikeScopeVoltageRange = 400;
        public int SpikeScopeVoltageRange
        {
            get { return _spikeScopeVoltageRange; }
            set { Set(ref _spikeScopeVoltageRange, value); }
        }


             /// <summary>
        /// spike scope图:电压范围
        /// </summary>
        private double _spikeScopeGain = 1.0;
        public double SpikeScopeGain
        {
            get { return _spikeScopeGain; }
            set { Set(ref _spikeScopeGain, value); }
        }

        /// <summary>
        /// spike scope图:spike长度：毫秒
        /// </summary>
        private int _spikeScopeTimeMS = 2;
        public int SpikeScopeTimeMS
        {
            get { return _spikeScopeTimeMS; }
            set { Set(ref _spikeScopeTimeMS, value); }
        }

        /// <summary>
        /// 通道
        /// </summary>
        private int _spikeScopeChannel = 0;
        public int SpikeScopeChannel
        {
            get { return _spikeScopeChannel; }
            set { Set(ref _spikeScopeChannel, value); }
        }

        /// <summary>
        /// 通道
        /// </summary>
        private bool _spikeScopeAllChannel = false;
        public bool SpikeScopeAllChannel
        {
            get { return _spikeScopeAllChannel; }
            set { Set(ref _spikeScopeAllChannel, value); }
        }

        private bool _spikeScopeEnable = false;
        public bool SpikeScopeEnable
        {
            get { return _spikeScopeEnable; }
            set { Set(ref _spikeScopeEnable, value); }
        }

        /// <summary>
        /// 绑定点选
        /// </summary>
        private bool _spikeScopeChannelLockToClick = true;
        public bool SpikeScopeChannelLockToClick
        {
            get { return _spikeScopeChannelLockToClick; }
            set { Set(ref _spikeScopeChannelLockToClick, value); }
        }

        /// <summary>
        /// kmeans的k
        /// </summary>
        private int _kMeansK = 1;
        public int KMeansK
        {
            get { return _kMeansK; }
            set { Set(ref _kMeansK, value); }
        }

        /// <summary>
        /// 自动kmeans的k
        /// </summary>
        private bool _autoKMeansK = false;
        public bool AutoKMeansK
        {
            get { return _autoKMeansK; }
            set { Set(ref _autoKMeansK, value); }
        }

        /// <summary>
        /// 启用抑制
        /// </summary>
        private bool _enableSuppression = true;
        public bool EnableSuppression
        {
            get { return _enableSuppression; }
            set { Set(ref _enableSuppression, value); }
        }

        /// <summary>
        /// 显示抑制波形
        /// </summary>
        private bool _showArtifacts = true;
        public bool ShowArtifacts
        {
            get { return _showArtifacts; }
            set { Set(ref _showArtifacts, value); }
        }

        /// <summary>
        /// 波形抑制阈值
        /// </summary>
        private int _artifactThreshold = 2500;
        public int ArtifactThreshold
        {
            get { return _artifactThreshold; }
            set { Set(ref _artifactThreshold, value); }
        }

        #endregion



        #region ISI

        /// <summary>
        /// ISI的时间长度
        /// </summary>
        private int _isiTimeScaleMS = 200;
        public int ISITimeScaleMS
        {
            get { return _isiTimeScaleMS; }
            set { Set(ref _isiTimeScaleMS, value); }
        }

        /// <summary>
        /// ISI的粒度
        /// </summary>
        private int _isiBinSizeMS = 5;
        public int ISIBinSizeMS
        {
            get { return _isiBinSizeMS; }
            set { Set(ref _isiBinSizeMS, value); }
        }

        /// <summary>
        /// ISI的通道
        /// </summary>
        private int _isiChannel = 0;
        public int ISIChannel
        {
            get { return _isiChannel; }
            set { Set(ref _isiChannel, value); }
        }

        /// <summary>
        /// ISI的Y轴是否为线性：or 对数
        /// </summary>
        private bool _isiYLinear = true;
        public bool ISIYLinear
        {
            get { return _isiYLinear; }
            set { Set(ref _isiYLinear, value); }
        }


        /// <summary>
        /// Signal Source Digital Signal
        /// </summary>
        private bool _filterSigSouDig = true;
        public bool FilterSigSouDig
        {
            get { return _filterSigSouDig; }
            set
            {
                Set(ref _filterSigSouDig, value);
                if (FilterSigSouAba == value)
                {
                    FilterSigSouAba = !value;
                }

            }
        }

        /// <summary>
        /// Signal Source Abalog Signal
        /// </summary>
        private bool _filterSigSouAba = false;
        public bool FilterSigSouAba
        {
            get { return _filterSigSouAba; }
            set
            {
                Set(ref _filterSigSouAba, value);
                if (FilterSigSouDig == value)
                {
                    FilterSigSouDig = !value;
                }
            }

        }

        private int _lowCutoff = 100;
        public int LowCutoff
        {
            get { return _lowCutoff; }
            set { Set(ref _lowCutoff, value); }
        }


        private int _higCutoff = 100;
        public int HigCutoff
        {
            get { return _higCutoff; }
            set { Set(ref _higCutoff, value); }
        }

        /// <summary>
        /// Filter Style Bessel
        /// </summary>
        private bool _filterStyleBess = true;
        public bool FilterStyleBess
        {
            get { return _filterStyleBess; }
            set
            {
                Set(ref _filterStyleBess, value);
                if (FilterStyleButt == value)
                {
                    FilterStyleButt = !value;
                }
            }

        }

        /// <summary>
        /// Filter Style Butterworth
        /// </summary>
        private bool _filterStyleButt = false;
        public bool FilterStyleButt
        {
            get { return _filterStyleButt; }
            set
            {
                Set(ref _filterStyleButt, value);
                if (FilterStyleBess == value)
                {
                    FilterStyleBess = !value;
                }
            }

        }


        /// <summary>
        /// Filter Type Low
        /// </summary>
        private bool _filterTypeLow = true;
        public bool FilterTypeLow
        {
            get { return _filterTypeLow; }
            set { Set(ref _filterTypeLow, value); }

        }

        /// <summary>
        /// Filter Type High
        /// </summary>
        private bool _filterTypeHigh = false;
        public bool FilterTypeHigh
        {
            get { return _filterTypeHigh; }
            set { Set(ref _filterTypeHigh, value); }

        }

        /// <summary>
        /// Filter Type Band
        /// </summary>
        private bool _filterTypeBand = false;
        public bool FilterTypeBand
        {
            get { return _filterTypeBand; }
            set { Set(ref _filterTypeBand, value); }

        }

        /// <summary>
        /// Filter Type Notch
        /// </summary>
        private bool _filterTypeNotch = false;
        public bool FilterTypeNotch
        {
            get { return _filterTypeNotch; }
            set { Set(ref _filterTypeNotch, value); }

        }

        /// <summary>
        /// Filter Cutoff Low
        /// </summary>
        private bool _filterCutoffLow = true;
        public bool FilterCutoffLow
        {
            get { return _filterCutoffLow; }
            set { Set(ref _filterCutoffLow, value); }

        }

        /// <summary>
        /// Filter Cutoff Hig
        /// </summary>
        private bool _filterCutoffHig = true;
        public bool FilterCutoffHig
        {
            get { return _filterCutoffHig; }
            set { Set(ref _filterCutoffHig, value); }
        }

        /// <summary>
        /// Filter Cutoff Band
        /// </summary>
        private bool _filterCutoffBand = true;
        public bool FilterCutoffBand
        {
            get { return _filterCutoffBand; }
            set { Set(ref _filterCutoffBand, value); }

        }




        /// <summary>
        /// 绑定点选
        /// </summary>
        private bool _isiChannelLockToClick = true;
        public bool ISIChannelLockToClick
        {
            get { return _isiChannelLockToClick; }
            set { Set(ref _isiChannelLockToClick, value); }
        }

        #endregion
        /// <summary>
        /// ISI的时间长度
        /// </summary>
        private int _filterTimeScaleMS = 3000;
        public int FilterTimeScaleMS
        {
            get { return _filterTimeScaleMS; }
            set { Set(ref _filterTimeScaleMS, value); }
        } 
        
        /// <summary>
        /// ISI的时间长度
        /// </summary>
        private int _filterBinSizeMS = 30;
        public int FilterBinSizeMS
        {
            get { return _filterBinSizeMS; }
            set { Set(ref _filterBinSizeMS, value); }
        }

        #region RMS

        /// <summary>
        /// ISI的时间长度
        /// </summary>
        private int _rmsTimeScaleMS = 200;
        public int RMSTimeScaleMS
        {
            get { return _rmsTimeScaleMS; }
            set { Set(ref _rmsTimeScaleMS, value); }
        }

        /// <summary>
        /// ISI的粒度
        /// </summary>
        private int _rmsBinSizeMS = 5;
        public int RMSBinSizeMS
        {
            get { return _rmsBinSizeMS; }
            set { Set(ref _rmsBinSizeMS, value); }
        }
        
        /// <summary>
        /// ISI的粒度
        /// </summary>
        private int _rmsValue = 33;
        public int RMSValue
        {
            get { return _rmsValue; }
            set { Set(ref _rmsValue, value); }
        }

        /// <summary>
        /// ISI的通道
        /// </summary>
        private int _rmsChannel = 1;
        public int RMSChannel
        {
            get { return _rmsChannel; }
            set { Set(ref _rmsChannel, value); }
        }

        /// <summary>
        /// ISI的Y轴是否为线性：or 对数
        /// </summary>
        private bool _rmsYLinear = true;
        public bool RMSYLinear
        {
            get { return _rmsYLinear; }
            set { Set(ref _rmsYLinear, value); }
        }

        /// <summary>
        /// 绑定点选
        /// </summary>
        private bool _rmsChannelLockToClick = true;
        public bool RMSChannelLockToClick
        {
            get { return _rmsChannelLockToClick; }
            set { Set(ref _rmsChannelLockToClick, value); }
        }
        #endregion



        #region 滤波器

        /// <summary>
        /// 是否使能DSP滤波
        /// </summary>
        private bool _enableDSP = true;
        public bool EnableDSP
        {
            get { return _enableDSP; }
            set { Set(ref _enableDSP, value); }
        }

        /// <summary>
        /// DSP截止
        /// </summary>
        private double _dspCutoff = 1;
        public double DSPCutoff
        {
            get { return _dspCutoff; }
            set { Set(ref _dspCutoff, value); }
        }

        /// <summary>
        /// 带通滤波下限截至频率
        /// </summary>
        private double _lowerBandwidth = 0.1;
        public double LowerBandwidth
        {
            get { return _lowerBandwidth; }
            set { Set(ref _lowerBandwidth, value); }
        }

        /// <summary>
        /// 设置带通滤波上限截至频率
        /// </summary>
        private double _upperBandwidth = 7500;
        public double UpperBandwidth
        {
            get { return _upperBandwidth; }
            set { Set(ref _upperBandwidth, value); }
        }

        /// <summary>
        /// 低通滤波器类型
        /// </summary>
        private FilterType _lowPassFilterType = FilterType.Bessel;
        public FilterType LowPassFilterType
        {
            get { return _lowPassFilterType; }
            set { Set(ref _lowPassFilterType, value); }
        }

        /// <summary>
        /// 低通滤波器阶数
        /// </summary>
        private int _lowPassFilterOrder = 2;
        public int LowPassFilterOrder
        {
            get { return _lowPassFilterOrder; }
            set { Set(ref _lowPassFilterOrder, value); }
        }

        /// <summary>
        /// 低通滤波器截止频率
        /// </summary>
        private int _lowPassFilterCutoff = 250;
        public int LowPassFilterCutoff
        {
            get { return _lowPassFilterCutoff; }
            set { Set(ref _lowPassFilterCutoff, value); }
        }

        /// <summary>
        /// 高通滤波器阶数
        /// </summary>
        private int _highPassFilterOrder = 2;
        public int HighPassFilterOrder
        {
            get { return _highPassFilterOrder; }
            set { Set(ref _highPassFilterOrder, value); }
        }

        /// <summary>
        /// 高通滤波器类型
        /// </summary>
        private FilterType _highPassFilterType = FilterType.Bessel;
        public FilterType HighPassFilterType
        {
            get { return _highPassFilterType; }
            set { Set(ref _highPassFilterType, value); }
        }

        /// <summary>
        /// 高通滤波器截止频率
        /// </summary>
        private int _highPassFilterCutoff = 250;
        public int HighPassFilterCutoff
        {
            get { return _highPassFilterCutoff; }
            set { Set(ref _highPassFilterCutoff, value); }
        }

        /// <summary>
        /// 抑制滤波器
        /// </summary>
        private NotchFilterType _notchFilterType = NotchFilterType.None;
        public NotchFilterType NotchFilterType
        {
            get { return _notchFilterType; }
            set { Set(ref _notchFilterType, value); }
        }

        #endregion


        #region 频谱图

        /// <summary>
        /// 频谱：通道
        /// </summary>
        private int _specturmChannel = 0;
        public int SpecturmChannel
        {
            get { return _specturmChannel; }
            set { Set(ref _specturmChannel, value); }
        }

        /// <summary>
        /// 绑定点选
        /// </summary>
        private bool _specturmChannelLockToClick = true;
        public bool SpectrumChannelLockToClick
        {
            get { return _specturmChannelLockToClick; }
            set { Set(ref _specturmChannelLockToClick, value); }
        }

        /// <summary>
        /// fft的数据长度
        /// </summary>
        private int _fftSize = 1024;
        public int FFTSize
        {
            get { return _fftSize; }
            set { Set(ref _fftSize, value); }
        }

        /// <summary>
        /// 谱显示方式：true:谱， false:线
        /// </summary>
        private bool _spectrumDisplayMode = true;
        public bool SpectrumDisplayMode
        {
            get { return _spectrumDisplayMode; }
            set { Set(ref _spectrumDisplayMode, value); }
        }

        /// <summary>
        /// 谱显示的最大频率
        /// </summary>
        private int _spectrumMaxFreq = 200;
        public int SpectrumMaxFreq
        {
            get { return _spectrumMaxFreq; }
            set { Set(ref _spectrumMaxFreq, value); }
        }

        /// <summary>
        /// 谱显示的最小频率
        /// </summary>
        private int _spectrumMinFreq = 0;
        public int SpectrumMinFreq
        {
            get { return _spectrumMinFreq; }
            set { Set(ref _spectrumMinFreq, value); }
        }

        /// <summary>
        /// 谱 是否显示 标记线
        /// </summary>
        private bool _spectrumShowMarker = true;
        public bool SpectrumShowMarker
        {
            get { return _spectrumShowMarker; }
            set { Set(ref _spectrumShowMarker, value); }
        }

        /// <summary>
        /// 谱 标记线频率
        /// </summary>
        private int _spectrumMarkerFreq = 60;
        public int SpectrumMarkerFreq
        {
            get { return _spectrumMarkerFreq; }
            set { Set(ref _spectrumMarkerFreq, value); }
        }

        /// <summary>
        /// 谱 谐波个数
        /// </summary>
        private int _spectrumHarmonics = 0;
        public int SpectrumHarmonics
        {
            get { return _spectrumHarmonics; }
            set { Set(ref _spectrumHarmonics, value); }
        }

        /// <summary>
        /// 谱显示 时间范围， 单位：秒
        /// </summary>
        private int _spectrumTimeScaleS = 5;
        public int SpectrumTimeScaleS
        {
            get { return _spectrumTimeScaleS; }
            set { Set(ref _spectrumTimeScaleS, value); }
        }

        /// <summary>
        /// 谱显示：多少列
        /// </summary>
        private int _spectrumReserved = 10;
        public int SpectrumReserved
        {
            get { return _spectrumReserved; }
            set { Set(ref _spectrumReserved, value); }
        }




        /// <summary>
        /// 是否显示trigger波形
        /// </summary>
        private bool _spectrumEnableTrigger = false;
        public bool SpectrumEnableTrigger
        {
            get { return _spectrumEnableTrigger; }
            set { Set(ref _spectrumEnableTrigger, value); }
        }

        /// <summary>
        /// trigger通道
        /// </summary>
        private int _spectrumTriggerChannel = 0;
        public int SpectrumTriggerChannel
        {
            get { return _spectrumTriggerChannel; }
            set { Set(ref _spectrumTriggerChannel, value); }
        }

        #endregion


        #region 数据保存格式

        /// <summary>
        /// 文件保存至matlab格式
        /// </summary>
        private bool _saveDataToMatlabType = true;
        public bool SaveDataToMatlabType
        {
            get { return _saveDataToMatlabType; }
            set { Set(ref _saveDataToMatlabType, value); }
        }

        /// <summary>
        /// 文件保存至CSV格式
        /// </summary>
        private bool _saveDataToCSVType = true;
        public bool SaveDataToCSVType
        {
            get { return _saveDataToCSVType; }
            set { Set(ref _saveDataToCSVType, value); }
        }

        /// <summary>
        /// 文件保存至PNG格式
        /// </summary>
        private bool _saveDataToPNGType = true;
        public bool SaveDataToPNGType
        {
            get { return _saveDataToPNGType; }
            set { Set(ref _saveDataToPNGType, value); }
        }

        #endregion


        #region PSTH

        /// <summary>
        /// PSTH通道
        /// </summary>
        private int _PSTHChannel = 0;
        public int PSTHChannel
        {
            get { return _PSTHChannel; }
            set { Set(ref _PSTHChannel, value); }
        }

        /// <summary>
        /// 绑定点选
        /// </summary>
        private bool _psthChannelLockToClick = true;
        public bool PSTHChannelLockToClick
        {
            get { return _psthChannelLockToClick; }
            set { Set(ref _psthChannelLockToClick, value); }
        }

        /// <summary>
        /// PSTH trigger通道
        /// </summary>
        private int _PSTHTriggerChannel = 0;
        public int PSTHTriggerChannel
        {
            get { return _PSTHTriggerChannel; }
            set { Set(ref _PSTHTriggerChannel, value); }
        }

        /// <summary>
        /// PSTH trigger边沿：true上升沿，false下降沿
        /// </summary>
        private bool _PSTHTriggerEdge = true;
        public bool PSTHTriggerEdge
        {
            get { return _PSTHTriggerEdge; }
            set { Set(ref _PSTHTriggerEdge, value); }
        }

        /// <summary>
        /// Trials个数
        /// </summary>
        private int _PSTHTrials = 50;
        public int PSTHTrials
        {
            get { return _PSTHTrials; }
            set { Set(ref _PSTHTrials, value); }
        }

        /// <summary>
        /// PSTH
        /// </summary>
        private int _PSTHPreTriggerSpanMS = 100;
        public int PSTHPreTriggerSpanMS
        {
            get { return _PSTHPreTriggerSpanMS; }
            set { Set(ref _PSTHPreTriggerSpanMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _PSTHPostTriggerSpanMS = 500;
        public int PSTHPostTriggerSpanMS
        {
            get { return _PSTHPostTriggerSpanMS; }
            set { Set(ref _PSTHPostTriggerSpanMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _PSTHBinSizeMS = 5;
        public int PSTHBinSizeMS
        {
            get { return _PSTHBinSizeMS; }
            set { Set(ref _PSTHBinSizeMS, value); }
        }

        #endregion


        #region probe map

        /// <summary>
        /// 
        /// </summary>
        private int _probeMapDecayMS = 1000;
        public int ProbeMapDecayMS
        {
            get { return _probeMapDecayMS; }
            set { Set(ref _probeMapDecayMS, value); }
        }

        #endregion


        #region

        /// <summary>
        /// 
        /// </summary>
        private string _commandHost = "127.0.0.1";
        public string CommandHost
        {
            get { return _commandHost; }
            set { Set(ref _commandHost, value); }
        }
        /// <summary>
        /// 
        /// </summary>
        private int _commandPort = 6000;
        public int CommandPort
        {
            get { return _commandPort; }
            set { Set(ref _commandPort, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _waveformHost = "127.0.0.1";
        public string WaveformHost
        {
            get { return _waveformHost; }
            set { Set(ref _waveformHost, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _waveformPort = 5001;
        public int WaveformPort
        {
            get { return _waveformPort; }
            set { Set(ref _waveformPort, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _spikeHost = "127.0.0.1";
        public string SpikeHost
        {
            get { return _spikeHost; }
            set { Set(ref _spikeHost, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _spikePort = 5002;
        public int SpikePort
        {
            get { return _spikePort; }
            set { Set(ref _spikePort, value); }
        }

        #endregion

        private int _mainViewType = 0;

        public int MainViewType
        {
            get { return _mainViewType; }
            set { Set(ref _mainViewType, value); }
        }

        private bool _wavefromChannelnameShow = true;

        public bool WavefromChannelnameShow
        {
            get { return _wavefromChannelnameShow; }
            set { Set(ref _wavefromChannelnameShow, value); }
        }

        private bool _waveformImpedanceShow = false;

        public bool WaveformImpedanceShow
        {
            get { return _waveformImpedanceShow; }
            set { Set(ref _waveformImpedanceShow, value); }
        }

        private ObservableCollection<MultiCbxBaseData> _waveformDisplayTitle = new ObservableCollection<MultiCbxBaseData> {
            new MultiCbxBaseData{ ID=0,ViewName="channel name",IsCheck=true },
            new MultiCbxBaseData{ ID=1,ViewName="impedance",IsCheck=false },
        };

        public ObservableCollection<MultiCbxBaseData> WaveformDisplayTitle
        {
            get { return _waveformDisplayTitle; }
            set 
            { 
                Set(ref _waveformDisplayTitle, value);
                try
                {
                    if (_waveformDisplayTitle != value)
                    {
                        if (value != null)
                        {
                            WavefromChannelnameShow = false;
                            WaveformImpedanceShow = false;
                            foreach (var item in value)
                            {
                                if (item.ViewName == "channel name" && item.IsCheck)
                                {
                                    WavefromChannelnameShow = true;
                                }
                                if (item.ViewName == "impedance" && item.IsCheck)
                                {
                                    WaveformImpedanceShow = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }
                
            }
        }

        private bool _impedanceToCSV = true;
        public bool ImpedanceToCSV
        {
            get { return _impedanceToCSV; }
            set { Set(ref _impedanceToCSV, value); }
        }


        private bool _impedanceToPNG = false;
        public bool ImpedanceToPNG
        {
            get { return _impedanceToPNG; }
            set { Set(ref _impedanceToPNG, value); }
        }
        private bool _impedanceToJPG = false;
        public bool ImpedanceToJPG
        {
            get { return _impedanceToJPG; }
            set { Set(ref _impedanceToJPG, value); }
        }

        private bool _isShowSpikeScopeGrid = false;

        public bool IsShowSpikeScopeGrid
        {
            get { return _isShowSpikeScopeGrid; }
            set { Set(ref _isShowSpikeScopeGrid, value); }
        }


        //private int _playerSpikeScopeChannel = 0;
        //public int PlayerSpikeScopeChannel
        //{
        //    get { return _playerSpikeScopeChannel; }
        //    set { Set(ref _playerSpikeScopeChannel, value); }
        //}
        //private int _playerWaveformChannel = 0;
        //public int PlayerWaveformChannel
        //{
        //    get { return _playerWaveformChannel; }
        //    set { Set(ref _playerWaveformChannel, value); }
        //}

        private List<string> _insertedHeadstages = new List<string>();

        public List<string> InsertedHeadstages
        {
            get { return _insertedHeadstages; }
            set { _insertedHeadstages = value; }
        }
    }


    public class MultiCbxBaseData:ViewModelBase
    {
        private int _id;
        /// <summary>
        /// 关联主键
        /// </summary>
        public int ID
        {
            get { return _id; }
            set { Set(ref _id, value); }
        }

        private string _viewName;
        /// <summary>
        /// 显示名称
        /// </summary>
        public string ViewName
        {
            get { return _viewName; }
            set
            {
                Set(ref _viewName,value);
            }
        }

        private bool _isCheck;
        /// <summary>
        /// 是否选中
        /// </summary>
        public bool IsCheck
        {
            get { return _isCheck; }
            set { Set(ref _isCheck,value); }
        }
    }
}
