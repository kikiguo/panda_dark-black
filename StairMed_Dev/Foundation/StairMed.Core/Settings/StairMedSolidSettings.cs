using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.MVVM.Base;
using StairMed.Probe;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Timers;

namespace StairMed.Core.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class StairMedSolidSettings : ViewModelBase
    {
        const string TAG = nameof(StairMedSolidSettings);
        private readonly Timer _timer = new Timer
        {
            Interval = 100,
            AutoReset = false,
        };

        #region 单例

        private class Helper
        {
            internal static PersistenceHelper _persistence = new PersistenceHelper("stairmed-settings.xml");
            internal static StairMedSolidSettings _instance = new StairMedSolidSettings();
            internal static bool _loaded = false;
            static Helper()
            {
                var instance = _persistence.Load<StairMedSolidSettings>();
                if (instance != null)
                {
                    _instance = instance;
                }
                _loaded = true;

                //
                if (_instance.HistoryCodes == null)
                {
                    _instance.HistoryCodes = new List<string>();
                }

                //
                if (_instance.HistoryTypes == null)
                {
                    _instance.HistoryTypes = new List<string>();
                }

                //
                if (string.IsNullOrWhiteSpace(_instance.NeuralDataSaveFolder) || !Directory.Exists(_instance.NeuralDataSaveFolder))
                {
                    var path = Path.Combine(CONST.AppInstallPath, $"Neural_Data_{ReadonlyCONST.DeviceType}");
                    if (ToolMix.HasChinese(path))
                    {
                        path = Path.Combine(Path.GetPathRoot(CONST.AppInstallPath), $"Neural_Data_{ReadonlyCONST.DeviceType}");
                    }

                    //
                    _instance.NeuralDataSaveFolder = path;
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(_instance.NeuralDataSaveFolder);
                    }
                }
            }
        }
        public static StairMedSolidSettings Instance => Helper._instance;
        private StairMedSolidSettings()
        {
            _timer.Elapsed += (sender, e) =>
            {
                Helper._persistence.Save(this);
                var changes = new HashSet<string>();
                lock (_changedParams)
                {
                    foreach (var item in _changedParams)
                    {
                        changes.Add(item);
                    }
                    _changedParams.Clear();
                }
                OnSettingChanged?.Invoke(changes);
            };
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public event Action<HashSet<string>> OnSettingChanged;

        /// <summary>
        /// 
        /// </summary>
        private HashSet<string> _changedParams = new HashSet<string>();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected override bool Set<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            var changed = base.Set(ref storage, value, propertyName);
            if (changed && Helper._loaded)
            {
                lock (_changedParams)
                {
                    _changedParams.Add(propertyName);
                }

                //
                _timer.Stop();
                _timer.Start();
            }
            return changed;
        }

        /// <summary>
        /// 采集数据保存路径
        /// </summary>
        private string _neuralDataSaveFolder;
        public string NeuralDataSaveFolder
        {
            get { return _neuralDataSaveFolder; }
            set { Set(ref _neuralDataSaveFolder, value); }
        }
        /// <summary>
        /// 案例编号
        /// </summary>
        private string _testCode = string.Empty;
        public string TestCode
        {
            get { return _testCode; }
            set { Set(ref _testCode, value); }
        }

        /// <summary>
        /// 医院
        /// </summary>
        private string _testHosptial = string.Empty;
        public string TestHosptial
        {
            get { return _testHosptial; }
            set { Set(ref _testHosptial, value); }
        }

        /// <summary>
        /// 实验对象
        /// </summary>
        private string _testType = string.Empty;
        public string TestType
        {
            get { return _testType; }
            set { Set(ref _testType, value); }
        }

        /// <summary>
        /// 电极序列
        /// </summary>
        private string _testEleCode = string.Empty;
        public string TestEleCode
        {
            get { return _testEleCode; }
            set { Set(ref _testEleCode, value); }
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        private string _createTime = string.Empty;
        public string CreateTime
        {
            get { return _createTime; }
            set { Set(ref _createTime, value); }
        }


        /// <summary>
        /// 所有的实验编号
        /// </summary>
        private List<string> _historyCodes = new List<string>();
        public List<string> HistoryCodes
        {
            get { return _historyCodes; }
            set { Set(ref _historyCodes, value); }
        }

        /// <summary>
        /// 所有的实验医院
        /// </summary>
        private List<string> _historyHospital = new List<string>();
        public List<string> HistoryHospital
        {
            get { return _historyHospital; }
            set { Set(ref _historyHospital, value); }
        }


        /// <summary>
        /// 所有的实验对象
        /// </summary>
        private List<string> _historyTypes = new List<string>();
        public List<string> HistoryTypes
        {
            get { return _historyTypes; }
            set { Set(ref _historyTypes, value); }
        }

        /// <summary>
        /// 所有电极序号
        /// </summary>
        private List<string> _historyEleCode = new List<string>();
        public List<string> HistoryEleCode
        {
            get { return _historyEleCode; }
            set { Set(ref _historyEleCode, value); }
        }

        /// <summary>
        /// 所有电极序号
        /// </summary>
        private List<string> _historyCreateTime = new List<string>();
        public List<string> HistoryCreateTime
        {
            get { return _historyCreateTime; }
            set { Set(ref _historyCreateTime, value); }
        }

        /// <summary>
        /// 是否显示阈值线
        /// </summary>
        private bool _showThresholdLine = true;
        public bool ShowThresholdLine
        {
            get { return _showThresholdLine; }
            set { Set(ref _showThresholdLine, value); }
        }

        /// <summary>
        /// 显示零值线
        /// </summary>
        private bool _showZeroLine = true;
        public bool ShowZeroLine
        {
            get { return _showZeroLine; }
            set { Set(ref _showZeroLine, value); }
        }

        /// <summary>
        /// spike发放率统计时长
        /// </summary>
        private int _spikeRateStatisticMS = 1000;
        public int SpikeRateStatisticMS
        {
            get { return _spikeRateStatisticMS; }
            set { Set(ref _spikeRateStatisticMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _filterStatisticMS = 1000;
        public int FilterStatisticMS
        {
            get { return _filterStatisticMS; }
            set { Set(ref _filterStatisticMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _RMSStatisticMS = 1000;
        public int RMSStatisticMS
        {
            get { return _RMSStatisticMS; }
            set { Set(ref _RMSStatisticMS, value); }
        }

        /// <summary>
        /// 默认配置的路径
        /// </summary>
        private string _defaultSettingPath = string.Empty;
        public string DefaultSettingPath
        {
            get { return _defaultSettingPath; }
            set { Set(ref _defaultSettingPath, value); }
        }
    }
}
