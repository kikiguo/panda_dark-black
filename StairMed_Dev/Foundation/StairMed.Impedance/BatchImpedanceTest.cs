﻿using StairMed.AsyncFIFO;
using StairMed.ChipConst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StairMed.Impedance
{
    /// <summary>
    /// 
    /// </summary>
    public class BatchImpedanceTest
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(BatchImpedanceTest);

        /// <summary>
        /// 
        /// </summary>
        private readonly AutoResetEvent _resetEvent = new AutoResetEvent(false);

        /// <summary>
        /// 
        /// </summary>
        private IntanImpedenceTest _tester; //阻抗测试

        /// <summary>
        /// 接收到的adc数据
        /// </summary>
        private readonly IAsyncFIFO _adcFIFO = null;

        /// <summary>
        /// 要测试的通道
        /// </summary>
        private List<int> _inputChannels = null;

        /// <summary>
        /// 单通道阻抗测试结果回调
        /// </summary>
        private Action<int, double, double> _callback = null;

        /// <summary>
        /// 每次阻抗测试应返回的ADC数据字节长度
        /// </summary>
        private int _requireADCByteLen = 0;

        /// <summary>
        /// 阻抗测试的ACD数据
        /// </summary>
        private readonly Dictionary<int, Dictionary<int, byte[]>> _channelCapDACDict = new Dictionary<int, Dictionary<int, byte[]>>();

        /// <summary>
        /// 已经测出阻抗的通道
        /// </summary>
        private readonly HashSet<int> _reportedChannels = new HashSet<int>();

        /// <summary>
        /// 
        /// </summary>
        private readonly List<byte> _bufferedWaveform = new List<byte>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IBatchImpedanceHelper _helper;

        /// <summary>
        /// 
        /// </summary>
        private int _maxWaitTimeMS = 3000;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="helper"></param>
        public BatchImpedanceTest(IBatchImpedanceHelper helper, int maxWaitTimeMS = 3000)
        {
            _helper = helper;
            _maxWaitTimeMS = maxWaitTimeMS;
            _adcFIFO = new BlockFIFO { Name = nameof(_adcFIFO), ConsumerAction = (object[] objs) => HandleRecvdImpedanceADCData((int)objs[0], (byte[])objs[1]) };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        private void HandleRecvdImpedanceADCData(int frameLen, byte[] frame)
        {
            if (!_helper.ResolveImpedanceADCData(frameLen, frame, out int channel, out int cap, out int adcOffset))
            {
                return;
            }

            //
            if (cap == 3)
            {
                cap = 2;
            }

            //长度超过需要值：不再处理，装死
            if (_bufferedWaveform.Count > _requireADCByteLen)
            {
                Logger.LogTool.Logger.LogE($"{nameof(HandleRecvdImpedanceADCData)}, require len={_requireADCByteLen / 2}, real len={(_bufferedWaveform.Count) / 2}");
                return;
            }

            //添加数据
            _bufferedWaveform.AddRange(frame.Skip(adcOffset).Take(frameLen - adcOffset));
            _resetEvent.Set();

            //不够长度，则等待添加下一包
            if (_bufferedWaveform.Count < _requireADCByteLen)
            {
                Logger.LogTool.Logger.LogT($"{nameof(HandleRecvdImpedanceADCData)}, channel={channel}, cap={cap}, require len={_requireADCByteLen / 2}, recvd len={(_bufferedWaveform.Count) / 2}");
                return;
            }

            //提取通道和电容
            Logger.LogTool.Logger.LogT($"{nameof(HandleRecvdImpedanceADCData)}, channel={channel}, cap={cap}, recvd len={_requireADCByteLen / 2}");

            //
            if (!_channelCapDACDict.ContainsKey(channel))
            {
                _channelCapDACDict[channel] = new Dictionary<int, byte[]>();
            }

            //保存ADC数据
            _channelCapDACDict[channel][cap] = _bufferedWaveform.ToArray();
            _bufferedWaveform.Clear();

            //判断是否已测试0.1pf、1pf、10pf
            if (_channelCapDACDict[channel].Count != 3)
            {
                return;
            }

            //执行阻抗测试计算
            var indexInInputChannel = _inputChannels.IndexOf(channel);
            if (indexInInputChannel < 0)
            {
                return;
            }

            //
            _tester.Channel = indexInInputChannel;
            var waveformBytes = new List<byte[]>
            {
                _channelCapDACDict[channel][0],
                _channelCapDACDict[channel][1],
                _channelCapDACDict[channel][2]
            };
            _channelCapDACDict.Remove(channel);
            var complex = _tester.MeasureComplexAmplitude(waveformBytes);

            //上报 单通道阻抗结果
            Task.Run(() =>
            {
                //Logger.LogTool.Logger.LogI($"channel={indexInInputChannel}, magnitude={complex.Magnitude:F2}, phase={complex.Phase:F2}");
                _callback?.Invoke(indexInInputChannel, complex.Magnitude, complex.Phase);
                _reportedChannels.Add(indexInInputChannel);

                //
                if (_reportedChannels.Count == _inputChannels.Count)
                {
                    _resetEvent.Set();
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannels"></param>
        /// <param name="applyNotch"></param>
        /// <param name="fNotch"></param>
        /// <param name="notchBandwidth"></param>
        /// <param name="freq"></param>
        /// <param name="sampleRate"></param>
        /// <param name="callback"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public bool ImpedanceTest(List<int> inputChannels, bool applyNotch, double fNotch, double notchBandwidth, double freq, double sampleRate, double lowerBandwidth, double upperBandwidth, bool enableDSP, double dspCutoff, Action<int, double, double> callback, out string errMsg)
        {
            _bufferedWaveform.Clear();
            _reportedChannels.Clear();
            _inputChannels = inputChannels;
            _callback = callback;
            _tester = new IntanImpedenceTest(freq, sampleRate, 128.0, applyNotch, fNotch, notchBandwidth, upperBandwidth);

            //
            errMsg = string.Empty;
            if (!_tester.CreateZcheckDac(out byte[] period, out int numPeriods))
            {
                errMsg = "测试频率设置不合适，请调整";
                return false;
            }

            //
            if (!_helper.InitImpedance(sampleRate, lowerBandwidth, upperBandwidth, enableDSP, dspCutoff, period, out errMsg))
            {
                _helper.StopImpedance();
                return false;
            }

            //
            var succeed = true;

            //
            _helper.ImpedanceADCDataRecvd -= OnImpedanceADCDataRecvd;
            _helper.ImpedanceADCDataRecvd += OnImpedanceADCDataRecvd;

            //
            try
            {
                //设置阻抗测试通道
                if (!_helper.SetImpedanceChannels(inputChannels))
                {
                    errMsg = "下发数据失败";
                    succeed = false;
                }
                else
                {
                    _requireADCByteLen = (period.Length * numPeriods) * 2;

                    //配置DAC，并开始阻抗测试（合并所有开始）
                    if (!_helper.StartImpedance(numPeriods))
                    {
                        errMsg = "下发数据失败";
                        succeed = false;
                    }
                    else
                    {
                        while (true)
                        {
                            if (!_resetEvent.WaitOne(_maxWaitTimeMS))
                            {
                                break;
                            }

                            //
                            if (_reportedChannels.Count >= _inputChannels.Count)
                            {
                                break;
                            }
                        }

                        //
                        succeed = _reportedChannels.Count >= _inputChannels.Count;
                        if (!succeed)
                        {
                            errMsg = "接收数据失败";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(ImpedanceTest)}", ex);
            }

            //
            _helper.StopImpedance();
            _helper.ImpedanceADCDataRecvd -= OnImpedanceADCDataRecvd;

            //
            return succeed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        private void OnImpedanceADCDataRecvd(int framelen, byte[] frame)
        {
            _adcFIFO.Add(framelen, frame);
        }
    }
}
