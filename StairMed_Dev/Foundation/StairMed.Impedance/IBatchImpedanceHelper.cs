﻿using System;
using System.Collections.Generic;

namespace StairMed.Impedance
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBatchImpedanceHelper
    {
        public bool InitImpedance(double sampleRate, double lowerBandwidth, double upperBandwidth, bool enableDSP, double dspCutoff, byte[] period, out string errMsg);

        public bool SetImpedanceChannels(List<int> inputChannels);

        public bool StartImpedance(int numPeriods);

        public bool StopImpedance();

        public event Action<int, byte[]> ImpedanceADCDataRecvd;

        public bool ResolveImpedanceADCData(int frameLen, byte[] frame, out int channel, out int cap, out int adcOffset);
    }
}
