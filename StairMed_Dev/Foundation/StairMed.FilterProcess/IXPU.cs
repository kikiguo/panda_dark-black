﻿using StairMed.Array;

namespace StairMed.FilterProcess
{
    /// <summary>
    /// 
    /// </summary>
    public interface IXPU
    {
        /// <summary>
        /// 
        /// </summary>
        ParamCenter ParamCenter { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headbytes"></param>
        /// <param name="chunk">ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN</param>
        /// <param name="raws">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="wfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="hfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="lfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="spks">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <returns></returns>
        bool ProcessByteADChunk(int offset, byte[] chunk, out PooledArray<float> raws, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wfps">ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN</param>
        /// <param name="hfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="lfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="spks">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <returns></returns>
        bool ProcessWFPChunk(float[] chunk, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spks"></param>
        /// <returns></returns>
        bool ProcessSpkChunk(float[] chunk, out PooledArray<float> spks);

        /// <summary>
        /// 
        /// </summary>
        void Free();
    }
}
