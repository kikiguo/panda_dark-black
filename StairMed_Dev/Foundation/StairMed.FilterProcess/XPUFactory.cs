﻿using StairMed.FilterProcess.XPU.GPU.CUDA;
using System;

namespace StairMed.FilterProcess
{
    /// <summary>
    /// 
    /// </summary>
    public class XPUFactory
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly bool _useMatlab = true;

        #region 单例

        private static readonly XPUFactory instance = new XPUFactory();
        public static XPUFactory Instance => instance;
        private XPUFactory() { }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IXPU GetXPU()
        {
            if (IsSupportCuda())
            {
                return GetGPU();
            }
            else
            {
                return GetCPU();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IXPU GetGPU()
        {
            return _useMatlab ? new MatlabCUDA() : (IXPU)new IntanCUDA();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IXPU GetCPU()
        {
            return new XPU.CPU.CPU(_useMatlab);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsSupportCuda()
        {
            return CUDALib.IsSupportCUDA();
            //try
            //{
            //    var len = 30;
            //    var rand = new Random((int)DateTime.Now.Ticks);

            //    //
            //    var a = new int[len];
            //    var b = new int[len];
            //    var c = new int[len];
            //    var d = new int[len];
            //    for (int i = 0; i < len; i++)
            //    {
            //        a[i] = rand.Next(1, 1000);
            //        b[i] = rand.Next(1, 1000);
            //        d[i] = a[i] + b[i];
            //    }

            //    //
            //    CUDALib.AddTest(len, a, b, c);

            //    //
            //    for (int i = 0; i < len; i++)
            //    {
            //        if (c[i] != d[i])
            //        {
            //            return false;
            //        }
            //    }

            //    //
            //    return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }
    }
}
