﻿namespace StairMed.FilterProcess.ParamMonitor
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ParamMonitorBase
    {
        /// <summary>
        /// 通道数
        /// </summary>
        protected int _channelCount = 0;
        internal int ChannelCount => _channelCount;

        /// <summary>
        /// 单个通道样本个数
        /// </summary>
        protected int _sampleCount = 0;
        internal int SampleCount => _sampleCount;

        /// <summary>
        /// 采样率
        /// </summary>
        protected int _sampleRate = 100;

        /// <summary>
        /// 是否参数发生了变化
        /// </summary>
        protected bool _changed = true;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="sampleRate"></param>
        public void UpdateBaseParam(int channelCount, int sampleCount, int sampleRate)
        {
            if (_channelCount != channelCount || _sampleCount != sampleCount || _sampleRate != sampleRate)
            {
                _channelCount = channelCount;
                _sampleCount = sampleCount;
                _sampleRate = sampleRate;
                _changed = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="force"></param>
        public abstract void ApplyWhenChanged(bool force = false);
    }
}
