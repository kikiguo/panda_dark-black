﻿using Microsoft.VisualBasic;
using System;

namespace StairMed.FilterProcess.ParamMonitor
{
    /// <summary>
    /// Monitors external trigger signals.
    /// </summary>
    public class ExternalTrigMonitor : ParamMonitorBase
    {
        private readonly Action<int, int, int, int> _handleParamChanged;

        public  int _triggerLevel;
        public int _triggerDelay;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalTrigMonitorRewrite"/> class.
        /// </summary>
        /// <param name="onParamChanged">The action to perform when parameters change.</param>
        public ExternalTrigMonitor(Action<int, int, int, int> onParamChanged)
        {
            _handleParamChanged = onParamChanged;
        }

        public void UpdateBaseParam(int triggerLevel, int triggerDelay)
        {
            if (_triggerLevel != triggerLevel || _triggerDelay != triggerDelay)
            {
                _triggerLevel = triggerLevel;
                _triggerDelay = triggerDelay;
                _changed = true; 
            }
        }

        /// <summary>
        /// Updates the K and B values for Trig2 and Trig3.
        /// </summary>
        /// <param name="trig2K">New K value for Trig2.</param>
        /// <param name="trig2B">New B value for Trig2.</param>
        /// <param name="trig3K">New K value for Trig3.</param>
        /// <param name="trig3B">New B value for Trig3.</param>
        public void Update(int triggerLevel, int triggerDelay)
        {
            _triggerLevel = triggerLevel;
            _triggerDelay = triggerDelay;
        }
   
        /// <summary>
        /// Applies changes when required.
        /// </summary>
        /// <param name="force">Forces the application of changes.</param>
        public override void ApplyWhenChanged(bool force = false)
        {
            if (force || _changed)
            {
                _handleParamChanged?.Invoke(_channelCount, _sampleCount, _triggerLevel, _triggerDelay);
                _changed = false;
            }
        }
    }
}
