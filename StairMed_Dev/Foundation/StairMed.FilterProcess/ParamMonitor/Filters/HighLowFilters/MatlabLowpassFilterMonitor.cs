﻿using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Filters.MatlabFilters;
using System;

namespace StairMed.FilterProcess.ParamMonitor.Filters.HighLowFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabLowpassFilterMonitor : FilterMonitorBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramSetter"></param>
        public MatlabLowpassFilterMonitor(Action<int, int, Filter> onParamChanged) : base(onParamChanged)
        {

        }

        /// <summary>
        /// 滤波器
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public override Filter GetFilter(int sampleRate, FilterType filterType, int order, int cutoff)
        {
            if (filterType == FilterType.Bessel)
            {
                return new MatlabBesselLowpassFilter(order, cutoff, sampleRate);
            }
            else
            {
                return new MatlabButterworthLowpassFilter(order, cutoff, sampleRate);
            }
        }
    }
}
