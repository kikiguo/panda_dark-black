﻿using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Filters.Intan;
using System;

namespace StairMed.FilterProcess.ParamMonitor.Filters.HighLowFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class IntanLowpassFilterMonitor : FilterMonitorBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramSetter"></param>
        public IntanLowpassFilterMonitor(Action<int, int, Filter> onParamChanged) : base(onParamChanged)
        {

        }

        /// <summary>
        /// 滤波器
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public override Filter GetFilter(int sampleRate, FilterType filterType, int order, int cutoff)
        {
            if (filterType == FilterType.Bessel)
            {
                return new IntanBesselLowpassFilter(order, cutoff, sampleRate);
            }
            else
            {
                return new IntanButterworthLowpassFilter(order, cutoff, sampleRate);
            }
        }
    }
}
