﻿using StairMed.FilterProcess.Filters;
using System;

namespace StairMed.FilterProcess.ParamMonitor.Filters.HighLowFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class FilterMonitorBase : ParamMonitorBase
    {
        /// <summary>
        /// 滤波器类型
        /// </summary>
        private FilterType _filterType = 0;

        /// <summary>
        /// 阶数
        /// </summary>
        private int _order = 2;

        /// <summary>
        /// 截止频率
        /// </summary>
        private int _cutoff = 250;

        /// <summary>
        /// 
        /// </summary>
        private readonly Action<int, int, Filter> _handleParamChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramSetter"></param>
        public FilterMonitorBase(Action<int, int, Filter> onParamChanged)
        {
            _handleParamChanged = onParamChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="filterType"></param>
        public void Update(int cutoff, int order, FilterType filterType)
        {
            if (_cutoff != cutoff || _order != order || _filterType != filterType)
            {
                _cutoff = cutoff;
                _order = order;
                _filterType = filterType;
                _changed = true;
            }
        }

        /// <summary>
        /// 更新滤波器
        /// </summary>
        /// <param name="force"></param>
        public override void ApplyWhenChanged(bool force = false)
        {
            if (force || _changed)
            {
                var filter = GetFilter(_sampleRate, _filterType, _order, _cutoff);

                //
                _handleParamChanged?.Invoke(_channelCount, _sampleCount, filter);

                //
                _changed = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="filterType"></param>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public abstract Filter GetFilter(int sampleRate, FilterType filterType, int order, int cutoff);
    }
}
