﻿using StairMed.FilterProcess.Filters;
using System;

namespace StairMed.FilterProcess.ParamMonitor.Filters.NotchFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class NotchFilterMonitorBase : ParamMonitorBase
    {
        /// <summary>
        /// 陷波频率
        /// </summary>
        private int _notchHz = 0;

        /// <summary>
        /// 陷波带宽
        /// </summary>
        private int _notchBandwidth = 10;

        /// <summary>
        /// 
        /// </summary>
        private readonly Action<int, int, Filter> _handleParamChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onParamChanged"></param>
        public NotchFilterMonitorBase(Action<int, int, Filter> onParamChanged)
        {
            _handleParamChanged = onParamChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        internal void Update(int notchHz, int notchBandwidth)
        {
            if (_notchHz != notchHz || _notchBandwidth != notchBandwidth)
            {
                _notchHz = notchHz;
                _notchBandwidth = notchBandwidth;
                _changed = true;
            }
        }

        /// <summary>
        /// 更新滤波器
        /// </summary>
        /// <param name="force"></param>
        public override void ApplyWhenChanged(bool force = false)
        {
            if (force || _changed)
            {
                var filter = GetFilter(_sampleRate, _notchHz, _notchBandwidth);

                //
                _handleParamChanged?.Invoke(_channelCount, _sampleCount, filter);

                //
                _changed = false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        /// <returns></returns>
        public abstract Filter GetFilter(int sampleRate, int notchHz, int notchBandwidth);
    }
}
