﻿using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Filters.Intan;
using System;

namespace StairMed.FilterProcess.ParamMonitor.Filters.NotchFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class IntanNotchFilterMonitor : NotchFilterMonitorBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="onParamChanged"></param>
        public IntanNotchFilterMonitor(Action<int, int, Filter> onParamChanged) : base(onParamChanged)
        {

        }

        /// <summary>
        /// 滤波器
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        /// <returns></returns>
        public override Filter GetFilter(int sampleRate, int notchHz, int notchBandwidth)
        {
            if (notchHz == 0)
            {
                return null;
            }

            //
            return new IntanNotchFilter(notchHz, notchBandwidth, sampleRate);
        }
    }
}
