﻿using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Filters.MatlabFilters;
using System;

namespace StairMed.FilterProcess.ParamMonitor.Filters.NotchFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabNotchFilterMonitor : NotchFilterMonitorBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="onParamChanged"></param>
        public MatlabNotchFilterMonitor(Action<int, int, Filter> onParamChanged) : base(onParamChanged)
        {

        }

        /// <summary>
        /// 滤波器
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        /// <returns></returns>
        public override Filter GetFilter(int sampleRate, int notchHz, int notchBandwidth)
        {
            if (notchHz == 0)
            {
                return null;
            }

            //
            return new MatlabNotchFilter(1, notchHz, notchBandwidth, sampleRate);
        }
    }
}
