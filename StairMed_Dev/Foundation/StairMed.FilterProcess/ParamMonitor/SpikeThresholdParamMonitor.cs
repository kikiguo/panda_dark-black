﻿using System;
using System.Collections.Generic;

namespace StairMed.FilterProcess.ParamMonitor
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeThresholdParamMonitor : ParamMonitorBase
    {
        /// <summary>
        /// 
        /// </summary>
        private List<float> _thresholds = new List<float>();

        /// <summary>
        /// 
        /// </summary>
        private float _threshold = -100;

        /// <summary>
        /// 
        /// </summary>
        private bool _allSame = true;

        /// <summary>
        /// 
        /// </summary>
        private readonly Action<int, int, int, float> _handleParamChanged;
        private readonly Action<int, int, int, float[]> _handleParamChangedArray;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onParamChanged"></param>
        /// <param name="onParamChangedArray"></param>
        public SpikeThresholdParamMonitor(Action<int, int, int, float> onParamChanged, Action<int, int, int, float[]> onParamChangedArray)
        {
            _handleParamChanged = onParamChanged;
            _handleParamChangedArray = onParamChangedArray;
        }

        /// <summary>
        /// 设置阈值
        /// </summary>
        /// <param name="thresholds"></param>
        internal void Update(List<float> thresholds)
        {
            _allSame = false;
            _thresholds = thresholds;
            _changed = true;
        }

        /// <summary>
        /// 设置阈值
        /// </summary>
        /// <param name="thresholds"></param>
        internal void Update(float threshold)
        {
            _allSame = true;
            _threshold = threshold;
            _changed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void ApplyWhenChanged(bool force = false)
        {
            if (force || _changed)
            {
                //
                var spikeGap = Convert.ToInt32(_sampleRate * 0.002);

                //
                if (!_allSame)
                {
                    _handleParamChangedArray?.Invoke(_channelCount, _sampleCount, spikeGap, _thresholds.ToArray());
                }
                else
                {
                    _handleParamChanged?.Invoke(_channelCount, _sampleCount, spikeGap, _threshold);
                }

                //
                _changed = false;
            }
        }
    }
}
