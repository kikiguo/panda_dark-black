﻿using System;

namespace StairMed.FilterProcess.ParamMonitor
{
    /// <summary>
    /// 
    /// </summary>
    internal class RawKBMonitor : ParamMonitorBase
    {
        /// <summary>
        /// raw = k * (ad + b)
        /// </summary>
        internal float _rawK = 1.0f;

        /// <summary>
        /// raw = k * (ad + b)
        /// </summary>
        internal int _rawB = 0;

        /// <summary>
        /// 
        /// </summary>
        private readonly Action<int, int, float, int> _handleParamChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramSetter"></param>
        public RawKBMonitor(Action<int, int, float, int> onParamChanged)
        {
            _handleParamChanged = onParamChanged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawK"></param>
        /// <param name="rawB"></param>
        internal void Update(float rawK, int rawB)
        {
            if (_rawK != rawK || _rawB != rawB)
            {
                _rawK = rawK;
                _rawB = rawB;
                _changed = true;
            }
        }

        /// <summary>
        /// 初始化raw参数
        /// </summary>
        public override void ApplyWhenChanged(bool force = false)
        {
            if (force || _changed)
            {
                _handleParamChanged?.Invoke(_channelCount, _sampleCount, _rawK, _rawB);

                //
                _changed = false;
            }
        }
    }
}
