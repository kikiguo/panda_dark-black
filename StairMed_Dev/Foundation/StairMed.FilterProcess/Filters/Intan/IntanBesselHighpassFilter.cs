﻿using StairMed.FilterProcess.Filters.Intan.Base;
using System.Collections.Generic;

namespace StairMed.FilterProcess.Filters.Intan
{
    public class IntanBesselHighpassFilter : HighOrderFilter
    {
        public IntanBesselHighpassFilter(int order, double fc, double sampleRate)
        {
            switch (order)
            {
                case 1:
                    {
                        filters = new List<BiquadFilter>
                        {
                            new FirstOrderHighpassFilter(fc, sampleRate)
                        };
                    }
                    break;
                case 2:
                    {
                        filters = new List<BiquadFilter>
                        {
                            new SecondOrderHighpassFilter(fc / 1.2736, 0.5773, sampleRate)
                        };
                    }
                    break;
                case 3:
                    {
                        filters = new List<BiquadFilter>
                        {
                            new FirstOrderHighpassFilter(fc / 1.3270, sampleRate),
                            new SecondOrderHighpassFilter(fc / 1.4524, 0.6910, sampleRate)
                        };
                    }
                    break;
                case 4:
                    {
                        filters = new List<BiquadFilter>
                        {
                            new SecondOrderHighpassFilter(fc / 1.4192, 0.5219, sampleRate),
                            new SecondOrderHighpassFilter(fc / 1.5912, 0.8055, sampleRate)
                        };
                    }
                    break;
                case 5:
                    {
                        filters = new List<BiquadFilter>
                        {
                            new FirstOrderHighpassFilter(fc / 1.5069, sampleRate),
                            new SecondOrderHighpassFilter(fc / 1.5611, 0.5635, sampleRate),
                            new SecondOrderHighpassFilter(fc / 1.7607, 0.9165, sampleRate)
                        };
                    }
                    break;
                case 6:
                    filters = new List<BiquadFilter>
                    {
                        new SecondOrderHighpassFilter(fc / 1.6060, 0.5103, sampleRate),
                        new SecondOrderHighpassFilter(fc / 1.6913, 0.6112, sampleRate),
                        new SecondOrderHighpassFilter(fc / 1.9071, 1.0234, sampleRate)
                    };
                    break;
                case 7:
                    filters = new List<BiquadFilter>
                    {
                        new FirstOrderHighpassFilter(fc / 1.6853, sampleRate),
                        new SecondOrderHighpassFilter(fc / 1.7174, 0.5324, sampleRate),
                        new SecondOrderHighpassFilter(fc / 1.8235, 0.6608, sampleRate),
                        new SecondOrderHighpassFilter(fc / 2.0507, 1.1262, sampleRate)
                    };
                    break;
                case 8:
                    filters = new List<BiquadFilter>
                    {
                        new SecondOrderHighpassFilter(fc / 1.7837, 0.5060, sampleRate),
                        new SecondOrderHighpassFilter(fc / 1.8376, 0.5596, sampleRate),
                        new SecondOrderHighpassFilter(fc / 1.9591, 0.7109, sampleRate),
                        new SecondOrderHighpassFilter(fc / 2.1953, 1.2258, sampleRate)
                    };
                    break;
                default:
                    break;
            }
        }
    };
}
