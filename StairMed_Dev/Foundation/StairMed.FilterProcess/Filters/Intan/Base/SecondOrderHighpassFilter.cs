﻿using System;

namespace StairMed.FilterProcess.Filters.Intan.Base
{
    internal class SecondOrderHighpassFilter : BiquadFilter
    {
        public SecondOrderHighpassFilter(double fc, double q, double sampleRate)
        {
            isDcGainZero = true;
            double k = Math.Tan(Math.PI * fc / sampleRate);
            double norm = 1.0 / (1.0 + k / q + k * k);
            b0 = (float)norm;
            b1 = (float)(-2.0 * norm);
            b2 = b0;
            a1 = (float)(2.0 * (k * k - 1.0) * norm);
            a2 = (float)((1.0 - k / q + k * k) * norm);
        }

        public SecondOrderHighpassFilter()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new SecondOrderHighpassFilter
            {
                a1 = a1,
                a2 = a2,
                b0 = b0,
                b1 = b1,
                b2 = b2,
                isDcGainZero = isDcGainZero,
            };
        }
    }
}
