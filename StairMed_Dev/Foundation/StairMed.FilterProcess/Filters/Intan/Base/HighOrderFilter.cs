﻿using System;
using System.Collections.Generic;

namespace StairMed.FilterProcess.Filters.Intan.Base
{
    public class HighOrderFilter : Filter
    {
        public override float filterOne(float inVal)
        {
            var outVal = inVal;
            foreach (var f in filters)
            {
                outVal = f.filterOne(outVal);
            }
            return outVal;
        }

        public override void reset()
        {
            foreach (var f in filters)
            {
                f.reset();
            }
        }


        public List<BiquadFilter> getFilters()
        {
            return filters;
        }

        public List<BiquadFilter> filters;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var list = new List<string>();
            foreach (var filter in filters)
            {
                list.Add(filter.ToString());
            }
            return string.Join(Environment.NewLine, list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override float[] getparams()
        {
            var list = new List<float>();
            foreach (var filter in filters)
            {
                list.AddRange(filter.getparams());
            }
            return list.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            var clone = new HighOrderFilter();
            var filters = new List<BiquadFilter>();
            {
                foreach (var filter in this.filters)
                {
                    filters.Add((BiquadFilter)filter.Clone());
                }
            }
            clone.filters = filters;
            return clone;
        }
    }
}
