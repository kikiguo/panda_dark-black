﻿using System;

namespace StairMed.FilterProcess.Filters.Intan.Base
{
    internal class SecondOrderLowpassFilter : BiquadFilter
    {
        public SecondOrderLowpassFilter(double fc, double q, double sampleRate)
        {
            isDcGainZero = false;
            double k = Math.Tan(Math.PI * fc / sampleRate);
            double norm = 1.0 / (1.0 + k / q + k * k);
            b0 = (float)(k * k * norm);
            b1 = (float)(2.0 * k * k * norm);
            b2 = b0;
            a1 = (float)(2.0 * (k * k - 1.0) * norm);
            a2 = (float)((1.0 - k / q + k * k) * norm);
        }

        public SecondOrderLowpassFilter()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new SecondOrderLowpassFilter
            {
                a1 = a1,
                a2 = a2,
                b0 = b0,
                b1 = b1,
                b2 = b2,
                isDcGainZero = isDcGainZero,
            };
        }
    }
}
