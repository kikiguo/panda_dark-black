﻿using System;

namespace StairMed.FilterProcess.Filters.Intan.Base
{
    internal class SecondOrderNotchFilter : BiquadFilter
    {
        public SecondOrderNotchFilter(double fNotch, double bandwidth, double sampleRate)
        {
            isDcGainZero = false;
            double d = Math.Exp(-1.0 * Math.PI * bandwidth / sampleRate);
            double b = (1.0 + d * d) * Math.Cos(2 * Math.PI * fNotch / sampleRate);
            b0 = (float)((1.0 + d * d) / 2.0);
            b1 = (float)-b;
            b2 = b0;
            a1 = b1;
            a2 = (float)(d * d);
        }

        public SecondOrderNotchFilter()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new SecondOrderNotchFilter
            {
                a1 = a1,
                a2 = a2,
                b0 = b0,
                b1 = b1,
                b2 = b2,
                isDcGainZero = isDcGainZero,
            };
        }
    }
}
