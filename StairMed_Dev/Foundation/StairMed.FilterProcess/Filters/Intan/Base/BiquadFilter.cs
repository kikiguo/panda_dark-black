﻿namespace StairMed.FilterProcess.Filters.Intan.Base
{
    /// <summary>
    /// // Implement a digital biquad filter with a transfer function
    //
    //           b0 + b1 z^-1 + b2 z^-2
    //   H(z)  = ----------------------
    //           a0 + a1 z^-1 + a2 z^-2
    //
    // Coefficients are normalized so that a[0] = 1.
    /// </summary>
    public abstract class BiquadFilter : Filter
    {
        public BiquadFilter()
        {
            reset();
        }


        public override float filterOne(float inVal)
        {
            if (firstTime)
            {
                prevIn = 0;
                prevPrevIn = 0;

                //
                if (isDcGainZero)
                {
                    prevOut = 0.0f;
                    prevPrevOut = 0.0f;
                }
                else
                {
                    prevOut = inVal;
                    prevPrevOut = inVal;
                }
                firstTime = false;
            }
            // Direct Form 1 implementation
            float outVal = b0 * inVal + b1 * prevIn + b2 * prevPrevIn - a1 * prevOut - a2 * prevPrevOut;
            // TODO: Compare to Direct Form 2 implementation (see Wikipedia page on "Digital biquad filter").

            prevPrevIn = prevIn;
            prevIn = inVal;
            prevPrevOut = prevOut;
            prevOut = outVal;

            return outVal;
        }

        public override void reset()
        {
            firstTime = true;
        }

        public float getA1()
        {
            return a1;
        }

        public float getA2()
        {
            return a2;
        }

        public float getB0()
        {
            return b0;
        }

        public float getB1()
        {
            return b1;
        }

        public float getB2()
        {
            return b2;
        }








        // We are not using arrays (e.g., float b[3]) in hopes that using non-arrayed variables improves speed.
        protected float a1;   // Filter coefficients.  'a0' is assumed to equal 1.
        protected float a2;
        protected float b0;
        protected float b1;
        protected float b2;
        protected bool isDcGainZero;  // Set true for highpass and bandpass filters; false for lowpass and notch filters


        private float prevIn;       // input at time t-1
        private float prevPrevIn;   // input at time t-2
        private float prevOut;      // output at time t-1
        private float prevPrevOut;  // output at time t-2
        private bool firstTime;

        public override string ToString()
        {
            return $"b0:{b0}, b1:{b1}, b2:{b2}, a2:{a2}, a1:{a1},  [{b0}, {b1}, {b2}, {a2}, {a1}]";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override float[] getparams()
        {
            return new float[] { b0, b1, b2, a2, a1 };
        }
    }
}
