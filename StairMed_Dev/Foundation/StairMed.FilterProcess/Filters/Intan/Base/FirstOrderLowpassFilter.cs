﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.FilterProcess.Filters.Intan.Base
{
    internal class FirstOrderLowpassFilter : BiquadFilter
    {
        public FirstOrderLowpassFilter(double fc, double sampleRate)
        {
            isDcGainZero = false;
            var k = Math.Exp(-Math.PI * 2 * fc / sampleRate);
            b0 = (float)(1.0F - k);
            b1 = 0.0F;
            b2 = 0.0F;
            a1 = (float)-k;
            a2 = 0.0F;
        }

        public FirstOrderLowpassFilter()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new FirstOrderLowpassFilter
            {
                a1 = a1,
                a2 = a2,
                b0 = b0,
                b1 = b1,
                b2 = b2,
                isDcGainZero = isDcGainZero,
            };
        }
    }
}
