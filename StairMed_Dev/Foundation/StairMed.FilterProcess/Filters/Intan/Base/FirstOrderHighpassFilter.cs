﻿using System;

namespace StairMed.FilterProcess.Filters.Intan.Base
{
    internal class FirstOrderHighpassFilter : BiquadFilter
    {
        public FirstOrderHighpassFilter(double fc, double sampleRate)
        {
            isDcGainZero = true;
            var k = Math.Exp(-Math.PI * 2 * fc / sampleRate);
            b0 = 1.0F;
            b1 = -1.0F;
            b2 = 0.0F;
            a1 = (float)-k;
            a2 = 0.0F;
        }

        public FirstOrderHighpassFilter()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new FirstOrderHighpassFilter
            {
                a1 = a1,
                a2 = a2,
                b0 = b0,
                b1 = b1,
                b2 = b2,
                isDcGainZero = isDcGainZero,
            };
        }
    }
}
