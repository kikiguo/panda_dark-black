﻿using StairMed.FilterProcess.Filters.Intan.Base;
using System.Collections.Generic;

namespace StairMed.FilterProcess.Filters.Intan
{
    /// <summary>
    /// 
    /// </summary>
    public class IntanButterworthHighpassFilter : HighOrderFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="fc"></param>
        /// <param name="sampleRate"></param>
        public IntanButterworthHighpassFilter(int order, double fc, double sampleRate)
        {
            switch (order)
            {
                case 1:
                    filters = new List<BiquadFilter>
                    {
                        new FirstOrderHighpassFilter(fc, sampleRate)
                    };
                    break;
                case 2:
                    filters = new List<BiquadFilter>
                    {
                        new SecondOrderHighpassFilter(fc, 0.7071, sampleRate)
                    };
                    break;
                case 3:
                    filters = new List<BiquadFilter>
                    {
                        new FirstOrderHighpassFilter(fc, sampleRate),
                        new SecondOrderHighpassFilter(fc, 1.0000, sampleRate)
                    };
                    break;
                case 4:
                    filters = new List<BiquadFilter>
                    {
                        new SecondOrderHighpassFilter(fc, 1.3065, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.5412, sampleRate)
                    };
                    break;
                case 5:
                    filters = new List<BiquadFilter>
                    {
                        new FirstOrderHighpassFilter(fc, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.6180, sampleRate),
                        new SecondOrderHighpassFilter(fc, 1.6181, sampleRate)
                    };
                    break;
                case 6:
                    filters = new List<BiquadFilter>
                    {
                        new SecondOrderHighpassFilter(fc, 0.5177, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.7071, sampleRate),
                        new SecondOrderHighpassFilter(fc, 1.9320, sampleRate)
                    };
                    break;
                case 7:
                    filters = new List<BiquadFilter>
                    {
                        new FirstOrderHighpassFilter(fc, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.5549, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.8019, sampleRate),
                        new SecondOrderHighpassFilter(fc, 2.2472, sampleRate)
                    };
                    break;
                case 8:
                    filters = new List<BiquadFilter>
                    {
                        new SecondOrderHighpassFilter(fc, 0.5098, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.6013, sampleRate),
                        new SecondOrderHighpassFilter(fc, 0.8999, sampleRate),
                        new SecondOrderHighpassFilter(fc, 2.5628, sampleRate)
                    };
                    break;
                default:
                    break;
            }
        }




    };
}
