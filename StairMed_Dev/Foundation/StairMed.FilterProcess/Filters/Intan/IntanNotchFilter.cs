﻿using StairMed.FilterProcess.Filters.Intan.Base;
using System.Collections.Generic;

namespace StairMed.FilterProcess.Filters.Intan
{
    /// <summary>
    /// 
    /// </summary>
    internal class IntanNotchFilter : HighOrderFilter
    {
        public IntanNotchFilter(double fNotch, double bandwidth, double sampleRate)
        {
            filters = new List<BiquadFilter>
            {
                new SecondOrderNotchFilter(fNotch, bandwidth, sampleRate)
            };
        }
    }
}
