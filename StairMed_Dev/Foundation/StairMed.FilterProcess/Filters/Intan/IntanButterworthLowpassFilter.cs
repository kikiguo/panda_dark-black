﻿using StairMed.FilterProcess.Filters.Intan.Base;
using System.Collections.Generic;

namespace StairMed.FilterProcess.Filters.Intan
{
    public class IntanButterworthLowpassFilter : HighOrderFilter
    {
        public IntanButterworthLowpassFilter(int order, double fc, double sampleRate)
        {
            switch (order)
            {
                case 1:
                    {
                        filters = new List<BiquadFilter>
                        {
                            new FirstOrderLowpassFilter(fc, sampleRate)
                        };
                    }
                    break;
                case 2:
                    filters = new List<BiquadFilter> {
   new  SecondOrderLowpassFilter(fc, 0.7071, sampleRate)
        };
                    break;
                case 3:
                    filters = new List<BiquadFilter> {
 new    FirstOrderLowpassFilter(fc, sampleRate),
   new          SecondOrderLowpassFilter(fc, 1.0000, sampleRate)
        };
                    break;
                case 4:
                    filters = new List<BiquadFilter>{
   new  SecondOrderLowpassFilter(fc, 1.3065, sampleRate),
    new         SecondOrderLowpassFilter(fc, 0.5412, sampleRate)
        };
                    break;
                case 5:
                    filters = new List<BiquadFilter>{
   new  FirstOrderLowpassFilter(fc, sampleRate),
   new          SecondOrderLowpassFilter(fc, 0.6180, sampleRate),
    new         SecondOrderLowpassFilter(fc, 1.6181, sampleRate)
        };
                    break;
                case 6:
                    filters = new List<BiquadFilter>{
   new  SecondOrderLowpassFilter(fc, 0.5177, sampleRate),
    new         SecondOrderLowpassFilter(fc, 0.7071, sampleRate),
     new        SecondOrderLowpassFilter(fc, 1.9320, sampleRate)
        };
                    break;
                case 7:
                    filters = new List<BiquadFilter>{
   new  FirstOrderLowpassFilter(fc, sampleRate),
   new          SecondOrderLowpassFilter(fc, 0.5549, sampleRate),
   new          SecondOrderLowpassFilter(fc, 0.8019, sampleRate),
   new          SecondOrderLowpassFilter(fc, 2.2472, sampleRate)
        };
                    break;
                case 8:
                    filters = new List<BiquadFilter>{
  new  SecondOrderLowpassFilter(fc, 0.5098, sampleRate),
   new          SecondOrderLowpassFilter(fc, 0.6013, sampleRate),
     new        SecondOrderLowpassFilter(fc, 0.8999, sampleRate),
    new         SecondOrderLowpassFilter(fc, 2.5628, sampleRate)
        };
                    break;
                default:
                    break;
            }
        }





    };
}
