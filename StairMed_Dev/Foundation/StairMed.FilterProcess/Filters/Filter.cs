﻿using System;

namespace StairMed.FilterProcess.Filters
{
    /// <summary>
    /// // Base class for all filters.
    // We use float instead of float for input/output values to reduce storage space in the theory that
    // moving data to and from memory is likely the dominant speed bottleneck.
    /// </summary>
    public abstract class Filter : ICloneable
    {
        /// <summary>
        /// Filter an array of data.
        /// </summary>
        /// <param name="inVals"></param>
        /// <param name="outVals"></param>
        /// <param name="length"></param>
        public void filter(float[] inVals, float[] outVals, uint length)
        {
            for (int i = 0; i < length; i++)
            {
                outVals[i] = filterOne(inVals[i]);
            }
        }

        /// <summary>
        /// // Filter an array of data and return difference as well.  If the filter is a lowpass filter with
        // cutoff frequency fc, then inMinusOut will act as a highpass filter with cutoff frequency fc.
        /// </summary>
        /// <param name="inVals"></param>
        /// <param name="outVals"></param>
        /// <param name="inMinusOutVals"></param>
        /// <param name="length"></param>
        void filter(float[] inVals, float[] outVals, float[] inMinusOutVals, uint length)
        {
            for (int i = 0; i < length; i++)
            {
                outVals[i] = filterOne(inVals[i]);
                inMinusOutVals[i] = inVals[i] - outVals[i];
            }
        }

        /// <summary>
        /// Filter an array of data and overwrite input.
        /// </summary>
        /// <param name="inout"></param>
        /// <param name="length"></param>
        public void filter(float[] inout, uint length)
        {
            for (int i = 0; i < length; i++)
            {
                inout[i] = filterOne(inout[i]);
            }
        }

        /// <summary>
        /// filter one data point
        /// </summary>
        /// <param name="inVal"></param>
        /// <returns></returns>
        public abstract float filterOne(float inVal);

        /// <summary>
        /// reset filter state
        /// </summary>
        public abstract void reset();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract float[] getparams();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual double[] getparamsDouble()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract object Clone();
    }
}
