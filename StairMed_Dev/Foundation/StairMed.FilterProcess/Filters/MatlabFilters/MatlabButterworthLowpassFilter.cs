﻿using StairMed.FilterProcess.Filters.MatlabFilters.Base;

namespace StairMed.FilterProcess.Filters.MatlabFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabButterworthLowpassFilter : MatlabFilterBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        public MatlabButterworthLowpassFilter(int order, int cutoff, int sampleRate) :
            base(FilterType.Butterworth, order, cutoff, sampleRate, false)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new MatlabButterworthLowpassFilter(this.Order, this.Cutoff, this.SampleRate);
        }
    }
}
