﻿using StairMed.FilterProcess.Filters.MatlabFilters.Base;

namespace StairMed.FilterProcess.Filters.MatlabFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabBesselLowpassFilter : MatlabFilterBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        public MatlabBesselLowpassFilter(int order, int cutoff, int sampleRate) :
            base(FilterType.Bessel, order, cutoff, sampleRate, false)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new MatlabBesselLowpassFilter(this.Order, this.Cutoff, this.SampleRate);
        }
    }
}
