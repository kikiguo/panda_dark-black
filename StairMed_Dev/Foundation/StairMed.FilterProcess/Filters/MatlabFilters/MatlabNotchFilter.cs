﻿using StairMed.FilterProcess.Filters.MatlabFilters.Base;

namespace StairMed.FilterProcess.Filters.MatlabFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabNotchFilter : MatlabFilterBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        public MatlabNotchFilter(int order, int notch, int notchBandwidth, int sampleRate) :
            base(order, notch, notchBandwidth, sampleRate)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new MatlabNotchFilter(this.Order, this.Notch, this.NotchBandwidth, this.SampleRate);
        }
    }
}
