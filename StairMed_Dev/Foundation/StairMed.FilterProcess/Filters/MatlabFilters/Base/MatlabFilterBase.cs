﻿using System;
using System.Collections.Generic;

namespace StairMed.FilterProcess.Filters.MatlabFilters.Base
{
    /// <summary>
    /// %   a(1)*y(n) = b(1)*x(n) + b(2)*x(n-1) + ... + b(nb+1)*x(n-nb) 
    ///                - a(2)* y(n-1) - ... - a(na+1)* y(n-na)
    /// </summary>
    internal abstract class MatlabFilterBase : Filter
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly MatlabFilterParamHelper _paramHelper = new MatlabFilterParamHelper();

        /// <summary>
        /// 
        /// </summary>
        private readonly double[] Bn = null;

        /// <summary>
        /// 
        /// </summary>
        private readonly double[] An = null;

        /// <summary>
        /// 
        /// </summary>
        private readonly double[] Xn = null;

        /// <summary>
        /// 
        /// </summary>
        private readonly double[] Yn = null;

        /// <summary>
        /// 
        /// </summary>
        private readonly int N = 0;

        /// <summary>
        /// 
        /// </summary>
        private int Offset = 0;
        /// <summary>
        /// 
        /// </summary>
        protected int Order;

        /// <summary>
        /// 
        /// </summary>
        protected int Cutoff;

        /// <summary>
        /// 
        /// </summary>
        protected int SampleRate;

        /// <summary>
        /// 
        /// </summary>
        protected int Notch;

        /// <summary>
        /// 
        /// </summary>
        protected int NotchBandwidth;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterType"></param>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        /// <param name="high"></param>
        public MatlabFilterBase(FilterType filterType, int order, int cutoff, int sampleRate, bool high)
        {
            Order = order;
            Cutoff = cutoff;
            SampleRate = sampleRate;

            //
            _paramHelper.GetMatlabHighLowFilterParams(filterType, order, cutoff, sampleRate, high, out List<double> Bs, out List<double> As);

            //
            Bn = Bs.ToArray();
            An = As.ToArray();

            //
            N = Bn.Length - 1;

            //
            Xn = new double[N];
            Yn = new double[N];

            //
            System.Array.Clear(Xn, 0, N);
            System.Array.Clear(Yn, 0, N);

            //
            Offset = 0;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="notch"></param>
        /// <param name="notchBandwidth"></param>
        /// <param name="sampleRate"></param>
        public MatlabFilterBase(int order, int notch, int notchBandwidth, int sampleRate)
        {
            Order = order;
            SampleRate = sampleRate;
            Notch = notch;
            NotchBandwidth = notchBandwidth;

            //
            _paramHelper.GetMatlabNotchFilterParam(notch, sampleRate, out List<double> Bs, out List<double> As);

            //
            Bn = Bs.ToArray();
            An = As.ToArray();

            //
            N = Bn.Length - 1;

            //
            Xn = new double[N];
            Yn = new double[N];

            //
            System.Array.Clear(Xn, 0, N);
            System.Array.Clear(Yn, 0, N);

            //
            Offset = 0;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="inVal"></param>
        /// <returns></returns>
        public override float filterOne(float inVal)
        {
            double yn = inVal * Bn[0];

            //
            Offset = Offset + N;

            //
            for (int i = 0; i < N; i++)
            {
                var index = (Offset - i) % N;

                //
                var n = i + 1;
                yn += Bn[n] * Xn[index];
                yn -= An[n] * Yn[index];
            }

            //
            Offset = (Offset + 1) % N;
            Xn[Offset] = inVal;
            Yn[Offset] = yn;

            //
            return (float)yn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override float[] getparams()
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override double[] getparamsDouble()
        {
            var doubles = new double[Bn.Length * 2];
            System.Array.Copy(Bn, 0, doubles, 0, Bn.Length);
            System.Array.Copy(An, 0, doubles, Bn.Length, An.Length);
            return doubles;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void reset()
        {
            System.Array.Clear(Xn, 0, Xn.Length);
            System.Array.Clear(Yn, 0, Yn.Length);
            Offset = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"bn: {string.Join(", ", Bn)}{Environment.NewLine}an: {string.Join(", ", An)}";
        }
    }
}
