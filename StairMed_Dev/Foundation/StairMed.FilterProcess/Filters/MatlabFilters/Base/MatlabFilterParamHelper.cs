﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace StairMed.FilterProcess.Filters.MatlabFilters.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class MatlabFilterParamHelper
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(MatlabFilterParamHelper);

        /// <summary>
        /// 
        /// </summary>
        public const string WaterMark = "StairMed Filter Params";
        public const int MagicNum = 20210815;
        public const string FilterParamFolder = "FilterParams";
        public const string FilterParamFilePrefix = "FilterParam";
        public const string NotchFilterParamFileName = "NotchFilterParam";
        public const int NotchSampleRateBegin = 200; //notch滤波参数支持采样率从200 - 30k
        public const int NotchSampleRateEnd = 30000; //notch滤波参数支持采样率从200 - 30k
        public const int NotchBandwidth = 10; //notch滤波固定带阻宽度为10hz
        public const int NotchOrder = 1; //notch滤波固定为一阶 butterworth 带阻滤波


        /// <summary>
        /// 
        /// </summary>
        public static readonly List<FilterType> FilterTypes = new List<FilterType> { FilterType.Bessel, FilterType.Butterworth };
        public static readonly List<bool> HighLows = new List<bool> { true, false };
        public static readonly List<int> Notchs = new List<int> { 50, 60 }; //notch滤波固定支持50和60两个滤波


        /// <summary>
        /// 
        /// </summary>
        private const int ByteOfDouble = 8; //单个double对应字节数
        private const int HighFilterParamsOf8Order = 4 + 6 + 8 + 10 + 12 + 14 + 16 + 18; // =88，1阶至8阶，仅针对高通或低通
        private static readonly int ByteOf1Cutoff = HighFilterParamsOf8Order * 2 * ByteOfDouble;

        /// <summary>
        /// 
        /// </summary>
        private static readonly int HighLowHeaderByte = WaterMark.Length + 4 + 4; // WaterMark + MagicNum + SampleRate
        private static readonly int NotchHeaderByte = WaterMark.Length + 4; // WaterMark + MagicNum

        /// <summary>
        /// 
        /// </summary>
        private static readonly Dictionary<int, FileStream> _paramFiles = new Dictionary<int, FileStream>(); //高/低通滤波器 在 各种采样率下 的参数
        private static readonly List<int> _sampleRates = new List<int>(); //高/低通滤波器 支持的 各种采样率
        private static FileStream _notchStream = null; //notch滤波器参数存储文件


        #region 初始化

        /// <summary>
        /// 
        /// </summary>
        static MatlabFilterParamHelper()
        {
            var dir = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), FilterParamFolder);
            if (!Directory.Exists(dir))
            {
                return;
            }

            //
            var dirInfo = new DirectoryInfo(dir);

            //
            var filterFiles = dirInfo.GetFiles().Where(r => r.Name.StartsWith(FilterParamFilePrefix)).ToList();
            InitHighLowFilter(filterFiles);

            //
            var notchFiles = dirInfo.GetFiles().Where(r => r.Name.StartsWith(NotchFilterParamFileName)).ToList();
            InitNotchFilter(notchFiles);
        }

        /// <summary>
        /// 初始化带阻滤波器文件流
        /// </summary>
        /// <param name="files"></param>
        private static void InitNotchFilter(List<FileInfo> files)
        {
            foreach (var file in files)
            {
                //判断文件长度
                if (file.Length <= HighLowHeaderByte)
                {
                    continue;
                }

                //                
                var fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read);
                var reader = new BinaryReader(fs, Encoding.ASCII);

                //
                var isFileCorrect = false;

                //
                do
                {
                    try
                    {
                        //判断水印是否一致
                        var waterMark = new string(reader.ReadChars(WaterMark.Length));
                        if (!WaterMark.Equals(waterMark))
                        {
                            break;
                        }

                        //判断魔幻数是否一致
                        var magicNum = reader.ReadInt32();
                        if (MagicNum != magicNum)
                        {
                            break;
                        }

                        //判断文件长度是否靠谱
                        if ((NotchSampleRateEnd - NotchSampleRateBegin + 1) * Notchs.Count * (NotchOrder * 2 + 1) * 2 * ByteOfDouble + NotchHeaderByte != file.Length)
                        {
                            break;
                        }

                        //
                        isFileCorrect = true;
                    }
                    catch { }
                }
                while (false);

                //
                if (!isFileCorrect)
                {
                    fs.Close();
                    continue;
                }

                //
                _notchStream = fs;
                return;
            }
        }

        /// <summary>
        /// 初始化高/低通滤波器文件流
        /// </summary>
        /// <param name="files"></param>
        private static void InitHighLowFilter(List<FileInfo> files)
        {
            foreach (var file in files)
            {
                //判断文件长度
                if (file.Length <= HighLowHeaderByte)
                {
                    continue;
                }

                //                
                var fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read);
                var reader = new BinaryReader(fs, Encoding.ASCII);

                //
                var isFileCorrect = false;
                int sampleRate = 0;

                //
                do
                {
                    try
                    {
                        //判断水印是否一致
                        var waterMark = new string(reader.ReadChars(WaterMark.Length));
                        if (!WaterMark.Equals(waterMark))
                        {
                            break;
                        }

                        //判断魔幻数是否一致
                        var magicNum = reader.ReadInt32();
                        if (MagicNum != magicNum)
                        {
                            break;
                        }

                        //判断文件长度是否靠谱
                        sampleRate = reader.ReadInt32();
                        var cutoffMax = GetMaxCutoff(sampleRate);
                        if (cutoffMax * ByteOf1Cutoff * FilterTypes.Count + HighLowHeaderByte != file.Length)
                        {
                            break;
                        }

                        //
                        isFileCorrect = true;
                    }
                    catch { }
                }
                while (false);

                //
                if (!isFileCorrect)
                {
                    fs.Close();
                    continue;
                }

                //
                _paramFiles[sampleRate] = fs;
                _sampleRates.Add(sampleRate);
            }

            //
            _sampleRates.Sort();
            _sampleRates.Reverse();
        }

        #endregion


        /// <summary>
        /// 获取高/低通滤波器参数
        /// </summary>
        /// <param name="searchFilterType"></param>
        /// <param name="searchOrder"></param>
        /// <param name="searchCutOff"></param>
        /// <param name="searchSampleRate"></param>
        /// <param name="searchIsHigh"></param>
        /// <param name="Bn"></param>
        /// <param name="An"></param>
        public void GetMatlabHighLowFilterParams(FilterType searchFilterType, int searchOrder, int searchCutOff, int searchSampleRate, bool searchIsHigh, out List<double> Bn, out List<double> An)
        {
            Bn = new List<double> { 1, 0 };
            An = new List<double> { 1, 0 };

            //
            try
            {
                //无滤波
                if (_sampleRates.Count == 0)
                {
                    return;
                }

                //
                if (!_sampleRates.Contains(searchSampleRate))
                {
                    //不存在这个采样率，则按系数倍增（采样率和截止频率）
                    foreach (var sampleRate in _sampleRates)
                    {
                        if (sampleRate % searchSampleRate == 0)
                        {
                            searchCutOff = sampleRate / searchSampleRate * searchCutOff;
                            searchSampleRate = sampleRate;
                            break;
                        }
                    }

                    //如果倍增系数不可巧，则对截止频率进行四舍五入处理
                    if (!_sampleRates.Contains(searchSampleRate))
                    {
                        var sampleRate = _sampleRates.Max();
                        searchCutOff = Math.Max(Math.Min(searchCutOff * sampleRate / searchSampleRate, sampleRate / 2 - 1), 1);
                        searchSampleRate = sampleRate;
                    }
                }

                // 2*cutoff/searchSampleRate 应在 (0,1) 之间
                var cutoffMax = GetMaxCutoff(searchSampleRate);
                searchCutOff = Math.Max(Math.Min(searchCutOff, cutoffMax), 1);

                //
                var offset = HighLowHeaderByte;

                //
                foreach (var filterType in FilterTypes)
                {
                    //滤波器类型不相等则执行偏移
                    if (searchFilterType != filterType)
                    {
                        offset += cutoffMax * ByteOf1Cutoff;
                        continue;
                    }

                    //
                    for (int cutoff = 1; cutoff <= cutoffMax; cutoff++)
                    {
                        //截止频率不相等则执行偏移
                        if (searchCutOff != cutoff)
                        {
                            offset += ByteOf1Cutoff;
                            continue;
                        }

                        //
                        for (int order = 1; order <= 8; order++)
                        {
                            //阶数不同则执行偏移
                            if (searchOrder != order)
                            {
                                offset += (order + 1) * 2 * HighLows.Count * ByteOfDouble; //2:bn+an
                                continue;
                            }

                            //
                            foreach (var isHigh in HighLows)
                            {
                                if (searchIsHigh != isHigh)
                                {
                                    offset += (order + 1) * 2 * ByteOfDouble;
                                    continue;
                                }

                                //
                                _paramFiles[searchSampleRate].Seek(offset, SeekOrigin.Begin);

                                //
                                var br = new BinaryReader(_paramFiles[searchSampleRate]);
                                var n = order + 1;
                                Bn.Clear();
                                An.Clear();
                                for (int i = 0; i < n; i++)
                                {
                                    Bn.Add(br.ReadDouble());
                                    An.Add(br.ReadDouble());
                                }

                                //
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(GetMatlabHighLowFilterParams)}", ex);
            }
        }

        /// <summary>
        /// 获取高/低通滤波器参数
        /// </summary>
        /// <param name="searchOrder"></param>
        /// <param name="searchNotch"></param>
        /// <param name="searchNotchBandwidth"></param>
        /// <param name="searchSampleRate"></param>
        /// <param name="Bn"></param>
        /// <param name="An"></param>
        public void GetMatlabNotchFilterParam(int searchNotch, int searchSampleRate, out List<double> Bn, out List<double> An)
        {
            Bn = new List<double> { 1, 0 };
            An = new List<double> { 1, 0 };

            try
            {
                //未找到文件
                if (_notchStream == null)
                {
                    return;
                }

                //不是50 / 60 Hz
                if (!Notchs.Contains(searchNotch))
                {
                    return;
                }

                //偏移
                var offset = NotchHeaderByte;

                //
                for (var sampleRate = NotchSampleRateBegin; sampleRate <= NotchSampleRateEnd; sampleRate++)
                {
                    //采样率不相等则执行偏移
                    if (searchSampleRate != sampleRate)
                    {
                        offset += Notchs.Count * (NotchOrder * 2 + 1) * 2 * ByteOfDouble;
                        continue;
                    }

                    //
                    foreach (var notch in Notchs)
                    {
                        //陷波频率不同，则执行偏移
                        if (searchNotch != notch)
                        {
                            offset += (NotchOrder * 2 + 1) * 2 * ByteOfDouble;
                            continue;
                        }

                        //
                        _notchStream.Seek(offset, SeekOrigin.Begin);

                        //
                        var br = new BinaryReader(_notchStream);
                        var n = NotchOrder * 2 + 1;
                        Bn.Clear();
                        An.Clear();
                        for (int i = 0; i < n; i++)
                        {
                            Bn.Add(br.ReadDouble());
                            An.Add(br.ReadDouble());
                        }

                        //
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(GetMatlabNotchFilterParam)}", ex);
            }
        }

        /// <summary>
        /// 获得可拥有的最大截止频率
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <returns></returns>
        public static int GetMaxCutoff(int sampleRate)
        {
            return sampleRate / 2 - 1;
        }
    }
}
