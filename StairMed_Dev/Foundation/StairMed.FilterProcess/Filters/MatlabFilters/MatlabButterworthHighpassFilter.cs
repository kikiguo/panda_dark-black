﻿using StairMed.FilterProcess.Filters.MatlabFilters.Base;

namespace StairMed.FilterProcess.Filters.MatlabFilters
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabButterworthHighpassFilter : MatlabFilterBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        public MatlabButterworthHighpassFilter(int order, int cutoff, int sampleRate) :
            base(FilterType.Butterworth, order, cutoff, sampleRate, true)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return new MatlabButterworthHighpassFilter(this.Order, this.Cutoff, this.SampleRate);
        }
    }
}
