﻿using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.ParamMonitor;
using StairMed.FilterProcess.ParamMonitor.Filters.HighLowFilters;
using StairMed.FilterProcess.ParamMonitor.Filters.NotchFilters;
using System;
using System.Collections.Generic;

namespace StairMed.FilterProcess
{
    /// <summary>
    /// 
    /// </summary>
    public class ParamCenter
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly RawKBMonitor _rawKBMonitor = null;
        private readonly SpikeThresholdParamMonitor _spikeParamMonitor = null;

        /// <summary>
        /// 
        /// </summary>
        private readonly FilterMonitorBase _highPassFilterMonitor = null;
        private readonly FilterMonitorBase _lowPassFilterMonitor = null;
        private readonly NotchFilterMonitorBase _notchFilterMonitor = null;
        private readonly ExternalTrigMonitor _externalTrigMonitor = null;

      


        /// <summary>
        /// 
        /// </summary>
        /// <param name="useMatlabFilter"></param>
        /// <param name="onHighpassFilterChanged"></param>
        /// <param name="onLowpassFilterChanged"></param>
        /// <param name="onNotchFilterChanged"></param>
        /// <param name="onRawKBChanged"></param>
        /// <param name="onSpikeChanged"></param>
        /// <param name="onSpikeChangedArray"></param>
        internal ParamCenter(bool useMatlabFilter, 
            Action<int, int, Filter> onHighpassFilterChanged, 
            Action<int, int, Filter> onLowpassFilterChanged,
            Action<int, int, Filter> onNotchFilterChanged,
            Action<int, int, float, int> onRawKBChanged,
            Action<int, int, int, float> onSpikeChanged,
            Action<int, int, int, float[]> onSpikeChangedArray, 
            Action<int, int, int, int> onTRIGChanged
           )
        {
            if (useMatlabFilter)
            {
                _highPassFilterMonitor = new MatlabHighpassFilterMonitor(onHighpassFilterChanged);
                _lowPassFilterMonitor = new MatlabLowpassFilterMonitor(onLowpassFilterChanged);
                _notchFilterMonitor = new MatlabNotchFilterMonitor(onNotchFilterChanged);
            }
            else
            {
                _highPassFilterMonitor = new IntanHighpassFilterMonitor(onHighpassFilterChanged);
                _lowPassFilterMonitor = new IntanLowpassFilterMonitor(onLowpassFilterChanged);
                _notchFilterMonitor = new IntanNotchFilterMonitor(onNotchFilterChanged);
            }

            //
            _rawKBMonitor = new RawKBMonitor(onRawKBChanged);
            _spikeParamMonitor = new SpikeThresholdParamMonitor(onSpikeChanged, onSpikeChangedArray);
            _externalTrigMonitor = new ExternalTrigMonitor(onTRIGChanged);
        }




        #region

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="sampleRate"></param>
        public void SetCommonParam(int channelCount, int sampleCount, int sampleRate)
        {
            _highPassFilterMonitor.UpdateBaseParam(channelCount, sampleCount, sampleRate);
            _lowPassFilterMonitor.UpdateBaseParam(channelCount, sampleCount, sampleRate);
            _notchFilterMonitor.UpdateBaseParam(channelCount, sampleCount, sampleRate);
            _rawKBMonitor.UpdateBaseParam(channelCount, sampleCount, sampleRate);
            _spikeParamMonitor.UpdateBaseParam(channelCount, sampleCount, sampleRate);
            _externalTrigMonitor.UpdateBaseParam(channelCount, sampleCount, sampleRate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="filterType"></param>
        public void SetHighPassFilter(int cutoff, int order, FilterType filterType)
        {
            _highPassFilterMonitor.Update(cutoff, order, filterType);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="filterType"></param>
        public void SetLowPassFilter(int cutoff, int order, FilterType filterType)
        {
            _lowPassFilterMonitor.Update(cutoff, order, filterType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        public void SetNotchFilter(int notchHz, int notchBandwidth = 10)
        {
            _notchFilterMonitor.Update(notchHz, notchBandwidth);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawK"></param>
        /// <param name="rawB"></param>
        public void SetRawKB(float rawK, int rawB)
        {
            _rawKBMonitor.Update(rawK, rawB);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="thresholds"></param>
        public void SetSpikeParam(List<float> thresholds)
        {
            _spikeParamMonitor.Update(thresholds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="threshold"></param>
        public void SetSpikeParam(float threshold)
        {
            _spikeParamMonitor.Update(threshold);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trig2,trig3"></param>
        public void SetTRIGParam(int _triggerLevel, int _triggerDelay)
        {
            _externalTrigMonitor.Update(_triggerLevel, _triggerDelay);
        }
 
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="force"></param>
        internal void ApplyWhenChanged(bool force = false)
        {
            _rawKBMonitor.ApplyWhenChanged(force);
            _spikeParamMonitor.ApplyWhenChanged(force);

            //
            _highPassFilterMonitor.ApplyWhenChanged(force);
            _lowPassFilterMonitor.ApplyWhenChanged(force);
            _notchFilterMonitor.ApplyWhenChanged(force);
            _externalTrigMonitor.ApplyWhenChanged(force);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        internal void GetChannelSample(out int channelCount, out int sampleCount)
        {
            channelCount = _rawKBMonitor.ChannelCount;
            sampleCount = _rawKBMonitor.SampleCount;
        }
    }

   
}
