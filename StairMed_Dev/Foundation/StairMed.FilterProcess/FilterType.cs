﻿namespace StairMed.FilterProcess
{
    /// <summary>
    /// 
    /// </summary>
    public enum FilterType
    {
        Bessel,
        Butterworth,
    }

    public enum EnumFilterType
    {
        Lowpass,
        Highpass,
        Bandpass,
        Notchfilter
    }

    public enum EnumSignalSource
    {
        AbalogSignal,
        DigitalSignal,
    }

    public enum EnumCutoffFrequency
    {
        Lowpass,
        Highpass,
        BandpassNotchfilter,
    }
}
