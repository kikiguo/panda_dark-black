﻿using System.Runtime.InteropServices;

namespace StairMed.FilterProcess.XPU.GPU.CUDA
{
    internal static class CUDALib
    {
        #region CUDA

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern int Init(int deviceIndex);

        //[DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        //public static extern void Reset();

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitRawParam(int channels, int oneChannelSamples, float k, int b);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitSpikeParams(int channels, int oneChannelSamples, int gap, float[] thresholds);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitSpikeParam(int channels, int oneChannelSamples, int gap, float threshold);

        #region Intan算法

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitIntanNotchFilterParam(int channels, int oneChannelSamples, int filterCount, float[] filterParam);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitIntanLfpFilterParam(int channels, int oneChannelSamples, int filterCount, float[] filterParam);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitIntanHfpFilterParam(int channels, int oneChannelSamples, int filterCount, float[] filterParam);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void ProcessADChunkByIntan(int headbytes, byte[] adbytes, int channels, int oneChannelSamples, float[] raws, float[] wfps, float[] hfps, float[] lfps, float[] spikes, int[] trigs);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void ProcessWFPChunkByIntan(int channels, int oneChannelSamples, float[] wfps, float[] hfps, float[] lfps, float[] spikes, int[] trigs);

        #endregion


        #region Matlab算法

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitMatlabNotchFilterParam(int channels, int oneChannelSamples, int filterParamCount, double[] filterParam);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitMatlabLfpFilterParam(int channels, int oneChannelSamples, int filterParamCount, double[] filterParam);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void InitMatlabHfpFilterParam(int channels, int oneChannelSamples, int filterParamCount, double[] filterParam);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void ProcessADChunkByMatlab(int headbytes, byte[] adbytes, int channels, int oneChannelSamples, float[] raws, float[] wfps, float[] hfps, float[] lfps, float[] spikes, int[] trigs);  
        
        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void ProcessADChunkByMatlab3(int headbytes, byte[] adbytes, int channels, int oneChannelSamples, float[] raws, float[] wfps, float[] hfps, float[] lfps, float[] spikes);   
        
        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void ProcessADChunkByMatlab2(int headbytes, byte[] adbytes, int channels, int oneChannelSamples, float[] wfps, float[] hfps, float[] lfps, float[] spikes);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void ProcessWFPChunkByMatlab(int channels, int oneChannelSamples, float[] wfps, float[] hfps, float[] lfps, float[] spikes, int[] trigs);

        #endregion



        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void FreeBee();

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern void AddTest(int len, int[] a, int[] b, int[] c);

        [DllImport("StairMed.Bee.dll", CharSet = CharSet.Auto)]
        public static extern bool IsSupportCUDA();

        #endregion
    }
}
