﻿using StairMed.Array;
using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Logger;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace StairMed.FilterProcess.XPU.GPU.CUDA
{
    /// <summary>
    /// 
    /// </summary>
    internal class MatlabCUDA : IXPU
    {
        private const string TAG = nameof(MatlabCUDA);

        /// <summary>
        /// 
        /// </summary>
        private bool _isInited = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly ParamCenter _paramCenter = null;

        /// <summary>
        /// 
        /// </summary>
        public ParamCenter ParamCenter => _paramCenter;

        private int _triggerLevel = 0;
        private int _triggerDelay = 0;
        private bool _triggerParamsChanged = false;
        private int[] trigs = new int[2];
        protected virtual int DataHeadOffset => 64;

        /// <summary>
        /// 
        /// </summary>
        public MatlabCUDA()
        {
            _paramCenter = new ParamCenter(true, ApplyHighPassFilterChanges, ApplyLowPassFilterChanges, ApplyNotchFilterChanges, ApplyRawKBChanges, ApplySpikeParamChanges, ApplySpikeParamChanges,ApplyTRIGChanges);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Free()
        {
            CUDALib.FreeBee();
            _isInited = false;
        }



        #region 每次计算前尝试更新

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        private void ApplyRawKBChanges(int channels, int oneChannelSamples, float k, int b)
        {
            CUDALib.InitRawParam(channels, oneChannelSamples, k, b);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="gap"></param>
        /// <param name="thresholds"></param>
        private void ApplySpikeParamChanges(int channels, int oneChannelSamples, int gap, float[] thresholds)
        {
            CUDALib.InitSpikeParams(channels, oneChannelSamples, gap, thresholds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="gap"></param>
        /// <param name="threshold"></param>
        private void ApplySpikeParamChanges(int channels, int oneChannelSamples, int gap, float threshold)
        {
            CUDALib.InitSpikeParam(channels, oneChannelSamples, gap, threshold);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyNotchFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            if (filter != null)
            {
                var filterParam = filter.getparamsDouble();
                CUDALib.InitMatlabNotchFilterParam(channels, oneChannelSamples, filterParam.Length, filterParam);
            }
            else
            {
                CUDALib.InitMatlabNotchFilterParam(channels, oneChannelSamples, 4, new double[4] { 1, 0, 1, 0 });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyHighPassFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            _mCount = 0;
            var filterParam = filter.getparamsDouble();
            CUDALib.InitMatlabHfpFilterParam(channels, oneChannelSamples, filterParam.Length, filterParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyLowPassFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            var filterParam = filter.getparamsDouble();
            CUDALib.InitMatlabLfpFilterParam(channels, oneChannelSamples, filterParam.Length, filterParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        private void ApplyTRIGChanges(int channels, int oneChannelSamples, int triggerLevel, int triggerDelay)
        {
            if (_triggerLevel != triggerLevel || _triggerDelay != triggerDelay)
            {
                // 更新触发参数
                _triggerLevel = triggerLevel;
                _triggerDelay = triggerDelay;

                // 标记触发参数已更改
                _triggerParamsChanged = true;
            }
        }
      

      



        #endregion





        #region 数据包处理

        private readonly Stopwatch _watch = new Stopwatch();
        private int _calcCount = 0;
        private long _costTicks = 0;
        private int _mCount = 0;

        /// <summary>
        /// 
        /// </summary>
        private void InitRunParams()
        {
            //
            if (!_isInited)
            {
                CUDALib.Init(0);
                ParamCenter.ApplyWhenChanged(true);
                _isInited = true;
            }

            //
            ParamCenter.ApplyWhenChanged(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="chunk"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="raws"></param>
        /// <param name="wfps"></param>
        /// <param name="hfps"></param>
        /// <param name="lfps"></param>
        /// <param name="spks"></param>
        /// <returns></returns>
        public bool ProcessByteADChunk(int offset, byte[] chunk, out PooledArray<float> raws, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs)
        {
            //
            InitRunParams();

            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            raws = new PooledArray<float>(len);
            wfps = new PooledArray<float>(len);
            hfps = new PooledArray<float>(len);
            lfps = new PooledArray<float>(len);
            spks = new PooledArray<float>(len);
            trigs = new PooledArray<int>(len);
            //
            try
            {
#if DEBUG
                _watch.Restart();
#endif

                CUDALib.ProcessADChunkByMatlab(offset, chunk, channelCount, sampleCount, raws.Array, wfps.Array, hfps.Array, lfps.Array, spks.Array,trigs.Array);
                //CUDALib.ProcessADChunkByMatlab2(offset, chunk, channelCount, sampleCount, wfps.Array, hfps.Array, lfps.Array, spks.Array);

#if DEBUG
                _watch.Stop();
                _calcCount++;
                _costTicks += _watch.ElapsedTicks;

                //
                if (_calcCount == 100)
                {
                    var avgTicks = _costTicks / _calcCount;
                    var avgMs = 1000 * _costTicks / _calcCount / Stopwatch.Frequency;
                    Debug.WriteLine($"{TAG}, {DateTime.Now}, {nameof(ProcessByteADChunk)}, chunk = [chl]{channelCount} * {sampleCount}, once cost={avgTicks}, [{avgMs}ms]");

                    //
                    _calcCount = 0;
                    _costTicks = 0;
                }
#endif

                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessByteADChunk)}", ex);
            }

            //
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wfps"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="hfps"></param>
        /// <param name="lfps"></param>
        /// <param name="spks"></param>
        /// <returns></returns>
        public bool ProcessWFPChunk(float[] chunk, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs)
        {
            //
            InitRunParams();

            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            wfps = new PooledArray<float>(len);
            hfps = new PooledArray<float>(len);
            lfps = new PooledArray<float>(len);
            spks = new PooledArray<float>(len);
            trigs = new PooledArray<int>(len);
            System.Array.Copy(chunk, 0, wfps.Array, 0, len);

            //
            try
            {
#if false //wrj for test
                WriteData(wfps.Array, "filter_before.bin");
                LogTool.Logger.LogI($"filter_before.bin write index = {_mCount}, data len = {wfps.Array.Length}");
#endif
                CUDALib.ProcessWFPChunkByMatlab(channelCount, sampleCount, wfps.Array, hfps.Array, lfps.Array, spks.Array,trigs.Array);
#if false //wrj for test
                WriteData(hfps.Array, "filter_after.bin");
                _mCount++;
#endif



                byte[] bytes = new byte[chunk.Length];
                Buffer.BlockCopy(chunk,0, bytes, 0, bytes.Length);
                CalculateTRIG(chunk, trigs.Array, channelCount, sampleCount);
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessWFPChunk)}", ex);
            }

            //
            return false;
        }

        private void WriteData(float[] data,string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Append))
            {
                //var bytes = new byte[data.Length * 4];
                //Buffer.BlockCopy(data, 0, bytes,0, bytes.Length);
                //byte[] bytes = data.SelectMany(x => BitConverter.GetBytes(x)).ToArray();
                //fs.Write(bytes, 0, bytes.Length);
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        bw.Write(data[i]);
                    }
                }

                
            }
        }

        public static void CalculateTRIG(float[] chunk, int[] trigs, int channelCount, int sampleCount)
        {
            if (chunk == null || trigs == null || channelCount <= 0 || sampleCount <= 0)
            {
                throw new ArgumentException("Invalid input data");
            }


            var validTrigChannelCount = 9; // 确保这个值不超过chunk.Trigs数组的长度


            for (int trigChannel = 0; trigChannel < validTrigChannelCount; trigChannel++)
            {
                var channelFixedOffset = trigChannel * sampleCount;

                for (int j = 0; j < sampleCount; j++)
                {
                    trigs[channelFixedOffset + j] = (int)chunk[j * 9 + trigChannel];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spksChunk"></param>
        /// <param name="spks"></param>
        /// <returns></returns>
        public bool ProcessSpkChunk(float[] chunk, out PooledArray<float> spks)
        {
            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            spks = new PooledArray<float>(len);

            //
            try
            {
                //转置
                for (int chl = 0; chl < channelCount; chl++)
                {
                    for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                    {
                        spks[chl * sampleCount + sampleIndex] = chunk[sampleIndex * channelCount + chl];
                    }
                }

                //
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessSpkChunk)}", ex);
            }

            //
            return false;
        }

#endregion


    }
}
