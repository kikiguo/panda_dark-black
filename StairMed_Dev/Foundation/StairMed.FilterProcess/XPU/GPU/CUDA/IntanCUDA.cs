﻿using StairMed.Array;
using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Logger;
using System;
using System.Diagnostics;
using System.Threading.Channels;

namespace StairMed.FilterProcess.XPU.GPU.CUDA
{
    /// <summary>
    /// 
    /// </summary>
    internal class IntanCUDA : IXPU
    {
        private const string TAG = nameof(IntanCUDA);

        /// <summary>
        /// 
        /// </summary>
        private bool _isInited = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly ParamCenter _paramCenter = null;

        /// <summary>
        /// 
        /// </summary>
        public ParamCenter ParamCenter => _paramCenter;

        private int _triggerLevel = 0;
        private int _triggerDelay = 0;
        private int _sampleRate = 100000000; // 采样率，假设为100MHz
        private int _oneChannelSamples = 1000; // 假设每帧数据的样本数为1000
        private int expectedFrameCounter = 0; // 期望的帧计数器
        private int _channel = 1024; //通道数
        private bool _triggerParamsChanged = false;
        private int[] trigs = new int[2000]; // 假设方波信号的样本数为2000
        protected virtual int DataHeadOffset => 64;
        /// <summary>
        /// 
        /// </summary>
        public IntanCUDA()
        {
            _paramCenter = new ParamCenter(false, ApplyHighPassFilterChanges, ApplyLowPassFilterChanges, ApplyNotchFilterChanges, ApplyRawKBChanges, ApplySpikeParamChanges, ApplySpikeParamChanges,ApplyTRIGChanges);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Free()
        {
            CUDALib.FreeBee();
            _isInited = false;
        }



        #region 每次计算前尝试更新

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        private void ApplyRawKBChanges(int channels, int oneChannelSamples, float k, int b)
        {
            CUDALib.InitRawParam(channels, oneChannelSamples, k, b);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        private void ApplyTRIGChanges(int channels, int oneChannelSamples, int triggerLevel, int triggerDelay)
        {
            if (_triggerLevel != triggerLevel || _triggerDelay != triggerDelay)
            {
                // 更新触发参数
                _triggerLevel = triggerLevel;
                _triggerDelay = triggerDelay;
                // 标记触发参数已更改
                _triggerParamsChanged = true;
            }
        }

   

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="gap"></param>
        /// <param name="thresholds"></param>
        private void ApplySpikeParamChanges(int channels, int oneChannelSamples, int gap, float[] thresholds)
        {
            CUDALib.InitSpikeParams(channels, oneChannelSamples, gap, thresholds);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="gap"></param>
        /// <param name="threshold"></param>
        private void ApplySpikeParamChanges(int channels, int oneChannelSamples, int gap, float threshold)
        {
            CUDALib.InitSpikeParam(channels, oneChannelSamples, gap, threshold);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyNotchFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            if (filter != null)
            {
                var filterParam = filter.getparams();
                int filterRank = filterParam.Length / 5;
                CUDALib.InitIntanNotchFilterParam(channels, oneChannelSamples, filterRank, filterParam);
            }
            else
            {
                CUDALib.InitIntanNotchFilterParam(channels, oneChannelSamples, 1, new float[5] { 1, 0, 0, 0, 0 });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyHighPassFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            var filterParam = filter.getparams();
            int filterRank = filterParam.Length / 5;
            CUDALib.InitIntanHfpFilterParam(channels, oneChannelSamples, filterRank, filterParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyLowPassFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            var filterParam = filter.getparams();
            int filterRank = filterParam.Length / 5;
            CUDALib.InitIntanLfpFilterParam(channels, oneChannelSamples, filterRank, filterParam);
        }

        #endregion





        #region 数据包处理

        private readonly Stopwatch _watch = new Stopwatch();
        private int _calcCount = 0;
        private long _costTicks = 0;

        /// <summary>
        /// 
        /// </summary>
        private void InitRunParams()
        {
            //
            if (!_isInited)
            {
                CUDALib.Init(0);
                ParamCenter.ApplyWhenChanged(true);
                _isInited = true;
            }

            //
            ParamCenter.ApplyWhenChanged(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="chunk"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="raws"></param>
        /// <param name="wfps"></param>
        /// <param name="hfps"></param>
        /// <param name="lfps"></param>
        /// <param name="spks"></param>
        /// <returns></returns>
        public bool ProcessByteADChunk(int offset, byte[] chunk, out PooledArray<float> raws, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs)
        {
            //
            InitRunParams();

            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            raws = new PooledArray<float>(len);
            wfps = new PooledArray<float>(len);
            hfps = new PooledArray<float>(len);
            lfps = new PooledArray<float>(len);
            spks = new PooledArray<float>(len);
            trigs = new PooledArray<int>(len);
            //
            try
            {
#if DEBUG
                _watch.Restart();
#endif 

                CUDALib.ProcessADChunkByIntan(offset, chunk, channelCount, sampleCount, raws.Array, wfps.Array, hfps.Array, lfps.Array, spks.Array,trigs.Array);

#if DEBUG
                _watch.Stop();
                _calcCount++;
                _costTicks += _watch.ElapsedTicks;

                //
                if (_calcCount == 100)
                {
                    var avgTicks = _costTicks / _calcCount;
                    var avgMs = 1000 * _costTicks / _calcCount / Stopwatch.Frequency;
                    Debug.WriteLine($"{TAG}, {nameof(ProcessByteADChunk)}, chunk = [chl]{channelCount} * {sampleCount}, once cost={avgTicks}, [{avgMs}ms]");

                    //
                    _calcCount = 0;
                    _costTicks = 0;
                }
#endif

                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessByteADChunk)}", ex);
            }

            //
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wfps"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="hfps"></param>
        /// <param name="lfps"></param>
        /// <param name="spks"></param>
        /// <param name="channelSpikeCount"></param>
        /// <param name="channelHfpSquareSum"></param>
        /// <returns></returns>
        public bool ProcessWFPChunk(float[] chunk, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs)
        {
            //
            InitRunParams();

            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            wfps = new PooledArray<float>(len);
            hfps = new PooledArray<float>(len);
            lfps = new PooledArray<float>(len);
            spks = new PooledArray<float>(len);
            trigs = new PooledArray<int>(len);
            System.Array.Copy(chunk, 0, wfps.Array, 0, len);

            //
            try
            {
                CUDALib.ProcessWFPChunkByIntan(channelCount, sampleCount, wfps.Array, hfps.Array, lfps.Array, spks.Array,trigs.Array);
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessWFPChunk)}", ex);
            }

            //
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spksChunk"></param>
        /// <param name="spks"></param>
        /// <param name="channelSpikeCount"></param>
        /// <param name="channelHfpSquareSum"></param>
        /// <returns></returns>
        public bool ProcessSpkChunk(float[] chunk, out PooledArray<float> spks)
        {
            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            spks = new PooledArray<float>(len);

            //
            try
            {
                //转置
                for (int chl = 0; chl < channelCount; chl++)
                {
                    for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                    {
                        spks[chl * sampleCount + sampleIndex] = chunk[sampleIndex * channelCount + chl];
                    }
                }

                //
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessSpkChunk)}", ex);
            }

            //
            return false;
        }

        #endregion

    }
}
