﻿using StairMed.Array;
using StairMed.FilterProcess.Filters;
using StairMed.FilterProcess.Logger;
using System;
using System.Linq;

namespace StairMed.FilterProcess.XPU.CPU
{
    /// <summary>
    /// 
    /// </summary>
    internal class CPU : IXPU
    {
        /// <summary>
        /// 
        /// </summary>
        private const string TAG = nameof(CPU);

        /// <summary>
        /// 
        /// </summary>
        private bool _isInited = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly ParamCenter _paramCenter = null;

        /// <summary>
        /// 
        /// </summary>
        public ParamCenter ParamCenter => _paramCenter;

        #region

        private int _channels = 0;
        private int _oneChannelSamples = 0;
        private float _k = 0;
        private float _b = 0;
        private int _triggerLevel = 0;
        private int _triggerDelay = 0;

        private bool _isFixThreshold = true;
        private int _spikeGap = 0;
        private float _threshold = -70;
        private float[] _thresholds = null;

        private Filter[] _notchFilters = null;
        private Filter[] _hfpFilters = null;
        private Filter[] _lfpFilters = null;

        private int[] _spkIndexs = null;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public CPU(bool useMatlab)
        {
            _paramCenter = new ParamCenter(useMatlab, ApplyHighPassFilterChanges, ApplyLowPassFilterChanges, ApplyNotchFilterChanges, ApplyRawKBChanges, ApplySpikeParamChanges, ApplySpikeParamChanges, ApplyTRIGChanges);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Free()
        {
            _isInited = false;
        }

        #region 每次计算前尝试更新

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        private void ApplyRawKBChanges(int channels, int oneChannelSamples, float k, int b)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            _k = k;
            _b = b;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="k"></param>
        /// <param name="b"></param>
        private void ApplyTRIGChanges(int channels, int oneChannelSamples, int _triggerLevel, int _triggerDelay)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            _triggerLevel = _triggerLevel;
            _triggerDelay = _triggerDelay;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="gap"></param>
        /// <param name="thresholds"></param>
        private void ApplySpikeParamChanges(int channels, int oneChannelSamples, int gap, float[] thresholds)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            _isFixThreshold = false;
            _spikeGap = gap;
            _thresholds = thresholds;

            //
            _spkIndexs = new int[channels];
            for (int i = 0; i < channels; i++)
            {
                _spkIndexs[i] = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="gap"></param>
        /// <param name="threshold"></param>
        private void ApplySpikeParamChanges(int channels, int oneChannelSamples, int gap, float threshold)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            _isFixThreshold = true;
            _spikeGap = gap;
            _threshold = threshold;

            //
            _spkIndexs = new int[channels];
            for (int i = 0; i < channels; i++)
            {
                _spkIndexs[i] = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyNotchFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            if (filter == null)
            {
                _notchFilters = null;
            }
            else
            {
                _notchFilters = new Filter[_channels];
                for (int chl = 0; chl < _channels; chl++)
                {
                    _notchFilters[chl] = (Filter)filter.Clone();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyHighPassFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            _hfpFilters = new Filter[_channels];
            for (int chl = 0; chl < _channels; chl++)
            {
                _hfpFilters[chl] = (Filter)filter.Clone();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="oneChannelSamples"></param>
        /// <param name="filter"></param>
        private void ApplyLowPassFilterChanges(int channels, int oneChannelSamples, Filter filter)
        {
            _channels = channels;
            _oneChannelSamples = oneChannelSamples;

            //
            _lfpFilters = new Filter[_channels];
            for (int chl = 0; chl < _channels; chl++)
            {
                _lfpFilters[chl] = (Filter)filter.Clone();
            }
        }

        #endregion





        #region 数据包处理

        /// <summary>
        /// 
        /// </summary>
        private void InitRunParams()
        {
            //
            if (!_isInited)
            {
                ParamCenter.ApplyWhenChanged(true);
                _isInited = true;
            }

            //
            ParamCenter.ApplyWhenChanged(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="chunk">ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN</param>
        /// <param name="raws">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="wfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="hfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="lfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="spks">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <returns></returns>
        public bool ProcessByteADChunk(int offset, byte[] chunk, out PooledArray<float> raws, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs)
        {
            //
            InitRunParams();

            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            raws = new PooledArray<float>(len);
            wfps = new PooledArray<float>(len);
            hfps = new PooledArray<float>(len);
            lfps = new PooledArray<float>(len);
            spks = new PooledArray<float>(len);
            trigs = new PooledArray<int>(len);

            System.Array.Clear(spks.Array, 0, len);

            //
            try
            {
                //raw
                for (int chl = 0; chl < channelCount; chl++)
                {
                    for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                    {
                        var val = _k * (BitConverter.ToUInt16(chunk, offset + (sampleIndex * channelCount + chl) * 2) + _b);
                        raws[chl * sampleCount + sampleIndex] = val;
                    }
                }

                //wfp
                if (_notchFilters == null)
                {
                    System.Array.Copy(raws.Array, 0, wfps.Array, 0, len);
                }
                else
                {
                    for (int chl = 0; chl < channelCount; chl++)
                    {
                        var filter = _notchFilters[chl];
                        var chlOffset = chl * sampleCount;

                        //
                        for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                        {
                            wfps[chlOffset + sampleIndex] = filter.filterOne(raws[chlOffset + sampleIndex]);
                        }
                    }
                }



                //hfp
                CalculateHFP(wfps.Array, hfps.Array, channelCount, sampleCount);

                //lfp
                CalculateLFP(wfps.Array, lfps.Array, channelCount, sampleCount);

                //spks
                CalculateSPK(hfps.Array, spks.Array, channelCount, sampleCount);

                //trigs
                CalculateTRIG(raws.Array,trigs.Array,channelCount,sampleCount);

                

                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessByteADChunk)}", ex);
            }

            //
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wfps">ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN ch1 ch2 ch3 ... chN</param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="hfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="lfps">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <param name="spks">ch1 ch1 ch1 ... ch1 ch2 ch2 ch2 ... ch2 .......chN chN chN ... chN</param>
        /// <returns></returns>
        public bool ProcessWFPChunk(float[] chunk, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs)
        {
            //
            InitRunParams();

            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            wfps = new PooledArray<float>(len);
            hfps = new PooledArray<float>(len);
            lfps = new PooledArray<float>(len);
            spks = new PooledArray<float>(len);
            trigs = new PooledArray<int>(len);
            System.Array.Clear(spks.Array, 0, len);

            //
            try
            {
                //转置                
                for (int chl = 0; chl < channelCount; chl++)
                {
                    for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                    {
                        wfps[chl * sampleCount + sampleIndex] = chunk[sampleIndex * channelCount + chl];
                    }
                }
              
                //hfp
                CalculateHFP(wfps.Array, hfps.Array, channelCount, sampleCount);

                //lfp
                CalculateLFP(wfps.Array, lfps.Array, channelCount, sampleCount);

                //spks
                CalculateSPK(hfps.Array, spks.Array, channelCount, sampleCount);

                //
                CalculateTRIG(wfps.Array, trigs.Array, channelCount, sampleCount);
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessWFPChunk)}", ex);
            }

            //
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spksChunk"></param>
        /// <param name="spks"></param>
        /// <returns></returns>
        public bool ProcessSpkChunk(float[] chunk, out PooledArray<float> spks)
        {
            //
            ParamCenter.GetChannelSample(out int channelCount, out int sampleCount);
            var len = channelCount * sampleCount;

            //
            spks = new PooledArray<float>(len);

            //
            try
            {
                //转置
                for (int chl = 0; chl < channelCount; chl++)
                {
                    for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                    {
                        spks[chl * sampleCount + sampleIndex] = chunk[sampleIndex * channelCount + chl];
                    }
                }

                //
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(ProcessSpkChunk)}", ex);
            }

            //
            return false;
        }

        #endregion


        #region 
        private void CalculateTRIG(float[] wfps, int[] trigs, int channelCount, int sampleCount)
        {
          
            float threshold = 0.5f;

            for (int chl = 0; chl < channelCount; chl++)
            {
                for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                {
                    float value = wfps[chl * sampleCount + sampleIndex];
                    int trigger = (value > threshold) ? 1 : 0;

                    trigs[chl * sampleCount + sampleIndex] = trigger;
                }
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="hfps"></param>
        /// <param name="spks"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        /// <param name="len"></param>
        private void CalculateSPK(float[] hfps, float[] spks, int channelCount, int sampleCount)
        {
            for (int chl = 0; chl < channelCount; chl++)
            {
                var chlOffset = chl * sampleCount;
                var threshold = _isFixThreshold ? _threshold : _thresholds[chl];

                //
                int prevSpikeIndex = _spkIndexs[chl];
                prevSpikeIndex -= sampleCount;

                //
                if (threshold < 0)
                {
                    for (int sampleIndex = Math.Max(0, prevSpikeIndex + _spikeGap); sampleIndex < sampleCount; sampleIndex++)
                    {
                        if (hfps[chlOffset + sampleIndex] < threshold)
                        {
                            spks[chlOffset + sampleIndex] = 1;
                            prevSpikeIndex = sampleIndex;
                            sampleIndex += _spikeGap;
                        }
                    }
                }
                else
                {
                    for (int sampleIndex = Math.Max(0, prevSpikeIndex + _spikeGap); sampleIndex < sampleCount; sampleIndex++)
                    {
                        if (hfps[chlOffset + sampleIndex] > threshold)
                        {
                            spks[chlOffset + sampleIndex] = 1;
                            prevSpikeIndex = sampleIndex;
                            sampleIndex += _spikeGap;
                        }
                    }
                }

                //
                _spkIndexs[chl] = prevSpikeIndex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wfps"></param>
        /// <param name="lfps"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        private void CalculateLFP(float[] wfps, float[] lfps, int channelCount, int sampleCount)
        {
            for (int chl = 0; chl < channelCount; chl++)
            {
                var filter = _lfpFilters[chl];
                var chlOffset = chl * sampleCount;

                //
                for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                {
                    lfps[chlOffset + sampleIndex] = filter.filterOne(wfps[chlOffset + sampleIndex]);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wfps"></param>
        /// <param name="hfps"></param>
        /// <param name="channelCount"></param>
        /// <param name="sampleCount"></param>
        private void CalculateHFP(float[] wfps, float[] hfps, int channelCount, int sampleCount)
        {
            for (int chl = 0; chl < channelCount; chl++)
            {
                var filter = _hfpFilters[chl];
                var chlOffset = chl * sampleCount;

                //
                for (int sampleIndex = 0; sampleIndex < sampleCount; sampleIndex++)
                {
                    hfps[chlOffset + sampleIndex] = filter.filterOne(wfps[chlOffset + sampleIndex]);
                }
            }
        }

        #endregion
    }
}
