﻿using StairMed.Entity.DataPools;

namespace StairMed.DataCenter.Recorder.StreamRecorder
{
    /// <summary>
    /// 
    /// </summary>
    internal class HFPStreamRecorder : StreamRecorderBase
    {
        /// <summary>
        /// 收集到所有通道后，将其整合为一个chunk
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkTranspose(object[] objs)
        {
            var chunk = (Chunk)objs[0];

            //
            if (chunk.Hfps == null)
            {
                return;
            }

            //
            var floats = new float[chunk.OneChannelSamples, chunk.ChannelCount];
            for (int channel = 0; channel < chunk.ChannelCount; channel++)
            {
                var channelFixedOffset = channel * chunk.OneChannelSamples;
                for (int j = 0; j < chunk.OneChannelSamples; j++)
                {
                    floats[j, channel] = chunk.Hfps[channelFixedOffset + j];
                }
            }
            _writerFIFO.Add(floats);
        }

        /// <summary>
        /// 将chunk存储至文件中
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkToFile(object[] objs)
        {
            var floats = (float[,])objs[0];
            _writer.AppendFloatData(floats);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <returns></returns>
        protected override string GetFileName(string fileNameWithoutExtension)
        {
            return $"{fileNameWithoutExtension}_hfp.nwb";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetTypeDescription()
        {
            return "Highpass data";
        }
    }
}
