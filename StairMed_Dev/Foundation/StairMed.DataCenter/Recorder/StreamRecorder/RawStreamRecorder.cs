﻿using StairMed.Entity.DataPools;

namespace StairMed.DataCenter.Recorder.StreamRecorder
{
    /// <summary>
    /// 
    /// </summary>
    internal class RawStreamRecorder : StreamRecorderBase
    {
        /// <summary>
        /// 收集到所有通道后，将其整合为一个chunk
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkTranspose(object[] objs)
        {
            var chunk = (Chunk)objs[0];

            // 处理原始数据
            if (chunk.Raws != null)
            {
                var rawFloats = new float[chunk.OneChannelSamples, chunk.ChannelCount];
                for (int channel = 0; channel < chunk.ChannelCount; channel++)
                {
                    var channelFixedOffset = channel * chunk.OneChannelSamples;
                    for (int j = 0; j < chunk.OneChannelSamples; j++)
                    {
                        rawFloats[j, channel] = chunk.Raws[channelFixedOffset + j];
                    }
                }
                _writerFIFO.Add(rawFloats);
            }

            // 处理TRIG数据
            if (chunk.Trigs != null)
            {
                var validTrigChannelCount = 9; // 确保这个值不超过chunk.Trigs数组的长度
                var trig = new int[chunk.OneChannelSamples, validTrigChannelCount];
                for (int trigChannel = 0; trigChannel < validTrigChannelCount; trigChannel++)
                {
                    var channelFixedOffset = trigChannel * chunk.OneChannelSamples;
                    for (int j = 0; j < chunk.OneChannelSamples; j++)
                    {
                        trig[j, trigChannel] = (int)chunk.Trigs[channelFixedOffset + j];
                    }
                }
                _writer.AppendTRIGFloatData(trig);
            }

            //处理HFP数据
            if (chunk.Hfps != null)
            {
                var hfpsfloats = new float[chunk.OneChannelSamples, chunk.ChannelCount];
                for (int channel = 0; channel < chunk.ChannelCount; channel++)
                {
                    var channelFixedOffset = channel * chunk.OneChannelSamples;
                    for (int j = 0; j < chunk.OneChannelSamples; j++)
                    {
                        hfpsfloats[j, channel] = chunk.Hfps[channelFixedOffset + j];
                    }
                }
                _writer.AppendHFPFloatData(hfpsfloats);
            }

            //处理spk数据
            if (chunk.Spks != null)
            {
                var bytes = new byte[chunk.OneChannelSamples, chunk.ChannelCount];
                for (int channel = 0; channel < chunk.ChannelCount; channel++)
                {
                    var channelFixedOffset = channel * chunk.OneChannelSamples;
                    for (int j = 0; j < chunk.OneChannelSamples; j++)
                    {
                        bytes[j, channel] = (byte)(chunk.Spks[channelFixedOffset + j] > 0 ? 0x01 : 0x00);
                    }
                }
                _writer.AppendBooleanData(bytes);
            }
        }

        /// <summary>
        /// 将chunk存储至文件中
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkToFile(object[] objs)
        {
            var floats = (float[,])objs[0];
            _writer.AppendFloatData(floats);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <returns></returns>
        protected override string GetFileName(string fileNameWithoutExtension)
        {
            return $"{fileNameWithoutExtension}_raw.nwb";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetTypeDescription()
        {
            return "Raw data";
        }
    }
}
