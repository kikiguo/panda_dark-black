﻿using StairMed.Entity.DataPools;

namespace StairMed.DataCenter.Recorder.StreamRecorder
{
    /// <summary>
    /// 
    /// </summary>
    internal class TRIGStreamRecorder : StreamRecorderBase
    {
        /// <summary>
        /// 收集到所有通道后，将其整合为一个chunk
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkTranspose(object[] objs)
        {
            if (!NeuralDataCollector.Instance.IsCollecting)
            {  
                return;
            }
            var chunk = (Chunk)objs[0];

            //
            if (chunk.Trigs == null)
            {
                return;
            }

            //
            var floats = new float[chunk.OneChannelSamples, chunk.ChannelCount];
            for (int channel = 0; channel < chunk.ChannelCount; channel++)
            {
                var channelFixedOffset = channel * chunk.OneChannelSamples;
                for (int j = 0; j < chunk.OneChannelSamples; j++)
                {
                    floats[j, channel] = chunk.Trigs[channelFixedOffset + j];
                }
            }
            _writerFIFO.Add(floats);
        }

        /// <summary>
        /// 将chunk存储至文件中
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkToFile(object[] objs)
        {
            var bytes = (float[,])objs[0];
            _writer.AppendFloatData(bytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <returns></returns>
        protected override string GetFileName(string fileNameWithoutExtension)
        {
            return $"{fileNameWithoutExtension}_trig.nwb";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetTypeDescription()
        {
            return "TRIG data";
        }
    }
}
