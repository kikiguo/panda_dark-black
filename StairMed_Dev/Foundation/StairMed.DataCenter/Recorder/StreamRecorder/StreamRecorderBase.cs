﻿using StairMed.AsyncFIFO;
using StairMed.DataFile.NWB;
using StairMed.Entity.DataPools;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StairMed.DataCenter.Recorder.StreamRecorder
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class StreamRecorderBase : IStreamRecorder
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly IAsyncFIFO _transposeFIFO = null; //chunk转为nwb所需要的格式

        /// <summary>
        /// 
        /// </summary>
        protected readonly IAsyncFIFO _writerFIFO = null; //将nwb格式的数据从内存写入至文件

        /// <summary>
        /// 
        /// </summary>
        protected NWBFileWriter _writer = null;

        /// <summary>
        /// 
        /// </summary>
        public StreamRecorderBase()
        {
            _transposeFIFO = new BlockFIFO { Name = nameof(ChunkTranspose), ConsumerAction = ChunkTranspose };
            _writerFIFO = new BlockFIFO { Name = nameof(ChunkToFile), ConsumerAction = ChunkToFile };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sampleRate"></param>
        /// <param name="channels"></param>
        /// <param name="dataTypeDescription"></param>
        /// <param name="probeMapJson"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceChannelCount"></param>
        /// <param name="testDescription"></param>
        /// <param name="version"></param>
        public void StartRecording(string folder, string fileNameWithoutExtension, int sampleRate, List<int> physicsChannels, string probeMapJson, string deviceType, int deviceChannelCount, string testDescription, int version = 1)
        {
            NeuralDataCollector.Instance.NewChunkReceived -= Instance_NewChunkReceived;
            NeuralDataCollector.Instance.NewChunkReceived += Instance_NewChunkReceived;
    

            //
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            //
            _writer = new NWBFileWriter();
            _writer.StartRecording(Path.Combine(folder, GetFileName(fileNameWithoutExtension)), sampleRate, physicsChannels, GetTypeDescription(), probeMapJson, deviceType, deviceChannelCount, testDescription, version);
        }

        /// <summary>
        /// 
        /// </summary>
        public void StopRecording()
        {
            NeuralDataCollector.Instance.NewChunkReceived -= Instance_NewChunkReceived;
            _writer.StopRecording();

            //
            Task.Run(() =>
            {
                //等待将缓存写完
               //Thread.Sleep(500);

                //
                try
                {
                    _transposeFIFO.Dispose();
                }
                catch { }
                try
                {
                    _writerFIFO.Dispose();
                }
                catch { }
            });
        }

        /// <summary>
        /// 接收到数据块
        /// </summary>
        /// <param name="chunk"></param>
        private void Instance_NewChunkReceived(Chunk chunk)
        {
            if (!NeuralDataCollector.Instance.IsCollecting)
            {
                return;
            }
            _transposeFIFO.Add(chunk);
        }

        /// <summary>
        /// 收集到所有通道后，将其整合为一个chunk
        /// </summary>
        /// <param name="objs"></param>
        protected abstract void ChunkTranspose(object[] objs);

        /// <summary>
        /// 将chunk存储至文件中
        /// </summary>
        /// <param name="objs"></param>
        protected abstract void ChunkToFile(object[] objs);

        /// <summary>
        /// 文件类型描述
        /// </summary>
        /// <returns></returns>
        protected abstract string GetTypeDescription();

        /// <summary>
        /// 文件名
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <returns></returns>
        protected abstract string GetFileName(string fileNameWithoutExtension);
    }
}
