﻿using StairMed.Entity.DataPools;

namespace StairMed.DataCenter.Recorder.StreamRecorder
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeStreamRecorder : StreamRecorderBase
    {
        /// <summary>
        /// 收集到所有通道后，将其整合为一个chunk
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkTranspose(object[] objs)
        {
            var chunk = (Chunk)objs[0];

            //
            if (chunk.Spks == null)
            {
                return;
            }

            //
            var bytes = new byte[chunk.OneChannelSamples, chunk.ChannelCount];
            for (int channel = 0; channel < chunk.ChannelCount; channel++)
            {
                var channelFixedOffset = channel * chunk.OneChannelSamples;
                for (int j = 0; j < chunk.OneChannelSamples; j++)
                {
                    bytes[j, channel] = (byte)(chunk.Spks[channelFixedOffset + j] > 0 ? 0x01 : 0x00);
                }
            }
            _writerFIFO.Add(bytes);
        }

        /// <summary>
        /// 将chunk存储至文件中
        /// </summary>
        /// <param name="objs"></param>
        protected override void ChunkToFile(object[] objs)
        {
            var bytes = (byte[,])objs[0];
            _writer.AppendBooleanData(bytes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <returns></returns>
        protected override string GetFileName(string fileNameWithoutExtension)
        {
            return $"{fileNameWithoutExtension}_spike.nwb";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetTypeDescription()
        {
            return "Spike data";
        }
    }
}
