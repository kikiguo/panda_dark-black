﻿using Newtonsoft.Json;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Recorder.StreamRecorder;
using StairMed.Entity.DataPools;
using StairMed.Enum;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;

namespace StairMed.DataCenter.Recorder
{
    /// <summary>
    /// 
    /// </summary>
    public class NeuralDataRecorder : ViewModelBase
    {
        const string TAG = nameof(NeuralDataRecorder);
        public string FullPath { get; private set; }

        public string statusFilePath { get; private set; }

        public string firstFolder { get; private set; }
        #region 单例

        private static readonly NeuralDataRecorder _instance = new NeuralDataRecorder();
        public static NeuralDataRecorder Instance => _instance;
        private NeuralDataRecorder() { }

        #endregion

        /// <summary>
        /// 是否已启动录制 / 是否正在录制中
        /// </summary>
        private bool _isRecording = false;
        public bool IsRecording
        {
            get { return _isRecording; }
            set { Set(ref _isRecording, value); }
        }

        /// <summary>
        /// 记录第一包数据的时间
        /// </summary>
        private DateTime _recordStartTime = DateTime.Now.AddDays(-10);
        public DateTime RecordStartTime
        {
            get { return _recordStartTime; }
            set { Set(ref _recordStartTime, value); }
        }

        /// <summary>
        /// 已录制时长
        /// </summary>
        private TimeSpan _recordedTime = TimeSpan.Zero;
        public TimeSpan RecordedTime
        {
            get { return _recordedTime; }
            set { Set(ref _recordedTime, value); }
        }

        /// <summary>
        /// 保存文件后，是否已开始保存第一包数据
        /// </summary>
        private bool _isRecordStarted = false;

        /// <summary>
        /// 
        /// </summary>
        private List<int> _inputChannels = new List<int>();
        private int _channelCount = 0;
        private int _sampleRate = 0;

        /// <summary>
        /// 采样方式
        /// </summary>
        private CollectMode _collectMode = CollectMode.RawAll;

        /// <summary>
        /// 数据流记录
        /// </summary>
        private readonly List<IStreamRecorder> _recorders = new List<IStreamRecorder>();

        #region 公开接口

        /// <summary>
        /// 保存文件文件
        /// </summary>
        public void StartRecording()
        {
            if (IsRecording)
            {
                return;
            }

            //
            IsRecording = true;
            RecordedTime = TimeSpan.Zero;

            //
            _isRecordStarted = false;

            //
            _sampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate();
            _channelCount = NeuralDataCollector.Instance.GetCollectingChannelsCount();
            _inputChannels = NeuralDataCollector.Instance.GetInputChannelInReports();
            var physicsChannels = new List<int>();
            foreach (var channel in _inputChannels)
            {
                physicsChannels.Add(ReadonlyCONST.ConvertToPhysicsChannel(channel));
            }

            //
            FullPath=GetSavePath(out string folder, out string fileNameWithoutExtension);
            firstFolder = folder;
            //
            _collectMode = NeuralDataCollector.Instance.GetCollectType();
            string probeMapJson = JsonConvert.SerializeObject(ReadonlyCONST.ProbeMap);
            string deviceType = ReadonlyCONST.DeviceType.ToString();
            int deviceChannelCount = ReadonlyCONST.DeviceChannelCount;
            string testDescription = $"object:[{StairMedSolidSettings.Instance.TestType}], purpose:[{StairMedSolidSettings.Instance.TestCode}]";

            //
            if (_collectMode == CollectMode.Spike)
            {
                _recorders.Add(new SpikeStreamRecorder());
            }
            else
            {
                //必须录制
                //if (AdvanceSettings.Instance.IsRecordRaw)
                {
                    _recorders.Add(new RawStreamRecorder());
                }

                //必须录制
                //if (AdvanceSettings.Instance.IsRecordWFP)
                //{
                //    _recorders.Add(new WFPStreamRecorder());
                //}

                //
                if (AdvanceSettings.Instance.IsRecordLFP)
                {
                    _recorders.Add(new LFPStreamRecorder());
                }

                //
                if (AdvanceSettings.Instance.IsRecordHFP)
                {
                    _recorders.Add(new HFPStreamRecorder());
                }

                //
                if (AdvanceSettings.Instance.IsRecordSpike)
                {
                    _recorders.Add(new SpikeStreamRecorder());
                }

                if (AdvanceSettings.Instance.IsRecordTRIG)
                {
                    _recorders.Add(new TRIGStreamRecorder());
                }
            }

            //
            foreach (var recorder in _recorders)
            {
                recorder.StartRecording(folder, fileNameWithoutExtension, _sampleRate, physicsChannels, probeMapJson, deviceType, deviceChannelCount, testDescription);
            }

            //
            NeuralDataCollector.Instance.NewChunkReceived -= Instance_NewChunkReceived;
            NeuralDataCollector.Instance.NewChunkReceived += Instance_NewChunkReceived;


        }

        /// <summary>
        /// 停止录制文件
        /// </summary>
        public void StopRecording()
        {
            if (!IsRecording)
            {
                return;
            }

            //
            IsRecording = false;

            //
            NeuralDataCollector.Instance.NewChunkReceived -= Instance_NewChunkReceived;

            //
            foreach (var recorder in _recorders)
            {
                recorder.StopRecording();
            }
            _recorders.Clear();

            statusFilePath = "nwb.txt";

            string finalfullPath = $"{FullPath}_raw.nwb";

            File.WriteAllText(statusFilePath, finalfullPath + "|"+firstFolder+ "|0");

            Task.Run(() =>
            {
                StartPythonExecutionTimer();
            });

            }

        public void StartPythonExecutionTimer()
        {
            // 设置定时器
            Timer timer = new Timer();
            timer.Elapsed += TimerElapsed;
           
            TimerElapsed(timer, null);
        }
        #endregion
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string statusContent = File.ReadAllText(Path.Combine(currentDirectory, statusFilePath));

            string[] parts = statusContent.Split('|');
            if (parts.Length >= 2)
            {
                string fullPath = parts[0];
                string convertPath = parts[1];
                int status = int.Parse(parts[2]); // 转换状态

                if (status == 0)
                {
                    string command = $"./nwb_convert_mda.exe --param1 '{fullPath}' --param2 '{firstFolder}'";

                    ProcessStartInfo startInfo = new ProcessStartInfo("powershell.exe", $"-ExecutionPolicy RemoteSigned -Command \"{command}\"");
                    startInfo.WorkingDirectory = currentDirectory;
                    startInfo.UseShellExecute = false; // 关闭 Shell 执行
                    startInfo.RedirectStandardOutput = true;
                    startInfo.RedirectStandardError = true;
                    startInfo.CreateNoWindow = true;
                    using (Process process = new Process())
                    {
                        process.StartInfo = startInfo;

                        process.OutputDataReceived += (cmdSender, cmdArgs) =>
                        {
                            if (!string.IsNullOrEmpty(cmdArgs.Data))
                            {
                                Console.WriteLine(cmdArgs.Data);
                            }
                        };
                        process.ErrorDataReceived += (cmdSender, cmdArgs) =>
                        {
                            if (!string.IsNullOrEmpty(cmdArgs.Data))
                            {
                                Console.WriteLine($"Error: {cmdArgs.Data}");
                            }
                        };

                        process.Start();
                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();
                        process.WaitForExit();
                    }

                    // 更新状态标记为1，表示命令已执行
                    status = 1;
                    File.WriteAllText(statusFilePath, $"{fullPath}|{convertPath}|{status}");
                }
            }
        }


        /// <summary>
        /// 存储路径
        /// </summary>
        /// <returns></returns>
        private string GetSavePath(out string folder, out string fileNameWithoutExtension)
        {
            var monkey = StairMedSolidSettings.Instance.TestType;
            var purpose = StairMedSolidSettings.Instance.TestCode;
            var timeStamp = $"{DateTime.Now:yyyyMMdd_HHmmss}";
            var deviceType = $"{ReadonlyCONST.DeviceType.ToString().ToLower()}";

            //
            fileNameWithoutExtension = $"{monkey}_{purpose}_{deviceType}_{timeStamp}";
            folder = Path.Combine(StairMedSolidSettings.Instance.NeuralDataSaveFolder, $"{monkey}_{purpose}_{timeStamp}");

            //
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            string path = Path.Combine(folder, fileNameWithoutExtension);

            return path;
        }


        /// <summary>
        /// 接收到数据块
        /// </summary>
        /// <param name="chunk"></param>
        private void Instance_NewChunkReceived(Chunk chunk)
        {
            if (!_isRecordStarted)
            {
                RecordStartTime = DateTime.Now;
                _isRecordStarted = true;
            }
            RecordedTime = DateTime.Now - RecordStartTime;
        }
    }
}
