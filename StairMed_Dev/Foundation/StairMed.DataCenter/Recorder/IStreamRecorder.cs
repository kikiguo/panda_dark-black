﻿using System.Collections.Generic;

namespace StairMed.DataCenter.Recorder
{
    /// <summary>
    /// 
    /// </summary>
    internal interface IStreamRecorder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileNameWithoutExtension"></param>
        /// <param name="sampleRate"></param>
        /// <param name="physicsChannels"></param>
        /// <param name="probeMapJson"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceChannelCount"></param>
        /// <param name="testDescription"></param>
        /// <param name="version"></param>
        void StartRecording(string folder, string fileNameWithoutExtension, int sampleRate, List<int> physicsChannels, string probeMapJson, string deviceType, int deviceChannelCount, string testDescription, int version = 1);

        /// <summary>
        /// 
        /// </summary>
        void StopRecording();
    }
}
