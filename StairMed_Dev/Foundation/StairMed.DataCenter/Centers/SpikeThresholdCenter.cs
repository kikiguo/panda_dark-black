﻿using HDF5CSharp;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeThresholdCenter : ViewModelBase
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static SpikeThresholdCenter Instance = new SpikeThresholdCenter();
        }
        public static SpikeThresholdCenter Instance => Helper.Instance;
        private SpikeThresholdCenter()
        {
            StairMedSettings.Instance.OnSettingChanged += StairMedSettings_OnSettingChanged;
            // 初始化 _channelThresholds
            for (int i = 0; i < ReadonlyCONST.DeviceChannelCount; i++)
            {
                _channelThresholds[i] = StairMedSettings.Instance.Threshold;
            }
            SetThreshold(StairMedSettings.Instance.Threshold);
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public event Action OnThresholdChanged;

        /// <summary>
        /// 
        /// </summary>
        private readonly object lockObj = new object();

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> SpikeThresholdSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.Threshold),
        };

        /// <summary>
        /// 
        /// </summary>
        private double DefaultThreshold = StairMedSettings.Instance.Threshold;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, double> _channelThresholds = new Dictionary<int, double>();

        /// <summary>
        /// 滤波器参数变化
        /// </summary>
        private void StairMedSettings_OnSettingChanged(HashSet<string> changes)
        {
            lock (lockObj)
            {
                if (SpikeThresholdSettings.Overlaps(changes))
                {
                    SetThreshold(StairMedSettings.Instance.Threshold);
                }
            }
            NotifyThresholdChanged();
        }

        /// <summary>
        /// 设置阈值
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="threshold"></param>
        public void SetThreshold(int inputChannel, double threshold)
        {
            lock (_channelThresholds)
            {
                // 检查键是否存在，如果不存在，则添加
                if (!_channelThresholds.ContainsKey(inputChannel))
                {
                    _channelThresholds[inputChannel] = threshold;
                }
                else if (_channelThresholds[inputChannel] != threshold)
                {
                    _channelThresholds[inputChannel] = threshold;
                }

                NotifyThresholdChanged();
            }
        }

        /// <summary>
        /// 设置阈值
        /// </summary>
        /// <param name="threshold"></param>
        public void SetThreshold(double threshold)
        {
            //DefaultThreshold = threshold;
            lock (_channelThresholds)
            {
                for (int i = 0; i < ReadonlyCONST.DeviceChannelCount; i++)
                {
                    _channelThresholds[i] = threshold;
                }
            }
            NotifyThresholdChanged();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <returns></returns>
        public double GetThreshold(int inputChannel)
        {
            if (_channelThresholds.TryGetValue(inputChannel, out double threshold))
            {
                return threshold;
            }
            return DefaultThreshold;
        }

        /// <summary>
        /// 
        /// </summary>
        private void NotifyThresholdChanged()
        {
            OnThresholdChanged?.Invoke();
        }

    }
}
