﻿using StairMed.AsyncFIFO;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.SpikeMap
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeMapCenter
    {
        #region 单例

        private static readonly SpikeMapCenter _instance = new SpikeMapCenter();
        public static SpikeMapCenter Instance => _instance;
        private SpikeMapCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(SpikeMapCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _fifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _fifo.Add(new object());
                }
            };
        }

        #endregion


        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careInputChannels = new HashSet<int>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, long> _newestSpikeIndexDict = new Dictionary<int, long>();

        /// <summary>
        /// 更新数据:samplerate, recvd, channel newest spike index
        /// </summary>
        public event Action<int, long, Dictionary<int, long>> NewestSpikeIndexUpdated;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs != null && objs.Length == 2)
            {
                var isRegister = (bool)objs[0];
                if (objs[1] is List<int> channels)
                {
                    foreach (var channel in channels)
                    {
                        HandleResigsterEvent(isRegister, channel);
                    }
                }
                return;
            }

            //处理新接收的数据块
            if (objs != null && objs.Length == 1 && objs[0] is Chunk chunk && _careInputChannels.Count > 0)
            {
                HandleNewChunk(chunk);
                return;
            }

            //开始新的采集，清空所有数据
            _newestSpikeIndexDict.Clear();
        }


        /// <summary>
        /// 处理新接收的数据块
        /// </summary>
        /// <param name="chunk"></param>
        private void HandleNewChunk(Chunk chunk)
        {
            //判断chunk块是否有效或是否有人监听
            if (chunk.OneChannelSamples == 0 || chunk.Spks == null)
            {
                return;
            }

            //处理所有关注的通道
            foreach (var channel in _careInputChannels)
            {
                //该通道的数据在chunk中的位置
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //未创建该通道的缓存，则创建
                if (!_newestSpikeIndexDict.ContainsKey(channel))
                {
                    _newestSpikeIndexDict[channel] = int.MinValue; //默认spike已经过去很久了
                }

                //
                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                //查找最新的spike：倒序查找
                for (int i = end; i >= begin; i--)
                {
                    if (chunk.Spks[i] > 0)
                    {
                        _newestSpikeIndexDict[channel] = i - begin + chunk.DataIndex;
                        break;
                    }
                }
            }

            //
            NewestSpikeIndexUpdated?.Invoke(chunk.SampleRate, chunk.DataIndex + chunk.OneChannelSamples, _newestSpikeIndexDict);
        }




        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="inputChannel"></param>
        private void HandleResigsterEvent(bool isRegister, int inputChannel)
        {
            if (isRegister)
            {
                _careInputChannels.Add(inputChannel);
            }
            else
            {
                _careInputChannels.Remove(inputChannel);
                _newestSpikeIndexDict.Remove(inputChannel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannel"></param>
        public void RegisterChannels(List<int> inputChannels)
        {
            _fifo.Add(true, inputChannels);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannel"></param>
        public void UnregisterChannels(List<int> inputChannels)
        {
            _fifo.Add(false, inputChannels);
        }

        #endregion
    }
}
