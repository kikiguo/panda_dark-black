﻿using StairMed.Array;
using StairMed.DataCenter.Centers.Trigger;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.PSTH
{
    /// <summary>
    /// 
    /// </summary>
    internal class PSTHBuffer
    {
        public int PSTHChannel { get; set; }
        public int PSTHTriggerChannel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long RasterIndex = 0;

        /// <summary>
        /// 
        /// </summary>
        public int SampleRate { get; set; }
        public int ChunkSpkCount { get; set; }
        public long SpikeFirstIndex { get; set; }
        public long SpikeUsed { get; set; }
        public List<PooledArray<float>> SpikeTrains { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Queue<TriggerEdge> TriggerTrains { get; set; }
    }
}
