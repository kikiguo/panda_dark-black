﻿using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.Trigger;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.PSTH
{
    /// <summary>
    /// 
    /// </summary>
    public class PSTHCenter
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static PSTHCenter Instance = new PSTHCenter();
        }
        public static PSTHCenter Instance => Helper.Instance;
        private PSTHCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(PSTHCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _fifo.Add(chunk);
            TriggerDataPool.Instance.NewTriggerPackReceived += (triggerPack) => _fifo.Add(triggerPack);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _fifo.Add(new object());
                }
            };
        }
        #endregion

        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careCombineChannels = new HashSet<int>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, PSTHBuffer> _psthBufferDict = new Dictionary<int, PSTHBuffer>();

        /// <summary>
        /// 更新数据:psthchannel, triggerchannel, samplerate,
        /// </summary>
        public event Action<int, int, int, List<PSTHRaster>> HandleNewPSTH;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs != null && objs.Length == 2)
            {
                HandleResigsterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //处理新接收的数据块
            if (objs != null && objs.Length == 1 && objs[0] is Chunk chunk && _careCombineChannels.Count > 0)
            {
                HandleNewChunk(chunk);
                return;
            }


            //处理新接收的数据块
            if (objs != null && objs.Length == 1 && objs[0] is TriggerPack triggerPack && _careCombineChannels.Count > 0)
            {
                HandleNewTrigger(triggerPack);
                return;
            }

            //开始新的采集，清空所有数据
            _psthBufferDict.Clear();
        }

        /// <summary>
        /// 处理新接收的数据块
        /// </summary>
        /// <param name="chunk"></param>
        private void HandleNewChunk(Chunk chunk)
        {
            //判断chunk块是否有效或是否有人监听
            if (chunk.OneChannelSamples == 0 || chunk.Spks == null || _careCombineChannels.Count <= 0)
            {
                return;
            }

            //处理所有关注的通道
            foreach (var combineChannel in _careCombineChannels)
            {
                //该通道的数据在chunk中的位置
                var channel = combineChannel >> 16;
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //未创建该通道的缓存，则创建
                if (!_psthBufferDict.ContainsKey(combineChannel))
                {
                    _psthBufferDict[combineChannel] = new PSTHBuffer { };
                }

                //确保已经创建spk的buffer
                var psthBuffer = _psthBufferDict[combineChannel];
                if (psthBuffer.SpikeTrains == null || psthBuffer.SpikeTrains.Count == 0)
                {
                    if (psthBuffer.SpikeTrains == null)
                    {
                        psthBuffer.SpikeTrains = new List<PooledArray<float>>();
                    }
                    psthBuffer.SpikeFirstIndex = chunk.DataIndex;
                    psthBuffer.SpikeUsed = 0;
                }

                //
                psthBuffer.PSTHChannel = channel;
                psthBuffer.SampleRate = chunk.SampleRate;
                psthBuffer.ChunkSpkCount = chunk.OneChannelSamples;

                //将当前chunk中的spk缓存
                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                var spksBuffer = new PooledArray<float>(chunk.OneChannelSamples);
                System.Array.Copy(chunk.Spks.Array, begin, spksBuffer.Array, 0, chunk.OneChannelSamples);
                psthBuffer.SpikeTrains.Add(spksBuffer);

                //
                CheckForNewRaster(psthBuffer);
            }
        }

        /// <summary>
        /// 处理新接收的trigger
        /// </summary>
        /// <param name="triggerPack"></param>
        private void HandleNewTrigger(TriggerPack triggerPack)
        {
            //判断chunk块是否有效或是否有人监听
            if (triggerPack.Triggers == null || triggerPack.Triggers.Count <= 0 || _careCombineChannels.Count <= 0)
            {
                return;
            }

            //处理所有关注的通道
            foreach (var combineChannel in _careCombineChannels)
            {
                //
                var triggerChannel = combineChannel & 0xffff;
                if (!triggerPack.Triggers.ContainsKey(triggerChannel))
                {
                    continue;
                }

                //
                var triggers = triggerPack.Triggers[triggerChannel];
                if (triggers == null || triggers.Count <= 0)
                {
                    continue;
                }

                //
                //未创建该通道的缓存，则创建
                if (!_psthBufferDict.ContainsKey(combineChannel))
                {
                    _psthBufferDict[combineChannel] = new PSTHBuffer { };
                }

                //确保已经创建spk的buffer
                var psthBuffer = _psthBufferDict[combineChannel];
                if (psthBuffer.TriggerTrains == null)
                {
                    psthBuffer.TriggerTrains = new Queue<TriggerEdge>();
                }

                //
                psthBuffer.PSTHTriggerChannel = triggerChannel;

                //
                foreach (var trigger in triggers)
                {
                    psthBuffer.TriggerTrains.Enqueue(trigger);
                }

                //
                CheckForNewRaster(psthBuffer);
            }
        }

        /// <summary>
        /// 判断是否需要新增raster
        /// </summary>
        /// <param name="psthBuffer"></param>
        private void CheckForNewRaster(PSTHBuffer psthBuffer)
        {
            //尚未缓存spk
            if (psthBuffer.SpikeTrains == null || psthBuffer.SpikeTrains.Count <= 0)
            {
                return;
            }

            //左右监控多少ms的spike
            var pre = StairMedSettings.Instance.PSTHPreTriggerSpanMS;
            var post = StairMedSettings.Instance.PSTHPostTriggerSpanMS;

            //关注上升沿 or 下降沿
            var raiseEdge = StairMedSettings.Instance.PSTHTriggerEdge;

            //左右监控多少个spike
            var preSpikeCount = pre * psthBuffer.SampleRate / 1000;
            var postSpikeCount = post * psthBuffer.SampleRate / 1000 - 1;

            //整体一个raster有多少个spike
            var rasterSpikeCount = preSpikeCount + postSpikeCount + 1;

            //spike数据太短，不够用
            if (psthBuffer.SpikeTrains.Count < 1.0 * rasterSpikeCount / psthBuffer.ChunkSpkCount)
            {
                return;
            }

            //尚未缓存trigger
            if (psthBuffer.TriggerTrains == null || psthBuffer.TriggerTrains.Count <= 0)
            {
                return;
            }

            //
            var rasters = new List<PSTHRaster>();
            while (true)
            {
                if (psthBuffer.TriggerTrains.Count <= 0)
                {
                    break;
                }

                //
                var trigger = psthBuffer.TriggerTrains.Peek();

                //上升沿或下降沿未对齐
                if (trigger.ToHigh != raiseEdge)
                {
                    psthBuffer.TriggerTrains.Dequeue();
                    continue;
                }

                //trigger所需的 pre spike 不足
                if (trigger.Index - preSpikeCount < psthBuffer.SpikeFirstIndex + psthBuffer.SpikeUsed)
                {
                    psthBuffer.TriggerTrains.Dequeue();
                    continue;
                }

                //raster的起始位置
                var leftChunk = Convert.ToInt32((trigger.Index - preSpikeCount - psthBuffer.SpikeFirstIndex) / psthBuffer.ChunkSpkCount);
                var leftSpkIndex = Convert.ToInt32((trigger.Index - preSpikeCount - psthBuffer.SpikeFirstIndex) % psthBuffer.ChunkSpkCount);
                if (leftChunk < 0 || leftSpkIndex < 0)
                {
                    psthBuffer.TriggerTrains.Dequeue();
                    continue;
                }

                //trigger所需的 post spike 不足
                if (trigger.Index + postSpikeCount >= psthBuffer.SpikeFirstIndex + psthBuffer.ChunkSpkCount * psthBuffer.SpikeTrains.Count)
                {
                    //清除过时的左侧的chunk
                    RemoveSpikeChunk(psthBuffer, leftChunk);
                    break;
                }

                //raster的终止位置
                var rightChunk = Convert.ToInt32((trigger.Index + postSpikeCount - psthBuffer.SpikeFirstIndex) / psthBuffer.ChunkSpkCount);
                var rightSpkIndex = Convert.ToInt32((trigger.Index + postSpikeCount - psthBuffer.SpikeFirstIndex) % psthBuffer.ChunkSpkCount);

                //取当前的raster：注raster仅保存spike的序号，非spike数据不保存
                var raster = new List<int>();
                if (leftChunk == rightChunk)
                {
                    for (int k = 0; k < rasterSpikeCount; k++)
                    {
                        //仅保存spike的序号
                        if (psthBuffer.SpikeTrains[leftChunk][leftSpkIndex + k] != 0)
                        {
                            raster.Add(k);
                        }
                    }
                }
                else //跨多个chunk时
                {
                    var offset = 0;

                    //第一个chunk
                    for (int m = 0; m < psthBuffer.ChunkSpkCount - leftSpkIndex; m++)
                    {
                        if (psthBuffer.SpikeTrains[leftChunk][leftSpkIndex + m] != 0)
                        {
                            raster.Add(offset + m);
                        }
                    }
                    offset += psthBuffer.ChunkSpkCount - leftSpkIndex;

                    //中间n个chunk
                    for (int k = leftChunk + 1; k < rightChunk; k++)
                    {
                        for (int m = 0; m < psthBuffer.ChunkSpkCount; m++)
                        {
                            if (psthBuffer.SpikeTrains[k][m] != 0)
                            {
                                raster.Add(offset + m);
                            }
                        }
                        offset += psthBuffer.ChunkSpkCount;
                    }

                    //最后一个chunk
                    for (int m = 0; m < rightSpkIndex + 1; m++)
                    {
                        if (psthBuffer.SpikeTrains[rightChunk][m] != 0)
                        {
                            raster.Add(offset + m);
                        }
                    }
                }

                //
                rasters.Add(new PSTHRaster
                {
                    Raster = raster,
                    RasterIndex = psthBuffer.RasterIndex++,
                });

                //去掉已经处理的trigger
                psthBuffer.TriggerTrains.Dequeue();

                //清除用过的chunk
                RemoveSpikeChunk(psthBuffer, Convert.ToInt32((trigger.Index + postSpikeCount - preSpikeCount - psthBuffer.SpikeFirstIndex) / psthBuffer.ChunkSpkCount));
            }

            //通知
            if (rasters.Count > 0)
            {
                HandleNewPSTH?.Invoke(psthBuffer.PSTHChannel, psthBuffer.PSTHTriggerChannel, psthBuffer.SampleRate, rasters);
            }
        }

        /// <summary>
        /// 清除用过的或无效的buffer
        /// </summary>
        /// <param name="psthBuffer"></param>
        /// <param name="removedChunk"></param>
        /// <returns></returns>
        private void RemoveSpikeChunk(PSTHBuffer psthBuffer, int removedChunk)
        {
            while (removedChunk > 0)
            {
                var toRemoveChunk = psthBuffer.SpikeTrains[0];
                toRemoveChunk.Dispose();
                psthBuffer.SpikeTrains.RemoveAt(0);
                psthBuffer.SpikeUsed = Math.Max(0, psthBuffer.SpikeUsed - psthBuffer.ChunkSpkCount);
                psthBuffer.SpikeFirstIndex += psthBuffer.ChunkSpkCount;
                removedChunk--;
            }
        }


        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="combineChannel"></param>
        private void HandleResigsterEvent(bool isRegister, int combineChannel)
        {
            if (isRegister)
            {
                _careCombineChannels.Add(combineChannel);
            }
            else
            {
                _careCombineChannels.Remove(combineChannel);
                if (_psthBufferDict.ContainsKey(combineChannel))
                {
                    var psthBuffer = _psthBufferDict[combineChannel];
                    RemoveSpikeChunk(psthBuffer, psthBuffer.SpikeTrains != null ? psthBuffer.SpikeTrains.Count : 0);
                    _psthBufferDict.Remove(combineChannel);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="psthChannel"></param>
        /// <param name="triggerChannel"></param>
        public void RegisterPSTHChannel(int psthChannel, int triggerChannel)
        {
            var combineChannel = (psthChannel << 16) + triggerChannel;
            _fifo.Add(true, combineChannel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="psthChannel"></param>
        /// <param name="triggerChannel"></param>
        public void UnregisterPSTHChannel(int psthChannel, int triggerChannel)
        {
            var combineChannel = (psthChannel << 16) + triggerChannel;
            _fifo.Add(false, combineChannel);
        }

        #endregion
    }
}
