﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.DataCenter.Centers.PSTH
{
    /// <summary>
    /// 
    /// </summary>
    public class PSTHRaster
    {
        public List<int> Raster { get; set; }
        public long RasterIndex { get; set; }
    }
}
