﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.DataCenter.Centers.SpikeScope
{
    public class TimestampWithLabel
    {
        public float Timestamp { get; set; }
        public int Label { get; set; }
        public TimestampWithLabel() { }

    }
}
