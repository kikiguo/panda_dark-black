﻿using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.DataCenter.Centers.SpikeScope
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeScopeCenter
    {
        #region 单例

        private static readonly SpikeScopeCenter _instance = new SpikeScopeCenter();
        public static SpikeScopeCenter Instance => _instance;
        private SpikeScopeCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(SpikeScopeCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _fifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _collectId++;
                    _bufferChunks.Clear();
                }
            };
        }

        #endregion

        /// <summary>
        /// 异步调用
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 第几次采集
        /// </summary>
        private int _collectId = 0;

        /// <summary>
        /// 需要计算哪些通道的spikescope
        /// </summary>
        private readonly Dictionary<int, int> _careSpikeScopeInputChannels = new Dictionary<int, int>();

        /// <summary>
        /// int collectId, int channel, int sampleRate, long recvd, List<SpikeScopeInfo> spikeScopes
        /// </summary>
        public event Action<int, int, int, long, List<SpikeScopeInfo>> RecvdChannelSpikeScopes;

        /// <summary>
        /// 接收到的chunk缓存
        /// </summary>
        private readonly List<Chunk> _bufferChunks = new List<Chunk>();
        private int _searchStartIndex = 0;
        public int SortedIndex = -1;

        /// <summary>
        /// 定义事件委托
        /// </summary>
        public event Action<int, int, int, long, int, float[]> SpikeTimestampsUpdated;


        /// <summary>
        /// 异步处理接收到的chunk
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //通道注册相关
            if (objs != null && objs.Length == 2)
            {
                HandleSpikeScopeRegisterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //没有hfp就没有spikescope
            var chunk = (Chunk)objs[0];
            if (chunk.Hfps == null || chunk.Spks == null)
            {
                return;
            }

            //无关注的spike scope
            if (!_careSpikeScopeInputChannels.Any(r => r.Value > 0))
            {
                return;
            }

            //如果不是连续的数据，否则清空
            if (_bufferChunks.Count == 0 || _bufferChunks[^1].DataIndex + chunk.OneChannelSamples != chunk.DataIndex)
            {
                _bufferChunks.Clear();
                _searchStartIndex = 0;
            }

            //缓存数据
            _bufferChunks.Add(chunk);

            
            //spikescope左右长度
            var leftLen = chunk.SpikeScopeTimeMS * chunk.SampleRate / 2000;
            var rightLen = chunk.SpikeScopeTimeMS * chunk.SampleRate / 1000;
            var faceLen = leftLen + rightLen + 1;

            //把所有缓存的chunk看做一个整体，计算搜索的起始位置和停止位置
            var searchStopIndex = _bufferChunks.Count * chunk.OneChannelSamples - rightLen - 1;
            var searchStartIndex = Math.Max(_searchStartIndex, leftLen); //必须大于spikescope的左侧预留长度
            if (searchStartIndex >= searchStopIndex)
            {
                return;
            }

            //
            var overflowThreshold = StairMedSettings.Instance.ArtifactThreshold;
            var ischeckOverflow = StairMedSettings.Instance.EnableSuppression;

            //循环遍历每个通道
            foreach (var kvp in _careSpikeScopeInputChannels)
            {
                //通道无人关注
                if (kvp.Value <= 0)
                {
                    continue;
                }

                //获取通道数据在chunk中的位置
                var inputChannel = kvp.Key;
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(inputChannel);

                //不存在此通道数据
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //提取新的spikescope
                var newSpikeScopes = new List<SpikeScopeInfo>(chunk.OneChannelSamples * 2 / faceLen + 1); //避免重复分配内存

                //通道数据在chunk中的固定偏移
                var fixedOffet = channelIndexInChunk * chunk.OneChannelSamples;

                //存储时间戳
                Dictionary<int, List<float>> labelToTimestamps = new Dictionary<int, List<float>>();


                //遍历spike数据，找到spike
                for (int i = searchStartIndex; i <= searchStopIndex; i++)
                {
                    var chunkIndex = i / chunk.OneChannelSamples;
                    var spkIndex = i % chunk.OneChannelSamples; //spike在单个chunk中的位置

                    //找到一个spike
                    if (_bufferChunks[chunkIndex].Spks[spkIndex + fixedOffet] >= 1)
                    {

                        //
                        var face = CopySpikeScopeFromChunk(chunk, leftLen, rightLen, faceLen, fixedOffet, i);

                        //pca/kmean判断
                        SpikeSort.Instance.Train(face.Array, face.Length);
                        var label = SpikeSort.Instance.Decide(face.Array, face.Length);


                        //是否溢出
                        var isOverflow = false;
                        if (ischeckOverflow)
                        {
                            for (int k = 0; k < face.Array.Length; k++)
                            {
                                var d = face.Array[k];
                                if (d > overflowThreshold || d < -overflowThreshold)
                                {
                                    isOverflow = true;
                                    break;
                                }
                            }
                        }

                        //计算频率
                        var frequency = CalculateFrequency(face.Array, chunk.SampleRate, faceLen);

                        //进行归一化
                        var normalizedFrequency = NormalizeFrequency(frequency);

                        // 先拿出电压值最低点计算时间戳
                        float timestamp = CalculateTimestamp(_bufferChunks[chunkIndex].DataIndex, spkIndex, chunk.SampleRate);

                        // 更新labelToTimestamps
                        if (!labelToTimestamps.ContainsKey(label))
                        {
                            labelToTimestamps[label] = new List<float>();
                        }
                        labelToTimestamps[label].Add(timestamp);

                        //存储
                        newSpikeScopes.Add(new SpikeScopeInfo
                        {
                            SpikeIndex = _bufferChunks[chunkIndex].DataIndex + spkIndex,
                            Hfps = face,
                            Label = label,
                            IsOverflow = isOverflow,
                            DischargeFrequency = normalizedFrequency,//66 is test value
                            Timestamps = timestamp,
                        });
                    }
                }

                //新的spikescope，则通知外部
                RecvdChannelSpikeScopes?.Invoke(_collectId, inputChannel, chunk.SampleRate, chunk.TotalRecvdSampleOfEachChannel, newSpikeScopes);
               
         
                //SpikeScopeUpdated?.Invoke(_collectId, inputChannel, chunk.SampleRate, chunk.TotalRecvdSampleOfEachChannel, newSpikeScopes);
            }

            //去掉已搜索过的chunk
            var searchedChunk = (searchStopIndex - leftLen) / chunk.OneChannelSamples;
            if (searchedChunk > 0)
            {
                _bufferChunks.RemoveRange(0, searchedChunk);
            }
            _searchStartIndex = searchStopIndex - chunk.OneChannelSamples * searchedChunk + 1;
        }

       

        /// <summary>
        /// 计算频率
        /// </summary>
        private double CalculateFrequency(float[] signal, int sampleRate, int windowSize)
        {
            // 寻找信号的峰值（或谷值）位置
            var peaks = FindPeaks(signal);

            // 计算峰值（或谷值）之间的时间间隔的平均值（以样本为单位）
            var peakIntervals = CalculatePeakIntervals(peaks);

            // 将时间间隔转换为频率（以赫兹为单位）
            var frequency = sampleRate / peakIntervals.Average();

            return frequency;
        }

        /// <summary>
        /// 谷值
        /// </summary>
        private List<int> FindPeaks(float[] signal)
        {
            // 寻找信号中的峰值（或谷值）位置
            var peaks = new List<int>();
            for (int i = 1; i < signal.Length - 1; i++)
            {
                if (signal[i] > signal[i - 1] && signal[i] > signal[i + 1])
                {
                    peaks.Add(i);
                }
            }
            return peaks;
        }

        /// <summary>
        /// 谷值
        /// </summary>
        private List<int> CalculatePeakIntervals(List<int> peaks)
        {
            // 计算峰值（或谷值）之间的时间间隔（样本数）
            var intervals = new List<int>();
            for (int i = 1; i < peaks.Count; i++)
            {
                intervals.Add(peaks[i] - peaks[i - 1]);
            }
            return intervals;
        }

        /// <summary>
        /// 频率进行归一化
        /// </summary>
        private double NormalizeFrequency(double frequency, double minFrequency=0, double maxFrequency=100)
        {
            // 将频率归一化到指定的范围（minFrequency和maxFrequency之间）
            var normalizedFrequency = (frequency - minFrequency) / (maxFrequency - minFrequency);
            return normalizedFrequency;
        }

    

        /// <summary>
        /// 计算SPK时间戳波谷值的timestamps
        /// </summary>
        private float CalculateTimestamp(long dataIndex, int spkIndex,int sampleRate)
        {
            // 实现计算时间戳的逻辑，根据索引、采样率和faceLen计算时间戳
            float rawTimestamp = (float)(dataIndex + spkIndex) / sampleRate;
            return rawTimestamp % 10000;
        }


        /// <summary>
        /// 分类时间戳
        /// </summary>
        private int FindValleyIndex(float[] array, int length)
        {
            if (array == null || length < 3) // 需要至少3个点来确定一个谷值
            {
                return -1;
            }

            for (int i = 1; i < length - 1; i++)
            {
                if (array[i] < array[i - 1] && array[i] < array[i + 1])
                {
                    return i;
                }
            }

            return -1; // 没有找到谷值
        }


        /// <summary>
        /// 从chunk中提取出spike对应的spike scope
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="leftLen"></param>
        /// <param name="rightLen"></param>
        /// <param name="faceLen"></param>
        /// <param name="fixedOffet"></param>
        /// <param name="spikePos"></param>
        /// <returns></returns>
        private PooledArray<float> CopySpikeScopeFromChunk(Chunk chunk, int leftLen, int rightLen, int faceLen, int fixedOffet, int spikePos)
        {
            var face = new PooledArray<float>(faceLen);

            //判断spike scope是否跨chunk
            var leftChunk = (spikePos - leftLen) / chunk.OneChannelSamples;
            var leftSpkIndex = (spikePos - leftLen) % chunk.OneChannelSamples;
            var rightChunk = (spikePos + rightLen) / chunk.OneChannelSamples;
            var rightSpkIndex = (spikePos + rightLen) % chunk.OneChannelSamples;

            //SpikeScope在一个chunk里
            if (leftChunk == rightChunk)
            {
                if(_bufferChunks[leftChunk].Hfps.Array!=null)
                System.Array.Copy(_bufferChunks[leftChunk].Hfps.Array, fixedOffet + leftSpkIndex, face.Array, 0, faceLen);
            }
            else //跨chunk
            {
                var offset = 0;

                //拷贝最左侧chunk
                var copyLen = chunk.OneChannelSamples - leftSpkIndex;
                if (_bufferChunks[leftChunk].Hfps.Array != null)
                    System.Array.Copy(_bufferChunks[leftChunk].Hfps.Array, leftSpkIndex + fixedOffet, face.Array, offset, copyLen);
                offset += copyLen;

                //拷贝中部chunk
                for (int k = leftChunk + 1; k < rightChunk; k++)
                {
                    if (_bufferChunks[k].Hfps.Array != null)
                        System.Array.Copy(_bufferChunks[k].Hfps.Array, fixedOffet, face.Array, offset, chunk.OneChannelSamples);
                    offset += chunk.OneChannelSamples;
                }

                //拷贝右侧chunk
                if (_bufferChunks[rightChunk].Hfps.Array != null)
                    System.Array.Copy(_bufferChunks[rightChunk].Hfps.Array, fixedOffet, face.Array, offset, rightSpkIndex + 1);
            }

            //
            return face;
        }





        #region 添加对通道的关注

        /// <summary>
        /// 注册:SpikeScope
        /// </summary>
        /// <param name="inputChannel"></param>
        public void RegisterSpikeScope(int inputChannel)
        {
            _fifo.Add(true, inputChannel); //通过在fifo线程注册，避免多线程访问，避免频繁lock
        }

        /// <summary>
        /// 取消注册:SpikeScope
        /// </summary>
        /// <param name="inputChannel"></param>
        public void UnregisterSpikeScope(int inputChannel)
        {
            _fifo.Add(false, inputChannel); //通过在fifo线程注册，避免多线程访问，避免频繁lock
        }

        /// <summary>
        /// 通道注册或取消注册:SpikeScope
        /// </summary>
        /// <param name="isregister"></param>
        /// <param name="inputChannel"></param>
        private void HandleSpikeScopeRegisterEvent(bool isregister, int inputChannel)
        {
            if (isregister)
            {
                if (!_careSpikeScopeInputChannels.ContainsKey(inputChannel))
                {
                    _careSpikeScopeInputChannels[inputChannel] = 0;
                }
                _careSpikeScopeInputChannels[inputChannel]++;
            }
            else
            {
                if (_careSpikeScopeInputChannels.ContainsKey(inputChannel))
                {
                    _careSpikeScopeInputChannels[inputChannel] = Math.Max(0, _careSpikeScopeInputChannels[inputChannel] - 1);
                }
            }
        }

        #endregion
    }
}
