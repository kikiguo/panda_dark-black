﻿using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.RMS;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.DataCenter.Centers.SpikeScope
{
    /// <summary>
    /// 
    /// </summary>
    public class RMSCenter
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static RMSCenter Instance = new RMSCenter();
        }
        public static RMSCenter Instance => Helper.Instance;
        private RMSCenter()
        {
            _chunkFifo = new BlockFIFO { Name = nameof(RMSCenter), ConsumerAction = Consume };


            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _chunkFifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _bufferedChunk.Clear();
                }
            };

         
        }


        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> IntervalsSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.RMSTimeScaleMS),
            nameof(StairMedSettings.Instance.RMSBinSizeMS),
        };


        #endregion

        /// <summary>
        /// 缓存chunk
        /// </summary>
        private readonly Queue<Chunk> _bufferedChunk = new Queue<Chunk>();

        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careInputChannels = new HashSet<int>();




        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, RMSInfo> _rmsDict = new Dictionary<int, RMSInfo>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _chunkFifo;

        /// <summary>
        /// 
        /// </summary>
        public event Action<int, double> HfpRmsUpdated;


        /// <summary>
        /// 更新数据
        /// </summary>
        public event Action<int, RMSInfo> RMSUpdated;


        /// <summary>
        /// 
        /// </summary>
        private int _timespan = 1;
        private int _binSize = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs == null || objs.Length == 0) return;

            if (objs.Length == 2 && objs[0] is bool && objs[1] is int)
            {
                HandleResigsterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //判断chunk块是否有效或是否有人监听
            var chunk = (Chunk)objs[0];

            if (chunk.OneChannelSamples == 0 || chunk.Hfps == null || chunk.Hfps.Length <= 0 || HfpRmsUpdated == null || _careInputChannels.Count <= 0)
            {
                _bufferedChunk.Clear();
                return;
            }

            //统计RMS时使用的数据长度
            int statisticSampleCount = chunk.SampleRate * StairMedSolidSettings.Instance.RMSStatisticMS / 1000;

            //缓存chunk
            _bufferedChunk.Enqueue(chunk);
            int maxBufferedChunk = (statisticSampleCount + chunk.OneChannelSamples - 1) / chunk.OneChannelSamples;
            while (_bufferedChunk.Count > maxBufferedChunk)
            {
                _bufferedChunk.Dequeue();
            }

            //数据不足，不进行计算
            if (_bufferedChunk.Count != maxBufferedChunk) return;

            CalculateRMSAndUpdate();
            /*  //
              var chunkArray = _bufferedChunk.ToArray();
              foreach (var channel in _careInputChannels)
              {
                  //该通道的数据在chunk中的位置
                  var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                  if (channelIndexInChunk < 0)
                  {
                      continue;
                  }

                  //
                  var begin = channelIndexInChunk * chunk.OneChannelSamples;
                  var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                  //求平方和
                  var powerSum = 0.0f;
                  var loadSampleCount = 0;

                  //循环求平方和
                  for (int chunkIndex = chunkArray.Length - 1; chunkIndex >= 0; chunkIndex--)
                  {
                      var curChunk = chunkArray[chunkIndex];

                      //从最新的数据开始加载
                      for (int sampleIndex = end; sampleIndex >= begin; sampleIndex--)
                      {
                          loadSampleCount++;
                          var val = curChunk.Hfps.Array[sampleIndex];
                          powerSum += val * val;

                          //
                          if (loadSampleCount >= statisticSampleCount)
                          {
                              break;
                          }
                      }

                      //
                      if (loadSampleCount >= statisticSampleCount)
                      {
                          break;
                      }
                  }

                  //计算 均方根
                  if (loadSampleCount > 0)
                  {
                      HfpRmsUpdated?.Invoke(channel, Math.Sqrt(powerSum / loadSampleCount));
                  }
        }*/


        }

        private void CalculateRMSAndUpdate()
        {
            var chunkArray = _bufferedChunk.ToArray();
            _rmsDict.Clear();

            foreach (var channel in _careInputChannels)
            {
                double rmsValue = ComputeRMSValue(channel, chunkArray);
                UpdateOrCreateRMSInfo(channel, rmsValue);

                RMSUpdated?.Invoke(channel, _rmsDict[channel]);
                HfpRmsUpdated?.Invoke(channel, rmsValue);
            }
        }

        private void UpdateOrCreateRMSInfo(int channel, double rmsValue)
        {
            if (_rmsDict.ContainsKey(channel))
            {
                _rmsDict[channel].UpdateRMS(rmsValue);
            }
            else
            {
                _rmsDict[channel] = new RMSInfo(channel);
                _rmsDict[channel].UpdateRMS(rmsValue);
            }
        }

        public static double[] Denoise(double[] inputData)
        {
            double sum = 0;
            for (int i = 0; i < inputData.Length; i++)
            {
                sum += inputData[i];
            }

            double average = sum / inputData.Length;

            double[] denoisedData = new double[inputData.Length];
            for (int i = 0; i < inputData.Length; i++)
            {
                denoisedData[i] = inputData[i] - average;
            }

            return denoisedData;
        }

        private double ComputeRMSValue(int channel, Chunk[] chunkArray)
        {
            
            double sumOfSquares = 0;
            int totalSamples = 0;

            foreach (var chunk in chunkArray)
            {
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                for (int sampleIndex = begin; sampleIndex <= end; sampleIndex++)
                {
                    totalSamples++;
                    var val = chunk.Hfps.Array[sampleIndex];
                    sumOfSquares += val * val;
                }
            }

            return totalSamples > 0 ? Math.Sqrt(sumOfSquares / totalSamples) : 0.0;
       
    }



        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="channel"></param>
        private void HandleResigsterEvent(bool isRegister, int channel)
        {
            if (isRegister)
            {
                _careInputChannels.Add(channel);
            }
            else
            {
                _careInputChannels.Remove(channel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterInputChannel(int channel)
        {
            _chunkFifo.Add(true, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterInputChannel(int channel)
        {
            _chunkFifo.Add(false, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterRMSChannel(int channel)
        {
            _chunkFifo.Add(true, channel);
        }


        /// <summary>
        /// 清空
        /// </summary>
        /// <param name="channel"></param>
        public void CleanChannel(int channel)
        {
            _chunkFifo.Add(channel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterRMSChannel(int channel)
        {
            _chunkFifo.Add(false, channel);
        }


        #endregion
    }
}
