﻿using Accord.MachineLearning;
using Accord.Statistics.Analysis;
using StairMed.Core.Settings;
using StairMed.DataCenter.Logger;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace StairMed.DataCenter.Centers.SpikeScope
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeSort
    {
        /// <summary>
        /// 
        /// </summary>
        enum TrainingState
        {
            Idle, //未计算分类方法
            Buffering, //缓存足够的数据
            Training, //计算分类方法
            Trained, //已计算完成
        }

        /// <summary>
        /// 
        /// </summary>
        private const string TAG = nameof(SpikeSort);
        private const double PCA_THRESHOLD = 0.95;

        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static SpikeSort Instance = new SpikeSort();
        }
        public static SpikeSort Instance => Helper.Instance;
        private SpikeSort()
        {
            CareSettings.Add(nameof(StairMedSettings.Instance.AutoKMeansK));
            if (!StairMedSettings.Instance.AutoKMeansK)
            {
                CareSettings.Add(nameof(StairMedSettings.Instance.KMeansK));
            }

            //
            StairMedSettings.Instance.OnSettingChanged += (changes) =>
            {
                if (CareSettings.Overlaps(changes))
                {
                    if (StairMedSettings.Instance.AutoKMeansK)
                    {
                        CareSettings.Remove(nameof(StairMedSettings.Instance.KMeansK));
                    }
                    else
                    {
                        CareSettings.Add(nameof(StairMedSettings.Instance.KMeansK));
                    }
                    _forceRetrain = true;
                }
            };
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> CareSettings = new HashSet<string> { };

        /// <summary>
        /// 训练数据集合
        /// </summary>
        private readonly List<float[]> _trainingList = new List<float[]>();
        private PrincipalComponentAnalysis _pca = null;
        private KMeansClusterCollection _clusters = null;
        private bool _forceRetrain = false;


        /// <summary>
        /// 训练状态
        /// </summary>
        private TrainingState _state = TrainingState.Idle;

        /// <summary>
        /// 训练所用的数据的长度
        /// </summary>
        private int _trainFaceLen = 0;

        /// <summary>
        /// 
        /// </summary>
        private int _kmeansK = 1;

        /// <summary>
        /// 训练数据个数有要求：N秒
        /// </summary>
        private readonly Stopwatch _watch = new Stopwatch();

        /// <summary>
        /// 训练
        /// </summary>
        /// <param name="face"></param>
        /// <param name="faceLen"></param>
        public void Train(float[] face, int faceLen)
        {
            switch (_state)
            {
                case TrainingState.Idle:
                    {
                        _state = TrainingState.Buffering;
                        ClearBufferedFace();
                        _watch.Restart();

                        //
                        _trainFaceLen = faceLen;
                        BufferNewFace(face, faceLen);
                    }
                    break;
                case TrainingState.Buffering:
                    {
                        if (_trainFaceLen != faceLen)
                        {
                            ClearBufferedFace();
                            _watch.Restart();
                        }

                        //
                        _trainFaceLen = faceLen;
                        BufferNewFace(face, faceLen);

                        //
                        if (_watch.ElapsedMilliseconds > 5000 && (_trainingList.Count > 150 || _trainingList.Count > _trainFaceLen * 5))
                        {
                            //
                            _watch.Stop();
                            _state = TrainingState.Training;

                            //
                            Task.Run(() =>
                            {
                                DoPcaKmeanCalculation();
                            });
                        }
                    }
                    break;
                case TrainingState.Training:
                    break;
                case TrainingState.Trained:
                    {
                        //spikescope长度发生变化 或者 需要强制重新分类
                        if (_trainFaceLen != faceLen || _forceRetrain)
                        {
                            _forceRetrain = false;

                            //清空历史
                            ClearBufferedFace();
                            _watch.Restart();

                            //切换为喂数据模式
                            _state = TrainingState.Buffering;

                            //
                            _trainFaceLen = faceLen;
                            BufferNewFace(face, faceLen);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 进行pca kmean计算：耗时操作，
        /// </summary>
        private void DoPcaKmeanCalculation()
        {
            //过多的数据，会导致PCA耗时严重，这里离散取N个样本(N:尽量与通道个数交叉)
            const int MaxSample = 1000;
            var trainingDatas = new List<float[]>();
            if (_trainingList.Count <= MaxSample)
            {
                trainingDatas = _trainingList;
            }
            else
            {
                var step = _trainingList.Count / MaxSample;
                for (int i = 0; i < MaxSample; i++)
                {
                    trainingDatas.Add(_trainingList[i * step]);
                }
            }
            var trainSampleCount = trainingDatas.Count;

            //对数据格式整理
            var dataForPCATrain = new double[trainSampleCount][];
            for (int i = 0; i < trainSampleCount; i++)
            {
                dataForPCATrain[i] = new double[_trainFaceLen];
                for (int j = 0; j < _trainFaceLen; j++)
                {
                    dataForPCATrain[i][j] = trainingDatas[i][j];
                }
            }

            //进行pca处理
            _watch.Restart();
            _pca = new PrincipalComponentAnalysis();
            _pca.Learn(dataForPCATrain);
            _pca.NumberOfOutputs = _pca.GetNumberOfComponents(PCA_THRESHOLD);
            var dataForKMeansTrain = _pca.Transform(dataForPCATrain);
            _watch.Stop();
            LogTool.Logger.LogI($"pca cost:{_watch.ElapsedMilliseconds}ms, data count={dataForPCATrain.Length}, face len={_trainFaceLen}, pca count={_pca.NumberOfOutputs}");

            //获取kmean的最佳分类k
            int bestK = GetBestKForKMeans(dataForKMeansTrain);
            StairMedSettings.Instance.KMeansK = bestK;
            _kmeansK = bestK;
            if (bestK > 1)
            {
                LogTool.Logger.LogI($"KMeans bestK={bestK}");
                var kmean = new KMeans(bestK);
                _clusters = kmean.Learn(dataForKMeansTrain);
            }

            //
            trainingDatas.Clear();
            ClearBufferedFace();
            _state = TrainingState.Trained;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newFace"></param>
        /// <param name="faceLen"></param>
        private void BufferNewFace(float[] newFace, int faceLen)
        {
            var newFaceCopy = ArrayPool<float>.Shared.Rent(faceLen);
            System.Array.Copy(newFace, newFaceCopy, faceLen);
            _trainingList.Add(newFaceCopy);
        }

        /// <summary>
        /// 
        /// </summary>
        private void ClearBufferedFace()
        {
            foreach (var face in _trainingList)
            {
                ArrayPool<float>.Shared.Return(face);
            }
            _trainingList.Clear();
        }

        /// <summary>
        /// 获取kmean的最佳分类k
        /// </summary>
        /// <param name="pcaOutput"></param>
        /// <param name="kMax"></param>
        /// <returns></returns>
        private static int GetBestKForKMeans(double[][] pcaOutput, int kMax = 10)
        {
            if (!StairMedSettings.Instance.AutoKMeansK)
            {
                return StairMedSettings.Instance.KMeansK;
            }

            //
            var bestK = -1;

            //
            var errorDict = new Dictionary<int, double>();
            for (int k = 2; k < kMax; k++)
            {
                var sum = 0.0;
                for (int i = 0; i < 10; i++)
                {
                    var temp = new KMeans(k);
                    temp.Learn(pcaOutput);
                    sum += temp.Error;
                }
                errorDict[k] = sum;
            }

            //
            for (int k = 2; k < kMax - 1; k++)
            {
                if ((errorDict[k] - errorDict[k + 1]) / errorDict[k] < 0.45)
                {
                    bestK = k;
                    break;
                }
            }

            //
            return Math.Max(bestK, 2);
        }

        /// <summary>
        /// 判决
        /// </summary>
        /// <param name="face"></param>
        /// <param name="faceLen"></param>
        /// <returns></returns>
        public int Decide(float[] face, int faceLen)
        {
            if (_trainFaceLen != faceLen || _state != TrainingState.Trained || _kmeansK == 1)
            {
                return -1;
            }

            //
            try
            {
                //
                var dataForPCA = new double[faceLen];
                for (int i = 0; i < faceLen; i++)
                {
                    dataForPCA[i] = face[i];
                }

                //
                return _clusters.Decide(_pca.Transform(dataForPCA));
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Decide)}", ex);
                return -1;
            }
        }
    }
}
