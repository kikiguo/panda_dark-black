﻿using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.DataCenter.Centers.SpikeScope
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeRateCenter
    {
        #region 单例

        private static readonly SpikeRateCenter _instance = new SpikeRateCenter();
        public static SpikeRateCenter Instance => _instance;
        private SpikeRateCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(SpikeScopeCenter), ConsumerAction = Consume };
            SpikeScopeCenter.Instance.RecvdChannelSpikeScopes += (int collectId, int channel, int sampleRate, long recvd, List<SpikeScopeInfo> spikeScopes) =>
            {
                _fifo.Add(collectId, channel, sampleRate, recvd, spikeScopes);
            };
        }

        #endregion

        /// <summary>
        /// 异步调用
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 第几次采集
        /// </summary>
        private int _collectId = 0;

        /// <summary>
        /// 需要计算哪些通道的spikerate
        /// </summary>
        private readonly Dictionary<int, int> _careSpikeRateInputChannels = new Dictionary<int, int>();

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, HashSet<long>> _channelSpks = new Dictionary<int, HashSet<long>>();

        /// <summary>
        /// Spike Rate
        /// </summary>
        public event Action<int, double> SpikeRateUpdated;


  


        /// <summary>
        /// 异步处理接收到的chunk
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //通道注册相关
            if (objs != null && objs.Length == 2)
            {
                HandleSpikeRateRegisterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //spikescope到来
            if (objs != null && objs.Length == 5)
            {
                //无关注的spike scope
                if (!_careSpikeRateInputChannels.Any(r => r.Value > 0))
                {
                    return;
                }

                //
                var collectId = (int)objs[0];
                var inputChannel = (int)objs[1];
                var sampleRate = (int)objs[2];
                var recvd = (long)objs[3];
                var spikeScopes = (List<SpikeScopeInfo>)objs[4];

                //新的一次采集，清空历史数据
                if (_collectId != collectId)
                {
                    _channelSpks.Clear();
                    _collectId = collectId;
                }

                //未关注此通道
                if (!_careSpikeRateInputChannels.ContainsKey(inputChannel) || _careSpikeRateInputChannels[inputChannel] <= 0)
                {
                    return;
                }

                //缓存起来
                if (!_channelSpks.ContainsKey(inputChannel))
                {
                    _channelSpks[inputChannel] = new HashSet<long>();
                }

                //遍历缓存，溢出的波形不计数
                foreach (var spikescope in spikeScopes)
                {
                    if (!spikescope.IsOverflow)
                    {
                        _channelSpks[inputChannel].Add(spikescope.SpikeIndex);
                    }
                }

                //计算发放率
                var statisticTimeMS = StairMedSolidSettings.Instance.SpikeRateStatisticMS;
                var statisticSampleCount = sampleRate * statisticTimeMS / 1000;

                //仅保留统计时间范围内的spike序号
                _channelSpks[inputChannel].RemoveWhere(r => r < recvd - statisticSampleCount);

                //对外通知
                SpikeRateUpdated?.Invoke(inputChannel, _channelSpks[inputChannel].Count * 1000.0 / statisticTimeMS);
            }
        }




        #region 添加对通道的关注

        /// <summary>
        /// 注册:SpikeRate
        /// </summary>
        /// <param name="inputChannel"></param>
        public void RegisterSpikeRate(int inputChannel)
        {
            _fifo.Add(true, inputChannel); //通过在fifo线程注册，避免多线程访问，避免频繁lock
        }

        /// <summary>
        /// 取消注册:SpikeRate
        /// </summary>
        /// <param name="inputChannel"></param>
        public void UnregisterSpikeRate(int inputChannel)
        {
            _fifo.Add(false, inputChannel); //通过在fifo线程注册，避免多线程访问，避免频繁lock
        }

        /// <summary>
        /// 通道注册或取消注册:SpikeRate
        /// </summary>
        /// <param name="isregister"></param>
        /// <param name="inputChannel"></param>
        private void HandleSpikeRateRegisterEvent(bool isregister, int inputChannel)
        {
            //计算SpikeRate时，需要判断SpikeScope是否overflow
            if (isregister)
            {
                SpikeScopeCenter.Instance.RegisterSpikeScope(inputChannel);
            }
            else
            {
                SpikeScopeCenter.Instance.UnregisterSpikeScope(inputChannel);
            }

            //
            if (isregister)
            {
                if (!_careSpikeRateInputChannels.ContainsKey(inputChannel))
                {
                    _careSpikeRateInputChannels[inputChannel] = 0;
                }
                _careSpikeRateInputChannels[inputChannel]++;
            }
            else
            {
                if (_careSpikeRateInputChannels.ContainsKey(inputChannel))
                {
                    _careSpikeRateInputChannels[inputChannel] = Math.Max(0, _careSpikeRateInputChannels[inputChannel] - 1);
                }
            }
        }

        #endregion

    }
}
