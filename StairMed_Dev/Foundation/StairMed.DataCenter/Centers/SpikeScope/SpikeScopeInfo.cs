﻿using StairMed.Array;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.SpikeScope
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeScopeInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public long SpikeIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PooledArray<float> Hfps { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Label { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOverflow = false;

        public bool Enable = true;

        public double DischargeFrequency { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float Timestamps { get; set; }

    }
}
