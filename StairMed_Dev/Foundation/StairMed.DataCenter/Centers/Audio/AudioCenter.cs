﻿using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.ISI;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StairMed.DataCenter.Centers.Audio
{
    public class AudioCenter
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static AudioCenter Instance = new AudioCenter();
        }
        public static AudioCenter Instance => Helper.Instance;
        private AudioCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(AudioCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _fifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _fifo.Add(new object());
                }
            };

            //
            _timespan = StairMedSettings.Instance.ISITimeScaleMS;
            _binSize = StairMedSettings.Instance.ISIBinSizeMS;

            //
            StairMedSettings.Instance.OnSettingChanged += (HashSet<string> changes) =>
            {
                if (IntervalsSettings.Overlaps(changes))
                {
                    _timespan = StairMedSettings.Instance.ISITimeScaleMS;
                    _binSize = StairMedSettings.Instance.ISIBinSizeMS;
                }
            };
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> IntervalsSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.ISITimeScaleMS),
            nameof(StairMedSettings.Instance.ISIBinSizeMS),
        };

        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careInputChannels = new HashSet<int>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, ISIInfo> _isiDict = new Dictionary<int, ISIInfo>();

        /// <summary>
        /// 更新数据
        /// </summary>
        public event Action<int, ISIInfo> ISIUpdated;

        /// <summary>
        /// 
        /// </summary>
        private int _timespan = 1;
        private int _binSize = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs != null && objs.Length == 2)
            {
                HandleResigsterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //清空
            if (objs != null && objs.Length == 1 && objs[0] is int channel)
            {
                _isiDict.Remove(channel);
                return;
            }

            //处理新接收的数据块
            if (objs != null && objs.Length == 1 && objs[0] is Chunk chunk && _careInputChannels.Count > 0)
            {
                HandleNewChunk(chunk);
                return;
            }

            //开始新的采集，清空所有数据
            _isiDict.Clear();
        }

        /// <summary>
        /// 处理新接收的数据块
        /// </summary>
        /// <param name="chunk"></param>
        private void HandleNewChunk(Chunk chunk)
        {
            //判断chunk块是否有效或是否有人监听
            if (chunk.OneChannelSamples == 0 || chunk.Spks == null || _careInputChannels.Count <= 0)
            {
                return;
            }

            //处理所有关注的通道
            foreach (var channel in _careInputChannels)
            {
                //该通道的数据在chunk中的位置
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //未创建该通道的缓存，则创建
                if (!_isiDict.ContainsKey(channel))
                {
                    _isiDict[channel] = new ISIInfo
                    {
                        Channel = channel,
                        Histograms = new List<float>(),
                        PreviousSpikeIndex = short.MinValue,
                        IntervalDict = new Dictionary<int, int>(),
                        BinSizeMs = _binSize,
                        TimeSpanMs = _timespan,
                    };
                }

                //
                var isiInfo = _isiDict[channel];
                isiInfo.TimeSpanMs = _timespan;
                isiInfo.BinSizeMs = _binSize;

                //预定义isi记录
                if (isiInfo.IntervalDict.Count != chunk.SampleRate)
                {
                    for (int i = 1; i <= chunk.SampleRate; i++)
                    {
                        isiInfo.IntervalDict[i] = 0;
                    }
                }

                //
                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                //上一次spike的index前移
                isiInfo.PreviousSpikeIndex -= chunk.OneChannelSamples;

                //计算每个时间间隔，并记录下来
                for (int i = begin; i <= end; i++)
                {
                    if (chunk.Spks[i] > 0)
                    {
                        var interval = i - begin - isiInfo.PreviousSpikeIndex;
                        if (interval > 0 && interval <= chunk.SampleRate)
                        {
                            isiInfo.IntervalDict[Convert.ToInt32(interval)] += 1;
                        }
                        isiInfo.PreviousSpikeIndex = i - begin;
                    }
                }

                //计算各个参数
                isiInfo.ISICount = isiInfo.IntervalDict.Sum(r => r.Value);
                if (isiInfo.ISICount <= 0)
                {
                    isiInfo.Mean = 0;
                    isiInfo.ISIFreq = 0;
                    isiInfo.StdDev = 0;
                    isiInfo.Histograms = new List<float>();
                }
                else
                {
                    //计算均值、方差、等
                    isiInfo.Mean = isiInfo.IntervalDict.Sum(r => 1000.0 * r.Key * r.Value / chunk.SampleRate) / isiInfo.ISICount;
                    isiInfo.ISIFreq = 1000 / isiInfo.Mean;
                    isiInfo.StdDev = Math.Sqrt(isiInfo.IntervalDict.Sum(r => Math.Pow(1000.0 * r.Key / chunk.SampleRate - isiInfo.Mean, 2) * r.Value) / isiInfo.ISICount);

                    //计算直方图
                    var histCount = isiInfo.TimeSpanMs / isiInfo.BinSizeMs;
                    var histograms = new List<float>(histCount);
                    var binSamples = isiInfo.BinSizeMs * chunk.SampleRate / 1000; //基本可以保证整数

                    //计算直方图
                    for (int i = 0; i < histCount; i++)
                    {
                        var sum = 0L;
                        for (int j = 0; j < binSamples; j++)
                        {
                            var isi = i * binSamples + j + 1;
                            if (isiInfo.IntervalDict.ContainsKey(isi))
                            {
                                sum += isiInfo.IntervalDict[isi];
                            }
                        }

                        //计算占比
                        histograms.Add(1.0f * sum / isiInfo.ISICount);
                    }

                    //
                    isiInfo.Histograms = histograms;
                }

                //
                ISIUpdated?.Invoke(channel, isiInfo);
            }
        }

        /// <summary>
        /// 清空
        /// </summary>
        /// <param name="channel"></param>
        public void CleanChannel(int channel)
        {
            _fifo.Add(channel);
        }


        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="channel"></param>
        private void HandleResigsterEvent(bool isRegister, int channel)
        {
            if (isRegister)
            {
                _careInputChannels.Add(channel);
            }
            else
            {
                _careInputChannels.Remove(channel);
                _isiDict.Remove(channel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterISIChannel(int channel)
        {
            _fifo.Add(true, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterISIChannel(int channel)
        {
            _fifo.Add(false, channel);
        }

        #endregion
    }
}
