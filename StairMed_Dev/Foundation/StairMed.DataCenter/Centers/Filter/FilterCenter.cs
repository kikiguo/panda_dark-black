﻿using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.Filter;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.DataCenter.Centers.Filter
{
    public class FilterCenter
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static FilterCenter Instance = new FilterCenter();
        }
        public static FilterCenter Instance => Helper.Instance;
        private FilterCenter()
        {
            _chunkFifo = new BlockFIFO { Name = nameof(FilterCenter), ConsumerAction = Consume };


            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _chunkFifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _bufferedChunk.Clear();
                }
            };


        }


        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> IntervalsSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.FilterTimeScaleMS),
            nameof(StairMedSettings.Instance.FilterBinSizeMS),
        };


        #endregion

        /// <summary>
        /// 缓存chunk
        /// </summary>
        private readonly Queue<Chunk> _bufferedChunk = new Queue<Chunk>();

        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careInputChannels = new HashSet<int>();




        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, FilterInfo> _filterDict = new Dictionary<int, FilterInfo>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _chunkFifo;


        /// <summary>
        /// 更新数据
        /// </summary>
        public event Action<int, FilterInfo> FilterUpdated;


        /// <summary>
        /// 
        /// </summary>
        private int _timespan = 1;
        private int _binSize = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs == null || objs.Length == 0) return;

            if (objs.Length == 2 && objs[0] is bool && objs[1] is int)
            {
                HandleResigsterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //判断chunk块是否有效或是否有人监听
            var chunk = (Chunk)objs[0];

            if (chunk.OneChannelSamples == 0 || chunk.Hfps == null || chunk.Hfps.Length <= 0 || _careInputChannels.Count <= 0)
            {
                _bufferedChunk.Clear();
                return;
            }

            //统计Filter时使用的数据长度
            int statisticSampleCount = chunk.SampleRate * StairMedSolidSettings.Instance.FilterStatisticMS / 1000;

            //缓存chunk
            _bufferedChunk.Enqueue(chunk);
            int maxBufferedChunk = (statisticSampleCount + chunk.OneChannelSamples - 1) / chunk.OneChannelSamples;
            while (_bufferedChunk.Count > maxBufferedChunk)
            {
                _bufferedChunk.Dequeue();
            }

            //数据不足，不进行计算
            if (_bufferedChunk.Count != maxBufferedChunk) return;

            CalculateFilterAndUpdate();
            /*  //
              var chunkArray = _bufferedChunk.ToArray();
              foreach (var channel in _careInputChannels)
              {
                  //该通道的数据在chunk中的位置
                  var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                  if (channelIndexInChunk < 0)
                  {
                      continue;
                  }

                  //
                  var begin = channelIndexInChunk * chunk.OneChannelSamples;
                  var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                  //求平方和
                  var powerSum = 0.0f;
                  var loadSampleCount = 0;

                  //循环求平方和
                  for (int chunkIndex = chunkArray.Length - 1; chunkIndex >= 0; chunkIndex--)
                  {
                      var curChunk = chunkArray[chunkIndex];

                      //从最新的数据开始加载
                      for (int sampleIndex = end; sampleIndex >= begin; sampleIndex--)
                      {
                          loadSampleCount++;
                          var val = curChunk.Hfps.Array[sampleIndex];
                          powerSum += val * val;

                          //
                          if (loadSampleCount >= statisticSampleCount)
                          {
                              break;
                          }
                      }

                      //
                      if (loadSampleCount >= statisticSampleCount)
                      {
                          break;
                      }
                  }

                  //计算 均方根
                  if (loadSampleCount > 0)
                  {
                      HfpRmsUpdated?.Invoke(channel, Math.Sqrt(powerSum / loadSampleCount));
                  }
        }*/


        }

        private void CalculateFilterAndUpdate()
        {
            var chunkArray = _bufferedChunk.ToArray();
            _filterDict.Clear();

            foreach (var channel in _careInputChannels)
            {
                double[] filterValue = ComputeFilterValue(channel, chunkArray);
                UpdateOrCreateFilterInfo(channel, filterValue);

                FilterUpdated?.Invoke(channel, _filterDict[channel]);

            }
        }

        private void UpdateOrCreateFilterInfo(int channel, double[] filterValue)
        {
            if (_filterDict.ContainsKey(channel))
            {
                _filterDict[channel].UpdateFilter(filterValue);
            }
            else
            {
                _filterDict[channel] = new FilterInfo(channel);
                _filterDict[channel].UpdateFilter(filterValue);
            }
        }

        public static double[] Denoise(double[] inputData)
        {
            double sum = 0;
            for (int i = 0; i < inputData.Length; i++)
            {
                sum += inputData[i];
            }

            double average = sum / inputData.Length;

            double[] denoisedData = new double[inputData.Length];
            for (int i = 0; i < inputData.Length; i++)
            {
                denoisedData[i] = inputData[i] - average;
            }

            return denoisedData;
        }

        private double[] ComputeFilterValue(int channel, Chunk[] chunkArray)
        {

            double LowsumOfSquares = 0;
            double HigsumOfSquares = 0;
            int totalSamples = 0;
            double[] d = new double[2];
            foreach (var chunk in chunkArray)
            {
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                for (int sampleIndex = begin; sampleIndex <= end; sampleIndex++)
                {
                    totalSamples++;
                    var val = chunk.Lfps.Array[sampleIndex];
                    var val2 = chunk.Hfps.Array[sampleIndex];
                    LowsumOfSquares += val * val;
                    HigsumOfSquares += val2 * val2;
                }
            }
            d[0] = totalSamples > 0 ? Math.Sqrt(LowsumOfSquares / totalSamples) : 0.0;
            d[1] = totalSamples > 0 ? Math.Sqrt(HigsumOfSquares / totalSamples) : 0.0;
            return d;

        }



        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="channel"></param>
        private void HandleResigsterEvent(bool isRegister, int channel)
        {
            if (isRegister)
            {
                _careInputChannels.Add(channel);
            }
            else
            {
                _careInputChannels.Remove(channel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterInputChannel(int channel)
        {
            _chunkFifo.Add(true, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterInputChannel(int channel)
        {
            _chunkFifo.Add(false, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterFilterChannel(int channel)
        {
            _chunkFifo.Add(true, channel);
        }


        /// <summary>
        /// 清空
        /// </summary>
        /// <param name="channel"></param>
        public void CleanChannel(int channel)
        {
            _chunkFifo.Add(channel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterFilterChannel(int channel)
        {
            _chunkFifo.Add(false, channel);
        }


        #endregion
    }
}
