﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.DataCenter.Centers.Filter
{
    public class FilterInfo
    {
        public int Channel { get; set; }

        //
        public int BinSizeMs { get; set; }
        public int TimeSpanMs { get; set; }

        //
        public double Mean { get; set; }
        public double FilterFreq { get; set; }
        public double StdDev { get; set; }
        public double Filters { get; set; }
        public int FilterCount { get; set; }

        //
        public List<float> LowHistograms { get; set; }
        public List<float> HigHistograms { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal long PreviousSpikeIndex { get; set; }
        internal List<float> IntervalDict { get; set; }

        public FilterInfo(int channel)
        {
            Channel = channel;
            BinSizeMs = 0;
            TimeSpanMs = 0;
            Mean = 0.0;
            FilterFreq = 0.0;
            StdDev = 0.0;
            Filters = 0.0;
            FilterCount = 0;
            LowHistograms = new List<float>();
            HigHistograms = new List<float>();
            PreviousSpikeIndex = 0;
            IntervalDict = new List<float>();
        }


        public void UpdateFilter(double[] newFilter)
        {
            this.Filters = newFilter[0];
            this.FilterCount++;
            this.LowHistograms.Add((float)newFilter[0]);
            this.HigHistograms.Add((float)newFilter[1]);
            double delta = newFilter[0] - this.Mean;

            if (this.FilterCount != 0)
            {
                this.Mean += delta / this.FilterCount;
            }

            double delta2 = newFilter[0] - this.Mean;
            this.StdDev += delta * delta2;

            // 现在不在这里进行开方运算
            if (this.FilterCount > 1)
            {
                double denominator = this.FilterCount - 1;
                if (denominator != 0)
                {
                    this.StdDev = this.StdDev / denominator;
                }
            }

            if (this.TimeSpanMs != 0)
            {
                this.FilterFreq = this.FilterCount / this.TimeSpanMs * 1000.0;
            }

        }

        // 创建一个新的方法来一次性计算标准差
        public double GetStandardDeviation()
        {
            if (this.FilterCount > 1)
            {
                return Math.Sqrt(this.StdDev / (this.FilterCount - 1));
            }
            return 0.0;
        }
    }
}
