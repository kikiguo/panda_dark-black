﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.DataCenter.Centers.RMS
{
    public class RMSInfo
    {
        public int Channel { get; set; }

        //
        public int BinSizeMs { get; set; }
        public int TimeSpanMs { get; set; }

        //
        public double Mean { get; set; }
        public double RMSFreq { get; set; }
        public double StdDev { get; set; }
        public double Rms { get; set; }
        public int RMSCount { get; set; }

        //
        public List<float> Histograms { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal long PreviousSpikeIndex { get; set; }
        internal List<float> IntervalDict { get; set; }

        public RMSInfo(int channel)
        {
            Channel = channel;
            BinSizeMs = 0;
            TimeSpanMs = 0;
            Mean = 0.0;
            RMSFreq = 0.0;
            StdDev = 0.0;
            Rms = 0.0;
            RMSCount = 0;
            Histograms = new List<float>();
            PreviousSpikeIndex = 0;
            IntervalDict = new List<float>();
        }


        public void UpdateRMS(double newRMS)
        {
            this.Rms = newRMS;
            this.RMSCount++;
            this.Histograms.Add((float)newRMS);
            double delta = newRMS - this.Mean;

            if (this.RMSCount != 0)
            {
                this.Mean += delta / this.RMSCount;
            }

            double delta2 = newRMS - this.Mean;
            this.StdDev += delta * delta2;

            // 现在不在这里进行开方运算
            if (this.RMSCount > 1)
            {
                double denominator = this.RMSCount - 1;
                if (denominator != 0)
                {
                    this.StdDev = this.StdDev / denominator;
                }
            }

            if (this.TimeSpanMs != 0)
            {
                this.RMSFreq = this.RMSCount / this.TimeSpanMs * 1000.0;
            }

        }

        // 创建一个新的方法来一次性计算标准差
        public double GetStandardDeviation()
        {
            if (this.RMSCount > 1)
            {
                return Math.Sqrt(this.StdDev / (this.RMSCount - 1));
            }
            return 0.0;
        }
    }
 }
