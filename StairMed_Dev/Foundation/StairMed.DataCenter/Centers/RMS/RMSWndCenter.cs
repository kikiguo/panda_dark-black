﻿using Accord.Math;
using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.Entity.DataPools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StairMed.DataCenter.Centers.RMS
{
    public class RMSWndCenter
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static RMSWndCenter Instance = new RMSWndCenter();
        }
        public static RMSWndCenter Instance => Helper.Instance;
        private RMSWndCenter()
        {
            _chunkFifo = new BlockFIFO { Name = nameof(RMSWndCenter), ConsumerAction = Consume };


            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _chunkFifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _bufferedChunk.Clear();
                }
            };

            _timespan = StairMedSettings.Instance.RMSTimeScaleMS;
            _binSize = StairMedSettings.Instance.RMSBinSizeMS;

            //
            StairMedSettings.Instance.OnSettingChanged += (HashSet<string> changes) =>
            {
                if (IntervalsSettings.Overlaps(changes))
                {
                    _timespan = StairMedSettings.Instance.RMSTimeScaleMS;
                    _binSize = StairMedSettings.Instance.RMSBinSizeMS;
                }
            };
        }


        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> IntervalsSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.RMSTimeScaleMS),
            nameof(StairMedSettings.Instance.RMSBinSizeMS),
        };


        #endregion

        /// <summary>
        /// 缓存chunk
        /// </summary>
        private readonly Queue<Chunk> _bufferedChunk = new Queue<Chunk>();

        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careInputChannels = new HashSet<int>();




        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, RMSInfo> _rmsDict = new Dictionary<int, RMSInfo>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _chunkFifo;

      


        /// <summary>
        /// 更新数据
        /// </summary>
        public event Action<int, RMSInfo> RMSUpdated;

        /// <summary>
        /// 
        /// </summary>
        private int _timespan = 1;
        private int _binSize = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs != null && objs.Length == 2)
            {
                HandleResigsterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //判断chunk块是否有效或是否有人监听
            var chunk = (Chunk)objs[0];
            if (chunk.OneChannelSamples == 0 || chunk.Hfps == null || chunk.Hfps.Length <= 0 || _careInputChannels.Count <= 0)
            {
                _bufferedChunk.Clear();
                return;
            }

            //统计RMS时使用的数据长度
            var statisticSampleCount = chunk.SampleRate * StairMedSolidSettings.Instance.RMSStatisticMS / 1000;

            //缓存chunk
            _bufferedChunk.Enqueue(chunk);
            var maxBufferedChunk = (statisticSampleCount + chunk.OneChannelSamples - 1) / chunk.OneChannelSamples;
            while (_bufferedChunk.Count > maxBufferedChunk)
            {
                _bufferedChunk.Dequeue();
            }

            //数据不足，不进行计算
            if (_bufferedChunk.Count != maxBufferedChunk)
            {
                return;
            }

            //
            var chunkArray = _bufferedChunk.ToArray();
         

            foreach (var channel in _careInputChannels)
            {
                //该通道的数据在chunk中的位置
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //
                var rmsInfo = new RMSInfo(channel)
                {
                    Histograms = new List<float>(),
                    PreviousSpikeIndex = short.MinValue,
                    IntervalDict = new List<float>(),
                    BinSizeMs = _binSize,
                    TimeSpanMs = _timespan,
                };

                rmsInfo.TimeSpanMs = _timespan;
                rmsInfo.BinSizeMs = _binSize;

                //
                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples - 1;

                //求平方和
                var powerSum = 0.0f;
                var loadSampleCount = 0;

                //上一次spike的index前移
                rmsInfo.PreviousSpikeIndex -= chunk.OneChannelSamples;
                for (int chunkIndex = chunkArray.Length - 1; chunkIndex >= 0; chunkIndex--)
                {
                    var curChunk = chunkArray[chunkIndex];

                    //从最新的数据开始加载
                    for (int sampleIndex = end; sampleIndex >= begin; sampleIndex--)
                    {
                        loadSampleCount++;
                        var val = curChunk.Hfps.Array[sampleIndex];
                        powerSum += val * val;
                        rmsInfo.IntervalDict.Add(val);
                        //
                        if (loadSampleCount >= statisticSampleCount)
                        {
                            break;
                        }
                    }

                    //
                    if (loadSampleCount >= statisticSampleCount)
                    {
                        break;
                    }

                  
                }

                rmsInfo.RMSCount = rmsInfo.IntervalDict.Count;
                if (rmsInfo.RMSCount <= 0)
                {
                    rmsInfo.Mean = 0;
                    rmsInfo.RMSFreq = 0;
                    rmsInfo.StdDev = 0;
                    rmsInfo.Histograms = new List<float>();
                }
                else
                {
                    rmsInfo.Rms = Math.Sqrt(powerSum / loadSampleCount);


                    //计算直方图
                    var histCount = rmsInfo.TimeSpanMs / rmsInfo.BinSizeMs;
                    var histograms = new List<float>(histCount);
                    var binSamples = rmsInfo.RMSCount / histCount; //基本可以保证整数
                    if(binSamples == 0) {
                        break;
                    }
                    //计算直方图
                    for (int i = 0; i < histCount; i++)
                    {
                        float sum = 0f;
                        var rmscount = 0;
                        for (int j = i * binSamples; j < (i + 1) * binSamples; j++)
                        {
                            rmscount++;
                            sum += (float)Math.Pow( rmsInfo.IntervalDict[i],2);
                        }

                        double o = Math.Sqrt(sum / rmscount);
                        if (double.IsNaN(o))
                        {
                            histograms.Add(0f);
                        }
                        else
                        {
                            histograms.Add((float)o);
                        }
                      

                        //计算占比

                    }

                    //
                    rmsInfo.Histograms = histograms;
                }



                //计算各个参数


                //
                RMSUpdated?.Invoke(channel, rmsInfo);
            }



        }


        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="channel"></param>
        private void HandleResigsterEvent(bool isRegister, int channel)
        {
            if (isRegister)
            {
                _careInputChannels.Add(channel);
            }
            else
            {
                _careInputChannels.Remove(channel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterInputChannel(int channel)
        {
            _chunkFifo.Add(true, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterInputChannel(int channel)
        {
            _chunkFifo.Add(false, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterRMSChannel(int channel)
        {
            _chunkFifo.Add(true, channel);
        }


        /// <summary>
        /// 清空
        /// </summary>
        /// <param name="channel"></param>
        public void CleanChannel(int channel)
        {
            _chunkFifo.Add(channel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterRMSChannel(int channel)
        {
            _chunkFifo.Add(false, channel);
        }


        #endregion
    }
}
