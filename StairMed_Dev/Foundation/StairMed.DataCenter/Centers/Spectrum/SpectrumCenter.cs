﻿using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.Entity.DataPools;
using StairMed.Tools;
using System;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.Spectrum
{
    /// <summary>
    /// 
    /// </summary>
    public class SpectrumCenter
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static SpectrumCenter Instance = new SpectrumCenter();
        }
        public static SpectrumCenter Instance => Helper.Instance;
        private SpectrumCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(SpectrumCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _fifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _fifo.Add(new object());
                }
            };

            //
            _fftSize = StairMedSettings.Instance.FFTSize;

            //
            StairMedSettings.Instance.OnSettingChanged += (HashSet<string> changes) =>
            {
                if (CareSettings.Overlaps(changes))
                {
                    _fftSize = StairMedSettings.Instance.FFTSize;
                }
            };
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> CareSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.FFTSize),
            nameof(StairMedSettings.Instance.SpectrumTimeScaleS),
        };

        /// <summary>
        /// 监听的通道
        /// </summary>
        private readonly HashSet<int> _careInputChannels = new HashSet<int>();

        /// <summary>
        /// FIFO顺序化
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, PSDBuffer> _psdBufferDict = new Dictionary<int, PSDBuffer>();

        /// <summary>
        /// 更新数据:channel, samplerate, fftsize, psds
        /// </summary>
        public event Action<int, int, int, List<PSDInfo>> HandleNewPSDs;

        /// <summary>
        /// 
        /// </summary>
        private int _fftSize = -1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            //监听事件
            if (objs != null && objs.Length == 2)
            {
                HandleResigsterEvent((bool)objs[0], (int)objs[1]);
                return;
            }

            //处理新接收的数据块
            if (objs != null && objs.Length == 1 && objs[0] is Chunk chunk && _careInputChannels.Count > 0)
            {
                HandleNewChunk(chunk);
                return;
            }

            //开始新的采集，清空所有数据
            _psdBufferDict.Clear();
        }

        /// <summary>
        /// 处理新接收的数据块
        /// </summary>
        /// <param name="chunk"></param>
        private void HandleNewChunk(Chunk chunk)
        {
            //判断chunk块是否有效或是否有人监听
            if (chunk.OneChannelSamples == 0 || chunk.Wfps == null || _careInputChannels.Count <= 0)
            {
                return;
            }

            //处理所有关注的通道
            foreach (var channel in _careInputChannels)
            {
                //该通道的数据在chunk中的位置
                var channelIndexInChunk = chunk.InputChannelInReports.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //未创建该通道的缓存，则创建
                if (!_psdBufferDict.ContainsKey(channel))
                {
                    _psdBufferDict[channel] = new PSDBuffer { };
                }

                //拿到
                var psdBuffer = _psdBufferDict[channel];

                //
                psdBuffer.FFTSize = _fftSize;
                psdBuffer.SampleRate = chunk.SampleRate;

                //构建wfp数据缓存
                var bufferLen = chunk.OneChannelSamples + psdBuffer.FFTSize;
                if (psdBuffer.WfpBuffer == null || (psdBuffer.WfpBuffer.Length != bufferLen) || (psdBuffer.BufferedLength + chunk.OneChannelSamples >= psdBuffer.WfpBuffer.Length))
                {
                    if (psdBuffer.WfpBuffer != null)
                    {
                        psdBuffer.WfpBuffer.Dispose();
                    }
                    psdBuffer.WfpBuffer = new PooledArray<float>(bufferLen);
                    psdBuffer.BufferedLength = 0;
                }

                //将wfp数据缓存起来
                var begin = channelIndexInChunk * chunk.OneChannelSamples;
                System.Array.Copy(chunk.Wfps.Array, begin, psdBuffer.WfpBuffer.Array, psdBuffer.BufferedLength, chunk.OneChannelSamples);
                psdBuffer.BufferedLength += chunk.OneChannelSamples;

                //数据长度足够，执行fft
                if (psdBuffer.BufferedLength >= psdBuffer.FFTSize)
                {
                    //判断是否需要更换fft
                    if (psdBuffer.FFT == null || psdBuffer.FFT.Length != psdBuffer.FFTSize)
                    {
                        psdBuffer.FFT = new FastFourierTransform(psdBuffer.SampleRate, psdBuffer.FFTSize);
                    }

                    //
                    var psds = new List<PSDInfo>();

                    //设置执行fft的输入数据buffer
                    var fftInput = new PooledArray<float>(psdBuffer.FFTSize);

                    //
                    var costCount = 0;
                    while (psdBuffer.BufferedLength - costCount >= psdBuffer.FFTSize)
                    {
                        //数据复制进fft的嘴里
                        System.Array.Copy(psdBuffer.WfpBuffer.Array, costCount, fftInput.Array, 0, psdBuffer.FFTSize);
                        costCount += psdBuffer.FFTSize / 2;

                        //计算psd
                        var logSqrtPSD = psdBuffer.FFT.logSqrtPowerSpectralDensity(fftInput.Array); //注意logSqrtPSD为fft成员变量，多次执行fft时，返回的logSqrtPSD的引用相同

                        //结果复制出副本
                        var targetPsd = new float[logSqrtPSD.Length];
                        System.Array.Copy(logSqrtPSD, 0, targetPsd, 0, logSqrtPSD.Length);

                        //保存
                        psds.Add(new PSDInfo
                        {
                            PSD = targetPsd,
                            PSDIndex = psdBuffer.PSDIndex++,
                        });
                    }

                    //释放
                    fftInput.Dispose();

                    //移动wfp的缓存数据，向index0对齐
                    var left = psdBuffer.BufferedLength - costCount;
                    if (left > 0)
                    {
                        System.Array.Copy(psdBuffer.WfpBuffer.Array, costCount, psdBuffer.WfpBuffer.Array, 0, left);
                    }
                    psdBuffer.BufferedLength = left;

                    //有更新才会重新
                    HandleNewPSDs?.Invoke(channel, psdBuffer.SampleRate, psdBuffer.FFTSize, psds);
                }
            }
        }

        #region 监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isRegister"></param>
        /// <param name="channel"></param>
        private void HandleResigsterEvent(bool isRegister, int channel)
        {
            if (isRegister)
            {
                _careInputChannels.Add(channel);
            }
            else
            {
                _careInputChannels.Remove(channel);
                _psdBufferDict.Remove(channel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RegisterSpectrumChannel(int channel)
        {
            _fifo.Add(true, channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void UnregisterSpectrumChannel(int channel)
        {
            _fifo.Add(false, channel);
        }

        #endregion
    }
}
