﻿using StairMed.Array;
using StairMed.Tools;

namespace StairMed.DataCenter.Centers.Spectrum
{
    /// <summary>
    /// 
    /// </summary>
    internal class PSDBuffer
    {
        //
        internal int FFTSize { get; set; }
        internal int SampleRate { get; set; }

        //
        internal FastFourierTransform FFT { get; set; }

        //
        internal PooledArray<float> WfpBuffer { get; set; }
        internal int BufferedLength { get; set; }

        //
        internal long PSDIndex = 0;
    }
}
