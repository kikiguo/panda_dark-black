﻿namespace StairMed.DataCenter.Centers.Spectrum
{
    /// <summary>
    /// 
    /// </summary>
    public class PSDInfo
    {
        public float[] PSD { get; set; }
        public long PSDIndex { get; set; }
    }
}
