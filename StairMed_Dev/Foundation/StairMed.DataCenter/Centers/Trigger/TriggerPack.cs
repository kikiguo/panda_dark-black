﻿using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.Trigger
{
    /// <summary>
    /// 
    /// </summary>
    public class TriggerPack
    {
        /// <summary>
        /// 
        /// </summary>
        public int SampleRate = 30 * 1000;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, List<TriggerEdge>> Triggers = new Dictionary<int, List<TriggerEdge>>();
    }
}



