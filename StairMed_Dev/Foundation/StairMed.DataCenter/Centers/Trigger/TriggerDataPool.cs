﻿using System;

namespace StairMed.DataCenter.Centers.Trigger
{
    /// <summary>
    /// 
    /// </summary>
    public class TriggerDataPool
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static TriggerDataPool Instance = new TriggerDataPool();
        }
        public static TriggerDataPool Instance => Helper.Instance;
        private TriggerDataPool() { }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public event Action<TriggerPack> NewTriggerPackReceived;

        /// <summary>
        /// 
        /// </summary>
        public void BufferTrigger(TriggerPack triggerPack)
        {
            if (triggerPack != null && triggerPack.Triggers != null && triggerPack.Triggers.Count > 0)
            {
                NewTriggerPackReceived?.Invoke(triggerPack);
            }
        }
    }
}
