﻿namespace StairMed.DataCenter.Centers.Trigger
{
    /// <summary>
    /// 
    /// </summary>
    public class TriggerEdge
    {
        /// <summary>
        /// 时间轴偏移
        /// </summary>
        public long Index { get; set; }

        /// <summary>
        /// 切换至高电平 or 切换至低电平
        /// </summary>
        public bool ToHigh { get; set; }
    }
}
