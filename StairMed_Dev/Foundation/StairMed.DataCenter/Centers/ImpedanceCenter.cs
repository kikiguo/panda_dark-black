﻿using StairMed.ChipConst.Intan;
using System;
using System.Collections.Generic;

namespace StairMed.DataCenter.Centers
{
    /// <summary>
    /// 
    /// </summary>
    public class ImpedanceCenter
    {
        #region 单例

        private static readonly ImpedanceCenter _instance = new ImpedanceCenter();
        public static ImpedanceCenter Instance => _instance;
        private ImpedanceCenter() { }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public event Action<long, int, double, double> OnDeviceChannelImpedanceChanged;

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<long, Dictionary<int, IntanImpedanceEntity>> _impedances = new Dictionary<long, Dictionary<int, IntanImpedanceEntity>>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicsChannel"></param>
        /// <param name="magnitude"></param>
        /// <param name="phase"></param>
        public void SetImpedance(long Id, int physicsChannel, double magnitude, double phase)
        {
            if (!_impedances.ContainsKey(Id))
            {
                _impedances[Id] = new Dictionary<int, IntanImpedanceEntity>();
            }

            //
            _impedances[Id][physicsChannel] = new IntanImpedanceEntity(magnitude, phase);
            OnDeviceChannelImpedanceChanged?.Invoke(Id, physicsChannel, magnitude, phase);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="physicsChannel"></param>
        public void GetImpedance(long Id, int physicsChannel, out double magnitude, out double phase)
        {
            magnitude = -1;
            phase = 0.0;

            //
            if (_impedances.ContainsKey(Id) && _impedances[Id].ContainsKey(physicsChannel))
            {
                magnitude = _impedances[Id][physicsChannel].Magnitude;
                phase = _impedances[Id][physicsChannel].Phase;
            }
        }
    }
}
