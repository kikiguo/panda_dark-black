﻿using System.Collections.Generic;

namespace StairMed.DataCenter.Centers.ISI
{
    /// <summary>
    /// 
    /// </summary>
    public class ISIInfo
    {
        public int Channel { get; set; }

        //
        public int BinSizeMs { get; set; }
        public int TimeSpanMs { get; set; }

        //
        public double Mean { get; set; }
        public double ISIFreq { get; set; }
        public double StdDev { get; set; }
        public int ISICount { get; set; }

        //
        public List<float> Histograms { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal long PreviousSpikeIndex { get; set; }
        internal Dictionary<int,int> IntervalDict { get; set; }
    }
}
