﻿using StairMed.AsyncFIFO;
using StairMed.Core.Settings;
using StairMed.DataCenter.Recorder;
using StairMed.Entity.DataPools;
using StairMed.Entity.Infos.Bases;
using StairMed.Enum;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;

namespace StairMed.DataCenter
{
    /// <summary>
    /// 
    /// </summary>
    public class NeuralDataCollector : ViewModelBase
    {
        private const string TAG = nameof(NeuralDataCollector);
        private const int RealtimeSampleRateMS = 5000; //计算实时采样率时的统计时长

        /// <summary>
        /// 
        /// </summary>
        private const int BufferedChunkTimeS = 6;

        /// <summary>
        /// 缓存起来，方便后面执行dispose
        /// </summary>
        private readonly Queue<Chunk> _bufferedChunk = new Queue<Chunk>();

        #region 单例
        private class Helper
        {
            internal static NeuralDataCollector _instance = new NeuralDataCollector();
            static Helper() { }
        }
        public static NeuralDataCollector Instance => Helper._instance;
        #endregion

        /// <summary>
        /// 采集的设备数据被 xpu 计算后的数据chunk
        /// </summary>
        public event Action<Chunk> NewChunkReceived;

        /// <summary>
        /// 
        /// </summary>
        public event Action<bool> RunningStateChanged;

        /// <summary>
        /// 
        /// </summary>
        private IAsyncFIFO _ntplHelper = null;

        #region 采集状态信息

        /// <summary>
        /// 是否正在采集
        /// </summary>
        private bool _isCollecting = false;
        public bool IsCollecting
        {
            get { return _isCollecting; }
            set
            {
                if (_isCollecting != value)
                {
                    Set(ref _isCollecting, value);
                    RunningStateChanged?.Invoke(value);
                }
                StairMedSettings.Instance.IsCollecting = value;
                StairMedSettings.Instance.RollOffsetTimeMs = 0;
            }
        }

        private bool _isPlaybacking = false;
    
        public bool IsPlaybacking
        {
            get { return _isPlaybacking; }
            set 
            {
                if (_isPlaybacking != value)
                    Set(ref _isPlaybacking, value);
            }
        }


        /// <summary>
        /// 是否已收到第一包数据
        /// </summary>
        private bool _collectStarted = false;


        private bool _PowerCharging = false;

        public bool PowerCharging
        {
            get { return _PowerCharging; }
            set { Set(ref _PowerCharging, value); }
        }

        /// <summary>
        /// 记录第一包数据的时间
        /// </summary>
        private DateTime _collectStartTime = DateTime.Now.AddDays(-10);
        public DateTime CollectStartTime
        {
            get { return _collectStartTime; }
            set { Set(ref _collectStartTime, value); }
        }

        /// <summary>
        /// 已录制时长
        /// </summary>
        private TimeSpan _collectedTime = TimeSpan.Zero;
        public TimeSpan CollectedTime
        {
            get { return _collectedTime; }
            set { Set(ref _collectedTime, value); }
        }

        /// <summary>
        /// 数据的实际采样率
        /// </summary>
        private int _realtimeSampleRate = 0;
        private DateTime _lastUpdateTime = DateTime.Now;
        private readonly Queue<DateTime> _recvdTime = new Queue<DateTime>();
        public int RealtimeSampleRate
        {
            get { return _realtimeSampleRate; }
            set { Set(ref _realtimeSampleRate, value); }
        }

        /// <summary>
        /// 已接收的数据长度
        /// </summary>
        private long _recvdLength = 0;
        public long RecvdLength
        {
            get { return _recvdLength; }
            set { Set(ref _recvdLength, value); }
        }

        #endregion


        #region 采集数据所使用的参数

        /// <summary>
        /// 正在进行采集的采集参数
        /// </summary>
        private CollectParamInfo _collectingParam = null;

        /// <summary>
        /// 采样通道个数
        /// </summary>
        private int _channelCount = 0;

        /// <summary>
        /// 采样率
        /// </summary>
        private int _sampleRate = 0;

        /// <summary>
        /// 正在采样的所有通道
        /// </summary>
        private List<int> _inputChannelInReports = new List<int>();

        /// <summary>
        /// 正在进行采集的设备id
        /// </summary>
        private long _collectingDeviceId = -1;

        #endregion


        /// <summary>
        /// 
        /// </summary>
        public NeuralDataCollector()
        {
        }

        /// <summary>
        /// 启动新的采样：记录将要采集所使用的采样参数、清理上次的采集数据
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="collectParam"></param>
        public void StartNewCollect(CollectParamInfo collectParam)
        {
            CollectedTime = TimeSpan.Zero;
            _collectingParam = collectParam;
            _collectingDeviceId = collectParam.DeviceId;
            _channelCount = collectParam.CollectChannelCount;
            _sampleRate = collectParam.SampleRate;
            _inputChannelInReports = collectParam.InputChannelInReports;

            //
            StairMedSettings.Instance.HalfSampleRate = _sampleRate / 2;

            //
            _collectStarted = false;
            RecvdLength = 0;
            RealtimeSampleRate = 0;

            //
            if (AdvanceSettings.Instance.BindCollectRecordButton)
            {
                NeuralDataRecorder.Instance.StartRecording();
            }

            //
            IsCollecting = true;
        }

        /// <summary>
        /// 停止采集
        /// </summary>
        public void StopCollect()
        {
            _collectStarted = false;
            IsCollecting = false; _recvdTime.Clear();
        }



        #region 获取采集数据所使用的参数

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collectParam"></param>
        public void InitCollectParamInfo(CollectParamInfo collectParam)
        {
            if (_collectingParam == null)
            {
                _collectingParam = collectParam;
            }
        }

        /// <summary>
        /// 获取采样参数
        /// </summary>
        /// <returns></returns>
        public CollectParamInfo GetCollectingDataParam()
        {
            return _collectingParam;
        }

        /// <summary>
        /// 获取上报数据中：字节顺序对应的input顺序
        /// </summary>
        /// <returns></returns>
        public List<int> GetInputChannelInReports()
        {
            return _inputChannelInReports;
        }

        /// <summary>
        /// 获取采样通道个数
        /// </summary>
        /// <returns></returns>
        public int GetCollectingChannelsCount()
        {
            return _channelCount;
        }

        /// <summary>
        /// 获取采样率
        /// </summary>
        /// <returns></returns>
        public int GetCollectingSampleRate()
        {
            return _sampleRate;
        }

        /// <summary>
        /// 获取数据的起始时间
        /// </summary>
        /// <param name="deviceId"></param>
        public DateTime GetCollectStartTime()
        {
            return _collectStartTime;
        }

        /// <summary>
        /// 
        /// </summary>
        public CollectMode GetCollectType()
        {
            return _collectingParam.CollectType;
        }

        #endregion




        private Chunk _lastChunk = null;

        /// <summary>
        /// 缓存数据
        /// </summary>
        /// <param name="chunk"></param>
        public void BufferChunk(Chunk chunk)
        {
            if (!IsCollecting)
            {
                return;
            }

            if (_ntplHelper == null)
            {
                _ntplHelper = new BlockFIFO
                {
                    Name = nameof(NeuralDataCollector),
                    ConsumerAction = (object[] objs) =>
                    {
                        //给数据包赋值index
                        var chunk = (Chunk)objs[0];
                        chunk.DataIndex = RecvdLength;
                        RecvdLength += chunk.OneChannelSamples;
                        chunk.TotalRecvdSampleOfEachChannel = RecvdLength;

                        //计算实时采样率
                        UpdateRealtimeSampleRate(chunk.OneChannelSamples);

                        //
                        NewChunkReceived?.Invoke(chunk);

                        //
                        BufferChunkForDispose(chunk);
                    }
                };
            }

            //
            if (!_collectStarted)
            {
                CollectStartTime = DateTime.Now;
                _collectStarted = true;
            }

            //
            _ntplHelper.Add(chunk);
            _lastChunk = chunk;

            //            
            CollectedTime = DateTime.Now - CollectStartTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void BufferChunkForDispose(Chunk chunk)
        {
            _bufferedChunk.Enqueue(chunk);
            var maxBufferedChunk = (chunk.SampleRate * BufferedChunkTimeS + chunk.OneChannelSamples - 1) / chunk.OneChannelSamples;
            while (_bufferedChunk.Count > maxBufferedChunk)
            {
                var old = _bufferedChunk.Dequeue();
                old.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oneChannelSampleCount"></param>
        private void UpdateRealtimeSampleRate(int oneChannelSampleCount)
        {
            if (oneChannelSampleCount <= 0)
            {
                return;
            }

            //
            var now = DateTime.Now;
            _recvdTime.Enqueue(now);

            //
            if ((now - _lastUpdateTime).TotalMilliseconds < 1000)
            {
                return;
            }
            _lastUpdateTime = now;

            //
            while (true)
            {
                if (_recvdTime.Count <= 0)
                {
                    break;
                }

                //
                if ((now - _recvdTime.Peek()).TotalMilliseconds < RealtimeSampleRateMS)
                {
                    break;
                }

                //
                _recvdTime.Dequeue();
            }

            //
            if (CollectedTime.TotalMilliseconds < RealtimeSampleRateMS)
            {
                RealtimeSampleRate = _sampleRate;
                return;
            }

            //
            if (_recvdTime.Count >= 2)
            {
                var bufferedTimeMs = (now - _recvdTime.Peek()).TotalMilliseconds;
                if (bufferedTimeMs != 0)
                {
                    RealtimeSampleRate = Convert.ToInt32((_recvdTime.Count - 1) * oneChannelSampleCount * 1000 / bufferedTimeMs);
                }
            }
        }

    }
}
