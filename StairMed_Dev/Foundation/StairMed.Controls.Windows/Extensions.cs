﻿using System;
using System.Windows;

namespace StairMed.Controls.Windows
{
    /// <summary>
    /// 
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        public static void DelayClose(this Window window)
        {
            try
            {
                window.Dispatcher.BeginInvoke(new Action(() =>
                {
                    window.Close();
                }));
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        public static void BindingDragMove(this Window window)
        {
            window.MouseDown += (object sender, System.Windows.Input.MouseButtonEventArgs e) =>
            {
                if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
                {
                    try
                    {
                        window.DragMove();
                    }
                    catch { }
                }
            };
        }

    }
}
