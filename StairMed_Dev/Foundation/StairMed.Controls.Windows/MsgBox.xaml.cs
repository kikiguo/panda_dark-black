using System.Windows;

namespace StairMed.Controls.Windows
{
    /// <summary>
    /// MsgBox.xaml 的交互逻辑
    /// </summary>
    public partial class MsgBox
    {
        private MessageBoxResult _result = MessageBoxResult.None;

        public string Yes { get; set; } = string.Empty;
        public string No { get; set; } = string.Empty;
        public string OK { get; set; } = string.Empty;
        public string Cancel { get; set; } = string.Empty;


        /// <summary>
        /// 
        /// </summary>
        private MsgBox()
        {
            InitializeComponent();
            this.BindingDragMove();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateBtnTxt()
        {
            if (!string.IsNullOrWhiteSpace(Yes))
            {
                btnYes.Content = Yes;
            }
            if (!string.IsNullOrWhiteSpace(No))
            {
                btnNo.Content = No;
            }
            if (!string.IsNullOrWhiteSpace(OK))
            {
                btnOk.Content = OK;
            }
            if (!string.IsNullOrWhiteSpace(Cancel))
            {
                btnCancel.Content = Cancel;
            }
        }




        #region 对外接口

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static MessageBoxResult Info(string text, string caption = "", Window? owner = null)
        {
            return Show(text, caption, owner, MsgBoxButton.OK, MsgBoxType.Information);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static MessageBoxResult AskPeaceful(string text, string caption = "", Window? owner = null)
        {
            return Show(text, caption, owner, MsgBoxButton.YesNo, MsgBoxType.Information);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static MessageBoxResult AskAnxious(string text, string caption = "", Window? owner = null)
        {
            return Show(text, caption, owner, MsgBoxButton.YesNo, MsgBoxType.Warning);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static MessageBoxResult Warn(string text, string caption = "", Window? owner = null)
        {
            return Show(text, caption, owner, MsgBoxButton.OK, MsgBoxType.Warning);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static MessageBoxResult Error(string text, string caption = "", Window? owner = null)
        {
            return Show(text, caption, owner, MsgBoxButton.OK, MsgBoxType.Error);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="caption"></param>
        /// <param name="owner"></param>
        /// <param name="boxButton"></param>
        /// <param name="boxType"></param>
        /// <returns></returns>
        private static MessageBoxResult Show(string text, string caption, Window? owner, MsgBoxButton boxButton, MsgBoxType boxType)
        {
            if (owner == null)
            {
                owner = Application.Current.MainWindow;
            }

            //
            var msgbox = new MsgBox
            {
                txtMsg = { Text = text },
                Title = caption,
            };
            msgbox.Owner = owner;
            SetVisibilityOfButtons(msgbox, boxButton);
            SetImageOfMessageBox(msgbox, boxType);
            msgbox.UpdateBtnTxt();
            msgbox.WindowStartupLocation = owner == null ? WindowStartupLocation.CenterScreen : WindowStartupLocation.CenterOwner;
            msgbox.ShowDialog();
            return msgbox._result;
        }

        #endregion




        #region

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msgbox"></param>
        /// <param name="button"></param>
        private static void SetVisibilityOfButtons(MsgBox msgbox, MsgBoxButton button)
        {
            switch (button)
            {
                case MsgBoxButton.OK:
                    {
                        msgbox.btnCancel.Visibility = Visibility.Collapsed;
                        msgbox.btnNo.Visibility = Visibility.Collapsed;
                        msgbox.btnYes.Visibility = Visibility.Collapsed;
                        msgbox.btnOk.IsDefault = true;
                    }
                    break;
                case MsgBoxButton.OKCancel:
                    {
                        msgbox.btnNo.Visibility = Visibility.Collapsed;
                        msgbox.btnYes.Visibility = Visibility.Collapsed;
                        msgbox.btnOk.IsDefault = true;
                    }
                    break;
                case MsgBoxButton.YesNo:
                    {
                        msgbox.btnOk.Visibility = Visibility.Collapsed;
                        msgbox.btnCancel.Visibility = Visibility.Collapsed;
                        msgbox.btnYes.IsDefault = true;
                    }
                    break;
                case MsgBoxButton.YesNoCancel:
                    {
                        msgbox.btnOk.Visibility = Visibility.Collapsed;
                        msgbox.btnYes.IsDefault = true;
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msgbox"></param>
        /// <param name="image"></param>
        private static void SetImageOfMessageBox(MsgBox msgbox, MsgBoxType image)
        {
            switch (image)
            {
                case MsgBoxType.Warning:
                    {
                        msgbox.normal.Visibility = Visibility.Collapsed;
                        msgbox.warn.Visibility = Visibility.Visible;
                        msgbox.error.Visibility = Visibility.Collapsed;
                        msgbox.imgok.Visibility = Visibility.Collapsed;
                        msgbox.imginfo.Visibility = Visibility.Visible;
                       
                    }
                    break;
                case MsgBoxType.Error:
                    {
                        msgbox.normal.Visibility = Visibility.Collapsed;
                        msgbox.warn.Visibility = Visibility.Collapsed;
                        msgbox.error.Visibility = Visibility.Visible;
                    }
                    break;
                default:
                    {
                        msgbox.normal.Visibility = Visibility.Visible;
                        msgbox.error.Visibility = Visibility.Collapsed;
                        msgbox.warn.Visibility = Visibility.Collapsed;
                        msgbox.imgok.Visibility = Visibility.Collapsed;
                        msgbox.imginfo.Visibility = Visibility.Visible;
                    }
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender == btnOk)
                _result = MessageBoxResult.OK;
            else if (sender == btnYes)
                _result = MessageBoxResult.Yes;
            else if (sender == btnNo)
                _result = MessageBoxResult.No;
            else if (sender == btnCancel)
                _result = MessageBoxResult.Cancel;
            else
                _result = MessageBoxResult.None;

            //
            this.Close();
        }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    public enum MsgBoxType
    {
        None = 0,
        Information = 0,
        Warning = 1,
        Error = 2,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MsgBoxButton
    {
        OK = 0,
        OKCancel = 1,
        YesNoCancel = 3,
        YesNo = 4
    }


}
