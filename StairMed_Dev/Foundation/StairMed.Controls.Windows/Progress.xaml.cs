﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.Controls.Windows
{
    /// <summary>
    /// Progress.xaml 的交互逻辑
    /// </summary>
    public partial class Progress
    {
        /// <summary>
        /// 
        /// </summary>
        private string TilteTxt { get; set; } = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public Progress()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="task"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        internal void Popup(Func<double> progress, Action task, Action? callback)
        {
            if (string.IsNullOrWhiteSpace(TilteTxt))
            {
                txtTitle.Visibility = Visibility.Collapsed;
            }
            else
            {
                txtTitle.Text = TilteTxt;
                txtTitle.Visibility = Visibility.Visible;
            }

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                bool stoped = false;
                Task.Run(() =>
                {
                    try
                    {
                        task?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine($"{nameof(Progress)}, {nameof(Popup)}:{ex}");
                    }

                    //
                    stoped = true;
                    Thread.Sleep(100);

                    //
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.Close();
                    }));
                });

                //
                Task.Run(() =>
                {
                    while (!stoped)
                    {
                        Thread.Sleep(100);

                        //
                        var p = progress.Invoke();
                        this.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            progressBar.Value = p;
                        }));
                    }
                });
            };

            //
            this.Closed += (object? sender, EventArgs e) =>
            {
                callback?.Invoke();
            };

            //
            this.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="task">任务：在非UI线程中执行</param>
        /// <param name="callbackUI">任务完成的回调：在UI线程中执行</param>
        /// <param name="owner">窗口拥有者</param>
        public static void Show(string title, Func<double> progress, Action task, Action? callbackUI = null, Window? owner = null)
        {
            if (owner == null)
            {
                owner = Application.Current.MainWindow;
            }

            //
            var wnd = new Progress
            {
                Owner = owner,
                WindowStartupLocation = owner == null ? WindowStartupLocation.CenterScreen : WindowStartupLocation.CenterOwner,
                TilteTxt = title,
            };
            wnd.Popup(progress, task, callbackUI);
        }
    }
}
