﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.Controls.Windows
{
    /// <summary>
    /// Loading.xaml 的交互逻辑
    /// </summary>
    public partial class Loading
    {
        /// <summary>
        /// 
        /// </summary>
        public Loading()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        internal bool? ShowDialog(Action task, Action? callback)
        {
            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                Task.Run(() =>
                {
                    try
                    {
                        task?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine($"{nameof(Loading)}, {nameof(ShowDialog)}:{ex}");
                    }

                    //
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.Close();
                    }));
                });
            };

            //
            this.Closed += (object? sender, EventArgs e) =>
            {
                callback?.Invoke();
            };

            //
            return this.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task">任务：在非UI线程中执行</param>
        /// <param name="callbackUI">任务完成的回调：在UI线程中执行</param>
        /// <param name="owner">窗口拥有者</param>
        public static void Show(Action task, Action? callbackUI = null, Window? owner = null)
        {
            if (owner == null)
            {
                owner = Application.Current.MainWindow;
            }

            //
            var loading = new Loading
            {
                Owner = owner,
                WindowStartupLocation = owner == null ? WindowStartupLocation.CenterScreen : WindowStartupLocation.CenterOwner
            };
            loading.ShowDialog(task, callbackUI);
        }
    }
}
