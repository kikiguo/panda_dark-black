﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Windows
{
    /// <summary>
    /// Toast.xaml 的交互逻辑
    /// </summary>
    public partial class Toast
    {
        public static readonly string TAG = nameof(Toast);

        /// <summary>
        /// 
        /// </summary>
        internal Toast(string message, int showMS, Color color)
        {
            InitializeComponent();

            //
            txtMsg.Text = message;
            border.Background = new SolidColorBrush(color);

            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                //
                Task.Run(() =>
                {
                    Thread.Sleep(Math.Min(60 * 1000, Math.Max(1000, showMS)));
                    Dispatcher.BeginInvoke(new Action(Close));
                });
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DelayClose();
        }

        #region 三种Toast

        private static readonly Color ErrorColor = Color.FromRgb(0xFF, 0x62, 0x5D);
        private static readonly Color WarnColor = Color.FromRgb(0xff, 0x93, 0);
        private static readonly Color InfoColor = Color.FromRgb(0x2F, 0x29, 0x29);

        /// <summary>
        /// 
        /// </summary>
        public static void Error(string message, Window? owner = null, int showMS = 3500)
        {
            Display(message, ErrorColor, showMS, owner);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Warn(string message, Window? owner = null, int showMS = 3500)
        {
            Display(message, WarnColor, showMS, owner);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Info(string message, Window? owner = null, int showMS = 2000)
        {
            Display(message, InfoColor, showMS, owner);
        }

        /// <summary>
        /// 
        /// </summary>
        private static void Display(string message, Color color, int showMS = 1000, Window? owner = null)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                return;
            }

            //
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (owner == null)
                {
                    owner = Application.Current.MainWindow;
                }

                //
                new Toast(message, showMS, color)
                {
                    Owner = owner,
                    WindowStartupLocation = owner == null ? WindowStartupLocation.CenterScreen : WindowStartupLocation.CenterOwner
                }.Show();
            }));
        }

        #endregion
    }
}
