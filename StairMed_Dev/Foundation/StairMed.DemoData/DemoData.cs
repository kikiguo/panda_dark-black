﻿namespace StairMed.DemoData
{
    /// <summary>
    /// 
    /// </summary>
    public static class DemoData
    {
        /// <summary>
        /// 
        /// </summary>
        public static byte[] Data = new byte[] { 0x00, 0x80, 0x00, 0x80 };

        /// <summary>
        /// 
        /// </summary>
        static DemoData()
        {
            try
            {
                var assembly = System.Reflection.Assembly.GetExecutingAssembly();
                var demoName = assembly.GetName().Name + ".amp-D-023.dat";
                var demoData = assembly.GetManifestResourceStream(demoName);

                //
                Data = new byte[demoData.Length];
                demoData.Read(Data, 0, (int)demoData.Length);

                //
                for (int i = 1; i < Data.Length; i += 2)
                {
                    Data[i] = (byte)(Data[i] ^ 0x80);
                }
            }
            catch { }
        }
    }
}
