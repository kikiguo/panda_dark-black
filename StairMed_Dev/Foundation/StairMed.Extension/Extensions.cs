﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.Extension
{
    /// <summary>
    /// 
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// 将字节流转文本，用于展示字节流内容
        /// </summary>
        /// <param name="byteArray"></param>
        /// <param name="maxCount"></param>
        /// <returns></returns>
        public static string ToStrings(this IEnumerable<byte> bytes, int maxCount = 512, bool newline = false)
        {
            var split = newline ? Environment.NewLine : "  ";
            var builder = new StringBuilder();
            int count = 0;
            foreach (var b in bytes)
            {
                count++;
                builder.Append(string.Format("{0:X2} ", b));//拼接
                if (count % 8 == 0)
                {
                    builder.Append(split);//拼接
                }
                if (count >= maxCount)
                {
                    break;
                }
            }
            return builder.ToString().TrimEnd();
        }
    }
}
