namespace StairMed.Extension
{
    /// <summary>
    /// 
    /// </summary>
    public static class ObjExts
    {
        /// <summary>
        /// 深拷贝
        /// </summary>
        /// <param name="objdst"></param>
        /// <param name="objSrc"></param>
        public static void DeepCopy(this object objdst, object objSrc)
        {
            var type = objdst.GetType();
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (prop.CanWrite && prop.CanRead)
                {
                    prop.SetValue(objdst, prop.GetValue(objSrc));
                }
            }
            return;
        }
    }
}
