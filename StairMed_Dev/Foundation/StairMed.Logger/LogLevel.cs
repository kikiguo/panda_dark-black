﻿namespace StairMed.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogLevel
    {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }
}
