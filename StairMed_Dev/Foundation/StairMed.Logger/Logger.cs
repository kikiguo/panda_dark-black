﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace StairMed.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public class Logger : ILogger
    {
        /// <summary>
        /// 
        /// </summary>
        private ILoggerOutput _output = new StairMedOutput();

        /// <summary>
        /// 
        /// </summary>
        public LogLevel _logLevel = LogLevel.Trace;

        /// <summary>
        /// 
        /// </summary>
        public LogLevel LogLevel
        {
            set
            {
                _logLevel = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static readonly Dictionary<LogLevel, string> _logLevelDict = new Dictionary<LogLevel, string>();

        /// <summary>
        /// 
        /// </summary>
        public ILoggerOutput Output
        {
            set
            {
                _output = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        static Logger()
        {
            for (LogLevel i = LogLevel.Trace; i <= LogLevel.Fatal; i++)
            {
                _logLevelDict[i] = i.ToString().Substring(0, 1);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Logger()
        {

        }

        #region 接口实现

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogT(string format, params object[] args)
        {
            Log(LogLevel.Trace, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogD(string format, params object[] args)
        {
            Log(LogLevel.Debug, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogI(string format, params object[] args)
        {
            Log(LogLevel.Info, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogW(string format, params object[] args)
        {
            Log(LogLevel.Warn, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogE(string format, params object[] args)
        {
            Log(LogLevel.Error, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void LogF(string format, params object[] args)
        {
            Log(LogLevel.Fatal, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="desc"></param>
        /// <param name="ex"></param>
        public void Exp(string tag, Exception ex)
        {
            Log(LogLevel.Fatal, $"{tag}, EXCEPTION, {ex}, xxxxxxxxxx");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        public void Writeline(string str)
        {
            Debug.WriteLine(str);
        }

        #endregion



        /// <summary>
        /// 
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="format"></param>
        /// <param name="formatParameters"></param>
        private void Log(LogLevel logLevel, string format, params object[] formatParameters)
        {
#if DEBUG
            

            if (logLevel < _logLevel || _output == null)
            {
                return;
            }

            //
            var log = FormatLog(logLevel, format, formatParameters);

            //
            try
            {
                _output?.Output($"{log}");
            }
            catch
            {
                Debug.WriteLine($"{log.Trim()}");
            }
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="format"></param>
        /// <param name="formatParameters"></param>
        /// <returns></returns>
        private string FormatLog(LogLevel logLevel, string format, object[] formatParameters)
        {
            string log;
            var msg = string.Empty;
            if (formatParameters != null && formatParameters.Length > 0)
            {
                try
                {
                    msg = string.Format(format, formatParameters);
                }
                catch
                {
                    msg = "format err:" + format + "-" + string.Join(",", formatParameters);
                }
            }
            else
            {
                msg = format;
            }

            //
            log = $"[{_logLevelDict[logLevel]}], [{Thread.CurrentThread.ManagedThreadId:D3}], {DateTime.Now:HH:mm:ss.fff}, {msg}";
            return log;
        }

    }
}
