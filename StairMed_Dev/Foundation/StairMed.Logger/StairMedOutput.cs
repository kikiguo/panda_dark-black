﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace StairMed.Logger
{
    /// <summary>
    ///
    /// </summary>
    public class StairMedOutput : ILoggerOutput
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly string LogFolderParent = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

        /// <summary>
        /// 软件退出
        /// </summary>
        public static bool ApplicationExit = false;

        /// <summary>
        /// 是否写入到控制台
        /// </summary>
        public static bool IsWriteToConsole = true;

        /// <summary>
        /// 
        /// </summary>
        public StairMedOutput()
        {
            new Thread(() =>
            {
                SaveLogLoop();
            })
            {
                IsBackground = false,
            }.Start();
        }

        /// <summary>
        /// 日志文件名，默认“UI”
        /// </summary>
        public string FileName { get; set; } = "App";

        /// <summary>
        /// 单文件大小限制
        /// </summary>
        public long FileRollSize { get; set; } = 1024 * 1024 * 16;

        //当前日志文件信息
        private string _curLogFile = string.Empty;
        private long _curFileSize = 0;
        private DateTime _curFileDate = DateTime.Now;
        private int _rollIndex = 0;

        //输出日志相关
        private readonly List<string> _logAList = new List<string>();
        private readonly List<string> _logBList = new List<string>();
        private bool _pingpang = true; //true:A, false:B

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Output(string message)
        {
            try
            {
                //异步线程输出
                var logs = _pingpang ? _logAList : _logBList;
                logs.Add(message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }


        #region 日志输出实际执行动作

        /// <summary>
        /// 
        /// </summary>
        private void SaveLogLoop()
        {
            Thread.Sleep(500);
            Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
            while (true)
            {
                if (ApplicationExit)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        var logs = _pingpang ? _logAList : _logBList;
                        _pingpang = !_pingpang;
                        Thread.Sleep(100); //等待以防止多线程同时处理某个list

                        //写入日志
                        SaveLog(logs);
                        logs.Clear();
                    }
                    break;
                }
                else
                {
                    var logs = _pingpang ? _logAList : _logBList;
                    _pingpang = !_pingpang;
                    Thread.Sleep(1000); //等待以防止多线程同时处理某个list

                    //写入日志
                    SaveLog(logs);
                    logs.Clear();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logs"></param>
        private void SaveLog(List<string> logs)
        {
            try
            {
                if (logs != null && logs.Count > 0)
                {
                    var builder = new StringBuilder();
                    foreach (var msg in logs)
                    {
                        builder.Append(msg + Environment.NewLine);
                    }
                    SaveToFile(builder.ToString());
#if DEBUG
                    if (IsWriteToConsole)
                    {
                        Debug.WriteLine($"{builder.ToString().Trim()}");
                    }
#endif
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Thread.Sleep(5000);
            }
        }

        /// <summary>
        /// 保存文件，带时间判断，大小判断，自动更换文件名
        /// </summary>
        /// <param name="log"></param>
        private void SaveToFile(string log)
        {
            try
            {
                //未指定日志文件  或  文件过大  或  日期变化， 重新创建文件
                if (string.IsNullOrWhiteSpace(_curLogFile) || _curFileSize >= FileRollSize || DateTime.Now.Date != _curFileDate.Date || !File.Exists(_curLogFile))
                {
                    while (true)
                    {
                        var now = DateTime.Now;

                        //创建日志目录
                        var logFolder = Path.Combine(LogFolderParent, "Log", now.ToString("yyyyMMdd"));
                        if (!Directory.Exists(logFolder))
                        {
                            Directory.CreateDirectory(logFolder);
                        }

                        //非同一天，则重置序号
                        if (now.Date != _curFileDate.Date)
                        {
                            _rollIndex = 0;
                        }

                        //日志文件
                        _curLogFile = Path.Combine(logFolder, $"{FileName}{(_rollIndex == 0 ? "" : "_" + _rollIndex.ToString("D3"))}.log");
                        if (!File.Exists(_curLogFile)) //不存在
                        {
                            _curFileDate = now;
                            _curFileSize = 0;
                            break;
                        }
                        else
                        {
                            //文件较小
                            var info = new FileInfo(_curLogFile);
                            if (info.Length < FileRollSize)
                            {
                                _curFileDate = now;
                                _curFileSize = info.Length;
                                break;
                            }
                        }

                        //
                        _rollIndex++;
                        Thread.Sleep(200);
                    }
                }

                //写日志，更新文件大小
                File.AppendAllText(_curLogFile, log, Encoding.UTF8);
                _curFileSize += Encoding.UTF8.GetByteCount(log);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }


        #endregion

    }
}
