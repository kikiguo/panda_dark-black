﻿using System;

namespace StairMed.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILogger
    {
        LogLevel LogLevel { set; }
        ILoggerOutput Output { set; }
        void LogT(string format, params object[] args);
        void LogD(string format, params object[] args);
        void LogI(string format, params object[] args);
        void LogW(string format, params object[] args);
        void LogE(string format, params object[] args);
        void LogF(string format, params object[] args);
        void Exp(string desc, Exception ex);
        void Writeline(string str);
    }
}

