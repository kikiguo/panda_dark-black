﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILoggerOutput
    {
        void Output(string message);
    }
}
