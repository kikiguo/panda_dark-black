﻿using HDF5CSharp;
using StairMed.DataFile.NWB.Entity;
using System;
using System.Collections.Generic;

namespace StairMed.DataFile.NWB
{
    /// <summary>
    /// 
    /// </summary>
    public class NWBFileReader
    {
        private readonly string _filePath = string.Empty;
        private readonly long _fileId = -1;
        private AcquisitionGroup _acq = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        public NWBFileReader(string filePath)
        {
            _filePath = filePath;
            _fileId = Hdf5.OpenFile(_filePath, true);
            _acq = new AcquisitionGroup(_fileId, _fileId, Hdf5.ReadTreeFileStructure(_filePath));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetStairMedNWBVersion()
        {
            return _acq.ElectricalSeries.CollectInformation.StairMedNWBVersion;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public long GetFloatRows()
        {
            if (_acq.ElectricalSeries.SampleDatas != null)
            {
                _acq.ElectricalSeries.SampleDatas.Open();
                return (long)_acq.ElectricalSeries.SampleDatas.GetRows();
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="beginIndex"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public float[,] GetFloatDatas(long beginIndex, long rows)
        {
            _acq.ElectricalSeries.SampleDatas.Open();
            return _acq.ElectricalSeries.SampleDatas.ReadDataset((ulong)beginIndex, (ulong)rows);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public long GetSpikeLength()
        {
            _acq.ElectricalSeries.SpikeDatas.Open();
            return (long)_acq.ElectricalSeries.SpikeDatas.GetRows();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="beginIndex"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public byte[,] GetSpikeDatas(long beginIndex, long rows)
        {
            _acq.ElectricalSeries.SpikeDatas.Open();
            return _acq.ElectricalSeries.SpikeDatas.ReadDataset((ulong)beginIndex, (ulong)rows);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetNotes()
        {
            return _acq.ElectricalSeries.CollectInformation.Note;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetSampleRate()
        {
            return _acq.ElectricalSeries.CollectInformation.AmplifierSampleRate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetDeviceType()
        {
            return _acq.ElectricalSeries.CollectInformation.DeviceType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetDeviceChannelCount()
        {
            return _acq.ElectricalSeries.CollectInformation.DeviceChannelCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetProbeMap()
        {
            return _acq.ElectricalSeries.CollectInformation.ProbeMap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DateTime GetRecordTime()
        {
            return _acq.ElectricalSeries.CollectInformation.StartTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<int> GetChannels()
        {
            var channelCount = _acq.ElectricalSeries.CollectInformation.ChannelCount;
            _acq.ElectricalSeries.Electrodes.Open();
            var channelmap = _acq.ElectricalSeries.Electrodes.ReadDataset(0, (ulong)channelCount);
            var list = new List<int>();
            for (int i = 0; i < channelCount; i++)
            {
                list.Add(channelmap[i, 0]);
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetDataTypeDescription()
        {
            return _acq.ElectricalSeries.CollectInformation.DataTypeDescription;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetTestDescription()
        {
            return _acq.ElectricalSeries.CollectInformation.Description;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            if (_acq == null || _acq.ElectricalSeries == null)
            {
                return;
            }

            //
            if (_acq.ElectricalSeries.SampleDatas != null)
            {
                _acq.ElectricalSeries.SampleDatas.CloseRead();
            }
            if (_acq.ElectricalSeries.SpikeDatas != null)
            {
                _acq.ElectricalSeries.SpikeDatas.CloseRead();
            }
            if (_acq.ElectricalSeries.Electrodes != null)
            {
                _acq.ElectricalSeries.Electrodes.CloseRead();
            }

            //
            _acq = null;
        }
    }
}
