﻿using HDF5CSharp;
using HDF5CSharp.DataTypes;
using System;

namespace StairMed.DataFile.NWB.Entity
{
    /// <summary>
    /// 
    /// </summary>
    [Hdf5GroupName("acquisition")]
    internal class AcquisitionGroup : Hdf5BaseFile
    {
        /// <summary>
        /// 采样数据
        /// </summary>
        [Hdf5EntryName("ElectricalSeries")]
        public ElectricalSeriesGroup ElectricalSeries { get; set; }

        //
        [Hdf5EntryName("neurodata_type")]
        public string NeurodataType { get; set; }
        //
        [Hdf5EntryName("namespace")]
        public string NameSpace { get; set; }
        //
        [Hdf5EntryName("object_id")]
        public string ObjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="groupRoot"></param>
        /// <param name="chunkSize"></param>
        /// <param name="sampleRate"></param>
        public AcquisitionGroup(long fileId, long groupRoot) : base(fileId, groupRoot)
        {
            ElectricalSeries = new ElectricalSeriesGroup(fileId, GroupId);
        }

        /// <summary>
        /// 
        /// </summary>
        public AcquisitionGroup(long fileId, long groupRoot, Hdf5Element structure)
        {
            FileId = fileId;
            GroupRoot = groupRoot;

            //
            var index = structure.Children.FindIndex(r => r.Name.Split("/")[^1].Equals(GetGroupName()));
            if (index >= 0)
            {
                GroupId = structure.Children[index].Id;

                //
                ElectricalSeries = new ElectricalSeriesGroup(fileId, GroupId, structure.Children[index]);
            }
        }
    }
}
