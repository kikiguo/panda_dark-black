﻿using HDF5CSharp;
using HDF5CSharp.DataTypes;
using System;

namespace StairMed.DataFile.NWB.Entity
{
    [Hdf5GroupName("collect_param_information")]
    internal class CollectInformationGroup : Hdf5BaseFile
    {
        /// <summary>
        /// 采样率
        /// </summary>
        [Hdf5EntryName("stairmed_version")]
        public int StairMedNWBVersion { get; set; }

        /// <summary>
        /// 采样率
        /// </summary>
        [Hdf5EntryName("amplifier_sample_rate")]
        public int AmplifierSampleRate { get; set; }

        /// <summary>
        /// 采样时间
        /// </summary>
        [Hdf5EntryName("start_time")]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 通道个数
        /// </summary>
        [Hdf5EntryName("channel_cout")]
        public int ChannelCount { get; set; }

        /// <summary>
        /// 设备总共通道数
        /// </summary>
        [Hdf5EntryName("device_channel_count")]
        public int DeviceChannelCount { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        [Hdf5EntryName("device_type")]
        public string DeviceType { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        [Hdf5EntryName("probe_map")]
        public string ProbeMap { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Hdf5EntryName("session_description")]
        public string Description { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Hdf5EntryName("data_type_description")]
        public string DataTypeDescription { get; set; }

        /// <summary>
        /// 笔记
        /// </summary>
        [Hdf5EntryName("note")]
        public string Note { get; set; }

        //
        [Hdf5EntryName("neurodata_type")]
        public string NeurodataType { get; set; }
        //
        [Hdf5EntryName("namespace")]
        public string NameSpace { get; set; }
        //
        [Hdf5EntryName("object_id")]
        public string ObjectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="groupRoot"></param>
        public CollectInformationGroup(long fileId, long groupRoot) : base(fileId, groupRoot)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public CollectInformationGroup()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public CollectInformationGroup(long fileId, long groupRoot, Hdf5Element structure)
        {
            FileId = fileId;
            GroupRoot = groupRoot;

            //
            var index = structure.Children.FindIndex(r => r.Name.Split("/")[^1].Equals(GetGroupName()));
            if (index >= 0)
            {
                GroupId = structure.Children[index].Id;

                //
                var info = Hdf5.ReadObject<CollectInformationGroup>(FileId, structure.Children[index].Name);
                AmplifierSampleRate = info.AmplifierSampleRate;
                ChannelCount = info.ChannelCount;
                StartTime = info.StartTime;
                DeviceChannelCount = info.DeviceChannelCount;
                DeviceType = info.DeviceType;
                ProbeMap = info.ProbeMap;
                StairMedNWBVersion = info.StairMedNWBVersion;
                Note = info.Note;
                Description = info.Description;
                DataTypeDescription = info.DataTypeDescription;
            }
        }
    }
}
