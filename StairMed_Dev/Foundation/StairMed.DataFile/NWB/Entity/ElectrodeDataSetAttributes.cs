﻿using HDF.PInvoke;
using HDF5CSharp;
using HDF5CSharp.DataTypes;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace StairMed.DataFile.NWB.Entity
{
    internal class ElectrodeDataSetAttributes:Hdf5BaseFile
    {
        [Hdf5EntryName("namespace")]
        public string Namespace { get; set; }
        //
        [Hdf5EntryName("neurodata_type")]
        public string NeurodataType { get; set; }
        //
        [Hdf5EntryName("object_id")]
        public string ObjectId { get; set; }

        [Hdf5EntryName("table")]
        public string Table { get; set; }
        public ElectrodeDataSetAttributes()
        {

        }
    }
}
