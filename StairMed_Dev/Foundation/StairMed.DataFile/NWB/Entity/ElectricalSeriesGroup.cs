﻿using HDF.PInvoke;
using HDF5.NET;
using HDF5CSharp;
using HDF5CSharp.DataTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace StairMed.DataFile.NWB.Entity
{
    /// <summary>
    /// 
    /// </summary>
    [Hdf5GroupName("ElectricalSeries")]
    internal class ElectricalSeriesGroup : Hdf5BaseFile
    {
        /// <summary>
        /// 数据流
        /// </summary>
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        public ChunkedDatasetExt<float> SampleDatas { get; set; }

        /// <summary>
        /// 数据流
        /// </summary>
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        public ChunkedDatasetExt<byte> SpikeDatas { get; set; }

        /// <summary>
        /// 采集通道
        /// </summary>
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        public ChunkedDatasetExt<int> Electrodes { get; set; }

        //
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        public ChunkedDatasetExt<long> Timestamps { get; set; }

        /// <summary>
        /// 采样率等信息
        /// </summary>
        [Hdf5EntryName("collect_param_information")]
        public CollectInformationGroup CollectInformation { get; set; }



        /// <summary>
        /// event数据流
        /// </summary>
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        public ChunkedDatasetExt<int> Event { get; set; }

        /// <summary>
        /// HFP数据流
        /// </summary>
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        public ChunkedDatasetExt<float> HfpDatas { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public ElectricalSeriesGroup(long fileId, long groupRoot) : base(fileId, groupRoot)
        {
            SampleDatas = new ChunkedDatasetExt<float>("data", GroupId);
            SpikeDatas = new ChunkedDatasetExt<byte>("spikedata", GroupId);
            Electrodes = new ChunkedDatasetExt<int>("electrodes",GroupId);
            Event = new ChunkedDatasetExt<int>("event", GroupId);
            HfpDatas = new ChunkedDatasetExt<float>("hfpdata", GroupId);
            Timestamps = new ChunkedDatasetExt<long>("timestamps", GroupId);
            CollectInformation = new CollectInformationGroup(fileId, GroupId);
        }

     
        /// <summary>
        /// 
        /// </summary>
        public ElectricalSeriesGroup(long fileId, long groupRoot, Hdf5Element structure)
        {
            FileId = fileId;
            GroupRoot = groupRoot;

            //
            var index = structure.Children.FindIndex(r => r.Name.Split("/")[^1].Equals(GetGroupName()));
            if (index >= 0)
            {
                GroupId = structure.Children[index].Id;

                //
                CollectInformation = new CollectInformationGroup(fileId, GroupId, structure.Children[index]);

                //
                if (structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("data")) != null)
                {
                    SampleDatas = new ChunkedDatasetExt<float>("data", GroupId)
                    {
                        FileId = fileId,

                        DatasetNameForRead = structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("data")).Name,
                    };
          
                }
                if (structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("spikedata")) != null)
                {
                    SpikeDatas = new ChunkedDatasetExt<byte>("spikedata", GroupId)
                    {
                        FileId = fileId,
                        DatasetNameForRead = structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("spikedata")).Name,
                    };
                }
                if (structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("electrodes")) != null)
                {
                    Electrodes = new ChunkedDatasetExt<int>("electrodes", GroupId)
                    {
                        FileId = fileId,
                        DatasetNameForRead = structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("electrodes")).Name,
                    };
                }
                if (structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("event")) != null)
                {
                    Event = new ChunkedDatasetExt<int>("event", GroupId)
                    {
                        FileId = fileId,
                        DatasetNameForRead = structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("event")).Name,
                    };
                }
                if (structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("hfpdata")) != null)
                {
                    Event = new ChunkedDatasetExt<int>("hfpdata", GroupId)
                    {
                        FileId = fileId,
                        DatasetNameForRead = structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("hfpdata")).Name,
                    };
                }
                if (structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("timestamps")) != null)
                {
                    Timestamps = new ChunkedDatasetExt<long>("timestamps", GroupId)
                    {
                        FileId = fileId,
                        DatasetNameForRead = structure.Children[index].Children.FirstOrDefault(r => r.Name.Split("/")[^1].Equals("timestamps")).Name,
                    };
                }
            }
        }


        //32-bit integer
        void NWB_write_type(long groupId, string dataname, int data)
        {
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.copy(H5T.NATIVE_INT32);  
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            byte[] dataBytes = BitConverter.GetBytes(data);
            GCHandle hnd = GCHandle.Alloc(dataBytes, GCHandleType.Pinned);
            var result = H5A.write(attributeId, dtype, hnd.AddrOfPinnedObject());
            hnd.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(dtype);
        }


        //64-bit floating-point
        void NWB_write_type_float(long groupId, string dataname, Object data)
        {
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.copy(H5T.IEEE_F64LE);
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            GCHandle hnd = GCHandle.Alloc(data, GCHandleType.Pinned);
            var result = H5A.write(attributeId, dtype, hnd.AddrOfPinnedObject());
            hnd.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(dtype);
        }

        //H5Y_STR_NULLTERM
        void NWB_write(long groupId, string dataname, string data)
        {
            byte[] wdata = Encoding.UTF8.GetBytes(data);
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.create(H5T.class_t.STRING, new IntPtr(wdata.Length));
            H5T.set_cset(dtype,H5T.cset_t.UTF8);
            var typeId = H5T.copy(dtype);
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            GCHandle hnd = GCHandle.Alloc(wdata, GCHandleType.Pinned);
            var result = H5A.write(attributeId, dtype, hnd.AddrOfPinnedObject());
            hnd.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(typeId);
        }

        /// <summary>
        /// //H5T_VARIABLE
        /// </summary>
        void NWB_write_E(long groupId, string dataname, string data)
        {
            // Convert .NET string to byte array
            byte[] wdata = Encoding.UTF8.GetBytes(data + "\0"); // Null-terminate the string
            long spaceId = H5S.create(H5S.class_t.SCALAR);

            // Create a datatype for a string of any length
            //long dtype = H5T.copy(H5T.C_S1);
            long dtype = H5T.copy(H5T.NATIVE_FLOAT);
            H5T.set_order(dtype, H5T.order_t.LE);
            H5T.set_size(dtype, H5T.VARIABLE);
            H5T.set_cset(dtype, H5T.cset_t.UTF8);

            // Create an attribute
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);

            // Allocate memory to hold the string pointer
            IntPtr[] stringPtrs = new IntPtr[] { Marshal.StringToHGlobalAnsi(data) };
            GCHandle pinnedArray = GCHandle.Alloc(stringPtrs, GCHandleType.Pinned);
            IntPtr pointerToPointer = pinnedArray.AddrOfPinnedObject();

            // Write the attribute data
            var result = H5A.write(attributeId, dtype, pointerToPointer);

            // Cleanup: release unmanaged memory and close HDF5 objects
            Marshal.FreeHGlobal(stringPtrs[0]);
            pinnedArray.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(dtype);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetElectrodes(List<int> channels)
        {
            Electrodes = new ChunkedDatasetExt<int>("electrodes", GroupId);
           
            var extrodes = new int[channels.Count, 1];
            for (int i = 0; i < channels.Count; i++)
            {
                extrodes[i, 0] = i;
            }
            Electrodes.AppendOrCreateDataset(extrodes);
            Electrodes.Flush();
           
            long datasetId = H5D.open(GroupId, "electrodes");

            // 
            NWB_write(datasetId, "namespace", "hdmf-common");
            NWB_write(datasetId, "neurodata_type", "DynamicTableRegion");
            NWB_write(datasetId, "object_id", Guid.NewGuid().ToString());
            //NWB_write(datasetId, "table", "/general/extracellular_ephys/electrodes");
            NWB_write(datasetId, "description", "Intan electrode table region");
            // 
            H5D.close(datasetId);
        }

        public void SetSampleDatas()
        {
            SampleDatas = new ChunkedDatasetExt<float>("data", GroupId);
            bool attributesAlreadySet = false;

        
            if (!attributesAlreadySet)
            {
                long datasetId = H5D.open(GroupId, "data");
                //
                NWB_write_type_float(datasetId, "conversion", 1.0);
                NWB_write_type_float(datasetId, "offset", 0.0);
                NWB_write_type_float(datasetId, "resolution", 1.95);
                NWB_write_E(datasetId, "unit", "volts");
                // 
                H5D.close(datasetId);
                attributesAlreadySet = true;
            }
        }


        /// <summary>
        /// 时间戳
        /// </summary>
        public void SetTimestamps()
        {
            Timestamps = new ChunkedDatasetExt<long>("timestamps", GroupId);
            bool attributesAlreadySet = false;

            if (!attributesAlreadySet)
            {
                long datasetId = H5D.open(GroupId, "timestamps");
                // ConvertTo1DArray(Timestamps.GetData()); 

                NWB_write_type(datasetId, "interval", 1);
                NWB_write_E(datasetId, "unit", "seconds");
                H5D.close(datasetId);
                attributesAlreadySet = true;

            }
        }

        /// <summary>
        /// SPK 
        /// </summary>
        public void SetEvent()
        {
            Event = new ChunkedDatasetExt<int>("event", GroupId);
            bool attributesAlreadySet = false;


            if (!attributesAlreadySet)
            {
                long datasetId = H5D.open(GroupId, "event");
                //
                NWB_write_type_float(datasetId, "conversion", 1.0);
                NWB_write_type_float(datasetId, "offset", 0.0);
                NWB_write_type_float(datasetId, "resolution", -1.0);
                NWB_write_E(datasetId, "unit", "volts");
                // 
                H5D.close(datasetId);
                attributesAlreadySet = true;
            }
        }

        /// <summary>
        /// spkdata
        /// </summary>
        public void SetSpkData()
        {
            SpikeDatas = new ChunkedDatasetExt<byte>("spikedata", GroupId);
            bool attributesAlreadySet = false;


            if (!attributesAlreadySet)
            {
                long datasetId = H5D.open(GroupId, "spikedata");
                //
                NWB_write_type_float(datasetId, "conversion", 1.0);
                NWB_write_type_float(datasetId, "offset", 0.0);
                NWB_write_type_float(datasetId, "resolution", -1.0);
                NWB_write_E(datasetId, "unit", "volts");
                // 
                H5D.close(datasetId);
                attributesAlreadySet = true;
            }
        }

        /// <summary>
        /// hfpdata
        /// </summary>
        public void SetHfpData()
        {
            HfpDatas = new ChunkedDatasetExt<float>("hfpdata", GroupId);
            bool attributesAlreadySet = false;


            if (!attributesAlreadySet)
            {
                long datasetId = H5D.open(GroupId, "hfpdata");
                //
                NWB_write_type_float(datasetId, "conversion", 1.0);
                NWB_write_type_float(datasetId, "offset", 0.0);
                NWB_write_type_float(datasetId, "resolution", -1.0);
                NWB_write_E(datasetId, "unit", "volts");
                // 
                H5D.close(datasetId);
                attributesAlreadySet = true;
            }
        }


    }
}
