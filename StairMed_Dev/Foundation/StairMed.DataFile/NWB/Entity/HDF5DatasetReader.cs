﻿using HDF.PInvoke;
using HDF5CSharp;
using System;
using System.Runtime.InteropServices;

namespace StairMed.DataFile.NWB.Entity
{
    /// <summary>
    /// 
    /// </summary>
    internal class HDF5DatasetReader
    {
        private long datasetId = 0;
        private long spaceId = 0;
        private int rank = 0;
        private long memId = 0;
        private ulong[] maxDims;
        private ulong[] dims;
        private ulong[] chunkDims;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="name"></param>
        public void Open(long groupId, string name)
        {
            datasetId = H5D.open(groupId, Hdf5Utils.NormalizedName(name));
            spaceId = H5D.get_space(datasetId);
            rank = H5S.get_simple_extent_ndims(spaceId);
            maxDims = new ulong[rank];
            dims = new ulong[rank];
            chunkDims = new ulong[rank];
            H5S.get_simple_extent_dims(spaceId, dims, maxDims);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="beginRow"></param>
        /// <param name="endIndex"></param>
        /// <returns></returns>
        public T[,] ReadDataset<T>(ulong beginRow, ulong rows)
        {
            //
            ulong[] start = { 0, 0 };
            ulong[] stride = null;
            ulong[] count = { 0, 0 };
            ulong[] block = null;
            ulong[] offsetOut = { 0, 0 };
            var datatype = GetDatatype(typeof(T));

            //
            start[0] = beginRow;
            start[1] = 0;
            count[0] = rows;
            count[1] = dims[1];

            //
            var status = H5S.select_hyperslab(spaceId, H5S.seloper_t.SET, start, stride, count, block);

            // Define the memory dataspace.
            T[,] dset = new T[count[0], count[1]];
            var memId = H5S.create_simple(rank, count, null);

            // Define memory hyperslab. 
            status = H5S.select_hyperslab(memId, H5S.seloper_t.SET, offsetOut, null, count, null);

            // Read data from hyperslab in the file into the hyperslab in 
            // memory and display.             
            GCHandle hnd = GCHandle.Alloc(dset, GCHandleType.Pinned);
            H5D.read(datasetId, datatype, memId, spaceId, H5P.DEFAULT, hnd.AddrOfPinnedObject());
            hnd.Free();

            //
            H5S.close(memId);
            return dset;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            H5D.close(datasetId);
            H5S.close(spaceId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ulong GetRows()
        {
            return dims[0];
        }

        internal static long GetDatatype(Type type)
        {
            long dataType;

            var typeCode = Type.GetTypeCode(type);
            switch (typeCode)
            {
                case TypeCode.Byte:
                    dataType = H5T.NATIVE_UINT8;
                    break;
                case TypeCode.SByte:
                    dataType = H5T.NATIVE_INT8;
                    break;
                case TypeCode.Int16:
                    dataType = H5T.NATIVE_INT16;
                    break;
                case TypeCode.Int32:
                    dataType = H5T.NATIVE_INT32;
                    break;
                case TypeCode.Int64:
                    dataType = H5T.NATIVE_INT64;
                    break;
                case TypeCode.UInt16:
                    dataType = H5T.NATIVE_UINT16;
                    break;
                case TypeCode.UInt32:
                    dataType = H5T.NATIVE_UINT32;
                    break;
                case TypeCode.UInt64:
                    dataType = H5T.NATIVE_UINT64;
                    break;
                case TypeCode.Single:
                    dataType = H5T.NATIVE_FLOAT;
                    break;
                case TypeCode.Double:
                    dataType = H5T.NATIVE_DOUBLE;
                    break;
                //case TypeCode.DateTime:
                //    dataType = H5T.Native_t;
                //    break;
                case TypeCode.Char:
                    //dataType = H5T.NATIVE_UCHAR;
                    dataType = H5T.C_S1;
                    break;
                case TypeCode.String:
                    //dataType = H5T.NATIVE_UCHAR;
                    dataType = H5T.C_S1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(type.Name, $"Data Type {type} not supported");
            }
            return dataType;
        }
    }
}
