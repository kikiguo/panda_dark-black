﻿using HDF5CSharp;

namespace StairMed.DataFile.NWB.Entity
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class ChunkedDatasetExt<T> : ChunkedDataset<T> where T : struct
    {
        HDF5DatasetReader reader = null;

        public ChunkedDatasetExt(string name, long groupId) : base(name, groupId)
        {
        }

        public ChunkedDatasetExt(string name, long groupId, ulong[] chunkSize) : base(name, groupId, chunkSize)
        {
        }

        public ChunkedDatasetExt(string name, long groupId, T[,] dataset) : base(name, groupId, dataset)
        {
        }

        public long FileId { get; set; }

        public string DatasetNameForRead { get; set; }

        private bool _opened = false;


        /// <summary>
        /// 
        /// </summary>
        public void Open()
        {
            reader = new HDF5DatasetReader();
            reader.Open(FileId, DatasetNameForRead);
            _opened = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ulong GetRows()
        {
            if (!_opened)
            {
                Open();
            }
            return reader.GetRows();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="beginRow"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public T[,] ReadDataset(ulong beginRow, ulong rows)
        {
            if (!_opened)
            {
                Open();
            }
            return reader.ReadDataset<T>(beginRow, rows);
        }

        /// <summary>
        /// 
        /// </summary>
        public void CloseRead()
        {
            if (_opened)
            {
                reader.Close();
                _opened = false;
            }
        }

       
    }
}
