﻿using HDF5CSharp.DataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.DataFile.NWB.Entity
{
    public class DataAttributes
    {
        //
        [Hdf5EntryName("conversion")]
        public float Conversion { get; set; }

        //
        [Hdf5EntryName("offset")]
        public float Offset {  get; set; }

        //
        [Hdf5EntryName("resolution")]
        public float Resolution { get; set; }

        //
        [Hdf5EntryName("unit")]
        public string Unit { get; set; }
        
    }
}
