﻿using HDF.PInvoke;
using HDF5CSharp;
using HDF5CSharp.DataTypes;
using StairMed.DataFile.Logger;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace StairMed.DataFile.NWB.Entity
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class Hdf5BaseFile
    {
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        protected long FileId { set; get; }
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        protected long GroupRoot { set; get; }
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        protected string GroupName { get; }
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        protected long GroupId { set; get; }
        [Hdf5ReadWrite(Hdf5ReadWrite.DoNothing)]
        protected bool Disposed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="groupRoot"></param>
        /// <param name="groupName"></param>
        protected Hdf5BaseFile(in long fileId, in long groupRoot)
        {
            FileId = fileId;
            GroupRoot = groupRoot;

            //
            GroupName = GetGroupName();

            //
            GroupId = Hdf5.CreateOrOpenGroup(groupRoot, GroupName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetGroupName()
        {
            return GetGroupName(GetType());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetGroupName(Type t)
        {
            var groupName = string.Empty;
            var attrs = t.GetCustomAttributes(false);
            foreach (var attr in attrs)
            {
                if (attr is Hdf5GroupName groupNameAttr)
                {
                    groupName = groupNameAttr.Name.Trim();
                    break;
                }
            }
            return string.IsNullOrEmpty(groupName) ? t.Name : groupName;
        }

        /// <summary>
        /// 
        /// </summary>
        protected Hdf5BaseFile()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attrName"></param>
        /// <param name="attrVal"></param>
        public virtual void AddAttribute<T>(string attrName, T attrVal)
        {
            Hdf5.WriteAttribute(GroupId, attrName, attrVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attrName"></param>
        /// <param name="attrVal"></param>
        public virtual void AddAttribute(object obj)
        {
            if (obj == null)
            {
                return; 
            }
            var ps = obj.GetType().GetProperties();
            foreach (var p in ps)
            {
                var val = p.GetValue(obj);
                if (val != null)
                {
                    var attributeName = p.GetCustomAttribute<Hdf5EntryNameAttribute>()?.Name ?? p.Name;
                    Hdf5.WriteAttribute(GroupId, attributeName, val.ToString());
                }
            }

        }
        public virtual void AddNWBAttribute(object obj)
        {
            if (obj == null)
            {
                return;
            }
            var ps = obj.GetType().GetProperties();
            foreach (var p in ps)
            {
                var val = p.GetValue(obj);
                if (val != null)
                {
                    var attributeName = p.GetCustomAttribute<Hdf5EntryNameAttribute>()?.Name ?? p.Name;
                    NWB_write(GroupId, attributeName, val.ToString());

                }
            }

        }

        void NWB_write(long groupId, string dataname, string data)
        {
            byte[] wdata = Encoding.UTF8.GetBytes(data);
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.create(H5T.class_t.STRING, new IntPtr(wdata.Length));
            var typeId = H5T.copy(dtype);
            H5T.set_cset(dtype, H5T.cset_t.UTF8);
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            GCHandle hnd = GCHandle.Alloc(wdata, GCHandleType.Pinned);
            var result = H5A.write(attributeId, dtype, hnd.AddrOfPinnedObject());
            hnd.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(typeId);
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void FlushData()
        {
            if (Disposed)
            {
                return;
            }

            try
            {
                Hdf5.WriteObject(GroupRoot, this, GroupName);
                Hdf5.Flush(GroupId, HDF.PInvoke.H5F.scope_t.LOCAL);
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp(nameof(FlushData), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void FlushDataAndCloseObject()
        {
            try
            {
                if (Disposed)
                {
                    return;
                }

                FlushData();
                Hdf5.CloseGroup(GroupId);
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp(nameof(FlushDataAndCloseObject), ex);
            }
        }
    }
}
