﻿using HDF.PInvoke;
using HDF5CSharp;
using StairMed.DataFile.NWB.Entity;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Markup;
using System.Xml.Linq;

namespace StairMed.DataFile.NWB
{
    /// <summary>
    /// 
    /// </summary>
    public class NWBFileWriter
    {
        //
        private readonly object lockObj = new object();

        /// <summary>
        /// 
        /// </summary>
        private long _fileId = 0;

        /// <summary>
        /// 
        /// </summary>
        private AcquisitionGroup _nwb = null;
        private string _fileName = string.Empty;


        void NWB_write(long groupId, string dataname, string data)
        {
            byte[] wdata = Encoding.UTF8.GetBytes(data);
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.create(H5T.class_t.STRING, new IntPtr(wdata.Length));
            var typeId = H5T.copy(dtype);
            H5T.set_cset(dtype, H5T.cset_t.UTF8);
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            GCHandle hnd = GCHandle.Alloc(wdata, GCHandleType.Pinned);
            var result = H5A.write(attributeId, dtype, hnd.AddrOfPinnedObject());
            hnd.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(typeId);
        }

        void NWB_write_type_float(long groupId, string dataname, Object data)
        {
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.copy(H5T.IEEE_F64LE);
            var attributeId = H5A.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            GCHandle hnd = GCHandle.Alloc(data, GCHandleType.Pinned);
            var result = H5A.write(attributeId, dtype, hnd.AddrOfPinnedObject());
            hnd.Free();
            H5A.close(attributeId);
            H5S.close(spaceId);
            H5T.close(dtype);
        }


        void NWB_write_dataset(long groupId, string dataname, string data)
        {
            byte[] wdata = Encoding.UTF8.GetBytes(data);
            long spaceId = H5S.create(H5S.class_t.SCALAR);
            long dtype = H5T.create(H5T.class_t.STRING, new IntPtr(wdata.Length));
            var typeId = H5T.copy(dtype);
            H5T.set_cset(dtype, H5T.cset_t.UTF8);
            var datasetId = H5D.create(groupId, Hdf5Utils.NormalizedName(dataname), dtype, spaceId);
            GCHandle hnd = GCHandle.Alloc(wdata, GCHandleType.Pinned);
            var result_nsession_start_time = H5D.write(datasetId, dtype, H5S.ALL, H5S.ALL, H5P.DEFAULT,
            hnd.AddrOfPinnedObject());
            hnd.Free();
            H5D.close(datasetId);
            H5S.close(spaceId);
            H5T.close(typeId);
        }



        void InitializeNWBAttributes()
        {
            
            // write namespace 
            NWB_write(_fileId, "namespace", "core");

            // write neurodata_type
            NWB_write(_fileId, "neurodata_type", "NWBFile");

            // write nwb_version 
            NWB_write(_fileId, "nwb_version", "2.2.5"); 

            NWB_write(_fileId, "object_id", Guid.NewGuid().ToString());

            // create datasets
            //long datasetId = H5D.open(_fileId, "dataset_name");

            //long datasets_GroupId = Hdf5.CreateOrOpenGroup(_fileId, "/datasets");

            // write session_start_time
            //long session_start_time_GroupId = Hdf5.CreateOrOpenGroup(datasetId, "session_start_time");
            //NWB_write(session_start_time_GroupId, "data", "2023-07-07T11:00:00+00:00");

            //String time = "2023-07-07T11:00:00+00:00";
            DateTime currentDateTime = DateTime.Now.ToUniversalTime();
            string time = currentDateTime.ToString("yyyy-MM-ddTHH:mm:sszzz");

            // write session_start_time
            NWB_write_dataset(_fileId, "session_start_time", time);

            // write timestamps_reference_time
            NWB_write_dataset(_fileId, "timestamps_reference_time", time);

            // write session_description
            NWB_write_dataset(_fileId, "session_description", "Stairmed 2023");

            // write identifier
            NWB_write_dataset(_fileId, "identifier", "TSD");


            // write file_create_date
            List<string> file_create_date = new List<string>() { time };
            Hdf5.WriteStrings(_fileId, Hdf5Utils.NormalizedName("file_create_date"), file_create_date);

            // create general group
            long generalId = Hdf5.CreateOrOpenGroup(_fileId, "general");

            // write experiment_description
            NWB_write_dataset(generalId, "experiment_description", "Calcium imaging of spontaneous activity in larval zebrafish tectum");

            // write experimenter
            List<string> experimenter = new List<string>() { "Marcus Triplett" };
            Hdf5.WriteStrings(generalId, Hdf5Utils.NormalizedName("file_create_date"), experimenter);

            // write institution
            NWB_write_dataset(generalId, "institution", "University of Queensland");
        }


        void SetAcquisitionGroupInfo(int sampleRate, List<int> channels, string dataTypeDescription, string probeMapJson, string deviceType, int deviceChannelCount, string testDescription, int version)
        {

            _nwb.ElectricalSeries.CollectInformation.StairMedNWBVersion = version;
            _nwb.ElectricalSeries.CollectInformation.ProbeMap = probeMapJson;
            _nwb.ElectricalSeries.CollectInformation.DeviceType = deviceType;
            _nwb.ElectricalSeries.CollectInformation.DeviceChannelCount = deviceChannelCount;
            _nwb.ElectricalSeries.CollectInformation.AmplifierSampleRate = sampleRate;
            _nwb.ElectricalSeries.CollectInformation.StartTime = DateTime.Now;
            _nwb.ElectricalSeries.CollectInformation.ChannelCount = channels.Count;
            _nwb.ElectricalSeries.CollectInformation.Description = testDescription;
            _nwb.ElectricalSeries.CollectInformation.DataTypeDescription = dataTypeDescription;
            _nwb.ElectricalSeries.CollectInformation.Note = string.Empty;
            _nwb.ElectricalSeries.CollectInformation.NameSpace = "core";
            _nwb.ElectricalSeries.CollectInformation.NeurodataType = "ElectricalSeries";
            _nwb.ElectricalSeries.CollectInformation.ObjectId = Guid.NewGuid().ToString();
            _nwb.ElectricalSeries.AddNWBAttribute(_nwb.ElectricalSeries.CollectInformation);
            _nwb.ElectricalSeries.SetElectrodes(channels);
           //_nwb.ElectricalSeries.SetTimestamps(GenerateTimestamps(channels.Count));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileNameWithInfo"></param>
        /// <param name="sampleRate"></param>
        /// <param name="channels"></param>
        /// <param name="dataTypeDescription"></param>
        /// <param name="probeMapJson"></param>
        /// <param name="deviceType"></param>
        /// <param name="deviceChannelCount"></param>
        /// <param name="testDescription"></param>
        /// <param name="version"></param>
        public void StartRecording(string fileName, int sampleRate, List<int> channels, string dataTypeDescription, string probeMapJson, string deviceType, int deviceChannelCount, string testDescription, int version = 1)
        {
            lock (lockObj)
            {
                _fileName = fileName;

                //
                _fileId = Hdf5.CreateFile(_fileName);


                InitializeNWBAttributes();

                // write notes

                _nwb = new AcquisitionGroup(_fileId, _fileId);

                //
                SetAcquisitionGroupInfo(sampleRate, channels, dataTypeDescription, probeMapJson, deviceType, deviceChannelCount, testDescription, version);
                
                //
                _nwb.FlushData();
            }
        }



        /// <summary>
        /// timestamps
        /// </summary>
        public static long[,] GenerateTimestamps(int numRows)
        {
            int sampleRate = 25000;
            long[,] timestamps = new long[numRows, 1];
            long currentTimestampMicros = (long)((DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds * 1000); // 当前时间的微秒值
            float timeDifference = (float)(1.0 / sampleRate * 1000000); // 每个样本之间的时间差（微秒）

            for (int row = 0; row < numRows; row++)
            {
                long timestamp = currentTimestampMicros + (row * Convert.ToInt64(timeDifference));
                timestamps[row, 0] = timestamp;
            }

            return timestamps;
        }

        /// <summary>
        /// 
        /// </summary>
        public void AppendFloatData(float[,] floats)
        {
            lock (lockObj)
            {
                if (_nwb == null)
                {
                    return;
                }

                int numRows = floats.GetLength(0);
                int numCols = floats.GetLength(1);
                if (numRows * numCols > floats.Length) // Ensure we don't exceed array bounds
                {
                    throw new ArgumentException("The provided 'floats' array dimensions do not match the expected dimensions.");
                }
                float[] flatArray = new float[numRows * numCols];
                int flatArrayIndex = 0;
                for (int row = 0; row < numRows; row++)
                {
                    for (int col = 0; col < numCols; col++)
                    {
                        flatArray[flatArrayIndex] = floats[row, col];
                        flatArrayIndex++;
                    }
                }
                long[,] timestamps = GenerateTimestamps(numRows);
                _nwb.ElectricalSeries.SampleDatas.AppendOrCreateDataset(floats);
                _nwb.ElectricalSeries.Timestamps.AppendOrCreateDataset(timestamps);
                _nwb.ElectricalSeries.SampleDatas.Flush();
                _nwb.ElectricalSeries.Timestamps.Flush();
            }
        }

        /// <summary>
        /// 处理HFP数据
        /// </summary>
        public void AppendHFPFloatData(float[,] floats)
        {
            lock (lockObj)
            {
                if (_nwb == null)
                {
                    return;
                }

                int numRows = floats.GetLength(0);
                int numCols = floats.GetLength(1);
                float[] flatArray = new float[numRows * numCols];
                int flatArrayIndex = 0;
                for (int row = 0; row < numRows; row++)
                {
                    for (int col = 0; col < numCols; col++)
                    {
                        flatArray[flatArrayIndex] = floats[row, col];
                        flatArrayIndex++;
                    }
                }

                _nwb.ElectricalSeries.HfpDatas.AppendOrCreateDataset(floats);
                _nwb.ElectricalSeries.HfpDatas.Flush();

            }
        }


        /// <summary>
        /// 每个int代表一个通道trig
        /// </summary>
        public void AppendTRIGFloatData(int[,] ints)
        {
            lock (lockObj)
            {
                if (_nwb == null)
                {
                    return;
                }

                _nwb.ElectricalSeries.Event.AppendOrCreateDataset(ints);
                _nwb.ElectricalSeries.Event.Flush();
            }
        }
        /// <summary>
        /// 每个byte代表一个通道有无spike
        /// </summary>
        public void AppendBooleanData(byte[,] bytes)
        {
            lock (lockObj)
            {
                if (_nwb == null)
                {
                    return;
                }

                //
                _nwb.ElectricalSeries.SpikeDatas.AppendOrCreateDataset(bytes);
                _nwb.ElectricalSeries.SpikeDatas.Flush();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="note"></param>
        public void AppendNote(string note)
        {
            lock (lockObj)
            {
                if (_nwb == null)
                {
                    return;
                }

                //
                _nwb.ElectricalSeries.CollectInformation.Note = $"{_nwb.ElectricalSeries.CollectInformation.Note}{DateTime.Now}{Environment.NewLine}{note}{Environment.NewLine}";
                _nwb.ElectricalSeries.AddAttribute(_nwb.ElectricalSeries.CollectInformation);
                _nwb.FlushData();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void StopRecording()
        {
            lock (lockObj)
            {
                if (_nwb == null)
                {
                    return;
                }

                //
                _nwb.ElectricalSeries.CollectInformation.FlushDataAndCloseObject();
                _nwb.ElectricalSeries.Electrodes.Dispose();
                _nwb.ElectricalSeries.SetSampleDatas();
                _nwb.ElectricalSeries.SampleDatas.Dispose();
                _nwb.ElectricalSeries.SetTimestamps();
                _nwb.ElectricalSeries.Timestamps.Dispose();
                _nwb.ElectricalSeries.SetSpkData();
                _nwb.ElectricalSeries.SpikeDatas.Dispose();
                _nwb.ElectricalSeries.SetEvent();
                _nwb.ElectricalSeries.Event.Dispose();
                _nwb.ElectricalSeries.SetHfpData();
                _nwb.ElectricalSeries.HfpDatas.Dispose();
                _nwb.ElectricalSeries.FlushDataAndCloseObject();
                _nwb.FlushDataAndCloseObject();
                Hdf5.CloseFile(_fileId);

                //
                _nwb = null;
            }
        }
    }
}