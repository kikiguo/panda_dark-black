﻿using StairMed.DataFile.Logger;
using StairMed.DataFile.NWB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace StairMed.DataFile.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class NWB2FilePerChannelBinaryConverter : IFileConverter
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(NWB2FilePerChannelBinaryConverter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcFile"></param>
        /// <param name="dstFolder"></param>
        /// <returns></returns>
        public bool Convert(string srcFile, string dstFolder)
        {
            try
            {
                //
                if (!File.Exists(srcFile))
                {
                    return false;
                }

                //
                if (!Directory.Exists(dstFolder))
                {
                    Directory.CreateDirectory(dstFolder);
                }

                //
                var reader = new NWBFileReader(srcFile);
                var isSpike = reader.GetDataTypeDescription().Contains("Spike");
                var totalRow = isSpike ? reader.GetSpikeLength() : reader.GetFloatRows();

                //
                if (totalRow <= 0)
                {
                    return false;
                }

                //
                var physicsChannels = reader.GetChannels();
                var channelCount = physicsChannels.Count;

                //创建文件读写器
                var fsList = new List<BinaryWriter>();
                for (int i = 0; i < physicsChannels.Count; i++)
                {
                    var fs = new FileStream(Path.Combine(dstFolder, $"{physicsChannels[i] + 1}.dat"), FileMode.Create, FileAccess.Write);
                    fsList.Add(new BinaryWriter(fs, Encoding.ASCII));
                }

                //
                try
                {
                    //
                    var readOnce = 1024;
                    var readed = 0L;

                    //循环读取数据
                    while (true)
                    {
                        var readRows = Math.Min(totalRow - readed, readOnce);
                        if (readRows <= 0)
                        {
                            break;
                        }

                        //将数据存文件
                        if (isSpike)
                        {
                            var chunk = reader.GetSpikeDatas(readed, readRows);
                            for (int i = 0; i < readRows; i++)
                            {
                                for (int j = 0; j < channelCount; j++)
                                {
                                    fsList[j].Write(chunk[i, j]);
                                }
                            }
                        }
                        else
                        {
                            var chunk = reader.GetFloatDatas(readed, readRows);
                            for (int i = 0; i < readRows; i++)
                            {
                                for (int j = 0; j < channelCount; j++)
                                {
                                    fsList[j].Write(chunk[i, j]);
                                }
                            }
                        }

                        //
                        readed += readRows;
                    }

                    //
                    return true;
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(Convert)} inner", ex);
                }
                finally
                {
                    //
                    foreach (var fs in fsList)
                    {
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Convert)} outer", ex);
            }

            //
            return false;
        }
    }
}
