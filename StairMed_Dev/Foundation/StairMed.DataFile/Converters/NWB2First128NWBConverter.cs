﻿using StairMed.DataFile.Logger;
using StairMed.DataFile.NWB;
using System;
using System.IO;


namespace StairMed.DataFile.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class NWB2First128NWBConverter
    {
        const string TAG = nameof(NWB2First128NWBConverter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="srcFile"></param>
        /// <param name="dstFile"></param>
        /// <param name="outputChannels"></param>
        /// <returns></returns>
        public bool Convert(string srcFile, string dstFile, int outputChannels = 128)
        {
            try
            {
                //
                if (!File.Exists(srcFile))
                {
                    return false;
                }

                //
                if (File.Exists(dstFile))
                {
                    File.Delete(dstFile);
                }

                //
                var reader = new NWBFileReader(srcFile);
                var isSpike = reader.GetDataTypeDescription().Contains("Spike");
                var totalRow = isSpike ? reader.GetSpikeLength() : reader.GetFloatRows();

                //
                if (totalRow <= 0)
                {
                    return false;
                }

                //
                var physicsChannels = reader.GetChannels();
                var channelCount = physicsChannels.Count;
                var sampleRate = reader.GetSampleRate();
                var dataTypeDescription = reader.GetDataTypeDescription();
                var probeMapJson = reader.GetProbeMap();
                var deviceType = reader.GetDeviceType();
                var testDescription = reader.GetTestDescription();
                var version = reader.GetStairMedNWBVersion();


                //
                if (channelCount < outputChannels || isSpike)
                {
                    return false;
                }

                //
                while (physicsChannels.Count > outputChannels)
                {
                    physicsChannels.RemoveAt(physicsChannels.Count - 1);
                }

                //
                var writer = new NWBFileWriter();
                writer.StartRecording(dstFile, sampleRate, physicsChannels, dataTypeDescription, probeMapJson, deviceType, reader.GetDeviceChannelCount(), testDescription, version);

                //
                try
                {
                    //
                    var readOnce = 1024;
                    var readed = 0L;

                    //循环读取数据
                    while (true)
                    {
                        var readRows = Math.Min(totalRow - readed, readOnce);
                        if (readRows <= 0)
                        {
                            break;
                        }

                        var floats = new float[readRows, outputChannels];

                        //将数据存文件
                        var chunk = reader.GetFloatDatas(readed, readRows);
                        for (int i = 0; i < readRows; i++)
                        {
                            for (int j = 0; j < outputChannels; j++)
                            {
                                floats[i, j] = chunk[i, j];
                            }
                        }

                        //
                        readed += readRows;

                        //
                        writer.AppendFloatData(floats);
                    }

                    //
                    return true;
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(Convert)} inner", ex);
                }
                finally
                {
                    writer.StopRecording();
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(Convert)} outer", ex);
            }

            //
            return false;
        }
    }
}
