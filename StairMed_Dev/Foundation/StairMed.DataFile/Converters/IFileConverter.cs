﻿namespace StairMed.DataFile.Converters
{
    /// <summary>
    /// 
    /// </summary>
    internal interface IFileConverter
    {
        bool Convert(string srcFile, string dstFolder);
    }
}
