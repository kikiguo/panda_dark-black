using System;
using System.Diagnostics;
using System.Threading;

namespace StairMed.BLEDiagnosis
{
    /// <summary>
    /// 
    /// </summary>
    public class BLEDiagnosisHelper
    {
        /// <summary>
        /// 
        /// </summary>
        const int BlueToothTestTimeMS = 10000;

        /// <summary>
        /// 
        /// </summary>
        private readonly ManualResetEvent _resetEvent = new ManualResetEvent(false);

        /// <summary>
        /// 
        /// </summary>
        private int _lastPackLastIndex = 0;
        private int _packCount = 0;
        private DateTime _firstPackTime = DateTime.Now;
        private DateTime _lastPackTime = DateTime.Now.AddSeconds(1);
        private int _packLen = 0;
        private readonly Stopwatch _watch = new Stopwatch();

        /// <summary>
        /// 
        /// </summary>
        private readonly IBLEDiagnosable _ble;

        /// <summary>
        /// 是否接收蓝牙数据
        /// </summary>
        private bool _isDiagnosising = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ble"></param>
        public BLEDiagnosisHelper(IBLEDiagnosable ble)
        {
            _ble = ble;
        }

        /// <summary>
        /// 蓝牙传输自检
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="recvdPack"></param>
        /// <param name="packPerSecond"></param>
        /// <param name="ms"></param>
        /// <param name="packLen"></param>
        /// <returns></returns>
        public bool RunDiagnosis(out int recvdPack, out double packPerSecond, out int packLen, out double bytePerSecond, out bool consecutive, int reqPackLen = 240)
        {
            packPerSecond = 0;
            recvdPack = 0;
            packLen = 0;
            bytePerSecond = 0;

            //
            consecutive = true;

            //
            _lastPackLastIndex = -1;
            _packCount = 0;

            //
            _isDiagnosising = true;

            //
            try
            {
                //先停止自检
                TryStopDiagnosis();

                //开启蓝牙自检
                if (!_ble.StartBLEDiagnosis())
                {
                    //启动失败
                    _isDiagnosising = false;
                    return false;
                }

                //
                _ble.BLEDiagnosisDataRecvd += HandleRecvdDiagnosisData;

                //
                _watch.Restart();

                //等待一会，蓝牙数据持续发送中
                if (_resetEvent.WaitOne(BlueToothTestTimeMS))
                {
                    consecutive = false;
                }

                //停止自检数据包
                _isDiagnosising = false;

                //停止自检
                TryStopDiagnosis();

                //总共接收了多少包
                recvdPack = _packCount;
                if (_packCount > 0)
                {
                    if (_packCount == 1)
                    {
                        packPerSecond = 1 / (_lastPackTime - _firstPackTime).TotalSeconds;
                    }
                    else
                    {
                        packPerSecond = (_packCount - 1) / (_lastPackTime - _firstPackTime).TotalSeconds;
                    }
                    packLen = _packLen;
                }

                //
                bytePerSecond = packLen * packPerSecond;

                //
                _ble.BLEDiagnosisDataRecvd -= HandleRecvdDiagnosisData;
            }
            catch (Exception ex)
            {
                var a = 0;
            }

            //
            _isDiagnosising = false;
            return true;
        }

        /// <summary>
        /// 蓝牙自检接收数据
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="offset"></param>
        /// <param name="dataLen"></param>
        public void HandleRecvdDiagnosisData(byte[] frame, int offset, int dataLen)
        {
            if (!_isDiagnosising)
            {
                return;
            }

            //
            var first = frame[offset];
            var last = frame[offset + dataLen - 1];

            //
            _packLen = dataLen;

            //
            if (_packCount > 0)
            {
                _lastPackTime = DateTime.Now;

                //首尾相连判断
                if (((_lastPackLastIndex + 1) % 256 != first || (last + 256 - first) % 256 != dataLen - 1))
                {
                    _resetEvent.Set();
                    return;
                }
            }
            else
            {
                _firstPackTime = DateTime.Now;
                _lastPackTime = _firstPackTime.AddSeconds(1);
                _packCount = 0;
            }

            //
            _ble.ReportBLEDiagnosisProgress((int)(_watch.ElapsedMilliseconds * 100 / BlueToothTestTimeMS));

            //
            _lastPackLastIndex = last;
            _packCount++;
        }

        #region

        /// <summary>
        /// 多次尝试停止蓝牙传输自检
        /// </summary>
        private void TryStopDiagnosis()
        {
            int count = 5;
            while (count > 0)
            {
                if (_ble.StopBLEDiagnosis())
                {
                    break;
                }
                count--;
            }
        }

        #endregion
    }
}
