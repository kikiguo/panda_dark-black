﻿using System;

namespace StairMed.BLEDiagnosis
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBLEDiagnosable
    {
        bool StartBLEDiagnosis();

        bool StopBLEDiagnosis();

        void ReportBLEDiagnosisProgress(int percent);

        event Action<byte[], int, int> BLEDiagnosisDataRecvd;
    }
}
