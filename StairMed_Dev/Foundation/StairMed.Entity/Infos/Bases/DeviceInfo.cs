﻿using StairMed.MVVM.Base;
using System.Xml.Serialization;

namespace StairMed.Entity.Infos.Bases
{
    /// <summary>
    /// 
    /// </summary>
    public class DeviceInfo : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private long _deviceId = 0;
        [XmlAttribute]
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }

        /// <summary>
        /// 名称
        /// </summary>
        private string _name = "StairMed";
        [XmlAttribute]
        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }
    }
}
