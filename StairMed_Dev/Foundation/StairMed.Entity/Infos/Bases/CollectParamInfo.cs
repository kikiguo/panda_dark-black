﻿using StairMed.Core.Consts;
using StairMed.Enum;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using System.Xml.Serialization;

namespace StairMed.Entity.Infos.Bases
{
    /// <summary>
    /// 采样参数
    /// </summary>
    public class CollectParamInfo : ViewModelBase
    {
        protected readonly int DEVICE_CHANNELS = 0;

        /// <summary>
        /// 
        /// </summary>
        public CollectParamInfo(int channelCount)
        {
            DEVICE_CHANNELS = channelCount;
        }

        /// <summary>
        /// 
        /// </summary>
        private long _deviceId = 0;
        [XmlAttribute]
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }

        /// <summary>
        /// 选取的数据类型
        /// </summary>
        protected CollectMode _collectType = CollectMode.RawAll;
        [XmlIgnore]
        public virtual CollectMode CollectType
        {
            get { return _collectType; }
            set
            {
                Set(ref _collectType, value);
                if (value == CollectMode.RawSingle)
                {
                    if (CollectChannelCount > 1)
                    {
                        var firstEnableIndex = Channels.FindIndex(r => r.Enable);
                        for (int i = firstEnableIndex + 1; i < Channels.Count; i++)
                        {
                            Channels[i].Enable = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 进行Spike采集时压缩阈值
        /// </summary>
        private double _thresholdUVA = -70;
        [XmlIgnore]
        public double ThresholdUVA
        {
            get { return _thresholdUVA; }
            set { Set(ref _thresholdUVA, value); }
        }

        private bool _LFPFirst = true;
        public bool LFPFirst
        {
            get { return _LFPFirst; }
            set { Set(ref _LFPFirst, value); }
        }

        private string _implantMac = "";
        public string ImplantMac
        {
            get { return _implantMac; }
            set { Set(ref _implantMac, value); }
        }
        /// <summary>
        /// 进行Spike采集时压缩阈值
        /// </summary>
        private double _thresholdUVB = 300;
        [XmlIgnore]
        public double ThresholdUVB
        {
            get { return _thresholdUVB; }
            set { Set(ref _thresholdUVB, value); }
        }

        /// <summary>
        /// 采样率
        /// </summary>
        private int _sampleRate = ReadonlyCONST.SampleRates.Last();
        [XmlIgnore]
        public int SampleRate
        {
            get { return _sampleRate; }
            set
            {
                var sampleRate = Math.Max(value, 10);
                Set(ref _sampleRate, sampleRate);
            }
        }

        /// <summary>
        /// 通道信息:对应physics index
        /// </summary>
        private List<ChannelInfo> _channels;
        [XmlIgnore]
        public List<ChannelInfo> Channels
        {
            get
            {
                return _channels;
            }
            set
            {
                Set(ref _channels, value);
            }
        }

        /// <summary>
        /// 多少个通道被启用采集
        /// </summary>
        [XmlIgnore]
        public int CollectChannelCount => Channels != null ? Channels.Count(r => r.Enable) : 0;

        /// <summary>
        /// 排好序的通道
        /// </summary>
        [XmlIgnore]
        public List<int> OrderedInputChannels => Channels != null ? Channels.Where(r => r.Enable).OrderBy(r => r.PhysicsIndex).Select(r => r.InputIndex).ToList() : new List<int>();

        /// <summary>
        /// 排好序的通道
        /// </summary>
        [XmlIgnore]
        public List<int> OrderedPhysicsChannels => Channels != null ? Channels.Where(r => r.Enable).OrderBy(r => r.PhysicsIndex).Select(r => r.PhysicsIndex).ToList() : new List<int>();

        /// <summary>
        /// 排好序的通道
        /// </summary>
        [XmlIgnore]
        public List<int> InputChannelInReports => Channels != null ? Channels.Where(r => r.Enable).OrderBy(r => r.InputIndexInReport).Select(r => r.InputIndex).ToList() : new List<int>();

        /// <summary>
        /// 
        /// </summary>
        public void SelectAll()
        {
            foreach (var item in Channels)
            {
                item.Enable = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicsChannel"></param>
        public void SelectSingleChannel(int physicsChannel)
        {
            //
            if (physicsChannel < 0 || physicsChannel >= Channels.Count)
            {
                return;
            }

            //
            foreach (var item in Channels)
            {
                item.Enable = false;
            }
            Channels[physicsChannel].Enable = true;
        }


        public void SetSingleChanne(int physicsChannel , bool bStatus)
        {
            //
            if (physicsChannel < 0 || physicsChannel >= Channels.Count)
            {
                return;
            }
            Channels[physicsChannel].Enable = bStatus;
        }

        /// <summary>
        /// 初始化通道数据
        /// </summary>
        public virtual CollectParamInfo InitChannels()
        {
            //
            if (Channels == null)
            {
                Channels = new List<ChannelInfo>();
            }
            for (int i = 0; i < DEVICE_CHANNELS; i++)
            {
                var channel = Channels.FirstOrDefault(r => r.PhysicsIndex == i);
                if (channel != null)
                {
                    channel.InputIndex = ReadonlyCONST.ConvertToInputChannel(i);
                    channel.NativeName = $"{i + 1:D2}";
                }
                else
                {
                    channel = new ChannelInfo
                    {
                        PhysicsIndex = i,
                        InputIndex = ReadonlyCONST.ConvertToInputChannel(i),
                        NativeName = $"{i + 1:D2}",
                        Enable = false
                        //Enable = i == 0
                    };
                    Channels.Add(channel);
                }

                //
                if (ReadonlyCONST.GetInputIndexInReport == null)
                {
                    channel.InputIndexInReport = channel.InputIndex;
                }
                else
                {
                    channel.InputIndexInReport = ReadonlyCONST.GetInputIndexInReport(channel.InputIndex);
                }
            }
            Channels.RemoveAll(r => r.PhysicsIndex < 0 || r.PhysicsIndex >= DEVICE_CHANNELS);
            if (!Channels.Exists(r => r.Enable))
            {
                Channels[0].Enable = true;
            }
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicsChannel"></param>
        /// <returns></returns>
        public string GetNativeName(int physicsChannel)
        {
            var channelInfo = Channels.FirstOrDefault(r => r.PhysicsIndex == physicsChannel);
            return channelInfo == null ? $"{physicsChannel}" : channelInfo.NativeName;
        }
    }
}
