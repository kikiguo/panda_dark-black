﻿using StairMed.Enum;
using StairMed.MVVM.Base;

namespace StairMed.Entity.Infos.Bases
{
    /// <summary>
    /// 
    /// </summary>
    public class StateInfo : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected ConnectState _connectState = ConnectState.Disconnected;
        public virtual ConnectState ConnectState
        {
            get { return _connectState; }
            set { Set(ref _connectState, value); }
        }

        /// <summary>
        /// 接收的数据包总数
        /// </summary>
        private long _recvdFrameCount = 0;
        public long RecvdFrameCount
        {
            get { return _recvdFrameCount; }
            set { Set(ref _recvdFrameCount, value); }
        }

        /// <summary>
        /// 丢失的数据包个数
        /// </summary>
        private long _lostFrameCount = 0;
        public long LostFrameCount
        {
            get { return _lostFrameCount; }
            set
            {
                Set(ref _lostFrameCount, value);
                IsLostFrame = value > 0;
                if (_recvdFrameCount > 0)
                {
                    LostRate = 100.0 * value / (value + _recvdFrameCount);
                }
            }
        }

        /// <summary>
        /// 丢包率
        /// </summary>
        private double _lostRate = 0;
        public double LostRate
        {
            get { return _lostRate; }
            set { Set(ref _lostRate, value); }
        }

        /// <summary>
        /// 是否有丢包
        /// </summary>
        private bool _isLostFrame = false;
        public bool IsLostFrame
        {
            get { return _isLostFrame; }
            set { Set(ref _isLostFrame, value); }
        }
    }
}
