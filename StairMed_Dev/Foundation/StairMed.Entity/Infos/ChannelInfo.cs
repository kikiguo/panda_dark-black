﻿using StairMed.MVVM.Base;
using System.Xml.Serialization;

namespace StairMed.Entity.Infos
{
    /// <summary>
    /// 通道信息
    /// </summary>
    public class ChannelInfo : ViewModelBase
    {
        /// <summary>
        /// 实际的通道序号:下位机的通道序号
        /// </summary>
        private int _inputIndex = -1;
        [XmlIgnore]
        public int InputIndex
        {
            get { return _inputIndex; }
            set { Set(ref _inputIndex, value); }
        }

        /// <summary>
        /// 相邻的index对应相邻的电极pad焊盘
        /// </summary>
        private int _physicsIndex = -1;
        [XmlIgnore]
        public int PhysicsIndex
        {
            get { return _physicsIndex; }
            set { Set(ref _physicsIndex, value); }
        }

        /// <summary>
        /// 数据上报序号：在多通道上报数据时的顺序号
        /// </summary>
        private int _reportIndex = -1;
        [XmlIgnore]
        public int InputIndexInReport
        {
            get { return _reportIndex; }
            set { Set(ref _reportIndex, value); }
        }

        /// <summary>
        /// NativeName
        /// </summary>
        private string _nativeName = "NativeName";
        [XmlAttribute]
        public string NativeName
        {
            get { return _nativeName; }
            set { Set(ref _nativeName, value); }
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        private bool _enable;
        [XmlIgnore]
        public bool Enable
        {
            get { return _enable; }
            set { Set(ref _enable, value); }
        }
    }
}
