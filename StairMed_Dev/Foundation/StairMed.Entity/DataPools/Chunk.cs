﻿using StairMed.Array;
using System;
using System.Collections.Generic;

namespace StairMed.Entity.DataPools
{
    /// <summary>
    /// 
    /// </summary>
    public class Chunk : IDisposable
    {
        /// <summary>
        /// 该块数据每个通道第一个样本的序号
        /// </summary>
        public long DataIndex = 0;
        //public long FirstSampleDataIndex = 0;

        /// <summary>
        /// 每个通道总共已经接收的样本个数
        /// </summary>
        public long TotalRecvdSampleOfEachChannel = 0;

        /// <summary>
        /// 
        /// </summary>
        public int SampleRate = 30000;

        /// <summary>
        /// 
        /// </summary>
        public int ChannelCount = 0;

        /// <summary>
        /// 
        /// </summary>
        public List<int> InputChannelInReports = new List<int>();

        /// <summary>
        /// 
        /// </summary>
        public int OneChannelSamples = 0;
        //public int SampleOfEachChannel = 0;


        /// <summary>
        /// 
        /// </summary>
        public PooledArray<float> Raws = null;

        /// <summary>
        /// 
        /// </summary>
        public PooledArray<float> Wfps = null;

        /// <summary>
        /// 
        /// </summary>
        public PooledArray<float> Lfps = null;

        /// <summary>
        /// 
        /// </summary>
        public PooledArray<float> Hfps = null;

        /// <summary>
        /// 
        /// </summary>
        public PooledArray<float> Spks = null;

        /// <summary>
        /// 
        /// </summary>
        public PooledArray<int> Trigs = null;

        /// <summary>
        /// 
        /// </summary>
        public int SpikeScopeTimeMS;

        /// <summary>
        /// 
        /// </summary>
        public int SpikeSumStepMS;

        /// <summary>
        /// 
        /// </summary>
        public int SpikeSumWindowMS;

        /// <summary>
        /// 是否已经被析构掉
        /// </summary>
        private bool _isDisposed = false;

        /// <summary>
        /// 
        /// </summary>
        public Chunk()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        ~Chunk()
        {
            DisposeImpl();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            DisposeImpl();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void DisposeImpl()
        {
            if (!_isDisposed)
            {
                if (Raws != null)
                {
                    Raws.Dispose();
                }
                if (Wfps != null)
                {
                    Wfps.Dispose();
                }
                if (Lfps != null)
                {
                    Lfps.Dispose();
                }
                if (Hfps != null)
                {
                    Hfps.Dispose();
                }
                if (Spks != null)
                {
                    Spks.Dispose();
                }
                if (Trigs != null)
                {
                    Trigs.Dispose();
                }
            }
            _isDisposed = true;
        }
    }
}
