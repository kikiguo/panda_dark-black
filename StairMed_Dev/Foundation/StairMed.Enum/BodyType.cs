﻿namespace StairMed.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum BodyType : byte
    {
        Home,
        Hardware,
        RealTime,
        History,
        Scene,
        Setting,
        Help,
        About,
    }
}
