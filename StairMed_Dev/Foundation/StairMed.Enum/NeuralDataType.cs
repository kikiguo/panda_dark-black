﻿namespace StairMed.Enum
{
    /// <summary>
    /// 神经数据类型：原始数据、Spike、Spike组合
    /// </summary>
    public enum NeuralDataType : byte
    {
        Raw,
        WFP,
        LFP,
        HFP,
        Spike,
        TRIG,
        SpikeSum,
        ISI,
        SpikeScope,
        Spectrum,
        Trigger,
        PSTH,
        SpikeRateRMS,
    }

    public enum EnumDisplayMode
    {
        RAW,
        SPK,
        LFP,
        HFP,
        FR
    }
}
