﻿namespace StairMed.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum CollectMode : byte
    {
        RawAll,
        RawSingle,
        RawLoop,
        Spike,
        LFP,
        TRIG
    }
    public enum EnumOutputType
    {
        Spike,
    }
}
