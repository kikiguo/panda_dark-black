﻿namespace StairMed.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum DeviceType
    {
        CS36 = 1,
        SS64 = 2,
        HInSX = 3,
        HInS32 = 4,
        CS64 = 5,
    }

    public enum PowerSupplyState
    {
        PowerCut,
        InPowerSupply,
    }

    public enum EnumSampleType
    {
        Spike_None,
        Spike_15KHz,
        Spike_20KHz,
        Spike_25KHz,
        Spike_30KHz,
        LFP_None,
        LFP_500Hz,
        LFP_1000Hz,
        LFP_1500Hz,
        LFP_2000Hz,
    }
}
