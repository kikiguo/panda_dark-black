﻿namespace StairMed.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum ConnectState : byte
    {
        Connecting,
        Connected,
        Disconnecting,
        Disconnected,
    }
}
