﻿namespace StairMed.Enum
{
    public enum NotchFilterType
    {
        None,
        _50Hz,
        _60Hz,
    }
}
