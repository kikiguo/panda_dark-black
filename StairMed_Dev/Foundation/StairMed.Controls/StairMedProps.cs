﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace StairMed.Controls
{
    public class StairMedProps
    {
        #region icon font 图标

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty IconTextProperty = DependencyProperty.RegisterAttached("IconText", typeof(string), typeof(StairMedProps), new PropertyMetadata(""));
        public static string GetIconText(DependencyObject d)
        {
            return (string)d.GetValue(IconTextProperty);
        }
        public static void SetIconText(DependencyObject d, string value)
        {
            d.SetValue(IconTextProperty, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty IconTextHorizontalAlignmentProperty = DependencyProperty.RegisterAttached("IconTextHorizontalAlignment", typeof(HorizontalAlignment), typeof(StairMedProps), new PropertyMetadata(HorizontalAlignment.Center));
        public static HorizontalAlignment GetIconTextHorizontalAlignment(DependencyObject d)
        {
            return (HorizontalAlignment)d.GetValue(IconTextHorizontalAlignmentProperty);
        }
        public static void SetIconTextHorizontalAlignment(DependencyObject d, HorizontalAlignment value)
        {
            d.SetValue(IconTextHorizontalAlignmentProperty, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty IconTextVerticalAlignmentProperty = DependencyProperty.RegisterAttached("IconTextVerticalAlignment", typeof(VerticalAlignment), typeof(StairMedProps), new PropertyMetadata(VerticalAlignment.Center));
        public static VerticalAlignment GetIconTextVerticalAlignment(DependencyObject d)
        {
            return (VerticalAlignment)d.GetValue(IconTextVerticalAlignmentProperty);
        }
        public static void SetIconTextVerticalAlignment(DependencyObject d, VerticalAlignment value)
        {
            d.SetValue(IconTextVerticalAlignmentProperty, value);
        }
        #endregion









        #region PasswordBox 密码引出

        public static readonly DependencyProperty PasswordProperty = DependencyProperty.RegisterAttached("Password", typeof(string), typeof(StairMedProps), new PropertyMetadata("", new PropertyChangedCallback(OnPasswordPropertyChanged)));
        public static string GetPassword(DependencyObject d)
        {
            return (string)d.GetValue(PasswordProperty);
        }
        public static void SetPassword(DependencyObject d, string value)
        {
            d.SetValue(PasswordProperty, value);
        }
        public static readonly DependencyProperty AttachProperty = DependencyProperty.RegisterAttached("Attach", typeof(string), typeof(StairMedProps), new PropertyMetadata(new PropertyChangedCallback(OnAttachChanged)));
        public static string GetAttach(DependencyObject d)
        {
            return (string)d.GetValue(PasswordProperty);
        }
        public static void SetAttach(DependencyObject d, string value)
        {
            d.SetValue(PasswordProperty, value);
        }
        private static void OnAttachChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PasswordBox pb)
            {
                pb.PasswordChanged += PasswordBox_PasswordChanged;
            }
        }
        static bool _isPasswordUpdating = false;
        private static void OnPasswordPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is PasswordBox pb)
            {
                pb.PasswordChanged -= PasswordBox_PasswordChanged;
                if (!_isPasswordUpdating)
                {
                    pb.Password = e.NewValue.ToString();
                }
                pb.PasswordChanged += PasswordBox_PasswordChanged;
            }
        }
        private static void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (sender is PasswordBox pb)
            {
                _isPasswordUpdating = true;
                SetPassword(pb, pb.Password);
                _isPasswordUpdating = false;
            }
        }

        #endregion






    }
}
