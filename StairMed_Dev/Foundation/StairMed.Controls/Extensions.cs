﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace StairMed.Controls
{
    public static class Extensions
    {
        private static Dictionary<Control, Typeface> TypefaceDict = new Dictionary<Control, Typeface>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrl"></param>
        /// <returns></returns>
        public static Typeface GetTypeface(this Control ctrl)
        {
            if (TypefaceDict.ContainsKey(ctrl))
            {
                return TypefaceDict[ctrl];
            }
            var typeface = new Typeface(ctrl.FontFamily, ctrl.FontStyle, ctrl.FontWeight, ctrl.FontStretch);
            TypefaceDict[ctrl] = typeface;
            return typeface;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static FormattedText CreateFormatText(this Control ctrl, string txt)
        {
            return new FormattedText(txt, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, ctrl.GetTypeface(), ctrl.FontSize, ctrl.Foreground, 1.0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static FormattedText CreateFormatText(this Control ctrl, string txt, Brush brush)
        {
            return new FormattedText(txt, CultureInfo.CurrentUICulture, FlowDirection.LeftToRight, ctrl.GetTypeface(), ctrl.FontSize, brush, 1.0);
        }

        /// <summary>
        /// 抗锯齿修正
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static double MakeLineThin(this Control ctrl, double d)
        {
            return Math.Floor(d) + 0.5;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static System.Drawing.Color ToColor(this System.Windows.Media.Color color)
        {
            return System.Drawing.Color.FromArgb(color.B, color.G, color.R);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        public static void DelayClose(this Window window)
        {
            try
            {
                window.Dispatcher.BeginInvoke(new Action(() =>
                {
                    window.Close();
                }));
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        public static void TakeToFront(this Window window)
        {
            try
            {
                if (window == null)
                {
                    return;
                }

                //
                if (!window.IsVisible)
                {
                    window.Show();
                }

                //
                if (window.WindowState == WindowState.Minimized)
                {
                    window.WindowState = WindowState.Normal;
                }

                //
                window.Activate();
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="window"></param>
        public static void BindingDragMove(this Window window)
        {
            window.MouseDown += (object sender, System.Windows.Input.MouseButtonEventArgs e) =>
            {
                if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
                {
                    try
                    {
                        window.DragMove();
                    }
                    catch { }
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clickCtrl"></param>
        public static void BindingCaptionClick(this Control clickCtrl)
        {
            clickCtrl.MouseDoubleClick += (object sender, System.Windows.Input.MouseButtonEventArgs e) =>
            {
                try
                {
                    FrameworkElement ctrl = clickCtrl;
                    while (true)
                    {
                        ctrl = (FrameworkElement)ctrl.Parent;
                        if (ctrl == null)
                        {
                            break;
                        }

                        if (ctrl is Window window)
                        {
                            window.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                if (window.WindowState == WindowState.Normal)
                                {
                                    window.WindowState = WindowState.Maximized;
                                }
                                else if (window.WindowState == WindowState.Maximized)
                                {
                                    window.WindowState = WindowState.Normal;
                                }
                            }));
                            break;
                        }
                    }
                }
                catch { }
            };
        }
    }
}
