﻿using StairMed.Core.Settings;
using StairMed.Entity.Infos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StairMed.Controls
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:StairMed.Controls"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:StairMed.Controls;assembly=StairMed.Controls"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:MultiComboBox/>
    ///
    /// </summary>
    public class MultiComboBox : ComboBox
    {
        static MultiComboBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MultiComboBox), new FrameworkPropertyMetadata(typeof(MultiComboBox)));
        }


        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            d.SetValue(e.Property, e.NewValue);
        }



        /// <summary>
        /// 选中项列表
        /// </summary>
        public ObservableCollection<MultiCbxBaseData> ChekedItems
        {
            get { return (ObservableCollection<MultiCbxBaseData>)GetValue(ChekedItemsProperty); }
            set { SetValue(ChekedItemsProperty, value); }
        }

        public static readonly DependencyProperty ChekedItemsProperty =
            DependencyProperty.Register("ChekedItems", typeof(ObservableCollection<MultiCbxBaseData>), typeof(MultiComboBox), new PropertyMetadata(new ObservableCollection<MultiCbxBaseData>(), OnPropertyChanged));



        /// <summary>
        /// ListBox竖向列表
        /// </summary>
        private ListBox _ListBoxV;

        /// <summary>
        /// ListBox横向列表
        /// </summary>
        private ListBox _ListBoxH;

        public override void OnApplyTemplate()
        {
            try
            {
                base.OnApplyTemplate();
                _ListBoxV = Template.FindName("PART_ListBox", this) as ListBox;
                _ListBoxH = Template.FindName("PART_ListBoxChk", this) as ListBox;
                _ListBoxH.ItemsSource = ChekedItems;
                _ListBoxV.SelectionChanged += _ListBoxV_SelectionChanged;
                //_ListBoxH.SelectionChanged += _ListBoxH_SelectionChanged;

                if (ItemsSource != null)
                {
                    foreach (var item in ItemsSource)
                    {
                        MultiCbxBaseData bdc = item as MultiCbxBaseData;
                        if (bdc.IsCheck)
                        {
                            _ListBoxV.SelectedItems.Add(bdc);
                        }
                    }
                }
            }
            catch
            { 
            
            }
        }

        private void _ListBoxH_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                foreach (var item in e.RemovedItems)
                {
                    MultiCbxBaseData datachk = item as MultiCbxBaseData;

                    for (int i = 0; i < _ListBoxV.SelectedItems.Count; i++)
                    {
                        MultiCbxBaseData datachklist = _ListBoxV.SelectedItems[i] as MultiCbxBaseData;
                        if (datachklist.ID == datachk.ID)
                        {
                            _ListBoxV.SelectedItems.Remove(_ListBoxV.SelectedItems[i]);
                        }
                    }
                }
            }
            catch
            { 
            
            }
        }

        void _ListBoxV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                foreach (var item in e.AddedItems)
                {
                    MultiCbxBaseData datachk = item as MultiCbxBaseData;
                    datachk.IsCheck = true;
                    if (ChekedItems.IndexOf(datachk) < 0)
                    {
                        ChekedItems.Add(datachk);
                    }
                }

                foreach (var item in e.RemovedItems)
                {
                    MultiCbxBaseData datachk = item as MultiCbxBaseData;
                    datachk.IsCheck = false;
                    ChekedItems.Remove(datachk);
                }
            }
            catch
            { 
            
            }
        }

    }
}
