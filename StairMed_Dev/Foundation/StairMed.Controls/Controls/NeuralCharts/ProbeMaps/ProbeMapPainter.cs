﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using StairMed.Probe;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.ProbeMaps
{
    /// <summary>
    /// 
    /// </summary>
    public class ProbeMapPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected static readonly ColorScale COLOR_SCALE = new ColorScale(0, 64);

        /// <summary>
        /// 边框预留空间
        /// </summary>
        private const int Padding = 2;

        /// <summary>
        /// 
        /// </summary>
        protected Font _channelFont = null;

        /// <summary>
        /// 
        /// </summary>
        protected string _fontFamily = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        protected NTProbeMap _probeMap = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="bgcolor"></param>
        /// <param name="fontFamily"></param>
        /// <param name="origin"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="probeMap"></param>
        public virtual void PaintProbeMap(DrawingContext ctx, System.Drawing.Color bgcolor, string fontFamily, System.Windows.Point origin, float width, float height, NTProbeMap probeMap)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out _, true);


            //
            if (_channelFont != null)
            {
                _channelFont.Dispose();
            }
            _channelFont = null;
            _fontFamily = string.IsNullOrWhiteSpace(fontFamily) ? "微软雅黑" : fontFamily;
            _probeMap = probeMap;

            //
            base.PaintToShadow(bgcolor, origin, width, height, true, (g) =>
            {
                PaintProbes(width, height, g);
            }, true, System.Drawing.Drawing2D.SmoothingMode.HighQuality);

            //
            CopyFromShadow(ctx, origin, width, height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="g"></param>
        protected virtual void PaintProbes(float width, float height, Graphics g)
        {
            if (_probeMap == null)
            {
                return;
            }

            //
            var borderLines = new List<NTLine>();
            foreach (var probe in _probeMap.Probes)
            {
                borderLines.AddRange(probe.Shape);
            }

            //找到水平和垂直方向的 最大 最小 值
            var minX = Math.Min(borderLines.Min(r => r.X1), borderLines.Min(r => r.X2));
            var maxX = Math.Max(borderLines.Max(r => r.X1), borderLines.Max(r => r.X2));
            var minY = Math.Min(borderLines.Min(r => r.Y1), borderLines.Min(r => r.Y2));
            var maxY = Math.Max(borderLines.Max(r => r.Y1), borderLines.Max(r => r.Y2));

            //
            if (minX == maxX || minY == maxY)
            {
                return;
            }

            //等比放缩绘制在控件上：判断 放缩比例 和 x/y偏移
            var ratio = Math.Min((width - Padding * 2) / (maxX - minX), (height - Padding * 2) / (maxY - minY));
            var xOffset = (width - Padding * 2 - (maxX - minX) * ratio) / 2 + Padding;
            var yOffset = (height - Padding * 2 - (maxY - minY) * ratio) / 2 + Padding;

            //
            var titleFont = new Font(_fontFamily, 12);

            //遍历所有probe
            var padIndexOnElectrode = 0;
            foreach (var probe in _probeMap.Probes)
            {
                //边框
                foreach (var line in probe.Shape)
                {
                    var x1 = xOffset + ratio * (line.X1 - minX);
                    var x2 = xOffset + ratio * (line.X2 - minX);
                    var y1 = height - yOffset - ratio * (maxY - line.Y1);
                    var y2 = height - yOffset - ratio * (maxY - line.Y2);
                    var pt1 = new PointF(x1, y1);
                    var pt2 = new PointF(x2, y2);
                    g.DrawLine(Pens.White, pt1, pt2);
                }

                //每个probe标题
                {
                    var x = xOffset + ratio * (probe.Title.X - minX);
                    var y = height - yOffset - ratio * (maxY - probe.Title.Y);
                    var txt = $"{probe.Title.Text}";
                    var sizef = g.MeasureString(txt, titleFont);
                    g.DrawString(txt, titleFont, System.Drawing.Brushes.White, new PointF(x - sizef.Width / 2, y - sizef.Height / 2));
                }

                //焊盘大小和高度
                var padWidth = probe.PadWidth * ratio;
                var padWidthHalf = padWidth / 2;
                var padHeight = probe.PadHeight * ratio;
                var padHeightHalf = padHeight / 2;

                //绘制每个焊盘
                var padIndex = 0;
                foreach (var pad in probe.Pads)
                {
                    var x = xOffset + ratio * (pad.X - minX);
                    var y = height - yOffset - ratio * (maxY - pad.Y);

                    //
                    PaintPad(g, padWidth, padWidthHalf, padHeight, padHeightHalf, padIndex, padIndexOnElectrode, pad, x, y);

                    //
                    padIndex++;
                    padIndexOnElectrode++;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="padWidth"></param>
        /// <param name="padWidthHalf"></param>
        /// <param name="padHeight"></param>
        /// <param name="padHeightHalf"></param>
        /// <param name="channelFont"></param>
        /// <param name="padIndex"></param>
        /// <param name="padIndexOnElectrode"></param>
        /// <param name="pad"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        protected virtual void PaintPad(Graphics g, float padWidth, float padWidthHalf, float padHeight, float padHeightHalf, int padIndex, int padIndexOnElectrode, NTPad pad, float x, float y)
        {
            //画圈
            var color = GetColor(padIndex);
            var padPen = new System.Drawing.Pen(color.ToColor(), 2.0f);
            g.DrawEllipse(padPen, x - padWidthHalf, y - padHeightHalf, padWidth, padHeight);

            //通道名称文本
            if (_channelFont == null)
            {
                var txt = $"{_probeMap.ChannelCount}";
                var fontSize = (int)padWidth;
                while (fontSize > 3)
                {
                    var font = new Font(_fontFamily, fontSize);
                    var sizef = g.MeasureString(txt, font);
                    if (sizef.Width <= padWidth)
                    {
                        break;
                    }
                    fontSize--;
                }
                _channelFont = new Font(_fontFamily, fontSize);
            }

            //对应的通道
            {
                var txt = $"{pad.Channel + 1}";
                var sizef = g.MeasureString(txt, _channelFont);
                g.DrawString(txt, _channelFont, System.Drawing.Brushes.White, new PointF((int)Math.Round(x - sizef.Width / 2), (int)Math.Round(y - sizef.Height / 2)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual System.Windows.Media.Color GetColor(int padIndex)
        {
            return COLOR_SCALE.GetColor(10 + 32.0 * padIndex / _probeMap.ChannelCount * _probeMap.Probes.Count);
        }
    }
}
