﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Probe;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.ProbeMaps
{
    /// <summary>
    /// 
    /// </summary>
    public class ProbeMapChart : NeuralChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        static ProbeMapChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ProbeMapChart), new FrameworkPropertyMetadata(typeof(ProbeMapChart)));
        }

        /// <summary>
        /// map样子
        /// </summary>
        public NTProbeMap ProbeMap
        {
            get { return (NTProbeMap)GetValue(ProbeMapProperty); }
            set { SetValue(ProbeMapProperty, value); }
        }
        public static readonly DependencyProperty ProbeMapProperty = DependencyProperty.Register(nameof(ProbeMap), typeof(NTProbeMap), typeof(ProbeMapChart), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));

        #region 属性变化监听

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(ProbeMapProperty))
            {
                DelayReRender();
                return;
            }
        }

        #endregion

        //绘图工具
        private readonly ProbeMapPainter _painter = new ProbeMapPainter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            _painter.PaintProbeMap(ctx, _bgColor, this.FontFamily?.Source, OriginPoint, DrawAreaWidth, DrawAreaHeight, ProbeMap);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            if (ProbeMap != null)
            {
                var typeTxt = this.CreateFormatText(ProbeMap.Type);
                ctx.DrawText(typeTxt, new Point(OriginPoint.X + (DrawAreaWidth - typeTxt.Width) / 2, (Padding.Top - typeTxt.Height) / 2));
            }
        }
    }
}
