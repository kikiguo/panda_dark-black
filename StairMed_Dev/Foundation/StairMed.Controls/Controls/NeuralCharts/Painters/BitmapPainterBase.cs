﻿using StairMed.Controls.Logger;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace StairMed.Controls.Controls.NeuralCharts.Painters
{
    /// <summary>
    /// 
    /// </summary>
    public class BitmapPainterBase : IDisposable
    {
        private const string TAG = nameof(BitmapPainterBase);

#if DEBUG 
        //统计耗时
        private int _count = 0;
        private readonly Stopwatch _watch = new Stopwatch();
        private long _tickCost = 0;
        const int PrintCount = 350;
#endif

        /// <summary>
        /// 将bitmap绘制到屏幕上
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="origin"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public virtual void CopyFromShadow(DrawingContext ctx, System.Windows.Point origin, float width, float height)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }

            //
            var bitmap = GetBitmap(bmpW, bmpH, out bool isCreateNew);
            if (!isCreateNew)
            {
                bitmap.Lock();
                bitmap.AddDirtyRect(new Int32Rect(0, 0, bmpW, bmpH));
                bitmap.Unlock();

                //
                ctx.DrawImage(bitmap, new Rect { X = Math.Round(origin.X), Y = Math.Floor(origin.Y - height), Width = width, Height = height });
            }
            else
            {
                ForceRepaint();
            }
        }

        /// <summary>
        /// 将图形绘制到缓存中
        /// </summary>
        /// <param name="bmpW"></param>
        /// <param name="bmpH"></param>
        /// <param name="callback"></param>
        protected virtual bool PaintToShadow(System.Drawing.Color bgcolor, System.Windows.Point origin, float width, float height, bool newBoard, Action<Graphics> callback, bool useShadowCopy = true, SmoothingMode smoothing = SmoothingMode.HighSpeed)
        {
            try
            {
                int bmpW = (int)width;
                int bmpH = (int)height;
                if (bmpW == 0 || bmpH == 0)
                {
                    return false;
                }

                //
                var shadow = GetShadow(bmpW, bmpH);
                if (shadow == null)
                {
                    return false;
                }

#if DEBUG
                _watch.Restart();
#endif
                if (useShadowCopy)
                {
                    if (_shadowCopy != null)
                    {
                        using (var g = Graphics.FromImage(_shadowCopy))
                        {
                            g.SmoothingMode = smoothing;
                            if (newBoard)
                            {
                                g.Clear(bgcolor);
                            }

                            //
                            callback(g);
                        }
                    }

                    //TODO:判断是否有优化空间:通过像素拷贝的方式
                    using (var g = Graphics.FromImage(shadow))
                    {
                        g.SmoothingMode = smoothing;
                        if (_shadowCopy != null)
                        {
                            g.DrawImageUnscaled(_shadowCopy, 0, 0);

                        }
                    }
                }
                else
                {
                    using (var g = Graphics.FromImage(shadow))
                    {
                        g.SmoothingMode = smoothing;
                        if (newBoard)
                        {
                            g.Clear(System.Drawing.Color.Brown);
                        }

                        //
                        callback(g);
                    }
                }

#if DEBUG
                _watch.Stop();
                _count++;
                _tickCost += _watch.ElapsedTicks;
                if (_count == PrintCount)
                {
                    //CtrlLogTool.Logger.Writeline($"{nameof(PaintToShadow)} [{this.GetType().Name}] once cost:{1.0 * _tickCost / PrintCount / TimeSpan.TicksPerMillisecond:F3} ms (avg of {PrintCount})");
                    _count = 0;
                    _tickCost = 0;
                }
#endif
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(PaintToShadow)}", ex);
            }

            //
            return true;
        }



        #region 


        /// <summary>
        /// 
        /// </summary>
        private int _bitmapId = -1;

        /// <summary>
        /// 前景图
        /// </summary>
        protected WriteableBitmap _bitmap = null;

        /// <summary>
        /// 缓存图片
        /// </summary>
        protected Bitmap _shadow = null;

        /// <summary>
        /// _shadow的缓存图：直接更新shadow的话，会导致绘图绘制了一半就被搬运至前台
        /// </summary>
        protected Bitmap _shadowCopy = null;

        /// <summary>
        /// 生成前景图
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        protected virtual WriteableBitmap GetBitmap(int width, int height, out bool isCreateNew, bool useShadowCopy = true)
        {
            int id = GetId(width, height);
            isCreateNew = false;
            if (id != _bitmapId)
            {
                isCreateNew = true;
                _bitmapId = id;
                _bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Rgb24, null); //只能在ui线程创建
                if (useShadowCopy)
                {
                    _shadowCopy = new Bitmap(width, height);
                }
                _shadow = new Bitmap(width, height, _bitmap.BackBufferStride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, _bitmap.BackBuffer);
            }
            return _bitmap;
        }

        /// <summary>
        /// 生成后景图：影子图
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private Bitmap GetShadow(int width, int height)
        {
            int id = GetId(width, height);
            if (id != _bitmapId)
            {
                return null;
            }
            return _shadow;
        }

        /// <summary>
        /// 创建唯一ID
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private static int GetId(int width, int height)
        {
            return (width << 15) + height;
        }

        #endregion

        /// <summary>
        /// 强制刷新
        /// </summary>
        protected virtual void ForceRepaint()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _bitmapId = -1;

            //
            if (_shadow != null)
            {
                _shadow.Dispose();
                _shadow = null;
            }

            //
            if (_shadowCopy != null)
            {
                _shadowCopy.Dispose();
                _shadowCopy = null;
            }

            //
            _bitmap = null;
        }
    }
}
