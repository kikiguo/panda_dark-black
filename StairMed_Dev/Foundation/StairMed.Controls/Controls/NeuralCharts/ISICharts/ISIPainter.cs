﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.ISICharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class ISIPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual void PaintISI(DrawingContext ctx, System.Drawing.Color bgcolor, System.Drawing.Brush solidBrush, System.Windows.Point origin, float width, float height, float xpixelStep, List<float> ys)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //
            PaintToShadow(bgcolor, origin, width, height, true, (g) =>
            {
                //绘制点
                double x = 0;
                for (int i = 0; i < ys.Count; i++)
                {
                    if (ys[i] != origin.Y)
                    {
                        g.FillRectangle(solidBrush, Convert.ToSingle(x), Convert.ToSingle(ys[i]), xpixelStep, Convert.ToSingle(origin.Y - ys[i]));
                    }
                    x += xpixelStep;
                }
            }, true);

            //
            CopyFromShadow(ctx, origin, width, height);
        }
    }
}
