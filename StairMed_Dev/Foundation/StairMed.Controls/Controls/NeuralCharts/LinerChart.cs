﻿using HDF5.NET;
using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Controls.Controls.NeuralCharts.ISICharts;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using static System.Net.Mime.MediaTypeNames;

namespace StairMed.Controls.Controls.NeuralCharts
{
    public partial class LinerChart : NeuralChartBase
    {
        private List<ScaleInfo> SecondScalePosition = new List<ScaleInfo>();

        static LinerChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LinerChart), new FrameworkPropertyMetadata(typeof(LinerChart)));


        }

        /// <summary>
        /// 数据线颜色
        /// </summary>
        public Brush LinerDataBrush
        {
            get { return (Brush)GetValue(LinerDataBrushProperty); }
            set { SetValue(LinerDataBrushProperty, value); }
        }
        public static readonly DependencyProperty LinerDataBrushProperty = DependencyProperty.Register(nameof(LinerDataBrush), typeof(Brush), typeof(LinerChart), new PropertyMetadata(Brushes.White, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 数据线颜色
        /// </summary>
        public Brush LinerData2Brush
        {
            get { return (Brush)GetValue(LinerData2BrushProperty); }
            set { SetValue(LinerData2BrushProperty, value); }
        }
        public static readonly DependencyProperty LinerData2BrushProperty = DependencyProperty.Register(nameof(LinerData2Brush), typeof(Brush), typeof(LinerChart), new PropertyMetadata(Brushes.White, new PropertyChangedCallback(PropertyChanged)));


        public double[,] LinerData
        {
            get { return (double[,])GetValue(LinerDataProperty); }
            set { SetValue(LinerDataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LinerData.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LinerDataProperty =
            DependencyProperty.Register("LinerData", typeof(double[,]), typeof(LinerChart), new PropertyMetadata(new double[,] { { 0, 0 } }, new PropertyChangedCallback(PropertyChanged)));

        public double[,] LinerData2
        {
            get { return (double[,])GetValue(LinerData2Property); }
            set { SetValue(LinerData2Property, value); }
        }

        // Using a DependencyProperty as the backing store for LinerData2.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LinerData2Property =
            DependencyProperty.Register("LinerData2", typeof(double[,]), typeof(LinerChart), new PropertyMetadata(new double[,] { { 0, 0 } }, new PropertyChangedCallback(PropertyChanged)));



        public int fieldAA
        {
            get { return (int)GetValue(fieldAAProperty); }
            set { SetValue(fieldAAProperty, value); }
        }

        // Using a DependencyProperty as the backing store for fieldAA.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty fieldAAProperty =
            DependencyProperty.Register("fieldAA", typeof(int), typeof(LinerChart), new PropertyMetadata(0, new PropertyChangedCallback(PropertyChanged)));

        public double Mean
        {
            get { return (double)GetValue(MeanProperty); }
            set { SetValue(MeanProperty, value); }
        }
        public static readonly DependencyProperty MeanProperty = DependencyProperty.Register(nameof(Mean), typeof(double), typeof(ISIChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        //public TextBlock TextBlockInfo
        //{
        //    get { return (TextBlock)GetValue(TextBlockInfoProperty); }
        //    set { SetValue(TextBlockInfoProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty TextBlockInfoProperty =
        //    DependencyProperty.Register("TextBlockInfo", typeof(TextBlock), typeof(LinerChart), new PropertyMetadata(null));


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property.Equals(fieldAAProperty) || e.Property.Equals(LinerDataProperty) || e.Property.Equals(LinerData2Property))
            {
                DelayReRender(false);
                return;
            }
        }

        private readonly LinerPainter _painter=new LinerPainter();
        

        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            //double[,] data = new double[,] { 
            //    { 0.03, 0.012 }, 
            //    { 0.04, 0.019 },
            //    { 0.05, 0.025 },
            //    { 0.06, 0.043 },
            //    { 0.07, 0.058 },
            //    { 0.08, 0.072 },
            //    { 0.09, 0.085 },
            //    { 0.10, 0.097 },
            //    { 0.20, 0.23 },
            //    { 0.30, 0.34 },
            //    { 0.40, 0.44 },
            //    { 0.50, 0.53 },
            //    { 0.60, 0.60 },
            //    { 0.70, 0.66 },
            //    { 0.80, 0.71 },
            //    { 0.90, 0.76 },
            //    { 1.00, 0.79 },
            //    { 2.00, 0.93 },
            //    { 3.00, 0.97 },
            //    { 4.00, 0.98 },
            //    { 5.00, 0.99 },
            //    { 6.00, 0.99 },
            //    { 7.00, 0.99 },
            //    { 9.00, 1.00 },
            //    { 10.00, 1.00 },
            //    { 20.00, 1.00 },
            //    { 30.00, 1.00 },

            //};

            List<Point> pts = new List<Point>();

            for (int i = 0; i < LinerData.GetLength(0); i++)
            {
                var xValue = LinerData[i, 0];
                var yValue = Math.Round(DrawAreaHeight- LinerData[i, 1] * DrawAreaHeight, 2);

                if (liners.Any(_ => _.DisplayX == xValue))
                {
                    liners.Where(_ => _.DisplayX == xValue).FirstOrDefault().DisplayY1 = LinerData[i, 1];
                    liners.Where(_ => _.DisplayX == xValue).FirstOrDefault().ActualY = yValue;
                    Point p = new Point(liners.Where(_=>_.DisplayX==xValue).FirstOrDefault().ActualX, yValue);
                    if (!pts.Contains(p))
                        pts.Add(p);
                }

            }
            //绘图
            if(pts.Count > 0)
            {
                var geometry = new StreamGeometry();

                //
                using (var gctx = geometry.Open())
                {
                    gctx.BeginFigure(pts[0], false, false);
                    for (int i = 1; i < pts.Count; i++)
                    {
                        gctx.LineTo(pts[i], true, true);
                    }
                }

                //
                geometry.Freeze();

                //
                ctx.DrawGeometry(null, new Pen(LinerDataBrush, LINE_WIDTH), geometry);
            }

            List<Point> pts2 = new List<Point>();

            for (int i = 0; i < LinerData2.GetLength(0); i++)
            {
                var xValue = LinerData2[i, 0];
                var yValue = Math.Round(DrawAreaHeight - LinerData2[i, 1] * DrawAreaHeight, 2);

                if (liners.Any(_ => _.DisplayX == xValue))
                {
                    liners.Where(_ => _.DisplayX == xValue).FirstOrDefault().DisplayY2 = LinerData2[i, 1];
                    liners.Where(_ => _.DisplayX == xValue).FirstOrDefault().ActualY = yValue;
                    Point p = new Point(liners.Where(_ => _.DisplayX == xValue).FirstOrDefault().ActualX, yValue);
                    if (!pts2.Contains(p))
                        pts2.Add(p);
                }

            }
            //绘图
            if (pts2.Count > 0)
            {
                var geometry = new StreamGeometry();

                //
                using (var gctx = geometry.Open())
                {
                    gctx.BeginFigure(pts2[0], false, false);
                    for (int i = 1; i < pts2.Count; i++)
                    {
                        gctx.LineTo(pts2[i], true, true);
                    }
                }

                //
                geometry.Freeze();

                //
                ctx.DrawGeometry(null, new Pen(LinerData2Brush,LINE_WIDTH), geometry);
            }
        }
        private List<LinerDrawing> liners = new List<LinerDrawing>();
        protected override void PaintAxises(DrawingContext ctx)
        {
            if (SecondScalePosition.Count == 0)
            {
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.04, PositionXPercent = 0.13, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.05, PositionXPercent = 0.23, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.06, PositionXPercent = 0.3, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.07, PositionXPercent = 0.37, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.08, PositionXPercent = 0.43, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.09, PositionXPercent = 0.48, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.10, PositionXPercent = 0.52, IsDisplayLine = true, IsDisplayAxis = true });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.11, PositionXPercent = 0.55, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.12, PositionXPercent = 0.58, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.13, PositionXPercent = 0.61, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.14, PositionXPercent = 0.64, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.15, PositionXPercent = 0.67, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.16, PositionXPercent = 0.7, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.17, PositionXPercent = 0.73, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.18, PositionXPercent = 0.76, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.19, PositionXPercent = 0.78, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.20, PositionXPercent = 0.8, IsDisplayLine = true, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.21, PositionXPercent = 0.82, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.22, PositionXPercent = 0.84, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.23, PositionXPercent = 0.86, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.24, PositionXPercent = 0.88, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.25, PositionXPercent = 0.90, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.26, PositionXPercent = 0.92, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.27, PositionXPercent = 0.94, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.28, PositionXPercent = 0.96, IsDisplayLine = false, IsDisplayAxis = false });
                SecondScalePosition.Add(new ScaleInfo { ScaleX = 0.29, PositionXPercent = 0.98, IsDisplayLine = false, IsDisplayAxis = false });
            }
            ctx.DrawRectangle(Brushes.Transparent, _linePen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));

            int SplitCount = 6;
            var xstep = DrawAreaWidth / SplitCount;

            double lastScale = 0.0;
            double lastX = 0.0;

            for (int i = 0; i < SplitCount + 1; i++)
            {
                var x = Math.Ceiling(i * xstep + OriginPoint.X);
                var scaleValue = MinX * Math.Pow(10, i);
                var txt = this.CreateFormatText($"{scaleValue}",LineBrush);
                ctx.DrawLine(_linePen, new Point(x, 0), new Point(x, OriginPoint.Y));
                ctx.DrawText(txt, new Point(x - txt.Width / (i == SplitCount ? 1 : 2), OriginPoint.Y + 2 * TEXT_MARGIN));
                if (!liners.Any(_ => _.DisplayX == scaleValue))
                {
                    liners.Add(new LinerDrawing { DisplayX = scaleValue, ActualX = x });
                }
                else
                {
                    liners.Where(_ => _.DisplayX == scaleValue).FirstOrDefault().ActualX = x;
                }
                if (i > 0)
                {
                    for (int j = 0; j < SecondScalePosition.Count; j++)
                    {
                        ScaleInfo scaleInfo = SecondScalePosition[j];
                        if (scaleInfo.IsDisplayLine)
                        {
                            double sec = Math.Round(scaleInfo.ScaleX * Math.Pow(10, i - 1), 2);
                            double x2 = Math.Ceiling(lastX + scaleInfo.PositionXPercent * xstep);
                            ctx.DrawLine(_linePen, new Point(x2, 0), new Point(x2, OriginPoint.Y));

                            if (!liners.Any(_ => _.DisplayX == sec))
                            {
                                liners.Add(new LinerDrawing { DisplayX = sec, ActualX = x2 });
                            }
                            else
                            {
                                liners.Where(_ => _.DisplayX == sec).FirstOrDefault().ActualX = x2;
                            }
                            if (scaleInfo.IsDisplayAxis)
                            {
                                var txt2 = this.CreateFormatText($"{sec.ToString()}", LineBrush);
                                ctx.DrawText(txt2, new Point(x2 - txt.Width / (i == SplitCount ? 1 : 2), OriginPoint.Y + 2 * TEXT_MARGIN));
                            }
                        }
                    }

                }
                //Geometry aa= 
                //ctx.DrawGeometry()
                lastScale = scaleValue;
                lastX = x;
            }
            double[] yAxisArray = { 0.2, 0.4, 0.6, 0.8, 1 };
            for(int i = 0; i < yAxisArray.Length; i++) {
                var yAxis=Math.Round(DrawAreaHeight - yAxisArray[i] * DrawAreaHeight, 2);
                ctx.DrawLine(_linePen, new Point(OriginPoint.X, yAxis), new Point(OriginPoint.X+DrawAreaWidth, yAxis));
                var txt = this.CreateFormatText($"{yAxisArray[i]}", LineBrush);
                ctx.DrawText(txt, new Point(OriginPoint.X - txt.Width - 2 * TEXT_MARGIN, yAxis - ((i == yAxisArray.Length - 1 ? 0 : 4 * TEXT_MARGIN))));
            }
        }

        protected override void PaintHover(DrawingContext ctx)
        {
            //if (liners.Any(_ => MouseHoverX >= _.ActualX - 1 && MouseHoverX <= _.ActualX + 1))
            if(liners.Any(_=>_.ActualX>=MouseHoverX-0.5 && _.ActualX<=MouseHoverX+0.5))
            {
                LinerDrawing liner = liners.Where(_ => _.ActualX >= MouseHoverX - 0.5 && _.ActualX <= MouseHoverX + 0.5).FirstOrDefault();
                if (liner != null)
                {
                    //PaintInfoOnTopRight(ctx, $"Frequence = {liner.DisplayX}, Gain: {liner.DisplayY}");
                    var txt = this.CreateFormatText($"Frequence = {liner.DisplayX}, Gain1: {liner.DisplayY1}, Gain2: {liner.DisplayY2}");
                    ctx.DrawRectangle(Brushes.Black, null, new Rect(XAxisMaxPoint.X - 4 * TEXT_MARGIN - txt.Width, 10 - TEXT_MARGIN, txt.Width + 2 * TEXT_MARGIN, txt.Height + 2 * TEXT_MARGIN));
                    ctx.DrawText(txt, new Point(XAxisMaxPoint.X - 3 * TEXT_MARGIN - txt.Width, 10));
                    //TextBlockInfo.Text = $"Frequence = {liner.DisplayX}, Gain: {liner.DisplayY}";
                }
            }
            //var txt2 = this.CreateFormatText($"MouseHoverX = {MouseHoverX}");
            //ctx.DrawText(txt2, new Point(XAxisMaxPoint.X - 3 * TEXT_MARGIN - txt2.Width, 40));
        }
        protected override void PaintTitle(DrawingContext ctx)
        {
            
        }
    }

    public class LinerDrawing
    {
        public double DisplayX { get; set; }    
        public double DisplayY1 { get; set; }
        public double DisplayY2 { get; set; }
        public double ActualX { get; set; }
        public double ActualY { get; set;}
    }

    public class ScaleInfo
    {
        public double ScaleX { get; set; }
        public double PositionXPercent { get; set; }
        public bool IsDisplayAxis { get; set; }
        public bool IsDisplayLine { get; set; }
    }
}
