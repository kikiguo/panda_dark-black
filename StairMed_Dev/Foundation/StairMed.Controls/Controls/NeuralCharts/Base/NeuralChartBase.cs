﻿using StairMed.Controls.Logger;
using StairMed.Tools;
using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Base
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class NeuralChartBase : Control
    {
        /// <summary>
        /// 绘图线宽
        /// </summary>
        public const float LINE_WIDTH = 1;

        /// <summary>
        /// 文本留空
        /// </summary>
        public const int TEXT_MARGIN = 2;


        /// <summary>
        /// 刻度线长度
        /// </summary>
        public const int TICK_LINE_LENGTH = 5;

        public const int TEXT_Margin_Left = 9;
        public const int TEXT_Margin_Top = 2;

        /// <summary>
        /// 
        /// </summary>
        static NeuralChartBase()
        {
           
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NeuralChartBase), new FrameworkPropertyMetadata(typeof(NeuralChartBase)));
        }

        #region

        /// <summary>
        /// 数据线颜色
        /// </summary>
        public Brush LineBrush
        {
            get { return (Brush)GetValue(LineBrushProperty); }
            set { SetValue(LineBrushProperty, value); }
        }
        public static readonly DependencyProperty LineBrushProperty = DependencyProperty.Register(nameof(LineBrush), typeof(Brush), typeof(NeuralChartBase), new PropertyMetadata(Brushes.White, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        #region 绘图空间区域

        /// <summary>
        /// 绘图区域宽度
        /// </summary>
        public float DrawAreaWidth
        {
            get { return (float)GetValue(DrawAreaWidthProperty); }
            set { SetValue(DrawAreaWidthProperty, value); }
        }
        public static readonly DependencyProperty DrawAreaWidthProperty = DependencyProperty.Register(nameof(DrawAreaWidth), typeof(float), typeof(NeuralChartBase), new PropertyMetadata(100f, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 绘图区域高度
        /// </summary>
        public float DrawAreaHeight
        {
            get { return (float)GetValue(DrawAreaHeightProperty); }
            set { SetValue(DrawAreaHeightProperty, value); }
        }
        public static readonly DependencyProperty DrawAreaHeightProperty = DependencyProperty.Register(nameof(DrawAreaHeight), typeof(float), typeof(NeuralChartBase), new PropertyMetadata(1f, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 坐标原点
        /// </summary>
        public Point OriginPoint
        {
            get { return (Point)GetValue(OriginPointProperty); }
            set { SetValue(OriginPointProperty, value); }
        }
        public static readonly DependencyProperty OriginPointProperty = DependencyProperty.Register(nameof(OriginPoint), typeof(Point), typeof(NeuralChartBase), new PropertyMetadata(new Point(10d, 10d), new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// x轴最大值坐标
        /// </summary>
        public Point XAxisMaxPoint
        {
            get { return (Point)GetValue(XAxisMaxPointProperty); }
            set { SetValue(XAxisMaxPointProperty, value); }
        }
        public static readonly DependencyProperty XAxisMaxPointProperty = DependencyProperty.Register(nameof(XAxisMaxPoint), typeof(Point), typeof(NeuralChartBase), new PropertyMetadata(new Point(20d, 10d), new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// y轴最大值坐标
        /// </summary>
        public Point YAxisMaxPoint
        {
            get { return (Point)GetValue(YAxisMaxPointProperty); }
            set { SetValue(YAxisMaxPointProperty, value); }
        }
        public static readonly DependencyProperty YAxisMaxPointProperty = DependencyProperty.Register(nameof(YAxisMaxPoint), typeof(Point), typeof(NeuralChartBase), new PropertyMetadata(new Point(10d, 5d), new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 绘图区域:绘制波形的区域：不包括xtick, ytick区域
        /// </summary>
        public Rect DrawAreaRect
        {
            get { return (Rect)GetValue(DrawAreaRectProperty); }
            set { SetValue(DrawAreaRectProperty, value); }
        }
        public static readonly DependencyProperty DrawAreaRectProperty = DependencyProperty.Register(nameof(DrawAreaRect), typeof(Rect), typeof(NeuralChartBase), new PropertyMetadata(new Rect(0, 0, 10, 10), new PropertyChangedCallback(PropertyChanged)));

        #endregion




        #region 通道名称

        /// <summary>
        /// 通道名称
        /// </summary>
        public string ChannelName
        {
            get { return (string)GetValue(ChannelNameProperty); }
            set { SetValue(ChannelNameProperty, value); }
        }
        public static readonly DependencyProperty ChannelNameProperty = DependencyProperty.Register(nameof(ChannelName), typeof(string), typeof(NeuralChartBase), new PropertyMetadata("", new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 对应的输入通道号
        /// </summary>
        public int Channel
        {
            get { return (int)GetValue(ChannelProperty); }
            set { SetValue(ChannelProperty, value); }
        }
        public static readonly DependencyProperty ChannelProperty = DependencyProperty.Register(nameof(Channel), typeof(int), typeof(NeuralChartBase), new PropertyMetadata(-1, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        #region 鼠标hover位置

        /// <summary>
        /// 是否关心鼠标hover
        /// </summary>
        public bool IsCareMouseHover
        {
            get { return (bool)GetValue(IsCareMouseHoverProperty); }
            set { SetValue(IsCareMouseHoverProperty, value); }
        }
        public static readonly DependencyProperty IsCareMouseHoverProperty = DependencyProperty.Register(nameof(IsCareMouseHover), typeof(bool), typeof(NeuralChartBase), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 鼠标是否hover在控件上
        /// </summary>
        public bool IsMouseHover
        {
            get { return (bool)GetValue(IsMouseHoverProperty); }
            set { SetValue(IsMouseHoverProperty, value); }
        }
        public static readonly DependencyProperty IsMouseHoverProperty = DependencyProperty.Register(nameof(IsMouseHover), typeof(bool), typeof(NeuralChartBase), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 鼠标是否hover在控件上
        /// </summary>
        public bool IsMouseClick
        {
            get { return (bool)GetValue(IsMouseClickProperty); }
            set { SetValue(IsMouseClickProperty, value); }
        }
        public static readonly DependencyProperty IsMouseClickProperty = DependencyProperty.Register(nameof(IsMouseClick), typeof(bool), typeof(NeuralChartBase), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));



        /// <summary>
        /// 鼠标X位置
        /// </summary>
        public double MouseHoverX
        {
            get { return (double)GetValue(MouseHoverXProperty); }
            set { SetValue(MouseHoverXProperty, value); }
        }
        public static readonly DependencyProperty MouseHoverXProperty = DependencyProperty.Register(nameof(MouseHoverX), typeof(double), typeof(NeuralChartBase), new PropertyMetadata(double.MinValue, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 鼠标Y位置
        /// </summary>
        public double MouseHoverY
        {
            get { return (double)GetValue(MouseYProperty); }
            set { SetValue(MouseYProperty, value); }
        }
        public static readonly DependencyProperty MouseYProperty = DependencyProperty.Register(nameof(MouseHoverY), typeof(double), typeof(NeuralChartBase), new PropertyMetadata(double.MinValue, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 鼠标Y位置
        /// </summary>
        public double RmsMouseHoverY
        {
            get { return (double)GetValue(RmsMouseHoverYProperty); }
            set { SetValue(RmsMouseHoverYProperty, value); }
        }
        public static readonly DependencyProperty RmsMouseHoverYProperty = DependencyProperty.Register(nameof(RmsMouseHoverY), typeof(double), typeof(NeuralChartBase), new PropertyMetadata(double.MinValue, new PropertyChangedCallback(PropertyChanged)));



        #endregion



        #region 视窗：y轴范围

        /// <summary>
        /// Y轴最大值:与value保持同一单位
        /// </summary>
        public float MaxY
        {
            get { return (float)GetValue(MaxYProperty); }
            set { SetValue(MaxYProperty, value); }
        }
        public static readonly DependencyProperty MaxYProperty = DependencyProperty.Register(nameof(MaxY), typeof(float), typeof(NeuralChartBase), new PropertyMetadata(100f, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Y轴最小值:与value保持同一单位
        /// </summary>
        public float MinY
        {
            get { return (float)GetValue(MinYProperty); }
            set { SetValue(MinYProperty, value); }
        }
        public static readonly DependencyProperty MinYProperty = DependencyProperty.Register(nameof(MinY), typeof(float), typeof(NeuralChartBase), new PropertyMetadata(-100f, new PropertyChangedCallback(PropertyChanged)));

        #endregion



        #region 视窗：x轴范围

        /// <summary>
        /// X轴最大值:与value保持同一单位
        /// </summary>
        public double MaxX
        {
            get { return (double)GetValue(MaxXProperty); }
            set { SetValue(MaxXProperty, value); }
        }
        public static readonly DependencyProperty MaxXProperty = DependencyProperty.Register(nameof(MaxX), typeof(double), typeof(NeuralChartBase), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// X轴最小值:与value保持同一单位
        /// </summary>
        public double MinX
        {
            get { return (double)GetValue(MinXProperty); }
            set { SetValue(MinXProperty, value); }
        }
        public static readonly DependencyProperty MinXProperty = DependencyProperty.Register(nameof(MinX), typeof(double), typeof(NeuralChartBase), new PropertyMetadata(-100d, new PropertyChangedCallback(PropertyChanged)));

        #endregion



        #region 属性变化监听


        /// <summary>
        /// 数据源变化
        /// </summary>
        protected static void PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
         
            if (sender is NeuralChartBase neuralChart)
            {
                //刷新
                //neuralChart.DelayReRender();
            }
        }

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(ActualHeightProperty) || e.Property.Equals(ActualWidthProperty) || e.Property.Equals(PaddingProperty))
            {
                //
                OriginPoint = new Point { X = this.MakeLineThin(Padding.Left), Y = this.MakeLineThin(ActualHeight - Padding.Bottom) };
                XAxisMaxPoint = new Point { X = this.MakeLineThin(ActualWidth - Padding.Right), Y = OriginPoint.Y };
                YAxisMaxPoint = new Point { X = OriginPoint.X, Y = this.MakeLineThin(Padding.Top) };

                //
                DrawAreaWidth = Convert.ToSingle(XAxisMaxPoint.X - OriginPoint.X);
                DrawAreaHeight = Convert.ToSingle(OriginPoint.Y - YAxisMaxPoint.Y);

                //
                if (DrawAreaWidth > 0 && DrawAreaHeight > 0)
                {
                    DrawAreaRect = new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight);
                }
                else
                {
                    DrawAreaRect = new Rect(OriginPoint.X, YAxisMaxPoint.Y, 0, 0);
                }

                //
                UpdateYRatio();

                //
                UpdateXRatio();

                //
                DelayReRender(true);
                return;
            }

            //
            if (e.Property.Equals(LineBrushProperty))
            {
                if (e.NewValue != null)
                {
                    _linePen = new Pen((Brush)e.NewValue, LINE_WIDTH);
                    if (_linePen.CanFreeze)
                    {
                        _linePen.Freeze();
                    }
                }
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(ForegroundProperty))
            {
                if (e.NewValue != null)
                {
                    _foregroundPen = new Pen((Brush)e.NewValue, LINE_WIDTH);
                    if (_foregroundPen.CanFreeze)
                    {
                        _foregroundPen.Freeze();
                    }

                    //
                    UpdateChannelName();
                    DelayReRender();
                }
                return;
            }

            //
            if (e.Property.Equals(MaxYProperty) || e.Property.Equals(MinYProperty))
            {
                UpdateYRatio();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(MaxXProperty) || e.Property.Equals(MinXProperty))
            {
                UpdateXRatio();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(ChannelNameProperty) || e.Property.Equals(FontSizeProperty))
            {
                UpdateChannelName();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(BackgroundProperty))
            {
                _bgColor = ((SolidColorBrush)Background).Color.ToColor();
                DelayReRender();
                return;
            }

            //
            //DelayReRender();
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void UpdateYRatio()
        {
            if (MaxY > MinY)
            {
                _yratio = Convert.ToSingle(DrawAreaHeight / (MaxY - MinY));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void UpdateXRatio()
        {
            if (MaxX > MinX)
            {
                _xratio = DrawAreaWidth / (MaxX - MinX);
            }
        }

        #endregion





        #region buffer data：avoid repeat

        /// <summary>
        /// 各种画笔
        /// </summary>
        public readonly Pen _limePen = new Pen(Brushes.Lime, LINE_WIDTH);
        public readonly Pen _scaleBarPen = new Pen(Brushes.LightGray, LINE_WIDTH);
        public readonly Pen _yellowPen = new Pen(Brushes.Yellow, LINE_WIDTH);
        public readonly Pen _whitePen = new Pen(Brushes.White, LINE_WIDTH);
        public readonly Pen _dimGrayPen = new Pen(Brushes.DimGray, LINE_WIDTH);
        public readonly Pen _redPen = new Pen(Brushes.Red, LINE_WIDTH);

        public readonly Pen _baseLinePen = new Pen(new SolidColorBrush(Color.FromArgb(0x80, 0x00, 0xFF, 0xA4)), LINE_WIDTH);
        public readonly Pen _baseLinePenFull = new Pen(new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xFF, 0xA4)), LINE_WIDTH);

        /// <summary>
        /// 通道名称
        /// </summary>
        public FormattedText _channelNameTxt = null;

        /// <summary>
        /// height/(maxy-miny)
        /// </summary>
        public float _yratio = 1.0f;

        /// <summary>
        /// x轴比例:with / (maxy-miny)
        /// </summary>
        public double _xratio = 5;

        /// <summary>
        /// 
        /// </summary>
        protected Pen _linePen = new Pen(Brushes.White, LINE_WIDTH);
        protected Pen _foregroundPen = new Pen(Brushes.White, LINE_WIDTH);
        protected Pen _blackPen = new Pen(Brushes.Black, LINE_WIDTH);

        /// <summary>
        /// 
        /// </summary>
        public System.Drawing.Color _bgColor = System.Drawing.Color.Black;

        #endregion



        /// <summary>
        /// 延迟刷新控制
        /// </summary>
        private readonly Timer _delayRenderTimer = new Timer { Interval = 5, AutoReset = false, };

        /// <summary>
        /// 
        /// </summary>
        public NeuralChartBase()
        {
            _delayRenderTimer.Elapsed += (sender, e) =>
            {
                UIHelper.BeginInvoke(new Action(() =>
                {
                    InvalidateVisual();
                }));
            };
        }

        /// <summary>
        /// 延迟刷新，将单个控件极短时间内的多次刷新转为一次刷新，避免短时间内多次刷新造成卡顿
        /// </summary>
        protected virtual void DelayReRender(bool sizeChanged = false)
        {
            try
            {
                _delayRenderTimer.Stop();
                _delayRenderTimer.Start();
            }
            catch { }
        }








        #region 绘制

        /// <summary>
        /// 重绘
        /// </summary>
        /// <param name="drawingContext"></param>
        protected override void OnRender(DrawingContext ctx)
        {
            try
            {
                //绘制背景
                ctx.DrawRectangle(Background, null, new Rect { X = 0, Y = 0, Width = ActualWidth, Height = ActualHeight });

                //画图区域为空
                if (DrawAreaWidth <= 0 || DrawAreaHeight <= 0)
                {
                    return;
                }

                //
                DoRealPaint(ctx);
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{nameof(OnRender)}:{ChannelName}", ex);
            }
        }

        /// <summary>
        /// 绘制过程
        /// </summary>
        /// <param name="ctx"></param>
        protected virtual void DoRealPaint(DrawingContext ctx)
        {

            //绘制数据
            PaintDataGraphics(ctx);

            //绘制x and y轴
            PaintAxises(ctx);

            //如果鼠标hover上了，绘制hover态
            if (IsCareMouseHover && IsMouseHover)
            {
                PaintHover(ctx);
            }

            //绘制标题
            PaintTitle(ctx);
        }


        #region 分步绘制：由子类实现

        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected virtual void PaintDataGraphics(DrawingContext ctx)
        {

        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected virtual void PaintAxises(DrawingContext ctx)
        {

        }

        /// <summary>
        /// 绘制Hover状态
        /// </summary>
        /// <param name="ctx"></param>
        protected virtual void PaintHover(DrawingContext ctx)
        {

        }

        /// <summary>
        /// 绘制标题信息等
        /// </summary>
        /// <param name="ctx"></param>
        protected virtual void PaintTitle(DrawingContext ctx)
        {

        }

        #endregion


        #region 绘制边框及标题文本等

        /// <summary>
        /// 更新通道名称
        /// </summary>
        protected virtual void UpdateChannelName()
        {
            _channelNameTxt = this.CreateFormatText($"{ChannelName}");
        }

        /// <summary>
        /// 在方框外的左上角绘制：绘制通道名称
        /// </summary>
        /// <param name="ctx"></param>
        protected void PaintChannelNameOnOuterTopLeft(DrawingContext ctx)
        {
            ctx.DrawText(_channelNameTxt, new Point(OriginPoint.X + TEXT_MARGIN, YAxisMaxPoint.Y - TEXT_MARGIN - _channelNameTxt.Height));
        }

        /// <summary>
        /// 绘制Y轴标题标题
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="title"></param>
        protected void PaintTitleOnLeft(DrawingContext ctx, string title)
        {
            var rotate = new RotateTransform(-90);
            ctx.PushTransform(rotate);
            var txt = this.CreateFormatText(title);
            var x = -(OriginPoint.Y + YAxisMaxPoint.Y + txt.Width) / 2;
            ctx.DrawText(txt, new Point(x, TEXT_MARGIN));
            ctx.Pop();
        }

        /// <summary>
        /// 在方框外的右上角绘制：动态信息
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="title"></param>
        protected void PaintInfoOnTopRight(DrawingContext ctx, string info)
        {
            var txt = this.CreateFormatText(info);
            ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - 2 * TEXT_MARGIN, YAxisMaxPoint.Y + TEXT_MARGIN));
            //ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - 2 * TEXT_MARGIN, YAxisMaxPoint.Y - TEXT_MARGIN - txt.Height));
        }

        #endregion


        #endregion





        #region 鼠标移动

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            if (!IsCareMouseHover)
            {
                return;
            }

            //
            JudgeMouseHover(e);
            DelayReRender();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonDown(e);
            if (!IsCareMouseHover)
            {
                return;
            }

            IsMouseClick = true;
            CMouseHover(e);
            //
            // JudgeMouseHover(e);
            DelayReRender();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonUp(e);
            if (!IsCareMouseHover)
            {
                return;
            }

            IsMouseClick = false;

            //
            // JudgeMouseHover(e);
            //  DelayReRender();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void JudgeMouseHover(MouseEventArgs e)
        {
            if (!IsCareMouseHover)
            {
                return;
            }

            //
            var pt = e.GetPosition(this);
            if (DrawAreaRect.Contains(pt.X, pt.Y))
            {
                IsMouseHover = true;
                MouseHoverX = pt.X;
                MouseHoverY = pt.Y;
            }
            else
            {
                IsMouseHover = false;
                MouseHoverX = int.MinValue;
                MouseHoverY = int.MinValue;
            }
            base.OnMouseEnter(e);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected void CMouseHover(MouseEventArgs e)
        {
            if (!IsCareMouseHover)
            {
                return;
            }

            //
            var pt = e.GetPosition(this);
            if (DrawAreaRect.Contains(pt.X, pt.Y))
            {

                RmsMouseHoverY = pt.Y;
            }
            else
            {
                RmsMouseHoverY = int.MinValue;
            }
            base.OnMouseEnter(e);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            if (!IsCareMouseHover)
            {
                return;
            }

            //
            IsMouseHover = false;
            DelayReRender();
        }


     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (!IsCareMouseHover)
            {
                return;
            }

            //
            JudgeMouseHover(e);
            DelayReRender();
        }

        #endregion
    }
}
