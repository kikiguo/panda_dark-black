﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Base
{
    /// <summary>
    /// 
    /// </summary>
    internal class DoubleYChart : Control
    {
        #region 左Y轴展示

        /// <summary>
        /// 展示左坐标轴
        /// </summary>
        public bool ShowYAxisLeft
        {
            get { return (bool)GetValue(ShowYAxisLeftProperty); }
            set { SetValue(ShowYAxisLeftProperty, value); }
        }
        public static readonly DependencyProperty ShowYAxisLeftProperty = DependencyProperty.Register(nameof(ShowYAxisLeft), typeof(bool), typeof(DoubleYChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 展示左Y轴刻度
        /// </summary>
        public bool ShowYTickLeft
        {
            get { return (bool)GetValue(ShowYTickLeftProperty); }
            set { SetValue(ShowYTickLeftProperty, value); }
        }
        public static readonly DependencyProperty ShowYTickLeftProperty = DependencyProperty.Register(nameof(ShowYTickLeft), typeof(bool), typeof(DoubleYChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴刻度是否都是整形
        /// </summary>
        public bool IsYTickIntLeft
        {
            get { return (bool)GetValue(IsYTickIntLeftProperty); }
            set { SetValue(IsYTickIntLeftProperty, value); }
        }
        public static readonly DependencyProperty IsYTickIntLeftProperty = DependencyProperty.Register(nameof(IsYTickIntLeft), typeof(bool), typeof(DoubleYChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴刻度间隔
        /// </summary>
        public double YTickStepLeft
        {
            get { return (double)GetValue(YTickStepLeftProperty); }
            set { SetValue(YTickStepLeftProperty, value); }
        }
        public static readonly DependencyProperty YTickStepLeftProperty = DependencyProperty.Register(nameof(YTickStepLeft), typeof(double), typeof(DoubleYChart), new PropertyMetadata(1D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴刻度基准水平线
        /// </summary>
        public double YTickBaseLeft
        {
            get { return (double)GetValue(YTickBaseLeftProperty); }
            set { SetValue(YTickBaseLeftProperty, value); }
        }
        public static readonly DependencyProperty YTickBaseLeftProperty = DependencyProperty.Register(nameof(YTickBaseLeft), typeof(double), typeof(DoubleYChart), new PropertyMetadata(0D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴刻度单位
        /// </summary>
        public string YTickUnitLeft
        {
            get { return (string)GetValue(YTickUnitLeftProperty); }
            set { SetValue(YTickUnitLeftProperty, value); }
        }
        public static readonly DependencyProperty YTickUnitLeftProperty = DependencyProperty.Register(nameof(YTickUnitLeft), typeof(string), typeof(DoubleYChart), new PropertyMetadata("", new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴刻度格式
        /// </summary>
        public string YTickFormatLeft
        {
            get { return (string)GetValue(YTickFormatLeftProperty); }
            set { SetValue(YTickFormatLeftProperty, value); }
        }
        public static readonly DependencyProperty YTickFormatLeftProperty = DependencyProperty.Register(nameof(YTickFormatLeft), typeof(string), typeof(DoubleYChart), new PropertyMetadata("0", new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴单位下最大值
        /// </summary>
        public double MaxYLeft
        {
            get { return (double)GetValue(MaxYLeftProperty); }
            set { SetValue(MaxYLeftProperty, value); }
        }
        public static readonly DependencyProperty MaxYLeftProperty = DependencyProperty.Register(nameof(MaxYLeft), typeof(double), typeof(DoubleYChart), new PropertyMetadata(1024D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴单位下最小值
        /// </summary>
        public double MinYLeft
        {
            get { return (double)GetValue(MinYLeftProperty); }
            set { SetValue(MinYLeftProperty, value); }
        }
        public static readonly DependencyProperty MinYLeftProperty = DependencyProperty.Register(nameof(MinYLeft), typeof(double), typeof(DoubleYChart), new PropertyMetadata(-1024D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 左Y轴刻度水平线颜色
        /// </summary>
        public Brush YTickLineBrushLeft
        {
            get { return (Brush)GetValue(YTickLineBrushLeftProperty); }
            set { SetValue(YTickLineBrushLeftProperty, value); }
        }
        public static readonly DependencyProperty YTickLineBrushLeftProperty = DependencyProperty.Register(nameof(YTickLineBrushLeft), typeof(Brush), typeof(DoubleYChart), new PropertyMetadata(Brushes.White, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        #region 右Y轴展示

        /// <summary>
        /// 展示右坐标轴
        /// </summary>
        public bool ShowYAxisRight
        {
            get { return (bool)GetValue(ShowYAxisRightProperty); }
            set { SetValue(ShowYAxisRightProperty, value); }
        }
        public static readonly DependencyProperty ShowYAxisRightProperty = DependencyProperty.Register(nameof(ShowYAxisRight), typeof(bool), typeof(DoubleYChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 展示右Y轴刻度
        /// </summary>
        public bool ShowYTickRight
        {
            get { return (bool)GetValue(ShowYTickRightProperty); }
            set { SetValue(ShowYTickRightProperty, value); }
        }
        public static readonly DependencyProperty ShowYTickRightProperty = DependencyProperty.Register(nameof(ShowYTickRight), typeof(bool), typeof(DoubleYChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴刻度是否都是整形
        /// </summary>
        public bool IsYTickIntRight
        {
            get { return (bool)GetValue(IsYTickIntRightProperty); }
            set { SetValue(IsYTickIntRightProperty, value); }
        }
        public static readonly DependencyProperty IsYTickIntRightProperty = DependencyProperty.Register(nameof(IsYTickIntRight), typeof(bool), typeof(DoubleYChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴刻度间隔
        /// </summary>
        public double YTickStepRight
        {
            get { return (double)GetValue(YTickStepRightProperty); }
            set { SetValue(YTickStepRightProperty, value); }
        }
        public static readonly DependencyProperty YTickStepRightProperty = DependencyProperty.Register(nameof(YTickStepRight), typeof(double), typeof(DoubleYChart), new PropertyMetadata(1D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴基准水平线
        /// </summary>
        public double YTickBaseRight
        {
            get { return (double)GetValue(YTickBaseRightProperty); }
            set { SetValue(YTickBaseRightProperty, value); }
        }
        public static readonly DependencyProperty YTickBaseRightProperty = DependencyProperty.Register(nameof(YTickBaseRight), typeof(double), typeof(DoubleYChart), new PropertyMetadata(0D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴刻度单位
        /// </summary>
        public string YTickUnitRight
        {
            get { return (string)GetValue(YTickUnitRightProperty); }
            set { SetValue(YTickUnitRightProperty, value); }
        }
        public static readonly DependencyProperty YTickUnitRightProperty = DependencyProperty.Register(nameof(YTickUnitRight), typeof(string), typeof(DoubleYChart), new PropertyMetadata("", new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴刻度格式
        /// </summary>
        public string YTickFormatRight
        {
            get { return (string)GetValue(YTickFormatRightProperty); }
            set { SetValue(YTickFormatRightProperty, value); }
        }
        public static readonly DependencyProperty YTickFormatRightProperty = DependencyProperty.Register(nameof(YTickFormatRight), typeof(string), typeof(DoubleYChart), new PropertyMetadata("0", new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴单位下最大值
        /// </summary>
        public double MaxYRight
        {
            get { return (double)GetValue(MaxYRightProperty); }
            set { SetValue(MaxYRightProperty, value); }
        }
        public static readonly DependencyProperty MaxYRightProperty = DependencyProperty.Register(nameof(MaxYRight), typeof(double), typeof(DoubleYChart), new PropertyMetadata(1024D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴单位下最小值
        /// </summary>
        public double MinYRight
        {
            get { return (double)GetValue(MinYRightProperty); }
            set { SetValue(MinYRightProperty, value); }
        }
        public static readonly DependencyProperty MinYRightProperty = DependencyProperty.Register(nameof(MinYRight), typeof(double), typeof(DoubleYChart), new PropertyMetadata(-1024D, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 右Y轴刻度水平线颜色
        /// </summary>
        public Brush YTickLineBrushRight
        {
            get { return (Brush)GetValue(YTickLineBrushRightProperty); }
            set { SetValue(YTickLineBrushRightProperty, value); }
        }
        public static readonly DependencyProperty YTickLineBrushRightProperty = DependencyProperty.Register(nameof(YTickLineBrushRight), typeof(Brush), typeof(DoubleYChart), new PropertyMetadata(Brushes.White, new PropertyChangedCallback(PropertyChanged)));

        #endregion

        /// <summary>
        /// 数据源变化
        /// </summary>
        protected static void PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender is NeuralChartBase neuralChart)
            {
                neuralChart.InvalidateVisual();
            }
        }

    }
}
