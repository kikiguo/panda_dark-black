﻿using StairMed.Controls.Logger;
using StairMed.Tools;
using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Base
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class NeuralChartWithContainer : NeuralChartBase
    {
        /// <summary>
        /// 方便计算控件是否在scrollviewer的可见区域中
        /// </summary>
        public readonly Point LEFT_TOP_ZERO_POINT = new Point(0, 0);


        #region 属性

        /// <summary>
        /// 父滚动视图容器
        /// </summary>
        public ScrollViewer ScrollViewContainer
        {
            get { return (ScrollViewer)GetValue(ScrollViewContainerProperty); }
            set { SetValue(ScrollViewContainerProperty, value); }
        }
        public static readonly DependencyProperty ScrollViewContainerProperty = DependencyProperty.Register(nameof(ScrollViewContainer), typeof(ScrollViewer), typeof(NeuralChartWithContainer), new PropertyMetadata(null, new PropertyChangedCallback(ScrollViewContainerPropertyChanged)));

        /// <summary>
        /// 是否在可见区域（例如放在了一个滚动容器中）
        /// </summary>
        public bool IsVisibleToUser
        {
            get { return (bool)GetValue(IsVisibleToUserProperty); }
            set { SetValue(IsVisibleToUserProperty, value); }
        }
        public static readonly DependencyProperty IsVisibleToUserProperty = DependencyProperty.Register(nameof(IsVisibleToUser), typeof(bool), typeof(NeuralChartWithContainer), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        /// <summary>
        /// 延迟刷新控制
        /// </summary>
        private readonly Timer _delayRenderTimer = new Timer
        {
            Interval = 5, //在scrollview中由不可见到可见时延迟重绘
            AutoReset = false,
        };

        /// <summary>
        /// 尺寸变化后是否有重新计算控件是否可见
        /// </summary>
        private bool _sizeChangedHandled = true;

        /// <summary>
        /// 
        /// </summary>
        public NeuralChartWithContainer()
        {
            _delayRenderTimer.Elapsed += (sender, e) =>
            {
                UIHelper.BeginInvoke(new Action(() =>
                {
                    if (!_sizeChangedHandled)
                    {
                        UpdateIsVisibleToUserState();
                    }
                    InvalidateVisual();
                }));
            };

            //
            this.Unloaded += NeuralChartWithContainer_Unloaded;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NeuralChartWithContainer_Unloaded(object sender, RoutedEventArgs e)
        {
            if (ScrollViewContainer != null)
            {
                UnbindScrollViewer(ScrollViewContainer);
            }
        }

        /// <summary>
        /// ScrollViewContainer数据源变化
        /// </summary>
        protected static void ScrollViewContainerPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            if (sender is NeuralChartWithContainer chart)
            {
                //刷新
                chart.DelayReRender(true);
                chart.UnbindScrollViewer(args.OldValue as ScrollViewer);
                chart.BindScrollViewer(args.NewValue as ScrollViewer);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scrollViewer"></param>
        private void UnbindScrollViewer(ScrollViewer scrollViewer)
        {
            if (scrollViewer != null)
            {
                scrollViewer.ScrollChanged -= ScrollView_ScrollChanged;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scrollViewer"></param>
        private void BindScrollViewer(ScrollViewer scrollViewer)
        {
            UnbindScrollViewer(scrollViewer);

            //
            if (scrollViewer != null)
            {
                scrollViewer.ScrollChanged += ScrollView_ScrollChanged;
            }
        }

        /// <summary>
        /// scollviewer滚动，sizechanged也会触发scrollchanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollView_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            this.DelayReRender(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(VisibilityProperty) || e.Property.Equals(IsVisibleProperty))
            {
                if (!IsVisible)
                {
                    IsVisibleToUser = false;
                }

                //
                DelayReRender(true);
                return;
            }

            //
            if (e.Property.Equals(ScrollViewContainerProperty))
            {
                DelayReRender(true);
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void DelayReRender(bool sizeChanged = false)
        {
            _sizeChangedHandled = _sizeChangedHandled && !sizeChanged;
            try
            {
                _delayRenderTimer.Stop();
                _delayRenderTimer.Start();
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{nameof(DelayReRender)}", ex);
            }
        }

        //private long _count = 0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void OnRender(DrawingContext ctx)
        {
            try
            {
                //
                base.OnRender(ctx);

                //
                if (!IsVisibleToUser)
                {
                    return;
                }

                //
                //_count++;
                //if (_count % 50 == 0)
                //{
                //    Logger.LogTool.Logger.LogD($"OnRender:{ChannelName}");
                //}

                //绘制背景
                ctx.DrawRectangle(Background, null, new Rect { X = 0, Y = 0, Width = ActualWidth, Height = ActualHeight });

                //画图区域为空
                if (DrawAreaWidth <= 0 || DrawAreaHeight <= 0)
                {
                    return;
                }

                //
                DoRealPaint(ctx);
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{nameof(OnRender)}:{ChannelName}", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateIsVisibleToUserState()
        {
            _sizeChangedHandled = true;

            //
            try
            {
                //
                if ((PresentationSource.FromVisual(this) == null) || (!IsVisible) || (Visibility != Visibility.Visible) || (!IsLoaded))
                {
                    IsVisibleToUser = false;
                    return;
                }

                //
                if (ScrollViewContainer == null)
                {
                    IsVisibleToUser = true;
                    return;
                }

                //
                var leftTop = PointToScreen(LEFT_TOP_ZERO_POINT);
                var rightBotton = PointToScreen(new Point(ActualWidth, ActualHeight));
                var rect = new Rect(leftTop.X, leftTop.Y, rightBotton.X - leftTop.X, rightBotton.Y - leftTop.Y);

                //
                var containerLeftTop = ScrollViewContainer.PointToScreen(LEFT_TOP_ZERO_POINT);
                var containerRightBottom = ScrollViewContainer.PointToScreen(new Point(ScrollViewContainer.ActualWidth, ScrollViewContainer.ActualHeight));
                var containerRect = new Rect(containerLeftTop.X, containerLeftTop.Y, containerRightBottom.X - containerLeftTop.X, containerRightBottom.Y - containerLeftTop.Y);

                //
                IsVisibleToUser = containerRect.IntersectsWith(rect);
                return;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{nameof(UpdateIsVisibleToUserState)}", ex);
            }

            //
            IsVisibleToUser = true;
        }
    }
}
