﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Controls.Controls.NeuralCharts.FilterCharts;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Xml.Serialization;
using Accord.Statistics.Visualizations;

namespace StairMed.Controls.Controls.NeuralCharts.FilterCharts
{
    public partial class FilterChart : NeuralChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        static FilterChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FilterChart), new FrameworkPropertyMetadata(typeof(FilterChart)));
        }


        #region 属性

        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public int TimeScaleMS
        {
            get { return (int)GetValue(TimeScaleMSProperty); }
            set { SetValue(TimeScaleMSProperty, value); }
        }
        public static readonly DependencyProperty TimeScaleMSProperty = DependencyProperty.Register(nameof(TimeScaleMS), typeof(int), typeof(FilterChart), new PropertyMetadata(1000, new PropertyChangedCallback(PropertyChanged)));



  

        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public bool FilterCutoffLow
        {
            get { return (bool)GetValue(FilterCutoffLowProperty); }
            set { SetValue(FilterCutoffLowProperty, value); }
        }
        public static readonly DependencyProperty FilterCutoffLowProperty = DependencyProperty.Register(nameof(FilterCutoffLow), typeof(bool), typeof(FilterChart), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public bool FilterCutoffHig
        {
            get { return (bool)GetValue(FilterCutoffHigProperty); }
            set { SetValue(FilterCutoffHigProperty, value); }
        }
        public static readonly DependencyProperty FilterCutoffHigProperty = DependencyProperty.Register(nameof(FilterCutoffHig), typeof(bool), typeof(FilterChart), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public bool FilterCutoffBand
        {
            get { return (bool)GetValue(FilterCutoffBandProperty); }
            set { SetValue(FilterCutoffBandProperty, value); }
        }
        public static readonly DependencyProperty FilterCutoffBandProperty = DependencyProperty.Register(nameof(FilterCutoffBand), typeof(bool), typeof(FilterChart), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));





      
        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public int LowCutoff
        {
            get { return (int)GetValue(LowCutoffProperty); }
            set { SetValue(LowCutoffProperty, value); }
        }
        public static readonly DependencyProperty LowCutoffProperty = DependencyProperty.Register(nameof(LowCutoff), typeof(int), typeof(FilterChart), new PropertyMetadata(100, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public int HigCutoff
        {
            get { return (int)GetValue(HigCutoffProperty); }
            set { SetValue(HigCutoffProperty, value); }
        }
        public static readonly DependencyProperty HigCutoffProperty = DependencyProperty.Register(nameof(HigCutoff), typeof(int), typeof(FilterChart), new PropertyMetadata(100, new PropertyChangedCallback(PropertyChanged)));




        /// <summary>
        /// 步长
        /// </summary>
        public int BinSizeMS
        {
            get { return (int)GetValue(BinSizeMSProperty); }
            set { SetValue(BinSizeMSProperty, value); }
        }
        public static readonly DependencyProperty BinSizeMSProperty = DependencyProperty.Register(nameof(BinSizeMS), typeof(int), typeof(FilterChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Y轴：true：线性，false：log
        /// </summary>
        public bool LinearScale
        {
            get { return (bool)GetValue(LinearScaleProperty); }
            set { SetValue(LinearScaleProperty, value); }
        }
        public static readonly DependencyProperty LinearScaleProperty = DependencyProperty.Register(nameof(LinearScale), typeof(bool), typeof(FilterChart), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        public List<float> LowHistograms
        {
            get { return (List<float>)GetValue(LowHistogramsProperty); }
            set { SetValue(LowHistogramsProperty, value); }
        }
        public static readonly DependencyProperty LowHistogramsProperty = DependencyProperty.Register(nameof(LowHistograms), typeof(List<float>), typeof(FilterChart), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// 
        /// </summary>
        public List<float> HigHistograms
        {
            get { return (List<float>)GetValue(HigHistogramsProperty); }
            set { SetValue(HigHistogramsProperty, value); }
        }
        public static readonly DependencyProperty HigHistogramsProperty = DependencyProperty.Register(nameof(HigHistograms), typeof(List<float>), typeof(FilterChart), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// 个数
        /// </summary>
        public int FilterCount
        {
            get { return (int)GetValue(FilterCountProperty); }
            set { SetValue(FilterCountProperty, value); }
        }
        public static readonly DependencyProperty FilterCountProperty = DependencyProperty.Register(nameof(FilterCount), typeof(int), typeof(FilterChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 均值
        /// </summary>
        public double Mean
        {
            get { return (double)GetValue(MeanProperty); }
            set { SetValue(MeanProperty, value); }
        }
        public static readonly DependencyProperty MeanProperty = DependencyProperty.Register(nameof(Mean), typeof(double), typeof(FilterChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 频率
        /// </summary>
        public double FilterFreq
        {
            get { return (double)GetValue(FilterFreqProperty); }
            set { SetValue(FilterFreqProperty, value); }
        }
        public static readonly DependencyProperty FilterFreqProperty = DependencyProperty.Register(nameof(FilterFreq), typeof(double), typeof(FilterChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 标准差
        /// </summary>
        public double StdDev
        {
            get { return (double)GetValue(StdDevProperty); }
            set { SetValue(StdDevProperty, value); }
        }
        public static readonly DependencyProperty StdDevProperty = DependencyProperty.Register(nameof(StdDev), typeof(double), typeof(FilterChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(TimeScaleMSProperty) || e.Property.Equals(BinSizeMSProperty) || e.Property.Equals(LinearScaleProperty) || e.Property.Equals(LowHistogramsProperty) || e.Property.Equals(HigHistogramsProperty) || e.Property.Equals(FilterCountProperty) || e.Property.Equals(MeanProperty) || e.Property.Equals(FilterFreqProperty) || e.Property.Equals(StdDevProperty))
            {
                DelayReRender(false);
                return;
            }
        }


        //绘图工具
        private readonly FilterPainter _painter = new FilterPainter();

        //
        public float maxYVal = 0.0f;
        public float yValStep = 0.0f;
        public int power = 0;
        public int yLevelCount = 10;

        //
        public double maxLogYVal = 0.0;
        public double minYVal = 0.00009;
        public double minLogYVal = 0.0;

        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            if (LowHistograms == null || LowHistograms.Count <= 0)
            {
                return;
            }


            if (HigHistograms == null || HigHistograms.Count <= 0)
            {
                return;
            }

            //
            if (LinearScale)
            {
                PaintGraphicsWithLinearY(ctx);
            }
            else
            {
                PaintGraphicsWithLogY(ctx);
            }
        }

   

         
        /// <summary>
        /// 以对数绘制Y轴
        /// </summary>
        /// <param name="ctx"></param>
        private void PaintGraphicsWithLogY(DrawingContext ctx)
        {
            maxLogYVal = 0.0;
            minYVal = 0.00009;
            minLogYVal = 0.0;
            maxYVal = LowHistograms.Max() > HigHistograms.Max()? LowHistograms.Max(): LowHistograms.Max();
            if (maxYVal.Equals(double.NaN) || maxYVal.Equals(double.NegativeInfinity) || maxYVal.Equals(double.PositiveInfinity))
            {
                maxYVal = 0.5f;
            }
            maxYVal = (float)(Math.Pow(10, Math.Ceiling(Math.Log10(maxYVal))) * 1.1);
            maxLogYVal = Math.Log10(maxYVal);
            minLogYVal = Math.Log10(minYVal);

            //
            var xpixelStep = DrawAreaWidth * BinSizeMS / TimeScaleMS;
            var ypixelStep = DrawAreaHeight / (maxLogYVal - minLogYVal);



            //
            var ys = new List<float>();
            var ys2 = new List<float>();
            foreach (var val in LowHistograms)
            {
                ys.Add((float)(DrawAreaHeight - ypixelStep * (Math.Log10(Math.Max(val, minYVal)) - minLogYVal)));
            }

            foreach (var val in HigHistograms)
            {
                ys2.Add((float)(DrawAreaHeight - ypixelStep * (Math.Log10(Math.Max(val, minYVal)) - minLogYVal)));
            }

            //
     
            //绘制波形
            if (maxYVal > 0)
            {
                //鼠标hover时的线条
                if (IsMouseHover)
                {
                    //垂直线
                    {
                        var x = this.MakeLineThin(MouseHoverX);
                        var xtick = (int)Math.Round((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
                        var txt = this.CreateFormatText($"{xtick}", _yellowPen.Brush);
                        ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                        ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + 2 * TEXT_MARGIN));
                    }

                    //水平线
                    {
                        var y = this.MakeLineThin(MouseHoverY);
                        ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                        var ytick = Math.Pow(10, (OriginPoint.Y - MouseHoverY) / ypixelStep + minLogYVal);
                        var txt = this.CreateFormatText($"{ytick:F4}", _yellowPen.Brush);
                        ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, y - txt.Height));
                    }

                }
            }

            //绘制Y刻度
            if (maxYVal > 0)
            {
                var val = 0.0;
                var pow = Math.Floor(Math.Log10(minYVal));
                while (val < maxYVal)
                {
                    for (int i = 1; i < 10; i++)
                    {
                        val = i * Math.Pow(10, pow);
                        if (val > minYVal && val < maxYVal)
                        {
                            var y = this.MakeLineThin(OriginPoint.Y - Math.Max(0, (Math.Log10(val) - minLogYVal) * ypixelStep));
                            ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TEXT_MARGIN, y));
                            if (i == 1)
                            {
                                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TICK_LINE_LENGTH, y));
                                var txt = this.CreateFormatText($"{val}");
                                ctx.DrawText(txt, new Point(OriginPoint.X - TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
                            }
                        }
                    }
                    pow += 1;
                }
            }
        }

        /// <summary>
        /// 以Y轴线性绘制
        /// </summary>
        /// <param name="ctx"></param>
        private void PaintGraphicsWithLinearY(DrawingContext ctx)
        {
            //Y轴最大值
            var format = $"F{Math.Abs(power) + 1}";

            //
            yValStep = 0.0f;
            power = 0;
            yLevelCount = 10;
            maxYVal = LowHistograms.Max() > HigHistograms.Max() ? LowHistograms.Max() : LowHistograms.Max();
            if (maxYVal.Equals(double.NaN) || maxYVal.Equals(double.NegativeInfinity) || maxYVal.Equals(double.PositiveInfinity) || maxYVal <= 0)
            {
                return;
            }

            //
            ToolMix.CoordinateAxisEqualDivide(maxYVal, out yValStep, out power, out yLevelCount, 8);
            maxYVal = yValStep * yLevelCount;

            //
            var xpixelStep = DrawAreaWidth * BinSizeMS / TimeScaleMS;
            var ypixelStep = DrawAreaHeight / maxYVal;

            //
            var ys = new List<float>();
            var ys2 = new List<float>();
            var ys3 = new List<float>();
            if (FilterCutoffLow)
                ys = LowHistograms.Select(val => DrawAreaHeight - ypixelStep * val).ToList();

            if(FilterCutoffHig)
                ys2 = HigHistograms.Select(val => DrawAreaHeight - ypixelStep * val).ToList();
            if (FilterCutoffBand)
                ys3 = HigHistograms.Select(val => DrawAreaHeight - ypixelStep * val).ToList();



            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Brushes.Red,3);
            System.Drawing.Pen pen2 = new System.Drawing.Pen(System.Drawing.Brushes.Green,3);
            System.Drawing.Pen pen3 = new System.Drawing.Pen(System.Drawing.Brushes.Yellow,3);

            //
            _painter.PaintFilter(ctx, _bgColor, ControlConst.HistDrawingBrush, OriginPoint, DrawAreaWidth, DrawAreaHeight, xpixelStep, ys,ys2,ys3, pen,pen2,pen3);
           // _painter.PaintFilter(ctx, _bgColor, ControlConst.HistDrawingBrush, OriginPoint, DrawAreaWidth, DrawAreaHeight, xpixelStep, ys2, pen2, false);

            //绘制波形
            if (maxYVal > 0)
            {
                //鼠标hover时的线条
                if (IsMouseHover)
                {
                    //垂直线
                    {
                        var x = this.MakeLineThin(MouseHoverX);
                        var xtick = (int)Math.Round((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
                        var txt = this.CreateFormatText($"{xtick}", _yellowPen.Brush);
                        ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                        ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + 2 * TEXT_MARGIN));
                    }

                    //水平线
                    {
                   
                        var y = this.MakeLineThin(MouseHoverY);
                        ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                        var ytick = (OriginPoint.Y - MouseHoverY) / DrawAreaHeight * maxYVal;
                        var txt = this.CreateFormatText($"{ytick.ToString(format)}", _yellowPen.Brush);
                        ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, y - txt.Height));
                    }

            
                }
            }

            //绘制Y刻度
            if (maxYVal > 0)
            {
                for (int i = 1; i <= yLevelCount; i++)
                {
                    var val = i * yValStep;
                    var y = this.MakeLineThin(OriginPoint.Y - val / maxYVal * DrawAreaHeight);
                    var txt = this.CreateFormatText($"{(i * yValStep).ToString(format)}");
                    ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X - TEXT_MARGIN, y), new Point(XAxisMaxPoint.X, y));
                    ctx.DrawText(txt, new Point(OriginPoint.X - TEXT_MARGIN * 2 - txt.Width, y - txt.Height / 2));
                }
            }
        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //绘制X刻度
            {
                const int SplitCount = 15;
                var timeSpanStep = TimeScaleMS / SplitCount;
                var xstep = DrawAreaWidth / SplitCount;
                for (int i = 0; i < SplitCount + 1; i++)
                {
                    var x = i * xstep + OriginPoint.X;
                    var txt = this.CreateFormatText($"{i * timeSpanStep}{(i == SplitCount ? "ms" : "")}");
                    ctx.DrawLine(_foregroundPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y ));
                    if ( i == SplitCount||i==0 )
                    ctx.DrawText(txt, new Point(x - txt.Width / (i == SplitCount ? 1 : 2), OriginPoint.Y + 2 * TEXT_MARGIN));
                }
            }

            //绘制大方框
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));
        }

        /// <summary>
        /// 绘制Hover状态
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintHover(DrawingContext ctx)
        {

        }

        /// <summary>
        /// 绘制标题信息等
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            //绘制常用信息
            PaintInfoOnTopRight(ctx, $"Recorded: {FilterCount}, Mean: {Mean:F2} ms, SD: {StdDev:F2} ms, Frequency: {FilterFreq:F2} Hz");

            //通道
            //PaintChannelNameOnOuterTopLeft(ctx);

            //
            //PaintTitleOnLeft(ctx, "概率分布");
        }

    }
}
