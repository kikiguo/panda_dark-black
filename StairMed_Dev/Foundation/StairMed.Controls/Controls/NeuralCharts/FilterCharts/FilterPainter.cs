﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.FilterCharts
{
    internal class FilterPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual void PaintFilter(DrawingContext ctx, System.Drawing.Color bgcolor, System.Drawing.Brush solidBrush, System.Windows.Point origin, float width, float height, float xpixelStep, List<float> ys, List<float> ys2, List<float> ys3, System.Drawing.Pen pen, System.Drawing.Pen pen2, System.Drawing.Pen pen3, bool newBoard = true)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //
            PaintToShadow(bgcolor, origin, width, height, newBoard, (g) =>
            {
                //绘制点
                float x = 0;
                float y = ys[0];
                for (int i = 0; i < ys.Count; i++)
                {
                    if (ys[i] != origin.Y)
                    {

                        // g.FillRectangle(solidBrush, Convert.ToSingle(x), Convert.ToSingle(ys[i]), xpixelStep, Convert.ToSingle(origin.Y - ys[i]));
                        System.Drawing.PointF pt1 = new System.Drawing.PointF();
                        System.Drawing.PointF pt2 = new System.Drawing.PointF();
                        pt1.X = x;
                        pt1.Y = y;
                        pt2.X = x + xpixelStep;
                        pt2.Y = ys[i];
                        g.DrawLine(pen, pt1, pt2);

                    }
                    x += xpixelStep;
                    y = ys[i];
                }

                 x = 0;
                y = ys2[0];
                for (int i = 0; i < ys2.Count; i++)
                {
                    if (ys2[i] != origin.Y)
                    {

                        // g.FillRectangle(solidBrush, Convert.ToSingle(x), Convert.ToSingle(ys[i]), xpixelStep, Convert.ToSingle(origin.Y - ys[i]));
                        System.Drawing.PointF pt1 = new System.Drawing.PointF();
                        System.Drawing.PointF pt2 = new System.Drawing.PointF();
                        pt1.X = x;
                        pt1.Y = y;
                        pt2.X = x + xpixelStep;
                        pt2.Y = ys2[i];
                        g.DrawLine(pen2, pt1, pt2);

                    }
                    x += xpixelStep;
                    y = ys2[i];
                }

                x = 0;
                y = ys3[0];
                for (int i = 0; i < ys3.Count; i++)
                {
                    if (ys3[i] != origin.Y)
                    {

                        // g.FillRectangle(solidBrush, Convert.ToSingle(x), Convert.ToSingle(ys[i]), xpixelStep, Convert.ToSingle(origin.Y - ys[i]));
                        System.Drawing.PointF pt1 = new System.Drawing.PointF();
                        System.Drawing.PointF pt2 = new System.Drawing.PointF();
                        pt1.X = x;
                        pt1.Y = y;
                        pt2.X = x + xpixelStep;
                        pt2.Y = ys3[i];
                        g.DrawLine(pen3, pt1, pt2);

                    }
                    x += xpixelStep;
                    y = ys3[i];
                }

            }, true);

            CopyFromShadow(ctx, origin, width, height);
        }
    }
}
