﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.RMSCharts
{
    internal class RMSPainter : BitmapPainterBase
    {

        int maxLine = 20;
        /// <summary>
        /// 
        /// </summary>
        /// 
        public virtual void PaintRMS(DrawingContext ctx, System.Drawing.Color bgcolor, System.Drawing.Brush solidBrush, System.Windows.Point origin, float width, float height, float xpixelStep, List<float> ys,bool newBoard=true)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //
            PaintToShadow(bgcolor, origin, width, height, newBoard, (g) =>
            {
                //绘制点
                float x = 0;
                float y = ys[0];
                for (int i = 0; i < ys.Count; i++)
                {
                    if (ys[i] != origin.Y)
                    {

                        // g.FillRectangle(solidBrush, Convert.ToSingle(x), Convert.ToSingle(ys[i]), xpixelStep, Convert.ToSingle(origin.Y - ys[i]));
                        System.Drawing.PointF pt1 = new System.Drawing.PointF();
                        System.Drawing.PointF pt2 = new System.Drawing.PointF();
                        pt1.X = x;
                        pt1.Y = y;
                        pt2.X = x + xpixelStep;
                        pt2.Y = ys[i];
                        g.DrawLine(Pens.White, pt1, pt2);

                    }
                    x += xpixelStep;
                    y = ys[i];
                }
            }, true);
    
            CopyFromShadow(ctx, origin, width, height);
        }

    }
}
