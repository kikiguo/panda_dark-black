﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Globalization;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using Accord.Statistics.Visualizations;

namespace StairMed.Controls.Controls.NeuralCharts.RMSCharts
{
    public partial class RMSChart : NeuralChartBase
    {


        /// <summary>
        /// 
        /// </summary>
        static RMSChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RMSChart), new FrameworkPropertyMetadata(typeof(RMSChart)));
        }


        #region 属性

        /// <summary>
        /// X时间范围：ms
        /// </summary>
        public int TimeScaleMS
        {
            get { return (int)GetValue(TimeScaleMSProperty); }
            set { SetValue(TimeScaleMSProperty, value); }
        }
        public static readonly DependencyProperty TimeScaleMSProperty = DependencyProperty.Register(nameof(TimeScaleMS), typeof(int), typeof(RMSChart), new PropertyMetadata(1000, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 步长
        /// </summary>
        public int BinSizeMS
        {
            get { return (int)GetValue(BinSizeMSProperty); }
            set { SetValue(BinSizeMSProperty, value); }
        }
        public static readonly DependencyProperty BinSizeMSProperty = DependencyProperty.Register(nameof(BinSizeMS), typeof(int), typeof(RMSChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Y轴：true：线性，false：log
        /// </summary>
        public bool LinearScale
        {
            get { return (bool)GetValue(LinearScaleProperty); }
            set { SetValue(LinearScaleProperty, value); }
        }
        public static readonly DependencyProperty LinearScaleProperty = DependencyProperty.Register(nameof(LinearScale), typeof(bool), typeof(RMSChart), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        public List<float> Histograms
        {
            get { return (List<float>)GetValue(HistogramsProperty); }
            set { SetValue(HistogramsProperty, value); }
        }
        public static readonly DependencyProperty HistogramsProperty = DependencyProperty.Register(nameof(Histograms), typeof(List<float>), typeof(RMSChart), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));



        /// <summary>
        /// 
        /// </summary>
        public List<float> OldHistograms
        {
            get { return (List<float>)GetValue(OldHistogramsProperty); }
            set { SetValue(OldHistogramsProperty, value); }
        }
        public static readonly DependencyProperty OldHistogramsProperty = DependencyProperty.Register(nameof(OldHistograms), typeof(List<float>), typeof(RMSChart), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// 个数
        /// </summary>
        public int RMSCount
        {
            get { return (int)GetValue(RMSCountProperty); }
            set { SetValue(RMSCountProperty, value); }
        }
        public static readonly DependencyProperty RMSCountProperty = DependencyProperty.Register(nameof(RMSCount), typeof(int), typeof(RMSChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));





        /// <summary>
        /// 均值
        /// </summary>
        public double Mean
        {
            get { return (double)GetValue(MeanProperty); }
            set { SetValue(MeanProperty, value); }
        }
        public static readonly DependencyProperty MeanProperty = DependencyProperty.Register(nameof(Mean), typeof(double), typeof(RMSChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 频率
        /// </summary>
        public double RMSFreq
        {
            get { return (double)GetValue(RMSFreqProperty); }
            set { SetValue(RMSFreqProperty, value); }
        }
        public static readonly DependencyProperty RMSFreqProperty = DependencyProperty.Register(nameof(RMSFreq), typeof(double), typeof(RMSChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// RMS
        /// </summary>
        public double Rms
        {
            get { return (double)GetValue(RmsProperty); }
            set { SetValue(RmsProperty, value); }
        }
        public static readonly DependencyProperty RmsProperty = DependencyProperty.Register(nameof(Rms), typeof(double), typeof(RMSChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));
         
        
        /// <summary>
        /// RMS
        /// </summary>
        public int RMSValue
        {
            get { return (int)GetValue(RMSValueProperty); }
            set { SetValue(RMSValueProperty, value); }
        }
        public static readonly DependencyProperty RMSValueProperty = DependencyProperty.Register(nameof(RMSValue), typeof(int), typeof(RMSChart), new PropertyMetadata(5, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// RMS
        /// </summary>
        public int StaRms
        {
            get { return (int)GetValue(StaRmsProperty); }
            set { SetValue(StaRmsProperty, value); }
        }
        public static readonly DependencyProperty StaRmsProperty = DependencyProperty.Register(nameof(StaRms), typeof(int), typeof(RMSChart), new PropertyMetadata(5, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// RMS
        /// </summary>
        public int EndRms
        {
            get { return (int)GetValue(EndRmsProperty); }
            set { SetValue(EndRmsProperty, value); }
        }
        public static readonly DependencyProperty EndRmsProperty = DependencyProperty.Register(nameof(EndRms), typeof(int), typeof(RMSChart), new PropertyMetadata(5, new PropertyChangedCallback(PropertyChanged)));



        /// <summary>
        /// 标准差
        /// </summary>
        public double StdDev
        {
            get { return (double)GetValue(StdDevProperty); }
            set { SetValue(StdDevProperty, value); }
        }
        public static readonly DependencyProperty StdDevProperty = DependencyProperty.Register(nameof(StdDev), typeof(double), typeof(RMSChart), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(TimeScaleMSProperty) || e.Property.Equals(BinSizeMSProperty) || e.Property.Equals(LinearScaleProperty) || e.Property.Equals(HistogramsProperty) || e.Property.Equals(RMSCountProperty) || e.Property.Equals(MeanProperty) || e.Property.Equals(RMSFreqProperty) || e.Property.Equals(StdDevProperty))
            {
                DelayReRender(false);
                return;
            }
        }


        //绘图工具
        private readonly RMSPainter _painter = new RMSPainter();

        //
        public float maxYVal = 0.0f;
        public float yValStep = 0.0f;
        public int power = 0;
        public int yLevelCount = 10;

        //
        public double maxLogYVal = 0.0;
        public double minYVal = 0.00009;
        public double minLogYVal = 0.0;

    


        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            if (Histograms == null || Histograms.Count <= 0)
            {
                return;
            }

            //
            if (LinearScale)
            {
                PaintGraphicsWithLinearY(ctx);
            }
            else
            {
                PaintGraphicsWithLogY(ctx);
            }
        }

        /// <summary>
        /// 以对数绘制Y轴
        /// </summary>
        /// <param name="ctx"></param>
        private void PaintGraphicsWithLogY(DrawingContext ctx)
        {
            maxLogYVal = 0.0;
            minYVal = 0.00009;
            minLogYVal = 0.0;
            maxYVal = (float)RMSValue;
            if (maxYVal.Equals(double.NaN) || maxYVal.Equals(double.NegativeInfinity) || maxYVal.Equals(double.PositiveInfinity))
            {
                maxYVal = 0.5f;
            }
            maxYVal = (float)(Math.Pow(10, Math.Ceiling(Math.Log10(maxYVal))) * 1.1);
            maxLogYVal = Math.Log10(maxYVal);
            minLogYVal = Math.Log10(minYVal);

            //
            var xpixelStep = DrawAreaWidth * BinSizeMS / TimeScaleMS;
            var ypixelStep = DrawAreaHeight / (maxLogYVal - minLogYVal);

            //
            var ys = new List<float>();
            foreach (var val in Histograms)
            {
                ys.Add((float)(DrawAreaHeight - ypixelStep * (Math.Log10(Math.Max(val, minYVal)) - minLogYVal)));
            }

            //
            _painter.PaintRMS(ctx, _bgColor, ControlConst.HistDrawingBrush, OriginPoint, DrawAreaWidth, DrawAreaHeight, xpixelStep, ys);

            //绘制波形
            if (maxYVal > 0)
            {
                //鼠标hover时的线条
                if (IsMouseHover)
                {
                    //垂直线
                    {
                        var x = this.MakeLineThin(MouseHoverX);
                        var xtick = (int)Math.Round((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
                        var txt = this.CreateFormatText($"{xtick}", _yellowPen.Brush);
                        ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                        ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + 2 * TEXT_MARGIN));
                    }

                    //水平线
                    {
                        var y = this.MakeLineThin(MouseHoverY);
                        ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                        var ytick = Math.Pow(10, (OriginPoint.Y - MouseHoverY) / ypixelStep + minLogYVal);
                        var txt = this.CreateFormatText($"{ytick:F4}", _yellowPen.Brush);
                        ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, y - txt.Height));
                    }

                    //柱子值
                    {
                        var index = (int)Math.Floor((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
                        if (index >= 0 && index < TimeScaleMS)
                        {
                            var histIndex = Math.Min(Histograms.Count - 1, Math.Max(0, index / BinSizeMS));
                            var hist = Histograms[histIndex];
                            var y = this.MakeLineThin(OriginPoint.Y - Math.Max(0, (Math.Log10(hist) - minLogYVal) * ypixelStep));
                            ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                            var txt = this.CreateFormatText($"{Histograms[histIndex]:F4}", _yellowPen.Brush);
                            ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, y - txt.Height));
                        }
                    }
                }
            }

            //绘制Y刻度
            if (maxYVal > 0)
            {
                var val = 0.0;
                var pow = Math.Floor(Math.Log10(minYVal));
                while (val < maxYVal)
                {
                    for (int i = 1; i < 10; i++)
                    {
                        val = i * Math.Pow(10, pow);
                        if (val > minYVal && val < maxYVal)
                        {
                            var y = this.MakeLineThin(OriginPoint.Y - Math.Max(0, (Math.Log10(val) - minLogYVal) * ypixelStep));
                            ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TEXT_MARGIN, y));
                            if (i == 1)
                            {
                                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TICK_LINE_LENGTH, y));
                                var txt = this.CreateFormatText($"{val}");
                                ctx.DrawText(txt, new Point(OriginPoint.X - TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
                            }
                        }
                    }
                    pow += 1;
                }
            }
        }

        double oldMouseH = 0.0;
        int oldsta = -1;
        int endsta = -1;
        /// <summary>
        /// 以Y轴线性绘制
        /// </summary>
        /// <param name="ctx"></param>
        private void PaintGraphicsWithLinearY(DrawingContext ctx)
        {
            /*          //Y轴最大值
                      var format = $"F{Math.Abs(power) + 1}";

                      //
                      yValStep = 0.0f;
                      power = 0;
                      yLevelCount = 10;
                      maxYVal = (float)RMSValue;
                      if (maxYVal.Equals(double.NaN) || maxYVal.Equals(double.NegativeInfinity) || maxYVal.Equals(double.PositiveInfinity) || maxYVal <= 0)
                      {
                          return;
                      }

                      //
                      ToolMix.CoordinateAxisEqualDivide(maxYVal, out yValStep, out power, out yLevelCount, 4);
                      maxYVal = yValStep * yLevelCount;

                      //
                      var xpixelStep = DrawAreaWidth * BinSizeMS / TimeScaleMS;
                      var ypixelStep = DrawAreaHeight / maxYVal;

                      //
                      var ys = new List<float>(TimeScaleMS);
                      foreach (var val in Histograms)
                      {
                          ys.Add(DrawAreaHeight - ypixelStep * val);
                      }

                      //
                      _painter.PaintRMS(ctx, _bgColor, ControlConst.HistDrawingBrush, OriginPoint, DrawAreaWidth, DrawAreaHeight, xpixelStep, ys);

                      //绘制波形
                      if (maxYVal > 0)
                      {
                          //鼠标hover时的线条
                          if (IsMouseHover)
                          {
                              //垂直线
                              {
                                  var x = this.MakeLineThin(MouseHoverX);
                                  var xtick = (int)Math.Round((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
                                  var txt = this.CreateFormatText($"{xtick}", _yellowPen.Brush);
                                  ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                                  ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + 2 * TEXT_MARGIN));
                              }

                              //水平线
                              {
                                  var y = this.MakeLineThin(MouseHoverY);
                                  ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                                  var ytick = (OriginPoint.Y - MouseHoverY) / DrawAreaHeight * maxYVal;
                                  var txt = this.CreateFormatText($"{ytick.ToString(format)}", _yellowPen.Brush);
                                  ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, y - txt.Height));
                              }

                              //柱子值
                              {
                                  var index = (int)Math.Floor((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
                                  if (index >= 0 && index < TimeScaleMS)
                                  {
                                      var histIndex = Math.Min(Histograms.Count - 1, Math.Max(0, index / BinSizeMS));
                                      var hist = Histograms[histIndex];
                                      var y = this.MakeLineThin(OriginPoint.Y - hist / maxYVal * DrawAreaHeight);
                                      ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                                      var txt = this.CreateFormatText($"{Histograms[histIndex].ToString(format)}", _yellowPen.Brush);
                                      ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, y - txt.Height));
                                  }
                              }
                          }
                      }

                      //绘制Y刻度
                      if (maxYVal > 0)
                      {
                          for (int i = 1; i <= yLevelCount; i++)
                          {
                              var val = i * yValStep;
                              var y = this.MakeLineThin(OriginPoint.Y - val / maxYVal * DrawAreaHeight);
                              var txt = this.CreateFormatText($"{(i * yValStep).ToString(format)}");
                              ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X - TEXT_MARGIN, y), new Point(OriginPoint.X, y));
                              ctx.DrawText(txt, new Point(OriginPoint.X - TEXT_MARGIN * 2 - txt.Width, y - txt.Height / 2));
                          }
                      }*/
            // Validation and initial calculations

            if (double.IsNaN(RMSValue) || double.IsInfinity(RMSValue) || RMSValue <= 0) return;


            var Histograms =this.Histograms;
            

            if (StairMedSettings.Instance.RMSMouseDouble)
            {
           
                if (oldsta == -1)
                {
                    oldsta = StaRms;
                    endsta = EndRms;
                }

                if (IsMouseClick)
                {
                    oldMouseH = RmsMouseHoverY;
                    double y = MakeLineThin(MouseHoverY);
                    if(oldMouseH ==0)
                    {
                   
                    }
                    else if (y < oldMouseH) {
                      
                        if (EndRms >= OldHistograms.Count)
                        {

                        }
                        else
                        {
                            StaRms += 1;
                            EndRms += 1;
                        }


                    }
                    else if (y > oldMouseH)
                    {
                      
                        if (StaRms <= 1)
                        {

                        }
                        else
                        {
                            StaRms -= 1;
                            EndRms -= 1;
                        }
                        
                  
               

                    }
                }

                var hist = OldHistograms.Skip(StaRms).Take(EndRms - StaRms).ToList();

                Histograms = hist;
            }

            maxYVal = (float)RMSValue;
            float yValStep = 0.0f;
            int yLevelCount = 10, power = 0;
            ToolMix.CoordinateAxisEqualDivide(maxYVal, out yValStep, out power, out yLevelCount, 4);
            maxYVal = yValStep * yLevelCount;

            string format = $"F{Math.Abs(power) + 1}";

            if(minRmsValue> Histograms.Min() || minRmsValue==0 || Histograms.Count <= 2)
            {
                minRmsValue = Histograms.Min();
            }


            if (maxRmsValue < Histograms.Max() ||Histograms.Count<=2)
            {
                maxRmsValue = Histograms.Max();
            }

            // Determine pixel steps
            var xpixelStep = DrawAreaWidth * BinSizeMS / TimeScaleMS;
            var ypixelStep = DrawAreaHeight / maxYVal;

            // Calculate Y positions for drawing

           


            var ys = Histograms.Select(val => DrawAreaHeight - ypixelStep * val).ToList();
        
            // Main plotting logic here
            _painter.PaintRMS(ctx, _bgColor, ControlConst.HistDrawingBrush, OriginPoint, DrawAreaWidth, DrawAreaHeight, xpixelStep, ys);

            // Additional logic for when mouse is hovering over the plot
            if (IsMouseHover)
            {
                DrawMouseHoverLines(ctx, format, maxYVal, 50);
            }

           



            // Draw Y-axis labels
            if (maxYVal > 0)
            {
                DrawYAxisLabels(ctx, maxYVal, yValStep, yLevelCount, format);
            }
            DrawHigAndLowLines(ctx, $"F{Math.Abs(0) + 1}", maxYVal, DrawAreaWidth * BinSizeMS / TimeScaleMS);
            DrawYAxisLabels(ctx, maxYVal, 0.0f, 10, $"F{Math.Abs(0) + 1}");

        }


        private void DrawHigAndLowLines(DrawingContext ctx, string format, float maxYVal, double xpixelStep)
        {
         
                int histIndex = Histograms.Count - 1;
                float hist = Histograms[histIndex];
               var y = MakeLineThin(OriginPoint.Y - hist / maxYVal * DrawAreaHeight);
                ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                var histTxt = CreateFormatText($"{hist.ToString(format)}", _yellowPen.Brush);
                ctx.DrawText(histTxt, new Point(XAxisMaxPoint.X - histTxt.Width - TEXT_MARGIN, y - histTxt.Height));


                //hist = maxYVal/2;
                //y = MakeLineThin(OriginPoint.Y - hist / maxYVal * DrawAreaHeight);
                //ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                //histTxt = CreateFormatText($"{hist.ToString(format)}", _yellowPen.Brush);
                //ctx.DrawText(histTxt, new Point(XAxisMaxPoint.X - histTxt.Width - TEXT_MARGIN, y - histTxt.Height));

                hist = maxRmsValue;
                y = MakeLineThin(OriginPoint.Y - hist / maxYVal * DrawAreaHeight);
                ctx.DrawLine(_redPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                histTxt = CreateFormatText($"{hist.ToString(format)}", _yellowPen.Brush);
                ctx.DrawText(histTxt, new Point(XAxisMaxPoint.X - histTxt.Width - TEXT_MARGIN, y - histTxt.Height));


                hist = minRmsValue;
                y = MakeLineThin(OriginPoint.Y - hist / maxYVal * DrawAreaHeight);
                ctx.DrawLine(_limePen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                histTxt = CreateFormatText($"{hist.ToString(format)}", _yellowPen.Brush);
                ctx.DrawText(histTxt, new Point(XAxisMaxPoint.X - histTxt.Width - TEXT_MARGIN, y - histTxt.Height));
            
        }

        private float maxRmsValue = 0.0f;
        private float minRmsValue = 0.0f;

        private void DrawMouseHoverLines(DrawingContext ctx, string format, float maxYVal, double xpixelStep)
        {

            // Vertical Line
            double x = MakeLineThin(MouseHoverX);
            int xtick = (int)Math.Round((MouseHoverX - OriginPoint.X) * BinSizeMS / xpixelStep);
            var xTxt = CreateFormatText($"{xtick}", _yellowPen.Brush);
            ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            ctx.DrawText(xTxt, new Point(x - xTxt.Width / 2, OriginPoint.Y + 2 * TEXT_MARGIN));

            // Horizontal Line
            double y = MakeLineThin(MouseHoverY);
            ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
            var yTxt = CreateFormatText($"{((OriginPoint.Y - MouseHoverY) / DrawAreaHeight * maxYVal).ToString(format)}", _yellowPen.Brush);
            ctx.DrawText(yTxt, new Point(XAxisMaxPoint.X - yTxt.Width - TEXT_MARGIN, y - yTxt.Height));

            // Histogram Value
          
        }

        private void DrawYAxisLabels(DrawingContext ctx, float maxYVal, float yValStep, int yLevelCount, string format)
        {
            for (int i = 1; i <= yLevelCount; i++)
            {
                float val = i * yValStep;
                double y = MakeLineThin(OriginPoint.Y - val / maxYVal * DrawAreaHeight);
                var txt = CreateFormatText($"{(i * yValStep).ToString(format)}", _whitePen.Brush);
                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X - TEXT_MARGIN, y), new Point(OriginPoint.X, y));
                ctx.DrawText(txt, new Point(OriginPoint.X - TEXT_MARGIN * 2 - txt.Width, y - txt.Height / 2));
            }
        }

        private FormattedText CreateFormatText(string text, Brush brush = null)
        {
            return new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                12,
                brush ?? Brushes.Black,
                VisualTreeHelper.GetDpi(this).PixelsPerDip
                );
        }

        private double MakeLineThin(double position)
        {
            return Math.Round(position) + 0.5;
        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            DrawXScale(ctx);
            DrawBoundingRectangle(ctx);
        }

        private FormattedText CreateFormatText(string text)
        {
            return new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                12,
                Brushes.Black,
                VisualTreeHelper.GetDpi(this).PixelsPerDip);
        }

        //绘制X刻度
        private void DrawXScale(DrawingContext ctx)
        {
            const int SplitCount = 5;
            var timeSpanStep = TimeScaleMS / SplitCount;
            var xPixelStep = DrawAreaWidth / SplitCount;

            for (int i = 0; i <= SplitCount; i++)
            {
                var xPos = i * xPixelStep + OriginPoint.X;
                var timeLabel = $"{i * timeSpanStep}{(i == SplitCount ? "ms" : "")}";
                var text = CreateFormatText(timeLabel, _whitePen.Brush);

                ctx.DrawLine(_foregroundPen, new Point(xPos, OriginPoint.Y), new Point(xPos, OriginPoint.Y + TEXT_MARGIN));
                ctx.DrawText(text, new Point(xPos - text.Width / (i == SplitCount ? 1 : 2), OriginPoint.Y + 2 * TEXT_MARGIN));
            }
        }
        //绘制大方框
        private void DrawBoundingRectangle(DrawingContext ctx)
        {
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));
        }


        /// <summary>
        /// 绘制Hover状态
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintHover(DrawingContext ctx)
        {

        }

        /// <summary>
        /// 绘制标题信息等
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            //绘制常用信息
            PaintInfoOnTopRight(ctx, $"Recorded: {RMSCount}, Mean: {Mean:F2} ms, SD: {StdDev:F2} ms, RMS: {Rms:F2} μV");

            //通道
            //PaintChannelNameOnOuterTopLeft(ctx);

            //
            //PaintTitleOnLeft(ctx, "概率分布");
        }

    }
}
