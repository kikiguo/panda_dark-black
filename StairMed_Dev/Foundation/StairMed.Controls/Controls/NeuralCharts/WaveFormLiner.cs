﻿using Accord.Statistics.Visualizations;
using HDF5.NET;
using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Controls.Controls.NeuralCharts.ISICharts;
using StairMed.Controls.Controls.NeuralCharts.SpikeScopeCharts;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts
{
    public partial class WaveFormLiner: NeuralChartBase
    {
        static WaveFormLiner()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WaveFormLiner), new FrameworkPropertyMetadata(typeof(WaveFormLiner)));


        }


        public bool IsRuleDisplay
        {
            get { return (bool)GetValue(IsRuleDisplayProperty); }
            set { SetValue(IsRuleDisplayProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsRule.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsRuleDisplayProperty =
            DependencyProperty.Register("IsRuleDisplay", typeof(bool), typeof(WaveFormLiner), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        public int SortedIndex
        {
            get { return (int)GetValue(SortedIndexProperty); }
            set { SetValue(SortedIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SortedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SortedIndexProperty =
            DependencyProperty.Register("SortedIndex", typeof(int), typeof(WaveFormLiner), new PropertyMetadata(-1, new PropertyChangedCallback(PropertyChanged)));

        public List<float> LinerDatas
        {
            get { return (List<float>)GetValue(LinerDatasProperty); }
            set { SetValue(LinerDatasProperty, value); }
        }
        public static readonly DependencyProperty LinerDatasProperty = DependencyProperty.Register(nameof(LinerDatas), typeof(List<float>), typeof(WaveFormLiner), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property.Equals(MaxXProperty) || e.Property.Equals(DrawAreaHeightProperty) || e.Property.Equals(DrawAreaWidthProperty) || e.Property.Equals(IsRuleDisplayProperty))
            {
                UpdateYRatio();
            }
            else if (e.Property.Equals(SortedIndexProperty))
            {
                var p = SpikeScopeColorHelper.GetPen(SortedIndex).Color;

                Color color = Color.FromArgb(p.A, p.B, p.G, p.R);
                SolidColorBrush brush = new SolidColorBrush(color);
                _foregroundPen = new Pen(brush, LINE_WIDTH);
            }
            else if (e.Property.Equals(LinerDatasProperty))
            {
                DelayReRender();
                return;
            }
        }

        private double baseLineY = 0.0;

        protected override void UpdateYRatio()
        {
            baseLineY = DrawAreaHeight / 2;
            if(IsRuleDisplay)
            {
                baseLineY = DrawAreaHeight * 2 / 3;
            }
            if (MaxX > 0)
                _xratio = DrawAreaWidth / MaxX;
        }
        List<float> lines = new List<float>();  

        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            if (LinerDatas == null || LinerDatas.Count <= 0)
            {
                return;
            }

            //paint timestamp

            List<Point> pts = new List<Point>();
            for (int i = 0; i < LinerDatas.Count; i++)
            {
                var liner = LinerDatas[i];
                if (liner == 0)
                {
                    lines.Add(0);
                }
                else
                {
                    lines.Add((float)1);
                }
            }

            _xratio = 0;

            for (int i = 0; i < lines.Count; i++)
            {

                var liner = lines[i];

                if (liner == 0)
                {
                    ctx.DrawLine(_foregroundPen, new Point(_xratio, baseLineY), new Point(_xratio, baseLineY));
                }
                else
                {
                    ctx.DrawLine(_foregroundPen, new Point(_xratio, baseLineY + 2 * TEXT_MARGIN), new Point(_xratio, baseLineY - 2 * TEXT_MARGIN));
                }
                _xratio++;
            }


            if (_xratio > ActualWidth)
            {
                lines.Clear();
            }
         

        }

        protected override void PaintAxises(DrawingContext ctx)
        {
            ctx.DrawLine(_foregroundPen, new Point(0, baseLineY), new Point(DrawAreaWidth, baseLineY));

            if (IsRuleDisplay)
            {
                ctx.DrawLine(_dimGrayPen, new Point(0, 10*TEXT_MARGIN), new Point(DrawAreaWidth,10* TEXT_MARGIN));
                //display rule
                var splitCount = 5;
                var xstep = DrawAreaWidth / splitCount;
                var ruleStep = MaxX / splitCount;
                for (int i = 0; i <= splitCount; i++)
                {
                    ctx.DrawLine(_dimGrayPen, new Point(i * xstep, 9 * TEXT_MARGIN), new Point(i * xstep, 11 * TEXT_MARGIN));
                    var txt = this.CreateFormatText($"{i * ruleStep}{(i == splitCount ? "ms" : "")}");
                    ctx.DrawText(txt, new Point(i * xstep - (i == splitCount ? txt.Width : txt.Width / 2), 0));
                }
            }
        }
        protected override void PaintHover(DrawingContext ctx)
        { 
        
        }

        protected override void PaintTitle(DrawingContext ctx)
        {

        }
    }
}
