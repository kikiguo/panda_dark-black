﻿using StairMed.Controls.Controls.NeuralCharts.ProbeMaps;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.SpikeMaps
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeMapChart : ProbeMapChart
    {
        /// <summary>
        /// 
        /// </summary>
        static SpikeMapChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpikeMapChart), new FrameworkPropertyMetadata(typeof(SpikeMapChart)));
        }

        /// <summary>
        /// 消隐时间长度
        /// </summary>
        public int DecayMS
        {
            get { return (int)GetValue(DecayMSProperty); }
            set { SetValue(DecayMSProperty, value); }
        }
        public static readonly DependencyProperty DecayMSProperty = DependencyProperty.Register(nameof(DecayMS), typeof(int), typeof(SpikeMapChart), new PropertyMetadata(1000, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// spike已过时时间
        /// </summary>
        public Dictionary<int, double> SpikePassedMsDict
        {
            get { return (Dictionary<int, double>)GetValue(SpikePassedMsDictProperty); }
            set { SetValue(SpikePassedMsDictProperty, value); }
        }
        public static readonly DependencyProperty SpikePassedMsDictProperty = DependencyProperty.Register(nameof(SpikePassedMsDict), typeof(Dictionary<int, double>), typeof(SpikeMapChart), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));


        #region 属性变化监听

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(DecayMSProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(SpikePassedMsDictProperty))
            {
                InvalidateVisual();
                return;
            }
        }

        #endregion

        //绘图工具
        private readonly SpikeMapPainter _painter = new SpikeMapPainter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            var brushes = new Dictionary<int, System.Drawing.Brush>();
            if (SpikePassedMsDict != null)
            {
                int i = 0;
                foreach (var kvp in SpikePassedMsDict)
                {

                    //if (kvp.Key == 0 || kvp.Key == 2 || kvp.Key == 6
                    //    || kvp.Key == 19 || kvp.Key == 25 || kvp.Key == 27)



                    //if (/*kvp.Key == 0 ||*/ kvp.Key == 25 || kvp.Key == 26
                    //    || kvp.Key == 28 || kvp.Key == 27 || kvp.Key == 38
                    //     || kvp.Key == 62 || kvp.Key == 97 || kvp.Key == 99 || kvp.Key == 101)


                    if (kvp.Key == 38|| kvp.Key == 25 || kvp.Key == 6 )
                    {
                        var rgb = Convert.ToByte(255);
                        var padBrush = new SolidBrush(System.Drawing.Color.FromArgb(rgb, rgb, rgb));
                        brushes[kvp.Key] = padBrush;
                    }
                    else
                    {
                        var rgb = Convert.ToByte(0);
                        var padBrush = new SolidBrush(System.Drawing.Color.FromArgb(rgb, rgb, rgb));
                        brushes[kvp.Key] = padBrush;
                    }
                    i++;
                    //var rgb = Convert.ToByte(Math.Min(255, Math.Max(0, 255 - kvp.Value / DecayMS * 255)));
                    //var padBrush = new SolidBrush(System.Drawing.Color.FromArgb(rgb, rgb, rgb));
                    //brushes[kvp.Key] = padBrush;
                }
            }

            //
            _painter.PaintSpikeMap(ctx, _bgColor, this.FontFamily?.Source, OriginPoint, DrawAreaWidth, DrawAreaHeight, ProbeMap, brushes);
        }
    }
}
