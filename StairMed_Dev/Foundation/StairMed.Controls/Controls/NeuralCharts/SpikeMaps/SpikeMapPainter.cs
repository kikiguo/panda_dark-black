﻿using StairMed.Controls.Controls.NeuralCharts.ProbeMaps;
using StairMed.Probe;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.SpikeMaps
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeMapPainter : ProbeMapPainter
    {
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<int, System.Drawing.Brush> _brushes = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="bgcolor"></param>
        /// <param name="fontFamily"></param>
        /// <param name="origin"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="probeMap"></param>
        public virtual void PaintSpikeMap(DrawingContext ctx, System.Drawing.Color bgcolor, string fontFamily, System.Windows.Point origin, float width, float height, NTProbeMap probeMap, Dictionary<int, System.Drawing.Brush> brushes)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out _, true);

            //
            if (_channelFont != null)
            {
                _channelFont.Dispose();
            }
            _channelFont = null;

            //
            _brushes = brushes;
            _fontFamily = string.IsNullOrWhiteSpace(fontFamily) ? "微软雅黑" : fontFamily;
            _probeMap = probeMap;

            //
            base.PaintToShadow(bgcolor, origin, width, height, true, (g) =>
            {
                PaintProbes(width, height, g);
            }, true, System.Drawing.Drawing2D.SmoothingMode.HighQuality);

            //
            CopyFromShadow(ctx, origin, width, height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="padWidth"></param>
        /// <param name="padWidthHalf"></param>
        /// <param name="padHeight"></param>
        /// <param name="padHeightHalf"></param>
        /// <param name="channelFont"></param>
        /// <param name="padIndex"></param>
        /// <param name="padIndexOnElectrode"></param>
        /// <param name="pad"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        protected override void PaintPad(Graphics g, float padWidth, float padWidthHalf, float padHeight, float padHeightHalf, int padIndex, int padIndexOnElectrode, NTPad pad, float x, float y)
        {
            if (_probeMap == null)
            {
                return;
            }

            //画饼
            if (_brushes.ContainsKey(padIndexOnElectrode))
            {
                g.FillEllipse(_brushes[padIndexOnElectrode], x - padWidthHalf, y - padHeightHalf, padWidth, padHeight);
            }

            //画圈
            var color = GetColor(padIndex);
            var padPen = new System.Drawing.Pen(color.ToColor(), 2.0f);
            g.DrawEllipse(padPen, x - padWidthHalf, y - padHeightHalf, padWidth, padHeight);

            //通道名称文本
            if (_channelFont == null)
            {
                var txt = $"{_probeMap.ChannelCount}";
                var fontSize = (int)padWidth;
                while (fontSize > 3)
                {
                    var font = new Font(_fontFamily, fontSize);
                    var sizef = g.MeasureString(txt, font);
                    if (sizef.Width <= padWidth)
                    {
                        break;
                    }
                    fontSize--;
                }
                _channelFont = new Font(_fontFamily, fontSize);
            }

            //对应的通道
            {
                var txt = $"{padIndexOnElectrode + 1}";
                var sizef = g.MeasureString(txt, _channelFont);
                g.DrawString(txt, _channelFont, System.Drawing.Brushes.Blue, new PointF((int)Math.Round(x - sizef.Width / 2), (int)Math.Round(y - sizef.Height / 2)));
            }
        }
    }
}
