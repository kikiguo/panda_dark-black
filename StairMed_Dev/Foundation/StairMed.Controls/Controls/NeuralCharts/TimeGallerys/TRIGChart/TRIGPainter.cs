﻿using Accord;
using HDF5CSharp;
using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Core.Settings;
using StairMed.Entity.DataPools;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.TRIGCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class TRIGPainter : WaveformPainter
    {
        public float High { get; set; } = 1.0f;
        public float Low { get; set; } = 0.0f;


        /// <summary>
        /// 构造函数
        /// </summary>
        public TRIGPainter(WaveformChart bindControl) : base(bindControl)
        {

        }


       
        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected override void RegisterPainter()
        {
           
            TRIGPainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected override void UnregisterPainter()
        {
            TRIGPainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected override float GetDefaultY()
        {
            return Height / 2;
        }


        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>


           protected override float[] GetFloatsArray(Chunk chunk)
            {
                if (chunk.Trigs.Length == 0)
                {
                    return null;
                }
                float[] test=new float[chunk.Trigs.Array.Length];
                for (var i=0; i < chunk.Trigs.Array.Length; i++)
                {
                    test[i] = chunk.Trigs.Array[i] > 0.5f ? High : Low;
                }

                Console.WriteLine("test 数组的长度：" + test.Length);
                return test;
            }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk[] GetBufferedChunks()
        {
            return TRIGPainterCenter.Instance.BufferedChunk.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk GetNewestChunk()
        {
            if (TRIGPainterCenter.Instance.BufferedChunk.Count <= 0)
            {
                return null;
            }

            //
            return TRIGPainterCenter.Instance.BufferedChunk.ToArray()[^1];
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ForceRepaint()
        {
            TRIGPainterCenter.Instance.ForceRepaint();
        }

        public void SetWaveThresholds(float newHigh, float newLow)
        {
            High = newHigh;
            Low = newLow;
        }


    }
}
