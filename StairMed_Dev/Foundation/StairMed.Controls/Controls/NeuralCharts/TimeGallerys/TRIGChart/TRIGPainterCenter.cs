﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.TRIGCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class TRIGPainterCenter : WaveformPainterCenter
    {
        #region 单例

        private static readonly TRIGPainterCenter _instance = new TRIGPainterCenter();
        public static TRIGPainterCenter Instance => _instance;
        private TRIGPainterCenter()
        {
            
        }

        

        #endregion
    }
}
