﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.TRIGCharts;
using StairMed.Core.Settings;
using System.Collections.Generic;
using System.Threading.Channels;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.TRIGCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class TRIGChart : WaveformChart
    {

        /// <summary>
        /// 
        /// </summary>
        static TRIGChart()
        {
           
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TRIGChart), new FrameworkPropertyMetadata(typeof(TRIGChart)));
        }
     
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override WaveformPainter GetPainter()
        {
            return new TRIGPainter(this);
        }


        

    }
}
