﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.RawCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class RawPainterCenter : WaveformPainterCenter
    {
        #region 单例

        private static readonly RawPainterCenter _instance = new RawPainterCenter();
        public static RawPainterCenter Instance => _instance;
        private RawPainterCenter()
        {

        }

        #endregion
    }
}
