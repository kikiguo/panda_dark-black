﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.RawCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class RawChart : WaveformChart
    {
        /// <summary>
        /// 
        /// </summary>
        static RawChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RawChart), new FrameworkPropertyMetadata(typeof(RawChart)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override WaveformPainter GetPainter()
        {
            return new RawPainter(this);
        }
    }
}
