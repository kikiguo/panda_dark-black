﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Entity.DataPools;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.RawCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class RawPainter : WaveformPainter
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public RawPainter(WaveformChart bindControl) : base(bindControl)
        {
            
        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected override void RegisterPainter()
        {
            RawPainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected override void UnregisterPainter()
        {
            RawPainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected override float GetDefaultY()
        {
            return Height / 2;
        }


        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        protected override float[] GetFloatsArray(Chunk chunk)
        {
            if (chunk.Raws == null)
            {
                return null;
            }
            return chunk.Raws.Array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns> 
        protected override Chunk[] GetBufferedChunks()
        {
            return RawPainterCenter.Instance.BufferedChunk.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk GetNewestChunk()
        {
            if (RawPainterCenter.Instance.BufferedChunk.Count <= 0)
            {
                return null;
            }

            //
            return RawPainterCenter.Instance.BufferedChunk.ToArray()[^1];
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ForceRepaint()
        {
            RawPainterCenter.Instance.ForceRepaint();
        }
    }
}
