﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Entity.DataPools;
using System.Drawing;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.SpikeCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikePainter : WaveformPainter
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SpikePainter(WaveformChart bindControl) : base(bindControl)
        {

        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected override void RegisterPainter()
        {
            SpikePainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected override void UnregisterPainter()
        {
            SpikePainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected override float GetDefaultY()
        {
            return _zeroY;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void PaintDataToBitmap()
        {
            base.PaintToShadow(BackgroundColor, OriginPt, Width, Height, true, (g) =>
            {
                if (_pts != null)
                {
                    foreach (var pt in _pts)
                    {
                        g.DrawLine(LineDrawingPen, new PointF(pt.X, _zeroY), new PointF(pt.X, pt.Y));
                    }
                }

                //
                PaintZeroLine(g);
            });
        }


        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        protected override float[] GetFloatsArray(Chunk chunk)
        {
            if (chunk.Spks == null)
            {
                return null;
            }
            return chunk.Spks.Array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk[] GetBufferedChunks()
        {
            return SpikePainterCenter.Instance.BufferedChunk.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk GetNewestChunk()
        {
            if (SpikePainterCenter.Instance.BufferedChunk.Count <= 0)
            {
                return null;
            }

            //
            return SpikePainterCenter.Instance.BufferedChunk.ToArray()[^1];
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ForceRepaint()
        {
            SpikePainterCenter.Instance.ForceRepaint();
        }
    }
}
