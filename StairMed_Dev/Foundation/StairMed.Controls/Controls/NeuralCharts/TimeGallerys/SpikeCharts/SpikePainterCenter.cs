﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.SpikeCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikePainterCenter : WaveformPainterCenter
    {
        #region 单例

        private static readonly SpikePainterCenter _instance = new SpikePainterCenter();
        public static SpikePainterCenter Instance => _instance;
        private SpikePainterCenter()
        {

        }

        #endregion

    }
}
