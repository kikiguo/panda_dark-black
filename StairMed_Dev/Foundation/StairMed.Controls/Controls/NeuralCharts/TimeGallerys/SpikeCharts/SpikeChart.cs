﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.SpikeCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeChart : WaveformChart
    {
        /// <summary>
        /// 
        /// </summary>
        static SpikeChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpikeChart), new FrameworkPropertyMetadata(typeof(SpikeChart)));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override WaveformPainter GetPainter()
        {
            return new SpikePainter(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintHover(DrawingContext ctx)
        {
            PaintBorder(ctx);
        }
    }
}
