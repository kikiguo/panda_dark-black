﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers.Trigger;
using StairMed.Entity.DataPools;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys
{
    /// <summary>
    /// 
    /// </summary>
    public partial class TriggerChart : TimeGalleryBase
    {
        /// <summary>
        /// 
        /// </summary>
        static TriggerChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TriggerChart), new FrameworkPropertyMetadata(typeof(TriggerChart)));
        }

        /// <summary>
        /// Mark线
        /// </summary>
        public int MarkLineMS
        {
            get { return (int)GetValue(MarkLineMSProperty); }
            set { SetValue(MarkLineMSProperty, value); }
        }
        public static readonly DependencyProperty MarkLineMSProperty = DependencyProperty.Register(nameof(MarkLineMS), typeof(int), typeof(TriggerChart), new PropertyMetadata(int.MinValue, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否展示参考线
        /// </summary>
        public bool ShowMarkLine
        {
            get { return (bool)GetValue(ShowMarkLineProperty); }
            set { SetValue(ShowMarkLineProperty, value); }
        }
        public static readonly DependencyProperty ShowMarkLineProperty = DependencyProperty.Register(nameof(ShowMarkLine), typeof(bool), typeof(TriggerChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(VisibilityProperty) || e.Property.Equals(IsVisibleProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(MarkLineMSProperty) || e.Property.Equals(ShowMarkLineProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(MouseHoverXProperty))
            {
                DelayReRender();
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _channel = -1;

        /// <summary>
        /// 缓存的上升沿或下降沿
        /// </summary>
        private readonly List<TriggerEdge> _bufferedEdges = new List<TriggerEdge>();

        /// <summary>
        /// 采样率
        /// </summary>
        private int _sampleRate = 100;

        /// <summary>
        /// 右侧时间
        /// </summary>
        private DateTime _endTime = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        private readonly object lockObj = new object();

        /// <summary>
        /// 
        /// </summary>
        public TriggerChart()
        {
            this.Loaded += TriggerChart_Loaded;
            this.Unloaded += TriggerChart_Unloaded;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TriggerChart_Unloaded(object sender, RoutedEventArgs e)
        {
            TriggerDataPool.Instance.NewTriggerPackReceived -= Instance_NewTriggerPackReceived;
            NeuralDataCollector.Instance.NewChunkReceived -= Instance_NewChunkReceived;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TriggerChart_Loaded(object sender, RoutedEventArgs e)
        {
            TriggerDataPool.Instance.NewTriggerPackReceived -= Instance_NewTriggerPackReceived;
            TriggerDataPool.Instance.NewTriggerPackReceived += Instance_NewTriggerPackReceived;

            //
            NeuralDataCollector.Instance.NewChunkReceived -= Instance_NewChunkReceived;
            NeuralDataCollector.Instance.NewChunkReceived += Instance_NewChunkReceived;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        private void Instance_NewChunkReceived(Chunk obj)
        {
            UIHelper.BeginInvoke(() =>
            {
                _endTime = NeuralDataCollector.Instance.GetCollectStartTime().AddSeconds(1.0 * (obj.DataIndex + obj.OneChannelSamples) / obj.SampleRate);
                this.InvalidateVisual();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="triggerPack"></param>
        private void Instance_NewTriggerPackReceived(TriggerPack triggerPack)
        {
            this.Dispatcher.Invoke(() =>
            {
                if (Channel != _channel)
                {
                    _channel = Channel;
                    _bufferedEdges.Clear();
                }

                //
                if (triggerPack.Triggers.ContainsKey(_channel))
                {
                    lock (lockObj)
                    {
                        _bufferedEdges.AddRange(triggerPack.Triggers[_channel]);
                        _sampleRate = triggerPack.SampleRate;
                    }
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            if (DisplayWindowMS <= 0 || this._bufferedEdges == null || this._bufferedEdges.Count <= 0)
            {
                return;
            }

            //视窗中左侧时间
            var beginTime = _endTime.AddMilliseconds(-DisplayWindowMS);

            //
            var highY = OriginPoint.Y - DrawAreaHeight * 0.8;
            var lowY = OriginPoint.Y - DrawAreaHeight * 0.2;
            var xoffset = OriginPoint.X;

            //
            var originTime = NeuralDataCollector.Instance.GetCollectStartTime();
            var pts = new List<Point>();
            lock (lockObj)
            {
                var prevIndex = -1;
                for (int i = 0; i < _bufferedEdges.Count; i++)
                {
                    var edge = _bufferedEdges[i];

                    //边沿对应的时间
                    var edgeTime = originTime.AddSeconds(1.0 * edge.Index / _sampleRate);

                    //边沿在视窗左侧：看不见的位置
                    if (edgeTime <= beginTime)
                    {
                        //如果为上升沿，则绘制向上的垂线，否则绘制向下的垂线
                        pts.Add(new Point(xoffset, edge.ToHigh ? lowY : highY));
                        pts.Add(new Point(xoffset, edge.ToHigh ? highY : lowY));
                        prevIndex = i;
                    }
                    else if (edgeTime >= _endTime) //边沿在视窗右侧：看不见的位置
                    {
                        //如果为上升沿，则绘制向上的垂线，否则绘制向下的垂线
                        pts.Add(new Point(XAxisMaxPoint.X, edge.ToHigh ? lowY : highY));
                        pts.Add(new Point(XAxisMaxPoint.X, edge.ToHigh ? highY : lowY));
                    }
                    else
                    {
                        //x坐标
                        var x = xoffset + (edgeTime - beginTime).TotalMilliseconds * _xratio;

                        //如果为上升沿，则绘制向上的垂线，否则绘制向下的垂线
                        pts.Add(new Point(x, edge.ToHigh ? lowY : highY));
                        pts.Add(new Point(x, edge.ToHigh ? highY : lowY));
                    }
                }

                //左侧存在看不见的边沿
                if (prevIndex >= 0)
                {
                    //
                    if (prevIndex + 1 < _bufferedEdges.Count)
                    {
                        _bufferedEdges.RemoveRange(0, prevIndex + 1);
                    }
                    else if (_bufferedEdges.Count > 1)
                    {
                        _bufferedEdges.RemoveRange(0, _bufferedEdges.Count - 1);
                    }
                }
            }

            //
            if (pts[0].X > xoffset)
            {
                //前插水平线
                pts.Insert(0, new Point(xoffset, pts[0].Y));
            }
            if (pts[^1].X < XAxisMaxPoint.X)
            {
                //后插水平线
                pts.Add(new Point(XAxisMaxPoint.X, pts[^1].Y));
            }

            //绘图
            {
                var geometry = new StreamGeometry();

                //
                using (var gctx = geometry.Open())
                {
                    gctx.BeginFigure(pts[0], false, false);
                    for (int i = 1; i < pts.Count; i++)
                    {
                        gctx.LineTo(pts[i], true, false);
                    }
                }

                //
                geometry.Freeze();

                //
                ctx.DrawGeometry(null, _limePen, geometry);
            }
        }

        /// <summary>
        /// 绘制各种轴线
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //mark线
            if (ShowMarkLine)
            {
                var x = this.MakeLineThin(OriginPoint.X + MarkLineMS * _xratio);
                ctx.DrawLine(_redPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            }

            if (ShowChannelname)
                //
                ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));

            //
            if (MouseHoverX > 0 && MouseHoverX < this.ActualWidth)
            {
                var x = this.MakeLineThin(MouseHoverX);
                ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            }
        }

        /// <summary>
        /// 绘制标题
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            if (ShowChannelname)
                PaintChannelNameOnOuterTopLeft(ctx);
        }
    }
}
