﻿using StairMed.AsyncFIFO;
using StairMed.DataCenter;
using StairMed.Entity.DataPools;
using StairMed.MVVM.Base;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.SpikeSumCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeSumPainterCenter : ViewModelBase
    {
        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static SpikeSumPainterCenter Instance = new SpikeSumPainterCenter();
        }
        public static SpikeSumPainterCenter Instance => Helper.Instance;
        private SpikeSumPainterCenter()
        {
            _ntplHelper = new BlockFIFO { Name = nameof(SpikeSumPainterCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _ntplHelper.Add(chunk);
        }
        #endregion

        /// <summary>
        /// 所有通道的painter
        /// </summary>
        private readonly Dictionary<int, HashSet<SpikeSumPainter>> _painterDict = new Dictionary<int, HashSet<SpikeSumPainter>>();

        /// <summary>
        /// 缓存数据
        /// </summary>
        private static readonly Dictionary<int, List<float>> _spikeBufferDict = new Dictionary<int, List<float>>();

        /// <summary>
        /// 异步调用
        /// </summary>
        private readonly IAsyncFIFO _ntplHelper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            var chunk = (Chunk)objs[0];
            if (chunk.Spks == null)
            {
                return;
            }

            //
            var dict = new Dictionary<int, List<SpikeSumPainter>>();

            //
            lock (_painterDict)
            {
                foreach (var kvp in _painterDict)
                {
                    if (kvp.Value != null && kvp.Value.Count > 0)
                    {
                        dict[kvp.Key] = new List<SpikeSumPainter>();
                        dict[kvp.Key].AddRange(kvp.Value);
                    }
                }
            }

            //
            var step = chunk.SpikeSumStepMS * chunk.SampleRate / 1000;
            var window = chunk.SpikeSumWindowMS * chunk.SampleRate / 1000;
            if (step >= window)
            {
                return;
            }

            //
            var channels = chunk.InputChannelInReports;
            foreach (var kvp in dict)
            {
                var channel = kvp.Key;
                var channelIndexInChunk = channels.IndexOf(channel);
                if (channelIndexInChunk < 0)
                {
                    continue;
                }

                //
                var spikeSumFloats = new List<float>();
                {
                    if (!_spikeBufferDict.ContainsKey(channel))
                    {
                        _spikeBufferDict[channel] = new List<float>();
                    }
                    var spikeBuffer = _spikeBufferDict[channel];

                    //
                    while ((step - chunk.DataIndex % step) != (window - spikeBuffer.Count % window))
                    {
                        spikeBuffer.Add(0);
                    }
                    while (spikeBuffer.Count >= window)
                    {
                        spikeBuffer.RemoveRange(0, step);
                    }

                    //
                    var begin = channelIndexInChunk * chunk.OneChannelSamples;
                    var end = (channelIndexInChunk + 1) * chunk.OneChannelSamples;
                    for (int i = begin; i < end; i++)
                    {
                        spikeBuffer.Add(chunk.Spks[i]);
                        if (spikeBuffer.Count >= window)
                        {
                            spikeSumFloats.Add(spikeBuffer.Sum());
                            spikeBuffer.RemoveRange(0, step);
                        }
                    }
                }

                //
                foreach (var painter in kvp.Value)
                {
                    painter.PaintToShadowAsync(spikeSumFloats.ToArray(), 0, spikeSumFloats.Count, (chunk.DataIndex + chunk.OneChannelSamples) / step + 1, 1.0 * chunk.SampleRate / step);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        public void RegisterPainter(int inputChannel, SpikeSumPainter painter)
        {
            lock (_painterDict)
            {
                if (!_painterDict.ContainsKey(inputChannel))
                {
                    _painterDict[inputChannel] = new HashSet<SpikeSumPainter>();
                }
                _painterDict[inputChannel].Add(painter);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        public void UnregisterPainter(int inputChannel, SpikeSumPainter painter)
        {
            lock (_painterDict)
            {
                if (!_painterDict.ContainsKey(inputChannel))
                {
                    return;
                }
                _painterDict[inputChannel].Remove(painter);
            }
        }

    }
}
