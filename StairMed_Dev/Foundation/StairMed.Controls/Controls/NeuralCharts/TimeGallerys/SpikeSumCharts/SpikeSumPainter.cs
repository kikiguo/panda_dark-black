﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Controls.Logger;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.SpikeSumCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeSumPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected readonly List<float> _ys = new List<float>();
        protected readonly List<float> _floats = new List<float>();
        protected int _sweepIndexOffset = 0;
        protected float _sweepLineX = 0;
        protected PointF[] _pts = null;

        /// <summary>
        /// 是否可见
        /// </summary>
        private bool _isVisibleToUser = false;
        public bool IsVisibleToUser
        {
            set
            {
                if (_isVisibleToUser != value)
                {
                    _isVisibleToUser = value;
                    if (value)
                    {
                        StartListenPaintEvent();
                    }
                    else
                    {
                        StopListenPaintEvent();
                    }
                }
            }
        }

        /// <summary>
        /// 宽度
        /// </summary>
        private int _width = 10;
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// 高度
        /// </summary>
        private float _height = 0.0f;
        public float Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// 最大Y值
        /// </summary>
        private float _maxY = 100f;
        public float MaxY
        {
            get { return _maxY; }
            set { _maxY = value; }
        }

        /// <summary>
        /// 最小Y值
        /// </summary>
        private float _minY = 10;
        public float MinY
        {
            get { return _minY; }
            set { _minY = value; }
        }

        /// <summary>
        /// 视图宽度
        /// </summary>
        private int _displayWindowMS = 0;
        public int DisplayWindowMS
        {
            get { return _displayWindowMS; }
            set { _displayWindowMS = value; }
        }

        /// <summary>
        /// sweep or roll 模式
        /// </summary>
        private bool _sweepOrRoll = false;
        public bool SweepOrRoll
        {
            get { return _sweepOrRoll; }
            set { _sweepOrRoll = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private float _threshold = -70f;
        public float Threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// 绑定的通道
        /// </summary>
        private bool _channelChanged = false;
        private int _channel = -1;
        public int Channel
        {
            get { return _channel; }
            set
            {
                if (_channel != value)
                {
                    StopListenPaintEvent();
                    _channel = value;
                    _channelChanged = true;
                    if (_isVisibleToUser)
                    {
                        StartListenPaintEvent();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Point OriginPt = new System.Windows.Point();
        public Color BackgroundColor = Color.Aqua;
        public Pen LineDrawingPen = null;
        public bool ShowThresholdLine = false;
        public bool ShowZeroLine = false;

        ///// <summary>
        ///// 当控件大小或显示范围发生变化时，清屏
        ///// </summary>
        //protected bool _clear = false;

        /// <summary>
        /// 对应的Control
        /// </summary>
        protected readonly SpikeSumChart _bindControl;

        /// <summary>
        /// 构造函数
        /// </summary>
        public SpikeSumPainter(SpikeSumChart bindControl)
        {
            _bindControl = bindControl;

            //
            _bindControl.Unloaded += (object sender, RoutedEventArgs e) =>
            {
                StopListenPaintEvent();
            };
            _bindControl.Loaded += (object sender, RoutedEventArgs e) =>
            {
                if (_isVisibleToUser)
                {
                    StartListenPaintEvent();
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        private float _oldTmpHeight = 1.0f;
        private int _oldTmpWidth = 1;
        private float _oldTmpMaxY = 1.0f;
        private float _oldTmpMinY = 1.0f;
        private int _oldTmpDisplayWindowMS = 1;
        private bool _oldTmpSweepOrRoll = false;

        /// <summary>
        /// 
        /// </summary>
        protected float _oldYratio = 10;
        protected float _zeroY = 10;
        protected float _thresholdY = 10;

        /// <summary>
        /// 绘制到shadow中
        /// </summary>
        /// <param name="floats"></param>
        /// <param name="offset"></param>
        /// <param name="len"></param>
        /// <param name="recvdCount"></param>
        /// <param name="sampleRate"></param>
        /// <returns></returns>
        public virtual bool PaintToShadowAsync(float[] floats, int offset, int len, long recvdCount, double sampleRate)
        {
            do
            {
                try
                {
                    var yratio = _oldYratio;

                    //
                    var tmpHeight = _height;
                    var tmpWidth = _width;
                    var tmpMaxY = _maxY;
                    var tmpMinY = _minY;
                    var tmpDisplayWindowMS = _displayWindowMS;
                    var tmpSweepOrRoll = _sweepOrRoll;

                    //
                    var clear = false;
                    var stretched = false;
                    if (_channelChanged)
                    {
                        _channelChanged = false;
                        clear = true;
                    }
                    else if (tmpSweepOrRoll != _oldTmpSweepOrRoll || tmpDisplayWindowMS != _oldTmpDisplayWindowMS)
                    {
                        clear = true;
                        if (tmpHeight != _oldTmpHeight || tmpMaxY != _oldTmpMaxY || tmpMinY != _oldTmpMinY)
                        {
                            yratio = tmpHeight / (tmpMaxY - tmpMinY);
                            _oldYratio = yratio;
                        }

                        //
                        _oldTmpDisplayWindowMS = tmpDisplayWindowMS;
                        _oldTmpSweepOrRoll = tmpSweepOrRoll;
                    }
                    else
                    {
                        //
                        if (tmpHeight != _oldTmpHeight || tmpMaxY != _oldTmpMaxY || tmpMinY != _oldTmpMinY)
                        {
                            yratio = tmpHeight / (tmpMaxY - tmpMinY);
                            if (_pts != null && _pts.Length > 0)
                            {
                                stretched = true;
                                for (int i = 0; i < _pts.Length; i++)
                                {
                                    var val = (_oldTmpHeight - _pts[i].Y) / _oldYratio + _oldTmpMinY;
                                    _pts[i].Y = tmpHeight - yratio * (val - tmpMinY);
                                }
                            }
                            _oldYratio = yratio;

                            //
                            _oldTmpHeight = tmpHeight;
                            _oldTmpMaxY = tmpMaxY;
                            _oldTmpMinY = tmpMinY;
                        }

                        //
                        if (tmpWidth != _oldTmpWidth)
                        {
                            if (_pts != null && _pts.Length > 0)
                            {
                                stretched = true;
                                var step = 1.0f * tmpWidth / _pts.Length;
                                for (int i = 0; i < _pts.Length; i++)
                                {
                                    _pts[i].X = i * step;
                                }
                            }
                            _oldTmpWidth = tmpWidth;
                        }
                    }

                    //
                    _zeroY = tmpHeight + yratio * tmpMinY;
                    _thresholdY = tmpHeight - yratio * (_threshold - tmpMinY);

                    //先将数据缓存起来
                    var endFloatIndex = len + offset;
                    for (int i = offset; i < endFloatIndex; i++)
                    {
                        _floats.Add(floats[i]);
                    }

                    //
                    if (clear)
                    {
                        _pts = null;
                        _floats.Clear();
                        _ys.Clear();
                        PaintPts();
                    }
                    else if (stretched || len == 0)
                    {
                        PaintPts();
                    }
                    else
                    {
                        //界面显示总数据个数
                        var displayFloats = Math.Max(1, Convert.ToInt32(tmpDisplayWindowMS * sampleRate / 1000.0));

                        //sweep模式 / roll模式
                        if (tmpSweepOrRoll)
                        {
                            if (displayFloats < tmpWidth)
                            {
                                //时间范围较小，数据点比较少，绘制点数==数据点数
                                if (_pts == null || _pts.Length != displayFloats)
                                {
                                    _pts = new PointF[displayFloats];
                                    var step = 1.0f * tmpWidth / displayFloats;
                                    var defaultY = this.GetDefaultY();
                                    for (int i = 0; i < displayFloats; i++)
                                    {
                                        _pts[i] = new PointF { X = i * step, Y = defaultY }; //提前分配X
                                    }
                                }

                                //每个数据点直接放在point上
                                var ptStart = recvdCount - _floats.Count;
                                for (int i = 0; i < _floats.Count; i++)
                                {
                                    _pts[Convert.ToInt32((ptStart + i) % displayFloats)].Y = tmpHeight - yratio * (_floats[i] - tmpMinY);
                                }
                                _sweepLineX = _pts[Convert.ToInt32((recvdCount - 1) % displayFloats)].X;

                                //
                                _floats.Clear();
                            }
                            else
                            {
                                //降采样->max 和 min 成对出现
                                var ptPairCount = tmpWidth;
                                var ptCount = ptPairCount * 2;

                                //
                                if (_pts == null || _pts.Length != ptCount)
                                {
                                    _pts = new PointF[ptCount];
                                    var defaultY = this.GetDefaultY();
                                    for (int i = 0; i < ptCount; i++)
                                    {
                                        _pts[i] = new PointF { X = i / 2, Y = defaultY };
                                    }
                                }

                                //每2个像素对应多少个float数据;(max + min) = pt pair
                                var floatPer2Pt = tmpDisplayWindowMS * sampleRate / 1000.0 / ptPairCount;

                                //当前数据对应哪几对
                                var floatStartIndex = recvdCount - _floats.Count;
                                var ptPairStart = Convert.ToInt64(Math.Floor(floatStartIndex / floatPer2Pt));
                                var ptPairEnd = Convert.ToInt64(Math.Floor(recvdCount / floatPer2Pt));

                                //循环处理每对：max + min
                                var removed = 0;
                                for (long ptPairIndex = ptPairStart; ptPairIndex < ptPairEnd; ptPairIndex++)
                                {
                                    //计算在缓存中的位置
                                    var begin = Math.Max(0, Convert.ToInt32(Math.Floor(ptPairIndex * floatPer2Pt - floatStartIndex)));
                                    var end = Convert.ToInt32(Math.Floor((ptPairIndex + 1) * floatPer2Pt - floatStartIndex));

                                    //求取最大值和最小值
                                    if (begin < end)
                                    {
                                        var maxVal = float.MinValue;
                                        var minVal = float.MaxValue;

                                        //求最大值最小值
                                        for (int k = begin; k < end; k++)
                                        {
                                            if (_floats[k] > maxVal)
                                            {
                                                maxVal = _floats[k];
                                            }
                                            if (_floats[k] < minVal)
                                            {
                                                minVal = _floats[k];
                                            }
                                        }

                                        //
                                        if (maxVal - minVal > 1500)
                                        {
                                            maxVal = minVal;
                                        }

                                        //更新进绘制点
                                        var ptIndex = (ptPairIndex + ptPairCount) % ptPairCount * 2;
                                        _pts[ptIndex].Y = tmpHeight - yratio * (maxVal - tmpMinY);
                                        _pts[ptIndex + 1].Y = tmpHeight - yratio * (minVal - tmpMinY);
                                        _sweepLineX = _pts[ptIndex + 1].X;
                                    }

                                    //更新已用过的数据点
                                    removed = end;
                                }

                                //有使用过的数据点，则清理掉
                                if (removed > 0)
                                {
                                    _floats.RemoveRange(0, removed);
                                }
                            }
                        }
                        else
                        {
                            _sweepLineX = short.MinValue;
                            if (displayFloats < tmpWidth)
                            {
                                //时间范围较小，数据点比较少，绘制点数==数据点数
                                if (_pts == null || _pts.Length != displayFloats)
                                {
                                    _pts = new PointF[displayFloats];
                                    var step = 1.0f * tmpWidth / displayFloats;
                                    var defaultY = this.GetDefaultY();
                                    for (int i = 0; i < displayFloats; i++)
                                    {
                                        _pts[i] = new PointF { X = i * step, Y = defaultY }; //提前分配X
                                    }
                                }

                                //将 老数据 往前移动
                                var newLen = Math.Min(_floats.Count, displayFloats);
                                var moveLen = _pts.Length - newLen;
                                for (int i = 0, j = newLen; i < moveLen; i++, j++)
                                {
                                    _pts[i].Y = _pts[j].Y;
                                }

                                //将 新数据库 补在后面
                                for (int i = moveLen, j = 0; i < _pts.Length; i++, j++)
                                {
                                    _pts[i].Y = tmpHeight - yratio * (_floats[j] - tmpMinY);
                                }

                                //
                                _floats.Clear();
                            }
                            else
                            {
                                //降采样->max 和 min 成对出现
                                var ptPairCount = tmpWidth;
                                var ptCount = ptPairCount * 2;

                                //
                                if (_pts == null || _pts.Length != ptCount)
                                {
                                    _pts = new PointF[ptCount];
                                    var defaultY = this.GetDefaultY();
                                    for (int i = 0; i < ptCount; i++)
                                    {
                                        _pts[i] = new PointF { X = i / 2, Y = defaultY };
                                    }
                                }

                                //每2个像素对应多少个float数据;(max + min) = pt pair
                                var floatPer2Pt = tmpDisplayWindowMS * sampleRate / 1000.0 / ptPairCount;

                                //当前数据对应哪几对
                                var newLen = Math.Min(Convert.ToInt32(Math.Floor(_floats.Count / floatPer2Pt)) * 2, ptCount);
                                if (newLen > 0)
                                {
                                    //将 老数据 往前移动
                                    var moveLen = ptCount - newLen;
                                    for (int i = 0, j = newLen; i < moveLen; i++, j++)
                                    {
                                        _pts[i].Y = _pts[j].Y;
                                    }

                                    //将 新数据库 补在后面
                                    var removed = 0;
                                    for (int i = moveLen, j = 0; i < ptCount; i += 2, j++)
                                    {
                                        //计算在缓存中的位置
                                        var begin = Convert.ToInt32(Math.Floor(j * floatPer2Pt));
                                        var end = Convert.ToInt32(Math.Floor((j + 1) * floatPer2Pt));
                                        removed = end;

                                        //求取最大值和最小值
                                        if (begin < end)
                                        {
                                            var maxVal = float.MinValue;
                                            var minVal = float.MaxValue;

                                            //求最大值最小值
                                            for (int k = begin; k < end; k++)
                                            {
                                                if (_floats[k] > maxVal)
                                                {
                                                    maxVal = _floats[k];
                                                }
                                                if (_floats[k] < minVal)
                                                {
                                                    minVal = _floats[k];
                                                }
                                            }

                                            //
                                            _pts[i].Y = tmpHeight - yratio * (maxVal - tmpMinY);
                                            _pts[i + 1].Y = tmpHeight - yratio * (minVal - tmpMinY);
                                        }
                                    }

                                    //有使用过的数据点，则清理掉
                                    if (removed > 0)
                                    {
                                        _floats.RemoveRange(0, removed);
                                    }
                                }
                            }
                        }

                        //
                        PaintPts();
                    }
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{nameof(WaveformPainter)}", ex);
                }

                //
                UIHelper.BeginInvoke(() =>
                {
                    if (_bindControl != null)
                    {
                        _bindControl.SweepX = _sweepLineX;
                        _bindControl.InvalidateVisual();
                    }
                });
            } while (false);

            //
            return true;
        }

        /// <summary>
        /// 绘制零值水平线
        /// </summary>
        /// <param name="g"></param>
        protected void PaintZeroLine(Graphics g)
        {
            if (ShowZeroLine)
            {
                g.DrawLine(Pens.DimGray, new PointF(0, _zeroY), new PointF(_width, _zeroY));
            }
        }

        /// <summary>
        /// 绘制阈值水平线
        /// </summary>
        /// <param name="g"></param>
        protected void PaintThresholdLine(Graphics g)
        {
            if (ShowThresholdLine)
            {
                g.DrawLine(Pens.Blue, new PointF(0, _thresholdY), new PointF(_width, _thresholdY));
            }
        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected void StartListenPaintEvent()
        {
            SpikeSumPainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected void StopListenPaintEvent()
        {
            SpikeSumPainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected float GetDefaultY()
        {
            return _zeroY;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void PaintPts()
        {
            base.PaintToShadow(BackgroundColor, OriginPt, Width, Height, true, (g) =>
            {
                if (_pts != null)
                {
                    var list = new List<PointF>
                    {
                        new PointF(0, _pts[0].Y)
                    };
                    for (int i = 0; i < _pts.Length - 1; i++)
                    {
                        list.Add(_pts[i]);
                        list.Add(new PointF(_pts[i + 1].X, _pts[i].Y));
                    }
                    list.Add(_pts[^1]);
                    list.Add(new PointF(Width, _pts[^1].Y));

                    //
                    if (!list.Any(r => r.Y > short.MaxValue || r.Y < short.MinValue || float.IsInfinity(r.Y)))
                    {
                        g.DrawLines(LineDrawingPen, list.ToArray());
                    }
                }

                //
                PaintZeroLine(g);
            });
        }
    }
}