﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.SpikeSumCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeSumChart : TimeGalleryBase
    {
        /// <summary>
        /// 
        /// </summary>
        static SpikeSumChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpikeSumChart), new FrameworkPropertyMetadata(typeof(SpikeSumChart)));
        }

        /// <summary>
        /// 采集窗口
        /// </summary>
        public int SpikeSumWindowMS
        {
            get { return (int)GetValue(SpikeSumWindowMSProperty); }
            set { SetValue(SpikeSumWindowMSProperty, value); }
        }
        public static readonly DependencyProperty SpikeSumWindowMSProperty = DependencyProperty.Register(nameof(SpikeSumWindowMS), typeof(int), typeof(SpikeSumChart), new PropertyMetadata(10000, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 采集步长
        /// </summary>
        public int SpikeSumStepMS
        {
            get { return (int)GetValue(SpikeSumStepMSProperty); }
            set { SetValue(SpikeSumStepMSProperty, value); }
        }
        public static readonly DependencyProperty SpikeSumStepMSProperty = DependencyProperty.Register(nameof(SpikeSumStepMS), typeof(int), typeof(SpikeSumChart), new PropertyMetadata(1000, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 阈值
        /// </summary>
        public double Threshold
        {
            get { return (double)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register(nameof(Threshold), typeof(double), typeof(SpikeSumChart), new PropertyMetadata(-70d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否展示阈值线
        /// </summary>
        public bool ShowThresholdLine
        {
            get { return (bool)GetValue(ShowThresholdLineProperty); }
            set { SetValue(ShowThresholdLineProperty, value); }
        }
        public static readonly DependencyProperty ShowThresholdLineProperty = DependencyProperty.Register(nameof(ShowThresholdLine), typeof(bool), typeof(SpikeSumChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否展示水平零值线
        /// </summary>
        public bool ShowZeroLine
        {
            get { return (bool)GetValue(ShowZeroLineProperty); }
            set { SetValue(ShowZeroLineProperty, value); }
        }
        public static readonly DependencyProperty ShowZeroLineProperty = DependencyProperty.Register(nameof(ShowZeroLine), typeof(bool), typeof(SpikeSumChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 绘制模式：true:sweep, false:roll
        /// </summary>
        public bool SweepOrRoll
        {
            get { return (bool)GetValue(SweepOrRollProperty); }
            set { SetValue(SweepOrRollProperty, value); }
        }
        public static readonly DependencyProperty SweepOrRollProperty = DependencyProperty.Register(nameof(SweepOrRoll), typeof(bool), typeof(SpikeSumChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// sweep模式下的x
        /// </summary>
        public float SweepX
        {
            get { return (float)GetValue(SweepXProperty); }
            set { SetValue(SweepXProperty, value); }
        }
        public static readonly DependencyProperty SweepXProperty = DependencyProperty.Register(nameof(SweepX), typeof(float), typeof(SpikeSumChart), new PropertyMetadata(-1f, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            UpdatePainterOnPropertyChanged(e);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdatePainterOnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (_painter == null)
            {
                return;
            }

            //
            if (e.Property.Equals(SweepXProperty))
            {
                return;
            }
            else if (e.Property.Equals(IsVisibleToUserProperty))
            {
                _painter.IsVisibleToUser = IsVisibleToUser;
            }
            else if (e.Property.Equals(DrawAreaWidthProperty))
            {
                _painter.Width = (int)DrawAreaWidth;
            }
            else if (e.Property.Equals(DrawAreaHeightProperty))
            {
                _painter.Height = DrawAreaHeight;
            }
            else if (e.Property.Equals(OriginPointProperty))
            {
                _painter.OriginPt = OriginPoint;
            }
            else if (e.Property.Equals(ChannelProperty))
            {
                _painter.Channel = Channel;
            }
            else if (e.Property.Equals(BackgroundProperty))
            {
                _painter.BackgroundColor = ((SolidColorBrush)Background).Color.ToColor();
            }
            else if (e.Property.Equals(MaxYProperty))
            {
                _painter.MaxY = MaxY;
            }
            else if (e.Property.Equals(MinYProperty))
            {
                _painter.MinY = MinY;
            }
            else if (e.Property.Equals(SweepOrRollProperty))
            {
                _painter.SweepOrRoll = SweepOrRoll;
            }
            else if (e.Property.Equals(ShowThresholdLineProperty))
            {
                _painter.ShowThresholdLine = ShowThresholdLine;
            }
            else if (e.Property.Equals(ThresholdProperty))
            {
                _painter.Threshold = (float)Threshold;
            }
            else if (e.Property.Equals(ShowZeroLineProperty))
            {
                _painter.ShowZeroLine = ShowZeroLine;
            }
            else if (e.Property.Equals(DisplayWindowMSProperty))
            {
                _painter.DisplayWindowMS = DisplayWindowMS;
            }
            else if (e.Property.Equals(LineBrushProperty))
            {
                _painter.LineDrawingPen = new System.Drawing.Pen(((SolidColorBrush)LineBrush).Color.ToColor(), LINE_WIDTH); ;
            }
        }

        /// <summary>
        /// 绘图工具
        /// </summary>
        private readonly SpikeSumPainter _painter = null;

        /// <summary>
        /// 
        /// </summary>
        public SpikeSumChart()
        {
            _painter = new SpikeSumPainter(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void PaintHover(DrawingContext ctx)
        {
            //绘制边框
            PaintBorder(ctx);

            //
            {
                var yPerPixel = (MaxY - MinY) / DrawAreaHeight;
                var zeroY = OriginPoint.Y - (0 - MinY) / yPerPixel;
                var yVal = Math.Round((zeroY - MouseHoverY) * yPerPixel);
                var y = OriginPoint.Y - (yVal - MinY) / yPerPixel;

                //
                {
                    var x = this.MakeLineThin(MouseHoverX - SCALE_BAR_GAP * 3);
                    var x1 = x - SCALE_BAR_GAP;
                    var x2 = x + SCALE_BAR_GAP;
                    ctx.DrawLine(_scaleBarPen, new Point(x, zeroY), new Point(x, y));
                    ctx.DrawLine(_scaleBarPen, new Point(x1, zeroY), new Point(x2, zeroY));
                    ctx.DrawLine(_scaleBarPen, new Point(x1, y), new Point(x2, y));

                    //
                    var txt = this.CreateFormatText($"{yVal}");
                    ctx.DrawText(txt, new Point(x - txt.Width - TEXT_MARGIN, (y + zeroY - txt.Height) / 2));
                }

                //
                {
                    var sumWindowWidth = DrawAreaWidth * SpikeSumWindowMS / DisplayWindowMS;
                    var x1 = this.MakeLineThin(MouseHoverX - sumWindowWidth);
                    var x2 = this.MakeLineThin(MouseHoverX);
                    var y1 = YAxisMaxPoint.Y;
                    var y2 = this.MakeLineThin(y1 + SCALE_BAR_GAP);
                    var y3 = y2 + SCALE_BAR_GAP;

                    ctx.DrawLine(_scaleBarPen, new Point(x1, y1), new Point(x1, y3));
                    ctx.DrawLine(_scaleBarPen, new Point(x2, y1), new Point(x2, y3));
                    ctx.DrawLine(_scaleBarPen, new Point(x1, y2), new Point(x2, y2));

                    //
                    //
                    var txt = this.CreateFormatText($"{SpikeSumWindowMS}ms");
                    ctx.DrawText(txt, new Point(x1 + sumWindowWidth / 2 - txt.Width / 2, y3));
                }

                //
                {
                    var stepWidth = DrawAreaWidth * SpikeSumStepMS / DisplayWindowMS;
                    var x1 = this.MakeLineThin(MouseHoverX);
                    var x2 = this.MakeLineThin(MouseHoverX + stepWidth);
                    var y1 = MouseHoverY - SCALE_BAR_GAP;
                    var y2 = this.MakeLineThin(y1 - SCALE_BAR_GAP);
                    var y3 = y2 - SCALE_BAR_GAP;

                    ctx.DrawLine(_scaleBarPen, new Point(x1, y1), new Point(x1, y3));
                    ctx.DrawLine(_scaleBarPen, new Point(x2, y1), new Point(x2, y3));
                    ctx.DrawLine(_scaleBarPen, new Point(x1, y2), new Point(x2, y2));

                    //
                    //
                    var txt = this.CreateFormatText($"{SpikeSumStepMS}ms");
                    ctx.DrawText(txt, new Point(x1 + stepWidth / 2 - txt.Width / 2, y3 - txt.Height));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            _painter.CopyFromShadow(ctx, OriginPoint, DrawAreaWidth, DrawAreaHeight);
        }

        /// <summary>
        /// 绘制标题
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            PaintChannelNameOnMidLeft(ctx);
        }
    }
}
