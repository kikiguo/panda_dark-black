﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys
{
    /// <summary>
    /// 
    /// </summary>
    public class TimeAxis : TimeGalleryBase
    {
        /// <summary>
        /// 屏幕的最大高度
        /// </summary>
        public const int SCREEN_MAX_HEIGHT = 6000;

        /// <summary>
        /// 
        /// </summary>
        static TimeAxis()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeAxis), new FrameworkPropertyMetadata(typeof(TimeAxis)));
        }

        /// <summary>
        /// x轴刻度在线上方
        /// </summary>
        public bool XTickTxtAboveLine
        {
            get { return (bool)GetValue(XTickTxtAboveLineProperty); }
            set { SetValue(XTickTxtAboveLineProperty, value); }
        }
        public static readonly DependencyProperty XTickTxtAboveLineProperty = DependencyProperty.Register(nameof(XTickTxtAboveLine), typeof(bool), typeof(TimeAxis), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// sweep模式下的x
        /// </summary>
        public float SweepX
        {
            get { return (float)GetValue(SweepXProperty); }
            set { SetValue(SweepXProperty, value); }
        }
        public static readonly DependencyProperty SweepXProperty = DependencyProperty.Register(nameof(SweepX), typeof(float), typeof(TimeAxis), new PropertyMetadata(-1f, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(DisplayWindowMSProperty) || e.Property.Equals(DrawAreaWidthProperty))
            {
                if (DisplayWindowMS > 0 && DrawAreaWidth > 0)
                {
                    _tickTxts.Clear();
                    _tickTxtXs.Clear();
                    _tickXs.Clear();

                    //
                    var list = new List<int> { 1, 2, 4, 5 };
                    var calced = false;
                    for (int power = 0; power < 5; power++)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            var stepMS = Convert.ToInt32(list[i] * Math.Pow(10, power));
                            if (DrawAreaWidth * stepMS / DisplayWindowMS > 100 && DisplayWindowMS % stepMS == 0)
                            {
                                calced = true;
                                var count = DisplayWindowMS / stepMS;
                                var stepPx = DrawAreaWidth / count;
                                for (int k = 0; k <= count; k++)
                                {
                                    if (k == 0)
                                    {
                                        var txt = this.CreateFormatText($"{k * stepMS}");
                                        _tickTxts.Add(txt);
                                        _tickTxtXs.Add(OriginPoint.X);
                                    }
                                    else if (k == count)
                                    {
                                        var txt = this.CreateFormatText($"{k * stepMS}ms");
                                        _tickTxts.Add(txt);
                                        _tickTxtXs.Add(OriginPoint.X + DrawAreaWidth - txt.Width);
                                    }
                                    else
                                    {
                                        var txt = this.CreateFormatText($"{k * stepMS}");
                                        _tickTxts.Add(txt);
                                        _tickTxtXs.Add(OriginPoint.X + k * stepPx - txt.Width / 2);
                                    }
                                    _tickXs.Add(OriginPoint.X + k * stepPx);
                                }
                                break;
                            }
                        }
                        if (calced)
                        {
                            break;
                        }
                    }

                    //
                    if (_tickTxts.Count == 0)
                    {
                        {
                            var txt = this.CreateFormatText($"{0}-{DisplayWindowMS}ms");
                            _tickTxts.Add(txt);
                            _tickTxtXs.Add(OriginPoint.X + (DrawAreaWidth - txt.Width) / 2);
                            _tickXs.Add(OriginPoint.X);
                        }
                        {
                            var txt = this.CreateFormatText($"");
                            _tickTxts.Add(txt);
                            _tickTxtXs.Add(OriginPoint.X + (DrawAreaWidth - txt.Width) / 2);
                            _tickXs.Add(OriginPoint.X + DrawAreaWidth);
                        }
                    }
                }

                //
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(XTickTxtAboveLineProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(SweepXProperty))
            {
                InvalidateVisual();
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly List<FormattedText> _tickTxts = new List<FormattedText>();
        private readonly List<double> _tickXs = new List<double>();
        private readonly List<double> _tickTxtXs = new List<double>();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //绘制X轴
            {
                var y = this.MakeLineThin(OriginPoint.Y);
                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
            }

            //绘制X轴刻度
            PaintXTicks(ctx);
        }

        /// <summary>
        /// 绘制X轴刻度
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="tickPen"></param>
        /// <param name="OriginPoint"></param>
        /// <param name="DrawAreaWidth"></param>
        protected void PaintXTicks(DrawingContext ctx)
        {
            if (DisplayWindowMS <= 0)
            {
                return;
            }
            if (_tickTxts.Count <= 0)
            {
                return;
            }

            //
            var txtY = XTickTxtAboveLine ? (OriginPoint.Y - _tickTxts[0].Height - TEXT_MARGIN) : (OriginPoint.Y + TEXT_MARGIN);
            var lineY1 = OriginPoint.Y - ((IsMouseHover && !XTickTxtAboveLine) ? SCREEN_MAX_HEIGHT : TEXT_MARGIN);
            var lineY2 = OriginPoint.Y + ((IsMouseHover && XTickTxtAboveLine) ? SCREEN_MAX_HEIGHT : TEXT_MARGIN);
            var tickLinePen = IsMouseHover ? _dimGrayPen : _foregroundPen;
            for (int i = 0; i < _tickTxts.Count; i++)
            {
                var txt = _tickTxts[i];
                var txtX = _tickTxtXs[i];
                var lineX = this.MakeLineThin(_tickXs[i]);
                ctx.DrawText(txt, new Point(txtX, txtY));
                ctx.DrawLine(tickLinePen, new Point(lineX, lineY1), new Point(lineX, lineY2));
            }

            //
            if (SweepX > 0)
            {
                ctx.DrawLine(_dimGrayPen, new Point(OriginPoint.X + SweepX, OriginPoint.Y), new Point(OriginPoint.X + SweepX, OriginPoint.Y + (XTickTxtAboveLine ? SCREEN_MAX_HEIGHT : -SCREEN_MAX_HEIGHT)));
            }

            //
            if (IsMouseHover)
            {
                var mouseX = this.MakeLineThin(MouseHoverX);
                ctx.DrawLine(_yellowPen, new Point(mouseX, lineY1), new Point(mouseX, lineY2));

                //
                var leftMS = Convert.ToInt32(Math.Floor((mouseX - OriginPoint.X) / _xratio));
                var rightMS = DisplayWindowMS - leftMS;

                var leftTxt = this.CreateFormatText($"{leftMS}ms", _yellowPen.Brush);
                ctx.DrawText(leftTxt, new Point(mouseX - leftTxt.Width - TEXT_MARGIN, OriginPoint.Y + TEXT_MARGIN));

                var rightTxt = this.CreateFormatText($"{rightMS}ms", _yellowPen.Brush);
                ctx.DrawText(rightTxt, new Point(mouseX + TEXT_MARGIN, OriginPoint.Y + TEXT_MARGIN));

                //
                ctx.DrawLine(_yellowPen, new Point(OriginPoint.X + leftMS * _xratio, OriginPoint.Y), new Point(OriginPoint.X + (leftMS + 1) * _xratio, OriginPoint.Y));
            }
        }
    }
}
