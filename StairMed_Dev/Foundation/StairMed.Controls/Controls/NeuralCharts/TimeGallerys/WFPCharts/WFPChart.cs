﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.WFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class WFPChart : WaveformChart
    {
        /// <summary>
        /// 
        /// </summary>
        static WFPChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WFPChart), new FrameworkPropertyMetadata(typeof(WFPChart)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override WaveformPainter GetPainter()
        {
            return new WFPPainter(this);
        }
    }
}
