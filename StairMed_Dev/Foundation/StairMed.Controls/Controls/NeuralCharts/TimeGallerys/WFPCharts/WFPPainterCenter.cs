﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.WFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class WFPPainterCenter : WaveformPainterCenter
    {
        #region 单例

        private static readonly WFPPainterCenter _instance = new WFPPainterCenter();
        public static WFPPainterCenter Instance => _instance;
        private WFPPainterCenter()
        {

        }

        #endregion
    }
}
