﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.HFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class HFPChart : WaveformChart
    {
        /// <summary>
        /// 
        /// </summary>
        static HFPChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HFPChart), new FrameworkPropertyMetadata(typeof(HFPChart)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override WaveformPainter GetPainter()
        {
            return new HFPPainter(this);
        }
    }
}