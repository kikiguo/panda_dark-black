﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Entity.DataPools;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.HFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class HFPPainter : WaveformPainter
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public HFPPainter(WaveformChart bindControl) : base(bindControl)
        {

        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected override void RegisterPainter()
        {
            HFPPainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected override void UnregisterPainter()
        {
            HFPPainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected override float GetDefaultY()
        {
            return Height / 2;
        }


        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        protected override float[] GetFloatsArray(Chunk chunk)
        {
            if (chunk.Hfps == null)
            {
                return null;
            }
            return chunk.Hfps.Array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk[] GetBufferedChunks()
        {
            return HFPPainterCenter.Instance.BufferedChunk.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk GetNewestChunk()
        {
            if (HFPPainterCenter.Instance.BufferedChunk.Count <= 0)
            {
                return null;
            }

            //
            return HFPPainterCenter.Instance.BufferedChunk.ToArray()[^1];
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ForceRepaint()
        {
            HFPPainterCenter.Instance.ForceRepaint();
        }
    }
}
