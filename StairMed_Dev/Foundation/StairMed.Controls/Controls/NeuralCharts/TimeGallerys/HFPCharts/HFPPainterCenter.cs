﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;


namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.HFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class HFPPainterCenter : WaveformPainterCenter
    {
        #region 单例

        private static readonly HFPPainterCenter _instance = new HFPPainterCenter();
        public static HFPPainterCenter Instance => _instance;
        private HFPPainterCenter()
        {

        }

        #endregion
    }
}
