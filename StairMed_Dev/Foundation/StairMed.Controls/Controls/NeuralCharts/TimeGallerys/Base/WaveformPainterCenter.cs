﻿using HDF5CSharp;
using StairMed.AsyncFIFO;
using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.LFPCharts;
using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.RawCharts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.Entity.DataPools;
using System.Collections.Generic;
using System.Reflection;
using System.Timers;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class WaveformPainterCenter
    {
        /// <summary>
        /// 视窗宽度最大N秒
        /// </summary>
        const int BufferedChunkS = 4;

        /// <summary>
        /// 第几次采集
        /// </summary>
        private int _collectId = 0;

        /// <summary>
        /// 
        /// </summary>
        private readonly Timer _forcePaintTimer = new Timer { Interval = 100, AutoReset = false, };

        /// <summary>
        /// 数据缓存
        /// </summary>
        internal readonly Queue<Chunk> BufferedChunk = new Queue<Chunk>();

        /// <summary>
        /// 
        /// </summary>
        private int _sampleRate = 30000;
        private int _sampleCountOfEachChannel = 10;

        /// <summary>
        /// 所有通道的painter
        /// </summary>
        private readonly Dictionary<int, HashSet<WaveformPainter>> _painterDict = new Dictionary<int, HashSet<WaveformPainter>>();
        private readonly Dictionary<int, HashSet<WaveformPainter>> _tempPainterDict = new Dictionary<int, HashSet<WaveformPainter>>();

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, int> _channelIndexInChunkDict = new Dictionary<int, int>();

        /// <summary>
        /// 异步调用
        /// </summary>
        private readonly IAsyncFIFO _chunkFifo;

        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        private readonly HashSet<string> SpikeScopeEnable = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.SpikeScopeEnable),
        };

        /// <summary>
        /// 
        /// </summary>
        protected WaveformPainterCenter()
        {
            _chunkFifo = new BlockFIFO { Name = nameof(WaveformPainterCenter), ConsumerAction = Consume };
            NeuralDataCollector.Instance.NewChunkReceived += (chunk) => _chunkFifo.Add(chunk);
            NeuralDataCollector.Instance.RunningStateChanged += (iscollecting) =>
            {
                //true表示重新开始采集
                if (iscollecting)
                {
                    _collectId++;
                    BufferedChunk.Clear();
                    _channelIndexInChunkDict.Clear();
                }
            };


            //
            _forcePaintTimer.Elapsed += (object sender, ElapsedEventArgs e) => _chunkFifo.Add(null); ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            if (objs != null && objs.Length > 0)
            {
                if (objs[0] is Chunk chunk)
                {
                    HandleRecvdChunk(chunk);
                    PainterToRepaint();
                }
                else
                {
                    HandlePainterRegisterEvent((bool)objs[0], (int)objs[1], (WaveformPainter)objs[2]);
                }

            }
            else
            {
                //为空为强制刷新信号
                PainterToRepaint();
            }
            Settings_OnSettingChanged();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        protected void Settings_OnSettingChanged()
        {
            lock (_painterDict)
            {
                    if (Settings.SpikeScopeEnable)
               {
                    if (_painterDict.ContainsKey(Settings.SpikeScopeChannel))
                    {
                        
                        if (_tempPainterDict.Count == 0)
                        {
                            foreach(var kvp in _painterDict)
                            {
                                _tempPainterDict.Add(kvp.Key, kvp.Value);
                            }
                        }
                        _painterDict.Remove(Settings.SpikeScopeChannel);
                    }
                  }
                    else
                    {
                        foreach (var kvp in _painterDict)
                    {
                        if(kvp.Key==Settings.SpikeScopeChannel)
                        {
                            return;
                        }

                    }

                    foreach (var kvp in _tempPainterDict)
                    {
                        if (kvp.Key == Settings.SpikeScopeChannel)
                        {
                            _painterDict.Add(kvp.Key,kvp.Value);
                        }

                    }

            
                  
             }
                
            }
            
            
        }



        /// <summary>
        /// 通道注册或取消注册
        /// </summary>
        /// <param name="isregister"></param>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        private void HandlePainterRegisterEvent(bool isregister, int inputChannel, WaveformPainter painter)
        {
            if (isregister)
            {
                if (!_painterDict.ContainsKey(inputChannel))
                {
                    _painterDict[inputChannel] = new HashSet<WaveformPainter>();
                }
                _painterDict[inputChannel].Add(painter);
            }
            else
            {
                if (_painterDict.ContainsKey(inputChannel))
                {
                    _painterDict[inputChannel].Remove(painter);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void HandleRecvdChunk(Chunk chunk)
        {
            
            //更新采样率等信息
            _sampleRate = chunk.SampleRate;
            _sampleCountOfEachChannel = chunk.OneChannelSamples;

            //将数据缓存
            BufferedChunk.Enqueue(chunk);
            var maxBufferedChunk = (_sampleRate * BufferedChunkS + _sampleCountOfEachChannel - 1) / _sampleCountOfEachChannel;
            //var maxBufferedChunk = 10;
            while (BufferedChunk.Count > maxBufferedChunk)
            {
                BufferedChunk.Dequeue();
            }

            //
            var channels = chunk.InputChannelInReports;
            for (int i = 0; i < channels.Count; i++)
            {
                _channelIndexInChunkDict[channels[i]] = i;
            }
        }

        /// <summary>
        /// 控件执行绘制
        /// </summary>
        private void PainterToRepaint()
        {
            //执行绘制
            foreach (var kvp in _painterDict)
            {
                var inputChannel = kvp.Key;
                var channelIndexInChunk = -1;
                if (_channelIndexInChunkDict.ContainsKey(inputChannel))
                {
                    channelIndexInChunk = _channelIndexInChunkDict[inputChannel];

                }
                foreach (var painter in kvp.Value)
                {
                    if (painter != null)
                    {
                        painter.Repaint(inputChannel, channelIndexInChunk, _sampleRate, _sampleCountOfEachChannel, _collectId);
                    }
                }
            }
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        internal void RegisterPainter(int inputChannel, WaveformPainter painter)
        {
            _chunkFifo.Add(true, inputChannel, painter); //通过在fifo线程注册，避免_painterDict的多线程访问，避免频繁lock
        }

        /// <summary>
        /// 取消注册
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        internal void UnregisterPainter(int inputChannel, WaveformPainter painter)
        {
            _chunkFifo.Add(false, inputChannel, painter); //通过在fifo线程注册，避免_painterDict的多线程访问，避免频繁lock
        }

        /// <summary>
        /// 强制重绘
        /// </summary>
        internal void ForceRepaint()
        {
            _forcePaintTimer.Stop();
            _forcePaintTimer.Interval = 50;
            _forcePaintTimer.Start();
        }
    }
}
