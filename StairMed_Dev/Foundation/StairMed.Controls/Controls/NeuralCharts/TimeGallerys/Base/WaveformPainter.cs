﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using StairMed.Core.Settings;
using StairMed.Entity.DataPools;
using StairMed.Tools;
using System;
using System.Drawing;
using System.Linq;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class WaveformPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected float _sweepLineX = 0;
        protected PointF[] _pts = null;

        /// <summary>
        /// 是否可见
        /// </summary>
        private bool _isVisibleToUser = false;
        public bool IsVisibleToUser
        {
            set
            {
                if (_isVisibleToUser != value)
                {
                    _isVisibleToUser = value;
                    ForceRepaint();
                }
            }
            get
            {
                return _isVisibleToUser;
            }
        }

        /// <summary>
        /// 宽度
        /// </summary>
        private int _width = 10;
        public int Width
        {
            get { return _width; }
            set
            {
                if (value != _width)
                {
                    _width = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 高度
        /// </summary>
        private float _height = 0.0f;
        public float Height
        {
            get { return _height; }
            set
            {
                if (value != _height)
                {
                    _height = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 最大Y值
        /// </summary>
        private float _maxY = 100f;
        public float MaxY
        {
            get { return _maxY; }
            set
            {
                if (value != _maxY)
                {
                    _maxY = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 最小Y值
        /// </summary>
        private float _minY = 10;
        public float MinY
        {
            get { return _minY; }
            set
            {
                if (value != _minY)
                {
                    _minY = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 视图宽度
        /// </summary>
        private int _displayWindowMS = 0;
        public int DisplayWindowMS
        {
            get { return _displayWindowMS; }
            set
            {
                if (value != _displayWindowMS)
                {
                    _displayWindowMS = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// sweep or roll 模式
        /// </summary>
        private bool _sweepOrRoll = false;
        public bool SweepOrRoll
        {
            get { return _sweepOrRoll; }
            set
            {
                if (value != _sweepOrRoll)
                {
                    _sweepOrRoll = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// roll偏移 模式
        /// </summary>
        private int _rollOffsetTimeMs = 0;
        public int RollOffsetTimeMs
        {
            get { return _rollOffsetTimeMs; }
            set
            {
                if (value != _rollOffsetTimeMs)
                {
                    _rollOffsetTimeMs = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private float _threshold = -70f;
        public float Threshold
        {
            get { return _threshold; }
            set
            {
                if (value != _threshold)
                {
                    _threshold = value;
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 绑定的通道
        /// </summary>
        private int _channel = -1;
        public int Channel
        {
            get { return _channel; }
            set
            {
                if (_channel != value)
                {
                    UnregisterPainter();
                    _channel = value;
                    RegisterPainter();
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Point OriginPt = new System.Windows.Point();
        public Color BackgroundColor = Color.Aqua;
        public Pen LineDrawingPen = null;
        public bool ShowThresholdLine = false;
        public bool ShowZeroLine = false;

        /// <summary>
        /// 对应的Control
        /// </summary>
        protected readonly WaveformChart _bindControl;

        /// <summary>
        /// 构造函数
        /// </summary>
        public WaveformPainter(WaveformChart bindControl)
        {
            _bindControl = bindControl;

            //
            _bindControl.Unloaded += (object sender, RoutedEventArgs e) =>
            {
                UnregisterPainter();
            };
            _bindControl.Loaded += (object sender, RoutedEventArgs e) =>
            {
                RegisterPainter();
            };
        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected abstract void RegisterPainter();

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected abstract void UnregisterPainter();

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected abstract float GetDefaultY();

        /// <summary>
        /// 
        /// </summary>
        private float _heightForPaint = 1.0f;
        private bool _heightForPaintChanged = true;

        /// <summary>
        /// 
        /// </summary>
        private int _widthForPaint = 1;
        private bool _widthForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private float _maxYForPaint = 1.0f;
        private bool _maxYForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private float _minYForPaint = 1.0f;
        private bool _minYForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private int _displayWindowMSForPaint = 1;
        private bool _displayWindowMSForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private bool _sweepOrRollForPaint = false;
        private bool _sweepOrRollForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private int _channelForPaint = 0;
        private bool _channelForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private bool _visibleForPaint = false;
        private bool _visibleForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private int _rollOffsetTimeMsForPaint = 0;
        private bool _rollOffsetTimeMsForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        protected float _zeroY = 10;
        protected float _thresholdY = 10;

        /// <summary>
        /// 
        /// </summary>
        protected double _sampleRate = 0.0;

        /// <summary>
        /// 
        /// </summary>
        private int _collectId = 0;

        /// <summary>
        /// 用于识别是刷新还是新来一个数据包
        /// </summary>
        private long _totalRecvd = -1;

        /// <summary>
        /// 对当前设置项快照一下，并记录是否有调整
        /// </summary>
        private void LoadPaintParams()
        {
            _heightForPaintChanged = _heightForPaint != Height;
            if (_heightForPaintChanged)
            {
                _heightForPaint = Height;
            }

            //
            _widthForPaintChanged = _widthForPaint != Width;
            if (_widthForPaintChanged)
            {
                _widthForPaint = Width;
            }

            //
            _maxYForPaintChanged = _maxYForPaint != MaxY;
            if (_maxYForPaintChanged)
            {
                _maxYForPaint = MaxY;
            }

            //
            _minYForPaintChanged = _minYForPaint != MinY;
            if (_minYForPaintChanged)
            {
                _minYForPaint = MinY;
            }

            //
            _displayWindowMSForPaintChanged = _displayWindowMSForPaint != DisplayWindowMS;
            if (_displayWindowMSForPaintChanged)
            {
                _displayWindowMSForPaint = DisplayWindowMS;
            }

            //
            _sweepOrRollForPaintChanged = _sweepOrRollForPaint != SweepOrRoll;
            if (_sweepOrRollForPaintChanged)
            {
                _sweepOrRollForPaint = SweepOrRoll;
            }

            //
            _visibleForPaintChanged = _visibleForPaint != IsVisibleToUser;
            if (_visibleForPaintChanged)
            {
                _visibleForPaint = IsVisibleToUser;
            }

            //
            _channelForPaintChanged = _channelForPaint != Channel;
            if (_channelForPaintChanged)
            {
                _channelForPaint = Channel;
            }

            //
            _rollOffsetTimeMsForPaintChanged = _rollOffsetTimeMsForPaint != RollOffsetTimeMs;
            if (_rollOffsetTimeMsForPaintChanged)
            {
                _rollOffsetTimeMsForPaint = RollOffsetTimeMs;
            }
        }
        private bool clean(int channel)
        {
            if (StairMedSettings.Instance.SpikeScopeEnable && StairMedSettings.Instance.SpikeScopeChannel == channel)
            {
                PaintToShadow(BackgroundColor, OriginPt, _widthForPaint, _heightForPaint, true, (g) =>
                {
                    PaintZeroLine(g);
                });
                UIHelper.BeginInvoke(() =>
                {
                    if (_bindControl != null)
                    {
                        _bindControl?.InvalidateVisual();
                    }
                });

                return true;
            }

            if (StairMedSettings.Instance.IsClean)
            {
                DateTime now = DateTime.Now;
                DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                long timestamp = (long)(now.ToUniversalTime() - epoch).TotalMilliseconds;


                if (StairMedSettings.Instance.cleanTiem < timestamp)
                {

                    StairMedSettings.Instance.IsClean = false;

                }
                // _snapshotScopes.Clear();
                // _frontScopes.Clear();
                var clean = true;
                PaintToShadow(BackgroundColor, OriginPt, _widthForPaint, _heightForPaint, clean, (g) =>
                {

                });
                UIHelper.BeginInvoke(() =>
                {
                    if (_bindControl != null)
                    {
                        _bindControl?.InvalidateVisual();
                    }
                });

                return true;

            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel">channelIndexInChunk对应的channel，【加此参数的原因：当切换channel时，chunk的缓存导致参数channelIndexInChunk与【字段_channel】不一致】</param>
        /// <param name="channelIndexInChunk"></param>
        /// <param name="sampleRate"></param>
        /// <param name="sampleCountOfEachChannel"></param>       
        /// <param name="collectId">第几次采集：用于比对是否为新的采集</param>       
        internal virtual void Repaint(int channel, int channelIndexInChunk, double sampleRate, int sampleCountOfEachChannel, int collectId)
        {
            //
            _sampleRate = sampleRate;

            //
            if (!_isVisibleToUser)
            {
                _visibleForPaint = false;
                return;
            }

            if (clean(channel)) return;





            //
            if (channelIndexInChunk < 0)
            {
                var yratio = _height / (_maxY - _minY);
                _zeroY = _height + yratio * _minY;
                _thresholdY = _height - yratio * (_threshold - _minY);
                _pts = new PointF[0];
                _channelForPaint = channel;
                _sweepLineX = 0;
            }
            else
            {
                if (channel == Channel)
                {
                    LoadPaintParams();

                    //
                    if (_widthForPaint > 0 && _heightForPaint > 0)
                    {
                        //
                        var yratio = _heightForPaint / (_maxYForPaint - _minYForPaint);
                        var samplesInWindow = Math.Max(2, Convert.ToInt32(sampleRate / 1000.0 * _displayWindowMSForPaint));
                        _zeroY = _heightForPaint + yratio * _minYForPaint;
                        _thresholdY = _heightForPaint - yratio * (_threshold - _minYForPaint);

                        //
                        var begin = channelIndexInChunk * sampleCountOfEachChannel;
                        var end = (channelIndexInChunk + 1) * sampleCountOfEachChannel - 1;

                        //显示有变化，则重新加载所有数据
                        if (_widthForPaintChanged || _heightForPaintChanged || _displayWindowMSForPaintChanged || _maxYForPaintChanged || _minYForPaintChanged || _sweepOrRollForPaintChanged || _visibleForPaintChanged || _channelForPaintChanged || _rollOffsetTimeMsForPaintChanged || _collectId != collectId)
                        {
                            _collectId = collectId;
                            ReloadAllChunk(yratio, samplesInWindow, begin, end);
                        }
                        else
                        {
                            LoadNewestChunk(yratio, samplesInWindow, begin, end);
                        }
                    }
                }
            }

            //
            if (_pts != null && _pts.Length > 0)
            {
                if (_pts.Any(r => r.Y > short.MaxValue || r.Y < short.MinValue || float.IsInfinity(r.Y)))
                {
                    Logger.LogTool.Logger.LogT($"{nameof(PaintDataToBitmap)}, some data is too large or is infinity");
                }
            }

            //将数据坐标点绘制出来
            PaintDataToBitmap();

            //
            UIHelper.BeginInvoke(() =>
            {
                if (_bindControl != null)
                {
                    _bindControl.SweepX = _sweepLineX;
                    _bindControl.InvalidateVisual();
                }
            });
        }



        #region 加载数据

        /// <summary>
        /// 加载新来的数据块
        /// </summary>
        /// <param name="yratio"></param>
        /// <param name="samplesInWindow"></param>
        private void LoadNewestChunk(float yratio, int samplesInWindow, int begin, int end)
        {
            var newestChunk = GetNewestChunk();
            if (newestChunk == null)
            {
                return;
            }

            //视窗时间太短，样本数小于控件宽度
            if (samplesInWindow <= _widthForPaint)
            {
                //波形绘制点数为样本个数
                var ptCount = samplesInWindow;

                //加载数据
                LoadDataOneByOne(yratio, samplesInWindow, ptCount, new Chunk[] { newestChunk }, begin, end);
            }
            else //视窗时间太长，样本数大于控件宽度，降采样绘制
            {
                //波形绘制点数为2倍控件宽度
                var ptCount = _widthForPaint * 2;

                //加载数据
                LoadDataWithDownSampling(yratio, samplesInWindow, ptCount, new Chunk[] { newestChunk }, begin, end);
            }
        }

        /// <summary>
        /// 重新加载整个波形图
        /// </summary>
        /// <param name="yratio"></param>
        /// <param name="samplesInWindow"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        private void ReloadAllChunk(float yratio, int samplesInWindow, int begin, int end)
        {
            //
            var bufferedChunks = GetBufferedChunks();

            //视窗时间太短，样本数小于控件宽度
            if (samplesInWindow <= _widthForPaint)
            {
                //波形绘制点数为样本个数
                var ptCount = samplesInWindow;

                //初始化所有点位
                _pts = new PointF[ptCount];
                var xstep = 1.0f * _widthForPaint / (ptCount - 1);
                var defaultY = this.GetDefaultY();
                for (int i = 0; i < ptCount; i++)
                {
                    _pts[i] = new PointF { X = i * xstep, Y = defaultY }; //提前分配X
                }

                //加载数据
                LoadDataOneByOne(yratio, samplesInWindow, ptCount, bufferedChunks, begin, end);
            }
            else //视窗时间太长，样本数大于控件宽度，降采样绘制
            {
                //波形绘制点数为2倍控件宽度
                var ptCount = _widthForPaint * 2;

                //初始化所有点位
                _pts = new PointF[ptCount];
                var xstep = 1.0f * _widthForPaint / (ptCount - 1);
                var defaultY = this.GetDefaultY();
                for (int i = 0; i < ptCount; i++)
                {
                    _pts[i] = new PointF { X = i * xstep, Y = defaultY }; //提前分配X
                }

                //加载数据
                LoadDataWithDownSampling(yratio, samplesInWindow, ptCount, bufferedChunks, begin, end);
            }
        }

        /// <summary>
        /// 时间比较长，视窗样本点比较多，降采样绘制
        /// </summary>
        /// <param name="yratio"></param>
        /// <param name="samplesInWindow"></param>
        /// <param name="ptCount"></param>
        /// <param name="chunkArray"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        private void LoadDataWithDownSampling(float yratio, int samplesInWindow, int ptCount, Chunk[] chunkArray, int begin, int end)
        {
            if (chunkArray.Length <= 0)
            {
                return;
            }

            //计算最新的数据应在波形图中显示的位置:两点为一对，分别绘制最大值和最小值
            var newestSampleIndex = (chunkArray[^1].TotalRecvdSampleOfEachChannel - 1);

            //扫屏模式 or 滚屏模式
            if (_sweepOrRollForPaint)
            {
                //后面面临取余，所以此处 添加一个周期 也无所谓，但可节省【后续为防止取余小于0的处理】
                newestSampleIndex += samplesInWindow;

                //每2个像素对应多少个float数据;(max + min) = pt pair
                var floatPer2Pt = samplesInWindow * 2.0 / ptCount;

                //
                var ptPairIndex = (int)Math.Floor(newestSampleIndex % samplesInWindow / floatPer2Pt);
                _sweepLineX = _pts[ptPairIndex * 2].X; //扫描线所处位置

                //
                var loadSampleCount = 0;
                var maxVal = float.MinValue;
                var minVal = float.MaxValue;

                //遍历已经缓存的数据，加载数据
                for (int chunkIndex = chunkArray.Length - 1; chunkIndex >= 0; chunkIndex--)
                {
                    var floats = GetFloatsArray(chunkArray[chunkIndex]);
                    if (floats == null)
                    {
                        continue;
                    }

                    //从最新的数据开始加载
                    for (int sampleIndex = end; sampleIndex >= begin; sampleIndex--)
                    {
                        //样本点对应的绘制点位
                        var newPtPairIndex = (int)Math.Floor((newestSampleIndex - loadSampleCount) % samplesInWindow / floatPer2Pt);

                        //切换了点位，则将上组数据保存
                        if (newPtPairIndex != ptPairIndex)
                        {
                            _pts[ptPairIndex * 2].Y = _heightForPaint - yratio * (maxVal - _minYForPaint);
                            _pts[ptPairIndex * 2 + 1].Y = _heightForPaint - yratio * (minVal - _minYForPaint);

                            //
                            ptPairIndex = newPtPairIndex;
                            maxVal = float.MinValue;
                            minVal = float.MaxValue;
                        }

                        //
                        var val = floats[sampleIndex];
                        if (val > maxVal)
                        {
                            maxVal = val;
                        }
                        if (val < minVal)
                        {
                            minVal = val;
                        }

                        //计数
                        loadSampleCount++;

                        //只需加载能够显示的样本个数即可
                        if (loadSampleCount >= samplesInWindow)
                        {
                            break;
                        }
                    }

                    //只需加载能够显示的样本个数即可
                    if (loadSampleCount >= samplesInWindow)
                    {
                        break;
                    }
                }

                //
                if (maxVal != float.MinValue && minVal != float.MaxValue)
                {
                    _pts[ptPairIndex * 2].Y = _heightForPaint - yratio * (maxVal - _minYForPaint);
                    _pts[ptPairIndex * 2 + 1].Y = _heightForPaint - yratio * (minVal - _minYForPaint);
                }
            }
            else
            {
                var ptPairCount = ptCount / 2;
                _sweepLineX = 0; //扫描线所处位置

                //每2个像素对应多少个float数据;(max + min) = pt pair
                var floatPer2Pt = 2.0 * samplesInWindow / ptCount;

                //当前最左边像素的第一个数据对应的样本点顺序
                var first2PtSampleIndex = (Math.Floor(newestSampleIndex / floatPer2Pt) - ptPairCount) * floatPer2Pt;
                var ptPairIndex = (int)Math.Floor((newestSampleIndex - first2PtSampleIndex) % samplesInWindow / floatPer2Pt);

                //
                if (chunkArray.Length == 1 && _totalRecvd != chunkArray[^1].TotalRecvdSampleOfEachChannel)
                {
                    var ptPairIndexForFirstData = (int)(((newestSampleIndex - chunkArray[^1].OneChannelSamples + 1 - first2PtSampleIndex) % samplesInWindow + samplesInWindow) % samplesInWindow / floatPer2Pt);
                    if (ptPairIndexForFirstData > 0)
                    {
                        var offset = (ptPairCount - ptPairIndexForFirstData) * 2;
                        for (int i = 0; i < ptPairIndexForFirstData * 2; i++)
                        {
                            _pts[i].Y = _pts[offset + i].Y;
                        }
                    }
                }

                //
                _totalRecvd = chunkArray[^1].TotalRecvdSampleOfEachChannel;

                //最新的数据相当于在当前视野中的顺序：肯定落在最后一个像素点上，但不一定为samplesInWindow
                //后面面临取余，所以此处 添加一个周期 也无所谓，但可节省【后续为防止取余小于0的处理】
                var newestSampleIndexInView = newestSampleIndex - first2PtSampleIndex + samplesInWindow * 10;

                //
                var loadSampleCount = -_rollOffsetTimeMsForPaint * _sampleRate / 1000; //roll模式下的偏移用于回放
                var maxVal = float.MinValue;
                var minVal = float.MaxValue;

                //遍历已经缓存的数据，加载数据
                for (int chunkIndex = chunkArray.Length - 1; chunkIndex >= 0; chunkIndex--)
                {
                    var floats = GetFloatsArray(chunkArray[chunkIndex]);
                    if (floats == null)
                    {
                        continue;
                    }

                    //从最新的数据开始加载
                    for (int sampleIndex = end; sampleIndex >= begin; sampleIndex--)
                    {
                        //roll模式下的偏移用于回放
                        if (loadSampleCount < 0)
                        {
                            loadSampleCount++;
                            continue;
                        }

                        //样本点对应的绘制点位
                        var newPtPairIndex = (int)((newestSampleIndexInView - loadSampleCount) % samplesInWindow / floatPer2Pt);

                        //切换了点位，则将上组数据保存
                        if (newPtPairIndex != ptPairIndex)
                        {
                            _pts[ptPairIndex * 2].Y = _heightForPaint - yratio * (maxVal - _minYForPaint);
                            _pts[ptPairIndex * 2 + 1].Y = _heightForPaint - yratio * (minVal - _minYForPaint);

                            //
                            ptPairIndex = (newPtPairIndex + ptPairCount) % ptPairCount;
                            maxVal = float.MinValue;
                            minVal = float.MaxValue;
                        }

                        //计算最大值最小值
                        var val = floats[sampleIndex];
                        if (val > maxVal)
                        {
                            maxVal = val;
                        }
                        if (val < minVal)
                        {
                            minVal = val;
                        }

                        //计数
                        loadSampleCount++;

                        //只需加载能够显示的样本个数即可
                        if (loadSampleCount >= samplesInWindow)
                        {
                            break;
                        }
                    }

                    //只需加载能够显示的样本个数即可
                    if (loadSampleCount >= samplesInWindow)
                    {
                        break;
                    }
                }

                //
                if (maxVal != float.MinValue && minVal != float.MaxValue)
                {
                    _pts[ptPairIndex * 2].Y = _heightForPaint - yratio * (maxVal - _minYForPaint);
                    _pts[ptPairIndex * 2 + 1].Y = _heightForPaint - yratio * (minVal - _minYForPaint);
                }
            }
        }

        /// <summary>
        /// 时间比较短，样本点稀疏，直接取样本点绘图
        /// </summary>
        /// <param name="yratio"></param>
        /// <param name="samplesInWindow"></param>
        /// <param name="ptCount"></param>
        /// <param name="chunkArray"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        private void LoadDataOneByOne(float yratio, int samplesInWindow, int ptCount, Chunk[] chunkArray, int begin, int end)
        {
            //计算最新的数据应在波形图中显示的位置
            var ptIndex = 0;

            //扫屏模式 or 滚屏模式
            if (_sweepOrRollForPaint)
            {
                ptIndex = (int)(chunkArray[^1].TotalRecvdSampleOfEachChannel + ptCount - 1) % ptCount; //例如：收到了1个样本点，则最新的点肯定在位置0（最左边）
                _sweepLineX = _pts[ptIndex].X; //扫描线所处位置
            }
            else
            {
                ptIndex = ptCount - 1; //滚屏时，最新数据一直在波形图最右边
                _sweepLineX = 0; //扫描线所处位置
            }



            //遍历已经缓存的数据，加载数据
            var loadSampleCount = _sweepOrRollForPaint ? 0 : (-_rollOffsetTimeMsForPaint * _sampleRate / 1000); //roll模式下的偏移用于回放
            for (int chunkIndex = chunkArray.Length - 1; chunkIndex >= 0; chunkIndex--)
            {
                var floats = GetFloatsArray(chunkArray[chunkIndex]);
                if (floats == null)
                {
                    continue;
                }

                //从最新的数据开始加载
                for (int sampleIndex = end; sampleIndex >= begin; sampleIndex--)
                {
                    //roll模式下的偏移用于回放
                    if (loadSampleCount < 0)
                    {
                        loadSampleCount++;
                        continue;
                    }

                    //
                    ptIndex = (ptIndex + ptCount) % ptCount;

                    //计算坐标
                    _pts[ptIndex].Y = _heightForPaint - yratio * (floats[sampleIndex] - _minYForPaint);

                    //计数、移动X位置
                    ptIndex--;
                    loadSampleCount++;

                    //只需加载能够显示的样本个数即可
                    if (loadSampleCount >= samplesInWindow)
                    {
                        break;
                    }
                }

                //只需加载能够显示的样本个数即可
                if (loadSampleCount >= samplesInWindow)
                {
                    break;
                }
            }
        }

        #endregion


        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        protected abstract float[] GetFloatsArray(Chunk chunk);

        /// <summary>
        /// 获取buffered 的数据块
        /// </summary>
        /// <returns></returns>
        protected abstract Chunk[] GetBufferedChunks();

        /// <summary>
        /// 获取最新的数据块
        /// </summary>
        /// <returns></returns>
        protected abstract Chunk GetNewestChunk();

        /// <summary>
        /// 将数据坐标点绘制出来
        /// </summary>
        protected virtual void PaintDataToBitmap()
        {
            base.PaintToShadow(BackgroundColor, OriginPt, _width, _height, true, (g) =>
            {
                PaintZeroLine(g);
                PaintThresholdLine(g);

                //
                if (_pts != null && _pts.Length > 0)
                {
                    if (!_pts.Any(r => r.Y > short.MaxValue || r.Y < short.MinValue || float.IsInfinity(r.Y)))
                    {
                        g.DrawLines(LineDrawingPen, _pts);
                    }
                }
            });
        }

        /// <summary>
        /// 绘制零值水平线
        /// </summary>
        /// <param name="g"></param>
        protected void PaintZeroLine(Graphics g)
        {
            if (ShowZeroLine)
            {
                g.DrawLine(Pens.DimGray, new PointF(0, _zeroY), new PointF(_width, _zeroY));
            }
        }

        /// <summary>
        /// 绘制阈值水平线
        /// </summary>
        /// <param name="g"></param>
        protected void PaintThresholdLine(Graphics g)
        {
            if (ShowThresholdLine)
            {
                g.DrawLine(Pens.Blue, new PointF(0, _thresholdY), new PointF(_width, _thresholdY));
            }
        }
    }
}
