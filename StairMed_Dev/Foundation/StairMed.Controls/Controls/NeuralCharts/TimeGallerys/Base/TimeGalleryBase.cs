﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using System;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TimeGalleryBase : NeuralChartWithContainer
    {
        /// <summary>
        /// 电压或时间刻度尺的头尾宽度
        /// </summary>
        public const int SCALE_BAR_GAP = TEXT_MARGIN * 2;

        /// <summary>
        /// 圆角矩形的圆角
        /// </summary>
        public const int ROUND_RECT_CORNER = 3;

        /// <summary>
        /// 阻抗
        /// </summary>
        const int M = 1000 * 1000;
        const int K = 1000;

        /// <summary>
        /// 
        /// </summary>
        static TimeGalleryBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeGalleryBase), new FrameworkPropertyMetadata(typeof(TimeGalleryBase)));
        }

        #region 属性

        /// <summary>
        /// 显示的时间轴长度
        /// </summary>
        public int DisplayWindowMS
        {
            get { return (int)GetValue(DisplayWindowMSProperty); }
            set { SetValue(DisplayWindowMSProperty, value); }
        }
        public static readonly DependencyProperty DisplayWindowMSProperty = DependencyProperty.Register(nameof(DisplayWindowMS), typeof(int), typeof(TimeGalleryBase), new PropertyMetadata(20000, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 显示的时间轴长度
        /// </summary>
        public double ImpedanceMagnitude
        {
            get { return (double)GetValue(ImpedanceMagnitudeProperty); }
            set { SetValue(ImpedanceMagnitudeProperty, value); }
        }
        public static readonly DependencyProperty ImpedanceMagnitudeProperty = DependencyProperty.Register(nameof(ImpedanceMagnitude), typeof(double), typeof(TimeGalleryBase), new PropertyMetadata(0.0, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Roll模式下水平偏移
        /// </summary>
        public int RollOffsetTimeMs
        {
            get { return (int)GetValue(RollOffsetTimeMsProperty); }
            set { SetValue(RollOffsetTimeMsProperty, value); }
        }
        public static readonly DependencyProperty RollOffsetTimeMsProperty = DependencyProperty.Register(nameof(RollOffsetTimeMs), typeof(int), typeof(TimeGalleryBase), new PropertyMetadata(20000, new PropertyChangedCallback(PropertyChanged)));



        public bool ShowChannelname
        {
            get { return (bool)GetValue(ShowChannelnameProperty); }
            set { SetValue(ShowChannelnameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowChannelname.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowChannelnameProperty =
            DependencyProperty.Register("ShowChannelname", typeof(bool), typeof(TimeGalleryBase), new PropertyMetadata(true,new PropertyChangedCallback(PropertyChanged)));

                
        public bool ShowImpedance
        {
            get { return (bool)GetValue(ShowImpedanceProperty); }
            set { SetValue(ShowImpedanceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowImpedance.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowImpedanceProperty =
            DependencyProperty.Register("ShowImpedance", typeof(bool), typeof(TimeGalleryBase), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));



        #endregion

        /// <summary>
        /// 监听属性变化
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {

            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(DisplayWindowMSProperty) || e.Property.Equals(DrawAreaWidthProperty))
            {
                //
                UpdateXRatio();

                //
                DelayReRender(true);
                return;
            }

            //
            if (e.Property.Equals(ImpedanceMagnitudeProperty))
            {
                UpdateChannelName();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(ShowChannelnameProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(ShowImpedanceProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(RollOffsetTimeMsProperty))
            {
                DelayReRender();
                return;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateXRatio()
        {
            if (DisplayWindowMS > 0 && DrawAreaWidth > 0)
            {
                _xratio = DrawAreaWidth / DisplayWindowMS;
            }
        }

        /// <summary>
        /// 绘制边框
        /// </summary>
        /// <param name="ctx"></param>
        protected void PaintBorder(DrawingContext ctx)
        {
            ctx.DrawRoundedRectangle(null, _linePen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight), ROUND_RECT_CORNER, ROUND_RECT_CORNER);
        }

        /// <summary>
        /// 绘制通道名称
        /// </summary>
        /// <param name="ctx"></param>
        protected void PaintChannelNameOnMidLeft(DrawingContext ctx)
        {
            try
            {
                var channelNameTxt_width = _channelNameTxt == null ? 0 : _channelNameTxt.Width;
                var channelNameTxt_height = _channelNameTxt == null ? 0 : _channelNameTxt.Height;

                var impedanceMagnitudeTxt_width = _impedanceMagnitudeTxt == null ? 0 : _impedanceMagnitudeTxt.Width;
                var impedanceMagnitudeTxt_height = _impedanceMagnitudeTxt == null ? 0 : _impedanceMagnitudeTxt.Height;

                var rectWidth = Math.Max(channelNameTxt_width, impedanceMagnitudeTxt_width);
                var rectHeight = Math.Max(channelNameTxt_height, impedanceMagnitudeTxt_height);


                if (ShowChannelname && ShowImpedance)
                {
                    var rect1 = new Rect
                    {
                        X = OriginPoint.X - Padding.Left,
                        Y = YAxisMaxPoint.Y + (DrawAreaHeight - rectHeight * 2) / 2,
                        Width = rectWidth + TEXT_MARGIN * 2,
                        Height = rectHeight,
                    };
                    ctx.DrawRoundedRectangle(Brushes.Black, _linePen, rect1, ROUND_RECT_CORNER, ROUND_RECT_CORNER);
                    ctx.DrawText(_channelNameTxt, new Point(rect1.X + TEXT_MARGIN, rect1.Y ));

                    var rect2 = rect1;
                    rect2.Y += rectHeight + TEXT_MARGIN;

                    ctx.DrawRoundedRectangle(Brushes.Black, _linePen, rect2, ROUND_RECT_CORNER, ROUND_RECT_CORNER);
                    ctx.DrawText(_impedanceMagnitudeTxt, new Point(rect2.X + TEXT_MARGIN, rect2.Y ));
                }
                else if (ShowChannelname || ShowImpedance)
                {
                    FormattedText showTxt = null;
                    double txtWidth = 0;
                    if (ShowChannelname && _channelNameTxt != null)
                    {
                        showTxt = _channelNameTxt;
                        txtWidth = _channelNameTxt.Width;
                    }
                    if (ShowImpedance && _impedanceMagnitudeTxt != null)
                    {
                        showTxt = _impedanceMagnitudeTxt;
                        txtWidth = _impedanceMagnitudeTxt.Width;
                    }
                    if (showTxt != null)
                    {
                        var rect3 = new Rect
                        {
                            X = OriginPoint.X - Padding.Left,
                            Y = YAxisMaxPoint.Y + (DrawAreaHeight - rectHeight) / 2,
                            Width = txtWidth + TEXT_MARGIN * 2,
                            Height = rectHeight,
                        };
                        ctx.DrawRoundedRectangle(Brushes.Black, _linePen, rect3, ROUND_RECT_CORNER, ROUND_RECT_CORNER);
                        ctx.DrawText(showTxt, new Point(rect3.X + TEXT_MARGIN, rect3.Y));
                    }
                }
            }
            catch
            { 
            
            }
        }

        /// <summary>
        /// 更新通道名称：附带上阻抗信息
        /// </summary>
        protected override void UpdateChannelName()
        {
            _channelNameTxt = this.CreateFormatText($"{ChannelName}");
            _impedanceMagnitudeTxt= this.CreateFormatText($"{GetImpedanceMagnitude()}");
        }
        public FormattedText _impedanceMagnitudeTxt = null;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetImpedanceMagnitude()
        {
            //if (ImpedanceMagnitude <= 0)
            //{
            //    return string.Empty;
            //}

            //
            try
            {
                var ohms = ImpedanceMagnitude;
                if (ohms >= M)
                {
                    return $"{ohms / M:F2}MΩ";
                }
                if (ohms >= K)
                {
                    return $"{ohms / K:F2}kΩ";
                }
                return $"{ohms:F2}Ω";
            }
            catch { }
            return string.Empty;
        }
    }
}
