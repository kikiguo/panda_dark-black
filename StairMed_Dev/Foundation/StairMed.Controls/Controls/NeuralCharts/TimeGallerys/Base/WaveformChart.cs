﻿using StairMed.Core.Consts;
using System;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class WaveformChart : TimeGalleryBase
    {
        /// <summary>
        /// 
        /// </summary>
        static WaveformChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(WaveformChart), new FrameworkPropertyMetadata(typeof(WaveformChart)));
        }

        #region 属性

        /// <summary>
        /// 阈值
        /// </summary>
        public double Threshold
        {
            get { return (double)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register(nameof(Threshold), typeof(double), typeof(WaveformChart), new PropertyMetadata(-70d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否展示阈值线
        /// </summary>
        public bool ShowThresholdLine
        {
            get { return (bool)GetValue(ShowThresholdLineProperty); }
            set { SetValue(ShowThresholdLineProperty, value); }
        }
        public static readonly DependencyProperty ShowThresholdLineProperty = DependencyProperty.Register(nameof(ShowThresholdLine), typeof(bool), typeof(WaveformChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否展示水平零值线
        /// </summary>
        public bool ShowZeroLine
        {
            get { return (bool)GetValue(ShowZeroLineProperty); }
            set { SetValue(ShowZeroLineProperty, value); }
        }
        public static readonly DependencyProperty ShowZeroLineProperty = DependencyProperty.Register(nameof(ShowZeroLine), typeof(bool), typeof(WaveformChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 绘制模式：true:sweep, false:roll
        /// </summary>
        public bool SweepOrRoll
        {
            get { return (bool)GetValue(SweepOrRollProperty); }
            set { SetValue(SweepOrRollProperty, value); }
        }
        public static readonly DependencyProperty SweepOrRollProperty = DependencyProperty.Register(nameof(SweepOrRoll), typeof(bool), typeof(WaveformChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// sweep模式下的x
        /// </summary>
        public float SweepX
        {
            get { return (float)GetValue(SweepXProperty); }
            set { SetValue(SweepXProperty, value); }
        }
        public static readonly DependencyProperty SweepXProperty = DependencyProperty.Register(nameof(SweepX), typeof(float), typeof(WaveformChart), new PropertyMetadata(-1f, new PropertyChangedCallback(PropertyChanged)));

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(ThresholdProperty) || e.Property.Equals(ShowThresholdLineProperty) || e.Property.Equals(ShowZeroLineProperty) || e.Property.Equals(SweepOrRollProperty))
            {
                DelayReRender();
            }

            //
            if (_painter == null)
            {
                return;
            }

            //
            if (e.Property.Equals(SweepXProperty))
            {
                return;
            }
            else if (e.Property.Equals(IsVisibleToUserProperty))
            {
                _painter.IsVisibleToUser = IsVisibleToUser;
            }
            else if (e.Property.Equals(DrawAreaWidthProperty))
            {
                _painter.Width = (int)DrawAreaWidth;
            }
            else if (e.Property.Equals(DrawAreaHeightProperty))
            {
                _painter.Height = DrawAreaHeight;
            }
            else if (e.Property.Equals(OriginPointProperty))
            {
                _painter.OriginPt = OriginPoint;
            }
            else if (e.Property.Equals(ChannelProperty))
            {
                _painter.Channel = Channel;
            }
            else if (e.Property.Equals(BackgroundProperty))
            {
                _painter.BackgroundColor = ((SolidColorBrush)Background).Color.ToColor();
            }
            else if (e.Property.Equals(MaxYProperty))
            {
                _painter.MaxY = MaxY;
            }
            else if (e.Property.Equals(MinYProperty))
            {
                _painter.MinY = MinY;
            }
            else if (e.Property.Equals(SweepOrRollProperty))
            {
                _painter.SweepOrRoll = SweepOrRoll;
            }
            else if (e.Property.Equals(RollOffsetTimeMsProperty))
            {
                _painter.RollOffsetTimeMs = RollOffsetTimeMs;
            }
            else if (e.Property.Equals(ShowThresholdLineProperty))
            {
                _painter.ShowThresholdLine = ShowThresholdLine;
            }
            else if (e.Property.Equals(ThresholdProperty))
            {
                _painter.Threshold = (float)Threshold;
            }
            else if (e.Property.Equals(ShowZeroLineProperty))
            {
                _painter.ShowZeroLine = ShowZeroLine;
            }
            else if (e.Property.Equals(DisplayWindowMSProperty))
            {
                _painter.DisplayWindowMS = DisplayWindowMS;
            }
            else if (e.Property.Equals(LineBrushProperty))
            {
                _painter.LineDrawingPen = new System.Drawing.Pen(((SolidColorBrush)LineBrush).Color.ToColor(), LINE_WIDTH); ;
            }
        }


        /// <summary>
        /// 绘图工具
        /// </summary>
        private readonly WaveformPainter _painter = null;

        /// <summary>
        /// 
        /// </summary>
        public WaveformChart()
        {
            _painter = GetPainter();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected abstract WaveformPainter GetPainter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            _painter.CopyFromShadow(ctx, OriginPoint, DrawAreaWidth, DrawAreaHeight);
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void PaintHover(DrawingContext ctx)
        {
            //绘制边框
            PaintBorder(ctx);

            //绘制垂直线：电压
            {
                //
                var x1 = MouseHoverX - SCALE_BAR_GAP;
                var x2 = this.MakeLineThin(x1 - SCALE_BAR_GAP);
                var x3 = x2 - SCALE_BAR_GAP;
                var y1 = this.MakeLineThin(OriginPoint.Y - DrawAreaHeight / 2);
                var y2 = this.MakeLineThin(MouseHoverY);
                ctx.DrawLine(_scaleBarPen, new Point(x1, y1), new Point(x3, y1));
                ctx.DrawLine(_scaleBarPen, new Point(x1, y2), new Point(x3, y2));
                ctx.DrawLine(_scaleBarPen, new Point(x2, y1), new Point(x2, y2));

                //
                var uv = Convert.ToInt32(Math.Abs((y1 - y2) / _yratio));
                var txt = this.CreateFormatText($"{uv}μV");
                ctx.DrawText(txt, new Point(x2 - txt.Width / 2, y2 > y1 ? y1 - txt.Height : y1));

                //
                ctx.DrawText(txt, new Point(XAxisMaxPoint.X - txt.Width - TEXT_MARGIN, YAxisMaxPoint.Y));
            }

            //绘制水平线：时间
            if (DisplayWindowMS > 0)
            {
                int xScaleBar = CONST.DisplayWindowScaleBarDict[DisplayWindowMS];

                //
                var w = xScaleBar * _xratio;
                var x1 = this.MakeLineThin(MouseHoverX - SCALE_BAR_GAP * 2);
                var x2 = this.MakeLineThin(x1 - w);
                var y1 = this.MakeLineThin(MouseHoverY - SCALE_BAR_GAP);
                var y2 = this.MakeLineThin(MouseHoverY);
                var y3 = this.MakeLineThin(MouseHoverY + SCALE_BAR_GAP);
                ctx.DrawLine(_scaleBarPen, new Point(x1, y1), new Point(x1, y3));
                ctx.DrawLine(_scaleBarPen, new Point(x2, y1), new Point(x2, y3));
                ctx.DrawLine(_scaleBarPen, new Point(x1, y2), new Point(x2, y2));

                //
                var txt = this.CreateFormatText($"{xScaleBar}ms");
                ctx.DrawText(txt, new Point((x2 + x1 - txt.Width) / 2, y2 - txt.Height));
            }
        }

        /// <summary>
        /// 绘制标题
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            PaintChannelNameOnMidLeft(ctx);
        }
    }
}
