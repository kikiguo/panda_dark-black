﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using StairMed.Entity.DataPools;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.LFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class LFPPainter : WaveformPainter
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public LFPPainter(WaveformChart bindControl) : base(bindControl)
        {

        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        protected override void RegisterPainter()
        {
            LFPPainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        protected override void UnregisterPainter()
        {
            LFPPainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 默认Y值
        /// </summary>
        /// <returns></returns>
        protected override float GetDefaultY()
        {
            return Height / 2;
        }


        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        protected override float[] GetFloatsArray(Chunk chunk)
        {
            if (chunk.Lfps == null)
            {
                return null;
            }
            return chunk.Lfps.Array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk[] GetBufferedChunks()
        {
            return LFPPainterCenter.Instance.BufferedChunk.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Chunk GetNewestChunk()
        {
            if (LFPPainterCenter.Instance.BufferedChunk.Count <= 0)
            {
                return null;
            }

            //
            return LFPPainterCenter.Instance.BufferedChunk.ToArray()[^1];
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ForceRepaint()
        {
            LFPPainterCenter.Instance.ForceRepaint();
        }
    }
}
