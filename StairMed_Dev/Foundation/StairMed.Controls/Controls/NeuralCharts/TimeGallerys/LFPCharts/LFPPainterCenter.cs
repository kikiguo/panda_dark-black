﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.LFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class LFPPainterCenter : WaveformPainterCenter
    {
        #region 单例

        private static readonly LFPPainterCenter _instance = new LFPPainterCenter();
        public static LFPPainterCenter Instance => _instance;
        private LFPPainterCenter()
        {

        }

        #endregion
    }
}
