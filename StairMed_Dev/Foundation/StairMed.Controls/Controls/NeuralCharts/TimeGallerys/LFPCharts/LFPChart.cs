﻿using StairMed.Controls.Controls.NeuralCharts.TimeGallerys.Base;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.TimeGallerys.LFPCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class LFPChart : WaveformChart
    {
        /// <summary>
        /// 
        /// </summary>
        static LFPChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(LFPChart), new FrameworkPropertyMetadata(typeof(LFPChart)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override WaveformPainter GetPainter()
        {
            return new LFPPainter(this);
        }
    }
}