﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using System.Windows;

namespace StairMed.Controls.Controls.NeuralCharts.PSTHs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PSTHBase : NeuralChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        static PSTHBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PSTHBase), new FrameworkPropertyMetadata(typeof(PSTHBase)));
        }

        #region 数据

        /// <summary>
        /// trigger前监听时长
        /// </summary>
        public int PreTriggerSpanMS
        {
            get { return (int)GetValue(PreTriggerSpanMSProperty); }
            set { SetValue(PreTriggerSpanMSProperty, value); }
        }
        public static readonly DependencyProperty PreTriggerSpanMSProperty = DependencyProperty.Register(nameof(PreTriggerSpanMS), typeof(int), typeof(PSTHBase), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// trigger后监听时长
        /// </summary>
        public int PostTriggerSpanMS
        {
            get { return (int)GetValue(PostTriggerSpanMSProperty); }
            set { SetValue(PostTriggerSpanMSProperty, value); }
        }
        public static readonly DependencyProperty PostTriggerSpanMSProperty = DependencyProperty.Register(nameof(PostTriggerSpanMS), typeof(int), typeof(PSTHBase), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        #region 属性变化监听

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(DrawAreaWidthProperty) || e.Property.Equals(PreTriggerSpanMSProperty) || e.Property.Equals(PostTriggerSpanMSProperty))
            {
                UpdateXRatio();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(MouseHoverXProperty))
            {
                DelayReRender();
                return;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateXRatio()
        {
            if (PreTriggerSpanMS + PostTriggerSpanMS > 0)
            {
                _xratio = DrawAreaWidth / (PreTriggerSpanMS + PostTriggerSpanMS);
            }
        }

        #endregion
    }
}
