﻿using StairMed.Core.Consts;
using StairMed.DataCenter.Centers.PSTH;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.PSTHs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PSTHChart : PSTHBase
    {
        /// <summary>
        /// 
        /// </summary>
        static PSTHChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PSTHChart), new FrameworkPropertyMetadata(typeof(PSTHChart)));
        }

        #region 数据

        /// <summary>
        /// 轮次
        /// </summary>
        public int Trials
        {
            get { return (int)GetValue(TrialsProperty); }
            set { SetValue(TrialsProperty, value); }
        }
        public static readonly DependencyProperty TrialsProperty = DependencyProperty.Register(nameof(Trials), typeof(int), typeof(PSTHChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 轮次数据
        /// </summary>
        public ObservableCollection<PSTHRaster> Rasters
        {
            get { return (ObservableCollection<PSTHRaster>)GetValue(RastersProperty); }
            set { SetValue(RastersProperty, value); }
        }
        public static readonly DependencyProperty RastersProperty = DependencyProperty.Register(nameof(Rasters), typeof(ObservableCollection<PSTHRaster>), typeof(PSTHChart), new PropertyMetadata(new ObservableCollection<PSTHRaster>(), new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        public int SampleRate
        {
            get { return (int)GetValue(SampleRateProperty); }
            set { SetValue(SampleRateProperty, value); }
        }
        public static readonly DependencyProperty SampleRateProperty = DependencyProperty.Register(nameof(SampleRate), typeof(int), typeof(PSTHChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Mark线
        /// </summary>
        public int MarkLineMS
        {
            get { return (int)GetValue(MarkLineMSProperty); }
            set { SetValue(MarkLineMSProperty, value); }
        }
        public static readonly DependencyProperty MarkLineMSProperty = DependencyProperty.Register(nameof(MarkLineMS), typeof(int), typeof(PSTHChart), new PropertyMetadata(int.MinValue, new PropertyChangedCallback(PropertyChanged)));

        #endregion


        #region 属性变化监听

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(DrawAreaHeightProperty) || e.Property.Equals(TrialsProperty))
            {
                UpdateYRatio();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(SampleRateProperty) || e.Property.Equals(MarkLineMSProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(RastersProperty))
            {
                if (e.OldValue != null && e.OldValue is ObservableCollection<PSTHRaster> oldRasters)
                {
                    oldRasters.CollectionChanged -= PSTHChart_CollectionChanged;
                }

                //
                if (e.NewValue != null && e.NewValue is ObservableCollection<PSTHRaster> newRasters)
                {
                    newRasters.CollectionChanged += PSTHChart_CollectionChanged;
                }

                //
                InvalidateVisual();

                //
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PSTHChart_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(() =>
            {
                this.InvalidateVisual();
            }));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateYRatio()
        {
            if (Trials > 0)
            {
                _yratio = DrawAreaHeight / Trials;
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private readonly PSTHPainter _painter = new PSTHPainter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            var xratio = 1.0 * DrawAreaWidth / (SampleRate * (PreTriggerSpanMS + PostTriggerSpanMS) / 1000);
            _painter.PaintPSTH(ctx, _bgColor, OriginPoint, DrawAreaWidth, DrawAreaHeight, xratio, _yratio, Rasters);
        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //pre 与 post 分割线
            {
                var x = this.MakeLineThin(OriginPoint.X + MarkLineMS * _xratio);
                ctx.DrawLine(_redPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            }

            //绘制y轴:横线
            {
                //
                var rowStep = CONST.PSTHTrialStepDict[Trials];
                for (int i = 1; i <= Trials; i++)
                {
                    var y = this.MakeLineThin(OriginPoint.Y - DrawAreaHeight + i * _yratio);

                    //
                    if (i % rowStep == 0)
                    {
                        ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TICK_LINE_LENGTH, y));
                        var txt = this.CreateFormatText($"{i}");
                        ctx.DrawText(txt, new Point(OriginPoint.X - TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
                    }
                }
            }

            //绘制大方框
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));

            //
            if (MouseHoverX > 0 && MouseHoverX < ActualWidth)
            {
                var x = this.MakeLineThin(MouseHoverX);
                ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            }
        }

        /// <summary>
        /// 绘制标题等信息
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            //通道
            PaintChannelNameOnOuterTopLeft(ctx);

            //
            PaintTitleOnLeft(ctx, "trials");

            //Trials
            {
                var txt = this.CreateFormatText($"{(Rasters != null ? Rasters.Count : 0)} trials");
                ctx.DrawText(txt, new Point(XAxisMaxPoint.X - 3 * TEXT_MARGIN - txt.Width, YAxisMaxPoint.Y - TEXT_MARGIN - txt.Height));
            }
        }

        /// <summary>
        /// 鼠标hover
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintHover(DrawingContext ctx)
        {
            var y = this.MakeLineThin(MouseHoverY);
            ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
        }
    }
}
