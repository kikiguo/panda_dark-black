﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.PSTHs
{
    /// <summary>
    /// 
    /// </summary>
    internal class PSTHHistogramPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual void PaintPSTHHistogram(DrawingContext ctx, System.Drawing.Color bgcolor, System.Drawing.Brush solidBrush, System.Windows.Point origin, float width, float height, float xpixelStep, List<float> ys)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //
            PaintToShadow(bgcolor, origin, width, height, true, (g) =>
            {
                if (ys.Count <= width)
                {
                    double x = 0;
                    for (int i = 0; i < ys.Count; i++)
                    {
                        if (ys[i] != origin.Y)
                        {
                            g.FillRectangle(solidBrush, Convert.ToSingle(x), Convert.ToSingle(ys[i]), xpixelStep, Convert.ToSingle(origin.Y - ys[i]));
                        }
                        x += xpixelStep;
                    }
                }
                else
                {
                    var dataStep = 1 / xpixelStep;
                    for (int i = 0; i < width; i++)
                    {
                        var index = (int)Math.Floor(i * dataStep);
                        var y = ys[index];
                        if (y != origin.Y)
                        {
                            g.DrawLine(new System.Drawing.Pen(solidBrush, 1), i, (int)y, i, (int)origin.Y);
                        }
                    }
                }
            }, true);

            //
            CopyFromShadow(ctx, origin, width, height);
        }
    }
}
