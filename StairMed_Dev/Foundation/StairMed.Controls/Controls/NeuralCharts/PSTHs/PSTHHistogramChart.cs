﻿using StairMed.Core.Consts;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.PSTHs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PSTHHistogramChart : PSTHBase
    {
        /// <summary>
        /// 
        /// </summary>
        static PSTHHistogramChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PSTHHistogramChart), new FrameworkPropertyMetadata(typeof(PSTHHistogramChart)));
        }

        /// <summary>
        /// bin size
        /// </summary>
        public int BinSizeMS
        {
            get { return (int)GetValue(BinSizeMSProperty); }
            set { SetValue(BinSizeMSProperty, value); }
        }
        public static readonly DependencyProperty BinSizeMSProperty = DependencyProperty.Register(nameof(BinSizeMS), typeof(int), typeof(PSTHHistogramChart), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Mark线
        /// </summary>
        public int MarkLineMS
        {
            get { return (int)GetValue(MarkLineMSProperty); }
            set { SetValue(MarkLineMSProperty, value); }
        }
        public static readonly DependencyProperty MarkLineMSProperty = DependencyProperty.Register(nameof(MarkLineMS), typeof(int), typeof(PSTHHistogramChart), new PropertyMetadata(int.MinValue, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        public List<float> Histograms
        {
            get { return (List<float>)GetValue(HistogramsProperty); }
            set { SetValue(HistogramsProperty, value); }
        }
        public static readonly DependencyProperty HistogramsProperty = DependencyProperty.Register(nameof(Histograms), typeof(List<float>), typeof(PSTHBase), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(HistogramsProperty))
            {
                InvalidateVisual();
                return;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private readonly PSTHHistogramPainter _painter = new PSTHHistogramPainter();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            //Y轴最大值
            var maxHist = 0.0f;
            var step = 0.0f;
            var n = 10;
            if (Histograms != null && Histograms.Count > 0)
            {
                maxHist = Histograms.Max();
                if (maxHist == 0 || maxHist.Equals(double.NaN) || maxHist.Equals(double.NegativeInfinity) || maxHist.Equals(double.PositiveInfinity) || maxHist <= 0)
                {
                    return;
                }
                ToolMix.CoordinateAxisEqualDivide(maxHist, out step, out _, out n, 4);
                maxHist = step * n;
            }
            var format = step > 1 ? "F0" : "F1";

            //绘制波形
            if (Histograms != null && BinSizeMS > 0 && Histograms.Count == (PreTriggerSpanMS + PostTriggerSpanMS) / BinSizeMS)
            {
                if (maxHist > 0)
                {
                    var xpixelStep = DrawAreaWidth / Histograms.Count;
                    var ypixelStep = DrawAreaHeight / maxHist;

                    //
                    var ys = new List<float>(Histograms.Count);
                    foreach (var val in Histograms)
                    {
                        ys.Add(DrawAreaHeight - ypixelStep * val);
                    }

                    //
                    _painter.PaintPSTHHistogram(ctx, _bgColor, ControlConst.HistDrawingBrush, OriginPoint, DrawAreaWidth, DrawAreaHeight, xpixelStep, ys);
                }
            }

            //绘制Y刻度
            for (int i = 1; i <= n; i++)
            {
                var val = i * step;
                var y = this.MakeLineThin(OriginPoint.Y - val / maxHist * DrawAreaHeight);
                var txt = this.CreateFormatText($"{(i * step).ToString(format)}");
                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X - TICK_LINE_LENGTH, y), new Point(OriginPoint.X, y));
                ctx.DrawText(txt, new Point(OriginPoint.X - TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //
            {
                var x = this.MakeLineThin(OriginPoint.X + MarkLineMS * _xratio);
                ctx.DrawLine(_redPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            }

            //X轴刻度
            {
                var xtickStepMS = Math.Max(CONST.PSTHPostTriggerStepDict[PostTriggerSpanMS], CONST.PSTHPreTriggerStepDict[PreTriggerSpanMS]);
                var ms = -50 * xtickStepMS;
                while (ms <= PostTriggerSpanMS)
                {
                    if (ms < -PreTriggerSpanMS)
                    {
                        ms += xtickStepMS;
                        continue;
                    }

                    //
                    var x = OriginPoint.X + _xratio * (ms + PreTriggerSpanMS);
                    var txt = this.CreateFormatText((ms >= 1000 || ms <= -1000 || ms == 0) ? $"{ms / 1000.0}s" : $"{ms}ms");
                    ctx.DrawLine(_foregroundPen, new Point(x, OriginPoint.Y), new Point(x, OriginPoint.Y + TICK_LINE_LENGTH));
                    ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + TICK_LINE_LENGTH + TEXT_MARGIN));
                    ms += xtickStepMS;
                }
            }

            //绘制大方框
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));

            //
            if (MouseHoverX > 0 && MouseHoverX < ActualWidth)
            {
                var x = this.MakeLineThin(MouseHoverX);
                ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            PaintTitleOnLeft(ctx, "spikes/s");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintHover(DrawingContext ctx)
        {
            var y = this.MakeLineThin(MouseHoverY);
            ctx.DrawLine(_yellowPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
        }

    }
}
