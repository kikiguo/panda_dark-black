﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using StairMed.DataCenter.Centers.PSTH;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows.Media;


namespace StairMed.Controls.Controls.NeuralCharts.PSTHs
{
    /// <summary>
    /// 
    /// </summary>
    internal class PSTHPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly List<long> _painteredRasters = new List<long>();
        private readonly List<int> _rasterYs = new List<int>();

        /// <summary>
        /// 
        /// </summary>
        private double _lastXRatio = 0;
        private double _lastYRatio = 0;

        /// <summary>
        /// 
        /// </summary>
        public virtual void PaintPSTH(DrawingContext ctx, System.Drawing.Color bgcolor, System.Windows.Point origin, float width, float height, double xratio, double yratio, ObservableCollection<PSTHRaster> rasters)
        {
            if (rasters == null || rasters.Count <= 0)
            {
                return;
            }

            //
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //
            if (xratio != _lastXRatio || yratio != _lastYRatio || !_painteredRasters.Contains(rasters[0].RasterIndex))
            {
                _painteredRasters.Clear();
                _rasterYs.Clear();

                //
                PaintToShadow(bgcolor, origin, width, height, true, (g) =>
                {
                    for (int rowIndex = 0; rowIndex < rasters.Count; rowIndex++)
                    {
                        var y = (int)(rowIndex * yratio);

                        //
                        var raster = rasters[rowIndex];
                        foreach (var index in raster.Raster)
                        {
                            var x = (int)(index * xratio);
                            g.DrawLine(Pens.White, x, y, x, (int)(y + Math.Max(1, yratio - 1)));
                        }

                        //
                        _painteredRasters.Add(raster.RasterIndex);
                        _rasterYs.Add(y);
                    }
                }, true);
            }
            else
            {
                //分三种：
                //1.正常raster依次增加到来，raster个数从0....n，未达到最大个数
                //2.raster已经满了，新增一个，去掉一个旧的
                //3.清空最后一行
                //以上场景中，基本都可以保证raster的第一个肯定在上次绘制的列表中

                //偏移
                var overlapStart = _painteredRasters.IndexOf(rasters[0].RasterIndex);

                //重叠部分，重叠部分必定在raster的最前面
                var overlapCount = 0;
                for (int i = 0; i < rasters.Count; i++)
                {
                    if (overlapStart + i >= _painteredRasters.Count)
                    {
                        break;
                    }

                    if (_painteredRasters[overlapStart + i] == rasters[i].RasterIndex)
                    {
                        overlapCount++;
                    }
                    else
                    {
                        break;
                    }
                }

                //
                PaintToShadow(bgcolor, origin, width, height, false, (g) =>
                {
                    //从上次绘制的哪个Y位置开始复制
                    var overlapYOffset = _rasterYs[overlapStart];

                    //复制多高的图像
                    var height = (int)(overlapCount * yratio);

                    //执行复制
                    var srcRect = new Rectangle(0, overlapYOffset, bmpW, height);
                    var dstRect = new Rectangle(0, 0, srcRect.Width, srcRect.Height);
                    g.DrawImage(_shadowCopy, dstRect, srcRect, GraphicsUnit.Pixel);

                    //将剩余可绘制区域填背景色：处理【清空最后一行】的情况
                    g.FillRectangle(new SolidBrush(bgcolor), new Rectangle(0, dstRect.Height, dstRect.Width, bmpH - dstRect.Height));

                    //去除上次的绘图中被丢弃的部分
                    for (int i = 0; i < overlapStart; i++)
                    {
                        _rasterYs.RemoveAt(0);
                        _painteredRasters.RemoveAt(0);
                    }
                    while (_painteredRasters.Count > overlapCount)
                    {
                        _rasterYs.RemoveAt(_rasterYs.Count - 1);
                        _painteredRasters.RemoveAt(_painteredRasters.Count - 1);
                    }

                    //上次绘制的raster坐标移动
                    for (int i = 0; i < _rasterYs.Count; i++)
                    {
                        _rasterYs[i] -= overlapYOffset;
                    }

                    //绘制新数据
                    for (int rowIndex = overlapCount; rowIndex < rasters.Count; rowIndex++)
                    {
                        var y = (int)(rowIndex * yratio);

                        //
                        var raster = rasters[rowIndex];
                        foreach (var index in raster.Raster)
                        {
                            var x = (int)(index * xratio);
                            g.DrawLine(Pens.White, x, y, x, (int)(y + Math.Max(1, yratio - 1)));
                        }

                        //
                        _painteredRasters.Add(raster.RasterIndex);
                        _rasterYs.Add(y);
                    }
                }, true);
            }

            //
            _lastYRatio = yratio;
            _lastXRatio = xratio;

            //
            CopyFromShadow(ctx, origin, width, height);
        }
    }
}
