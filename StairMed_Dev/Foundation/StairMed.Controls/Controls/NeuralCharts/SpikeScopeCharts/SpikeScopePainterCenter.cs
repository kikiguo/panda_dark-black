﻿using StairMed.AsyncFIFO;
using StairMed.DataCenter.Centers.SpikeScope;
using System.Collections.Generic;
using System.Timers;

namespace StairMed.Controls.Controls.NeuralCharts.SpikeScopeCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeScopePainterCenter
    {
        #region 单例

        private static readonly SpikeScopePainterCenter _instance = new SpikeScopePainterCenter();
        public static SpikeScopePainterCenter Instance => _instance;
        private SpikeScopePainterCenter()
        {
            _fifo = new BlockFIFO { Name = nameof(SpikeScopePainterCenter), ConsumerAction = Consume };
            _forcePaintTimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                lock (_channels)
                {
                    foreach (var channel in _channels)
                    {
                        _fifo.Add(_collectId, channel, null);
                    }
                }
            };
            SpikeScopeCenter.Instance.RecvdChannelSpikeScopes += (int collectId, int channel, int sampleRate, long recvd, List<SpikeScopeInfo> spikeScopes) =>
            {
                _collectId = collectId;
                if (spikeScopes.Count > 0)
                {
                    _fifo.Add(collectId, channel, spikeScopes);
                }
            };
        }

        #endregion

        /// <summary>
        /// 重绘定时器
        /// </summary>
        private readonly Timer _forcePaintTimer = new Timer { Interval = 100, AutoReset = false, };

        /// <summary>
        /// 用于判断是否为同一次采集
        /// </summary>
        private int _collectId = -1;

        /// <summary>
        /// 所有通道的painter
        /// </summary>
        private readonly Dictionary<int, HashSet<SpikeScopePainter>> _painterDict = new Dictionary<int, HashSet<SpikeScopePainter>>();

        /// <summary>
        /// 监听的所有通道
        /// </summary>
        private readonly HashSet<int> _channels = new HashSet<int>();

        /// <summary>
        /// 异步调用
        /// </summary>
        private readonly IAsyncFIFO _fifo;

        /// <summary>
        /// 处理绘制通知消息
        /// </summary>
        /// <param name="objs"></param>
        private void Consume(object[] objs)
        {
            if (objs != null && objs.Length == 3 && objs[0] is bool isRegister)
            {
                HandlePainterRegisterEvent(isRegister, (int)objs[1], (SpikeScopePainter)objs[2]);
                return;
            }

            //
            if (objs != null && objs.Length == 3 && objs[0] is int collectId)
            {
                var channel = (int)objs[1];
                var spikeScopes = (List<SpikeScopeInfo>)objs[2];

                //
                if (_painterDict.ContainsKey(channel))
                {
                    foreach (var painter in _painterDict[channel])
                    {
                        painter.Repaint(collectId, channel, spikeScopes);
                    }
                }
            }
        }


        #region 注册 painter

        /// <summary>
        /// 通道注册或取消注册
        /// </summary>
        /// <param name="isregister"></param>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        private void HandlePainterRegisterEvent(bool isregister, int inputChannel, SpikeScopePainter painter)
        {
            if (isregister)
            {
                if (!_painterDict.ContainsKey(inputChannel))
                {
                    _painterDict[inputChannel] = new HashSet<SpikeScopePainter>();
                }
                _painterDict[inputChannel].Add(painter);
                if (_painterDict[inputChannel].Count == 1)
                {
                    SpikeScopeCenter.Instance.RegisterSpikeScope(inputChannel);
                    lock (_channels)
                    {
                        _channels.Add(inputChannel);
                    }
                }
            }
            else
            {
                if (_painterDict.ContainsKey(inputChannel))
                {
                    _painterDict[inputChannel].Remove(painter);
                    if (_painterDict[inputChannel].Count == 0)
                    {
                        SpikeScopeCenter.Instance.UnregisterSpikeScope(inputChannel);
                        lock (_channels)
                        {
                            _channels.Remove(inputChannel);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        internal void RegisterPainter(int inputChannel, SpikeScopePainter painter)
        {
            _fifo.Add(true, inputChannel, painter); //通过在fifo线程注册，避免多线程访问，避免频繁lock
        }

        /// <summary>
        /// 取消注册
        /// </summary>
        /// <param name="inputChannel"></param>
        /// <param name="painter"></param>
        internal void UnregisterPainter(int inputChannel, SpikeScopePainter painter)
        {
            _fifo.Add(false, inputChannel, painter); //通过在fifo线程注册，避免多线程访问，避免频繁lock
        }

        #endregion


        /// <summary>
        /// 强制重绘
        /// </summary>
        internal void ForceRepaint()
        {
            _forcePaintTimer.Stop();
            _forcePaintTimer.Interval = 50;
            _forcePaintTimer.Start();
        }

    }
}


