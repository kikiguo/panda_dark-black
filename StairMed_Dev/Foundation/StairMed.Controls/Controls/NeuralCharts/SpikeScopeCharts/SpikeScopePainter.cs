﻿using HDF5CSharp;
using StairMed.Controls.Controls.NeuralCharts.Painters;
using StairMed.Controls.Logger;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace StairMed.Controls.Controls.NeuralCharts.SpikeScopeCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeScopePainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        private class SpikeScopeDrawing
        {
            public SpikeScopeInfo SpikeScopeInfo;
            public PointF[] Points;
            public Pen Pen;
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly Pen _snapshotPen = new Pen(Color.FromArgb(0x2d, 0x52, 0xa0), Base.NeuralChartBase.LINE_WIDTH);
        private readonly Pen _artifactPen = new Pen(Color.FromArgb(0xff, 0x00, 0x00), Base.NeuralChartBase.LINE_WIDTH);

        /// <summary>
        /// 是否可见
        /// </summary>
        private bool _isVisibleToUser = false;
        public bool IsVisibleToUser
        {
            set
            {
                if (_isVisibleToUser != value)
                {
                    _isVisibleToUser = value;
                    if (value)
                    {
                        RegisterPainter();
                    }
                    else
                    {
                        UnregisterPainter();
                    }
                }
            }
            get
            {
                return _isVisibleToUser;
            }
        }

        /// <summary>
        /// 高度
        /// </summary>
        private float _height = 1.0f;
        public float Height
        {
            get { return _height; }
            set
            {
                _height = value;
                ForceRepaint();
            }
        }

        /// <summary>
        /// 宽度
        /// </summary>
        private float _width = 1.0f;
        public float Width
        {
            get { return _width; }
            set
            {
                _width = value;
                ForceRepaint();
            }
        }

        /// <summary>
        /// 电压幅值
        /// </summary>
        private int _uvScale = 1;
        public int UVScale
        {
            get { return _uvScale; }
            set
            {
                _uvScale = value;
                ForceRepaint();
            }
        }

        /// <summary>
        /// 是否快照
        /// </summary>
        private bool _takeSnapshot = false;
        public bool TakeSnapshot
        {
            get { return _takeSnapshot; }
            set
            {
                if (!_takeSnapshot && value)
                {
                    _takeSnapshot = value;
                }
                ForceRepaint();
            }
        }

        /// <summary>
        /// 是否清理快照
        /// </summary>
        private bool _clearSnapshot = false;
        public bool ClearSnapshot
        {
            get { return _clearSnapshot; }
            set
            {
                if (!_clearSnapshot && value)
                {
                    _clearSnapshot = value;
                }
                ForceRepaint();
            }
        }

        /// <summary>
        /// 清空所有
        /// </summary>
        private bool _clearScope = false;
        public bool ClearScope
        {
            get { return _clearScope; }
            set
            {
                if (!_clearScope && value)
                {
                    _clearScope = value;
                }
                ForceRepaint();
            }
        }

        /// <summary>
        /// 绑定的通道
        /// </summary>
        private int _channel = -1;
        public int Channel
        {
            get { return _channel; }
            set
            {
                if (_channel != value)
                {
                    UnregisterPainter();
                    _channel = value;
                    ClearScope = true;
                    ClearSnapshot = true;
                    if (_isVisibleToUser)
                    {
                        RegisterPainter();
                    }
                    ForceRepaint();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _spikeScopeReserved = 50;
        public int SpikeScopeReserved
        {
            get
            { return _spikeScopeReserved; }
            set
            {
                if (_spikeScopeReserved != value)
                {
                    _spikeScopeReserved = value;
                    ForceRepaint();
                }
            }
        }
        private bool _isSorted = false;

        public bool IsSorted
        {
            get { return _isSorted; }
            set
            {
                if (_isSorted != value)
                {
                    _isSorted = value;
                    ForceRepaint();
                }
            }
        }
        public Pen LineDrawingPen = new Pen(Color.FromArgb(0xff, 0xff, 0xff, 0xff), Base.NeuralChartBase.LINE_WIDTH);
        public Pen barBackgroundPen = new Pen(Color.FromArgb(0xff, 0x3c, 0x3c, 0x3c), Base.NeuralChartBase.LINE_WIDTH);
        public int SortedIndex = -1;
        int freqBarWidth = 6;
        int freqBarHeight = 44;
        int freqBarMargin = 5;
        int freqBarValueWidth = 4;
        Rectangle rectFreqBar = new Rectangle(0, 0, 0, 0);
        /// <summary>
        /// 显示抑制波形
        /// </summary>
        public bool ShowArtifacts { get; set; } = false;

        //
        public System.Windows.Point OriginPt = new System.Windows.Point();
        public Color BackgroundColor = Color.Aqua;
        public bool LazyPaint = true;

        /// <summary>
        /// 对应的Control
        /// </summary>
        private readonly Control _bindControl;

        /// <summary>
        /// 构造函数
        /// </summary>
        public SpikeScopePainter(Control bindControl)
        {
            _bindControl = bindControl;
        }

        /// <summary>
        /// 来数据时绘制
        /// </summary>
        private void RegisterPainter()
        {
            SpikeScopePainterCenter.Instance.RegisterPainter(Channel, this);
        }

        /// <summary>
        /// 来数据时不绘制
        /// </summary>
        private void UnregisterPainter()
        {
            SpikeScopePainterCenter.Instance.UnregisterPainter(Channel, this);
        }

        /// <summary>
        /// 
        /// </summary>
        private float _heightForPaint = 1.0f;
        private bool _heightForPaintChanged = true;

        /// <summary>
        /// 
        /// </summary>
        private float _widthForPaint = 1;
        private bool _widthForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private int _uvScaleForPaint = 1;
        private bool _uvScaleForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private bool _visibleForPaint = false;
        private bool _visibleForPaintChanged = false;

        /// <summary>
        /// 
        /// </summary>
        private int _channelForPaint = 0;
        private bool _channelForPaintChanged = false;

        /// <summary>
        /// 对当前设置项快照一下，并记录是否有调整
        /// </summary>
        private void LoadPaintParams()
        {
            _heightForPaintChanged = _heightForPaint != Height;
            if (_heightForPaintChanged)
            {
                _heightForPaint = Height;
                rectFreqBar = new Rectangle(freqBarMargin, Convert.ToInt32(_heightForPaint - freqBarHeight - freqBarMargin), freqBarWidth, freqBarHeight);
            }

            //
            _widthForPaintChanged = _widthForPaint != Width;
            if (_widthForPaintChanged)
            {
                _widthForPaint = Width;
            }

            //
            _uvScaleForPaintChanged = _uvScaleForPaint != UVScale;
            if (_uvScaleForPaintChanged)
            {
                _uvScaleForPaint = UVScale;
            }

            //
            _visibleForPaintChanged = _visibleForPaint != IsVisibleToUser;
            if (_visibleForPaintChanged)
            {
                _visibleForPaint = IsVisibleToUser;
            }

            //
            _channelForPaintChanged = _channelForPaint != Channel;
            if (_channelForPaintChanged)
            {
                _channelForPaint = Channel;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly List<SpikeScopeDrawing> _snapshotScopes = new List<SpikeScopeDrawing>(); //快照波形
        private readonly List<SpikeScopeDrawing> _frontScopes = new List<SpikeScopeDrawing>(); //与快照相对的非快照波形
        private int _spikeScopeLen = 0; //每个spikescope的长度
        private int _collectId = -1; //用于判断是否为同一次采集

        /// <summary>
        /// spike scope数据更新
        /// </summary>
        /// <param name="collectId"></param>
        /// <param name="spikeScopes"></param>
        internal void Repaint(int collectId, int channel, List<SpikeScopeInfo> spikeScopes)
        {
            try
            {
                if (LazyPaint) //不清空缓存
                {
                    DoLazyPaint(collectId, channel, spikeScopes);
                }
                else //单独弹窗
                {
                    List<SpikeScopeInfo> newSpa = new List<SpikeScopeInfo>();
                    if(spikeScopes != null)
                    foreach (var spikeScope in spikeScopes)
                    {
                        if (spikeScope != null)
                        {
                           List<float> newsp=spikeScope.Hfps.Array.Select(r => r *(float)StairMedSettings.Instance.SpikeScopeGain).ToList();
                            for(int i = 0;i< spikeScope.Hfps.Length; i++)
                                {
                                    spikeScope.Hfps[i] =newsp[i];
                                }


                            newSpa.Add(spikeScope);
                        }
                    }
                    IndependentPaint(collectId, channel, newSpa);
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{nameof(SpikeScopePainter)}", ex);
            }
        }

        /// <summary>
        /// 不擦除黑板，在历史波形上追加绘制
        /// </summary>
        /// <param name="collectId"></param>
        /// <param name="channel"></param>
        /// <param name="spikeScopes"></param>
        private void DoLazyPaint(int collectId, int channel, List<SpikeScopeInfo> spikeScopes)
        {
            if (clean(channel)) return;
            if (channel == Channel)
            {
                //是否清空历史，使用新的黑板：叠加绘制时，会留存上次绘制的痕迹
                var newBoard = false;

                //需要新绘制多少条spike scope
                var paintScpoeCount = 0;

                //新的一次采集或新的时间跨度，则清空
                if (_collectId != collectId || (spikeScopes != null && spikeScopes.Count > 0 && _spikeScopeLen != spikeScopes[0].Hfps.Length))
                {
                    _frontScopes.Clear();
                    newBoard = true;
                }

                //
                _collectId = collectId;
                if (spikeScopes != null && spikeScopes.Count > 0)
                {
                    _spikeScopeLen = spikeScopes[0].Hfps.Length;
                }

                //
                LoadPaintParams();

                //
                if (_channelForPaintChanged)
                {
                    _frontScopes.Clear();
                    newBoard = true;
                }

                //
                var yratio = _heightForPaint / _uvScaleForPaint / 2;
                var xratio = _widthForPaint / (_spikeScopeLen > 1 ? (_spikeScopeLen - 1) : _spikeScopeLen);

                //高宽有变化
                if (_heightForPaintChanged || _widthForPaintChanged || _uvScaleForPaintChanged || _visibleForPaintChanged)
                {
                    //清空大部分，留存小部分
                    if (_frontScopes.Count > StairMedSettings.Instance.SpikeScopeReserved)
                    {
                        _frontScopes.RemoveRange(0, _frontScopes.Count - StairMedSettings.Instance.SpikeScopeReserved);
                    }

                    //更新绘制点坐标
                    for (int scopeIndex = 0; scopeIndex < _frontScopes.Count; scopeIndex++)
                    {
                        var spikeScope = _frontScopes[scopeIndex];

                        //
                        var x = 0f;
                        for (int ptIndex = 0; ptIndex < _spikeScopeLen; ptIndex++)
                        {
                            spikeScope.Points[ptIndex].X = x;
                            spikeScope.Points[ptIndex].Y = _heightForPaint - yratio * (spikeScope.SpikeScopeInfo.Hfps.Array[ptIndex] + _uvScaleForPaint);
                            x += xratio;
                        }
                    }

                    //
                    paintScpoeCount = _frontScopes.Count;

                    //擦除黑板
                    newBoard = true;
                }

                //添加新来的spikescope
                if (spikeScopes != null && spikeScopes.Count > 0)
                {
                    for (int scopeIndex = 0; scopeIndex < spikeScopes.Count; scopeIndex++)
                    {
                        var spikeScope = spikeScopes[scopeIndex];

                        //波形溢出则不添加进来
                        if (!ShowArtifacts && spikeScope.IsOverflow)
                        {
                            continue;
                        }

                        //
                        var pts = new PointF[_spikeScopeLen];
                        var x = 0f;
                        for (int ptIndex = 0; ptIndex < _spikeScopeLen; ptIndex++)
                        {
                            pts[ptIndex].X = x;
                            pts[ptIndex].Y = _heightForPaint - yratio * (spikeScope.Hfps.Array[ptIndex] + _uvScaleForPaint);
                            x += xratio;
                        }
#if true
                        if (IsSorted)
                        {
                            if (SortedIndex == spikeScope.Label)
                            {
                                paintScpoeCount++;
                                _frontScopes.Add(new SpikeScopeDrawing
                                {
                                    SpikeScopeInfo = spikeScope,
                                    Points = pts,
                                    Pen = spikeScope.IsOverflow ? _artifactPen : SpikeScopeColorHelper.GetPen(spikeScope.Label),
                                });
                            }
                        }
                        else
                        {
                            paintScpoeCount++;
                            _frontScopes.Add(new SpikeScopeDrawing
                            {
                                SpikeScopeInfo = spikeScope,
                                Points = pts,
                                Pen = spikeScope.IsOverflow ? _artifactPen : SpikeScopeColorHelper.GetPen(spikeScope.Label),
                            });
                        }
#else
                        //
                        paintScpoeCount++;
                        _frontScopes.Add(new SpikeScopeDrawing
                        {
                            SpikeScopeInfo = spikeScope,
                            Points = pts,
                            Pen = spikeScope.IsOverflow ? _artifactPen : SpikeScopeColorHelper.GetPen(spikeScope.Label),
                        });
#endif
                    }
                }

                        //限制个数
                        const int OverlyingMax = 600; //保留多少个不用擦除背景
                if (_frontScopes.Count > StairMedSettings.Instance.SpikeScopeReserved)
                {
                    newBoard = true;
                    //_frontScopes.RemoveRange(0, _frontScopes.Count - OverlyingMax);
                    if (_frontScopes.Count > StairMedSettings.Instance.SpikeScopeReserved)
                    {
                        _frontScopes.RemoveRange(0, _frontScopes.Count - StairMedSettings.Instance.SpikeScopeReserved);
                    }
                    paintScpoeCount = _frontScopes.Count;
                }

                //绘制波形
                PaintToShadow(BackgroundColor, OriginPt, _widthForPaint, _heightForPaint, newBoard, (g) =>
                {
                    var startIndex = Math.Max(0, _frontScopes.Count - paintScpoeCount);
                    for (var scopeIndex = startIndex; scopeIndex < _frontScopes.Count; scopeIndex++)
                    {
                        var spikeScope = _frontScopes[scopeIndex];
                        g.DrawLines(spikeScope.Pen, spikeScope.Points);
                    }
                    if (IsSorted && _frontScopes.Count > 0)
                    {
                        DrawFrequencyBar(g, barBackgroundPen.Brush, rectFreqBar);
                        double dischargeFrequency = _frontScopes[0].SpikeScopeInfo.DischargeFrequency;
                        int freqBarValueHeight = Convert.ToInt32(Math.Floor(dischargeFrequency / 100.0 * freqBarHeight));
                        Rectangle rectFreqValueBar = new Rectangle(freqBarMargin + 1, Convert.ToInt32(_heightForPaint - freqBarValueHeight - freqBarMargin), freqBarValueWidth, freqBarValueHeight);
                        DrawFrequencyBar(g, _frontScopes[0].Pen.Brush, rectFreqValueBar);
                    }
                });

                //
                UIHelper.BeginInvoke(() =>
                {
                    if (_bindControl != null)
                    {
                        _bindControl?.InvalidateVisual();
                    }
                });
            }
        }
        private void DrawFrequencyBar(Graphics g, Brush brush, Rectangle rectBar)
        {
            g.FillRectangle(brush, rectBar);
            g.FillEllipse(brush, new Rectangle(rectBar.X, (rectBar.Y - rectBar.Width / 2), rectBar.Width - 1, rectBar.Width - 1));
            g.FillEllipse(brush, new Rectangle(rectBar.X, (rectBar.Y + rectBar.Height - rectBar.Width / 2), rectBar.Width - 1, rectBar.Width - 1));
        }
        private bool clean(int channel)
        {
            if (StairMedSettings.Instance.SpikeScopeEnable&& StairMedSettings.Instance.SpikeScopeChannel == channel)
            {
                PaintToShadow(BackgroundColor, OriginPt, _widthForPaint, _heightForPaint, true, (g) =>
                {

                });
                UIHelper.BeginInvoke(() =>
                {
                    if (_bindControl != null)
                    {
                        _bindControl?.InvalidateVisual();
                    }
                });

                return true;
            }

            if (StairMedSettings.Instance.IsClean)
            {
                DateTime now = DateTime.Now;
                DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                long timestamp = (long)(now.ToUniversalTime() - epoch).TotalMilliseconds;


                if (StairMedSettings.Instance.cleanTiem < timestamp)
                {

                    StairMedSettings.Instance.IsClean = false;

                }
                // _snapshotScopes.Clear();
                // _frontScopes.Clear();
                var clean = true;
                PaintToShadow(BackgroundColor, OriginPt, _widthForPaint, _heightForPaint, clean, (g) =>
                {

                });
                UIHelper.BeginInvoke(() =>
                {
                    if (_bindControl != null)
                    {
                        _bindControl?.InvalidateVisual();
                    }
                });

                return true;

            }
            return false;
        }

        /// <summary>
        /// 单独窗口的绘图
        /// </summary>
        /// <param name="collectId"></param>
        /// <param name="channel"></param>
        /// <param name="spikeScopes"></param>
        private void IndependentPaint(int collectId, int channel, List<SpikeScopeInfo> spikeScopes)
        {
            if (clean(channel)) return;
            if (channel == Channel)
            {
                //新的一次采集或新的时间跨度，则清空
                if (_collectId != collectId || (spikeScopes != null && spikeScopes.Count > 0 && _spikeScopeLen != spikeScopes[0].Hfps.Length))
                {
                    _snapshotScopes.Clear();
                    _frontScopes.Clear();
                }

                //
                if (ClearScope)
                {
                    _clearScope = false;
                    _frontScopes.Clear();
                }
                else if (ClearSnapshot)
                {
                    _clearSnapshot = false;
                    _snapshotScopes.Clear();
                }
                else if (TakeSnapshot)
                {
                    _takeSnapshot = false;
                    _snapshotScopes.Clear();
                    _snapshotScopes.AddRange(_frontScopes);
                    for (int scopeIndex = 0; scopeIndex < _snapshotScopes.Count; scopeIndex++)
                    {
                        _snapshotScopes[scopeIndex].Pen = _snapshotPen;
                    }
                }

                //
                _collectId = collectId;
                if (spikeScopes != null && spikeScopes.Count > 0)
                {
                    _spikeScopeLen = spikeScopes[0].Hfps.Length;
                }

                //
                LoadPaintParams();

                //
                if (_channelForPaintChanged)
                {
                    _frontScopes.Clear();
                    _snapshotScopes.Clear();
                }

                //
                var yratio = _heightForPaint / _uvScaleForPaint / 2;
                var xratio = _widthForPaint / (_spikeScopeLen > 1 ? (_spikeScopeLen - 1) : _spikeScopeLen);

                //高宽有变化，需要更新坐标点
                if (_heightForPaintChanged || _widthForPaintChanged || _uvScaleForPaintChanged || _visibleForPaintChanged)
                {
                    //更新历史snapshot的坐标点位置
                    for (int scopeIndex = 0; scopeIndex < _snapshotScopes.Count; scopeIndex++)
                    {
                        var spikeScope = _snapshotScopes[scopeIndex];

                        //
                        var x = 0f;
                        for (int ptIndex = 0; ptIndex < _spikeScopeLen; ptIndex++)
                        {
                            spikeScope.Points[ptIndex].X = x;
                            spikeScope.Points[ptIndex].Y = _heightForPaint - yratio * (spikeScope.SpikeScopeInfo.Hfps.Array[ptIndex] + _uvScaleForPaint);
                            x += xratio;
                        }
                    }

                    //更新非snapshot的坐标点位置
                    for (int scopeIndex = 0; scopeIndex < _frontScopes.Count; scopeIndex++)
                    {
                        var spikeScope = _frontScopes[scopeIndex];

                        //
                        var x = 0f;
                        for (int ptIndex = 0; ptIndex < _spikeScopeLen; ptIndex++)
                        {
                            spikeScope.Points[ptIndex].X = x;
                            spikeScope.Points[ptIndex].Y = _heightForPaint - yratio * (spikeScope.SpikeScopeInfo.Hfps.Array[ptIndex] + _uvScaleForPaint);
                            x += xratio;
                        }
                    }
                }

                //添加新来的spikescope
                if (spikeScopes != null && spikeScopes.Count > 0)
                {
                    for (int scopeIndex = 0; scopeIndex < spikeScopes.Count; scopeIndex++)
                    {
                        var spikeScope = spikeScopes[scopeIndex];

                        //波形溢出则不添加进来
                        if (!ShowArtifacts && spikeScope.IsOverflow)
                        {
                            continue;
                        }

                        //
                        var pts = new PointF[_spikeScopeLen];
                        var x = 0f;
                        for (int ptIndex = 0; ptIndex < _spikeScopeLen; ptIndex++)
                        {
                            pts[ptIndex].X = x;
                            pts[ptIndex].Y = _heightForPaint - yratio * (spikeScope.Hfps.Array[ptIndex] + _uvScaleForPaint);
                            x += xratio;
                        }
#if true
                        if (IsSorted)
                        {
                            if (SortedIndex == spikeScope.Label)
                            {
                                _frontScopes.Add(new SpikeScopeDrawing
                                {
                                    SpikeScopeInfo = spikeScope,
                                    Points = pts,
                                    Pen = spikeScope.IsOverflow ? _artifactPen : SpikeScopeColorHelper.GetPen(spikeScope.Label),
                                });
                            }
                        }
                        else
                        {
                            _frontScopes.Add(new SpikeScopeDrawing
                            {
                                SpikeScopeInfo = spikeScope,
                                Points = pts,
                                Pen = spikeScope.IsOverflow ? _artifactPen : SpikeScopeColorHelper.GetPen(spikeScope.Label),
                            });
                        }
#else
                        //
                        _frontScopes.Add(new SpikeScopeDrawing
                        {
                            SpikeScopeInfo = spikeScope,
                            Points = pts,
                            Pen = spikeScope.IsOverflow ? _artifactPen : SpikeScopeColorHelper.GetPen(spikeScope.Label),
                        });
#endif
                    }
                }

                        //限制个数
                        if (_frontScopes.Count > StairMedSettings.Instance.SpikeScopeReserved)
                {
                    _frontScopes.RemoveRange(0, _frontScopes.Count - StairMedSettings.Instance.SpikeScopeReserved);
                }
            }

            //绘制波形
            PaintToShadow(BackgroundColor, OriginPt, _widthForPaint, _heightForPaint, true, (g) =>
            {
                //
                foreach (var spikeScope in _snapshotScopes)
                {
                    g.DrawLines(spikeScope.Pen, spikeScope.Points);
                }

                //
                foreach (var spikeScope in _frontScopes)
                {
                    g.DrawLines(spikeScope.Pen, spikeScope.Points);
                }
                if (IsSorted && _frontScopes.Count > 0)
                {
                    DrawFrequencyBar(g, barBackgroundPen.Brush, rectFreqBar);
                    double dischargeFrequency = _frontScopes[0].SpikeScopeInfo.DischargeFrequency;
                    int freqBarValueHeight = Convert.ToInt32(Math.Floor(dischargeFrequency / 100.0 * freqBarHeight));
                    Rectangle rectFreqValueBar = new Rectangle(freqBarMargin + 1, Convert.ToInt32(_heightForPaint - freqBarValueHeight - freqBarMargin), freqBarValueWidth, freqBarValueHeight);
                    DrawFrequencyBar(g, _frontScopes[0].Pen.Brush, rectFreqValueBar);
                }
            });

            //
            UIHelper.BeginInvoke(() =>
            {
                if (_bindControl != null)
                {
                    _bindControl?.InvalidateVisual();
                }
            });
        }

        /// <summary>
        /// 强制重绘
        /// </summary>
        protected override void ForceRepaint()
        {
            SpikeScopePainterCenter.Instance.ForceRepaint();
        }

    }
}
