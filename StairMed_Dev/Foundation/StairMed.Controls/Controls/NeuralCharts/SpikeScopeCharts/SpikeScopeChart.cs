﻿using HDF5CSharp;
using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.SpikeScopeCharts
{
    /// <summary>
    /// 
    /// </summary>
    public class SpikeScopeChart : NeuralChartWithContainer
    {
        /// <summary>
        /// 
        /// </summary>
        static SpikeScopeChart()
        {
            
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpikeScopeChart), new FrameworkPropertyMetadata(typeof(SpikeScopeChart)));
        }

        #region 数据

        /// <summary>
        /// X范围：+x， -x/2
        /// </summary>
        public int TimeScaleMS
        {
            get { return (int)GetValue(TimeScaleMSProperty); }
            set { SetValue(TimeScaleMSProperty, value); }
        }
        public static readonly DependencyProperty TimeScaleMSProperty = DependencyProperty.Register(nameof(TimeScaleMS), typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(4, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// Y轴范围：正负
        /// </summary>
        public int UVScale
        {
            get { return (int)GetValue(UVScaleProperty); }
            set { SetValue(UVScaleProperty, value); }
        }
        public static readonly DependencyProperty UVScaleProperty = DependencyProperty.Register(nameof(UVScale), typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(0, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否绘制横线
        /// </summary>
        public bool IsPaintGridLine
        {
            get { return (bool)GetValue(IsPaintGridLineProperty); }
            set { SetValue(IsPaintGridLineProperty, value); }
        }
        public static readonly DependencyProperty IsPaintGridLineProperty = DependencyProperty.Register(nameof(IsPaintGridLine), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        public bool IsPaintGridLineMore
        {
            get { return (bool)GetValue(IsPaintGridLineMoreProperty); }
            set { SetValue(IsPaintGridLineMoreProperty, value); }
        }
        public static readonly DependencyProperty IsPaintGridLineMoreProperty = DependencyProperty.Register(nameof(IsPaintGridLineMore), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// 是否绘制阈值线
        /// </summary>
        public bool IsPaintThreshold
        {
            get { return (bool)GetValue(IsPaintThresholdProperty); }
            set { SetValue(IsPaintThresholdProperty, value); }
        }
        public static readonly DependencyProperty IsPaintThresholdProperty = DependencyProperty.Register(nameof(IsPaintThreshold), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        public static System.Drawing.Color colorbg;
     /// <summary>
     /// 是否绘制rms和spikerate
     /// </summary>
     public bool IsPaintInfo
        {
            get { return (bool)GetValue(IsPaintInfoProperty); }
            set { SetValue(IsPaintInfoProperty, value); }
        }
        public static readonly DependencyProperty IsPaintInfoProperty = DependencyProperty.Register(nameof(IsPaintInfo), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 名称是否绘制在方框内
        /// </summary>
        public bool IsChannelNameInRect
        {
            get { return (bool)GetValue(IsChannelNameInRectProperty); }
            set { SetValue(IsChannelNameInRectProperty, value); }
        }
        public static readonly DependencyProperty IsChannelNameInRectProperty = DependencyProperty.Register(nameof(IsChannelNameInRect), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 电压阈值
        /// </summary>
        public double Threshold
        {
            get { return (double)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register(nameof(Threshold), typeof(double), typeof(SpikeScopeChart), new PropertyMetadata(-80d, new PropertyChangedCallback(PropertyChanged)));

        public int SelectedChannel
        {
            get { return (int)GetValue(SelectedChannelProperty); }
            set { SetValue(SelectedChannelProperty, value); }
        }
        public static readonly DependencyProperty SelectedChannelProperty = DependencyProperty.Register(nameof(SelectedChannel), typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(-1, new PropertyChangedCallback(PropertyChanged)));

        public int PhysicsChannel
        {
            get { return (int)GetValue(PhysicsChannelProperty); }
            set { SetValue(PhysicsChannelProperty, value); }
        }

        public static readonly DependencyProperty PhysicsChannelProperty = DependencyProperty.Register(nameof(PhysicsChannel), typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(-2, new PropertyChangedCallback(PropertyChanged)));


        /// <summary>
        /// RMS
        /// </summary>
        public double RMS
        {
            get { return (double)GetValue(RMSProperty); }
            set { SetValue(RMSProperty, value); }
        }
        public static readonly DependencyProperty RMSProperty = DependencyProperty.Register(nameof(RMS), typeof(double), typeof(SpikeScopeChart), new PropertyMetadata(0d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// spike发放频率
        /// </summary>
        public double SpikeRate
        {
            get { return (double)GetValue(SpikeRateProperty); }
            set { SetValue(SpikeRateProperty, value); }
        }
        public static readonly DependencyProperty SpikeRateProperty = DependencyProperty.Register(nameof(SpikeRate), typeof(double), typeof(SpikeScopeChart), new PropertyMetadata(0d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否快照一下
        /// </summary>
        public bool TakeSnapshot
        {
            get { return (bool)GetValue(TakeSnapshotProperty); }
            set { SetValue(TakeSnapshotProperty, value); }
        }
        public static readonly DependencyProperty TakeSnapshotProperty = DependencyProperty.Register(nameof(TakeSnapshot), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否清空快照
        /// </summary>
        public bool ClearSnapshot
        {
            get { return (bool)GetValue(ClearSnapshotProperty); }
            set { SetValue(ClearSnapshotProperty, value); }
        }
        public static readonly DependencyProperty ClearSnapshotProperty = DependencyProperty.Register(nameof(ClearSnapshot), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否清空Scope
        /// </summary>
        public bool ClearScope
        {
            get { return (bool)GetValue(ClearScopeProperty); }
            set { SetValue(ClearScopeProperty, value); }
        }
        public static readonly DependencyProperty ClearScopeProperty = DependencyProperty.Register(nameof(ClearScope), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否懒绘制
        /// </summary>
        public bool IsLazyPaint
        {
            get { return (bool)GetValue(IsLazyPaintProperty); }
            set { SetValue(IsLazyPaintProperty, value); }
        }
        public static readonly DependencyProperty IsLazyPaintProperty = DependencyProperty.Register(nameof(IsLazyPaint), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(true, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 显示抑制波形
        /// </summary>
        public bool ShowArtifacts
        {
            get { return (bool)GetValue(ShowArtifactsProperty); }
            set { SetValue(ShowArtifactsProperty, value); }
        }
        public static readonly DependencyProperty ShowArtifactsProperty = DependencyProperty.Register(nameof(ShowArtifacts), typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// ArtifactThreshold
        /// </summary>
        public int ArtifactThreshold
        {
            get { return (int)GetValue(ArtifactThresholdProperty); }
            set { SetValue(ArtifactThresholdProperty, value); }
        }
        public static readonly DependencyProperty ArtifactThresholdProperty = DependencyProperty.Register(nameof(ArtifactThreshold), typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(2500, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 绘制的spikeScope个数
        /// </summary>
        public int SpikeScopeReserved
        {
            get { return (int)GetValue(SpikeScopeReservedProperty); }
            set { SetValue(SpikeScopeReservedProperty, value); }
        }
        public static readonly DependencyProperty SpikeScopeReservedProperty = DependencyProperty.Register(nameof(SpikeScopeReserved), typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(22, new PropertyChangedCallback(PropertyChanged)));
        public bool IsSorted
        {
            get { return (bool)GetValue(IsSortedProperty); }
            set { SetValue(IsSortedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsSorted.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsSortedProperty =
            DependencyProperty.Register("IsSorted", typeof(bool), typeof(SpikeScopeChart), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));



        public int SortedIndex
        {
            get { return (int)GetValue(SortedIndexProperty); }
            set { SetValue(SortedIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SortedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SortedIndexProperty =
            DependencyProperty.Register("SortedIndex", typeof(int), typeof(SpikeScopeChart), new PropertyMetadata(-1, new PropertyChangedCallback(PropertyChanged)));



        private int MScount = 0;
        #endregion


        #region 属性变化监听

        /// <summary>
        /// 属性变化监听
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            //
            UpdatePainterOnPropertyChanged(e);

            //
            if (e.Property.Equals(UVScaleProperty) || e.Property.Equals(DrawAreaHeightProperty))
            {
                UpdateYRatio();

                //
                if (UVScale > 0)
                {
                    _horizonLineTxt.Clear();
                    _horizonLineTxt.Add(this.CreateFormatText($"{UVScale}μV"));
                    _horizonLineTxt.Add(this.CreateFormatText($"{0}μV"));
                    _horizonLineTxt.Add(this.CreateFormatText($"{-UVScale}μV"));
                }

                //
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(TimeScaleMSProperty) || e.Property.Equals(DrawAreaWidthProperty))
            {
                MScount=0;
                minX = -TimeScaleMS / 2;
                for (int i = minX; i < TimeScaleMS+1; i++)
                {
                    MScount++;
                }
                MScount -= 2;
                UpdateXRatio();
                //
                if (TimeScaleMS > 0&& maxX!=TimeScaleMS)
                {
                    maxX = TimeScaleMS;
                    _verticalLineX.Clear();
                    _verticalLineTxt.Clear();
                    _verticalLineTxtX.Clear();

                    //
                    for (int i = 0; i < MScount+2; i++)
                    {
                        var x = this.MakeLineThin(i  * _xratio + OriginPoint.X);
                        _verticalLineX.Add(x);
                        var txt = this.CreateFormatText($"{minX}{"ms"}");
                        minX++;
                        _verticalLineTxt.Add(txt);
                        _verticalLineTxtX.Add(x - txt.Width / 2);
                    }
                }

                //
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(ThresholdProperty))
            {
                _thresholdTxt = this.CreateFormatText($"{Threshold}", Brushes.White);

                //
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(IsPaintGridLineProperty) 
                || e.Property.Equals(IsPaintGridLineMoreProperty)
                || e.Property.Equals(IsPaintThresholdProperty) || e.Property.Equals(IsPaintInfoProperty) || e.Property.Equals(IsChannelNameInRectProperty) || e.Property.Equals(TakeSnapshotProperty) || e.Property.Equals(ClearSnapshotProperty) || e.Property.Equals(ClearScopeProperty) || e.Property.Equals(IsLazyPaintProperty) || e.Property.Equals(ShowArtifactsProperty) || e.Property.Equals(ArtifactThresholdProperty) || e.Property.Equals(SpikeScopeReservedProperty))
            {
                DelayReRender();
                return;
            }

            if (e.Property.Equals(SelectedChannelProperty))
            {
                InvalidateVisual();
                return;
            }

            //
            if (e.Property.Equals(RMSProperty) || e.Property.Equals(SpikeRateProperty))
            {
                InvalidateVisual();
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void UpdatePainterOnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (_painter == null)
            {
                return;
            }

            //
            if (e.Property.Equals(IsVisibleToUserProperty))
            {
                _painter.IsVisibleToUser = IsVisibleToUser;
            }
            else if (e.Property.Equals(DrawAreaWidthProperty))
            {
                _painter.Width = DrawAreaWidth;
            }
            else if (e.Property.Equals(DrawAreaHeightProperty))
            {
                _painter.Height = DrawAreaHeight;
            }
            else if (e.Property.Equals(OriginPointProperty))
            {
                _painter.OriginPt = OriginPoint;
            }
            else if (e.Property.Equals(TakeSnapshotProperty))
            {
                _painter.TakeSnapshot = TakeSnapshot;
            }
            else if (e.Property.Equals(ClearSnapshotProperty))
            {
                _painter.ClearSnapshot = ClearSnapshot;
            }
            else if (e.Property.Equals(ClearScopeProperty))
            {
                _painter.ClearScope = ClearScope;
            }
            else if (e.Property.Equals(ChannelProperty))
            {
                _painter.Channel = Channel;
            }
            else if (e.Property.Equals(UVScaleProperty))
            {
                _painter.UVScale = this.UVScale;
            }
            else if (e.Property.Equals(IsLazyPaintProperty))
            {
                _painter.LazyPaint = IsLazyPaint;
            }
            else if (e.Property.Equals(ShowArtifactsProperty))
            {
                _painter.ShowArtifacts = ShowArtifacts;
            }
            else if (e.Property.Equals(BackgroundProperty))
            {
                _painter.BackgroundColor = ((SolidColorBrush)Background).Color.ToColor();
            }
            else if (e.Property.Equals(SpikeScopeReservedProperty))
            {
                _painter.SpikeScopeReserved = SpikeScopeReserved;
            }
            else if (e.Property.Equals(IsSortedProperty))
            {
                _painter.IsSorted = IsSorted;
            }
            else if (e.Property.Equals(SortedIndexProperty))
            {
                _painter.SortedIndex = SortedIndex;
            }
            else if (e.Property.Equals(LineBrushProperty))
            {
                _painter.LineDrawingPen = new System.Drawing.Pen(((SolidColorBrush)LineBrush).Color.ToColor(), LINE_WIDTH);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateYRatio()
        {
            if (UVScale > 0)
            {
                _yratio = DrawAreaHeight / UVScale / 2;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateXRatio()
        {
            if (TimeScaleMS > 0)
            {
                
                _xratio = DrawAreaWidth / (MScount+1);// / 1.5;
            }
        }

        #endregion



        #region buffer data：avoid repeat

        //垂直线作图数据
        private int minX = -1;
        private int maxX = 2;
        private readonly List<double> _verticalLineX = new List<double>();
        private readonly List<FormattedText> _verticalLineTxt = new List<FormattedText>();
        private readonly List<double> _verticalLineTxtX = new List<double>();

        //水平线文本
        private readonly List<FormattedText> _horizonLineTxt = new List<FormattedText>();

        //阈值文本
        private FormattedText _thresholdTxt;

        /// <summary>
        /// overflow线条颜色
        /// </summary>
        private readonly Pen _artifactPen = new Pen(new SolidColorBrush(Color.FromRgb(0x00, 0x00, 0xff)), LINE_WIDTH);

        #endregion


        //绘图工具
        private readonly SpikeScopePainter _painter;

        /// <summary>
        /// 
        /// </summary>
        public SpikeScopeChart()
        {
            _painter = new SpikeScopePainter(this);
        }

        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            System.Windows.Media.Color color = System.Windows.Media.Color.FromRgb(173, 224, 243);
            colorbg = Extensions.ToColor(color);
            _painter.BackgroundColor = colorbg;
            _painter.CopyFromShadow(ctx, OriginPoint, DrawAreaWidth, DrawAreaHeight);
        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            
            //绘制垂直线
            if (IsPaintGridLine&& !IsPaintGridLineMore)
            {
               PaintVerticalLines(ctx);
                PaintHorizontalLines(ctx);
            }

            //绘制阈值线
            if (IsPaintThreshold && UVScale > 0 && Threshold < UVScale && _thresholdTxt != null)
            {
                var y = this.MakeLineThin(OriginPoint.Y - (Threshold + UVScale) * _yratio);
                ctx.DrawLine(_redPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                ctx.DrawRectangle(_redPen.Brush, _redPen, new Rect(
                    OriginPoint.X,// - TEXT_Margin_Left * 2 - _thresholdTxt.Width - TEXT_MARGIN
                    y - _thresholdTxt.Height / 2 - TEXT_Margin_Top,
                    _thresholdTxt.Width + TEXT_Margin_Left * 2,
                    _thresholdTxt.Height + TEXT_Margin_Top * 2));
                ctx.DrawText(_thresholdTxt, new Point(OriginPoint.X, y - _thresholdTxt.Height / 2));// - TEXT_MARGIN - _thresholdTxt.Width- TEXT_Margin_Left


            }

            //绘制overflow线
            if (IsPaintThreshold && ArtifactThreshold > 0 && ArtifactThreshold < UVScale)
            {
                var y1 = this.MakeLineThin(OriginPoint.Y - (ArtifactThreshold + UVScale) * _yratio);
                var y2 = this.MakeLineThin(OriginPoint.Y - (-ArtifactThreshold + UVScale) * _yratio);
                ctx.DrawLine(_artifactPen, new Point(OriginPoint.X, y1), new Point(XAxisMaxPoint.X, y1));
                ctx.DrawLine(_artifactPen, new Point(OriginPoint.X, y2), new Point(XAxisMaxPoint.X, y2));
            }
            
              ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));

            //绘制大方框
            if (SelectedChannel == PhysicsChannel)
            {
                System.Windows.Media.Color color = System.Windows.Media.Color.FromRgb(98, 189, 154);
                _painter.BackgroundColor = Extensions.ToColor(color);
            }
                
                

            if (IsPaintGridLineMore)
            {
                try
                {
                   double  minX = -TimeScaleMS / 2;
                   var  CountX =0;
                    for (double i = minX; i < TimeScaleMS + 1; i++)
                    {
                        CountX++;
                    }
                    
                    var Xshu = Math.Round( (double)CountX / (TimeScaleMS==2?8: 11),1);//876

                    for (int i = 0; i < (TimeScaleMS == 2 ? 7 : 11); i++)
                    {
                        //paint grid uv
                      
                            var y1 = OriginPoint.Y - (DrawAreaHeight/(TimeScaleMS == 2 ? 6 : 10)) * i;
                         
                            ctx.DrawLine(_dimGrayPen, new Point(OriginPoint.X, y1), new Point(XAxisMaxPoint.X, y1));

                            var grid_text = this.CreateFormatText($"{(-UVScale) + (UVScale / 5) * (i)}");
                            if (i < 4)
                                ctx.DrawText(grid_text, new Point(OriginPoint.X, y1 - grid_text.Height));
                            else
                                ctx.DrawText(grid_text, new Point(OriginPoint.X, y1));
                        

                        //paint grid ts
                        var x = OriginPoint.X + (DrawAreaWidth/ (TimeScaleMS == 2 ? 6 : 10)) * i ;
                        var grid_text_x = this.CreateFormatText(string.Format("{0:N1}",$"{Convert.ToDecimal(minX)}"));

                       
                        ctx.DrawLine(_dimGrayPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                        ctx.DrawText(grid_text_x, new Point(x- (TEXT_MARGIN*2), OriginPoint.Y + TEXT_MARGIN));
                        minX += Xshu;


                    }
                }
                catch
                {
                    ;
                }
            }
        }

        /// <summary>
        /// 绘制水平线
        /// </summary>
        /// <param name="ctx"></param>
        private void PaintHorizontalLines(DrawingContext ctx)
        {
            {
                var txt = _horizonLineTxt[0];
                ctx.DrawText(txt, new Point(OriginPoint.X, YAxisMaxPoint.Y));// - TEXT_MARGIN - txt.Width
            }
            {
                var y = OriginPoint.Y - DrawAreaHeight / 2;
                ctx.DrawLine(_dimGrayPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                var txt = _horizonLineTxt[1];
                ctx.DrawText(txt, new Point(OriginPoint.X, y - txt.Height / 2));// - TEXT_MARGIN - txt.Width
            }
            {
                var txt = _horizonLineTxt[2];
                ctx.DrawText(txt, new Point(OriginPoint.X, OriginPoint.Y - txt.Height));// - TEXT_MARGIN - txt.Width
            }
        }

        /// <summary>
        /// 绘制竖线
        /// </summary>
        /// <param name="ctx"></param>
        private void PaintVerticalLines(DrawingContext ctx)
        {
            var count = _verticalLineX.Count;
            for (int i = 0; i < count; i++)
            {
                var x = _verticalLineX[i];

                ctx.DrawLine(_dimGrayPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                ctx.DrawText(_verticalLineTxt[i], new Point(_verticalLineTxtX[i], OriginPoint.Y + TEXT_MARGIN));
            }
        }

        /// <summary>
        /// 绘制标题信息等
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            if (IsPaintInfo)
            {
                PaintInfoOnTopRight(ctx, $"RMS: {RMS:F2} μV,  {SpikeRate:F2} spikes/s");
            }

            //
            if (IsChannelNameInRect)
            {
                ctx.DrawText(_channelNameTxt, new Point(OriginPoint.X + TEXT_MARGIN, YAxisMaxPoint.Y + TEXT_MARGIN));
            }
            else
            {
                PaintChannelNameOnOuterTopLeft(ctx);
            }
        }

        /// <summary>
        /// 鼠标点选阈值
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            //
            if (DrawAreaWidth <= 0 || DrawAreaHeight <= 0)
            {
                return;
            }
          
           //
           var pt = e.GetPosition(this);

            //
            if (DrawAreaRect.Contains(pt.X, pt.Y))
            {
                var uv = Math.Round((OriginPoint.Y - pt.Y) / _yratio - UVScale);
               // StairMedSettings.Instance.Threshold = uv;
                Threshold = uv;
               
            }

            //
            base.OnMouseDown(e);
        }
    }
}
