﻿using StairMed.Tools;
using System.Collections.Generic;
using System.Drawing;

namespace StairMed.Controls.Controls.NeuralCharts.SpikeScopeCharts
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpikeScopeColorHelper
    {
        #region 线条颜色

        /// <summary>
        /// 
        /// </summary>
        public static readonly Dictionary<int, Color> _colorDict = new Dictionary<int, Color>() { };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        public static Color GetColor(int label)
        {
            if (!_colorDict.ContainsKey(label))
            {
                if (label < 0)
                {
                    _colorDict[label] = System.Windows.Media.Color.FromRgb(0xff, 0xff, 0xff).ToColor();
                }
                else
                {
                    _colorDict[label] = RGBHSB.GetColor(label).ToColor();
                }
            }
            return _colorDict[label];
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly Dictionary<int, Pen> _penDict = new Dictionary<int, Pen>() { };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        public static Pen GetPen(int label)
        {
            if (!_penDict.ContainsKey(label))
            {
                _penDict[label] = new Pen(GetColor(label), Base.NeuralChartBase.LINE_WIDTH);
            }
            return _penDict[label];
        }

        #endregion
    }
}
