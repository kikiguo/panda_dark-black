﻿using StairMed.Tools;
using System;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Spectrums
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SpectrogramChart : SpectrumChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        public static ColorScale colorScale = new ColorScale(-1.7f, 3.5f);

        /// <summary>
        /// 
        /// </summary>
        static SpectrogramChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpectrogramChart), new FrameworkPropertyMetadata(typeof(SpectrogramChart)));
        }

        /// <summary>
        /// 总共多少列：time scale / deltaT
        /// </summary>
        public int Columns
        {
            get { return (int)GetValue(ColumnsProperty); }
            set { SetValue(ColumnsProperty, value); }
        }
        public static readonly DependencyProperty ColumnsProperty = DependencyProperty.Register(nameof(Columns), typeof(int), typeof(SpectrumChartBase), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 时长
        /// </summary>
        public int TimeScaleS
        {
            get { return (int)GetValue(TimeScaleSProperty); }
            set { SetValue(TimeScaleSProperty, value); }
        }
        public static readonly DependencyProperty TimeScaleSProperty = DependencyProperty.Register(nameof(TimeScaleS), typeof(int), typeof(SpectrumChartBase), new PropertyMetadata(10, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        private readonly SpectrogramPainter _spectrogramPainter = new SpectrogramPainter();

        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            //
            var maxFreq = (double)MaxFreq;
            var minFreq = (double)MinFreq;
            var rows = 0;

            //绘制波形
            if (PSDs != null && PSDs.Count > 0 && PSDs[^1].PSD.Length > 0 && MaxFreq > 0 && MinFreq >= 0 && MaxFreq > MinFreq && DeltaF > 0 && TimeScaleS > 0 && Columns > 0)
            {
                //
                var firstIndex = Math.Max(0, Convert.ToInt32(Math.Floor(0.5 + MinFreq / DeltaF)));
                var endIndex = Math.Min(PSDs[0].PSD.Length - 1, Convert.ToInt32(Math.Floor(0.5 + MaxFreq / DeltaF)));
                rows = endIndex - firstIndex + 1;

                //
                maxFreq = (endIndex + 0.5) * DeltaF;
                minFreq = (firstIndex - 0.5) * DeltaF;

                //
                _spectrogramPainter.PaintSpectrogram(ctx, _bgColor, OriginPoint, DrawAreaWidth, DrawAreaHeight, firstIndex, endIndex, PSDs, Columns);
            }

            //绘制彩带
            if (MaxY > MinY)
            {
                var left = XAxisMaxPoint.X + 10;
                var right = XAxisMaxPoint.X + 30;

                //彩虹
                {
                    var y = this.MakeLineThin(YAxisMaxPoint.Y);
                    var yval = MaxY;
                    var valStep = (MinY - MaxY) / DrawAreaHeight;
                    while (y <= OriginPoint.Y)
                    {
                        var color = colorScale.GetColor(yval);
                        ctx.DrawLine(new Pen(new SolidColorBrush(color), 1), new Point(left, y), new Point(right, y));
                        y += 1;
                        yval += valStep;
                    }
                }

                //纵轴对数
                {
                    var maxY = Math.Pow(10, MaxY);
                    var minY = Math.Pow(10, MinY);
                    var yPerPixel = DrawAreaHeight / (MaxY - MinY);
                    var val = 0.0;
                    var pow = Math.Floor(Math.Log10(minY));
                    while (val < maxY)
                    {
                        for (int i = 1; i < 10; i++)
                        {
                            val = i * Math.Pow(10, pow);
                            if (val > minY && val < maxY)
                            {
                                var y = this.MakeLineThin(OriginPoint.Y - (Math.Log10(val) - MinY) * yPerPixel);
                                ctx.DrawLine(_foregroundPen, new Point(right, y), new Point(right + TICK_LINE_LENGTH, y));
                                if (i == 1)
                                {
                                    ctx.DrawLine(_foregroundPen, new Point(right, y), new Point(right + 2 * TICK_LINE_LENGTH, y));
                                    var txt = this.CreateFormatText($"{val}{Environment.NewLine}μV/√Hz");
                                    ctx.DrawText(txt, new Point(right + 2 * TICK_LINE_LENGTH + TEXT_MARGIN, y - txt.Height / 2));
                                }
                            }
                        }
                        pow += 1;
                    }
                }
            }

            //Y轴
            {
                var minTxt = this.CreateFormatText($"{MinFreq}Hz");
                var maxTxt = this.CreateFormatText($"{MaxFreq}Hz");
                if (rows > 0)
                {
                    {
                        var y = OriginPoint.Y - (MinFreq - minFreq) / (maxFreq - minFreq) * DrawAreaHeight;
                        ctx.DrawText(minTxt, new Point(OriginPoint.X - minTxt.Width - TEXT_MARGIN - TICK_LINE_LENGTH, y - minTxt.Height / 2));
                        ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X - TICK_LINE_LENGTH, y), new Point(OriginPoint.X, y));
                    }
                    {
                        var y = OriginPoint.Y - (MaxFreq - minFreq) / (maxFreq - minFreq) * DrawAreaHeight;
                        ctx.DrawText(maxTxt, new Point(OriginPoint.X - maxTxt.Width - TEXT_MARGIN - TICK_LINE_LENGTH, y - maxTxt.Height / 2));
                        ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X - TICK_LINE_LENGTH, y), new Point(OriginPoint.X, y));
                    }
                }
                else
                {
                    ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, OriginPoint.Y), new Point(OriginPoint.X - TICK_LINE_LENGTH, OriginPoint.Y));
                    ctx.DrawText(minTxt, new Point(OriginPoint.X - minTxt.Width - TEXT_MARGIN, OriginPoint.Y - minTxt.Height));
                    ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, YAxisMaxPoint.Y), new Point(OriginPoint.X - TICK_LINE_LENGTH, YAxisMaxPoint.Y));
                    ctx.DrawText(maxTxt, new Point(OriginPoint.X - maxTxt.Width - TEXT_MARGIN, YAxisMaxPoint.Y - maxTxt.Height / 2));
                }
            }

            //X轴
            if (TimeScaleS > 0)
            {
                var xstep = DrawAreaWidth / TimeScaleS;
                for (int i = 0; i <= TimeScaleS; i++)
                {
                    var txt = this.CreateFormatText($"{i}{(i == TimeScaleS ? "s" : "")}");
                    var x = OriginPoint.X + i * xstep;
                    ctx.DrawLine(_foregroundPen, new Point(x, OriginPoint.Y), new Point(x, OriginPoint.Y + TICK_LINE_LENGTH));
                    ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + TICK_LINE_LENGTH + TEXT_MARGIN));
                }
            }


            //绘制Marker线
            if (ShowMarkerFreq && MarkerFreq > 0 && MaxFreq > MinFreq)
            {
                var markFreq = MarkerFreq;
                var yperPixel = DrawAreaHeight / (maxFreq - minFreq);
                for (int i = 0; i < 1 + Harmonics; i++)
                {
                    if (markFreq < minFreq)
                    {
                        markFreq += MarkerFreq;
                        continue;
                    }
                    if (markFreq > maxFreq)
                    {
                        break;
                    }

                    //
                    var y = this.MakeLineThin(OriginPoint.Y - (markFreq - minFreq) * yperPixel);
                    ctx.DrawLine(_redPen, new Point(OriginPoint.X - TICK_LINE_LENGTH, y), new Point(XAxisMaxPoint.X, y));
                    var txt = this.CreateFormatText($"{markFreq}Hz", _redPen.Brush);
                    ctx.DrawText(txt, new Point(OriginPoint.X - TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));

                    //
                    markFreq += MarkerFreq;
                }
            }

            //鼠标移动线
            if (IsMouseHover && MaxFreq > MinFreq)
            {
                var yperPixel = DrawAreaHeight / (maxFreq - minFreq);
                var val = Convert.ToInt32((OriginPoint.Y - MouseHoverY) / yperPixel + minFreq);
                var y = this.MakeLineThin(OriginPoint.Y - (val - minFreq) * yperPixel);
                ctx.DrawLine(_yellowPen, new Point(OriginPoint.X - TICK_LINE_LENGTH, y), new Point(XAxisMaxPoint.X, y));
                var txt = this.CreateFormatText($"{val}Hz", _yellowPen.Brush);
                ctx.DrawText(txt, new Point(OriginPoint.X - TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
            }

            //绘制大方框
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));
        }
    }
}
