﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Spectrums
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpectrumPainter : BitmapPainterBase
    {
        /// <summary>
        /// 缓存
        /// </summary>
        private PointF[] _bmpPts = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="bgcolor"></param>
        /// <param name="origin"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="pts"></param>
        /// <param name="linePen"></param>
        public void PaintSpectrum(DrawingContext ctx, System.Drawing.Color bgcolor, System.Windows.Point origin, float width, float height, List<System.Windows.Point> pts, System.Windows.Media.Pen linePen)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //
            PaintToShadow(bgcolor, origin, width, height, true, (g) =>
            {
                //
                if (pts.Count == 0)
                {

                }
                else if (pts.Count == 1)
                {
                    if (linePen.Brush is SolidColorBrush brush)
                    {
                        g.DrawEllipse(new System.Drawing.Pen(brush.Color.ToColor(), (float)linePen.Thickness), new Rectangle
                        {
                            X = Convert.ToInt32(pts[0].X - 1),
                            Y = Convert.ToInt32(pts[0].Y - 1),
                            Width = 2,
                            Height = 2
                        });
                    }
                    else
                    {
                        g.DrawEllipse(new System.Drawing.Pen(System.Drawing.Color.White, (float)linePen.Thickness), new Rectangle
                        {
                            X = Convert.ToInt32(pts[0].X - 1),
                            Y = Convert.ToInt32(pts[0].Y - 1),
                            Width = 2,
                            Height = 2
                        });
                    }
                }
                else
                {
                    if (true)
                    {
                        //绘制点
                        if (_bmpPts == null || _bmpPts.Length != pts.Count)
                        {
                            _bmpPts = new PointF[pts.Count];
                        }

                        //
                        for (int i = 0; i < pts.Count; i++)
                        {
                            _bmpPts[i] = new PointF { X = Convert.ToSingle(pts[i].X), Y = Convert.ToSingle(pts[i].Y) };
                        }

                        //
                        if (!_bmpPts.Any(r => r.Y > short.MaxValue || r.Y < short.MinValue || float.IsInfinity(r.Y)))
                        {
                            if (linePen.Brush is SolidColorBrush brush)
                            {
                                g.DrawLines(new System.Drawing.Pen(brush.Color.ToColor(), (float)linePen.Thickness), _bmpPts);
                            }
                            else
                            {
                                g.DrawLines(new System.Drawing.Pen(System.Drawing.Color.White, (float)linePen.Thickness), _bmpPts);
                            }
                        }
                    }
                    //else
                    //{
                    //    if (linePen.Brush is SolidColorBrush brush)
                    //    {
                    //        var pen = new System.Drawing.Pen(brush.Color.ToColor(), (float)linePen.Thickness);
                    //        for (int i = 0; i < pts.Count; i++)
                    //        {
                    //            g.DrawEllipse(pen, new Rectangle
                    //            {
                    //                X = Convert.ToInt32(pts[i].X),
                    //                Y = Convert.ToInt32(pts[i].Y),
                    //                Width = 1,
                    //                Height = 1
                    //            });
                    //        }
                    //    }
                    //}
                }
            }, true);

            //
            CopyFromShadow(ctx, origin, width, height);
        }
    }
}
