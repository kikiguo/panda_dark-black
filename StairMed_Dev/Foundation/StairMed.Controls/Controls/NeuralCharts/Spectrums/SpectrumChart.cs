﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Spectrums
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SpectrumChart : SpectrumChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        static SpectrumChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpectrumChart), new FrameworkPropertyMetadata(typeof(SpectrumChart)));
        }

        #region 属性变化监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            //
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(MinFreqProperty) || e.Property.Equals(MaxFreqProperty) || e.Property.Equals(DrawAreaWidthProperty))
            {
                UpdateXRatio();
                DelayReRender();
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateXRatio()
        {
            if (MaxFreq > MinFreq)
            {
                _xratio = DrawAreaWidth / (MaxFreq - MinFreq);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private readonly SpectrumPainter _spectrumPainter = new SpectrumPainter();

        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            if (PSDs == null || PSDs.Count <= 0 || PSDs[^1].PSD.Length <= 0 || MaxFreq < 0 || MinFreq < 0 || MaxFreq < MinFreq || DeltaF <= 0)
            {
                return;
            }

            //最新的一个频谱
            var newestPSD = PSDs[^1];

            //对应的数据位置
            var firstIndex = Math.Max(0, Convert.ToInt32(Math.Floor(MinFreq / DeltaF)));
            var endIndex = Math.Min(newestPSD.PSD.Length - 1, Convert.ToInt32(Math.Ceiling(MaxFreq / DeltaF)));

            //
            if (firstIndex >= endIndex)
            {
                return;
            }

            //
            var xpixelStep = _xratio * DeltaF;
            var ypixelStep = DrawAreaHeight / (MaxY - MinY);
            var firstX = (firstIndex * DeltaF - MinFreq) / DeltaF * xpixelStep;
            var lastX = (endIndex * DeltaF - MinFreq) / DeltaF * xpixelStep;

            //
            var dataLen = endIndex - firstIndex + 1;

            //点数较少时，直接绘制；点数较多时，抽样绘制
            if (dataLen <= (lastX - firstX) * 2)
            {
                var pts = new List<Point>(dataLen);

                //
                var x = firstX;
                for (var i = firstIndex; i <= endIndex; i++)
                {
                    var val = newestPSD.PSD[i];
                    pts.Add(new Point
                    {
                        X = x,
                        Y = DrawAreaHeight - ypixelStep * (val - MinY),
                    });

                    //
                    x += xpixelStep;
                }

                //
                _spectrumPainter.PaintSpectrum(ctx, _bgColor, OriginPoint, DrawAreaWidth, DrawAreaHeight, pts, _linePen);
            }
            else
            {
                var ptPairCount = Convert.ToInt32(lastX - firstX);
                var floatPer2Pt = 1.0 * dataLen / ptPairCount;
                var pts = new List<Point>(ptPairCount * 2);

                //
                var maxVal = float.MinValue;
                var minVal = float.MaxValue;
                var ptPairIndex = 0;

                //
                for (var i = firstIndex; i <= endIndex; i++)
                {
                    //样本点对应的绘制点位
                    var newPtPairIndex = (int)Math.Floor((i - firstIndex) / floatPer2Pt);

                    //切换了点位，则将上组数据保存
                    if (newPtPairIndex != ptPairIndex)
                    {
                        pts.Add(new Point
                        {
                            X = ptPairIndex,
                            Y = DrawAreaHeight - ypixelStep * (maxVal - MinY)
                        });
                        pts.Add(new Point
                        {
                            X = ptPairIndex + 0.5,
                            Y = DrawAreaHeight - ypixelStep * (minVal - MinY)
                        });

                        //
                        ptPairIndex = newPtPairIndex;
                        maxVal = float.MinValue;
                        minVal = float.MaxValue;
                    }

                    //取最大值和最小值
                    var val = newestPSD.PSD[i];
                    if (val > maxVal)
                    {
                        maxVal = val;
                    }
                    if (val < minVal)
                    {
                        minVal = val;
                    }
                }

                //
                if (maxVal != float.MinValue && minVal != float.MaxValue)
                {
                    pts.Add(new Point
                    {
                        X = ptPairIndex,
                        Y = DrawAreaHeight - ypixelStep * (maxVal - MinY)
                    });
                    pts.Add(new Point
                    {
                        X = ptPairIndex + 0.5,
                        Y = DrawAreaHeight - ypixelStep * (minVal - MinY)
                    });
                }

                //
                _spectrumPainter.PaintSpectrum(ctx, _bgColor, OriginPoint, DrawAreaWidth, DrawAreaHeight, pts, _linePen);
            }
        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //X轴刻度
            if (MaxFreq > MinFreq)
            {
                var minTxt = this.CreateFormatText($"{MinFreq}Hz");
                ctx.DrawText(minTxt, new Point(OriginPoint.X - minTxt.Width / 2, OriginPoint.Y + TEXT_MARGIN));
                var maxTxt = this.CreateFormatText($"{MaxFreq}Hz");
                ctx.DrawText(maxTxt, new Point(XAxisMaxPoint.X - maxTxt.Width, OriginPoint.Y + TEXT_MARGIN));
            }

            //绘制Marker线和谐波线：垂直线
            if (ShowMarkerFreq && MarkerFreq > 0 && MaxFreq > MinFreq)
            {
                var markFreq = MarkerFreq;
                for (int i = 0; i < 1 + Harmonics; i++)
                {
                    if (markFreq < MinFreq)
                    {
                        markFreq += MarkerFreq;
                        continue;
                    }
                    if (markFreq > MaxFreq)
                    {
                        break;
                    }

                    //
                    var x = this.MakeLineThin(OriginPoint.X + (markFreq - MinFreq) * _xratio);
                    ctx.DrawLine(_redPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                    var txt = this.CreateFormatText($"{markFreq}Hz", _redPen.Brush);
                    ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + TEXT_MARGIN));

                    //
                    markFreq += MarkerFreq;
                }
            }

            //绘制Y轴
            if (MaxY > MinY)
            {
                var maxY = Math.Pow(10, MaxY);
                var minY = Math.Pow(10, MinY);
                var yPerPixel = DrawAreaHeight / (MaxY - MinY);
                var yval = 0.0;
                var pow = Math.Floor(Math.Log10(minY));
                while (yval < maxY)
                {
                    for (int i = 1; i < 10; i++)
                    {
                        yval = i * Math.Pow(10, pow);
                        if (yval > minY && yval < maxY)
                        {
                            var y = this.MakeLineThin(OriginPoint.Y - (Math.Log10(yval) - MinY) * yPerPixel);
                            ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TICK_LINE_LENGTH, y));
                            if (i == 1)
                            {
                                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - 2 * TICK_LINE_LENGTH, y));
                                ctx.DrawLine(_dimGrayPen, new Point(OriginPoint.X, y), new Point(XAxisMaxPoint.X, y));
                                var txt = this.CreateFormatText($"{yval}{Environment.NewLine}μV/√Hz");
                                ctx.DrawText(txt, new Point(OriginPoint.X - 2 * TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
                            }
                        }
                    }
                    pow += 1;
                }
            }

            //绘制大方框
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));
        }

        /// <summary>
        /// 绘制Hover状态
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintHover(DrawingContext ctx)
        {
            if (MaxFreq > MinFreq)
            {
                var val = Convert.ToInt32((MouseHoverX - OriginPoint.X) / _xratio + MinFreq);
                var x = this.MakeLineThin(OriginPoint.X + (val - MinFreq) * _xratio);
                ctx.DrawLine(_yellowPen, new Point(x, OriginPoint.Y), new Point(x, YAxisMaxPoint.Y));
                var txt = this.CreateFormatText($"{val}Hz", _yellowPen.Brush);
                ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + TEXT_MARGIN));
            }
        }
    }
}
