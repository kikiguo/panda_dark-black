﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.DataCenter.Centers.Spectrum;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Spectrums
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SpectrumChartBase : NeuralChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        static SpectrumChartBase()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SpectrumChartBase), new FrameworkPropertyMetadata(typeof(SpectrumChartBase)));
        }

        /// <summary>
        /// 
        /// </summary>
        public SpectrumChartBase()
        {
            MaxY = 3.5f;
            MinY = -1.7f;
        }

        /// <summary>
        /// 功率谱
        /// </summary>
        public List<PSDInfo> PSDs
        {
            get { return (List<PSDInfo>)GetValue(PSDsProperty); }
            set { SetValue(PSDsProperty, value); }
        }
        public static readonly DependencyProperty PSDsProperty = DependencyProperty.Register(nameof(PSDs), typeof(List<PSDInfo>), typeof(SpectrumChartBase), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 是否展示参考频率
        /// </summary>
        public bool ShowMarkerFreq
        {
            get { return (bool)GetValue(ShowMarkerFreqProperty); }
            set { SetValue(ShowMarkerFreqProperty, value); }
        }
        public static readonly DependencyProperty ShowMarkerFreqProperty = DependencyProperty.Register(nameof(ShowMarkerFreq), typeof(bool), typeof(SpectrumChartBase), new PropertyMetadata(false, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 标记线频率
        /// </summary>
        public int MarkerFreq
        {
            get { return (int)GetValue(MarkerFreqProperty); }
            set { SetValue(MarkerFreqProperty, value); }
        }
        public static readonly DependencyProperty MarkerFreqProperty = DependencyProperty.Register(nameof(MarkerFreq), typeof(int), typeof(SpectrumChartBase), new PropertyMetadata(60, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 谐波个数
        /// </summary>
        public int Harmonics
        {
            get { return (int)GetValue(HarmonicsProperty); }
            set { SetValue(HarmonicsProperty, value); }
        }
        public static readonly DependencyProperty HarmonicsProperty = DependencyProperty.Register(nameof(Harmonics), typeof(int), typeof(SpectrumChartBase), new PropertyMetadata(0, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 频率上限
        /// </summary>
        public int MaxFreq
        {
            get { return (int)GetValue(MaxFreqProperty); }
            set { SetValue(MaxFreqProperty, value); }
        }
        public static readonly DependencyProperty MaxFreqProperty = DependencyProperty.Register(nameof(MaxFreq), typeof(int), typeof(SpectrumChartBase), new PropertyMetadata(100, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 频率下限
        /// </summary>
        public int MinFreq
        {
            get { return (int)GetValue(MinFreqProperty); }
            set { SetValue(MinFreqProperty, value); }
        }
        public static readonly DependencyProperty MinFreqProperty = DependencyProperty.Register(nameof(MinFreq), typeof(int), typeof(SpectrumChartBase), new PropertyMetadata(0, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 频率步长
        /// </summary>
        public double DeltaF
        {
            get { return (double)GetValue(DeltaFProperty); }
            set { SetValue(DeltaFProperty, value); }
        }
        public static readonly DependencyProperty DeltaFProperty = DependencyProperty.Register(nameof(DeltaF), typeof(double), typeof(SpectrumChartBase), new PropertyMetadata(100d, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            //
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(ShowMarkerFreqProperty) || e.Property.Equals(MarkerFreqProperty) || e.Property.Equals(HarmonicsProperty) || e.Property.Equals(MaxFreqProperty) || e.Property.Equals(MinFreqProperty) || e.Property.Equals(DeltaFProperty))
            {
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(PSDsProperty))
            {
                InvalidateVisual();
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            PaintChannelNameOnOuterTopLeft(ctx);
        }

    }
}
