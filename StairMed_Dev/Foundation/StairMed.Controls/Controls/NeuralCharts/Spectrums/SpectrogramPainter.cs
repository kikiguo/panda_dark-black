﻿using StairMed.Controls.Controls.NeuralCharts.Painters;
using StairMed.DataCenter.Centers.Spectrum;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts.Spectrums
{
    /// <summary>
    /// 
    /// </summary>
    internal class SpectrogramPainter : BitmapPainterBase
    {
        /// <summary>
        /// 
        /// </summary>
        public static ColorScale colorScale = new ColorScale(-1.7f, 3.5f);

        /// <summary>
        /// 
        /// </summary>
        private long _lastPsdIndex = -1;
        private int _lastFirstFreqIndex = -1;
        private int _lastEndFreqIndex = -1;
        private int _lastColumns = -1;
        private int _lastHeight = -1;
        private int _lastWidth = -1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="bgcolor"></param>
        /// <param name="origin"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="firstFreqIndex"></param>
        /// <param name="endFreqIndex"></param>
        /// <param name="psds"></param>
        /// <param name="columns"></param>
        public void PaintSpectrogram(DrawingContext ctx, System.Drawing.Color bgcolor, System.Windows.Point origin, float width, float height, int firstFreqIndex, int endFreqIndex, List<PSDInfo> psds, int columns)
        {
            int bmpW = (int)width;
            int bmpH = (int)height;
            if (bmpW == 0 || bmpH == 0)
            {
                return;
            }
            GetBitmap(bmpW, bmpH, out bool _, true);

            //频率个数
            var freqCount = endFreqIndex - firstFreqIndex + 1;
            var yStep = height / freqCount;
            var xStep = width / columns;

            //
            if (_lastFirstFreqIndex != firstFreqIndex || _lastEndFreqIndex != endFreqIndex || _lastColumns != columns || !psds.Any(r => r.PSDIndex == _lastPsdIndex) || _lastHeight != bmpH || _lastWidth != bmpW)
            {
                PaintToShadow(bgcolor, origin, width, height, true, (g) =>
                {
                    for (int i = 0; i < psds.Count; i++)
                    {
                        PaintPSD(height, firstFreqIndex, endFreqIndex, psds, columns, g, yStep, xStep, i);
                    }
                }, true);
            }
            else
            {
                PaintToShadow(bgcolor, origin, width, height, false, (g) =>
                {
                    var index = psds.FindIndex(r => r.PSDIndex == _lastPsdIndex);
                    if (index < psds.Count - 1)
                    {
                        var newDrawW = Convert.ToInt32((psds.Count - index - 1) * xStep);
                        var dstRect = new Rectangle(0, 0, bmpW - newDrawW, bmpH);
                        var srcRect = new Rectangle(newDrawW, dstRect.Y, dstRect.Width, dstRect.Height);
                        g.DrawImage(_shadowCopy, dstRect, srcRect, GraphicsUnit.Pixel);
                    }

                    //
                    for (int i = index + 1; i < psds.Count; i++)
                    {
                        PaintPSD(height, firstFreqIndex, endFreqIndex, psds, columns, g, yStep, xStep, i);
                    }
                }, true);
            }

            //
            _lastFirstFreqIndex = firstFreqIndex;
            _lastEndFreqIndex = endFreqIndex;
            _lastColumns = columns;
            _lastPsdIndex = psds[^1].PSDIndex;
            _lastHeight = bmpH;
            _lastWidth = bmpW;

            //
            CopyFromShadow(ctx, origin, width, height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="height"></param>
        /// <param name="firstFreqIndex"></param>
        /// <param name="endFreqIndex"></param>
        /// <param name="psds"></param>
        /// <param name="columns"></param>
        /// <param name="g"></param>
        /// <param name="yStep"></param>
        /// <param name="xStep"></param>
        /// <param name="psdIndex"></param>
        private static void PaintPSD(float height, int firstFreqIndex, int endFreqIndex, List<PSDInfo> psds, int columns, Graphics g, float yStep, float xStep, int psdIndex)
        {
            //当fftsize特别大时，如16384时，对应psd中频率有8193个数据，单窗口像素明显较小，可以抽样绘制即可
            var freqStep = (int)Math.Max(1, 1 / yStep);

            //
            var psd = psds[psdIndex];
            var x = (columns - psds.Count + psdIndex) * xStep;
            for (int freqIndex = firstFreqIndex; freqIndex <= endFreqIndex; freqIndex += freqStep)
            {
                var y = height - (freqIndex - firstFreqIndex) * yStep - yStep;
                var color = colorScale.GetColor(psd.PSD[freqIndex]);
                g.FillRectangle(new SolidBrush(color.ToColor()), new Rectangle(Convert.ToInt32(x), Convert.ToInt32(y), Convert.ToInt32(xStep + 1), Convert.ToInt32(yStep + 1)));
            }
        }
    }
}
