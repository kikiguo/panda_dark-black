﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace StairMed.Controls.Controls.NeuralCharts
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImpedanceChart : NeuralChartBase
    {
        /// <summary>
        /// 
        /// </summary>
        private const int xstep = 10;

        /// <summary>
        /// 
        /// </summary>
        static ImpedanceChart()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImpedanceChart), new FrameworkPropertyMetadata(typeof(ImpedanceChart)));
        }

        /// <summary>
        /// 通道个数
        /// </summary>
        public int ChannelCount
        {
            get { return (int)GetValue(ChannelCountProperty); }
            set { SetValue(ChannelCountProperty, value); }
        }
        public static readonly DependencyProperty ChannelCountProperty = DependencyProperty.Register(nameof(ChannelCount), typeof(int), typeof(ImpedanceChart), new PropertyMetadata(128, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 当前检测中的通道
        /// </summary>
        public int CurrentChannel
        {
            get { return (int)GetValue(CurrentChannelProperty); }
            set { SetValue(CurrentChannelProperty, value); }
        }
        public static readonly DependencyProperty CurrentChannelProperty = DependencyProperty.Register(nameof(CurrentChannel), typeof(int), typeof(ImpedanceChart), new PropertyMetadata(5, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 当前检测中的Headstage
        /// </summary>
        public int CurrentHeadstage
        {
            get { return (int)GetValue(CurrentHeadstageProperty); }
            set { SetValue(CurrentHeadstageProperty, value); }
        }
        public static readonly DependencyProperty CurrentHeadstageProperty = DependencyProperty.Register(nameof(CurrentHeadstage), typeof(int), typeof(ImpedanceChart), new PropertyMetadata(-1, new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 电阻值们
        /// </summary>
        public ObservableCollection<double> Impedances
        {
            get { return (ObservableCollection<double>)GetValue(ImpedancesProperty); }
            set { SetValue(ImpedancesProperty, value); }
        }
        public static readonly DependencyProperty ImpedancesProperty = DependencyProperty.Register(nameof(Impedances), typeof(ObservableCollection<double>), typeof(ImpedanceChart), new PropertyMetadata(new ObservableCollection<double>(), new PropertyChangedCallback(PropertyChanged)));

        /// <summary>
        /// 阻抗阈值
        /// </summary>
        public double Threshold
        {
            get { return (double)GetValue(ThresholdProperty); }
            set { SetValue(ThresholdProperty, value); }
        }
        public static readonly DependencyProperty ThresholdProperty = DependencyProperty.Register(nameof(Threshold), typeof(double), typeof(ImpedanceChart), new PropertyMetadata(0d, new PropertyChangedCallback(PropertyChanged)));

        #region 属性变化监听

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            //
            base.OnPropertyChanged(e);

            //
            if (e.Property.Equals(ChannelCountProperty) || e.Property.Equals(DrawAreaWidthProperty))
            {
                UpdateXRatio();
                DelayReRender();
                return;
            }

            //
            if (e.Property.Equals(CurrentChannelProperty) || e.Property.Equals(CurrentHeadstageProperty) || e.Property.Equals(ThresholdProperty) || e.Property.Equals(ImpedancesProperty))
            {
                DelayReRender();
                return;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected override void UpdateXRatio()
        {
            _xratio = DrawAreaWidth / ((ChannelCount + xstep - 1) / xstep * xstep + 1);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        protected override void DoRealPaint(DrawingContext ctx)
        {
            //
            PaintAxises(ctx);

            //
            PaintDataGraphics(ctx);

            //
            if (IsCareMouseHover && IsMouseHover)
            {
                PaintHover(ctx);
            }

            //
            PaintTitle(ctx);
        }

        /// <summary>
        /// 绘制个性化波形
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintDataGraphics(DrawingContext ctx)
        {
            var maxY = Math.Log10(MaxY);
            var minY = Math.Log10(MinY);
            var yPerPixel = DrawAreaHeight / (maxY - minY);

            //
            var normalSize = _xratio / 3;
            for (int i = 0; i < Impedances.Count; i++)
            {
                var size = (i == CurrentChannel) ? normalSize * 1.5 : normalSize;

                //var size = (i == CurrentChannel) ? 10 : 20;
                var x = (i + 1) * _xratio + OriginPoint.X;
                var val = Impedances[i];
                if (val < MinY)
                {
                    var y = this.MakeLineThin(OriginPoint.Y);

                    #region 绘制倒三角形
                    var g1 = new PathGeometry();
                    {
                        var pt1 = new Point(x - size, y - size);
                        var pt2 = new Point(x, y + size);
                        var pt3 = new Point(x + size, y - size);

                        //
                        var line1 = new LineSegment(pt1, true);
                        var line2 = new LineSegment(pt2, true);
                        var line3 = new LineSegment(pt3, true);

                        //
                        var pf = new PathFigure
                        {
                            IsClosed = true,
                            StartPoint = pt1
                        };
                        pf.Segments.Add(line1);
                        pf.Segments.Add(line2);
                        pf.Segments.Add(line3);

                        //
                        g1.Figures.Add(pf);
                    }
                    #endregion

                    ctx.DrawGeometry((i == CurrentChannel) ? Brushes.Red : null, _redPen, g1);
                }
                else if (val > MaxY)
                {
                    var y = this.MakeLineThin(YAxisMaxPoint.Y);
                    //ctx.DrawEllipse(Brushes.DarkRed, null, new Point(x, y), size, size);

                    #region 绘制倒三角形
                    var g1 = new PathGeometry();
                    {
                        var pt1 = new Point(x - size, y + size);
                        var pt2 = new Point(x, y - size);
                        var pt3 = new Point(x + size, y + size);

                        //
                        var line1 = new LineSegment(pt1, true);
                        var line2 = new LineSegment(pt2, true);
                        var line3 = new LineSegment(pt3, true);

                        //
                        var pf = new PathFigure
                        {
                            IsClosed = true,
                            StartPoint = pt1
                        };
                        pf.Segments.Add(line1);
                        pf.Segments.Add(line2);
                        pf.Segments.Add(line3);

                        //
                        g1.Figures.Add(pf);
                    }
                    #endregion

                    ctx.DrawGeometry((i == CurrentChannel) ? Brushes.Red : null, _redPen, g1);
                }
                else
                {
                    size = (i == CurrentChannel) ? normalSize * 1.5 : Math.Min(2, normalSize);
                    var y = this.MakeLineThin(OriginPoint.Y - (Math.Log10(val) - minY) * yPerPixel);
                    //ctx.DrawEllipse(val <= Threshold ? Brushes.Blue : Brushes.Red, null, new Point(x, y), size, size);
                    ctx.DrawEllipse(_baseLinePenFull.Brush, null, new Point(x, y), size, size);
                }
            }
        }

        /// <summary>
        /// 绘制坐标轴
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintAxises(DrawingContext ctx)
        {
            //X轴刻度
            for (int i = 0; i < ChannelCount; i += xstep)
            {
                var index = i == 0 ? 1 : i;
                var txt = this.CreateFormatText($"{index}");
                var x = index * _xratio + OriginPoint.X;
                ctx.DrawLine(_foregroundPen, new Point(x, OriginPoint.Y), new Point(x, OriginPoint.Y - TICK_LINE_LENGTH));
                ctx.DrawText(txt, new Point(x - txt.Width / 2, OriginPoint.Y + TEXT_MARGIN));
            }

            //绘制Y轴
            if (MaxY > MinY)
            {
                var maxY = Math.Log10(MaxY);
                var minY = Math.Log10(MinY);
                var yPerPixel = DrawAreaHeight / (maxY - minY);
                var yval = 0.0;
                var pow = Math.Floor(minY);
                while (yval < MaxY)
                {
                    for (int i = 1; i < 10; i++)
                    {
                        yval = i * Math.Pow(10, pow);
                        if (yval <= 100000 && yval >= 10000)
                        {
                            yval *= 2;
                        }
                        if (yval >= MinY && yval <= MaxY)
                        {
                            var y = this.MakeLineThin(OriginPoint.Y - (Math.Log10(yval) - minY) * yPerPixel);
                            ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - TICK_LINE_LENGTH, y));
                            if (i == 1)
                            {
                                ctx.DrawLine(_foregroundPen, new Point(OriginPoint.X, y), new Point(OriginPoint.X - 2 * TICK_LINE_LENGTH, y));
                                FormattedText txt = null;
                                if (yval >= 1000000)
                                {
                                    txt = this.CreateFormatText($"{yval / 1000000}M");
                                }
                                else if (yval >= 1000)
                                {
                                    txt = this.CreateFormatText($"{yval / 1000}k");
                                }
                                else
                                {
                                    txt = this.CreateFormatText($"{yval}");
                                }
                                ctx.DrawText(txt, new Point(OriginPoint.X - 2 * TICK_LINE_LENGTH - TEXT_MARGIN - txt.Width, y - txt.Height / 2));
                            }
                        }
                    }
                    pow += 1;
                }

                //
                if (Threshold > MinY && Threshold < MaxY)
                {
                    var thresholdY = this.MakeLineThin(OriginPoint.Y - (Math.Log10(Threshold) - minY) * yPerPixel);
                    ctx.DrawLine(_limePen, new Point(OriginPoint.X, thresholdY), new Point(XAxisMaxPoint.X, thresholdY));
                }
            }

            //绘制大方框
            ctx.DrawRectangle(null, _foregroundPen, new Rect(OriginPoint.X, YAxisMaxPoint.Y, DrawAreaWidth, DrawAreaHeight));
        }

        /// <summary>
        /// 绘制标题信息等
        /// </summary>
        /// <param name="ctx"></param>
        protected override void PaintTitle(DrawingContext ctx)
        {
            FormattedText txt = null;
            if (Impedances.Count > 0)
            {
                txt = this.CreateFormatText($"阻抗值(总计{Impedances.Count}个, 其中 {Impedances.Count(r => r < Threshold)} 个在阈值以下)");
            }
            else
            {
                txt = this.CreateFormatText($"阻抗值");
            }
            ctx.DrawText(txt, new Point(OriginPoint.X + (DrawAreaWidth - txt.Width) / 2, YAxisMaxPoint.Y - TEXT_MARGIN * 2 - txt.Height));

            //
            if (CurrentHeadstage >= 0)
            {
                var headStageText = this.CreateFormatText($"Headstage-{CurrentHeadstage + 1}");
                ctx.DrawText(headStageText, new Point(OriginPoint.X + TEXT_MARGIN * 2, YAxisMaxPoint.Y - TEXT_MARGIN * 2 - txt.Height));
            }

            //
            PaintTitleOnLeft(ctx, "阻抗(Ohms)");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            //
            if (DrawAreaWidth <= 0 || DrawAreaHeight <= 0)
            {
                return;
            }

            //
            var pt = e.GetPosition(this);
            var channel = Convert.ToInt32((pt.X - OriginPoint.X) / _xratio);
            CurrentChannel = Math.Min(Math.Max(1, channel), ChannelCount) - 1;

            //
            base.OnMouseDown(e);
        }
    }
}
