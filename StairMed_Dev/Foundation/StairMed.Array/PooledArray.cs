﻿using System;
using System.Buffers;

namespace StairMed.Array
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PooledArray<T> : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly ArrayPool<T> Shared = ArrayPool<T>.Create(1024 * 1024 * 16, 128);

        /// <summary>
        /// 
        /// </summary>
        private int _length = 0;
        public int Length
        {
            get { return _length; }
            private set { _length = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private T[] _array;
        public T[] Array
        {
            get { return _array; }
            private set
            {
                _array = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public PooledArray(int reqLen)
        {
            Length = reqLen;
            Array = Shared.Rent(Length);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index]
        {
            get
            {
                if (Array!=null && Array.Length > index)
                    return Array[index];
                else
                    return default(T);
            }
            set
            {
                Array[index] = value;
            }

        }

        ///// <summary>
        ///// 隐式转换
        ///// </summary>
        ///// <param name="array"></param>
        //public static implicit operator T[](PooledArray<T> array)
        //{
        //    return array.Array;
        //}

        #region 释放

        /// <summary>
        /// 是否已经被析构掉
        /// </summary>
        private bool _isDisposed = false;

        /// <summary>
        /// 
        /// </summary>
        ~PooledArray()
        {
            DisposeImpl();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            DisposeImpl();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void DisposeImpl()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                Length = 0;
                Shared.Return(Array);
                Array = null;
            }
        }

        #endregion
    }
}