﻿using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.Entity.DataPools;
using StairMed.Enum;
using StairMed.MVVM.Base;
using StairMed.TCPControl.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace StairMed.TCPControl
{
    /// <summary>
    /// TCP远程控制
    /// </summary>
    public class WaveformManager : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(WaveformManager);

        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static WaveformManager Instance = new WaveformManager();
        }
        public static WaveformManager Instance => Helper.Instance;
        private WaveformManager()
        {
            NeuralDataCollector.Instance.NewChunkReceived += Instance_NewChunkReceived;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void Instance_NewChunkReceived(Chunk chunk)
        {
            if (_client == null)
            {
                return;
            }

            //
            TrySendLFPWave(chunk);

            //
            TrySendHFPWave(chunk);

            //
            TrySendWFPWave(chunk);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void TrySendWFPWave(Chunk chunk)
        {
            if (!_inputChannelDict.ContainsKey(NeuralDataType.WFP))
            {
                return;
            }

            //
            var channels = new List<int>();
            channels.AddRange(_inputChannelDict[NeuralDataType.WFP]);

            //
            if (_asyncHelperWFP == null)
            {
                _asyncHelperWFP = new BlockFIFO
                {
                    Name = nameof(TrySendWFPWave),
                    ConsumerAction = (objs) =>
                    {
                        TCPSend((int)objs[0], ((PooledArray<float>)objs[1]).Array, (int)objs[2], (int)objs[3], (long)objs[4], NeuralDataType.WFP, "Wide");
                    }
                };
            }

            //
            foreach (var channel in channels)
            {
                _asyncHelperWFP.Add(channel, chunk.Wfps, chunk.OneChannelSamples, channel * chunk.OneChannelSamples, chunk.DataIndex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void TrySendHFPWave(Chunk chunk)
        {
            if (!_inputChannelDict.ContainsKey(NeuralDataType.HFP))
            {
                return;
            }

            //
            var channels = new List<int>();
            channels.AddRange(_inputChannelDict[NeuralDataType.HFP]);

            //
            if (_asyncHelperHFP == null)
            {
                _asyncHelperHFP = new BlockFIFO
                {
                    Name = nameof(TrySendHFPWave),
                    ConsumerAction = (objs) =>
                    {
                        TCPSend((int)objs[0], ((PooledArray<float>)objs[1]).Array, (int)objs[2], (int)objs[3], (long)objs[4], NeuralDataType.HFP,"High");
                    }
                };
            }

            //
            foreach (var channel in channels)
            {
                _asyncHelperHFP.Add(channel, chunk.Hfps, chunk.OneChannelSamples, channel * chunk.OneChannelSamples, chunk.DataIndex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void TrySendLFPWave(Chunk chunk)
        {
            if (!_inputChannelDict.ContainsKey(NeuralDataType.LFP))
            {
                return;
            }

            //
            var channels = new List<int>();
            channels.AddRange(_inputChannelDict[NeuralDataType.LFP]);

            //
            if (_asyncHelperLFP == null)
            {
                _asyncHelperLFP = new BlockFIFO
                {
                    Name = nameof(TrySendLFPWave),
                    ConsumerAction = (objs) =>
                    {
                        TCPSend((int)objs[0], ((PooledArray<float>)objs[1]).Array, (int)objs[2], (int)objs[3], (long)objs[4], NeuralDataType.LFP, "Low");
                    }
                };
            }

            //
            foreach (var channel in channels)
            {
                _asyncHelperLFP.Add(channel, chunk.Lfps, chunk.OneChannelSamples, channel * chunk.OneChannelSamples, chunk.DataIndex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _listening = false;
        public bool Listening
        {
            get { return _listening; }
            set { Set(ref _listening, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _connected = false;
        public bool Connected
        {
            get { return _connected; }
            set { Set(ref _connected, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private Socket _server = null;
        private Socket _client = null;

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<NeuralDataType, HashSet<int>> _inputChannelDict = new Dictionary<NeuralDataType, HashSet<int>>();
        private IAsyncFIFO _asyncHelperWFP = null;
        private IAsyncFIFO _asyncHelperLFP = null;
        private IAsyncFIFO _asyncHelperHFP = null;


        /// <summary>
        /// 
        /// </summary>
        public bool StartListen()
        {
            StopListen();

            //
            if (!IPAddress.TryParse(StairMedSettings.Instance.WaveformHost, out IPAddress ip))
            {
                Toast.Error("Host错误");
                return false;
            }

            //
            if (StairMedSettings.Instance.WaveformPort < 1 || StairMedSettings.Instance.WaveformPort > 65535)
            {
                Toast.Error("Port错误");
                return false;
            }

            //
            try
            {
                //
                var localEP = new IPEndPoint(ip, StairMedSettings.Instance.WaveformPort);
                _server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                {
                    ReceiveBufferSize = CONST.SocketBufferSize,
                    SendBufferSize = CONST.SocketBufferSize,
                };
                _server.Bind(localEP);
                _server.Listen(1);

                //
                _server.BeginAccept((IAsyncResult ar) =>
                {
                    if (_server == null)
                    {
                        return;
                    }

                    //
                    try
                    {
                        _client = _server.EndAccept(ar);
                        Connected = true;
                    }
                    catch (Exception ex)
                    {
                        LogTool.Logger.Exp($"{TAG}, EndAccept", ex);
                    }
                }, null);

                //
                Listening = true;
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, Listen", ex);
                Toast.Error($"请检查 Host 和 Port 是否重复或无效");
                _server = null;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void StopListen()
        {
            //
            if (_server != null)
            {
                try
                {
                    _server.Close();
                    _server.Dispose();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, Close", ex);
                }
                _server = null;
            }

            //
            if (_client != null)
            {
                try
                {
                    _client.Shutdown(SocketShutdown.Both);
                    _client.Dispose();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, Shutdown", ex);
                }
                _client = null;
            }

            //
            Listening = false;
            Connected = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="dataType"></param>
        public void AddChannel(int channel, NeuralDataType dataType)
        {
            channel = ReadonlyCONST.ConvertToInputChannel(channel);
            if (!_inputChannelDict.ContainsKey(dataType))
            {
                _inputChannelDict[dataType] = new HashSet<int>();
            }
            _inputChannelDict[dataType].Add(channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="dataType"></param>
        public void RemoveChannel(int channel, NeuralDataType dataType)
        {
            if (!_inputChannelDict.ContainsKey(dataType))
            {
                return;
            }

            //
            channel = ReadonlyCONST.ConvertToInputChannel(channel);
            _inputChannelDict[dataType].Remove(channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="floats"></param>
        /// <param name="firstIndex"></param>
        /// <param name="dataType"></param>
 
       public static UInt32 timeS = 2316282/90;
        private void TCPSend(int channel, float[] floats, int len, int srcOffset, long firstIndex, NeuralDataType dataType, string currentPlotBand)
        {
            if (_client == null)
            {
                return;
            }

            try
            {
                // 处理通道名称
                string channelString = $"A-{channel:000}"; // 格式化通道名称
                var bytes = new List<byte>();
                // 时间戳
                System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
                UInt32 timeStamp = (UInt32)(DateTime.Now - startTime).TotalMinutes;
                UInt32 magicNumber = 0x2ef07a08;
                bytes.AddRange(BitConverter.GetBytes(magicNumber));

 
                for (int i = 0; i < len; i++)
                {
                    timeS+=10;
                   // 魔数，用于确认数据包开始
                    // 添加时间戳
                    bytes.AddRange(BitConverter.GetBytes(timeS));

                    // 根据波形频带处理数据
                    float value = floats[i + srcOffset];
                    ushort waveformValue = 0;

                    if (currentPlotBand == "Wide")
                    {
                        waveformValue = BitConverter.ToUInt16(BitConverter.GetBytes(value), 0);
                    }
                    else if (currentPlotBand == "Low")
                    {
                        waveformValue = BitConverter.ToUInt16(BitConverter.GetBytes(value), 2);
                    }
                    else if (currentPlotBand == "High")
                    {
                        waveformValue = BitConverter.ToUInt16(BitConverter.GetBytes(value), 2);
                    }
                 
                     waveformValue = (ushort)(33000 + (waveformValue/100));


                    // 添加波形值
                    bytes.AddRange(BitConverter.GetBytes(waveformValue));
                    bytes.AddRange(BitConverter.GetBytes(waveformValue));
                }

                // 发送数据
                _client.Send(bytes.ToArray());
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(TCPSend)}", ex);
                StopListen();
            }
        }



    }
}

