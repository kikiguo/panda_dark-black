﻿using Accord.MachineLearning.Bayes;
using HandyControl.Collections;
using HDF5.NET;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels.Sockets;
using DotNetty.Handlers.Timeout;
using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.Entity.DataPools;
using StairMed.Enum;
using StairMed.MVVM.Base;
using StairMed.TCPControl.Logger;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Accord;
using System.Threading.Channels;
using StairMed.DataFile.NWB;

namespace StairMed.TCPControl
{
    /// <summary>
    /// TCP远程控制
    /// </summary>
    public class SpikeManager : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(SpikeManager);
        private readonly ServerBootstrap _bootstrap;
        private IChannel _boundChannel;
        private ConcurrentQueue<Chunk> _chunkQueue = new ConcurrentQueue<Chunk>();

        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static SpikeManager Instance = new SpikeManager();
        }
        public static SpikeManager Instance => Helper.Instance;
        private SpikeManager()
        {
            NeuralDataCollector.Instance.NewChunkReceived += Instance_NewChunkReceived;
            _bootstrap = new ServerBootstrap();
            _bootstrap.Group(new MultithreadEventLoopGroup(), new MultithreadEventLoopGroup())
                      .Channel<TcpServerSocketChannel>()
                      .ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
                      {
                          IChannelPipeline pipeline = channel.Pipeline;
                          pipeline.AddLast(new ReadTimeoutHandler(TimeSpan.FromMilliseconds(20))); // 设置超时时间
                          //pipeline.AddLast(new SpikeManagerHandler(this));

                      }));

            StartServerAsync();
        }

        private async Task StartServerAsync()
        {
            try
            {
                _boundChannel = await _bootstrap.BindAsync(StairMedSettings.Instance.SpikePort);
            }
            catch (Exception ex)
            {
                // 处理启动服务器时的异常
                LogTool.Logger.Exp($"{TAG}, StartServerAsync", ex);
            }
        }
        public async Task StopServerAsync()
        {
            if (_boundChannel != null)
            {
                await _boundChannel.CloseAsync();
            }
        }


        #endregion
        public void QueueChunk(Chunk chunk)
        {
            _chunkQueue.Enqueue(chunk);
            ProcessQueue();
        }

        private void ProcessQueue()
        {
            while (_chunkQueue.TryDequeue(out Chunk chunk))
            {
                // Process and send chunk
                Instance_NewChunkReceived(chunk);
            }
        }
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chunk"></param>
        private void Instance_NewChunkReceived(Chunk chunk)
       {

            if (_boundChannel == null)
            {
                return;
            }
            if (!NeuralDataCollector.Instance.IsCollecting)
            {
                return;
            }
           // _transposeFIFO.Add(chunk);
          
          
            //
            if (_asyncHelper == null)
             {
                 _asyncHelper = new BlockFIFO
                 {
                     Name = nameof(SpikeManager),
                     ConsumerAction = (objs) =>
                     {
                       
                         TCPSend((int)objs[0], ((PooledArray<float>)objs[1]).Array, (int)objs[2], (int)objs[3], (long)objs[4], NeuralDataType.Spike);
                     }
                 };
             }

             //
           
                _asyncHelper.Add(1, chunk.Spks, chunk.OneChannelSamples, 1 * chunk.OneChannelSamples, chunk.DataIndex);
                
            
        }


        /// <summary>
        /// 
        /// </summary>
        private bool _listening = false;
        public bool Listening
        {
            get { return _listening; }
            set { Set(ref _listening, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _connected = false;
        public bool Connected
        {
            get { return _connected; }
            set { Set(ref _connected, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private Socket _server = null;
        private Socket _client = null;

        /// <summary>
        /// 
        /// </summary>
        private HashSet<int> _inputChannelHashset = new HashSet<int>();
        private IAsyncFIFO _asyncHelper = null;
       

        /// <summary>
        /// 
        /// </summary>
        public bool StartListen()
        {
            StopListen();

            //
            if (!IPAddress.TryParse(StairMedSettings.Instance.SpikeHost, out IPAddress ip))
            {
                Toast.Error("Host错误");
                return false;
            }

            //
            if (StairMedSettings.Instance.SpikePort < 1 || StairMedSettings.Instance.SpikePort > 65535)
            {
                Toast.Error("Port错误");
                return false;
            }

            //
            try
            {
                //
                var localEP = new IPEndPoint(ip, StairMedSettings.Instance.SpikePort);
                _server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                {
                    ReceiveBufferSize = CONST.SocketBufferSize,
                    SendBufferSize = CONST.SocketBufferSize,
                };
                _server.Bind(localEP);
                _server.Listen(1);

                //
                _server.BeginAccept((IAsyncResult ar) =>
                {
                    if (_server == null)
                    {
                        return;
                    }

                    //
                    try
                    {
                        _client = _server.EndAccept(ar);
                        Connected = true;
                    }
                    catch (Exception ex)
                    {
                        LogTool.Logger.Exp($"{TAG}, EndAccept", ex);
                    }
                }, null);

                //
                Listening = true;
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, Listen", ex);
                Toast.Error($"请检查 Host 和 Port 是否重复或无效");
                _server = null;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void StopListen()
        {
            //
            if (_server != null)
            {
                try
                {
                    _server.Close();
                    _server.Dispose();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, Close", ex);
                }
                _server = null;
            }

            //
            if (_client != null)
            {
                try
                {
                    _client.Shutdown(SocketShutdown.Both);
                    _client.Dispose();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, Shutdown", ex);
                }
                _client = null;
            }

            //
            Listening = false;
            Connected = false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void AddChannel(int channel)
        {
            channel = ReadonlyCONST.ConvertToInputChannel(channel);
            _inputChannelHashset.Add(channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        public void RemoveChannel(int channel)
        {
            channel = ReadonlyCONST.ConvertToInputChannel(channel);
            _inputChannelHashset.Remove(channel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="floats"></param>
        /// <param name="firstIndex"></param>
        /// <param name="dataType"></param>
        //private async Task TCPSend(int channel, float[] floats, int[] triggers, int len, int srcOffset, long firstIndex, NeuralDataType dataType)
        private async Task TCPSend(int channel, float[] floats, int len, int srcOffset, long firstIndex, NeuralDataType dataType)
        {
            if (_client == null || !NeuralDataCollector.Instance.IsCollecting)
            {
                return;
            }
            try
                {
                var channels = new List<int>();
                channels.AddRange(_inputChannelHashset);
                var bytes = new List<byte>();
                long[,] timestamps = NWBFileWriter.GenerateTimestamps(len);
                long firstTimestamp = timestamps[0, 0];
                //UInt32 timeStamp = Convert.ToUInt32(firstTimestamp % 1_000_000_000);
                //UInt32 timeStamp = WaveformManager.timeS;
                UInt32 TCPSpikeMagicNumber = 0x3ae2710f;
                bytes.AddRange(BitConverter.GetBytes(TCPSpikeMagicNumber));
                bytes.AddRange(BitConverter.GetBytes(firstTimestamp));
                bytes.AddRange(BitConverter.GetBytes(len));
                bytes.AddRange(BitConverter.GetBytes(channels.Count));
                for (int c=0;c< channels.Count; c++)
                {
                    string channelName = $"A-{c+1:000}";
                    bytes.AddRange(Encoding.ASCII.GetBytes(channelName));
                    {
                        for (int i = 0; i < len; i++)
                        {
                            var tmp = floats[i + c*len];
                            //var triggerValue = triggers[i + srcOffset];
                            uint singleID = 0;
                            if (tmp > 0)
                                singleID = 1;
                            else
                                singleID = 0;
                            bytes.Add((byte)singleID);
                            //bytes.AddRange(BitConverter.GetBytes(triggerValue)); 
                        }
                    }

                }
                
                // _client.Send(bytes.ToArray());
                await _client.SendAsync(new ArraySegment<byte>(bytes.ToArray()), SocketFlags.None);
                    
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(TCPSend)}", ex);
                    StopListen();
                }
            }

     
    }
}
