﻿using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Event.RemoteTCPControl;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.TCPControl.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace StairMed.TCPControl
{
    /// <summary>
    /// 
    /// </summary>
    public class CmdManager : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private const string TAG = nameof(CmdManager);

        /// <summary>
        /// 
        /// </summary>
        private const int CommandsMaxLength = 1024 * 64;
        private Socket _listener;
        private SocketAsyncEventArgs _acceptEventArgs;

        #region 单例
        private class Helper
        {
            static Helper() { }
            internal static CmdManager Instance = new CmdManager();
        }
        public static CmdManager Instance => Helper.Instance;
        private CmdManager()
        {
            _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _acceptEventArgs = new SocketAsyncEventArgs();
            _acceptEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnAcceptCompleted);
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        private bool _listening = false;
        public bool Listening
        {
            get { return _listening; }
            set { Set(ref _listening, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _connected = false;
        public bool Connected
        {
            get { return _connected; }
            set { Set(ref _connected, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private Socket _server = null;
        private Socket _client = null;

        /// <summary>
        /// 
        /// </summary>
        private string _recvdCommands = string.Empty;
        public string RecvdCommands
        {
            get { return _recvdCommands; }
            set { Set(ref _recvdCommands, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private List<byte> _recvdBuffer = new List<byte>();

        /// <summary>
        /// 
        /// </summary>
        public bool StartListen()
        {
            StopListen();

            //
            if (!IPAddress.TryParse(StairMedSettings.Instance.CommandHost, out IPAddress ip))
            {
                Toast.Error("Host错误");
                return false;
            }

            //
            if (StairMedSettings.Instance.CommandPort < 1 || StairMedSettings.Instance.CommandPort > 65535)
            {
                Toast.Error("Port错误");
                return false;
            }

            //
            try
            {
                //
                var localEP = new IPEndPoint(ip, StairMedSettings.Instance.CommandPort);
                _server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                {
                    ReceiveBufferSize = CONST.SocketBufferSize,
                    SendBufferSize = CONST.SocketBufferSize,
                };
                _server.Bind(localEP);
                _server.Listen(1);

                //
                _server.BeginAccept((IAsyncResult ar) =>
                {
                    if (_server == null)
                    {
                        return;
                    }

                    //
                    try
                    {
                        _client = _server.EndAccept(ar);
                        BeginReceive();
                        Connected = true;
                        RecvdCommands = string.Empty;
                        
                      
                    }
                    catch (Exception ex)
                    {
                        LogTool.Logger.Exp($"{TAG}, EndAccept", ex);
                    }
                }, null);

                //
                Listening = true;
                return true;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, Listen", ex);
                Toast.Error($"请检查 Host 和 Port 是否重复或无效");
                _server = null;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void StopListen()
        {
            //
            if (_server != null)
            {
                try
                {
                    _server.Close();
                    _server.Dispose();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, Close", ex);
                }
                _server = null;
            }

            //
            if (_client != null)
            {
                try
                {
                    _client.Shutdown(SocketShutdown.Both);
                    _client.Dispose();
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, Shutdown", ex);
                }
                _client = null;
            }

            //
            Listening = false;
            Connected = false;

            //
            lock (_recvdBuffer)
            {
                _recvdBuffer.Clear();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BeginReceive()
        {
            const int bufferSize = 1024;
            var buffer = new byte[bufferSize];
            _client.BeginReceive(buffer, 0, bufferSize, SocketFlags.None, ReceiveCallback, buffer);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ar"></param>
        private void ReceiveCallback(IAsyncResult ar)
        {
            if (_client == null)
            {
                return;
            }

            //
            try
            {
                var rx = _client.EndReceive(ar);
                if (rx == 0)
                {
                    StopListen();
                }
                else
                {
                    var buffer = (byte[])ar.AsyncState;
                    lock (_recvdBuffer)
                    {
                        _recvdBuffer.AddRange(buffer.Take(rx));
                        HandleRecvdData();
                    }
                    BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, EndReceive", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void HandleRecvdData()
        {
            var newCmd = Encoding.ASCII.GetString(_recvdBuffer.ToArray());
            HandleCommand(newCmd);

            //
            RecvdCommands = $"{RecvdCommands}{newCmd}{Environment.NewLine}";

            SendData(newCmd);
            if (_recvdCommands.Length > CommandsMaxLength)
            {
                RecvdCommands = _recvdCommands.Substring(_recvdCommands.Length - CommandsMaxLength, CommandsMaxLength);
            }
            _recvdBuffer.Clear();
        }


        private async void SendData(string str)
        {
            if (_client != null && _client.Connected)
            {
                try
                {

                    if (str.Contains("sampleratehertz"))
                    {
                        string sampleRateCommand = "Return: SampleRateHertz 25000";
                        byte[] sampleRateCommandBytes = System.Text.Encoding.ASCII.GetBytes(sampleRateCommand);
                        await _client.SendAsync(new ArraySegment<byte>(sampleRateCommandBytes), SocketFlags.None);

                    }
                    else if (str.Contains("get type"))
                    {
                        string responseType = "Return: Type ControllerRecordUSB2";
                        byte[] responseTypeBytes = System.Text.Encoding.ASCII.GetBytes(responseType);
                        await _client.SendAsync(new ArraySegment<byte>(responseTypeBytes), SocketFlags.None);
                        LogTool.Logger.LogF("Sent controller type command.");

                    }
                   
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(SendData)}", ex);
                }
            }
            else
            {
                LogTool.Logger.LogF("Cannot send data: No connected client.");
            }
        }


        /// <summary>
        /// 执行命令
        /// </summary>
        /// <param name="cmd"></param>
        private void HandleCommand(string cmd)
        {
            try
            {
                cmd = cmd.ToLower();
                var cmdParams = cmd.Split(" ").Where(r => !string.IsNullOrWhiteSpace(r)).ToList();

                //
                if (cmdParams[0].Equals("get"))
                {

                }
                else if (cmdParams[0].Equals("select"))
                {
                    EventHelper.Publish<RemoteTCPControlSelectCommandEvent, List<string>>(cmdParams);
                }
                else if (cmdParams[0].Equals("unselect"))
                {
                    EventHelper.Publish<RemoteTCPControlUnselectCommandEvent, List<string>>(cmdParams);
                }
                else if (cmdParams[0].Equals("set"))
                {
                    switch (cmdParams[1])
                    {
                        case "sampleratehertz":
                            {
                                EventHelper.Publish<RemoteTCPControlSetSampleRateEvent, List<string>>(cmdParams);
                            }
                            break;
                        default:
                            break;
                    }
                }
                else if (cmdParams[0].Equals("execute"))
                {
                    switch (cmdParams[1])
                    {
                        case "runmode":
                            {
                                EventHelper.Publish<RemoteTCPControlRunModeCommandEvent, List<string>>(cmdParams);
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(HandleCommand)}", ex);
            }
        }

        private void StartAccept()
        {
            _acceptEventArgs.AcceptSocket = null;
            if (!_listener.AcceptAsync(_acceptEventArgs))
            {
                ProcessAccept(_acceptEventArgs);
            }
        }

        private void OnAcceptCompleted(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                Socket client = e.AcceptSocket;
                // 处理新的客户端连接...
                // 为每个客户端创建一个新的 SocketAsyncEventArgs 实例以接收数据
                SetupReceiveEventArgs(client);
            }

            StartAccept(); // 继续接受新的连接
        }

        private void SetupReceiveEventArgs(Socket client)
        {
            var receiveEventArgs = new SocketAsyncEventArgs();
            receiveEventArgs.SetBuffer(new byte[1024], 0, 1024);
            receiveEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(OnReceiveCompleted);
            receiveEventArgs.AcceptSocket = client;

            if (!client.ReceiveAsync(receiveEventArgs))
            {
                ProcessReceive(receiveEventArgs);
            }
        }

        private void OnReceiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            ProcessReceive(e);
        }
        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success && e.BytesTransferred > 0)
            {
                // 处理接收到的数据
                string receivedData = Encoding.ASCII.GetString(e.Buffer, e.Offset, e.BytesTransferred);
                // 继续监听更多数据
                if (!e.AcceptSocket.ReceiveAsync(e))
                {
                    ProcessReceive(e);
                }
            }
            else
            {
                CloseClientSocket(e);
            }
        }

        private void CloseClientSocket(SocketAsyncEventArgs e)
        {
            e.AcceptSocket.Close();
            e.Dispose();
        }

    }
}
