﻿using StairMed.Array;

namespace StairMed.Net.Nanomsg.Protocols
{
    public class ReplySocket : NanomsgSocketBase, IBindSocket, ISendSocket, IReceiveSocket
    {
        public ReplySocket()
            : base(Domain.SP, Protocol.REP)
        {
        }

        #region Bind
        public NanomsgEndpoint Bind(string address)
        {
            return BindImpl(address);
        }
        #endregion

        #region Send
        public bool Send(byte[] buffer)
        {
           return SendImpl(buffer);
        }

        public bool SendImmediate(byte[] buffer)
        {
            return SendImmediateImpl(buffer);
        }

        public NanomsgWriteStream CreateSendStream()
        {
            return CreateSendStreamImpl();
        }

        public void SendStream(NanomsgWriteStream stream)
        {
            SendStreamImpl(stream);
        }

        public bool SendStreamImmediate(NanomsgWriteStream stream)
        {
            return SendStreamImmediateImpl(stream);
        }
        #endregion

        #region Receive
        public PooledArray<byte> Receive()
        {
            return ReceiveImpl();
        }

        public PooledArray<byte> ReceiveImmediate()
        {
            return ReceiveImmediateImpl();
        }

        public NanomsgReadStream ReceiveStream()
        {
            return ReceiveStreamImpl();
        }

        public NanomsgReadStream ReceiveStreamImmediate()
        {
            return ReceiveStreamImmediateImpl();
        }
        #endregion
    }
}
