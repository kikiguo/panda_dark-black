﻿using StairMed.Array;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace StairMed.Net.Nanomsg.Protocols
{
    public class PairSocket : NanomsgSocketBase, ISendSocket, IReceiveSocket, IConnectSocket, IBindSocket
    {
        public PairSocket() : base(Domain.SP, Protocol.PAIR) { }

        #region Connect
        public NanomsgEndpoint Connect(string address)
        {
            return ConnectImpl(address);
        }

        public NanomsgEndpoint Connect(IPAddress address, int port)
        {
            return ConnectImpl(address, port);
        }
        #endregion

        #region Bind
        public NanomsgEndpoint Bind(string address)
        {
            return BindImpl(address);
        }
        #endregion

        #region Send

        /// <summary>
        /// 
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public bool SendCommand(uint command, IEnumerable<byte> bytes = null)
        {
            if (bytes == null || bytes.Count() <= 0)
            {
                return Send(BitConverter.GetBytes(command));
            }
            else
            {
                var buffer = new List<byte>();
                buffer.AddRange(BitConverter.GetBytes(command));
                buffer.AddRange(bytes);
                return Send(buffer.ToArray());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public bool Send(byte[] buffer)
        {
            return SendImpl(buffer);
        }

        public bool SendImmediate(byte[] buffer)
        {
            return SendImmediateImpl(buffer);
        }

        public NanomsgWriteStream CreateSendStream()
        {
            return CreateSendStreamImpl();
        }

        public void SendStream(NanomsgWriteStream stream)
        {
            SendStreamImpl(stream);
        }

        public bool SendStreamImmediate(NanomsgWriteStream stream)
        {
            return SendStreamImmediateImpl(stream);
        }
        #endregion

        #region Receive
        public PooledArray<byte> Receive()
        {
            return ReceiveImpl();
        }

        public PooledArray<byte> ReceiveImmediate()
        {
            return ReceiveImmediateImpl();
        }

        public NanomsgReadStream ReceiveStream()
        {
            return ReceiveStreamImpl();
        }

        public NanomsgReadStream ReceiveStreamImmediate()
        {
            return ReceiveStreamImmediateImpl();
        }
        #endregion
    }
}
