﻿using Prism.Ioc;
using Prism.Modularity;

namespace StairMed.MVVM.Base
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ModuleBase : ViewModelBase, IModule
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="containerRegistry"></param>
        public virtual void RegisterTypes(IContainerRegistry containerRegistry) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="containerProvider"></param>
        public virtual void OnInitialized(IContainerProvider containerProvider) { }
    }
}
