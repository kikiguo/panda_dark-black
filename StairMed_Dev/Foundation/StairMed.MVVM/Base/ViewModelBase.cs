﻿using Prism.Mvvm;
using Prism.Navigation;
using System.Runtime.CompilerServices;

namespace StairMed.MVVM.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class ViewModelBase : BindableBase, IDestructible
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual void Destroy()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storage"></param>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        protected virtual bool Set<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            return SetProperty(ref storage, value, propertyName);
        }
    }
}
