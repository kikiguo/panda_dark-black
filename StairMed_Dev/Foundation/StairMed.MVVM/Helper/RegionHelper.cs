﻿using Prism.Regions;

namespace StairMed.MVVM.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class RegionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static IRegionManager RegionManager { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regionName"></param>
        /// <param name="source"></param>
        public static void RequestNavigate(string regionName, string source)
        {
            RegionManager.RequestNavigate(regionName, source);
        }
    }
}
