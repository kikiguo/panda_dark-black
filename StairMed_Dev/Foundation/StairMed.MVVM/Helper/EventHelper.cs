﻿using Prism.Events;
using System;

namespace StairMed.MVVM.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class EventHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static IEventAggregator EventAggregator { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void Publish<T>() where T : PubSubEvent, new()
        {
            EventAggregator.GetEvent<T>().Publish();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="threadOption"></param>
        /// <param name="keepRefAlive"></param>
        /// <returns></returns>
        public static SubscriptionToken Subscribe<T>(Action action, ThreadOption threadOption = ThreadOption.PublisherThread, bool keepRefAlive = true) where T : PubSubEvent, new()
        {
            return EventAggregator.GetEvent<T>().Subscribe(action, threadOption, keepRefAlive);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="subscriber"></param>
        public static void Unsubscribe<T>(Action subscriber) where T : PubSubEvent, new()
        {
            EventAggregator.GetEvent<T>().Unsubscribe(subscriber);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TPayload"></typeparam>
        /// <param name="payload"></param>
        public static void Publish<T, TPayload>(TPayload payload) where T : PubSubEvent<TPayload>, new()
        {
            EventAggregator.GetEvent<T>().Publish(payload);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TPayload"></typeparam>
        /// <param name="action"></param>
        /// <param name="threadOption"></param>
        /// <param name="keepRefAlive"></param>
        /// <returns></returns>
        public static SubscriptionToken Subscribe<T, TPayload>(Action<TPayload> action, ThreadOption threadOption = ThreadOption.PublisherThread, bool keepRefAlive = true, Predicate<TPayload> filter = null) where T : PubSubEvent<TPayload>, new()
        {
            return EventAggregator.GetEvent<T>().Subscribe(action, threadOption, keepRefAlive, filter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="subscriber"></param>
        public static void Unsubscribe<T, TPayload>(Action<TPayload> subscriber) where T : PubSubEvent<TPayload>, new()
        {
            EventAggregator.GetEvent<T>().Unsubscribe(subscriber);
        }
    }
}
