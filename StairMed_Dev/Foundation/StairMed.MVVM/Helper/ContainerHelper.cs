﻿using Prism.Ioc;

namespace StairMed.MVVM.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class ContainerHelper
    {
        public static IContainerExtension Container { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
