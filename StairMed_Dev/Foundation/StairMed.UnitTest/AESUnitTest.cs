﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.Tools;
using System.Collections.Generic;
using System.Text;

namespace StairMed.UnitTest
{
    [TestClass]
    public class AESUnitTest
    {
        [TestMethod]
        public void TestAESCBC()
        {
            var key = Encoding.ASCII.GetBytes("abcdefghijklmnop");
            var iv = Encoding.ASCII.GetBytes("1234567812345678");
            var plaintext = new List<byte> { 0x02, 0x03, 0x75, }.ToArray();

            //
            var ciphertext = AES.AESCBCEncrypt(plaintext, key, iv);
            var target = new byte[] { 0x50, 0x8E, 0xEE, 0xB0, 0x5A, 0x98, 0x37, 0x4D, 0x31, 0x53, 0x98, 0xF9, 0x95, 0xF9, 0x0B, 0xB1 };
            for (int i = 0; i < ciphertext.Length; i++)
            {
                Assert.AreEqual(ciphertext[i], target[i]);
            }            

            //
            var plaintext2 = AES.AESCBCDecrypt(ciphertext, key, iv);
            for (int i = 0; i < plaintext.Length; i++)
            {
                Assert.AreEqual(plaintext[i], plaintext2[i]);
            }
        }
    }
}
