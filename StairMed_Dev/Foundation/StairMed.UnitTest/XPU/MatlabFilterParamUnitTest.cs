﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.FilterProcess.Filters.MatlabFilters.Base;
using StairMed.UnitTest.TestTools;
using System.Collections.Generic;

namespace StairMed.UnitTest.XPU
{
    [TestClass]
    public class MatlabFilterParamUnitTest
    {
        /// <summary>
        /// 
        /// </summary>
        private MatlabFilterParamHelper _matlabFilterParamHelper = new MatlabFilterParamHelper();

        [TestMethod]
        public void MatlabFilterParamTest()
        {
            var sampleRates = new List<int> { 20 * 1000, 25 * 1000, 30 * 1000 };

            //
            foreach (var sampleRate in sampleRates)
            {
                foreach (var filterType in MatlabFilterParamHelper.FilterTypes)
                {
                    foreach (var isHigh in MatlabFilterParamHelper.HighLows)
                    {
                        for (int order = 1; order <= 8; order++)
                        {
                            var cutoffMax = MatlabFilterParamHelper.GetMaxCutoff(sampleRate);
                            var cutoffStep = cutoffMax / 15;
                            var cutoffs = new List<int> { 1, 2, 3, 4, cutoffMax };
                            for (int cutoff = 1; cutoff <= cutoffMax; cutoff += cutoffStep)
                            {
                                cutoffs.Add(cutoff);
                            }

                            //
                            foreach (var cutoff in cutoffs)
                            {
                                _matlabFilterParamHelper.GetMatlabHighLowFilterParams(filterType, order, cutoff, sampleRate, isHigh, out List<double> MyBn, out List<double> MyAn);
                                MatlabFilterParamGenerator.GetMatlabFilterParams(filterType, order, cutoff, sampleRate, isHigh, out List<double> matlabBn, out List<double> matlabAn);

                                //
                                for (int i = 0; i < matlabBn.Count; i++)
                                {
                                    Assert.AreEqual(matlabBn[i], MyBn[i]);
                                    Assert.AreEqual(matlabAn[i], MyAn[i]);
                                }
                            }
                        }
                    }
                }
            }

            //
            for (var sampleRate = MatlabFilterParamHelper.NotchSampleRateBegin; sampleRate <= MatlabFilterParamHelper.NotchSampleRateEnd; sampleRate += 100)
            {
                foreach (var notch in MatlabFilterParamHelper.Notchs)
                {
                    _matlabFilterParamHelper.GetMatlabNotchFilterParam(notch, sampleRate, out List<double> MyBn, out List<double> MyAn);
                    MatlabFilterParamGenerator.GetNotchFilterParams(MatlabFilterParamHelper.NotchOrder, notch, MatlabFilterParamHelper.NotchBandwidth, sampleRate, out List<double> matlabBn, out List<double> matlabAn);

                    //
                    for (int i = 0; i < matlabBn.Count; i++)
                    {
                        Assert.AreEqual(matlabBn[i], MyBn[i]);
                        Assert.AreEqual(matlabAn[i], MyAn[i]);
                    }
                }
            }
        }

    }
}
