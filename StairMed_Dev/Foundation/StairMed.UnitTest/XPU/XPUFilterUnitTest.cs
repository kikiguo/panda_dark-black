using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.ChipConst.Intan;
using StairMed.FilterProcess;
using StairMed.UnitTest.TestTools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace StairMed.UnitTest.XPU
{
    [TestClass]
    public class XPUFilterUnitTest
    {
        /// <summary>
        /// 验证算法中使用matlab滤波算法的正确
        /// </summary>
        [TestMethod]
        public void Filter_High_Low_20k_Cutoff250_Order2_notch50()
        {
            var runDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var fileName = $"20k_cufoff250_order2_notch50";

            //
            var sampleRate = 20000;
            var k = IntanConst.CoefK;
            var offset = IntanConst.CoefB;
            var cutoff = 250;
            var order = 2;
            var notch = 50;

            //
            var adcFile = Path.Combine(runDir, "XPU", $"{fileName}_adcs.csv");

            //
            FilterAndCompare(sampleRate, k, offset, cutoff, order, notch, adcFile,
                Path.Combine(runDir, "XPU", $"{fileName}_bessel_hfp.csv"),
                Path.Combine(runDir, "XPU", $"{fileName}_bessel_lfp.csv"),
                XPUFactory.Instance.GetGPU(),
                FilterType.Bessel);
            FilterAndCompare(sampleRate, k, offset, cutoff, order, notch, adcFile,
                Path.Combine(runDir, "XPU", $"{fileName}_bessel_hfp.csv"),
                Path.Combine(runDir, "XPU", $"{fileName}_bessel_lfp.csv"),
                XPUFactory.Instance.GetCPU(),
                FilterType.Bessel);
            FilterAndCompare(sampleRate, k, offset, cutoff, order, notch, adcFile,
                Path.Combine(runDir, "XPU", $"{fileName}_butter_hfp.csv"),
                Path.Combine(runDir, "XPU", $"{fileName}_butter_lfp.csv"),
                XPUFactory.Instance.GetGPU(),
                FilterType.Butterworth);
            FilterAndCompare(sampleRate, k, offset, cutoff, order, notch, adcFile,
                Path.Combine(runDir, "XPU", $"{fileName}_butter_hfp.csv"),
                Path.Combine(runDir, "XPU", $"{fileName}_butter_lfp.csv"),
                XPUFactory.Instance.GetCPU(),
                FilterType.Butterworth);
        }

        #region

        /// <summary>
        /// 滤波后与预期结果对比
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="k"></param>
        /// <param name="offset"></param>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="notch"></param>
        /// <param name="adcFile"></param>
        /// <param name="targetHighFile"></param>
        /// <param name="targetLowFile"></param>
        /// <param name="xpu"></param>
        /// <param name="filterType"></param>
        private static void FilterAndCompare(int sampleRate, float k, int offset, int cutoff, int order, int notch, string adcFile, string targetHighFile, string targetLowFile, IXPU xpu, FilterType filterType)
        {
            MatlabStairmedFilterCompare.DoFilter(sampleRate, k, offset, cutoff, order, notch, adcFile, xpu, filterType, out List<float> wfps_together, out List<float> hfps_together, out List<float> lfps_together, out List<int> trigs_together);

            //
            CompareFloatArray(hfps_together, LoadFromCSV(targetHighFile));

            //
            CompareFloatArray(lfps_together, LoadFromCSV(targetLowFile));
        }

        /// <summary>
        /// 数组比较
        /// </summary>
        /// <param name="floats"></param>
        /// <param name="targetFloats"></param>
        private static void CompareFloatArray(List<float> floats, List<float> targetFloats)
        {
            for (int i = 0; i < targetFloats.Count; i++)
            {
                Assert.IsTrue(Math.Abs(targetFloats[i] - floats[i]) < 0.001);
            }
        }

        /// <summary>
        /// 从文件加载float数据
        /// </summary>
        /// <returns></returns>
        private static List<float> LoadFromCSV(string csv)
        {
            var floats = new List<float>();

            //
            using (var fs = new FileStream(csv, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(fs, Encoding.UTF8))
                {
                    var line = reader.ReadLine();
                    while (line != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            if (float.TryParse(line.Trim(), out float val))
                            {
                                floats.Add(val);
                            }
                        }
                        line = reader.ReadLine();
                    }
                }
            }

            //
            return floats;
        }

        #endregion


    }
}
