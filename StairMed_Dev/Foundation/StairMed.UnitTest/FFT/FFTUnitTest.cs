﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.Tools;
using System;
using System.Diagnostics;
using StairMed.Extension;
using System.Linq;
using System.Collections.Generic;

namespace StairMed.UnitTest.FFT
{
    [TestClass]
    public class FFTUnitTest
    {
        [TestMethod]
        public void FFTTest()
        {
            var fs = 30 * 1000;

            //
            for (int k = 4; k <= 16; k++)
            {
                var fftSize = (int)Math.Pow(2, k);

                //
                var fft = new FastFourierTransform(fs, fftSize);

                //
                var f = 500;
                var data = new float[fftSize];
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] = (float)(5 * Math.Cos(2 * Math.PI * f * i / fs + Math.PI / 2));
                }

                //
                var fftCount = 10 * fs / 2 / fftSize;
                {
                    var psd = fft.logSqrtPowerSpectralDensity(data);
                }

                //
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                for (int i = 0; i < fftCount; i++)
                {
                    var psd = fft.logSqrtPowerSpectralDensity(data);
                }
                stopwatch.Stop();
                Debug.WriteLine($"fftSize={fftSize}, repeat={fftCount}, total cost {stopwatch.ElapsedTicks} ticks");
            }
        }




        //Debug Trace:
        //fftSize=16, repeat=9375, total cost 41555 ticks
        //fftSize = 32, repeat = 4687, total cost 40014 ticks
        //fftSize = 64, repeat = 2343, total cost 45269 ticks
        //fftSize = 128, repeat = 1171, total cost 43731 ticks
        //fftSize = 256, repeat = 585, total cost 44906 ticks
        //fftSize = 512, repeat = 292, total cost 57013 ticks
        //fftSize = 1024, repeat = 146, total cost 54917 ticks
        //fftSize = 2048, repeat = 73, total cost 60598 ticks
        //fftSize = 4096, repeat = 36, total cost 60810 ticks
        //fftSize = 8192, repeat = 18, total cost 73332 ticks
        //fftSize = 16384, repeat = 9, total cost 72801 ticks
        //fftSize = 32768, repeat = 4, total cost 68682 ticks
        //fftSize = 65536, repeat = 2, total cost 75805 ticks



        [TestMethod]
        public void FFTVerifyTest()
        {
            var fs = 10;
            var fftSize = (int)Math.Pow(2, 6);
            var fft = new FastFourierTransform(fs, fftSize);

            //
            var f = 3;
            var data = new float[fftSize];
            for (int i = 1; i < data.Length + 1; i++)
            {
                data[i - 1] = (float)(1 * Math.Cos(2 * Math.PI * f * i / fs + Math.PI / 2));
            }

            //
            Debug.WriteLine($"data -----------------------------");
            for (int i = 0; i < data.Length; i++)
            {
                Debug.WriteLine(data[i]);
            }
            

            FastFourierTransform.realInputFft(data, (uint)(fftSize >> 1));

            //
            var reals = new float[fftSize >> 1];
            for (int i = 0; i < (fftSize >> 1); i++)
            {
                reals[i] = data[i * 2];
            }
            Debug.WriteLine($"reals -----------------------------");
            for (int i = 0; i < reals.Length; i++)
            {
                Debug.WriteLine(reals[i]);
            }

            //
            var imags = new float[fftSize >> 1];
            for (int i = 0; i < (fftSize >> 1); i++)
            {
                imags[i] = data[i * 2 + 1];
            }
            Debug.WriteLine($"imags -----------------------------");
            for (int i = 0; i < imags.Length; i++)
            {
                Debug.WriteLine(imags[i]);
            }

            //
            var abss = new float[fftSize >> 1];
            for (int i = 0; i < (fftSize >> 1); i++)
            {
                abss[i] = (float)Math.Sqrt(data[i * 2] * data[i * 2] + data[i * 2 + 1] * data[i * 2 + 1]);
            }
            Debug.WriteLine($"abss -----------------------------");
            for (int i = 0; i < abss.Length; i++)
            {
                Debug.WriteLine(abss[i]);
            }

            var ww = 0;
        }









        [TestMethod]
        public void PSDTest()
        {
            var fs = 30 * 1000;
            var fftSize = (int)Math.Pow(2, 8);

            //
            var fft = new FastFourierTransform(fs, fftSize);

            //
            var f = 500;
            var data = new float[fftSize];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (float)(5 * Math.Cos(2 * Math.PI * f * i / fs + Math.PI / 2));
            }

            //
            var psd = fft.logSqrtPowerSpectralDensity(data);

            //
            var target = new List<double> { -1.7044479, -1.4571621, -1.4290096, 0.07450556, 0.6732201, 0.50647223, -0.7019643, -1.8018827, -1.542714, -1.5303216, -1.5608641, -1.6000999, -1.6398531, -1.6778094, -1.7133707, -1.7465029, -1.7773627, -1.8061581, -1.8331035, -1.8583882, -1.8821858, -1.9046502, -1.9259068, -1.9460742, -1.9652466, -1.9835209, -2.0009623, -2.017643, -2.0336108, -2.0489423, -2.0636597, -2.0778165, -2.0914435, -2.1045828, -2.1172607, -2.1295006, -2.1413293, -2.1527748, -2.1638522, -2.174577, -2.1849835, -2.1950607, -2.2048488, -2.2143543, -2.223578, -2.2325423, -2.2412584, -2.249733, -2.2579777, -2.2660053, -2.2738142, -2.2814212, -2.2888205, -2.2960422, -2.3030722, -2.3099208, -2.3165963, -2.3231082, -2.3294523, -2.3356369, -2.3416564, -2.34757, -2.3533065, -2.3589072, -2.3643749, -2.3696942, -2.3748958, -2.379967, -2.3849118, -2.3897405, -2.3944416, -2.3990464, -2.4035032, -2.407865, -2.4121249, -2.4162688, -2.4203055, -2.4242566, -2.4280894, -2.4318333, -2.435481, -2.4390202, -2.4424622, -2.4458253, -2.4490964, -2.452272, -2.4553592, -2.4583552, -2.461268, -2.4640992, -2.4668481, -2.4695177, -2.4721036, -2.4745839, -2.4770055, -2.479342, -2.4816117, -2.4837945, -2.4858994, -2.4879372, -2.4898927, -2.4917865, -2.4935923, -2.4953213, -2.4969895, -2.4985769, -2.5001035, -2.5015328, -2.5029314, -2.504241, -2.5054774, -2.5066447, -2.5077422, -2.5087802, -2.509742, -2.5106394, -2.5114598, -2.5122292, -2.5129333, -2.5135517, -2.514112, -2.5146103, -2.5150392, -2.5153797, -2.515758, -2.5159187, -2.5160928, -2.5162013, -2.8172555 };

            //
            for (int i = 0; i < target.Count; i++)
            {
                Assert.IsTrue(Math.Abs(target[i] - psd[i]) < 0.01);
            }
        }
    }
}
