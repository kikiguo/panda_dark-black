using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.ChipConst;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace StairMed.UnitTest.Impedence
{
    [TestClass]
    public class ImpedenceUnitTest
    {
        [TestMethod]
        public void ImpedenceTest()
        {
            var rundir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

            var sampleRate = 20000;
            var frequency = 1000;

            //
            var waveforms = new List<List<double>>();
            for (int i = 0; i < 3; i++)
            {
                var waveform = new List<double>();

                //
                using (var fs = new FileStream(Path.Combine(rundir, "Impedence", $"waveform{i + 1}.txt"), FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(fs, Encoding.UTF8))
                    {
                        var line = reader.ReadLine();
                        while (line != null)
                        {
                            if (!string.IsNullOrWhiteSpace(line))
                            {
                                if (double.TryParse(line.Trim(), out double val))
                                {
                                    waveform.Add(val);
                                }
                            }
                            line = reader.ReadLine();
                        }
                    }
                }

                //
                waveforms.Add(waveform);
            }

            //
            var test = new IntanImpedenceTest(frequency, sampleRate, 128.0);
            var impedance = test.MeasureComplexAmplitude(waveforms[0].ToArray(), waveforms[1].ToArray(), waveforms[2].ToArray());
            Assert.AreEqual(Math.Floor(impedance.Magnitude), 4940975);
            Assert.AreEqual(Math.Floor(impedance.Phase), -96);
        }
    }
}
