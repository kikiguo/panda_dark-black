﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.FilterProcess.Filters.Intan;
using StairMed.Tools;
using System;
using System.Collections.Generic;

namespace StairMed.UnitTest.TestTools
{
    [TestClass]
    public class IntanStairmedFilterCompare
    {
        [TestMethod]
        public void CompareStairmedIntanFilter()
        {
            var input = new List<int>();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    input.Add(j);
                }
            }

            //
            var order = 2;
            var cutoff = 250;
            var fs = 30 * 1000;
            {
                var filter = new IntanBesselHighpassFilter(order, cutoff, fs);
                var filteredDatas = new List<double>();
                foreach (var data in input)
                {
                    filteredDatas.Add(filter.filterOne(data));
                }
                var strs = new List<string>();
                foreach (var data in filteredDatas)
                {
                    strs.Add($"{data:F5}");
                }
                FileHelper.SaveToFileNoBom("aaaaaaaaaaaaaaaaaaaaaa_bessel_hfp.csv", string.Join(Environment.NewLine, strs), out _);
            }
            {
                var filter = new IntanBesselLowpassFilter(order, cutoff, fs);
                var filteredDatas = new List<double>();
                foreach (var data in input)
                {
                    filteredDatas.Add(filter.filterOne(data));
                }
                var strs = new List<string>();
                foreach (var data in filteredDatas)
                {
                    strs.Add($"{data:F5}");
                }
                FileHelper.SaveToFileNoBom("aaaaaaaaaaaaaaaaaaaaaa_bessel_lfp.csv", string.Join(Environment.NewLine, strs), out _);
            }
            {
                var filter = new IntanButterworthHighpassFilter(order, cutoff, fs);
                var filteredDatas = new List<double>();
                foreach (var data in input)
                {
                    filteredDatas.Add(filter.filterOne(data));
                }
                var strs = new List<string>();
                foreach (var data in filteredDatas)
                {
                    strs.Add($"{data:F5}");
                }
                FileHelper.SaveToFileNoBom("aaaaaaaaaaaaaaaaaaaaaa_butter_hfp.csv", string.Join(Environment.NewLine, strs), out _);
            }
            {
                var filter = new IntanButterworthLowpassFilter(order, cutoff, fs);
                var filteredDatas = new List<double>();
                foreach (var data in input)
                {
                    filteredDatas.Add(filter.filterOne(data));
                }
                var strs = new List<string>();
                foreach (var data in filteredDatas)
                {
                    strs.Add($"{data:F5}");
                }
                FileHelper.SaveToFileNoBom("aaaaaaaaaaaaaaaaaaaaaa_butter_lfp.csv", string.Join(Environment.NewLine, strs), out _);
            }
        }
    }
}
