﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.Array;
using StairMed.ChipConst.Intan;
using StairMed.FilterProcess;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace StairMed.UnitTest.TestTools
{
    [TestClass]
    public class MatlabStairmedFilterCompare
    {
        /// <summary>
        /// 使用matlab提供的数据，进行滤波，然后将结果输出到matlab目录下
        /// </summary>
        [TestMethod]
        public void FilterAndCompareWithMatlab()
        {
            //
            var sampleRate = 20000;
            var cutoff = 250;
            var order = 3;

            //
            var notch = 0;

            //
            var matlabFolder = @"E:\00SVNGIT\01Main\MatlabCode\01Filter\";
            var dstFolder = Path.Combine(matlabFolder, "result_stairmed");

            //
            var adcFile = Path.Combine(matlabFolder, "source_data", $"adcs.csv");

            //
            var k = IntanConst.CoefK;
            var offset = IntanConst.CoefB;

            //
            if (false)
            {
                FilterAndCopyToMatlab(sampleRate, k, offset, cutoff, order, notch, adcFile,
                    Path.Combine(dstFolder, $"wfp.csv"),
                    Path.Combine(dstFolder, $"bessel_hfp.csv"),
                    Path.Combine(dstFolder, $"bessel_lfp.csv"),
                    XPUFactory.Instance.GetCPU(),
                    FilterType.Bessel);
                FilterAndCopyToMatlab(sampleRate, k, offset, cutoff, order, notch, adcFile,
                    Path.Combine(dstFolder, $"wfp.csv"),
                    Path.Combine(dstFolder, $"butter_hfp.csv"),
                    Path.Combine(dstFolder, $"butter_lfp.csv"),
                    XPUFactory.Instance.GetCPU(),
                    FilterType.Butterworth);
            }
            else
            {
                FilterAndCopyToMatlab(sampleRate, k, offset, cutoff, order, notch, adcFile,
                    Path.Combine(dstFolder, $"wfp.csv"),
                    Path.Combine(dstFolder, $"bessel_hfp.csv"),
                    Path.Combine(dstFolder, $"bessel_lfp.csv"),
                    XPUFactory.Instance.GetGPU(),
                    FilterType.Bessel);
                FilterAndCopyToMatlab(sampleRate, k, offset, cutoff, order, notch, adcFile,
                    Path.Combine(dstFolder, $"wfp.csv"),
                    Path.Combine(dstFolder, $"butter_hfp.csv"),
                    Path.Combine(dstFolder, $"butter_lfp.csv"),
                    XPUFactory.Instance.GetGPU(),
                    FilterType.Butterworth);
            }
        }




        #region


        /// <summary>
        /// 滤波后将结果复制到matlab目录下
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="k"></param>
        /// <param name="offset"></param>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="notch"></param>
        /// <param name="adcFile"></param>
        /// <param name="dstHighFile"></param>
        /// <param name="dstLowFile"></param>
        /// <param name="xpu"></param>
        /// <param name="filterType"></param>
        private static void FilterAndCopyToMatlab(int sampleRate, float k, int offset, int cutoff, int order, int notch, string adcFile, string dstNotchFile, string dstHighFile, string dstLowFile, IXPU xpu, FilterType filterType)
        {
            DoFilter(sampleRate, k, offset, cutoff, order, notch, adcFile, xpu, filterType, out List<float> wfps_together, out List<float> hfps_together, out List<float> lfps_together, out List<int> trigs_together);

            //
            FileHelper.SaveToFileNoBom(dstNotchFile, string.Join(Environment.NewLine, wfps_together), out _);
            FileHelper.SaveToFileNoBom(dstHighFile, string.Join(Environment.NewLine, hfps_together), out _);
            FileHelper.SaveToFileNoBom(dstLowFile, string.Join(Environment.NewLine, lfps_together), out _);
        }


        /// <summary>
        /// 滤波
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="k"></param>
        /// <param name="offset"></param>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="notch"></param>
        /// <param name="adcFile"></param>
        /// <param name="xpu"></param>
        /// <param name="filterType"></param>
        /// <param name="hfps_together"></param>
        /// <param name="lfps_together"></param>
        public static void DoFilter(int sampleRate, float k, int offset, int cutoff, int order, int notch, string adcFile, IXPU xpu, FilterType filterType, out List<float> wfps_together, out List<float> hfps_together, out List<float> lfps_together, out List<int> trigs_together)
        {
            //
            var adcs = LoadADCs(adcFile);

            //
            var sampleCount = adcs.Count;
            var splitPartCount = 128;
            var splitPartLen = sampleCount / splitPartCount;
            while (sampleCount % splitPartCount != 0)
            {
                splitPartCount--;
                splitPartLen = sampleCount / splitPartCount;
            }

            //
            xpu.Free();
            xpu.ParamCenter.SetRawKB(k, offset);
            xpu.ParamCenter.SetCommonParam(1, splitPartLen, sampleRate);
            xpu.ParamCenter.SetHighPassFilter(cutoff, order, filterType);
            xpu.ParamCenter.SetLowPassFilter(cutoff, order, filterType);
            xpu.ParamCenter.SetNotchFilter(notch, 10);
            xpu.ParamCenter.SetTRIGParam(0, 0);

            //
            wfps_together = new List<float>();
            hfps_together = new List<float>();
            lfps_together = new List<float>();
            trigs_together = new List<int>();

            //
            for (int i = 0; i < splitPartCount; i++)
            {
                var byteChunk = new List<byte>(2 * splitPartLen);
                var indexOffset = splitPartLen * i;
                for (var j = 0; j < splitPartLen; j++)
                {
                    byteChunk.AddRange(BitConverter.GetBytes(adcs[j + indexOffset]));
                }

                //
                xpu.ProcessByteADChunk(0, byteChunk.ToArray(), out PooledArray<float> raws, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks,out PooledArray<int> trigs);
                wfps_together.AddRange(wfps.Array.Take(splitPartLen));
                hfps_together.AddRange(hfps.Array.Take(splitPartLen));
                lfps_together.AddRange(lfps.Array.Take(splitPartLen));
                trigs_together.AddRange(trigs.Array.Take(splitPartLen));
            }
        }


        /// <summary>
        /// 加载adc数据
        /// </summary>
        /// <param name="adcFile"></param>
        /// <returns></returns>
        private static List<ushort> LoadADCs(string adcFile)
        {
            var adcs = new List<ushort>();
            using (var fs = new FileStream(adcFile, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(fs, Encoding.UTF8))
                {
                    var line = reader.ReadLine();
                    while (line != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            if (double.TryParse(line.Trim(), out double val))
                            {
                                adcs.Add(Convert.ToUInt16(val));
                            }
                        }
                        line = reader.ReadLine();
                    }
                }
            }

            return adcs;
        }


        #endregion
    }
}
