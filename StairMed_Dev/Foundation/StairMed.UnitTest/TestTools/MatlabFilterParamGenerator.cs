﻿using MathWorks.MATLAB.NET.Arrays;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.FilterProcess;
using StairMed.FilterProcess.Filters.MatlabFilters.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;


namespace StairMed.UnitTest.TestTools
{
    [TestClass]
    public class MatlabFilterParamGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        private static readonly MatlabLib.FilterHelper _filterHelper = new MatlabLib.FilterHelper();

        [TestMethod]
        public void GenerateMatlabFilterParamFile()
        {
            var sampleRates = new List<int> { 20 * 1000, 25 * 1000, 30 * 1000 };

            //
            GenerateMatlabNotchFilterParams($"{MatlabFilterParamHelper.FilterParamFolder}\\{MatlabFilterParamHelper.NotchFilterParamFileName}.dll");
            foreach (var sampleRate in sampleRates)
            {
                GenerateMatlabHighLowFilterParams($"{MatlabFilterParamHelper.FilterParamFolder}\\{MatlabFilterParamHelper.FilterParamFilePrefix}{sampleRate / 1000}k.dll", sampleRate);
            }
        }


        [TestMethod]
        public void MatlabFilterFsCutoffMultipleTest()
        {
            //注意：这里得到的结果不太一致:
            for (int fs = 1000; fs <= 30000; fs += 1000)
            {
                for (int i = 1; i <= 2; i++)
                {
                    var cutoff = (fs / 2 - 1) * i;
                    var sampleRate = fs * i;
                    GetMatlabFilterParams(FilterType.Bessel, 3, cutoff, sampleRate, true, out List<double> matlabBn, out List<double> matlabAn);
                    Debug.WriteLine($"Matlab: fs=[{fs:D5}], sampleRate={sampleRate:D5}, cutoff={cutoff:D5}, Bn: {string.Join(", ", matlabBn)} An:{string.Join(", ", matlabAn)}");
                }
                Debug.WriteLine("--------------");
            }
        }







        #region

        /// <summary>
        /// 生成 Matlab 高/低通 滤波参数文件
        /// </summary>
        /// <param name="filePath"></param>
        private void GenerateMatlabHighLowFilterParams(string filePath, int sampleRate)
        {
            if (File.Exists(filePath))
            {
                //File.Delete(filePath);
                return;
            }

            //
            var dir = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            //
            using (var fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write))
            {
                using (var writer = new BinaryWriter(fs, Encoding.ASCII))
                {
                    writer.Write(MatlabFilterParamHelper.WaterMark.ToCharArray());
                    writer.Write(MatlabFilterParamHelper.MagicNum);
                    writer.Write(sampleRate);

                    //
                    var cutoffMax = MatlabFilterParamHelper.GetMaxCutoff(sampleRate);
                    foreach (var filterType in MatlabFilterParamHelper.FilterTypes)
                    {
                        for (int cutoff = 1; cutoff <= cutoffMax; cutoff++)
                        {
                            for (int order = 1; order <= 8; order++)
                            {
                                foreach (var isHigh in MatlabFilterParamHelper.HighLows)
                                {
                                    GetMatlabFilterParams(filterType, order, cutoff, sampleRate, isHigh, out List<double> Bn, out List<double> An);

                                    //
                                    for (int i = 0; i < Bn.Count; i++)
                                    {
                                        writer.Write(Bn[i]);
                                        writer.Write(An[i]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 生成 Matlab 带阻 滤波参数文件
        /// </summary>
        /// <param name="filePath"></param>
        private void GenerateMatlabNotchFilterParams(string filePath)
        {
            if (File.Exists(filePath))
            {
                //File.Delete(filePath);
                return;
            }

            //
            var dir = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            //
            using (var fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write))
            {
                using (var writer = new BinaryWriter(fs, Encoding.ASCII))
                {
                    writer.Write(MatlabFilterParamHelper.WaterMark.ToCharArray());
                    writer.Write(MatlabFilterParamHelper.MagicNum);

                    //
                    for (var sampleRate = MatlabFilterParamHelper.NotchSampleRateBegin; sampleRate <= MatlabFilterParamHelper.NotchSampleRateEnd; sampleRate++)
                    {
                        foreach (var notch in MatlabFilterParamHelper.Notchs)
                        {
                            GetNotchFilterParams(MatlabFilterParamHelper.NotchOrder, notch, MatlabFilterParamHelper.NotchBandwidth, sampleRate, out List<double> Bn, out List<double> An);
                            for (int i = 0; i < Bn.Count; i++)
                            {
                                writer.Write(Bn[i]);
                                writer.Write(An[i]);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 获得 butterworth / bessel 滤波器的参数
        /// </summary>
        /// <param name="filterType"></param>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        /// <param name="high"></param>
        /// <param name="Bn"></param>
        /// <param name="An"></param>
        public static void GetMatlabFilterParams(FilterType filterType, int order, int cutoff, int sampleRate, bool high, out List<double> Bn, out List<double> An)
        {
            Bn = new List<double>();
            An = new List<double>();

            //
            var coefArray = _filterHelper.filter_coef(2, sampleRate, $"{filterType.ToString().ToLower()}", high ? "high" : "low", cutoff, order);

            //
            var types = new Type[] { typeof(double[]), typeof(double[]) };

            //
            var datas = MWArray.ConvertToNativeTypes(coefArray, types);

            //
            var bsData = (double[])datas[0];
            for (int i = 0; i < bsData.Length; i++)
            {
                Bn.Add(bsData[i]);
            }

            //
            var asData = (double[])datas[1];
            for (int i = 0; i < asData.Length; i++)
            {
                An.Add(asData[i]);
            }
        }

        /// <summary>
        /// 获取 notch 滤波器参数
        /// </summary>
        /// <param name="filterType"></param>
        /// <param name="order"></param>
        /// <param name="cutoff"></param>
        /// <param name="sampleRate"></param>
        /// <param name="high"></param>
        /// <param name="Bn"></param>
        /// <param name="An"></param>
        public static void GetNotchFilterParams(int order, int notch, int notchBandwodth, int sampleRate, out List<double> Bn, out List<double> An)
        {
            Bn = new List<double>();
            An = new List<double>();

            //
            var coefArray = _filterHelper.notch_coef(2, sampleRate, notch, notchBandwodth, order);

            //
            var types = new Type[] { typeof(double[]), typeof(double[]) };

            //
            var datas = MWArray.ConvertToNativeTypes(coefArray, types);

            //
            var bsData = (double[])datas[0];
            for (int i = 0; i < bsData.Length; i++)
            {
                Bn.Add(bsData[i]);
            }

            //
            var asData = (double[])datas[1];
            for (int i = 0; i < asData.Length; i++)
            {
                An.Add(asData[i]);
            }
        }

        #endregion
    }
}
