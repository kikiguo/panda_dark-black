﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;


namespace StairMed.UnitTest.TestTools
{
    [TestClass]
    public class CUDAMatlabKernelGenerator
    {
        [TestMethod]
        public void GenerateCUDAMatlabKernel()
        {
            try
            {
                for (int i = 1; i <= 8; i++)
                {
                    CreateMatlabFilterKernel(i);
                }
            }
            catch { }
            Console.WriteLine($"Done..............");

            try
            {
                Process.Start("explorer.exe", Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName));
            }
            catch { }
        }

        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="order"></param>
        private static void CreateMatlabFilterKernel(int order)
        {
            var func = $"__global__ void MatlabFilterOrder{order}Kernel(int channels, int oneChannelSamples, const float* srcs, double* prevs, const double* filter, float* fps)";
            var cuh = $"MatlabFilterOrder{order}.cuh";
            var cu = $"MatlabFilterOrder{order}.cu";

            //
            var cuLines = CreateCuLines(order, func, cuh);
            FileHelper.SaveToFileNoBom(Path.Combine("Kernel", cu), string.Join(Environment.NewLine, cuLines), out _);

            //
            var cuhLines = CreateCuhLines(func);
            FileHelper.SaveToFileNoBom(Path.Combine("Kernel", cuh), string.Join(Environment.NewLine, cuhLines), out _);
        }

        /// <summary>
        /// 创建头 .cuh 文件
        /// </summary>
        /// <param name="order"></param>
        /// <param name="func"></param>
        /// <param name="cuh"></param>
        /// <returns></returns>
        private static List<string> CreateCuhLines(string func)
        {
            var lines = new List<string>();
            lines.Add($"#pragma once");
            lines.Add($"");
            lines.Add($"#include \"cuda_runtime.h\"");
            lines.Add($"#include \"device_launch_parameters.h\"");
            lines.Add($"");
            lines.Add($"{func};");
            lines.Add($"");
            lines.Add($"");
            lines.Add($"");
            lines.Add($"");


            //
            return lines;
        }

        /// <summary>
        /// 创建 .cu 文件
        /// </summary>
        /// <param name="order"></param>
        /// <param name="func"></param>
        /// <param name="cuh"></param>
        /// <returns></returns>
        private static List<string> CreateCuLines(int order, string func, string cuh)
        {
            var lines = new List<string>();
            lines.Add($"#include \"{cuh}\"");
            lines.Add($"");

            //
            var TAG = $"FilterOrder{order}";
            lines.Add($"#define {TAG} \\");
            lines.Add($" \\");
            lines.Add($"inVal = srcs[idx]; \\");
            lines.Add($"outVal = b0 * inVal; \\");
            lines.Add($" \\");
            for (int i = 0; i < order; i++)
            {
                lines.Add($"outVal += b{i + 1} * preX{i}; \\");
            }
            for (int i = 0; i < order; i++)
            {
                lines.Add($"outVal -= a{i + 1} * preY{i}; \\");
            }
            lines.Add($" \\");
            for (int i = order - 1; i >= 1; i--)
            {
                lines.Add($"preX{i} = preX{i - 1}; \\");
            }
            lines.Add($"preX0 = inVal; \\");
            for (int i = order - 1; i >= 1; i--)
            {
                lines.Add($"preY{i} = preY{i - 1}; \\");
            }
            lines.Add($"preY0 = outVal; \\");
            lines.Add($" \\");
            lines.Add($"fps[idx] = outVal; \\");
            lines.Add($"");

            //
            lines.Add($"//filter");
            lines.Add($"{func}");
            lines.Add($"{{");
            lines.Add($"	int channel = threadIdx.x + blockDim.x * blockIdx.x;");
            lines.Add($"");
            lines.Add($"	//");
            lines.Add($"	if (channel < channels)");
            lines.Add($"	{{");
            {
                var offset = 0;
                for (int i = 0; i < order + 1; i++)
                {
                    lines.Add($"		double b{i} = filter[{offset++}];");
                }
                for (int i = 0; i < order + 1; i++)
                {
                    if (i == 0)
                    {
                        lines.Add($"		//double a{i} = filter[{offset++}];");
                    }
                    else
                    {
                        lines.Add($"		double a{i} = filter[{offset++}];");
                    }
                }
            }
            lines.Add($"");
            {
                var offset = 0;
                for (int i = 0; i < order; i++)
                {
                    lines.Add($"		double preX{i} = prevs[channel + {offset++} * channels];");
                }
                for (int i = 0; i < order; i++)
                {
                    lines.Add($"		double preY{i} = prevs[channel + {offset++} * channels];");
                }
            }
            lines.Add($"");
            lines.Add($"		//loop");
            lines.Add($"		{{");
            lines.Add($"			float inVal = 0;");
            lines.Add($"			double outVal = 0;");
            lines.Add($"");
            lines.Add($"			if ((oneChannelSamples & 0x1F) == 0)");
            lines.Add($"			{{");
            lines.Add($"				int repeat = oneChannelSamples >> 5;");
            lines.Add($"				int idx = channel;");
            lines.Add($"");
            lines.Add($"				//");
            lines.Add($"				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)");
            lines.Add($"				{{");
            {
                for (int i = 0; i < 32; i++)
                {
                    lines.Add($"					{TAG}; idx += channels;	//{i + 1}");
                }
            }
            lines.Add($"				}}");
            lines.Add($"			}}");
            lines.Add($"			else");
            lines.Add($"			{{");
            lines.Add($"				//");
            lines.Add($"				int repeat = oneChannelSamples;");
            lines.Add($"				int idx = channel;");
            lines.Add($"");
            lines.Add($"				//");
            lines.Add($"				for (int repeatIndex = 0; repeatIndex < repeat; ++repeatIndex)");
            lines.Add($"				{{");
            lines.Add($"					{TAG}; idx += channels;");
            lines.Add($"				}}");
            lines.Add($"			}}");
            lines.Add($"		}}");
            lines.Add($"");
            lines.Add($"		//");
            {
                var offset = 0;
                for (int i = 0; i < order; i++)
                {
                    lines.Add($"		prevs[channel + {offset++} * channels] = preX{i};");
                }
                for (int i = 0; i < order; i++)
                {
                    lines.Add($"		prevs[channel + {offset++} * channels] = preY{i};");
                }
            }
            lines.Add($"	}}");
            lines.Add($"}}");
            lines.Add($"");
            lines.Add($"");
            lines.Add($"");
            lines.Add($"");
            lines.Add($"");
            lines.Add($"");

            //
            return lines;
        }

    }
}
