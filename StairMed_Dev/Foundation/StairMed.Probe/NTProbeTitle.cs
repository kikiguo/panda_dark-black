﻿using System.Xml.Serialization;

namespace StairMed.Probe
{
    /// <summary>
    /// 
    /// </summary>
    public class NTProbeTitle
    {
        [XmlAttribute]
        public string Text { get; set; }

        [XmlAttribute]
        public int X { get; set; }

        [XmlAttribute]
        public int Y { get; set; }
    }
}
