﻿using System.Xml.Serialization;

namespace StairMed.Probe
{
    /// <summary>
    /// 
    /// </summary>
    public class NTLine
    {
        [XmlAttribute]
        public int X1 { get; set; }

        [XmlAttribute]
        public int X2 { get; set; }

        [XmlAttribute]
        public int Y1 { get; set; }

        [XmlAttribute]
        public int Y2 { get; set; }
    }
}
