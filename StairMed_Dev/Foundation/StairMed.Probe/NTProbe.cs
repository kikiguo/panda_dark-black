﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace StairMed.Probe
{
    /// <summary>
    /// 单根电极
    /// </summary>
    public class NTProbe
    {
        /// <summary>
        /// 外观
        /// </summary>
        public List<NTLine> Shape { get; set; }

        /// <summary>
        /// 命名
        /// </summary>
        public NTProbeTitle Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<NTPad> Pads { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int PadWidth { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlAttribute]
        public int PadHeight { get; set; }
    }
}
