﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace StairMed.Probe
{
    /// <summary>
    /// 
    /// </summary>
    public class NTProbeMap
    {
        /// <summary>
        /// 版本类型
        /// </summary>
        [XmlAttribute]
        public string Type { get; set; }

        /// <summary>
        /// 通道个数
        /// </summary>
        [XmlAttribute]
        public int ChannelCount { get; set; }

        /// <summary>
        /// 电极尖端们
        /// </summary>
        public List<NTProbe> Probes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetUniqueId()
        {
            try
            {
                var builder = new StringBuilder();
                builder.Append(ChannelCount.ToString());
                foreach (var electrode in Probes)
                {
                    builder.Append(electrode.Title.Text);
                    foreach (var pad in electrode.Pads)
                    {
                        builder.Append(pad.Channel.ToString());
                    }
                }
                return builder.ToString().GetHashCode().ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsInValid()
        {
            if (ChannelCount == 0)
            {
                return true;
            }

            //
            if (Probes == null)
            {
                return true;
            }

            //
            if (Probes.Count <= 0)
            {
                return true;
            }

            //
            foreach (var probe in Probes)
            {
                if (probe.Pads == null || probe.Pads.Count <= 0)
                {
                    return true;
                }
            }

            //
            int probeChannel = Probes[0].Pads.Count;
            for (int i = 0; i < Probes.Count; i++)
            {
                if (Probes[i].Pads.Count != probeChannel)
                {
                    return true;
                }
            }

            //
            if (probeChannel * Probes.Count != ChannelCount)
            {
                return true;
            }

            //
            var hashset = new HashSet<int>();
            foreach (var probe in Probes)
            {
                foreach (var pad in probe.Pads)
                {
                    hashset.Add(pad.Channel);
                    if (pad.Channel < 0 || pad.Channel >= ChannelCount)
                    {
                        return true;
                    }
                }
            }

            //
            if (hashset.Count != ChannelCount)
            {
                return true;
            }

            //
            return false;
        }

        #region 

        /// <summary>
        /// 电极通道的映射关系及显示的先后顺序
        /// </summary>
        private readonly object _locker = new object();
        private List<int> _electrodeChannels = null;
        private void InitElectrodeChannels()
        {
            if (_electrodeChannels == null)
            {
                lock (_locker)
                {
                    if (_electrodeChannels == null)
                    {
                        var probeChannelOrder = new List<int>();
                        foreach (var electrode in Probes)
                        {
                            foreach (var pad in electrode.Pads)
                            {
                                probeChannelOrder.Add(pad.Channel);
                            }
                        }
                        _electrodeChannels = probeChannelOrder;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, int> _real2VirtualDict = new Dictionary<int, int>();
        public int GetPhysicsIndex(int inputIndex)
        {
            lock (_real2VirtualDict)
            {
                if (!_real2VirtualDict.ContainsKey(inputIndex))
                {
                    InitElectrodeChannels();
                    _real2VirtualDict[inputIndex] = inputIndex / ChannelCount * ChannelCount + _electrodeChannels.IndexOf(inputIndex % ChannelCount);
                }

                //
                return _real2VirtualDict[inputIndex];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<int, int> _virtual2RealDict = new Dictionary<int, int>();
        public int GetInputIndex(int physicsIndex)
        {
            lock (_virtual2RealDict)
            {
                if (!_virtual2RealDict.ContainsKey(physicsIndex))
                {
                    InitElectrodeChannels();
                    _virtual2RealDict[physicsIndex] = physicsIndex / ChannelCount * ChannelCount + _electrodeChannels[physicsIndex % ChannelCount];
                }

                //
                return _virtual2RealDict[physicsIndex];
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="probeChannelCount"></param>
        /// <param name="probeCount"></param>
        /// <returns></returns>
        public static NTProbeMap GetDefault(int probeChannelCount = 32, int probeCount = 4)
        {
            probeChannelCount = (probeChannelCount >> 1) << 1;
            return GetProbeMap((probeIndex, padIndex) =>
            {
                return probeChannelCount * probeIndex + padIndex;
            }, probeChannelCount, probeCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="probeChannelCount"></param>
        /// <param name="probeCount"></param>
        /// <returns></returns>
        private static NTProbeMap GetRandom(int probeChannelCount = 32, int probeCount = 4)
        {
            probeChannelCount = (probeChannelCount >> 1) << 1;

            //
            var rand = new Random(DateTime.Now.Millisecond);

            //
            return GetProbeMap((probeIndex, padIndex) =>
            {
                return rand.Next(0, probeChannelCount * probeCount);
            }, probeChannelCount, probeCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static NTProbeMap GetSS64Random(int probeChannelCount = 32, int probeCount = 4)
        {
            probeChannelCount = (probeChannelCount >> 1) << 1;

            //
            var rand = new Random(DateTime.Now.Millisecond);

            //
            return GetProbeMap((probeIndex, padIndex) =>
            {
                var randBegin = probeIndex < 2 ? 0 : 64;
                var randEnd = probeIndex < 2 ? 64 : 128;
                return rand.Next(randBegin, randEnd);
            }, probeChannelCount, probeCount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GetChannel"></param>
        /// <param name="probeChannelCount"></param>
        /// <param name="probeCount"></param>
        /// <returns></returns>
        private static NTProbeMap GetProbeMap(Func<int, int, int> GetChannel, int probeChannelCount = 32, int probeCount = 4)
        {
            //
            const int PadSize = 10;
            const int PadGap = 3;

            //
            const int BodyWidth = 3 * PadGap + 2 * PadSize;

            //
            const int CapWidth = BodyWidth + PadSize * 3;
            const int CapLeft = PadGap;

            //
            const int CapDiagonalTop = PadSize + PadGap;
            const int CapDiagonalBottom = PadSize * 2 + PadGap;

            //
            const int ProbeWidth = CapWidth + 2 * CapLeft;

            //

            const int BodyLeft = CapLeft + (CapWidth - BodyWidth) / 2;
            const int BodyRight = ProbeWidth - BodyLeft;

            //
            int Rows = probeChannelCount / 2;

            //
            int PeakDiagonalTop = CapDiagonalBottom + Rows * (PadSize + PadGap) + PadSize * 3;
            int PeakDiagonalBottom = PeakDiagonalTop + PadSize * 2 + PadGap;

            //
            int BodyHeight = PeakDiagonalTop - CapDiagonalBottom;
            int Yoffset = (BodyHeight - Rows * PadSize - (Rows - 1) * PadGap) / 2;

            //
            var namedChannel = new HashSet<int>();
            var probes = new List<NTProbe>();
            for (int probeIndex = 0; probeIndex < probeCount; probeIndex++)
            {
                //
                var pads = new List<NTPad>();
                for (int padIndex = 0; padIndex < probeChannelCount; padIndex++)
                {
                    var outpad = GetChannel(probeIndex, padIndex);
                    while (namedChannel.Contains(outpad))
                    {
                        outpad = GetChannel(probeIndex, padIndex);
                    }

                    //
                    namedChannel.Add(outpad);
                    pads.Add(new NTPad
                    {
                        X = ProbeWidth * probeIndex + BodyLeft + (padIndex % 2 == 0 ? PadGap + PadSize / 2 : 2 * PadGap + PadSize + PadSize / 2),
                        Y = Yoffset + (probeChannelCount - padIndex) / 2 * (PadSize + PadGap) + PadSize / 2 + CapDiagonalBottom + ((probeChannelCount - padIndex) % 2 == 0 ? 0 : PadSize / 2),
                        Channel = outpad,
                    });
                }

                var xoffset = ProbeWidth * probeIndex;

                //
                var probe = new NTProbe
                {
                    PadWidth = PadSize,
                    PadHeight = PadSize,
                    Pads = pads,
                    Title = new NTProbeTitle
                    {
                        Text = ((char)('1' + probeIndex)).ToString(),
                        X = xoffset + ProbeWidth / 2,
                        Y = CapDiagonalBottom / 2,
                    },
                    Shape = new List<NTLine>
                    {
                        //帽左竖
                        new NTLine { X1 = xoffset + CapLeft, X2 = xoffset + CapLeft, Y1 = 0, Y2 = CapDiagonalTop },
                        //帽左斜
                        new NTLine { X1 = xoffset + CapLeft, X2 = xoffset + BodyLeft, Y1 = CapDiagonalTop, Y2 = CapDiagonalBottom },
                        //躯干左竖
                        new NTLine { X1 = xoffset + BodyLeft, X2 = xoffset + BodyLeft, Y1 = CapDiagonalBottom, Y2 = PeakDiagonalTop },
                        //斜入尖
                        new NTLine { X1 = xoffset + BodyLeft, X2 = xoffset + ProbeWidth / 2, Y1 = PeakDiagonalTop, Y2 = PeakDiagonalBottom },
                        //斜出尖
                        new NTLine { X1 = xoffset + ProbeWidth / 2, X2 = xoffset + BodyRight, Y1 = PeakDiagonalBottom, Y2 = PeakDiagonalTop },
                        //躯干右竖
                        new NTLine { X1 = xoffset + BodyRight, X2 = xoffset + BodyRight, Y1 = PeakDiagonalTop, Y2 = CapDiagonalBottom },
                        //躯干右斜
                        new NTLine { X1 = xoffset + BodyRight, X2 = xoffset + ProbeWidth - (ProbeWidth-CapWidth)/2, Y1 = CapDiagonalBottom, Y2 = CapDiagonalTop },
                        //帽右竖
                        new NTLine { X1 = xoffset + ProbeWidth - (ProbeWidth-CapWidth)/2, X2 = xoffset + ProbeWidth - (ProbeWidth-CapWidth)/2, Y1 = CapDiagonalTop, Y2 = 0 },
                        //帽顶
                        new NTLine { X1 = xoffset + ProbeWidth - (ProbeWidth-CapWidth)/2, X2 = xoffset + CapLeft, Y1 = 0, Y2 = 0 },
                    }
                };

                //
                probes.Add(probe);
            }

            //
            return new NTProbeMap()
            {
                Type = $"HNEC-{probeCount * probeChannelCount}-{probeCount}",
                ChannelCount = probeCount * probeChannelCount,
                Probes = probes,
            };
        }


        /// <summary>
        /// 
        /// </summary>
        public static void CreateDefaultProbeMaps()
        {
            SaveToFile("staimed_probeMap_32channel_16X2.xml", GetDefault(16, 2));
            SaveToFile("staimed_probeMap_32channel_16X2_rand.xml", GetRandom(16, 2));

            //
            SaveToFile("staimed_probeMap_32channel_32X1.xml", GetDefault(32, 1));
            SaveToFile("staimed_probeMap_32channel_32X1_rand.xml", GetRandom(32, 1));

            //
            SaveToFile("staimed_probeMap_64channel_32X2.xml", GetDefault(32, 2));
            SaveToFile("staimed_probeMap_64channel_32X2_rand.xml", GetRandom(32, 2));

            //
            SaveToFile("staimed_probeMap_128channel_32X4.xml", GetDefault(32, 4));
            SaveToFile("staimed_probeMap_128channel_32X4_rand.xml", GetRandom(32, 4));

            //
            SaveToFile("staimed_probeMap_128channel_32X4_ss64.xml", GetSS64Random(32, 4));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="probeMap"></param>
        public static void SaveToFile(string file, NTProbeMap probeMap)
        {
            using (var fs = new FileStream(file, FileMode.Create, FileAccess.ReadWrite))
            {
                using (var writer = new StreamWriter(fs, Encoding.UTF8))
                {
                    new XmlSerializer(probeMap.GetType()).Serialize(writer, probeMap);
                }
            }
        }
    }
}