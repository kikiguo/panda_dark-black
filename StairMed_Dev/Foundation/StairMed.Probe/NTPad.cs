﻿using System.Xml.Serialization;

namespace StairMed.Probe
{
    /// <summary>
    /// 焊盘
    /// </summary>
    public class NTPad
    {
        [XmlAttribute]
        public int Channel { get; set; }

        [XmlAttribute]
        public int X { get; set; }

        [XmlAttribute]
        public int Y { get; set; }
    }
}
