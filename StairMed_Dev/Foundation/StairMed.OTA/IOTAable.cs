﻿namespace StairMed.OTA
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOTAable
    {
        /// <summary>
        /// 检查是否就绪
        /// </summary>
        /// <returns></returns>
        bool IsReadyToUpgrade();

        /// <summary>
        /// 检查是否已为最新版本
        /// </summary>
        /// <param name="majorVersion"></param>
        /// <param name="minorVersion"></param>
        /// <param name="leastVersion"></param>
        /// <param name="md5"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        bool IsNeedUpgrade(int majorVersion, int minorVersion, int leastVersion, byte[] md5, long size);

        /// <summary>
        /// 下发数据
        /// </summary>
        /// <param name="packIndex"></param>
        /// <param name="binData"></param>
        /// <returns></returns>
        bool SendBinData(int packIndex, byte[] binData);

        /// <summary>
        /// 下发进度回调
        /// </summary>
        /// <param name="percent"></param>
        void ReportOTAProgress(int percent);

        /// <summary>
        /// 发送完成后，设备校验接收数据的md5是否与下发的md5一致
        /// </summary>
        /// <returns></returns>
        bool VerifyAfterSent();
    }
}
