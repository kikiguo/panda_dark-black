using System;
using System.IO;
using System.Security.Cryptography;

namespace StairMed.OTA
{
    /// <summary>
    /// 
    /// </summary>
    public class OTAHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly string _firmware = string.Empty;

        /// <summary>
        /// 主版本
        /// </summary>
        private readonly int _majorVersion = 0;

        /// <summary>
        /// 次版本
        /// </summary>
        private readonly int _minorVersion = 0;

        /// <summary>
        /// 次次版本
        /// </summary>
        private readonly int _leastVersion = 0;

        /// <summary>
        /// 下发数据时，每包大小
        /// </summary>
        private readonly int _packSize = 128;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTAable _ota;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firmware"></param>
        /// <param name="majorVersion"></param>
        /// <param name="minorVersion"></param>
        /// <param name="leastVersion"></param>
        /// <param name="packSize"></param>
        public OTAHelper(string firmware, int majorVersion, int minorVersion, int leastVersion, int packSize, IOTAable ota)
        {
            _firmware = firmware;
            _majorVersion = majorVersion;
            _minorVersion = minorVersion;
            _leastVersion = leastVersion;
            _packSize = packSize;
            _ota = ota;
        }

        /// <summary>
        /// 启动升级
        /// </summary>
        /// <param name="tip"></param>
        /// <returns></returns>
        public bool Start(out string tip)
        {
            tip = string.Empty;

            //文件不存在则失败
            if (!File.Exists(_firmware))
            {
                tip = "文件不存在";
                return false;
            }

            //创建文件缓存:(bin文件一般比较小，可以一次性加载到内存)
            var size = new FileInfo(_firmware).Length;
            var binData = new byte[size];

            //加载 bin文件
            using (var fs = new FileStream(_firmware, FileMode.Open, FileAccess.Read))
            {
                var bytes = new byte[4096];
                var offset = 0;
                while (true)
                {
                    var rx = fs.Read(bytes, 0, bytes.Length);
                    if (rx <= 0)
                    {
                        break;
                    }

                    //
                    Array.Copy(bytes, 0, binData, offset, rx);
                    offset += rx;
                }
            }

            //判断设备是否已就绪
            if (!_ota.IsReadyToUpgrade())
            {
                tip = "设备未准备好升级";
                return false;
            }

            //计算md5
            var md5 = MD5.Create().ComputeHash(binData);

            //是否需要升级
            if (!_ota.IsNeedUpgrade(_majorVersion, _minorVersion, _leastVersion, md5, size))
            {
                tip = "已为最新版本，无需升级";
                return true;
            }

            //发送文件内容
            var packIndex = 0;
            var sendBuffer = new byte[_packSize];
            var totalPack = binData.Length / _packSize;

            //循环下发 bin数据
            while (true)
            {
                var sent = packIndex * _packSize;
                var left = binData.Length - packIndex * _packSize;

                //
                if (left <= 0)
                {
                    break;
                }

                //
                byte[] sendBytes;
                if (left >= _packSize)
                {
                    Array.Copy(binData, sent, sendBuffer, 0, _packSize);
                    sendBytes = sendBuffer;
                }
                else
                {
                    sendBytes = new byte[left];
                    Array.Copy(binData, sent, sendBytes, 0, left);
                }

                //
                if (!_ota.SendBinData(packIndex++, sendBytes))
                {
                    tip = "下发数据失败";
                    return false;
                }

                //上报进度
                _ota.ReportOTAProgress(packIndex * 100 / totalPack);
            }

            //
            if (!_ota.VerifyAfterSent())
            {
                tip = "数据校验失败";
                return false;
            }

            //
            tip = "升级成功";
            return true;
        }

    }
}
