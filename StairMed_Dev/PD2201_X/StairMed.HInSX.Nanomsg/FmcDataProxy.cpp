#include "FmcDataProxy.h"

#define SENDTIMEOUT 5000
#define RECVTIMEOUT 5000
#define RECVMINSIZE 2

#define RETRY_NUMBER 3
//int gRetryCount = 0;
//int sock;
//int retconn;
//char* sendBuff = new char[1024 * 1024 * 10];
#define ERR_INVALID_NETWORK -2
int gTimeoutData = 0;
int gretconnData = 0;


FmcDataProxy::FmcDataProxy() {
	sock = nn_socket(AF_SP, NN_PAIR);
	endpoint = "tcp://192.168.52.1:5556";
	Init();
}
void FmcDataProxy::SetXferCallback(Callback* xferCallback)
{
	m_xferCallback = xferCallback;
}
FmcDataProxy::~FmcDataProxy() {
	if (!endpoint)
		delete[] endpoint;
	if (sock >= 0)
	{
		nn_close(sock);
	}
	if (m_Thread != NULL)
	{
		m_bXferThreadStop = true;
		m_bStartReceive = false;
		WaitForSingleObject(m_Thread, INFINITE);
		CloseHandle(m_Thread);
		m_Thread = NULL;
	}
	if (m_xferCallback != NULL)
	{
		m_xferCallback = NULL;
	}
}

unsigned FmcDataProxy::XferThread(void* pParam)
{
	FmcDataProxy* port = (FmcDataProxy*)pParam;

	unsigned char* buf = NULL;
	int to = RECVTIMEOUT;
	int recvsize = 0;
	while (!m_bXferThreadStop)
	{
		if (m_bStartReceive)
		{
			if ((recvsize = nn_recv(port->sock, &buf, NN_MSG, 0)) > RECVMINSIZE)
			{

				if (m_xferCallback != NULL)
				{
					m_xferCallback->ExecuteCallback(buf, recvsize);
				}
				nn_freemsg(buf);
			}
		}
	}
	return 0;
}

unsigned __stdcall FmcDataProxy::CommThread2(void* pParam)
{
	FmcDataProxy* pOCT = (FmcDataProxy*)pParam;
	pOCT->XferThread(pParam);
	return 0;
}

bool FmcDataProxy::Init()
{
	m_bXferThreadStop = false;
	m_bStartReceive = false;
	m_Thread = (HANDLE)_beginthreadex(NULL, 0, &FmcDataProxy::CommThread2, this, 0, NULL);

#if 1
	int gretconn = nn_connect(sock, endpoint);
	int sendtimeout = SENDTIMEOUT;

	int ret = nn_setsockopt(sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendtimeout, sizeof(sendtimeout));
	if (ret < 0)
	{
		return false;
	}

	int to = RECVTIMEOUT;
	ret = nn_setsockopt(sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof(to));
	if (ret < 0)
	{
		return false;
	}
	int opt = 1024 * 1024 * 16;
	ret = nn_setsockopt(sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &opt, sizeof(opt));
	if (ret < 0)
	{
		return false;
	}


#endif

	return true;
}

bool FmcDataProxy::SetAsyncReceive(bool isAsyncReceive)
{
	m_bStartReceive = isAsyncReceive;
	return true;
}

int FmcDataProxy::send(unsigned char* buf, int bufLen)
{
	if (sock >= 0)
		return call_fmc(sock, endpoint, NULL, buf, bufLen);
	return 0;
}

int FmcDataProxy::SendReceive(unsigned char* inbuf, int inlen, unsigned char* outbuf, int* outlen)
{
	if (sock >= 0)
		return call_fmc(sock, endpoint, NULL, inbuf, inlen, outbuf, outlen);
	return sock;
}

int FmcDataProxy::call_fmc(int fmc_sock, const char* sock_endpoint, const char* soap_action, void* inbuf, int inlen)
{
	int cmdLen = sizeof(unsigned int);

	if (fmc_sock < 0)
		return fmc_sock;
	char buffire[256] = { 0 };



	do
	{

		if (gTimeoutData > RETRY_NUMBER)
		{
			return ERR_INVALID_NETWORK;
		}

		//20161114
		Thread::Sleep(5);

		int gretconnData = nn_connect(fmc_sock, sock_endpoint);
#if 0
		int sendtimeout = SENDTIMEOUT;
		{
			sendtimeout = 200;
		}
		int ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendtimeout, sizeof(sendtimeout));
		if (ret < 0)
		{
			gTimeoutData++;
			nn_shutdown(fmc_sock, gretconnData);
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
#else
		int ret = -1;
#endif
		//send command
		ret = 0;
		ret = nn_send(fmc_sock, inbuf, inlen, 0);
		if (ret < 0)
		{
			gTimeoutData++;
			nn_shutdown(fmc_sock, gretconnData);
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
#if 0
		//recieve response
		char* buf = NULL;
		int to = RECVTIMEOUT;

		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof(to));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconnData);
			gTimeoutData++;
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
		int opt = 100000000;
		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &opt, sizeof(opt));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconnData);
			gTimeoutData++;
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
#endif
		return nn_shutdown(fmc_sock, gretconnData);
	} while (1);
}

int FmcDataProxy::call_fmc(int fmc_sock, const char* sock_endpoint, const char* soap_action, void* inbuf, int inlen, unsigned char* outbuf, int* outlen)
{

	if (fmc_sock < 0)
		return fmc_sock;
	char buffire[256] = { 0 };



	do
	{


		if (gTimeoutData > RETRY_NUMBER)
		{
			return ERR_INVALID_NETWORK;
		}

		//20161114
		Sleep(5);

		int gretconnData = nn_connect(fmc_sock, sock_endpoint);
#if 0
		int sendtimeout = SENDTIMEOUT;

		{
			sendtimeout = 200;
		}
		int ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendtimeout, sizeof(sendtimeout));
		if (ret < 0)
		{
			gTimeoutData++;
			nn_shutdown(fmc_sock, gretconnData);
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
#else
		int ret = -1;
#endif

		//send command
		//unsigned int command = cmd;
		//memcpy(sendBuff, &command, cmdLen);
		ret = -1;

		ret = nn_send(fmc_sock, inbuf, inlen, 0);
		if (ret < 0)
		{
			gTimeoutData++;
			nn_shutdown(fmc_sock, gretconnData);
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}

		//recieve response
		char* buf = NULL;

#if 0
		int to = RECVTIMEOUT;


		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof(to));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconnData);
			gTimeoutData++;
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
		int opt = 100000000;
		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &opt, sizeof(opt));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconnData);
			gTimeoutData++;
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
#endif
		int recvsize = 0;
		if ((recvsize = nn_recv(fmc_sock, &buf, NN_MSG, 0)) > RECVMINSIZE)
		{
			*outlen = recvsize;
			memcpy(outbuf, (unsigned char*)buf, recvsize);
			nn_freemsg(buf);
			gTimeoutData = 0;
			//*((int *)fileMapping.pView) = gTimeoutData;
			return nn_shutdown(fmc_sock, gretconnData);
		}
		else
		{
			gTimeoutData++;
			nn_shutdown(fmc_sock, gretconnData);
			//*((int *)fileMapping.pView) = gTimeoutData;
			continue;
		}
	} while (1);
}