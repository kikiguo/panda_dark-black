
#include "FmcComm.h"
#include "fmcCallback.h"

CallbackFmc::CallbackFmc(FmcComm^ pManaged)
{
	m_pManaged = pManaged;
}

CallbackFmc::~CallbackFmc()
{
	m_pManaged = nullptr;
}

void CallbackFmc::ExecuteCallback(unsigned char* dataBuffer, int dataLen)
{
	m_pManaged->raise_XferCallback(dataBuffer, dataLen);
}

