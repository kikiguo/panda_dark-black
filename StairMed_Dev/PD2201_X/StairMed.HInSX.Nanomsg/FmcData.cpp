
#include "FmcData.h"
#include "fmcCallbackAcq.h"
FmcData::FmcData()
{
	revdbuf = NULL;
	m_pCallbackFmc = new AcqCallbackFmc(this);
	_pProxy = new FmcDataProxy();
	//_pProxy->endpoint = "tcp://192.168.52.1:5556";
	_pProxy->SetXferCallback(m_pCallbackFmc);
	revdbuf = new unsigned char[1024 * 1024 * 16];
}
FmcData::~FmcData()
{
	_lock3->WaitOne();
	if (_pProxy) { delete _pProxy; _pProxy = nullptr; }
	if (revdbuf != NULL)
	{
		//delete[] revdbuf;
		revdbuf = NULL;
	}
	//if (_instance2)
	//{
	//	_instance2 = nullptr;
	//}
	_lock3->ReleaseMutex();
}

FmcData^ FmcData::CreateInstance()
{
	_lock3->WaitOne();
	try
	{
		if (_instance2 == nullptr)
		{
			//Thread::Sleep(1000);
			_instance2 = gcnew FmcData();
		}
		_lock3->ReleaseMutex();
	}
	catch (Exception^ e)
	{
		_lock3->ReleaseMutex();
		throw gcnew Exception(e->Message);
	}
	return _instance2;
}

bool FmcData::Send(unsigned char* buf, int bufLen)
{
	int send = 0;
	//_lock3->WaitOne();
	if (_pProxy)
		send = _pProxy->send(buf, bufLen);
	//_lock3->ReleaseMutex();
	return send == 0;
}

array<unsigned char>^ FmcData::SendReceive(unsigned char* inbuf, int inLen)
{
	int outlen = 0;
	bool result = false;
	//_lock3->WaitOne();
	if (_pProxy)
		result = _pProxy->SendReceive(inbuf, inLen, revdbuf, &outlen);
	//_lock3->ReleaseMutex();
	if (outlen > 0)
	{
		array<unsigned char>^ outbuf = gcnew array<unsigned char>(outlen);
		pin_ptr<unsigned char> cssrc = &outbuf[0];
#if 1
		memcpy(cssrc, revdbuf, outlen);
#else
		for (int i = 0; i < outlen; i++)
		{
			*(cssrc++) = *(revdbuf++);
		}
#endif
		return outbuf;
	}
	return gcnew array<unsigned char>(1);
}

bool FmcData::SetAsyncReceive(bool isAsyncReceive)
{
	if (_pProxy)
		return _pProxy->SetAsyncReceive(isAsyncReceive);
	return false;
}

void FmcData::add_XferCallback(AcqCallbackHandler^ pXferCallbackHandler)
{
	m_pAcqCallbackHandler = static_cast<AcqCallbackHandler^>(Delegate::Combine(m_pAcqCallbackHandler, pXferCallbackHandler));
}

void FmcData::remove_XferCallback(AcqCallbackHandler^ pXferCallbackHandler)
{
	m_pAcqCallbackHandler = static_cast<AcqCallbackHandler^>(Delegate::Remove(m_pAcqCallbackHandler, pXferCallbackHandler));
}

void FmcData::raise_XferCallback(unsigned char* dataBuffer, int dataLen)
{
	if (m_pAcqCallbackHandler)
	{
		array<unsigned char>^ ch = gcnew array<unsigned char>(dataLen);
		pin_ptr<unsigned char> cssrc = &ch[0];
		for (int i = 0; i < dataLen; i++)
		{
			*(cssrc++) = *(dataBuffer++);
		}
		m_pAcqCallbackHandler->Invoke(ch, dataLen);
		delete[] ch;
	}
}