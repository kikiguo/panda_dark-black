#pragma once
#include <vcclr.h>
#include "Def.h"

class AcqCallbackFmc :public Callback
{
public:
	AcqCallbackFmc(FmcData^ pFmcData);
	~AcqCallbackFmc();
	void ExecuteCallback(unsigned char* dataBuffer, int dataLen);
private:
	gcroot<FmcData^> m_pFmcData;
};

