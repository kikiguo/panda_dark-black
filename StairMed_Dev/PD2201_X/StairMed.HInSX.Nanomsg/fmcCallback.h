#pragma once
#include <vcclr.h>
#include "Def.h"
class CallbackFmc:public Callback
{
public:
	CallbackFmc(FmcComm^ pManaged);
	~CallbackFmc();
	void ExecuteCallback(unsigned char* dataBuffer,int dataLen);
private:
	gcroot<FmcComm^> m_pManaged;
};

