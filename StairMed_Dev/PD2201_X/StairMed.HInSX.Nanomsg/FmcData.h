#pragma once

#include "FmcDataProxy.h"
using namespace System;
public delegate void AcqCallbackHandler(array<unsigned char>^ data, int dataLen);
class AcqCallbackFmc;
public ref class FmcData
{
public:
	FmcData();
	~FmcData();
	static FmcData^ CreateInstance();
	bool Send(unsigned char* buf, int bufLen);
	array<unsigned char>^ SendReceive(unsigned char* inbuf, int inLen);
	bool SetAsyncReceive(bool isAsyncReceive);
	void add_XferCallback(AcqCallbackHandler^ pAcqCallbackHandler);
	void remove_XferCallback(AcqCallbackHandler^ pAcqCallbackHandler);
	void raise_XferCallback(unsigned char* dataBuffer, int dataLen);
private:
	AcqCallbackHandler^ m_pAcqCallbackHandler;
	FmcDataProxy* _pProxy;
	AcqCallbackFmc* m_pCallbackFmc;
	static FmcData^ _instance2 = nullptr;
	static Mutex^ _lock3 = gcnew Mutex(false);
	unsigned char* revdbuf;
};

