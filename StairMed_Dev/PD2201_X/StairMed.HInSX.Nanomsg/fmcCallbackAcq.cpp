
#include "FmcData.h"
#include "fmcCallbackAcq.h"

AcqCallbackFmc::AcqCallbackFmc(FmcData^ pFmcData)
{
	m_pFmcData = pFmcData;
}

AcqCallbackFmc::~AcqCallbackFmc()
{
	m_pFmcData = nullptr;
}

void AcqCallbackFmc::ExecuteCallback(unsigned char* dataBuffer, int dataLen)
{
	m_pFmcData->raise_XferCallback(dataBuffer, dataLen);
}