
#include "fmcProxy.h"
#include "process.h"
#include "nn.h"
#include <pair.h>
using namespace System;
using namespace System::Threading;
#define SENDTIMEOUT 5000
#define RECVTIMEOUT 5000
#define RECVMINSIZE 2

#define RETRY_NUMBER 3


//char* sendBuff = new char[1024 * 1024 * 10];
#define ERR_INVALID_NETWORK -2
int gTimeout = 0;
int gretconn = 0;


fmc::fmc() {
	sock = nn_socket(AF_SP, NN_PAIR);
	endpoint = "tcp://192.168.52.1:5555";
	Init();
}
void fmc::SetXferCallback(Callback* xferCallback)
{
	m_xferCallback = xferCallback;
}
fmc::~fmc() {
	if (!endpoint)
		delete[] endpoint;
	if (sock >= 0)
	{
		nn_close(sock);
	}
	if (m_Thread != NULL)
	{
		m_bXferThreadStop = true;
		m_bStartReceive = false;
		WaitForSingleObject(m_Thread, INFINITE);
		CloseHandle(m_Thread);
		m_Thread = NULL;
	}
	if (m_xferCallback != NULL)
	{
		m_xferCallback = NULL;
	}
}

unsigned fmc::XferThread(void* pParam)
{
	fmc* port = (fmc*)pParam;

	unsigned char* buf = NULL;
	int to = RECVTIMEOUT;
	int recvsize = 0;
	while (!m_bXferThreadStop)
	{
		if (m_bStartReceive)
		{
			if ((recvsize = nn_recv(port->sock, &buf, NN_MSG, 0)) > RECVMINSIZE)
			{

				if (m_xferCallback != NULL)
				{
					m_xferCallback->ExecuteCallback(buf, recvsize);
				}
				nn_freemsg(buf);
			}
		}
		//Thread::Sleep(10);
	}
	return 0;
}

unsigned __stdcall fmc::CommThread(void* pParam)
{
	fmc* pOCT = (fmc*)pParam;
	pOCT->XferThread(pParam);
	return 0;
}

bool fmc::Init()
{
	m_bXferThreadStop = false;
	m_bStartReceive = false;
	m_Thread = (HANDLE)_beginthreadex(NULL, 0, &fmc::CommThread, this, 0, NULL);

#if 1
	int gretconn = nn_connect(sock, endpoint);
	int sendtimeout = SENDTIMEOUT;

	int ret = nn_setsockopt(sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendtimeout, sizeof(sendtimeout));
	if (ret < 0)
	{
		return false;
	}

	int to = RECVTIMEOUT;
	ret = nn_setsockopt(sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof(to));
	if (ret < 0)
	{
		return false;
	}
	int opt = 1024 * 1024 * 16;
	ret = nn_setsockopt(sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &opt, sizeof(opt));
	if (ret < 0)
	{
		return false;
	}


#endif
	return true;
}

bool fmc::SetAsyncReceive(bool isAsyncReceive)
{
	m_bStartReceive = isAsyncReceive;
	return true;
}

int fmc::send(unsigned char* buf, int bufLen)
{
	if (sock >= 0)
		return call_fmc(sock, endpoint, NULL, buf, bufLen);
	return 0;
}

int fmc::SendReceive( unsigned char* inbuf, int inlen, unsigned char* outbuf, int* outlen)
{
	if (sock >= 0)
		return call_fmc(sock, endpoint, NULL, inbuf, inlen, outbuf, outlen);
	return 0;
}

int fmc::call_fmc(int fmc_sock, const char* sock_endpoint, const char* soap_action, void* inbuf, int inlen)
{
	int cmdLen = sizeof(unsigned int);

	if (fmc_sock < 0)
		return fmc_sock;
	char buffire[256] = { 0 };



	do
	{

		if (gTimeout > RETRY_NUMBER)
		{
			return ERR_INVALID_NETWORK;
		}

		//20161114
		//Thread::Sleep(5);
		int gretconn = nn_connect(fmc_sock, sock_endpoint);
#if 0
		int sendtimeout = SENDTIMEOUT;
		{
			sendtimeout = 200;
		}
		int ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendtimeout, sizeof(sendtimeout));
		if (ret < 0)
		{
			gTimeout++;
			nn_shutdown(fmc_sock, gretconn);
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
#else
		int ret = -1;
#endif

		//send command
		ret = 0;
		ret = nn_send(fmc_sock, inbuf, inlen, 0);
		if (ret < 0)
		{
			gTimeout++;
			nn_shutdown(fmc_sock, gretconn);
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
#if 0
		//recieve response
		char* buf = NULL;
		int to = RECVTIMEOUT;

		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof(to));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconn);
			gTimeout++;
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
		int opt = 100000000;
		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &opt, sizeof(opt));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconn);
			gTimeout++;
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
#endif
		return nn_shutdown(fmc_sock, gretconn);
	} while (1);
}

int fmc::call_fmc(int fmc_sock, const char* sock_endpoint, const char* soap_action,  void* inbuf, int inlen, unsigned char* outbuf, int* outlen)
{

	if (fmc_sock < 0)
		return fmc_sock;
	char buffire[256] = { 0 };



	do
	{


		if (gTimeout > RETRY_NUMBER)
		{
			return ERR_INVALID_NETWORK;
		}

		//20161114
		//Sleep(5);

		int gretconn = nn_connect(fmc_sock, sock_endpoint);
#if 0
		int sendtimeout = SENDTIMEOUT;
		
		{
			sendtimeout = 200;
		}
		int ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_SNDTIMEO, &sendtimeout, sizeof(sendtimeout));
		if (ret < 0)
		{
			gTimeout++;
			nn_shutdown(fmc_sock, gretconn);
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
#else
		int ret = -1;
#endif

		//send command
		//unsigned int command = cmd;
		//memcpy(sendBuff, &command, cmdLen);
		ret = -1;

		ret = nn_send(fmc_sock, inbuf, inlen, 0);
		if (ret < 0)
		{
			gTimeout++;
			nn_shutdown(fmc_sock, gretconn);
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}

		//recieve response
		char* buf = NULL;
		
#if 0
		int to = RECVTIMEOUT;
		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVTIMEO, &to, sizeof(to));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconn);
			gTimeout++;
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
		int opt = 100000000;
		ret = nn_setsockopt(fmc_sock, NN_SOL_SOCKET, NN_RCVMAXSIZE, &opt, sizeof(opt));
		if (ret < 0)
		{
			nn_shutdown(fmc_sock, gretconn);
			gTimeout++;
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
#endif
		int recvsize = 0;
		if ((recvsize = nn_recv(fmc_sock, &buf, NN_MSG, 0)) > RECVMINSIZE)
		{
			*outlen = recvsize;
			memcpy(outbuf, (unsigned char*)buf, recvsize);			
			nn_freemsg(buf);
			gTimeout = 0;
			//*((int *)fileMapping.pView) = gTimeout;
			return nn_shutdown(fmc_sock, gretconn);
		}
		else
		{
			gTimeout++;
			nn_shutdown(fmc_sock, gretconn);
			//*((int *)fileMapping.pView) = gTimeout;
			continue;
		}
	} while (1);
}