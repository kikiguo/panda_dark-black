
#include "FmcComm.h"
#include "fmcCallback.h"
FmcComm::FmcComm()
{
	revdbuf = NULL;
	m_pCallbackFmc = new CallbackFmc(this);
	_pProxy = new fmc();
	//_pProxy->endpoint = "tcp://192.168.52.1:5555";
	_pProxy->SetXferCallback(m_pCallbackFmc);
	revdbuf = new unsigned char[1024 * 1024 * 16];
}
FmcComm::~FmcComm()
{
	_lock2->WaitOne();
	if (_pProxy) { delete _pProxy; _pProxy = nullptr; }
	if (revdbuf != NULL)
	{
		//delete[] revdbuf;
		revdbuf = NULL;
	}
	//if (_instance)
	//{
	//	_instance = nullptr;
	//}
	_lock2->ReleaseMutex();
}
#if FLAC_INSTANCE
FmcComm^ FmcComm::CreateInstance()
{
	_lock2->WaitOne();
	try
	{
		if (_instance == nullptr)
		{
			//Thread::Sleep(1000);
			_instance = gcnew FmcComm();
		}
		_lock2->ReleaseMutex();
	}
	catch (Exception^ e)
	{
		_lock2->ReleaseMutex();
		throw gcnew Exception(e->Message);
	}
	return _instance;
}
#endif

bool FmcComm::Send(unsigned char* buf, int bufLen)
{
	int send = 0;
	//_lock2->WaitOne();
	if (_pProxy)
		send = _pProxy->send(buf, bufLen);
	//_lock2->ReleaseMutex();
	return send == 0;
}

array<unsigned char>^ FmcComm::SendReceive(unsigned char* inbuf, int inLen)
{
	int outlen = 0;
	bool result = false;
	//_lock2->WaitOne();
	if (_pProxy)
		_pProxy->SendReceive(inbuf, inLen, revdbuf, &outlen);
	//_lock2->ReleaseMutex();
	if (outlen > 0)
	{
		array<unsigned char>^ outbuf = gcnew array<unsigned char>(outlen);
		pin_ptr<unsigned char> cssrc = &outbuf[0];
#if 1
		memcpy(cssrc, revdbuf, outlen);
#else
		for (int i = 0; i < outlen; i++)
		{
			*(cssrc++) = *(revdbuf++);
		}
#endif
		return outbuf;
	}
	return gcnew array<unsigned char>(1);
}

//array<unsigned char>^ FmcComm::SendReceive(array<unsigned char>^ inbufs, int inLen)
//{
//	unsigned char* inbuf = new unsigned char[inLen];
//	pin_ptr<unsigned char> cssrc1 = &inbufs[0];
//	memcpy(inbuf, cssrc1, inLen);
//	int outlen = 0;
//	bool result = false;
//	//_lock2->WaitOne();
//	if (_pProxy)
//		_pProxy->SendReceive(inbuf, inLen, revdbuf, &outlen);
//	//_lock2->ReleaseMutex();
//	delete[] inbuf;
//	if (outlen > 0)
//	{
//		array<unsigned char>^ outbuf = gcnew array<unsigned char>(outlen);
//		pin_ptr<unsigned char> cssrc = &outbuf[0];
//#if 1
//		memcpy(cssrc, revdbuf, outlen);
//#else
//		for (int i = 0; i < outlen; i++)
//		{
//			*(cssrc++) = *(revdbuf++);
//		}
//#endif
//		return outbuf;
//	}
//	return gcnew array<unsigned char>(1);
//}

bool FmcComm::SetAsyncReceive(bool isAsyncReceive)
{
	if (_pProxy)
		return _pProxy->SetAsyncReceive(isAsyncReceive);
	return false;
}

void FmcComm::add_XferCallback(XferCallbackHandler^ pXferCallbackHandler)
{
	m_pXferCallbackHandler = static_cast<XferCallbackHandler^>(Delegate::Combine(m_pXferCallbackHandler, pXferCallbackHandler));
}

void FmcComm::remove_XferCallback(XferCallbackHandler^ pXferCallbackHandler)
{
	m_pXferCallbackHandler = static_cast<XferCallbackHandler^>(Delegate::Remove(m_pXferCallbackHandler, pXferCallbackHandler));
}

void FmcComm::raise_XferCallback(unsigned char* dataBuffer,int dataLen)
{
	if (m_pXferCallbackHandler)
	{
		array<unsigned char>^ ch = gcnew array<unsigned char> (dataLen);
		pin_ptr<unsigned char> cssrc = &ch[0];
		for (int i = 0; i < dataLen; i++)
		{
			*(cssrc++) = *(dataBuffer++);
		}
		m_pXferCallbackHandler->Invoke(ch, dataLen);
		delete[] ch;
	}
}