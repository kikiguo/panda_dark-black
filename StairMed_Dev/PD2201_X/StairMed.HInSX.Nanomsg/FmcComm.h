#pragma once
#include "fmcProxy.h"
using namespace System;
using namespace System::Runtime::InteropServices;
public delegate void XferCallbackHandler(array<unsigned char>^ data, int dataLen);

#define FLAC_INSTANCE 1

class CallbackFmc;
public ref class FmcComm
{
public:
	FmcComm();
	~FmcComm();
#if FLAC_INSTANCE
	static FmcComm^ CreateInstance();
#endif
	bool Send(unsigned char* buf, int bufLen);
	array<unsigned char>^ SendReceive(unsigned char* inbuf, int inLen);
	//array<unsigned char>^ SendReceive(array<unsigned char>^ inbufs, int inLen);
	void add_XferCallback(XferCallbackHandler^ pXferCallbackHandler);
	void remove_XferCallback(XferCallbackHandler^ pXferCallbackHandler);
	void raise_XferCallback(unsigned char* dataBuffer,int dataLen);
	bool SetAsyncReceive(bool isAsyncReceive);
private:
	XferCallbackHandler^ m_pXferCallbackHandler;
	fmc* _pProxy;
	CallbackFmc* m_pCallbackFmc;
#if FLAC_INSTANCE
	static FmcComm^ _instance = nullptr;
	static Mutex^ _lock2 = gcnew Mutex(false);
#else
	Mutex^ _lock2 = gcnew Mutex(false);
#endif
	unsigned char* revdbuf;
};

