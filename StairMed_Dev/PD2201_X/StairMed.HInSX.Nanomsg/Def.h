#pragma once
class Callback
{
public:
	virtual void ExecuteCallback(unsigned char* dataBuffer, int dataLen) = 0;
};