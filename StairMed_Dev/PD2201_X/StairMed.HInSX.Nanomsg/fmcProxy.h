#pragma once
#include <windows.h>
#include "process.h"
#include "nn.h"
#include <pair.h>
#include "Def.h"
using namespace System;
using namespace System::Threading;



public class fmc
{
public:
	int sock;
	const char* endpoint;
	fmc();
	virtual ~fmc();

	unsigned XferThread(void* pParam);

	static unsigned __stdcall CommThread(void* pParam);

	bool Init();
	
	int send(unsigned char* buf, int bufLen);
	
	bool SetAsyncReceive(bool isAsyncReceive);

	int SendReceive(unsigned char* inbuf, int inlen, unsigned char* outbuf, int* outlen);

	void SetXferCallback(Callback* xferCallback);
private:
	HANDLE m_Thread;
	bool m_bXferThreadStop;
	bool m_bStartReceive;
	Callback* m_xferCallback;
	int call_fmc(int sock, const char* sock_endpoint, const char* soap_action, void* inbuf, int inlen);

	int call_fmc(int sock, const char* sock_endpoint, const char* soap_action, void* inbuf, int inlen, unsigned char* outbuf, int* outlen);
};
