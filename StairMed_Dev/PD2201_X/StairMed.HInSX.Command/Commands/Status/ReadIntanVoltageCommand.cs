﻿using System;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadIntanVoltageCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public ReadIntanVoltageCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 电压状态读取
        /// </summary>
        /// <param name="headstage"></param>
        /// <param name="voltageA"></param>
        /// <param name="voltageB"></param>
        /// <returns></returns>
        public bool ReadIntanVoltage(int headstage, out double voltageA, out double voltageB)
        {
            voltageA = 0.0;
            voltageB = 0.0;

            //
            var recvd = _cmd.SendCommand(CommandCode.ReadIntanVoltage, (byte)headstage);
            if (recvd == null || recvd.Length < 12)
            {
                return false;
            }

            voltageA = BitConverter.ToUInt16(recvd.Array, 4) / 100.0;
            voltageB = BitConverter.ToUInt16(recvd.Array, 8) / 100.0;

            //
            recvd.Dispose();

            //
            return true;
        }
    }
}

