﻿using System;
using System.Collections.Generic;
using System.Threading.Channels;
using System.Xml;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class GetHSStatusCommand : HeadstageCommand
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public GetHSStatusCommand(HInSXCmd cmd) : base(cmd)
        {

        }


        public bool GetBit(byte b, int index)
        {
            bool bit = (b & (1 << index)) != 0;
                return bit;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="headStageCount"></param>
        /// <param name="headstageStates"></param>
        /// <returns></returns>
        public override bool GetHeadstageStatus(int headStageCount, out List<bool> headstageStates)
        {
            headstageStates = new List<bool>();
            for (int i = 0; i < headStageCount; i++)
            {
                headstageStates.Add(false);
            }
            var bytes = new byte[4] { 0x00, 0x00, 0x00, 0x11 };
            var recvd = _cmd.SendCommand(CommandCode.GetHeadstageState, bytes);
            return true;

            //headstageStates = new List<bool>();
            //for (int i = 0; i < headStageCount; i++)
            //{
            //    headstageStates.Add(false);
            //}

            ////
            //var bytes = new byte[4] {0x00, 0x00, 0x00, 0x11};
            //var recvd = _cmd.SendCommand(CommandCode.GetHeadstageState, bytes);
            //if (recvd == null || recvd.Length != 8)
            //{
            //    return false;
            //}

            //var offset = 4;
            //for (int i = 0; i < headStageCount; i++)
            //{
            //    //每个bit代表一个headstage的状态
            //    //if (recvd[offset+i] ==  && recvd[offset + i +1] == 0)
            //    //{
            //    //    headstageStates[i/ 2] = true;
            //    //}
            //    var b = recvd[offset + i / 4];
            //    bool bit1 = GetBit(b, (i % 4) * 2);
            //    bool bit2 = GetBit(b, ((i % 4) * 2 + 1));
            //    var exist = !bit1 && bit2;
            //    headstageStates[i] = exist;
            //}

            ////
            //recvd.Dispose();

            ////
            //return true;
        }
    }
}
