﻿using System;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadIntanTemperatureCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public ReadIntanTemperatureCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 温度状态读取
        /// </summary>
        /// <param name="headstage"></param>
        /// <param name="temperatureA"></param>
        /// <param name="temperatureB"></param>
        /// <returns></returns>
        public bool ReadIntanTemperature(int headstage, out double temperatureA, out double temperatureB)
        {
            temperatureA = 0.0;
            temperatureB = 0.0;

            //
            var recvd = _cmd.SendCommand(CommandCode.ReadIntanTemperature, BitConverter.GetBytes(headstage));
            if (recvd == null || recvd.Length < 12)
            {
                return false;
            }

            //
            temperatureA = BitConverter.ToUInt16(recvd.Array, 4) / 100.0;
            temperatureB = BitConverter.ToUInt16(recvd.Array, 8) / 100.0;

            //
            recvd.Dispose();

            //
            return true;
        }
    }
}