﻿using StairMed.HInSX.Command.Logger;
using System;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class GetDAUInfoCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public GetDAUInfoCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 获取温度
        /// </summary>
        /// <param name="temperature"></param>
        /// <returns></returns>
        public bool GetDAUInfo(out double temperature)
        {
            temperature = 0;

            //
            var recvd = _cmd.SendCommand(CommandCode.GetDAUInfo);

            //
            if (recvd == null || recvd.Length != 24)
            {
                return false;
            }

            //
            temperature = BitConverter.ToSingle(recvd.Array, 8);
            LogTool.Logger.LogT($"{nameof(GetDAUInfo)} get temperature:{temperature}");

            //
            return true;
        }
    }
}
