﻿using StairMed.HInSX.Command.Logger;
using System;
using System.Linq;
using System.Text;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class VersionCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public VersionCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firmware"></param>
        /// <param name="fpga"></param>
        /// <param name="linux"></param>
        /// <param name="hardwareConfiguration"></param>
        /// <returns></returns>
        public bool GetVersionInfo(out string firmware, out string fpga, out string linux, out string hardwareConfiguration)
        {
            firmware = string.Empty;
            fpga = string.Empty;
            linux = string.Empty;
            hardwareConfiguration = string.Empty;

            //
            var recvd = _cmd.SendCommand(CommandCode.GetVersion);

            //
            if (recvd == null || recvd.Length <= 4)
            {
                return false;
            }

            //
            try
            {
                var strs = Encoding.ASCII.GetString(recvd.Array, 4, recvd.Length - 4);
                var parts = strs.Split("\0").Where(r => !string.IsNullOrWhiteSpace(r)).ToList();
                if (parts.Count == 4)
                {
                    firmware = parts[0];
                    fpga = parts[1];
                    linux = parts[2];
                    hardwareConfiguration = parts[3];
                }
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{nameof(GetVersionInfo)}", ex);
            }

            //
            return true;
        }
    }

    public class ARMTestCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public ARMTestCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firmware"></param>
        /// <param name="fpga"></param>
        /// <param name="linux"></param>
        /// <param name="hardwareConfiguration"></param>
        /// <returns></returns>
        public bool ArmSelfTest(int port,int channel)
        {
            byte[] sd = new byte[2];
            sd[0] = (byte)port;
            sd[1] = (byte)channel;
            //
            var recvd = _cmd.SendCommand(CommandCode.ARMSelfTest, sd);

            //
            if (recvd == null || recvd.Length <= 4)
            {
                return false;
            }


            //
            return true;
        }
    }
}
