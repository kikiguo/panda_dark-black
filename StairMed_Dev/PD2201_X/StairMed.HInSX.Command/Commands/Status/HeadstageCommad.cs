﻿using System.Collections.Generic;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class HeadstageCommand : CommandBase
    {

        public HeadstageCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        public virtual bool GetHeadstageStatus(int headStageCount, out List<bool> headstageStates)
        {
            headstageStates = new List<bool>();
            return true;
        }
    }
}
