﻿using StairMed.Extension;
using StairMed.HInSX.Command.Logger;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StairMed.HInSX.Command.Commands.Status
{
    /// <summary>
    /// 
    /// </summary>
    public class HeadstageStateCommand : HeadstageCommand
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public HeadstageStateCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        private const int OneChipRegisterByteNum = 46 * 4 * 2;
        private const int HeadOffset = 43;
        private const int TagOffset1 = 62;
        private const int TagOffset2 = 152;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool GetHeadstageStatus(int headStageCount, out List<bool> headstageStates)
        {
            //LogTool.Logger.LogT($"start {nameof(GetHeadstageState)}");

            //
            headstageStates = new List<bool>();
            for (int i = 0; i < headStageCount; i++)
            {
                headstageStates.Add(false);
            }

            //
            var recvd = _cmd.SendCommand(CommandCode.GetHeadstageStateOld);

            //
            if (recvd == null || recvd.Length < HeadOffset)
            {
                return false;
            }

            //TODO:临时操作-》将状态返回寄存器保存至文件
            if (true)
            {
                try
                {
                    var builder = new StringBuilder();
                    if (recvd.Length >= HeadOffset)
                    {
                        var head = recvd.Array.Take(HeadOffset).ToList();
                        builder.AppendLine(head.ToStrings(short.MaxValue, true));
                    }
                    for (int i = 0; i < headStageCount; i++)
                    {
                        if (recvd.Length >= HeadOffset + (i + 1) * OneChipRegisterByteNum)
                        {
                            builder.AppendLine($"[{i + 1}]");
                            builder.AppendLine(recvd.Array.Skip(HeadOffset + i * OneChipRegisterByteNum).Take(OneChipRegisterByteNum).ToStrings(short.MaxValue, true));
                        }
                    }
                    LogTool.Logger.LogF(builder.ToString());
                }
                catch { }
            }

            //
            for (int i = 0; i < headStageCount; i++)
            {
                if (recvd.Length >= HeadOffset + (i + 1) * OneChipRegisterByteNum)
                {
                    var tagByte11 = recvd[HeadOffset + i * OneChipRegisterByteNum + TagOffset1];
                    var tagByte12 = recvd[HeadOffset + i * OneChipRegisterByteNum + TagOffset1 + 1];
                    var tagByte21 = recvd[HeadOffset + i * OneChipRegisterByteNum + TagOffset2];
                    var tagByte22 = recvd[HeadOffset + i * OneChipRegisterByteNum + TagOffset2 + 1];
                    if (tagByte11 == 0x35 && tagByte12 == 0x00 && tagByte21 == 0x3A && tagByte22 == 0x00)
                    {
                        headstageStates[i] = true;
                    }
                }
            }

            //
            //LogTool.Logger.LogT($"{nameof(GetHeadstageState)} succeed");

            //
            return true;
        }
    }
}
