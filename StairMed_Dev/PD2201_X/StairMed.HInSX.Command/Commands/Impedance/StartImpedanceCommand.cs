﻿using System;

namespace StairMed.HInSX.Command.Commands.Impedance
{
    /// <summary>
    /// 
    /// </summary>
    public class StartImpedanceCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public StartImpedanceCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool StartImpedance(int headstage)
        {
            return _cmd.SendCommand(CommandCode.StartImpedance, BitConverter.GetBytes(headstage)) != null;
        }
    }
}
