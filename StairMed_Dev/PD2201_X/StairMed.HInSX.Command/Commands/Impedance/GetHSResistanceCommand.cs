﻿using StairMed.Array;
using StairMed.Extension;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.HInSX.Command.Commands.Impedance
{
    /// <summary>
    /// 
    /// </summary>
    public class GetHSResistanceCommand : CommandBase
    {
        /// <summary>
        /// 每种电容下的采样点个数
        /// </summary>
        public const int ImpedanceSampleCount = 440;

        /// <summary>
        /// 每种电容下的采样数据字节数
        /// </summary>
        public const int ImpedanceSampleByteCount = ImpedanceSampleCount * 2;

        /// <summary>
        /// 电容类型个数：0.1pf、1pf、10pf
        /// </summary>
        public const int CapCount = 3;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public GetHSResistanceCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstage"></param>
        /// <param name="channel"></param>
        /// <param name="recvd"></param>
        /// <returns></returns>
        public bool GetHSResistance(int headstage, int channel, out PooledArray<byte> recvd)
        {
            //
            //headstage = 1;
            var bytes = new byte[4];
            bytes[0] = (byte)headstage;
            bytes[1] = (byte)(headstage >> 8);
            bytes[2] = (byte)channel;
            bytes[3] = (byte)(channel >> 8);
            recvd = _cmd.SendCommand(CommandCode.QueryImpedanceADC, bytes);

            //
            //{
            if (recvd == null || recvd.Length != ImpedanceSampleByteCount * CapCount + 35)
            {
               // Logger.LogTool.Logger.LogW($"{nameof(GetHSResistance)} recvd length wrong:{recvd.Length}");
                return false;
            }

            //
            return true;
        }
    }
}
