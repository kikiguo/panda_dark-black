﻿using System;

namespace StairMed.HInSX.Command.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class InitCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public InitCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Init()
        {
            //
            var recvd = _cmd.SendCommand(CommandCode.SetHeadstageCmd, BitConverter.GetBytes((uint)(0x01 << 28)));
            
            //
            if (recvd == null || recvd.Length < 8)
            {
                return false;
            }

            //
            return BitConverter.ToUInt32(recvd.Array, 4) == 1;
        }
    }
}
