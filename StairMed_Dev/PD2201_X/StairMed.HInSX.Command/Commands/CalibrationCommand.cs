﻿using System;

namespace StairMed.HInSX.Command.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class CalibrationCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public CalibrationCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool StartCalibration()
        {
            //
            var recvd = _cmd.SendCommand(CommandCode.SetHeadstageCmd, BitConverter.GetBytes((uint)(0x02 << 28)));

            //
            if (recvd == null || recvd.Length < 8)
            {
                return false;
            }

            //
            return BitConverter.ToUInt32(recvd.Array, 4) == 0;
        }
    }
}
