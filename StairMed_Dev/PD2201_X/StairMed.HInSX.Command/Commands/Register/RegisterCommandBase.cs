﻿namespace StairMed.HInSX.Command.Commands.Register
{
    /// <summary>
    /// 
    /// </summary>
    public class RegisterCommandBase : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public RegisterCommandBase(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 寄存器唯一地址:D27-D8
        /// D27-D20:通道数:0-255， 1个采集盒最多支持256个Headstage通道
        /// D19-D16:Headstage中第几颗芯片,0-15, 1个Headstage最多支持16个芯片级联
        /// D15-D8:地址,0-63，最多支持64个寄存器, 具体见器件规格书
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="coreIndex"></param>
        /// <param name="registerIndex"></param>
        /// <returns></returns>
        public uint GetRegisterAddress(uint headstageIndex, uint coreIndex, uint registerIndex)
        {
            headstageIndex = headstageIndex % 256;
            coreIndex = coreIndex % 16;
            registerIndex = registerIndex % 64;

            //
            return (headstageIndex << 20) + (coreIndex << 16) + (registerIndex << 8);
        }
    }
}
