﻿using System;

namespace StairMed.HInSX.Command.Commands.Register
{
    /// <summary>
    /// 
    /// </summary>
    public class WriteRegisterCommand : RegisterCommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public WriteRegisterCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool WriteRegister(uint headstageIndex, uint coreIndex, uint registerIndex, byte val)
        {
            //
            var addr = GetRegisterAddress(headstageIndex, coreIndex, registerIndex);
            var param = (0x05 << 28) + addr + (uint)(val << 0);
            var recvd = _cmd.SendCommand(CommandCode.SetHeadstageCmd, BitConverter.GetBytes(param));

            //
            if (recvd == null || recvd.Length < 8)
            {
                return false;
            }

            //
            return BitConverter.ToUInt32(recvd.Array, 4) == 0;
        }
    }
}
