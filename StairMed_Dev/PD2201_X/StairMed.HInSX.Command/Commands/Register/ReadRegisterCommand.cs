﻿using System;

namespace StairMed.HInSX.Command.Commands.Register
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadRegisterCommand : RegisterCommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public ReadRegisterCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ReadRegister(uint headstageIndex, uint coreIndex, uint registerIndex, out byte val)
        {
            //
            val = 0;

            //
            var addr = GetRegisterAddress(headstageIndex, coreIndex, registerIndex);
            var recvd = _cmd.SendCommand(CommandCode.ReadRegister, BitConverter.GetBytes(addr));

            //
            if (recvd == null || recvd.Length < 8)
            {
                return false;
            }

            //
            val = (byte)(BitConverter.ToUInt32(recvd.Array, 4) & 0xff);
            return true;
        }
    }
}
