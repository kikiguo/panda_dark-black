﻿namespace StairMed.HInSX.Command.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class PingCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public PingCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public bool DoPing()
        {
            return _cmd.SendCommand(CommandCode.Ping) != null;
        }
    }
}
