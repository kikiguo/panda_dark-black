﻿using System;

namespace StairMed.HInSX.Command.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class DeviceTypeCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public DeviceTypeCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hinsType"></param>
        /// <returns></returns>
        public bool GetDeviceType(out HInSXType hinsType)
        {
            hinsType = HInSXType.H_Unknown;
            //
            var recvd = _cmd.SendCommand(CommandCode.GetDeviceType);

            //
            if (recvd == null || recvd.Length <= 8)
            {
                return false;
            }

            //
            var tmpType = (HInSXType)recvd[4];

            //
            var cmds = Enum.GetValues(typeof(HInSXType));
            foreach (var item in cmds)
            {
                if ((HInSXType)item == tmpType)
                {
                    hinsType = tmpType;
                    break;
                }
            }

            //
            return true;
        }
    }
}
