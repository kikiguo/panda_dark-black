﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.HInSX.Command.Commands.Record
{
    /// <summary>
    /// 
    /// </summary>
    public class StartCollectCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public StartCollectCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool StartCollect()
        {
            //
            var bytes = new List<byte>();
            bytes.AddRange(BitConverter.GetBytes(0x04));
            bytes.AddRange(BitConverter.GetBytes(0x01));

            //
            return _cmd.SendCommand(CommandCode.StartCollect, bytes.ToArray()) != null;
        }
    }
}
