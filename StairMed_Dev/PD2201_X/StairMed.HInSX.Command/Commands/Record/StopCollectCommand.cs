﻿namespace StairMed.HInSX.Command.Commands.Record
{
    /// <summary>
    /// 
    /// </summary>
    public class StopCollectCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public StopCollectCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool StopCollect()
        {
            return _cmd.SendCommand(CommandCode.StopCollect) != null;
        }
    }
}
