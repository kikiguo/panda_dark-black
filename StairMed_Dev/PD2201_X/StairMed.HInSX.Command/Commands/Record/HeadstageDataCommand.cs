﻿using StairMed.Array;
using System.Collections.Generic;
using System;

namespace StairMed.HInSX.Command.Commands.Record
{
    /// <summary>
    /// 
    /// </summary>
    public class HeadstageDataCommand : CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public HeadstageDataCommand(HInSXCmd cmd) : base(cmd)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public bool GetHeadstageData(out PooledArray<byte> recvd)
        {
            //
            recvd = _cmd.SendCommand(CommandCode.GetHeadstageData);

            //
            if (recvd == null || recvd.Length < 512)
            {
                //LogTool.Logger.LogW($"{nameof(GetHeadstageData)} recvd {(recvd == null ? "null" : "too short")}");
                return false;
            }

            //
            return true;
        }


        /// <summary>
        /// 设置微放大器采样频率，500Hz-30000Hz，4B
        /// </summary>
        /// <returns></returns>
        public bool SetHeadstageFreq()
        {
            //
            var bytes = new List<byte>();
            bytes.AddRange(BitConverter.GetBytes(0x0D02));

            //
            return _cmd.SendCommand(CommandCode.SetHeadstageFreq, bytes.ToArray()) != null;
        }

        /// <summary>
        /// 读取微放大器采样频率，500Hz-30000Hz，4B
        /// </summary>
        /// <returns></returns>
        public bool GetHeadstageFreq(out PooledArray<byte> recvd)
        {
            //
            recvd = _cmd.SendCommand(CommandCode.GetHeadstageFreq);

            //
            if (recvd == null || recvd.Length < 512)
            {
                //LogTool.Logger.LogW($"{nameof(GetHeadstageData)} recvd {(recvd == null ? "null" : "too short")}");
                return false;
            }
            //
            return true;
        }

       

    }
}
