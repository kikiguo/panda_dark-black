﻿using StairMed.Array;

namespace StairMed.HInSX.Command
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHInSXCmdHelper
    {
        PooledArray<byte> Send(byte[] bytes, CommandCode code);
        void HandleRevicedBuffer(PooledArray<byte> bytes);
    }
}
