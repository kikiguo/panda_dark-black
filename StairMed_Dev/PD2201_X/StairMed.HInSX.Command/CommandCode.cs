﻿namespace StairMed.HInSX.Command
{
    /// <summary>
    /// HInS1024 CommandCode
    /// </summary>
    public enum CommandCode : uint
    {
        Ping = 0x0914,

        //
        GetHeadstageData = 0x1202,

        //
        StartCollect = 0x1102,
        StopCollect = 0x1103,

        //GetHeadstageData = 0x2701,

        //获取采集盒FPGA芯片内部温度, 测温范围:10°C -85°C
        GetDAUInfo = 0x0708,

        //
        GetVersion = 0x0903,
        SetHeadstageCmd = 0x0d01,
        ReadRegister = 0x0d03,

        //设备类型
        GetDeviceType = 0x134, //TODO：待定

        
        //采集盒相连的所有Headstage通道工作状态
        GetHeadstageState = 0x1022,

        //读取intan芯片 电压 or 温度
        ReadIntanTemperature = 0x1023, //
        ReadIntanVoltage = 0x1024, //
        //阻抗测试
        QueryImpedanceADC = 0x1025, //
        StartImpedance, //TODO：待定

        // 旧协议
        //采集盒相连的所有Headstage通道工作状态
        GetHeadstageStateOld = 0x2701,
        ARMSelfTest = 0x1016,

        SetHeadstageFreq= 0x0D02,
        GetHeadstageFreq= 0x0D04,
    }
}
