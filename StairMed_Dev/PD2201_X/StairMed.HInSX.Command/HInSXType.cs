﻿namespace StairMed.HInSX.Command
{
    /// <summary>
    /// 
    /// </summary>
    public enum HInSXType
    {
        H_Unknown = 0x00,
        HInS_1024 = 0x01,
        HInS_New_1024 = 0x02,
        HInS_2048 = 0x03,
        HInS_New_2048 = 0x04,
        DemoMode = 0x05,

    }
}
