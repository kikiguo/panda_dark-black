using StairMed.Array;
using StairMed.Extension;
using StairMed.HInSX.Command.Commands;
using StairMed.HInSX.Command.Commands.Impedance;
using StairMed.HInSX.Command.Commands.Record;
using StairMed.HInSX.Command.Commands.Register;
using StairMed.HInSX.Command.Commands.Status;
using StairMed.HInSX.Command.Logger;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace StairMed.HInSX.Command
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXCmd
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(HInSXCmd);

        /// <summary>
        /// 
        /// </summary>
        public readonly InitCommand InitCommand;
        public readonly CalibrationCommand CalibrationCommand;

        /// <summary>
        /// 
        /// </summary>
        public readonly StartCollectCommand StartCollectCommand;
        public readonly StopCollectCommand StopCollectCommand;
        public readonly HeadstageDataCommand HeadstageDataCommand;

        /// <summary>
        /// 
        /// </summary>
        public readonly PingCommand PingCommand;
        public readonly GetDAUInfoCommand GetDAUInfoCommand;
        public readonly VersionCommand VersionCommand;
        public readonly HeadstageCommand HSStatusCommand;

        /// <summary>
        /// 
        /// </summary>
        public readonly ReadRegisterCommand ReadRegisterCommand;
        public readonly WriteRegisterCommand WriteRegisterCommand;

        /// <summary>
        /// 
        /// </summary>
        public readonly DeviceTypeCommand DeviceTypeCommand;

        /// <summary>
        /// 
        /// </summary>
        public readonly StartImpedanceCommand StartImpedanceCommand;
        public readonly GetHSResistanceCommand GetHSResistanceCommand;

        /// <summary>
        /// 
        /// </summary>
        public readonly ReadIntanTemperatureCommand ReadIntanTemperatureCommand;
        public readonly ReadIntanVoltageCommand ReadIntanVoltageCommand;

        /// <summary>
        /// 
        /// </summary>
        protected static readonly Dictionary<CommandCode, byte[]> _commandBytes = new Dictionary<CommandCode, byte[]>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IHInSXCmdHelper _helper = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="demo"></param>
        public HInSXCmd(IHInSXCmdHelper helper, bool bNew = false)
        {
            _helper = helper;

            //
            InitCommand = new InitCommand(this);
            CalibrationCommand = new CalibrationCommand(this);

            //
            StartCollectCommand = new StartCollectCommand(this);
            StopCollectCommand = new StopCollectCommand(this);
            HeadstageDataCommand = new HeadstageDataCommand(this);

            //
            PingCommand = new PingCommand(this);
            GetDAUInfoCommand = new GetDAUInfoCommand(this);
            VersionCommand = new VersionCommand(this);
            //GetHSStatusCommand = new GetHSStatusCommand(this);
            if (!bNew)
            {
                HSStatusCommand = new GetHSStatusCommand(this);
            }
            else 
            {
                HSStatusCommand = new HeadstageStateCommand(this);
            }
            

            //
            ReadRegisterCommand = new ReadRegisterCommand(this);
            WriteRegisterCommand = new WriteRegisterCommand(this);

            //
            DeviceTypeCommand = new DeviceTypeCommand(this);

            //
            StartImpedanceCommand = new StartImpedanceCommand(this);
            GetHSResistanceCommand = new GetHSResistanceCommand(this);

            //
            ReadIntanVoltageCommand = new ReadIntanVoltageCommand(this);
            ReadIntanTemperatureCommand = new ReadIntanTemperatureCommand(this);

            ARMTestCommand = new ARMTestCommand(this);
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="extraArgs"></param>
        /// <returns></returns>
        public PooledArray<byte> SendCommand(CommandCode cmd, params byte[] extraArgs)
        {
            //发送的字节
            byte[] sendBytes;
            //封装消息
            if (extraArgs == null || extraArgs.Length == 0)
            {
                sendBytes = BitConverter.GetBytes((uint)cmd);
            }
            else
            {
                var buffer = new List<byte>(4 + extraArgs.Length);
                buffer.AddRange(BitConverter.GetBytes((uint)cmd));
                buffer.AddRange(extraArgs);
                buffer.ToArray();

                //
                sendBytes = buffer.ToArray();
            }

            //
            LogTool.Logger.LogE($"{TAG}, ->, {cmd}, len={sendBytes.Length}, [{sendBytes.ToStrings()}]");

            //执行发送并接收数据
             var recvd = _helper.Send(sendBytes, cmd);

            //打印返回数据字节流
            if (recvd != null)
            {
                if (recvd.Length < 256)
                {
                    LogTool.Logger.LogE($"{TAG}, <-, {cmd}, len={recvd.Length}, [{recvd.Array.ToStrings(recvd.Length)}]");
                }
                else
                {
                    LogTool.Logger.LogE($"{TAG}, <-, {cmd}, len={recvd.Length}, [{recvd.Array.ToStrings(32)}]");
                }
            }
            else
            {
                LogTool.Logger.LogE($"{TAG}, <-, {cmd}, recvd none");
            }

            //判断请求指令是否与发送指令一致
            if (recvd != null && !IsResponseOfThisCommand(cmd, recvd.Array))
            {
                LogTool.Logger.LogE($"{TAG}, <-, {cmd}, recvd first 4 bytes wrong, xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                return null;
            }

            //返回接收的数据
            return recvd;
        }

        /// <summary>
        /// 是否收发一致
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private bool IsResponseOfThisCommand(CommandCode cmd, byte[] bytes)
        {
            if (bytes == null)
            {
                return false;
            }

            //
            if (!_commandBytes.ContainsKey(cmd))
            {
                _commandBytes[cmd] = BitConverter.GetBytes((uint)cmd);
            }

            //
            var target = _commandBytes[cmd];

            //
            if (bytes.Length < target.Length)
            {
                return false;
            }

            //
            for (int i = 0; i < target.Length; i++)
            {
                if (bytes[i] != target[i])
                {
                    return false;
                }
            }

            //
            return true;
        }

        public readonly ARMTestCommand ARMTestCommand;
    }
}
