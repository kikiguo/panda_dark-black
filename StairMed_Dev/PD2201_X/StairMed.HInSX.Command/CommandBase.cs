﻿namespace StairMed.HInSX.Command
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class CommandBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected HInSXCmd _cmd = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        public CommandBase(HInSXCmd cmd)
        {
            _cmd = cmd;
        }
    }
}
