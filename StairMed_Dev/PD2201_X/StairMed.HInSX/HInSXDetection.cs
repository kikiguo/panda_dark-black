﻿using StairMed.Array;
using StairMed.Enum;
using StairMed.HInSX.Comm.TCP;
using StairMed.HInSX.Comm.USB;
using StairMed.HInSX.Command;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StairMed.HInSX
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXDetection : IDisposable, IHInSXCmdHelper
    {
        const string TAG = nameof(HInSXDetection);

        /// <summary>
        /// 
        /// </summary>
        private NanoTcp5555 _tcp5555 = null;
        private NanoTcp5556 _tcp5556 = null;
        private TamaUsb _usb = null;

        /// <summary>
        /// 
        /// </summary>
        private HInSXCmd _cmd = null;

        /// <summary>
        /// 
        /// </summary>
        private bool _stopQuery = false;

        /// <summary>
        /// 
        /// </summary>
        private bool _started = false;

        /// <summary>
        /// 连接状态
        /// </summary>
        public ConnectState ConnectionState { get; private set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        public HInSXType HInSType { get; private set; } = HInSXType.H_Unknown;

        /// <summary>
        /// 
        /// </summary>
        public bool IsUsbOk { get; private set; } = false;

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            if (_started)
            {
                return;
            }


            _started = true;

            //
            _tcp5555 = new NanoTcp5555();
            _tcp5556 = new NanoTcp5556();
            _usb = new TamaUsb();

            
            _cmd = new HInSXCmd(this);

            ////
            Task.Factory.StartNew(QueryHInSType, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// 查询设备类型
        /// </summary>
        private void QueryHInSType()
        {
            Thread.Sleep(2000);
            var pingFailedCount = 0;

            //
            while (!_stopQuery)
            {
                Thread.Sleep(1000);

                if (_initPing)
                {
                    continue;
                }

                //ping失败则计数，否则计数清零
                if (_cmd.PingCommand.DoPing())
                {
                    ConnectionState = ConnectState.Connected;
                    pingFailedCount = 0;

                    //
                    if (_cmd.DeviceTypeCommand.GetDeviceType(out HInSXType hinsType))
                    {
                        HInSType = hinsType;
                    }

                    //
                    HInSType = HInSXType.HInS_1024;

                    //
                    switch (HInSType)
                    {
                        case HInSXType.HInS_2048:
                            {
                                if (!IsUsbOk)
                                {
                                    if (_usb.OpenUsb())
                                    {
                                        IsUsbOk = true;
                                    }

                                    //
                                    _usb.CloseUsb();
                                }
                            }
                            break;
                        case HInSXType.HInS_1024:
                        default:
                            {
                                IsUsbOk = true;
                            }
                            break;
                    }
                }
                else
                {
                    pingFailedCount++;

                    //ping连续失败3次判断为失联
                    if (pingFailedCount >= 3)
                    {
                        ConnectionState = ConnectState.Disconnected;
                    }
                }
            }
            _tcp5555.ShutDown();
            _tcp5556.ShutDown();
        }


        void IHInSXCmdHelper.HandleRevicedBuffer(PooledArray<byte> bytes)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _stopQuery = true;
        }


        #region IHInSXCmdHelper

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        PooledArray<byte> IHInSXCmdHelper.Send(byte[] bytes, CommandCode code)
        {
            switch (code)
            {
                case CommandCode.StopCollect:
                    {
                        //PooledArray<byte> revd = _tcp5556.SendRecieve(bytes);
                        //_tcp5556.ShutDown();
                        return _tcp5556.SendRecieve(bytes); 
                    }
                    break;
                case CommandCode.GetHeadstageData:
                case CommandCode.StartCollect:
                    {
                        return _tcp5556.SendRecieve(bytes);
                    }
                    break;
                case CommandCode.Ping:
                case CommandCode.GetDAUInfo:
                case CommandCode.GetVersion:
                case CommandCode.SetHeadstageCmd:
                case CommandCode.ReadRegister:
                case CommandCode.GetHeadstageState:
                case CommandCode.GetDeviceType:
                case CommandCode.StartImpedance:
                case CommandCode.QueryImpedanceADC:
                default:
                    {
                        return _tcp5555.SendRecieve(bytes);
                    }
                    break;
            }
        }

        #endregion

        private bool _initPing = false;
        public bool InitPing()
        {
            _initPing = true;
            return _cmd.PingCommand.DoPing();
        }
        public void EndInitPing()
        {
            _initPing = false;
        }
    }
}
