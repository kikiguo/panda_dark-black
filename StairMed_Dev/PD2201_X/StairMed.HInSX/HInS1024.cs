using StairMed.Array;
using StairMed.Entity.Infos.Bases;
using StairMed.Enum;
using StairMed.HInSX.Comm.TCP;
using StairMed.HInSX.Command;
using System.Threading;

namespace StairMed.HInSX
{
    /// <summary>
    /// 
    /// </summary>
    public class HInS1024 : HInSXBase, IHInSXCmdHelper
    {
        private const string TAG = nameof(HInS1024);

        /// <summary>
        /// 数据头偏移
        /// </summary>
        protected override int DataHeadOffset => 64;

        /// <summary>
        /// headstage个数
        /// </summary>
        protected override int HeadStageCount => HInSXCONST.HInS1024_HeadStageCount;

        /// <summary>
        /// 收到的每包数据中，每个通道的采样点个数
        /// </summary>
        protected override int PerChannelSampleCount => 1024;

        /// <summary>
        /// 通信链路
        /// </summary>
        private readonly NanoTcp5555 _tcp5555 = null;
        private readonly NanoTcp5556 _tcp5556 = null;

        /// <summary>
        /// 是否已执行初始化
        /// </summary>
        private bool _inited = false;

        /// <summary>
        /// 是否存在数据流
        /// </summary>
        private bool DataFlowing
        {
            get
            {
                return _tcp5555.DataFlowing || _tcp5556.DataFlowing;
            }
            set
            {
                _tcp5555.DataFlowing = value;
                _tcp5556.DataFlowing = value;
            }
        }

        /// <summary>
        /// 连接状态
        /// </summary>
        private ConnectState _connectionState = ConnectState.Disconnected;
        public override ConnectState ConnectionState
        {
            get { return _connectionState; }
            set
            {
                if (value != _connectionState)
                {
                    _connectionState = value;
                    NotifyConnectionStatusChanged();
                    if (value == ConnectState.Disconnected)
                    {
                        _inited = false;
                        _queryedHardwareVersion = false;
                    }
                }
            }
        }

        /// <summary>
        /// 连接状态字符串描述
        /// </summary>
        private string _connectionTip = string.Empty;
        public override string ConnectionTip
        {
            get { return _connectionTip; }
            protected set
            {
                if (_connectionTip != value)
                {
                    _connectionTip = value;
                }
            }
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public HInS1024(bool demo = false) : base(demo)
        {
            //初始化通信接口
            _tcp5555 = new NanoTcp5555();
#if false
            _tcp5555.SetHelper(this);
#else
            _tcp5555.SetHelper2(this);
#endif
            _tcp5556 = new NanoTcp5556();
            _tcp5556.SetHelper2(this);

            //
            _cmd = new HInSXCmd(this);
        }

        public override void QueryRefreshStates() 
        {
                //查询Headstage是否有插入
                QueryHeadstageState();
                //查询intan芯片的电压和温度
                QueryIntanVoltage();
                QueryIntanTemperature();
        }

       
        /// <summary>
        /// 连接状态、温度、headstage状态查询
        /// </summary>
        protected override void QueryStateLoop()
        {
            Thread.Sleep(500);

            //
            while (!_stopQueryState)
            {

                Thread.Sleep(500);
                //
                if (ConnectionState == ConnectState.Connected)
                {
                    //通过统计3s判断是否正在采集数据
                    if (DataFlowing)
                    {
                        Thread.Sleep(3000);
                        ConnectionState = ConnectState.Connected;
                        _pingFailedCount = 0;
                        continue;
                    }
                }

                if (!_inited)
                {
                    QueryTemperature();
                    //查询Headstage是否有插入
                    QueryHeadstageState();
                    //查询固件版本
                    QueryHardwareVersion();
                    //查询intan芯片的电压和温度
                    QueryIntanVoltage();
                    QueryIntanTemperature();
                    Thread.Sleep(1000);
                    _inited = true;
                }

                //ping失败则计数，否则计数清零
                //if((IsCollecting || ImpedanceTesting) && (!IsStopCollecting))
                if (IsCollecting  || ImpedanceTesting)
                {
#if false //在采集或阻抗测试时不需要ping和查询温度
                    if (!_cmd.PingCommand.DoPing())
                    {

                        if (!DataFlowing)
                        {
                            _pingFailedCount++;
                            Thread.Sleep(1000);
                            if (Demo)
                            {
                                _pingFailedCount = 0;
                            }
                            if (_pingFailedCount > 0 && _pingFailedCount < 3)//if (_pingFailedCount != 0)
                                continue;
                            //ping连续失败3次判断为失联
                            if (_pingFailedCount >= 3)
                            {
                                ConnectionState = ConnectState.Disconnected;
                            }
                        }
                    }
                    QueryTemperature(); 
#endif
                }
                else/* if(!IsStopCollecting)*/
                {
                    if (!_cmd.PingCommand.DoPing())
                    {

                        if (!DataFlowing)
                        {
                            _pingFailedCount++;
                            Thread.Sleep(1000);
                            if (_pingFailedCount > 0 && _pingFailedCount < 3)//if (_pingFailedCount != 0)
                                continue;
                            //ping连续失败3次判断为失联
                            if (_pingFailedCount >= 3)
                            {
                                ConnectionState = ConnectState.Disconnected;
                            }
                        }
                    }
                    else
                    {
                        _pingFailedCount = 0;
                    }
                    QueryStatuses();
                }

                //
                //ConnectionState = ConnectState.Connected;
                //_pingFailedCount = 0;

                //尝试对设备执行初始化

                //

                Thread.Sleep(10000);
            }
        }


        public void QueryStatuses()
        {
                //TryInitDevice();
                //查询温度
                QueryTemperature();
                //查询Headstage是否有插入
                QueryHeadstageState();
                ////查询固件版本
                //QueryHardwareVersion();
                //查询intan芯片的电压和温度
                QueryIntanVoltage();
                QueryIntanTemperature();
        }
#region IHInSXCmdHelper

        void IHInSXCmdHelper.HandleRevicedBuffer(PooledArray<byte> bytes)
        {
            OnHandleRevicedBuffer(bytes);
            if (_tcp5555 != null)
                _tcp5555.DataFlowing = false;
            if (_tcp5556 != null)
                _tcp5556.DataFlowing = false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        PooledArray<byte> IHInSXCmdHelper.Send(byte[] bytes, CommandCode code)
        {
            if (Demo)
            {
                var recvd = new PooledArray<byte>(256);
                System.Array.Copy(bytes, recvd.Array, bytes.Length);

                //
                _tcp5555.DataFlowing = true;
                _tcp5556.DataFlowing = true;

                //
                return recvd;
            }

            //
            switch (code)
            {
                case CommandCode.GetHeadstageData:
                case CommandCode.StartCollect:
                case CommandCode.StopCollect:
                    {
                        // return _tcp5556.SendRecieve(bytes);
                        _tcp5556.Send(bytes);
                        return null;
                    }
                case CommandCode.Ping:
                case CommandCode.GetDAUInfo:
                case CommandCode.GetVersion:
                case CommandCode.SetHeadstageCmd:
                case CommandCode.ReadRegister:
                case CommandCode.GetHeadstageState:
                case CommandCode.GetDeviceType:
                case CommandCode.StartImpedance:
                case CommandCode.QueryImpedanceADC:
                default:
                    {
                        _tcp5555.Send(bytes);
                        return null;
                        //return _tcp5555.SendRecieve(bytes);
                    }
            }
        }

        #endregion
        public override void Dispose()
        {
            base.Dispose();
            if (_tcp5555 != null)
                _tcp5555.CloseNanomsg();
            if (_tcp5556 != null)
                _tcp5556.CloseNanomsg();
        }
    }
}
