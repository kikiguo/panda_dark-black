using StairMed.Array;
using StairMed.Enum;
using StairMed.HInSX.Comm.TCP;
using StairMed.HInSX.Comm.USB;
using StairMed.HInSX.Command;
using System.Threading;

namespace StairMed.HInSX
{
    /// <summary>
    /// 
    /// </summary>
    public class HInS2048 : HInSXBase, IHInSXCmdHelper
    {
        private const string TAG = nameof(HInS2048);

        /// <summary>
        /// 数据头偏移
        /// </summary>
        protected override int DataHeadOffset => 64;

        /// <summary>
        /// headstage个数
        /// </summary>
        protected override int HeadStageCount => HInSXCONST.HInS2048_HeadStageCount;

        /// <summary>
        /// 收到的每包数据中，每个通道的采样点个数
        /// </summary>
        protected override int PerChannelSampleCount => 1024;

        /// <summary>
        /// 通信链路
        /// </summary>
        private readonly NanoTcp5555 _tcp5555 = null;
        private readonly NanoTcp5556 _tcp5556 = null;
        private TamaUsb _usb = null;

        /// <summary>
        /// 是否已执行初始化
        /// </summary>
        private bool _inited = false;

        /// <summary>
        /// 是否存在数据流
        /// </summary>
        private bool DataFlowing
        {
            get
            {
                return _tcp5555.DataFlowing || _tcp5556.DataFlowing || _usb.DataFlowing;
            }
            set
            {
                _tcp5555.DataFlowing = value;
                _tcp5556.DataFlowing = value;
                _usb.DataFlowing = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _firstQuery = true;

        /// <summary>
        /// 连接状态
        /// </summary>
        private ConnectState _connectionState = ConnectState.Disconnected;
        public override ConnectState ConnectionState
        {
            get { return _connectionState; }
            set
            {
                _connectionState = value;
                NotifyConnectionStatusChanged();
                if (value == ConnectState.Disconnected)
                {
                    _inited = false;
                    _queryedHardwareVersion = false;
                }
            }
        }

        /// <summary>
        /// 连接状态字符串描述
        /// </summary>
        private string _connectionTip = string.Empty;
        public override string ConnectionTip
        {
            get { return _connectionTip; }
            protected set
            {
                _connectionTip = value;
                if (string.IsNullOrWhiteSpace(_connectionTip))
                {
                    ConnectionState = ConnectState.Connected;
                }
                else
                {
                    ConnectionState = ConnectState.Disconnected;
                }
            }
        }

        /// <summary>
        /// TCP连接状态
        /// </summary>
        private bool _isTcpOk = false;
        public bool IsTcpOk
        {
            get { return _isTcpOk; }
            set
            {
                if (_isTcpOk != value)
                {
                    _isTcpOk = value;
                    UpdateConnectionState();
                }
            }
        }

        /// <summary>
        /// USB连接状态
        /// </summary>
        private bool _isUsbOk = false;
        public bool IsUsbOk
        {
            get { return _isUsbOk; }
            set
            {
                if (_isUsbOk != value)
                {
                    _isUsbOk = value;
                    UpdateConnectionState();
                }
            }
        }


        /// <summary>
        /// 构造函数
        /// </summary>
        public HInS2048(bool demo = false) : base(demo)
        {
            //初始化通信接口
            _tcp5555 = new NanoTcp5555();
            _tcp5556 = new NanoTcp5556();
            _usb = new TamaUsb();

            //
            _cmd = new HInSXCmd(this);
        }


        void IHInSXCmdHelper.HandleRevicedBuffer(PooledArray<byte> bytes)
        {
            OnHandleRevicedBuffer(bytes);
            _tcp5555.DataFlowing = false;
            _tcp5556.DataFlowing = false;
        }
        #region 连接状态判断

        /// <summary>
        /// 连接状态、温度、headstage状态查询
        /// </summary>
        protected override void QueryStateLoop()
        {
            Thread.Sleep(500);

            //
            while (!_stopQueryState)
            {
                Thread.Sleep(500);

                //
                if (_firstQuery)
                {
                    UpdateConnectionState();
                    _firstQuery = false;
                }

                //
                QueryUsbState();
                QueryTcpConnectionState();

                //
                if (!IsTcpOk || _pingFailedCount != 0)
                {
                    continue;
                }

                //尝试对设备执行初始化
                if (!_inited)
                {
                    TryInitDevice();
                }

                //查询温度
                QueryTemperature();

                //查询Headstage是否有插入
                QueryHeadstageState();

                //查询固件版本
                QueryHardwareVersion();

                //查询intan芯片的电压和温度
                QueryIntanVoltage();
                QueryIntanTemperature();

                //
                Thread.Sleep(10000);
            }
        }

        /// <summary>
        /// 查询usb状态
        /// </summary>
        private void QueryUsbState()
        {
            try
            {
                if (_usb != null)
                {
                    if (!_usb.IsUsbOk)
                    {
                        if (_usb.IsUsbOpened)
                        {
                            _usb.CloseUsb();
                        }

                        //
                        IsUsbOk = _usb.OpenUsb();
                    }
                    else
                    {
                        IsUsbOk = true;
                    }
                }

                //
                if (Demo)
                {
                    IsUsbOk = true;
                }
            }
            catch { }
        }

        /// <summary>
        /// 查询TCP的连接状态
        /// </summary>
        private void QueryTcpConnectionState()
        {
            try
            {
                if (IsTcpOk)
                {
                    DataFlowing = false;

                    //通过统计3s判断是否正在采集数据
                    Thread.Sleep(8000);
                    if (DataFlowing)
                    {
                        IsTcpOk = true;
                        _pingFailedCount = 0;
                        return;
                    }
                }

                //ping失败则计数，否则计数清零
                if (_cmd.PingCommand.DoPing())
                {
                    IsTcpOk = true;
                    _pingFailedCount = 0;
                }
                else
                {
                    _pingFailedCount++;

                    //ping连续失败3次判断为失联
                    if (_pingFailedCount >= 3)
                    {
                        IsTcpOk = false;
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 更新连接状态
        /// </summary>
        private void UpdateConnectionState()
        {
            if (IsTcpOk)
            {
                if (IsUsbOk)
                {
                    ConnectionTip = string.Empty;
                }
                else
                {
                    ConnectionTip = "USB";
                }
            }
            else
            {
                if (IsUsbOk)
                {
                    ConnectionTip = "TCP";
                }
                else
                {
                    ConnectionTip = "USB,TCP";
                }
            }
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            if (_usb != null)
            {
                _usb.CloseUsb();
                _usb = null;
            }
            base.Dispose();
        }


        #region IHInSXCmdHelper

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        PooledArray<byte> IHInSXCmdHelper.Send(byte[] bytes, CommandCode code)
        {
            if (Demo)
            {
                var recvd = new PooledArray<byte>(256);
                System.Array.Copy(bytes, recvd.Array, bytes.Length);

                //
                _tcp5555.DataFlowing = true;
                _tcp5556.DataFlowing = true;
                _usb.DataFlowing = true;

                //
                return recvd;
            }

            //
            switch (code)
            {
                case CommandCode.GetHeadstageData:
                    {
                        return _usb.SendRecieve(bytes);
                    }
                case CommandCode.StartCollect:
                case CommandCode.StopCollect:
                    {
                        return _tcp5556.SendRecieve(bytes);
                    }
                case CommandCode.Ping:
                case CommandCode.GetDAUInfo:
                case CommandCode.GetVersion:
                case CommandCode.SetHeadstageCmd:
                case CommandCode.ReadRegister:
                case CommandCode.GetHeadstageState:
                case CommandCode.GetDeviceType:
                case CommandCode.StartImpedance:
                case CommandCode.QueryImpedanceADC:
                default:
                    {
                        return _tcp5555.SendRecieve(bytes);
                    }
            }
        }

        #endregion
    }
}
