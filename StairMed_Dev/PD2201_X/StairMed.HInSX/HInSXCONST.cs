﻿using StairMed.HInSX.Command;

namespace StairMed.HInSX
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXCONST
    {
        /// <summary>
        /// 
        /// </summary>
        public const int HInS1024_HeadStageCount = 8;
        public const int HInS1024_ChannelCount = 1024;     
        
        /// <summary>
        /// 
        /// </summary>
        public const int HInS2048_HeadStageCount = 16;
        public const int HInS2048_ChannelCount = 2048;

        //设备通道个数
        public static int CHANNELS_COUNT = 1024;

        /// <summary>
        /// 
        /// </summary>
        public static int HeadStageCount = 8;

        /// <summary>
        /// 
        /// </summary>
        private static HInSXType _hinsType = HInSXType.HInS_1024;
        public static HInSXType HInSXType
        {
            get
            {
                return _hinsType;
            }
            set
            {
                _hinsType = value;
                switch (_hinsType)
                {
                    case HInSXType.HInS_1024:
                        {
                            CHANNELS_COUNT = 1024;
                            HeadStageCount = 8;
                        }
                        break;
                    case HInSXType.HInS_2048:
                        {
                            CHANNELS_COUNT = 2048;
                            HeadStageCount = 16;
                        }
                        break;
                    default:
                        {
                            CHANNELS_COUNT = 1024;
                            HeadStageCount = 8;
                        }
                        break;
                }
            }
        }
    }
}
