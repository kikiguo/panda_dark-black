using StairMed.Array;
using StairMed.AsyncFIFO;
using StairMed.ChipConst.Intan;
using StairMed.Core.Settings;
using StairMed.Entity.DataPools;
using StairMed.Enum;
using StairMed.FilterProcess;
using StairMed.HInSX.Command;
using StairMed.HInSX.Command.Commands.Impedance;
using StairMed.HInSX.Logger;
using StairMed.Impedance;
using StairMed.Logger;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using static System.Net.Mime.MediaTypeNames;

namespace StairMed.HInSX
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class HInSXBase : IDisposable, IBatchImpedanceHelper
    {
        private const string TAG = nameof(HInSXBase);

        private ConcurrentQueue<int[]> trigsQueue = new ConcurrentQueue<int[]>();

        /// <summary>
        /// 数据头偏移
        /// </summary>
        protected virtual int DataHeadOffset => 64;

        /// <summary>
        /// headstage个数
        /// </summary>
        protected virtual int HeadStageCount => HInSXCONST.HInS1024_HeadStageCount;

        /// <summary>
        /// Intan2164芯片个数
        /// </summary>
        protected int DeviceIntan2164Count => HeadStageCount * 2;

        /// <summary>
        /// 收到的每包数据中，每个通道的采样点个数
        /// </summary>
        protected virtual int PerChannelSampleCount => 1024;

        /// <summary>
        /// 随机数
        /// </summary>
        protected Random _rand = new Random(DateTime.Now.Millisecond);

        /// <summary>
        /// 连接状态发生变化
        /// </summary>
        public event Action<ConnectState, string> OnConnectionStatusChanged;

        /// <summary>
        /// 温度发生变化
        /// </summary>
        public event Action<double> OnTemperatureChanged;

        /// <summary>
        /// HeadStage状态变化
        /// </summary>
        public event Action<int, bool> OnHeadStageInsertedStateChanged;

        /// <summary>
        /// 
        /// </summary>
        public event Action<int, double> OnIntanVoltageChanged;

        /// <summary>
        /// 
        /// </summary>
        public event Action<int, double> OnIntanTemperatureChanged;

        /// <summary>
        /// channelCount, sampleCount, raws, wfps, hfps, lfps, spks, recvdFrameCount, lostFrameCount
        /// </summary>
        public event Action<int, int, PooledArray<float>, PooledArray<float>, PooledArray<float>, PooledArray<float>, PooledArray<float>, PooledArray<int>, long, long> DataReport;

        /// <summary>
        /// 数据chunk处理线程
        /// </summary>
        private readonly IAsyncFIFO _chunkFIFO = null;

        /// <summary>
        /// 命令
        /// </summary>
        protected HInSXCmd _cmd = null;

        /// <summary>
        /// 滤波处理器
        /// </summary>
        private readonly IXPU _xpu = null; //数据处理

        /// <summary>
        /// 
        /// </summary>
        private int _channelCount = 1024; //通道数
        private int _sampleRate = 25 * 1000; //采样率

        /// <summary>
        /// 连接状态
        /// </summary>
        public abstract ConnectState ConnectionState { get; set; }

        /// <summary>
        /// 连接状态字符串描述
        /// </summary>
        public abstract string ConnectionTip { get; protected set; }

        /// <summary>
        /// 温度
        /// </summary>
        private readonly Queue<double> _historyTemperatureQueue = new Queue<double>();
        private double _temperature = 0.0;
        public double Temperature
        {
            get { return _temperature; }
            set
            {
                if (_temperature != value)
                {
                    _temperature = value;
                    OnTemperatureChanged?.Invoke(value);
                }
            }
        }

        /// <summary>
        /// 是否已启动
        /// </summary>
        private bool _started = false;

        /// <summary>
        /// 停止状态查询：释放对象时停止
        /// </summary>
        protected bool _stopQueryState = false;

        /// <summary>
        /// 是否已查询过硬件版本
        /// </summary>
        protected bool _queryedHardwareVersion = false;

        /// <summary>
        /// DEMO运行
        /// </summary>

        private bool _demo = false;
        public bool Demo
        {
            get { return _demo; }
            set { _demo = value; }
        }

        /// <summary>
        /// 拉取数据的循环
        /// </summary>
        private readonly AutoResetEvent _pullRawResetEvent = new AutoResetEvent(false);

        private readonly AutoResetEvent _pullDemoRawResetEvent = new AutoResetEvent(false);

        /// <summary>
        /// 循环拉取阻抗测试数据
        /// </summary>
        private readonly AutoResetEvent _pullImpedanceResetEvent = new AutoResetEvent(false);

        /// <summary>
        /// 采集数据状态
        /// </summary>
        private bool _isCollecting = false;
        public bool IsCollecting
        {
            get { return _isCollecting; }
            set { _isCollecting = value; }
        }

        private bool _isStopCollecting = false;
        public bool IsStopCollecting
        {
            get { return _isStopCollecting; }
            set { _isStopCollecting = value; }
        }


        /// <summary>
        /// 是否正在阻抗测试
        /// </summary>
        private bool _impedanceTesting = false;
        public bool ImpedanceTesting
        {
            get { return _impedanceTesting; }
            set { _impedanceTesting = value; }
        }

        /// <summary>
        /// headstage插入状态
        /// </summary>
        private List<bool> _headStageInsertedStates = new List<bool> { };
        public List<bool> HeadstageInsertedStates
        {
            get { return _headStageInsertedStates; }
            set { _headStageInsertedStates = value; }
        }

        /// <summary>
        /// Intan温度
        /// </summary>
        protected readonly Dictionary<int, Queue<double>> _intan2164HistoryTemperatureDict = new Dictionary<int, Queue<double>>();
        private List<double> _intanTemperatures = new List<double> { };
        public List<double> IntanTemperatures
        {
            get { return _intanTemperatures; }
            set { _intanTemperatures = value; }
        }

        /// <summary>
        /// Intan电压
        /// </summary>
        private List<double> _intanVoltages = new List<double> { };
        public List<double> IntanVoltages
        {
            get { return _intanVoltages; }
            set { _intanVoltages = value; }
        }



        /// <summary>
        /// 
        /// </summary>
        public HInSXBase(bool demo)
        {
            Demo = demo;

            //
            _chunkFIFO = new BlockFIFO { Name = nameof(_chunkFIFO), ConsumerAction = ChunkConsume };

            //
            _xpu = XPUFactory.Instance.GetGPU();
            _xpu.ParamCenter.SetRawKB(IntanConst.CoefK, IntanConst.CoefB);

            //
            HeadstageInsertedStates.Clear();
            for (int i = 0; i < HeadStageCount; i++)
            {
                HeadstageInsertedStates.Add(false);
            }
            for (int i = 0; i < DeviceIntan2164Count; i++)
            {
                IntanVoltages.Add(0);
                IntanTemperatures.Add(0);
                _intan2164HistoryTemperatureDict[i] = new Queue<double>();
            }
        }



        #region 读取状态

        /// <summary>
        /// 连接状态、温度、headstage状态查询
        /// </summary>
        protected abstract void QueryStateLoop();

        /// <summary>
        /// 尝试对设备执行初始化
        /// </summary>
        protected void TryInitDevice()
        {
            //_inited = _cmd.InitializerCommand.InitFireware();
        }

        /// <summary>
        /// 查询温度
        /// </summary>
        protected void QueryTemperature()
        {
            _cmd.GetDAUInfoCommand.GetDAUInfo(out double temperature);
            //if (_cmd.GetDAUInfoCommand.GetDAUInfo(out double temperature))
            //{
            //    _historyTemperatureQueue.Enqueue(temperature);
            //    while (_historyTemperatureQueue.Count > 5)
            //    {
            //        _historyTemperatureQueue.Dequeue();
            //    }
            //    Temperature = _historyTemperatureQueue.Average();
            //}
        }

        private bool _bStopCollect = true;
        public void OnQueryTemperature(double dTemperature)
        {
            _historyTemperatureQueue.Enqueue(dTemperature);
            while (_historyTemperatureQueue.Count > 5)
            {
                _historyTemperatureQueue.Dequeue();
            }
            Temperature = _historyTemperatureQueue.Average();
        }

        public bool GetBit(byte b, int index)
        {
            bool bit = (b & (1 << index)) != 0;
            return bit;
        }

        /// <summary>
        /// 查询Headstage是否有插入的状态
        /// </summary>
        protected void QueryHeadstageState()
        {
            //数据采集过程中，获取的状态不靠谱
            if (IsCollecting)
            {
                return;
            }
            _cmd.HSStatusCommand.GetHeadstageStatus(HeadStageCount, out List<bool> headstageInsertedStates);


            //if (IsCollecting)
            //{
            //    return;
            //}

            //if (!_cmd.HSStatusCommand.GetHeadstageStatus(HeadStageCount, out List<bool> headstageInsertedStates))
            //{
            //    return;
            //}

            //if (headstageInsertedStates.Count < HeadStageCount)
            //{
            //    var count = headstageInsertedStates.Count;
            //    for (int i = count; i < HeadStageCount; i++)
            //    {
            //        headstageInsertedStates.Add(false);
            //    }
            //}

            ////
            //for (int i = 0; i < HeadstageInsertedStates.Count; i++)
            //{
            //    if (HeadstageInsertedStates[i] != headstageInsertedStates[i])
            //    {
            //        HeadstageInsertedStates[i] = headstageInsertedStates[i];
            //        OnHeadStageInsertedStateChanged?.Invoke(i, headstageInsertedStates[i]);
            //    }
            //}

        }

        public void OnQueryHeadstageState(List<bool> headstageInsertedStates)
        {
            if (headstageInsertedStates.Count < HeadStageCount)
            {
                var count = headstageInsertedStates.Count;
                for (int i = count; i < HeadStageCount; i++)
                {
                    headstageInsertedStates.Add(false);
                }
            }

            //
            for (int i = 0; i < HeadstageInsertedStates.Count; i++)
            {
                if (HeadstageInsertedStates[i] != headstageInsertedStates[i])
                {
                    HeadstageInsertedStates[i] = headstageInsertedStates[i];
                    OnHeadStageInsertedStateChanged?.Invoke(i, headstageInsertedStates[i]);
                }
            }
        }

        public void OnHandleRevicedBuffer(PooledArray<byte> bytes)
        {
            try
            {
                CommandCode nCmd = (CommandCode)BitConverter.ToUInt32(bytes.Array, 0);
                switch (nCmd)
                {
                    case CommandCode.GetDAUInfo:
                        {
                            if (bytes == null || bytes.Length != 24)
                            {
                                break;
                            }
                            double dtemperature = BitConverter.ToSingle(bytes.Array, 8);
                            OnQueryTemperature(dtemperature);
                            LogTool.Logger.LogT($"{nameof(CommandCode.GetDAUInfo)} get temperature:{dtemperature}");
                        }
                        break;
                    case CommandCode.GetHeadstageState:
                        {

                            if (bytes == null || bytes.Length != 8)
                            {
                                break;
                            }

                            List<bool> headstageStates = new List<bool>();
                            for (int i = 0; i < HeadStageCount; i++)
                            {
                                headstageStates.Add(false);
                            }
                            var offset = 4;
                            for (int i = 0; i < HeadStageCount; i++)
                            {
                                //每个bit代表一个headstage的状态
                                //if (recvd[offset+i] ==  && recvd[offset + i +1] == 0)
                                //{
                                //    headstageStates[i/ 2] = true;
                                //}
                                var b = bytes[offset + i / 4];
                                bool bit1 = GetBit(b, (i % 4) * 2);
                                bool bit2 = GetBit(b, ((i % 4) * 2 + 1));
                                var exist = !bit1 && bit2;
                                headstageStates[i] = exist;

                            }

                            OnQueryHeadstageState(headstageStates);

                        }
                        break;
                    case CommandCode.ReadIntanVoltage:
                        {
                            if (bytes == null || bytes.Length < 12)
                            {
                                break;
                            }
#if true
                            int m_headstageIndex = 0;
                            int m_a = 0;
                            m_headstageIndex = BitConverter.ToUInt16(bytes.Array, 4);
                            for (; m_a < HeadStageCount; m_a++)
                            {
                                double voltageA = 0.0;
                                double voltageB = 0.0;

                                if (_headStageInsertedStates[m_a])
                                {//inserted
                                    if (m_headstageIndex == m_a)
                                    {//only update receive index
                                        voltageA = BitConverter.ToUInt16(bytes.Array, 8) / 100.0;
                                        voltageB = BitConverter.ToUInt16(bytes.Array, 12) / 100.0;
                                        OnQueryIntanVoltage(m_a, voltageA, voltageB);
                                    }
                                }
                                else
                                {//un-inserted
                                    OnQueryIntanVoltage(m_a, voltageA, voltageB);
                                }
                            }
#else
                for (; _headstageIndex < HeadStageCount; _headstageIndex++)
                {
                    double voltageA = 0.0;
                    double voltageB = 0.0;
                    if (_headStageInsertedStates[_headstageIndex])
                    {
                        voltageA = BitConverter.ToUInt16(bytes.Array, 4) / 100.0;
                        voltageB = BitConverter.ToUInt16(bytes.Array, 8) / 100.0;
                    }                    
                    OnQueryIntanVoltage(_headstageIndex, voltageA, voltageB);
                }
#endif
                        }
                        break;
                    case CommandCode.ReadIntanTemperature:
                        {
                            if (bytes == null || bytes.Length < 12)
                            {
                                break;
                            }
#if true
                            int m_temperatureIndex = BitConverter.ToUInt16(bytes.Array, 4);
                            int m_a = 0;
                            for (; m_a < HeadStageCount; m_a++)
                            {
                                double temperatureA = 0.0;
                                double temperatureB = 0.0;
                                if (_headStageInsertedStates[m_a])
                                {//inserted
                                    if (m_temperatureIndex == m_a)
                                    {//only update receive index
                                        temperatureA = BitConverter.ToUInt16(bytes.Array, 8) / 100.0;
                                        temperatureB = BitConverter.ToUInt16(bytes.Array, 12) / 100.0;
                                        OnQueryIntanTemperature(m_a, temperatureA, temperatureB);
                                    }
                                }
                                else
                                {//not inserted
                                    OnQueryIntanTemperature(m_a, temperatureA, temperatureB);
                                }
                            }
#else
                for (; _temperatureIndex < HeadStageCount; _temperatureIndex++)
                {
                    double temperatureA = 0.0;
                    double temperatureB = 0.0;
                    if (_headStageInsertedStates[_temperatureIndex])
                    {
                        temperatureA = BitConverter.ToUInt16(bytes.Array, 4) / 100.0;
                        temperatureB = BitConverter.ToUInt16(bytes.Array, 8) / 100.0;
                    }                    
                    OnQueryIntanTemperature(_temperatureIndex, temperatureA, temperatureB);

                    }
#endif
                        }
                        break;
                    case CommandCode.StartCollect:
                        {
                            IsCollecting = true;
                            _pullRawResetEvent.Set();
                        }
                        break;
                    case CommandCode.GetHeadstageData:
                        {
                            if (bytes == null || bytes.Length < 512)
                            {
                                break;
                            }
                            OnGetHeadstageData(bytes);
                            return;
                        }
                        break;
                    case CommandCode.StopCollect:
                        {
                            if (_bStopCollect)
                            {
                                _bStopCollect = false;
                                Thread.Sleep(3000);
                                _cmd.StopCollectCommand.StopCollect();
                                IsCollecting = false;
                            }
                            else
                            {
                                IsStopCollecting = false;
                            }

                        }
                        break;
                    case CommandCode.QueryImpedanceADC:
                        {
                            if (bytes == null || bytes.Length != GetHSResistanceCommand.ImpedanceSampleByteCount * GetHSResistanceCommand.CapCount + 35)
                            {

                                break;
                            }
                            OnGetImpedance(bytes);
                        }
                        break;
                    case CommandCode.Ping:
                        {
                            _pingFailedCount = 0;
                            ConnectionState = ConnectState.Connected;
                        }
                        break;
                    case CommandCode.GetVersion:
                        OnGetVersion(bytes);
                        break;
                    case CommandCode.SetHeadstageCmd:
                    case CommandCode.ReadRegister:
                    case CommandCode.GetDeviceType:
                    case CommandCode.StartImpedance:
                    case CommandCode.GetHeadstageStateOld:
                        {
                        }
                        break;
                }
            }
            catch
            {
                ;
            }
            bytes.Dispose();
        }

        private void OnGetVersion(PooledArray<byte> bytes)
        {
            string firmware = "";
            string fpga = "";
            string linux = "";
            string hardwareConfiguration = "";
            try
            {
                var strs = Encoding.ASCII.GetString(bytes.Array, 4, bytes.Array.Length - 4);
                var parts = strs.Split("\0").Where(r => !string.IsNullOrWhiteSpace(r)).ToList();
                if (parts.Count == 4)
                {
                    firmware = parts[0];
                    try
                    {
                        var fpgaOri = parts[1];

                        var strFpga = Convert.ToString(Convert.ToInt32(fpgaOri), 16);
                        if (strFpga.Length > 1)
                        {
                            strFpga = strFpga.Substring(1, strFpga.Length - 1);
                            if (strFpga.Length % 2 != 0)
                            {
                                strFpga = $"0{strFpga}";
                            }
                            for (int i = 0; i < strFpga.Length; i += 2)
                            {
                                string strV = strFpga.Substring(i, 2);
                                fpga += Convert.ToInt32(strV).ToString() + ".";
                            }
                            if (fpga.EndsWith("."))
                            {
                                fpga = fpga.Substring(0, fpga.Length - 1);
                            }
                        }
                    }
                    catch
                    {
                        ;
                    }

                    linux = parts[2];
                    hardwareConfiguration = parts[3];
                }
            }
            catch
            {

            }
            OnVersionChanged?.Invoke(firmware, fpga, linux, hardwareConfiguration);
        }

        /// <summary>
        /// 查询intan温度
        /// </summary>
        protected void QueryIntanTemperature()
        {

            if (IsCollecting)
            {
                return;
            }

            if (ConnectionState != ConnectState.Connected)
            {
                return;
            }
            //
            _temperatureIndex = 0;
            for (int headstage = 0; headstage < HeadStageCount; headstage++)
            {
                if (!_headStageInsertedStates[headstage])
                {
                    continue;
                }
                _cmd.ReadIntanTemperatureCommand.ReadIntanTemperature(headstage, out double temperatureA, out double temperatureB);
            }
            ////数据采集过程中，获取的状态不靠谱
            //if (IsCollecting)
            //{
            //    return;
            //}

            ////
            //for (int headstage = 0; headstage < HeadStageCount; headstage++)
            //{
            //    if (!_headStageInsertedStates[headstage])
            //    {
            //        continue;
            //    }

            //    //
            //    if (ConnectionState != ConnectState.Connected)
            //    {
            //        return;
            //    }

            //    //数据采集过程中，获取的状态不靠谱
            //    if (IsCollecting)
            //    {
            //        return;
            //    }

            //    //
            //    if (!_cmd.ReadIntanTemperatureCommand.ReadIntanTemperature(headstage, out double temperatureA, out double temperatureB))
            //    {
            //        continue;
            //    }

            //    //数据采集过程中，获取的状态不靠谱
            //    if (IsCollecting)
            //    {
            //        return;
            //    }

            //    //每个headstage有2片RHD2164
            //    UpdateIntan2164Temperature(headstage * 2, temperatureA);
            //    UpdateIntan2164Temperature(headstage * 2 + 1, temperatureB);
            //}
        }

        public void OnQueryIntanTemperature(int headstage, double temperatureA, double temperatureB)
        {
            UpdateIntan2164Temperature(headstage * 2, temperatureA);
            UpdateIntan2164Temperature(headstage * 2 + 1, temperatureB);
        }

        /// <summary>
        /// 更新RHD2164的温度状态
        /// </summary>
        /// <param name="index"></param>
        /// <param name="newTemperature"></param>
        private void UpdateIntan2164Temperature(int index, double newTemperature)
        {
            var temperature = GetSmoothTemperature(index, newTemperature);
            if (IntanTemperatures[index] != temperature)
            {
                IntanTemperatures[index] = temperature;
                OnIntanTemperatureChanged?.Invoke(index, temperature);
            }
        }

        /// <summary>
        /// 缓存采集到的温度，以最近N次的平均值作为当前温度
        /// </summary>
        /// <param name="index"></param>
        /// <param name="newTemperature"></param>
        /// <returns></returns>
        private double GetSmoothTemperature(int index, double newTemperature)
        {
            var queue = _intan2164HistoryTemperatureDict[index];
            queue.Enqueue(newTemperature);

            //
            while (queue.Count > 5)
            {
                queue.Dequeue();
            }

            //
            return queue.Average();
        }

        private int _headstageIndex = 0;
        private int _temperatureIndex = 0;
        private int _impedanceIndex = 0;
        private int _frameLen = 0;
        /// <summary>
        /// ping失败计数:失败3次即为连接失败
        /// </summary>
        protected int _pingFailedCount = 0;

        /// <summary>
        /// 查询intan电压
        /// </summary>
        protected void QueryIntanVoltage()
        {
            //数据采集过程中，获取的状态不靠谱
            if (IsCollecting)
            {
                return;
            }

            if (ConnectionState != ConnectState.Connected)
            {
                return;
            }

            //
            _headstageIndex = 0;
            for (int headstage = 0; headstage < HeadStageCount; headstage++)
            {
                if (!_headStageInsertedStates[headstage])
                {
                    continue;
                }

                _cmd.ReadIntanVoltageCommand.ReadIntanVoltage(headstage, out double voltageA, out double voltageB);
            }

            ////数据采集过程中，获取的状态不靠谱
            //if (IsCollecting)
            //{
            //    return;
            //}

            ////
            //for (int headstage = 0; headstage < HeadStageCount; headstage++)
            //{
            //    if (!_headStageInsertedStates[headstage])
            //    {
            //        continue;
            //    }

            //    //
            //    if (ConnectionState != ConnectState.Connected)
            //    {
            //        return;
            //    }

            //    //数据采集过程中，获取的状态不靠谱
            //    if (IsCollecting)
            //    {
            //        return;
            //    }

            //    //
            //    if (!_cmd.ReadIntanVoltageCommand.ReadIntanVoltage(headstage, out double voltageA, out double voltageB))
            //    {
            //        continue;
            //    }

            //    //数据采集过程中，获取的状态不靠谱
            //    if (IsCollecting)
            //    {
            //        return;
            //    }

            //    //每个headstage有2片RHD2164
            //    {
            //        var index = headstage * 2;
            //        if (IntanVoltages[index] != voltageA)
            //        {
            //            IntanVoltages[index] = voltageA;
            //            OnIntanVoltageChanged?.Invoke(index, voltageA);
            //        }
            //    }
            //    {
            //        var index = headstage * 2 + 1;
            //        if (IntanVoltages[index] != voltageB)
            //        {
            //            IntanVoltages[index] = voltageB;
            //            OnIntanVoltageChanged?.Invoke(index, voltageB);
            //        }
            //    }
            //}
        }

        public void OnQueryIntanVoltage(int headstage, double voltageA, double voltageB)
        {
            //每个headstage有2片RHD2164
            {
                var index = headstage * 2;
                if (IntanVoltages[index] != voltageA)
                {
                    IntanVoltages[index] = voltageA;
                    OnIntanVoltageChanged?.Invoke(index, voltageA);
                }
            }
            {
                var index = headstage * 2 + 1;
                if (IntanVoltages[index] != voltageB)
                {
                    IntanVoltages[index] = voltageB;
                    OnIntanVoltageChanged?.Invoke(index, voltageB);
                }
            }
        }
        /// <summary>
        /// 查询固件版本
        /// </summary>
        public void QueryHardwareVersion()
        {
            //return;
            if (IsCollecting)
            {
                return;
            }

            if (ConnectionState != ConnectState.Connected)
            {
                return;
            }
            //if (!_queryedHardwareVersion)
            {
                if (_cmd.VersionCommand.GetVersionInfo(out string firmware, out string fpga, out string linux, out string hardwareConfiguration))
                {
                    LogTool.Logger.LogI($"version: firmware={firmware}, fpga={fpga}, linux={linux}, hardwareConfiguration={hardwareConfiguration}");
                    //_queryedHardwareVersion = true;
                }
            }
        }

        #endregion



        #region 运行控制

        /// <summary>
        /// 连接初始化
        /// </summary>
        /// <returns></returns>
        public void Connect()
        {
            if (!_started)
            {
                _started = true;

                //
                Task.Factory.StartNew(QueryStateLoop, TaskCreationOptions.LongRunning);
                Task.Factory.StartNew(DemoPullRawDataLoop, TaskCreationOptions.LongRunning);
                Task.Factory.StartNew(PullRawDataLoop, TaskCreationOptions.LongRunning);
                //
                //if (_demo)
                //{
                //    Task.Factory.StartNew(DemoPullRawDataLoop, TaskCreationOptions.LongRunning);
                //}
                //else
                //{
                //    Task.Factory.StartNew(PullRawDataLoop, TaskCreationOptions.LongRunning);
                //}

                //
                Task.Factory.StartNew(PullImpedanceLoop, TaskCreationOptions.LongRunning);
            }
        }

        /// <summary>
        /// 开始采集
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="inputChannels"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public bool StartCollect(int sampleRate, HashSet<int> inputChannels, out string errMsg)
        {
            //ResetChannelImpedance();
            errMsg = string.Empty;

            //
            _sampleRate = sampleRate;
            _channelCount = inputChannels.Count;
            //
            _xpu.Free();


            IsStopCollecting = false;
            if (IsCollecting)
            {
                if (_bStopCollect)
                {
                    _bStopCollect = false;
                    IsCollecting = false;
                }
                _cmd.StopCollectCommand.StopCollect();
                IsCollecting = false;
                errMsg = "还在采集中";
                return false;
            }
            //
            if (ConnectionState != ConnectState.Connected)
            {
                errMsg = "设备未连接";
                return false;
            }

            do
            {
                _cmd.StartCollectCommand.StartCollect();
                //if (!_cmd.StartCollectCommand.StartCollect())
                //{
                //    errMsg = "启动失败";
                //    break;
                //}

                IsCollecting = true;
                if (Demo)
                {
                    _pullDemoRawResetEvent.Set();
                }
                else
                {
                    _pullRawResetEvent.Set();
                }
                return true;
            } while (false);

            //
            return false;
        }

        /// <summary>
        /// 停止采集
        /// </summary>
        public virtual bool StopCollect()
        {
            IsStopCollecting = true;
            _bStopCollect = true;
            _cmd.StopCollectCommand.StopCollect();
            Thread.Sleep(4000);
#if true //似乎比停止采集命令更早执行
            IsCollecting = false;
#endif
            return true;
            //
            //Thread.Sleep(100);
            //IsCollecting = false;

            ////
            //var repeat = ConnectionState != ConnectState.Connected ? 2 : 5;
            //while (repeat > 0)
            //{
            //    if (_cmd.StopCollectCommand.StopCollect())
            //    {
            //        Thread.Sleep(100);
            //        return true;
            //    }
            //    repeat--;
            //}

            ////
            //return ConnectionState != ConnectState.Connected;
        }

        /// <summary>
        /// 阻抗测试
        /// </summary>
        /// <param name="channelSet"></param>
        /// <param name="freq"></param>
        /// <param name="sampleRate"></param>
        /// <param name=""></param>
        public bool ImpedanceTest(List<int> inputChannels, bool applyNotch, double fNotch, double bandwidth, double freq, double sampleRate, double lowerBandwidth, double upperBandwidth, bool enableDSP, double dspCutoff, Action<int, double, double> callback, out string errMsg)
        {
            //
            if (ConnectionState != ConnectState.Connected)
            {
                errMsg = "设备未连接";
                return false;
            }

            //
            if (ImpedanceTesting)
            {
                errMsg = "正在进行阻抗测试";
                return false;
            }

            //
            var watch = new Stopwatch();
            watch.Start();
            _callback = callback;
            //
            var isSucceed = new BatchImpedanceTest(this, 3000).ImpedanceTest(inputChannels, applyNotch, fNotch, bandwidth, freq, sampleRate, lowerBandwidth, upperBandwidth, enableDSP, dspCutoff, callback, out errMsg);


            //
            LogTool.Logger.LogT($"{nameof(BatchImpedanceTest)} cost:{watch.ElapsedMilliseconds}ms");

            //
            if (isSucceed)
            {
                Thread.Sleep(500);
            }
            else
            {
                Thread.Sleep(2000);
            }

            //
            return isSucceed;
        }

        #endregion



        #region 数据处理参数

        /// <summary>
        /// 高通滤波
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="filterType"></param>
        public void UpdateHighPassFilter(int cutoff, int order, FilterType filterType)
        {
            _xpu.ParamCenter.SetHighPassFilter(cutoff, order, filterType);
        }

        /// <summary>
        /// 低通滤波
        /// </summary>
        /// <param name="cutoff"></param>
        /// <param name="order"></param>
        /// <param name="filterType"></param>
        public void UpdateLowPassFilter(int cutoff, int order, FilterType filterType)
        {
            _xpu.ParamCenter.SetLowPassFilter(cutoff, order, filterType);
        }

        /// <summary>
        /// 陷波滤波
        /// </summary>
        /// <param name="notchHz"></param>
        /// <param name="notchBandwidth"></param>
        public void UpdateNotchFilter(int notchHz, int notchBandwidth = 10)
        {
            _xpu.ParamCenter.SetNotchFilter(notchHz, notchBandwidth);
        }

        /// <summary>
        /// spike检测阈值
        /// </summary>
        /// <param name="thresholds"></param>
        public void UpdateSpikeParam(List<float> thresholds)
        {
            _xpu.ParamCenter.SetSpikeParam(thresholds);
        }

        /// <summary>
        /// spike检测阈值
        /// </summary>
        /// <param name="threshold"></param>
        public void UpdateSpikeParam(float threshold)
        {
            _xpu.ParamCenter.SetSpikeParam(threshold);
        }

        #endregion



        #region 寄存器读写

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="coreIndex"></param>
        /// <param name="registerIndex"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool ReadRegister(uint headstageIndex, uint coreIndex, uint registerIndex, out byte val)
        {
            return _cmd.ReadRegisterCommand.ReadRegister(headstageIndex, coreIndex, registerIndex, out val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="coreIndex"></param>
        /// <param name="registerIndex"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool WriteRegister(uint headstageIndex, uint coreIndex, uint registerIndex, byte val)
        {
            return _cmd.WriteRegisterCommand.WriteRegister(headstageIndex, coreIndex, registerIndex, val);
        }

        #endregion



        #region 数据获取

        /// <summary>
        /// 丢包统计
        /// </summary>
        const int FrameIndexCycle = 0x1f;
        private uint _frameIndex = uint.MaxValue; //帧序号，用于计算丢包
        private long _recvdFrameCount = 0;
        private long _lostFrameCount = 0;
        private bool _recvdFirstFrame = false;

        /// <summary>
        /// PC内部起1个50ms定时器，轮询读，保证读出数率>写入数率即可.
        /// </summary>
        /// 
        private void PullRawDataLoop()
        {
            _frameLen = _channelCount * 2 * PerChannelSampleCount + DataHeadOffset;

            //
            while (true)
            {
                Thread.Sleep(30);

                //
                if (IsCollecting)
                {
                    if (ConnectionState != ConnectState.Connected)
                    {
                        IsCollecting = false;
                    }
                    else
                    {

                        if (!IsCollecting)
                            return;
                        _cmd.HeadstageDataCommand.GetHeadstageData(out PooledArray<byte> bytes);
                    }
                }
                else
                {
                    _pullRawResetEvent.WaitOne();
                    _frameLen = _channelCount * 2 * PerChannelSampleCount + DataHeadOffset;
                    //
                    _recvdFirstFrame = false;
                    _recvdFrameCount = 0;
                    _lostFrameCount = 0;
                }
            }
        }

        public void OnGetHeadstageData(PooledArray<byte> bytes)
        {
            var newFrameIndex = BitConverter.ToUInt32(bytes.Array, 8);
            var lost = FrameLostCheck(newFrameIndex);

            //
            if (bytes.Length != _frameLen)
            {
                LogTool.Logger.LogF($"{TAG}, frame len!=chl{_channelCount} * {PerChannelSampleCount} * 2 + {DataHeadOffset}， [64->{bytes.Length - _channelCount * PerChannelSampleCount * 2}] xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            }
            //
            _chunkFIFO.Add(bytes);
        }
        //private void PullRawDataLoop()
        //{
        //    var frameLen = _channelCount * 2 * PerChannelSampleCount + DataHeadOffset;

        //    //
        //    while (true)
        //    {
        //        Thread.Sleep(5);

        //        //
        //        if (IsCollecting)
        //        {
        //            if (ConnectionState != ConnectState.Connected)
        //            {
        //                IsCollecting = false;
        //            }
        //            else
        //            {
        //                if (false)
        //                {
        //                    Logger.LogTool.Logger.LogF($"pull once");
        //                    _cmd.HeadstageDataCommand.GetHeadstageData(out PooledArray<byte> bytes1);
        //                    bytes1.Dispose();
        //                }
        //                else
        //                {
        //                    if (!IsCollecting)
        //                        return;
        //                    if (_cmd.HeadstageDataCommand.GetHeadstageData(out PooledArray<byte> bytes))
        //                    {
        //                        //
        //                        var newFrameIndex = BitConverter.ToUInt32(bytes.Array, 8);
        //                        var lost = FrameLostCheck(newFrameIndex);

        //                        //
        //                        if (bytes.Length != frameLen)
        //                        {
        //                            LogTool.Logger.LogF($"{TAG}, frame len!=chl{_channelCount} * {PerChannelSampleCount} * 2 + {DataHeadOffset}， [64->{bytes.Length - _channelCount * PerChannelSampleCount * 2}] xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        //                        }
        //                        else if (false)
        //                        {
        //                            if (false)
        //                            {
        //                                var builder = new StringBuilder(_channelCount * 10);
        //                                for (int j = 0; j < _channelCount; j++)
        //                                {
        //                                    builder.Append($"{bytes[j * 2 + 1 + DataHeadOffset]:X2}{bytes[j * 2 + DataHeadOffset]:X2}, ");
        //                                }
        //                                LogTool.Logger.LogF($"{TAG}, [{builder.ToString()}]");
        //                            }
        //                            else
        //                            {
        //                                //
        //                                var builder = new StringBuilder(_channelCount * PerChannelSampleCount * 2 * 2);
        //                                for (int i = 0; i < DataHeadOffset / 2; i++)
        //                                {
        //                                    builder.Append($"{bytes[i * 2 + 1]:X2}{bytes[i * 2]:X2}, ");
        //                                }
        //                                builder.AppendLine();

        //                                //
        //                                for (int trial = 0; trial < PerChannelSampleCount; trial++)
        //                                {
        //                                    for (int channel = 0; channel < _channelCount; channel++)
        //                                    {
        //                                        if (channel % 32 == 0)
        //                                        {
        //                                            builder.AppendLine();
        //                                        }
        //                                        builder.Append($"{bytes[trial * _channelCount * 2 + channel * 2 + 1 + DataHeadOffset]:X2}{bytes[trial * _channelCount * 2 + channel * 2 + DataHeadOffset]:X2}, ");
        //                                    }
        //                                    builder.AppendLine();
        //                                }

        //                                //
        //                                var fs = new FileStream($"raw_Data_{DateTime.Now:yyyyMMdd_HHmmss_fff}.log", FileMode.CreateNew, FileAccess.ReadWrite);
        //                                var txtBytes = Encoding.UTF8.GetBytes(builder.ToString());
        //                                fs.Write(txtBytes, 0, txtBytes.Length);
        //                                fs.Close();
        //                            }
        //                        }

        //                        //
        //                        _chunkFIFO.Add(bytes);
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _pullRawResetEvent.WaitOne();
        //            frameLen = _channelCount * 2 * PerChannelSampleCount + DataHeadOffset;

        //            //
        //            _recvdFirstFrame = false;
        //            _recvdFrameCount = 0;
        //            _lostFrameCount = 0;
        //        }
        //    }
        //}

        /// <summary>
        /// 丢包检查
        /// </summary>
        /// <param name="newFrameIndex"></param>
        /// <returns></returns>
        private long FrameLostCheck(uint newFrameIndex)
        {
            long lost = 0;

            //丢包判断
            if (true)
            {
                _recvdFrameCount++;

                //
                if (_recvdFirstFrame)
                {
                    var skip = (newFrameIndex + FrameIndexCycle - _frameIndex) % FrameIndexCycle; //假定一次不会丢超过0xffff包

                    //存在丢包
                    if (skip != 1)
                    {
                        Logger.LogTool.Logger.LogW($"{TAG}, frame lost, lastIndex={_frameIndex}, newIndex={newFrameIndex}");
                        lost = (skip + FrameIndexCycle - 1) % FrameIndexCycle;
                        _lostFrameCount += lost;
                    }
                }

                //
                _frameIndex = newFrameIndex;
                _recvdFirstFrame = true;
            }

            //
            return lost;
        }

        private Dictionary<int, int> channelMap = new Dictionary<int, int>();

        /// <summary>
        /// ChannelMap映射初始化加载
        /// </summary>
        private void LoadChannelMappingFromFile()
        {
            try
            {
                string currentDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                string[] lines = File.ReadAllLines(Path.Combine(currentDirectory, "map.txt"));

                for (int i = 1; i < lines.Length; i++)
                {
                    string[] parts = lines[i].Split('|');
                    if (parts.Length == 2)
                    {
                        int intanChannel = int.Parse(parts[0]);
                        int pdChannel = int.Parse(parts[1]);
                        channelMap[intanChannel] = pdChannel;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("loading map error：" + ex.Message);
            }
        }

        private void refChunk(ref PooledArray<byte> chunk)
        {

            if (chunk != null)
            {
                Dictionary<int, int> map = ProbeMapSettings.LoadChannelMapping("A");

                PooledArray<byte> bytes = new PooledArray<byte>(chunk.Length);

                for (int i = 0; i < DataHeadOffset - 1; i++)
                {
                    bytes[i] = chunk[i];
                }


                foreach (var dis in map)
                {

                    for (int sampleIndex = 0; sampleIndex < PerChannelSampleCount; sampleIndex++)
                    {
                        bytes[DataHeadOffset + (sampleIndex * _channelCount + dis.Value - 1) * 2] = chunk[DataHeadOffset + (sampleIndex * _channelCount + dis.Key - 1) * 2];
                        bytes[DataHeadOffset + (sampleIndex * _channelCount + dis.Value - 1) * 2 + 1] = chunk[DataHeadOffset + (sampleIndex * _channelCount + dis.Key - 1) * 2 + 1];

                    }

                }
                chunk = bytes;
            }

        }


        private void CalculateTrigger(byte[] chunk, int[] trigs)
            {
                if (trigs != null && chunk != null)
                {
                    for (int chl = 0; chl < _channelCount - 1; chl++)
                    {

                        for (int sampleIndex = 0; sampleIndex < PerChannelSampleCount; sampleIndex++)
                        {

                            byte channelDataLowByte = chunk[DataHeadOffset +(sampleIndex *_channelCount +chl)  * 2 ];
                            byte channelDataHighByte = chunk[DataHeadOffset + (sampleIndex * _channelCount + chl) * 2 + 1];

                            int channelData = (channelDataHighByte << 8) | channelDataLowByte;

                            int trigBit = (channelData >> 0) & 0x01;

                            trigs[(chl+1) * PerChannelSampleCount + sampleIndex] = trigBit;
                        }
                    }

                }
            }




        /// <summary>
        /// 原始数据包处理
        /// </summary>
        /// <param name="objs"></param>
        private void ChunkConsume(object[] objs)
        {
            var chunk = (PooledArray<byte>)objs[0];

            var originalChunk = (PooledArray<byte>)objs[0];

            //refChunk(ref chunk);

            //
            _xpu.ParamCenter.SetCommonParam(_channelCount, PerChannelSampleCount, _sampleRate);

            _xpu.ProcessByteADChunk(DataHeadOffset, chunk.Array, out PooledArray<float> raws, out PooledArray<float> wfps, out PooledArray<float> hfps, out PooledArray<float> lfps, out PooledArray<float> spks, out PooledArray<int> trigs);

            CalculateTrigger(originalChunk.Array, trigs.Array);

            chunk.Dispose();

            DataReport?.Invoke(_channelCount, PerChannelSampleCount, raws, wfps, hfps, lfps, spks, trigs, _recvdFrameCount, _lostFrameCount);

        }


        #endregion



        #region Demo

        /// <summary>
        /// 
        /// </summary>
        private void DemoPullRawDataLoop()
        {
            var watch = new Stopwatch();

            //
            const int Artifact = 3;
            var sentCount = 0;
            var frameLen = _channelCount * 2 * PerChannelSampleCount + DataHeadOffset;
            var demoData = DemoData.DemoData.Data;
            var index = 0;

            //
            while (true)
            {
                if (!IsCollecting || !Demo)
                {
                    _pullDemoRawResetEvent.WaitOne();
                    frameLen = _channelCount * 2 * PerChannelSampleCount + 64;
                    watch.Restart();
                    sentCount = 0;
                    continue;
                }

                //
                try
                {
                    var shouldSent = watch.ElapsedMilliseconds * _sampleRate / 1000;
                    if (sentCount > shouldSent)
                    {
                        Thread.Sleep(2);
                        continue;
                    }

                    //
                    _cmd.HeadstageDataCommand.GetHeadstageData(out PooledArray<byte> bytes);

                    //
                    var frame = new PooledArray<byte>(frameLen);
                    for (int i = 0; i < PerChannelSampleCount; i++)
                    {
                        for (int j = 0; j < _channelCount - Artifact; j++)
                        {
                            frame[DataHeadOffset + i * _channelCount * 2 + j * 2] = demoData[index];
                            frame[DataHeadOffset + i * _channelCount * 2 + j * 2 + 1] = demoData[index + 1];
                            //if (j == 0)
                            //{
                            //    Logger.LogTool.Logger.LogE($"{frame.Array[DataHeadOffset + i * _channelCount * 2 + j * 2 + 1]:X2}{frame.Array[DataHeadOffset + i * _channelCount * 2 + j * 2]:X2}");
                            //}
                        }
                        for (int j = _channelCount - Artifact; j < _channelCount; j++)
                        {
                            var sawBytes = BitConverter.GetBytes((ushort)(index % 2000 + 32768 - 500));
                            frame[DataHeadOffset + i * _channelCount * 2 + j * 2] = sawBytes[0];
                            frame[DataHeadOffset + i * _channelCount * 2 + j * 2 + 1] = sawBytes[1];
                        }
                        index = (index + 2) % demoData.Length;
                    }

                    //
                    _chunkFIFO.Add(frame);
                    sentCount += PerChannelSampleCount;
                }
                catch { }
            }
        }

#endregion


        /// <summary>
        /// 触发event
        /// </summary>
        protected void NotifyConnectionStatusChanged()
        {
            OnConnectionStatusChanged?.Invoke(ConnectionState, ConnectionTip);
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Dispose()
        {
            _stopQueryState = true;
        }

        public virtual void QueryRefreshStates()
        {
            
        }



#region IBatchImpedanceHelper

        /// <summary>
        /// 每个headstage上具有128个通道：2片RHD2164
        /// </summary>
        const int HeadstageChannelCount = 128;

        /// <summary>
        /// 进行阻抗测试的通道：对应一个headstage
        /// </summary>
        private List<int> _impedanceChannels = new List<int>();


        //private int _DeviceId = -1;
        //public int DeviceId
        //{
        //    get { return _DeviceId; }
        //    set { _DeviceId = value; }
        //}

        /// <summary>
        /// 每组数据对应的
        /// </summary>
        private readonly ConcurrentQueue<Tuple<int, int>> _channelCapQueue = new ConcurrentQueue<Tuple<int, int>>();

        /// <summary>
        /// 
        /// </summary>
        public event Action<int, byte[]> ImpedanceADCDataRecvd;

        private Action<int, double, double> _callback = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleRate"></param>
        /// <param name="lowerBandwidth"></param>
        /// <param name="upperBandwidth"></param>
        /// <param name="enableDSP"></param>
        /// <param name="dspCutoff"></param>
        /// <param name="period"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        bool IBatchImpedanceHelper.InitImpedance(double sampleRate, double lowerBandwidth, double upperBandwidth, bool enableDSP, double dspCutoff, byte[] period, out string errMsg)
        {
            //StairMedOutput.IsWriteToConsole = false;
            errMsg = string.Empty;

            //
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputChannels"></param>
        /// <returns></returns>
        bool IBatchImpedanceHelper.SetImpedanceChannels(List<int> inputChannels)
        {
            _impedanceChannels = inputChannels;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="numPeriods"></param>
        /// <returns></returns>
        bool IBatchImpedanceHelper.StartImpedance(int numPeriods)
        {
            ImpedanceTesting = true;
            _pullImpedanceResetEvent.Set();
            return true;
        }

        public void ResetChannelImpedance()
        {
            if (_callback == null)
            {
                return;
            }
            
            foreach (var inputChannel in _impedanceChannels)
            {
                _callback(inputChannel, 0, 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool IBatchImpedanceHelper.StopImpedance()
        {
            ImpedanceTesting = false;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="inputChannel"></param>
        /// <param name="cap"></param>
        /// <param name="adcOffset"></param>
        /// <returns></returns>
        bool IBatchImpedanceHelper.ResolveImpedanceADCData(int frameLen, byte[] frame, out int inputChannel, out int cap, out int adcOffset)
        {
            adcOffset = 0;
            inputChannel = 0;
            cap = 0;

            //
            if (!_channelCapQueue.TryDequeue(out Tuple<int, int> channelCapInfo))
            {
                return false;
            }

            //
            inputChannel = channelCapInfo.Item1;
            cap = channelCapInfo.Item2;

            //
            return true;
        }

        public void OnGetImpedance(PooledArray<byte> bytes)
        {

            if (_impedanceIndex >= _impedanceChannels.Count)
                return;

            if (_impedanceIndex == _impedanceChannels.Count-1)
            {
                ImpedanceTesting = false;
            }
            var inputChannel = _impedanceChannels[_impedanceIndex++];
                //先预备好数据对应的通道和电容值
            _channelCapQueue.Enqueue(Tuple.Create(inputChannel, 0));
            _channelCapQueue.Enqueue(Tuple.Create(inputChannel, 1));
            _channelCapQueue.Enqueue(Tuple.Create(inputChannel, 2));

            //将三种电容下对应的采样点分割开来
            var byte01PF = new byte[GetHSResistanceCommand.ImpedanceSampleByteCount];
            var byte1PF = new byte[GetHSResistanceCommand.ImpedanceSampleByteCount];
            var byte10PF = new byte[GetHSResistanceCommand.ImpedanceSampleByteCount];
            {
                var offset = 35;
                //if (channelIndexOnHeadstage == 0)
                //{
                //    for (int iki = offset; iki < bytes.Length; iki += 2)
                //    {
                //        Logger.LogTool.Logger.LogE($"{bytes.Array[iki + 1]:X2}{bytes.Array[iki]:X2}");
                //    }
                //}

                System.Array.Copy(bytes.Array, offset, byte01PF, 0, GetHSResistanceCommand.ImpedanceSampleByteCount);
                offset += GetHSResistanceCommand.ImpedanceSampleByteCount;
                System.Array.Copy(bytes.Array, offset, byte1PF, 0, GetHSResistanceCommand.ImpedanceSampleByteCount);
                offset += GetHSResistanceCommand.ImpedanceSampleByteCount;
                System.Array.Copy(bytes.Array, offset, byte10PF, 0, GetHSResistanceCommand.ImpedanceSampleByteCount);
            }

            ////释放原始数据
            //bytes.Dispose();


            //丢给阻抗测试计算逻辑去执行
            ImpedanceADCDataRecvd?.Invoke(byte01PF.Length, byte01PF);
            ImpedanceADCDataRecvd?.Invoke(byte1PF.Length, byte1PF);
            ImpedanceADCDataRecvd?.Invoke(byte10PF.Length, byte10PF);
        }


        private void PullImpedanceLoop()
        {
            //
            bool bTimeLoop = true;
            while (true)
            {
                Thread.Sleep(30);

                //
                if (bTimeLoop)
                {
                    _channelCapQueue.Clear();

                    //
                    do
                    {
                        //设备未连接，阻抗测试失败
                        if (ConnectionState != ConnectState.Connected)
                        {
                            break;
                        }

                        _impedanceIndex = 0;
                        //遍历测试所有通道
                        for (int i = 0; i < _impedanceChannels.Count; i++)
                        {
                            Thread.Sleep(150);
                            //通道
                            var inputChannel = _impedanceChannels[i];

                            //该通道所在headstage、及在该headstage的序号
                            var headstage = inputChannel / HeadstageChannelCount;
                            var channelIndexOnHeadstage = inputChannel % HeadstageChannelCount;
                            _cmd.GetHSResistanceCommand.GetHSResistance(headstage, channelIndexOnHeadstage, out PooledArray<byte> bytes);
                            //demo下，创建伪造的数据
                            if (Demo)
                            {
                                Thread.Sleep(66);
                                bytes.Dispose();

                                //
                                var capCount = GetHSResistanceCommand.CapCount;
                                bytes = new PooledArray<byte>(4 + GetHSResistanceCommand.ImpedanceSampleByteCount * capCount);

                                //
                                var offset = 4;
                                for (int cap = 0; cap < capCount; cap++)
                                {
                                    //规律：幅值越大，阻抗值越大
                                    //var magnitude = _rand.Next(0, 100) > 90 ? _rand.Next(1500, 35000) : _rand.Next(500, 1300);
                                    //var magnitude = (inputChannel / 10 + 3) * 200;
                                    var magnitude = (inputChannel / 20 + 3) * 150;

                                    //
                                    for (int adIndex = 0; adIndex < GetHSResistanceCommand.ImpedanceSampleCount; adIndex++)
                                    {
                                        var adc = (ushort)(magnitude * Math.Sin((adIndex + inputChannel + cap) * 2 * Math.PI / 20) + 32768);
                                        var adcBytes = BitConverter.GetBytes(adc);
                                        bytes[offset++] = adcBytes[0];
                                        bytes[offset++] = adcBytes[1];
                                    }
                                }
                            }
                            //
                            if (!ImpedanceTesting)
                            {
                                break;
                            }
                        }

                    } while (false);
                    bTimeLoop = false;
                    //
                }
                else
                {
                    bTimeLoop = true;
                    _pullImpedanceResetEvent.WaitOne();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        //private void PullImpedanceLoop()
        //{
        //    //
        //    while (true)
        //    {
        //        Thread.Sleep(30);

        //        //
        //        if (ImpedanceTesting)
        //        {
        //            _channelCapQueue.Clear();

        //            //
        //            do
        //            {
        //                //设备未连接，阻抗测试失败
        //                if (ConnectionState != ConnectState.Connected)
        //                {
        //                    break;
        //                }

        //                //遍历测试所有通道
        //                for (int i = 0; i < _impedanceChannels.Count; i++)
        //                {
        //                    //通道
        //                    var inputChannel = _impedanceChannels[i];

        //                    //该通道所在headstage、及在该headstage的序号
        //                    var headstage = inputChannel / HeadstageChannelCount;
        //                    var channelIndexOnHeadstage = inputChannel % HeadstageChannelCount;

        //                    //下发指令，执行阻抗测试
        //                    if (!_cmd.GetHSResistanceCommand.GetHSResistance(headstage, channelIndexOnHeadstage, out PooledArray<byte> bytes))
        //                    {
        //                        //demo下使用demo数据
        //                        if (!_demo)
        //                        {
        //                            break;
        //                        }
        //                    }



        //                    //demo下，创建伪造的数据
        //                    if (_demo)
        //                    {
        //                        Thread.Sleep(66);
        //                        bytes.Dispose();

        //                        //
        //                        var capCount = GetHSResistanceCommand.CapCount;
        //                        bytes = new PooledArray<byte>(4 + GetHSResistanceCommand.ImpedanceSampleByteCount * capCount);

        //                        //
        //                        var offset = 4;
        //                        for (int cap = 0; cap < capCount; cap++)
        //                        {
        //                            //规律：幅值越大，阻抗值越大
        //                            //var magnitude = _rand.Next(0, 100) > 90 ? _rand.Next(1500, 35000) : _rand.Next(500, 1300);
        //                            //var magnitude = (inputChannel / 10 + 3) * 200;
        //                            var magnitude = (inputChannel / 20 + 3) * 150;

        //                            //
        //                            for (int adIndex = 0; adIndex < GetHSResistanceCommand.ImpedanceSampleCount; adIndex++)
        //                            {
        //                                var adc = (ushort)(magnitude * Math.Sin((adIndex + inputChannel + cap) * 2 * Math.PI / 20) + 32768);
        //                                var adcBytes = BitConverter.GetBytes(adc);
        //                                bytes[offset++] = adcBytes[0];
        //                                bytes[offset++] = adcBytes[1];
        //                            }
        //                        }
        //                    }

        //                    //先预备好数据对应的通道和电容值
        //                    _channelCapQueue.Enqueue(Tuple.Create(inputChannel, 0));
        //                    _channelCapQueue.Enqueue(Tuple.Create(inputChannel, 1));
        //                    _channelCapQueue.Enqueue(Tuple.Create(inputChannel, 2));

        //                    //将三种电容下对应的采样点分割开来
        //                    var byte01PF = new byte[GetHSResistanceCommand.ImpedanceSampleByteCount];
        //                    var byte1PF = new byte[GetHSResistanceCommand.ImpedanceSampleByteCount];
        //                    var byte10PF = new byte[GetHSResistanceCommand.ImpedanceSampleByteCount];
        //                    {
        //                        var offset = 35;
        //                        //if (channelIndexOnHeadstage == 0)
        //                        //{
        //                        //    for (int iki = offset; iki < bytes.Length; iki += 2)
        //                        //    {
        //                        //        Logger.LogTool.Logger.LogE($"{bytes.Array[iki + 1]:X2}{bytes.Array[iki]:X2}");
        //                        //    }
        //                        //}

        //                        System.Array.Copy(bytes.Array, offset, byte01PF, 0, GetHSResistanceCommand.ImpedanceSampleByteCount);
        //                        offset += GetHSResistanceCommand.ImpedanceSampleByteCount;
        //                        System.Array.Copy(bytes.Array, offset, byte1PF, 0, GetHSResistanceCommand.ImpedanceSampleByteCount);
        //                        offset += GetHSResistanceCommand.ImpedanceSampleByteCount;
        //                        System.Array.Copy(bytes.Array, offset, byte10PF, 0, GetHSResistanceCommand.ImpedanceSampleByteCount);
        //                    }

        //                    //释放原始数据
        //                    bytes.Dispose();


        //                    //丢给阻抗测试计算逻辑去执行
        //                    ImpedanceADCDataRecvd?.Invoke(byte01PF.Length, byte01PF);
        //                    ImpedanceADCDataRecvd?.Invoke(byte1PF.Length, byte1PF);
        //                    ImpedanceADCDataRecvd?.Invoke(byte10PF.Length, byte10PF);

        //                    //
        //                    if (!ImpedanceTesting)
        //                    {
        //                        break;
        //                    }
        //                }
        //            } while (false);

        //            //
        //            ImpedanceTesting = false;
        //        }
        //        else
        //        {
        //            _pullImpedanceResetEvent.WaitOne();
        //        }
        //    }
        //}

#endregion

        //firmware, fpga, linux, hardwareConfiguration
        public event Action<string, string, string, string> OnVersionChanged;

        public void SendARMTest(int port,int channel)
        {
            //return;
            if (_cmd.ARMTestCommand.ArmSelfTest(port,channel))
            {
                

            }
            
        }
    }
}
