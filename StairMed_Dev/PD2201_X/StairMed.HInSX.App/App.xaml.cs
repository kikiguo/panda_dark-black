﻿using Prism.DryIoc;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using StairMed.Core.Settings;
using StairMed.HInSX.App.Views.Regions;
using StairMed.HInSX.App.Views.Regions.Bodies;
using StairMed.HInSX.App.Views.Windows;
using StairMed.Logger;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Views.Windows;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using StairMed.HInSX.Command;

namespace StairMed.HInSX.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        /// <summary>
        /// 
        /// </summary>
        private static Mutex _singletonMutex = null;
        internal static bool Reboot = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            //单实例运行
            var watch = new Stopwatch();
            watch.Start();
            var createdNew = false;
            while (watch.ElapsedMilliseconds < 15000)
            {
                _singletonMutex = new Mutex(true, this.GetType().FullName, out createdNew);
                if (!createdNew)
                {
                    Thread.Sleep(500);
                }
                else
                {
                    break;
                }
            }
            if (!createdNew)
            {
                this.Shutdown();
                return;
            }

            //
            StairMed.HInSX.App.Logger.LogTool.InitLogger(this.GetType().FullName.Split(".")[1]);
            Logger.LogTool.Logger.LogF($">>>>>>>>>>>>>>>>>>>>>>>>>> enter app, version:{VersionInfo.SoftVersion}, build:{VersionInfo.BuildDate}");

            //
            Initializer.Init();

            //
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
            base.OnStartup(e);
            this.Exit += (object sender, ExitEventArgs e) =>
            {
                Logger.LogTool.Logger.LogF($"<<<<<<<<<<<<<<<<<<<<<<<<<< leave app, version:{VersionInfo.SoftVersion}, build:{VersionInfo.BuildDate}");

                //
                UIHelper.Skip = true;
                StairMedOutput.ApplicationExit = true;
                _singletonMutex.ReleaseMutex();
                _singletonMutex.Dispose();
                Thread.Sleep(1000);

                //
                if (Reboot)
                {
                    try
                    {
                        var exePath = Process.GetCurrentProcess().MainModule.FileName;
                        Process.Start(exePath);
                    }
                    catch { }
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskScheduler_UnobservedTaskException(object? sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception != null && e.Exception.InnerExceptions.Count > 0)
            {
                Logger.LogTool.Logger.Exp($"", e.Exception.InnerExceptions[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Window CreateShell()
        {
            return //new WelcomeWnd(() =>
            //{
                new HInSDetectionWnd((hinsType) =>
                {
                    HInSXCONST.HInSXType = hinsType;
                    StairMedSolidSettings.Instance.TestType = "Monkey";
                    {
                        StairMedSolidSettings.Instance.HistoryTypes.Remove(StairMedSolidSettings.Instance.TestType);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryTypes);
                        list.Insert(0, StairMedSolidSettings.Instance.TestType);
                        StairMedSolidSettings.Instance.HistoryTypes = list;
                    }

                    //
                    StairMedSolidSettings.Instance.TestCode = "8888";
                    {
                        StairMedSolidSettings.Instance.HistoryCodes.Remove(StairMedSolidSettings.Instance.TestCode);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryCodes);
                        list.Insert(0, StairMedSolidSettings.Instance.TestCode);
                        StairMedSolidSettings.Instance.HistoryCodes = list;
                    }


                    StairMedSolidSettings.Instance.TestHosptial = "Hospital";
                    {
                        StairMedSolidSettings.Instance.HistoryHospital.Remove(StairMedSolidSettings.Instance.TestHosptial);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryHospital);
                        list.Insert(0, StairMedSolidSettings.Instance.TestHosptial);
                        StairMedSolidSettings.Instance.HistoryHospital = list;
                    }

                    StairMedSolidSettings.Instance.TestEleCode = "1";
                    {
                        StairMedSolidSettings.Instance.HistoryEleCode.Remove(StairMedSolidSettings.Instance.TestEleCode);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryEleCode);
                        list.Insert(0, StairMedSolidSettings.Instance.TestEleCode);
                        StairMedSolidSettings.Instance.HistoryEleCode = list;
                    }

                    var testCreateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
                    StairMedSolidSettings.Instance.CreateTime = testCreateTime;
                    {
                        StairMedSolidSettings.Instance.HistoryCreateTime.Remove(StairMedSolidSettings.Instance.CreateTime);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryCreateTime);
                        list.Insert(0, StairMedSolidSettings.Instance.CreateTime);
                        StairMedSolidSettings.Instance.HistoryCreateTime = list;
                    }
                    new HInSMainstayWnd().ShowDialog();
                }, () =>
                {
                    HInSXCONST.HInSXType = HInSXType.HInS_1024;
                    StairMedSolidSettings.Instance.TestType = "Monkey";
                    {
                        StairMedSolidSettings.Instance.HistoryTypes.Remove(StairMedSolidSettings.Instance.TestType);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryTypes);
                        list.Insert(0, StairMedSolidSettings.Instance.TestType);
                        StairMedSolidSettings.Instance.HistoryTypes = list;
                    }

                    //
                    StairMedSolidSettings.Instance.TestCode = "8888";
                    {
                        StairMedSolidSettings.Instance.HistoryCodes.Remove(StairMedSolidSettings.Instance.TestCode);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryCodes);
                        list.Insert(0, StairMedSolidSettings.Instance.TestCode);
                        StairMedSolidSettings.Instance.HistoryCodes = list;
                    }


                    StairMedSolidSettings.Instance.TestHosptial = "Hospital";
                    {
                        StairMedSolidSettings.Instance.HistoryHospital.Remove(StairMedSolidSettings.Instance.TestHosptial);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryHospital);
                        list.Insert(0, StairMedSolidSettings.Instance.TestHosptial);
                        StairMedSolidSettings.Instance.HistoryHospital = list;
                    }

                    StairMedSolidSettings.Instance.TestEleCode = "1";
                    {
                        StairMedSolidSettings.Instance.HistoryEleCode.Remove(StairMedSolidSettings.Instance.TestEleCode);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryEleCode);
                        list.Insert(0, StairMedSolidSettings.Instance.TestEleCode);
                        StairMedSolidSettings.Instance.HistoryEleCode = list;
                    }

                    var testCreateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
                    StairMedSolidSettings.Instance.CreateTime = testCreateTime;
                    {
                        StairMedSolidSettings.Instance.HistoryCreateTime.Remove(StairMedSolidSettings.Instance.CreateTime);
                        var list = new List<string>(StairMedSolidSettings.Instance.HistoryCreateTime);
                        list.Insert(0, StairMedSolidSettings.Instance.CreateTime);
                        StairMedSolidSettings.Instance.HistoryCreateTime = list;
                    }
                    new HInSMainstayWnd().ShowDialog();
                //}).ShowDialog();
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        protected override void RegisterTypes(IContainerRegistry container)
        {
            ContainerHelper.Container = Container.Resolve<IContainerExtension>();
            EventHelper.EventAggregator = ContainerHelper.Resolve<IEventAggregator>();
            RegionHelper.RegionManager = ContainerHelper.Resolve<IRegionManager>();

            //
            container.RegisterForNavigation<HInSHeadView>();
            container.RegisterForNavigation<HInSNeckView>();
            container.RegisterForNavigation<BodyDefaultView>();
            container.RegisterForNavigation<HInSXRTView>();
            container.RegisterForNavigation<HistoryView>();
            container.RegisterForNavigation<PlayerView>();
        }
    }
}
