﻿using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Enum;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.MVVM.Base;
using StairMed.Probe;
using System.Linq;
using System.Xml.Serialization;

namespace StairMed.HInSX.App.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXSettings : ViewModelBase
    {
        public const string TAG = nameof(HInSXSettings);

        #region 单例

        private static readonly HInSXSettings _instance = new HInSXSettings();
        public static HInSXSettings Instance => _instance;
        private HInSXSettings()
        {
            DeviceInfo = new HInSXDeviceInfo();
            CollectParamInfo = new HInSXCollectParamInfo(HInSXCONST.CHANNELS_COUNT)
            {
                DeviceId = DeviceInfo.DeviceId,
                SampleRate = ReadonlyCONST.SampleRates.Last(),
                CollectType = CollectMode.RawAll,
            };

            //
            if (ProbeMapSettings.Instance.ProbeMap == null || ProbeMapSettings.Instance.ProbeMap.IsInValid() || ProbeMapSettings.Instance.ProbeMap.ChannelCount != 128)
            {
                ProbeMapSettings.Instance.ProbeMap = NTProbeMap.GetDefault();
            }

            //
            ReadonlyCONST.ProbeMap = ProbeMapSettings.Instance.ProbeMap;

            //
            CollectParamInfo.InitChannels(ReadonlyCONST.ProbeMap);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private bool _demoMode = false;
        public bool DemoMode
        {
            get { return _demoMode; }
            set { Set(ref _demoMode, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSXDeviceInfo DeviceInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public HInSXCollectParamInfo CollectParamInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HInSXDeviceInfo GetDeviceInfo()
        {
            return DeviceInfo;
        }

        /// <summary>
        /// 保存采样参数
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="param"></param>
        public void SetCollectionParam(HInSXCollectParamInfo param)
        {
            CollectParamInfo = param;
        }

        /// <summary>
        /// 获取采样参数
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public HInSXCollectParamInfo GetCollectionParam()
        {
            return CollectParamInfo;
        }
    }
}
