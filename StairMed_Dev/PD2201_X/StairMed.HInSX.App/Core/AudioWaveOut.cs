﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.HInSX.App.Core
{
    public class AudioWaveOut : NAudio.Wave.WaveProvider32
    {

        public float Frequency { get; set; }
        public float Amplitude { get; set; }

        int sample;

        public AudioWaveOut(float freq,float amp,int sp)
        {
            Frequency = freq; Amplitude=amp;sample = sp;
        }


        public override int Read(float[] buffer, int offset, int sampleCount)
        {
            int sampleRate = WaveFormat.SampleRate;
            for (int n = 0; n < sampleCount; n++)
            {
                buffer[n + offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate));
                sample++;
                if (sample >= sampleRate)
                    sample = 0;
            }

            return sample;
        }

    }
}
