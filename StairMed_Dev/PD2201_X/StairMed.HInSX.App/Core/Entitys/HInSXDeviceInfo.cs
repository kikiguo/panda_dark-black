﻿using StairMed.Entity.Infos.Bases;

namespace StairMed.HInSX.App.Core.Entitys
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXDeviceInfo : DeviceInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public HInSXDeviceInfo()
        {
            DeviceId = 1;
            Name = "HINS1024";
        }

        /// <summary>
        /// 固件版本
        /// </summary>
        private string _fireware = string.Empty;
        public string Fireware
        {
            get { return _fireware; }
            set { Set(ref _fireware, value); }
        }

        /// <summary>
        /// FPGA版本
        /// </summary>
        private string _fpga = string.Empty;
        public string Fpga
        {
            get { return _fpga; }
            set { Set(ref _fpga, value); }
        }

        /// <summary>
        /// Linux版本
        /// </summary>
        private string _linux = string.Empty;
        public string Linux
        {
            get { return _linux; }
            set { Set(ref _linux, value); }
        }

        /// <summary>
        /// 固件配置
        /// </summary>
        private string _hardwareConfiguration = string.Empty;
        public string HardwareConfiguration
        {
            get { return _hardwareConfiguration; }
            set { Set(ref _hardwareConfiguration, value); }
        }
    }
}
