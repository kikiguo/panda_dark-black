﻿using StairMed.MVVM.Base;

namespace StairMed.HInSX.App.Core.Entitys
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXVersionInfo : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private string _firmware = string.Empty;
        public string Firmware
        {
            get { return _firmware; }
            set { Set(ref _firmware, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _FPGA = string.Empty;
        public string FPGA
        {
            get { return _FPGA; }
            set { Set(ref _FPGA, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _linux = string.Empty;
        public string Linux
        {
            get { return _linux; }
            set { Set(ref _linux, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _hardwareConfiguration = string.Empty;
        public string HardwareConfiguration
        {
            get { return _hardwareConfiguration; }
            set { Set(ref _hardwareConfiguration, value); }
        }
    }
}
