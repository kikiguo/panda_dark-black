﻿using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace StairMed.HInSX.App.Core.Entitys
{
    /// <summary>
    /// 电极
    /// </summary>
    public class HInSXElectrodeInfo : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public event Action<HInSXElectrodeInfo, bool> ElectrodeEnableChanged;

        /// <summary>
        /// 唯一标识：包含通道排序及通道个数等信息
        /// </summary>
        private string _id = "";
        [XmlAttribute]
        public string Id
        {
            get { return _id; }
            set { Set(ref _id, value); }
        }

        /// <summary>
        /// 电极名称：第几个插孔
        /// </summary>
        private string _electrodeName = string.Empty;
        [XmlAttribute]
        public string ElectrodeName
        {
            get { return _electrodeName; }
            set { Set(ref _electrodeName, value); }
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        private bool _enabled = false;
        [XmlAttribute]
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                Set(ref _enabled, value);
                ElectrodeEnableChanged?.Invoke(this, value);
            }
        }

        /// <summary>
        /// 该电极在设备上的顺序
        /// </summary>
        private int _electrodeIndex;
        [XmlAttribute]
        public int ElectrodeIndex
        {
            get { return _electrodeIndex; }
            set { Set(ref _electrodeIndex, value); }
        }

        /// <summary>
        /// 该电极对应的通道
        /// </summary>
        private List<int> _inputChannels;
        public List<int> InputChannels
        {
            get { return _inputChannels; }
            set { Set(ref _inputChannels, value); }
        }

        /// <summary>
        /// 该电极上有多少个通道
        /// </summary>
        private int _channelCount = 0;
        [XmlAttribute]
        public int ChannelCount
        {
            get { return _channelCount; }
            set { Set(ref _channelCount, value); }
        }
    }

}
