﻿using StairMed.Entity.Infos.Bases;
using StairMed.Probe;
using StairMed.Tools;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Windows.Documents;

namespace StairMed.HInSX.App.Core.Entitys
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXCollectParamInfo : CollectParamInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public HInSXCollectParamInfo(int channelCount) : base(channelCount)
        {
        }

        /// <summary>
        /// 电极们
        /// </summary>
        private List<HInSXElectrodeInfo> _electrodes = new List<HInSXElectrodeInfo>();

        private List<string> _electrodeIds = new List<string>();
        public List<HInSXElectrodeInfo> Electrodes
        {
            get { return _electrodes; }
            set { Set(ref _electrodes, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        protected NTProbeMap _probeMap = null;
        protected string _probeMapId = string.Empty;

        /// <summary>
        /// 电极上每个探针的通道数
        /// </summary>
        protected int _probeChannelCount = 1;

        /// <summary>
        /// 每个电极的通道数
        /// </summary>
        protected int _electrodeChannelCount = 1;

        /// <summary>
        /// 每个电极上有多少个电极丝
        /// </summary>
        protected int _probePerElectrode = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CollectParamInfo InitChannels(NTProbeMap probeMap)
        {
            base.InitChannels();

            //
            _probeMap = probeMap;
            _probeMapId = _probeMap.GetUniqueId();
            _electrodeChannelCount = _probeMap.ChannelCount;
            _probeChannelCount = _probeMap.Probes[0].Pads.Count;
            _probePerElectrode = _probeMap.Probes.Count;

            //
            if (_electrodes == null)
            {
                _electrodes = new List<HInSXElectrodeInfo>();
            }

            //
            _electrodeIds = new List<string>();
            for (int electrodeIndex = 0; electrodeIndex < DEVICE_CHANNELS / _electrodeChannelCount; electrodeIndex++)
            {
                _electrodeIds.Add(CreateElectrodeId(electrodeIndex));
            }

            //
            foreach (var electrode in _electrodes)
            {
                electrode.ElectrodeEnableChanged -= Electrode_ElectrodeEnableChanged;
            }

            //
            _electrodes.RemoveAll(r => !_electrodeIds.Contains(r.Id));

            //
            for (int electrodeIndex = 0; electrodeIndex < DEVICE_CHANNELS / _electrodeChannelCount; electrodeIndex++)
            {
                var id = _electrodeIds[electrodeIndex];
                if (!_electrodes.Exists(r => id.Equals(r.Id)))
                {
                    _electrodes.Add(new HInSXElectrodeInfo
                    {
                        Id = id,
                        ElectrodeName = CreateElectrodeName(electrodeIndex),
                        ElectrodeIndex = electrodeIndex,
                        ChannelCount = _electrodeChannelCount,
                        InputChannels = GetElectrodeChannels(electrodeIndex),
                    });
                }

                var electrode = _electrodes.FirstOrDefault(r => id.Equals(r.Id));
                electrode.ElectrodeEnableChanged -= Electrode_ElectrodeEnableChanged;
                electrode.ElectrodeEnableChanged += Electrode_ElectrodeEnableChanged;
                electrode.Enabled = Channels.Exists(r => electrode.InputChannels.Contains(r.InputIndex) && r.Enable);
                electrode.Enabled = true;
            }

            //
            return this;
        }

        public void ChangeElectrodeEnable(int nIndex, bool bEnable)
        {
            for (int electrodeIndex = 0; electrodeIndex < DEVICE_CHANNELS / _electrodeChannelCount; electrodeIndex++)
            {
                if( electrodeIndex == nIndex)
                {
                    var id = _electrodeIds[electrodeIndex];
                    var electrode = _electrodes.FirstOrDefault(r => id.Equals(r.Id));
                    electrode.Enabled = bEnable;
                }
            }
        }

        public void ChangeAllElectrodeEnable(bool bEnable)
        {
            for (int electrodeIndex = 0; electrodeIndex < DEVICE_CHANNELS / _electrodeChannelCount; electrodeIndex++)
            {
                var id = _electrodeIds[electrodeIndex];
                var electrode = _electrodes.FirstOrDefault(r => id.Equals(r.Id));
                electrode.Enabled = bEnable;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="electrode"></param>
        /// <param name="enable"></param>
        private void Electrode_ElectrodeEnableChanged(HInSXElectrodeInfo electrode, bool enable)
        {
            foreach (var channel in electrode.InputChannels)
            {
                var channelInfo = Channels.FirstOrDefault(r => r.InputIndex == channel);
                if (channelInfo != null)
                {
                    channelInfo.Enable = enable;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="electrodeIndex"></param>
        /// <returns></returns>
        private string CreateElectrodeId(int electrodeIndex)
        {
            var name = CreateElectrodeName(electrodeIndex);
            return ToolMix.MD5Encrypt($"{_probeMapId}{name}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="electrodeIndex"></param>
        /// <returns></returns>
        private string CreateElectrodeName(int electrodeIndex)
        {
            //var electrodeName = ((char)('A' + electrodeIndex)).ToString();
            var electrodeName = HeadstageNameHelper.GetNameByTotalChannel(electrodeIndex, DEVICE_CHANNELS);
            return $"{electrodeName}-[{_electrodeChannelCount * electrodeIndex + 1}-{_electrodeChannelCount * (electrodeIndex + 1)}]";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<int> GetElectrodeChannels(int electrodeIndex)
        {
            var offset = electrodeIndex * _electrodeChannelCount;
            var channelOnProbe = new List<int>();
            foreach (var probe in _probeMap.Probes)
            {
                foreach (var pad in probe.Pads)
                {
                    channelOnProbe.Add(offset + pad.Channel);
                }
            }
            return channelOnProbe;
        }
    }
}
