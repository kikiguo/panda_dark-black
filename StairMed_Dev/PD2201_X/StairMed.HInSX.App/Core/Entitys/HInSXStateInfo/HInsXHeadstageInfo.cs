﻿using StairMed.MVVM.Base;

namespace StairMed.HInSX.App.Core.Entitys.HInSXStateInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class HInsXHeadstageInfo : ViewModelBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        private string _name = "A";
        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }

        /// <summary>
        /// 是否已插入
        /// </summary>
        private bool _inserted = false;
        public bool Inserted
        {
            get { return _inserted; }
            set { Set(ref _inserted, value); }
        }

        /// <summary>
        /// 第1片2164温度
        /// </summary>
        private double _1st2164Temperature = 0;
        public double First2164Temperature
        {
            get { return _1st2164Temperature; }
            set { Set(ref _1st2164Temperature, value); }
        }

        /// <summary>
        /// 第2片2164温度
        /// </summary>
        private double _2nd2164Temperature = 0;
        public double Second2164Temperature
        {
            get { return _2nd2164Temperature; }
            set { Set(ref _2nd2164Temperature, value); }
        }

        /// <summary>
        /// 第1片2164电压
        /// </summary>
        private double _1st2164Voltage = 0;
        public double First2164Voltage
        {
            get { return _1st2164Voltage; }
            set { Set(ref _1st2164Voltage, value); }
        }

        /// <summary>
        /// 第2片2164电压
        /// </summary>
        private double _2nd2164Voltage = 0;
        public double Second2164Voltage
        {
            get { return _2nd2164Voltage; }
            set { Set(ref _2nd2164Voltage, value); }
        }
    }
}
