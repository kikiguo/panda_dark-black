﻿using System.Collections.Generic;

namespace StairMed.HInSX.App.Core.Entitys.HInSXStateInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class HInS1024StateInfo : HInSXStateInfoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public HInS1024StateInfo()
        {
            var headstageStates = new List<HInsXHeadstageInfo>();

            //
            for (int i = 0; i < HInSXCONST.HInS1024_HeadStageCount; i++)
            {
                headstageStates.Add(new HInsXHeadstageInfo
                {
                    Name = ((char)('A' + i)).ToString(),
                    Inserted = false,
                });
            }

            //
            HeadstageStates = headstageStates;
        }
    }
}
