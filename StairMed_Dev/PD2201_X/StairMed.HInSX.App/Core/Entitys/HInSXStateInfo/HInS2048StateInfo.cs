﻿using System.Collections.Generic;

namespace StairMed.HInSX.App.Core.Entitys.HInSXStateInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class HInS2048StateInfo : HInSXStateInfoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public HInS2048StateInfo()
        {
            var headstageExistState = new List<HInsXHeadstageInfo>();

            //
            for (int i = 0; i < HInSXCONST.HInS2048_HeadStageCount; i++)
            {
                headstageExistState.Add(new HInsXHeadstageInfo
                {
                    Name = $"{((char)('A' + i / 8))}{i % 8 + 1}",
                    Inserted = false,
                });
            }

            //
            HeadstageStates = headstageExistState;
        }
    }
}
