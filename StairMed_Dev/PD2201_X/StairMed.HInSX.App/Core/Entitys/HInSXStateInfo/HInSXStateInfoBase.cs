﻿using StairMed.Entity.Infos.Bases;
using System.Collections.Generic;

namespace StairMed.HInSX.App.Core.Entitys.HInSXStateInfo
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSXStateInfoBase : StateInfo
    {
        /// <summary>
        /// 连接状态提示文本
        /// </summary>
        private string _connectionTip = string.Empty;
        public string ConnectionTip
        {
            get { return _connectionTip; }
            set { Set(ref _connectionTip, value); }
        }

        /// <summary>
        /// 温度
        /// </summary>
        private double _temperature = 0.0;
        public double Temperature
        {
            get { return _temperature; }
            set { Set(ref _temperature, value); }
        }

        /// <summary>
        /// 采集盒插孔是否有插入headstage
        /// </summary>
        private List<HInsXHeadstageInfo> _headstageStates = new List<HInsXHeadstageInfo> { };
        public List<HInsXHeadstageInfo> HeadstageStates
        {
            get { return _headstageStates; }
            set { Set(ref _headstageStates, value); }
        }
    }
}
