﻿using StairMed.Array;
using StairMed.Controls.Windows;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers;
using StairMed.Entity.DataPools;
using StairMed.Enum;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.HInSX.App.Core.Entitys.HInSXStateInfo;
using StairMed.HInSX.App.ViewModels.Windows;
using StairMed.HInSX.Command;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Xml;

namespace StairMed.HInSX.App.Core
{
    /// <summary>
    /// 
    /// </summary>
    internal class HInSXManager : ViewModelBase
    {
        const string TAG = nameof(HInSXManager);

        #region 单例

        private static readonly HInSXManager _instance = new HInSXManager();
        public static HInSXManager Instance => _instance;

        #endregion

        /// <summary>
        /// 配置信息
        /// </summary>
        private HInSXDeviceInfo _deviceInfo;
        public virtual HInSXDeviceInfo DeviceInfo
        {
            get { return _deviceInfo; }
            set { Set(ref _deviceInfo, value); }
        }

        /// <summary>
        /// 状态信息
        /// </summary>
        private HInSXStateInfoBase _stateInfo = null;
        public virtual HInSXStateInfoBase StateInfo
        {
            get { return _stateInfo; }
            set { Set(ref _stateInfo, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly HInSXBase _hinsX = null;

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _notchFilterSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.NotchFilterType),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _lowFilterSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.LowPassFilterCutoff),
            nameof(StairMedSettings.Instance.LowPassFilterOrder),
            nameof(StairMedSettings.Instance.LowPassFilterType),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _highFilterSettings = new HashSet<string>()
        {
            nameof(StairMedSettings.Instance.HighPassFilterCutoff),
            nameof(StairMedSettings.Instance.HighPassFilterOrder),
            nameof(StairMedSettings.Instance.HighPassFilterType),
        };

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> _careSettings = new HashSet<string>();

        /// <summary>
        /// 
        /// </summary>
        private HInSXManager()
        {
            switch (HInSXCONST.HInSXType)
            {
                case Command.HInSXType.HInS_2048:
                    {
                        _hinsX = new HInS2048(HInSXSettings.Instance.DemoMode);
                        StateInfo = new HInS2048StateInfo();
                    }
                    break;
                case Command.HInSXType.H_Unknown:
                case Command.HInSXType.HInS_1024:
                default:
                    {
                        _hinsX = new HInS1024(HInSXSettings.Instance.DemoMode);
                        StateInfo = new HInS1024StateInfo();
                    }
                    break;
            }

            //
            DeviceInfo = HInSXSettings.Instance.GetDeviceInfo();

            //
            _hinsX.OnConnectionStatusChanged += (status, tip) =>
            {
                int connectionAttempts = 0;
                StateInfo.ConnectState = status;
                StateInfo.ConnectionTip = tip;

                //
                if (status == ConnectState.Disconnected)
                {
                    if (connectionAttempts < 3)
                    {
                        connectionAttempts++;

                        Thread.Sleep(TimeSpan.FromSeconds(5));

                        _hinsX.Connect();
                    }
                    else
                    {
                        _hinsX.Dispose();
                        Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            MsgBox.Error($"设备已断开连接，软件将退出", "设备断开连接");
                            Application.Current.Shutdown();
                        }));
                    }
                    
                }
                else
                {
                    connectionAttempts = 0;
                    _hinsX.QueryRefreshStates();
                }
            };

            //DAU温度
            _hinsX.OnTemperatureChanged += (double temperature) =>
            {
                StateInfo.Temperature = temperature;
            };

            //headstage是否插入
            _hinsX.OnHeadStageInsertedStateChanged += (int headstage, bool inserted) =>
            {
                if (headstage >= 0 && headstage < StateInfo.HeadstageStates.Count)
                {
                    StateInfo.HeadstageStates[headstage].Inserted = inserted;
                    HInSXSettings.Instance.CollectParamInfo.ChangeElectrodeEnable(headstage, inserted);
                    OnHeadStageInsertedStateChanged?.Invoke(headstage, inserted);
                }
            };

            //intan芯片温度
            _hinsX.OnIntanTemperatureChanged += (int index, double temperature) =>
            {
                var headstageIndex = index / 2;
                if (headstageIndex >= 0 && headstageIndex < StateInfo.HeadstageStates.Count)
                {
                    if (index % 2 == 0)
                    {
                        StateInfo.HeadstageStates[headstageIndex].First2164Temperature = temperature;
                    }
                    else
                    {
                        StateInfo.HeadstageStates[headstageIndex].Second2164Temperature = temperature;
                    }
                }
            };

            //intan芯片电压
            _hinsX.OnIntanVoltageChanged += (int index, double voltage) =>
            {
                var headstageIndex = index / 2;
                if (headstageIndex >= 0 && headstageIndex < StateInfo.HeadstageStates.Count)
                {
                    if (index % 2 == 0)
                    {
                        StateInfo.HeadstageStates[headstageIndex].First2164Voltage = voltage;
                    }
                    else
                    {
                        StateInfo.HeadstageStates[headstageIndex].Second2164Voltage = voltage;
                    }
                }
            };

            _hinsX.OnVersionChanged += (string firmware, string fpga, string linux, string hardwareConfiguration) =>
              {
                  DeviceInfo.Fireware = firmware;
                  DeviceInfo.Fpga = fpga;
                  DeviceInfo.Linux = linux;
                  DeviceInfo.HardwareConfiguration = hardwareConfiguration;
              };

            _hinsX.DataReport += (int channelCount, int sampleCount, PooledArray<float> raws, PooledArray<float> wfps, PooledArray<float> hfps, PooledArray<float> lfps, PooledArray<float> spks, PooledArray<int> trigs ,long recvdFrameCount, long lostFrameCount) =>
            {

                StateInfo.RecvdFrameCount = recvdFrameCount;
                StateInfo.LostFrameCount = lostFrameCount;

                NeuralDataCollector.Instance.BufferChunk(new Chunk
                {
                    InputChannelInReports = NeuralDataCollector.Instance.GetInputChannelInReports(),
                    ChannelCount = channelCount,
                    OneChannelSamples = sampleCount,
                    SampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate(),
                    SpikeScopeTimeMS = StairMedSettings.Instance.SpikeScopeTimeMS,
                    SpikeSumStepMS = StairMedSettings.Instance.SpikeSumStepMS,
                    SpikeSumWindowMS = StairMedSettings.Instance.SpikeSumWindowMS,
                    Raws = raws,
                    Wfps = wfps,
                    Hfps = hfps,
                    Lfps = lfps,
                    Spks = spks,
                    Trigs = trigs,
                });
            };

            //
            UpdateFilterParams();

            //
            _careSettings.UnionWith(_notchFilterSettings);
            _careSettings.UnionWith(_lowFilterSettings);
            _careSettings.UnionWith(_highFilterSettings);

            //
            StairMedSettings.Instance.OnSettingChanged += (HashSet<string> changes) =>
            {
                if (_careSettings.Overlaps(changes))
                {
                    UpdateFilterParams();
                }
            };
            SpikeThresholdCenter.Instance.OnThresholdChanged += () =>
            {
                UpdateFilterParams();
            };

            //
            _hinsX.Connect();

            GetCollectionParam().ChangeAllElectrodeEnable(false);
        }

        public void ResetMode()
        {
            _hinsX.Demo = HInSXSettings.Instance.DemoMode;
            if (_hinsX.Demo)
            {
                _hinsX.ConnectionState = ConnectState.Connected;

            }
           

        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateFilterParams()
        {
            var notchHz = StairMedSettings.Instance.NotchFilterType == NotchFilterType._60Hz ? 60 : StairMedSettings.Instance.NotchFilterType == NotchFilterType._50Hz ? 50 : 0;
            _hinsX.UpdateNotchFilter(notchHz);

            //            
            _hinsX.UpdateLowPassFilter(StairMedSettings.Instance.LowPassFilterCutoff, StairMedSettings.Instance.LowPassFilterOrder, StairMedSettings.Instance.LowPassFilterType);

            //
            _hinsX.UpdateHighPassFilter(StairMedSettings.Instance.HighPassFilterCutoff, StairMedSettings.Instance.HighPassFilterOrder, StairMedSettings.Instance.HighPassFilterType);

            //
            var hashset = new HashSet<float>();
            var thresholds = new List<float>();
            var inputChannels = GetCollectionParam().OrderedInputChannels;
            foreach (var inputChannel in inputChannels)
            {
                var threshold = Convert.ToSingle(SpikeThresholdCenter.Instance.GetThreshold(inputChannel));
                thresholds.Add(threshold);
                hashset.Add(threshold);
            }
            if (hashset.Count > 1)
            {
                _hinsX.UpdateSpikeParam(thresholds);
            }
            //else
            //{
            //_hinsX.UpdateSpikeParam(thresholds[0]);
            // }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HInSXCollectParamInfo GetCollectionParam()
        {
            return HInSXSettings.Instance.GetCollectionParam();
        }

        public void QueryRefreshStates()
        {
             _hinsX.QueryRefreshStates();
        }

        /// <summary>
        /// 开始采集
        /// </summary>
        public bool StartCollect(HInSXCollectParamInfo newParam, out string errMsg)
        {
            //保存采样参数
            HInSXSettings.Instance.SetCollectionParam(newParam);

            //重设新开始采样
            NeuralDataCollector.Instance.StartNewCollect(newParam);

            //
            if (_hinsX.StartCollect(newParam.SampleRate, newParam.OrderedInputChannels.ToHashSet(), out errMsg))
            {
                return true;
            }

            //
            NeuralDataCollector.Instance.StopCollect();
            return false;
        }

        /// <summary>
        /// 停止采集
        /// </summary>
        public bool StopCollect()
        {
            if (_hinsX.StopCollect())
            {
                NeuralDataCollector.Instance.StopCollect();
                return true;
            }
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="coreIndex"></param>
        /// <param name="registerIndex"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool ReadRegister(uint headstageIndex, uint coreIndex, uint registerIndex, out byte val)
        {
            return _hinsX.ReadRegister(headstageIndex, coreIndex, registerIndex, out val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="coreIndex"></param>
        /// <param name="registerIndex"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool WriteRegister(uint headstageIndex, uint coreIndex, uint registerIndex, byte val)
        {
            return _hinsX.WriteRegister(headstageIndex, coreIndex, registerIndex, val);
        }

        /// <summary>
        /// 阻抗测试
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="recvdPack"></param>
        /// <param name="pps"></param>
        /// <param name="ms"></param>
        /// <param name="packLen"></param>
        /// <returns></returns>
        public bool ImpedanceTest(long Id, List<int> inputChannels, bool applyNotch, double fNotch, double bandwidth, double freq, double sampleRate, Action<int, double, double> callback)
        {
            if (StateInfo.ConnectState != ConnectState.Connected)
            {
                Toast.Warn("未连接");
            }

            //
            if (_hinsX.ImpedanceTest(inputChannels, applyNotch, fNotch, bandwidth, freq, sampleRate, StairMedSettings.Instance.LowerBandwidth, StairMedSettings.Instance.UpperBandwidth, StairMedSettings.Instance.EnableDSP, StairMedSettings.Instance.DSPCutoff, callback, out string errMsg))
            {
                Toast.Info("已完成阻抗测试");
                return true;
            }

            //
            if (!string.IsNullOrWhiteSpace(errMsg))
            {
                Toast.Warn(errMsg);
            }

            //
            return false;
        }

        /// <summary>
        /// headstage是否已插入
        /// </summary>
        /// <param name="headstage"></param>
        /// <returns></returns>
        public bool IsHeadstageInserted(int headstage)
        {
            if (headstage >= 0 && headstage < StateInfo.HeadstageStates.Count)
            {
                return StateInfo.HeadstageStates[headstage].Inserted;
            }
            return false;
        }

        public event Action<int, bool> OnHeadStageInsertedStateChanged;

        public void SendArmTest(int port, int channel)
        {
            _hinsX.SendARMTest(port, channel);
        }

        public void QueryVersions()
        {
            _hinsX.QueryHardwareVersion();
        }

        public override void Destroy()
        {
            base.Destroy();
            _hinsX.Dispose();
        }
    }
}
