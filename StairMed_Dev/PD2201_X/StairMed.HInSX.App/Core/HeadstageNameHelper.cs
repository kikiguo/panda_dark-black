﻿namespace StairMed.HInSX.App.Core
{
    /// <summary>
    /// 
    /// </summary>
    internal static class HeadstageNameHelper
    {
        /// <summary>
        /// 每组headstage通道总数
        /// </summary>
        const int HeadstageGroupChannelCount = 1024;

        /// <summary>
        /// 
        /// </summary>
        const int HeadstagesOfGroup = 8;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="totalChannels"></param>
        /// <returns></returns>
        public static string GetNameByTotalChannel(int headstageIndex, int totalChannels)
        {
            if (totalChannels <= HeadstageGroupChannelCount)
            {
                return $"{(char)('A' + headstageIndex)}";
            }
            else
            {
                return $"{(char)('A' + headstageIndex / HeadstagesOfGroup)}{headstageIndex % HeadstagesOfGroup}";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headstageIndex"></param>
        /// <param name="totalHeadstage"></param>
        /// <returns></returns>
        public static string GetNameByTotalHeadstage(int headstageIndex, int totalHeadstage)
        {
            if (totalHeadstage <= HeadstagesOfGroup)
            {
                return $"{(char)('A' + headstageIndex)}";
            }
            else
            {
                return $"{(char)('A' + headstageIndex / HeadstagesOfGroup)}{headstageIndex % HeadstagesOfGroup}";
            }
        }
    }
}
