﻿using StairMed.HInSX.App.Core;
using System.Windows.Controls;

namespace StairMed.HInSX.App.Views.Regions.Bodies
{
    /// <summary>
    /// BodyDefaultView.xaml 的交互逻辑
    /// </summary>
    public partial class BodyDefaultView : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public HInSXSettings Settings { get; set; } = HInSXSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public BodyDefaultView()
        {
            this.DataContext = this;
            InitializeComponent();
        }
    }
}
