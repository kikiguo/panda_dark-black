﻿using StairMed.Controls.Controls.NeuralCharts.Base;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers.Trigger;
using StairMed.Entity.Infos;
using StairMed.Enum;
using StairMed.Event;
using StairMed.HInSX.App.ViewModels.Regions.Bodies;
using StairMed.HInSX.App.Views.Windows;
using StairMed.MVVM.Helper;
using StairMed.UI.ViewModels.UserControls.Waveforms;
using StairMed.UI.Views.UserControls.Waveforms;
using StairMed.UI.Views.Windows.Analyses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace StairMed.HInSX.App.Views.Regions.Bodies
{
    /// <summary>
    /// HInSXRTView.xaml 的交互逻辑
    /// </summary>
    public partial class HInSXRTView : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public HInSXRTView()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
            //this.cmbKMeansK.ItemsSource = CONST.SpikeScopeTypeOptions;
           // this.cmbReserved.ItemsSource = CONST.SpikeScopeGlowOptions;
            this.cmbTimeScale.ItemsSource = CONST.SpikeScopeTimeScaleMSOptions;
            this.cmbVoltageScale.ItemsSource = CONST.SpikeScopeVoltageScaleMSOptions;

            this.cmbGain.ItemsSource = CONST.SpikeScopecmbGain;

            this.cmbVoltageScale2.ItemsSource = CONST.WaveformVoltageScaleMSOptions;
            this.cmbKMeansK.ItemsSource = CONST.SpikeScopeTypeOptions;
           // this.ComboChannel.ItemsSource = CONST.SpikeScopeChannel;
            try
            {
                //var listDisplayMode = new List<string>();
                //foreach (var d in System.Enum.GetValues(typeof(EnumDisplayMode)))
                //{

                //           this.Grid_RasterPlot.RowDefinitions[0].Height = new System.Windows.GridLength( this.Grid_RasterPlot.Height / 2);
             //   this.Grid_RasterPlot.RowDefinitions[1].Height = new System.Windows.GridLength(this.Grid_RasterPlot.Height / 2);
                //    listDisplayMode.Add(d.ToString());
                //}
                //this.cmbDisplayMode.ItemsSource = listDisplayMode;
                //this.cmbDisplayMode.SelectedIndex = 0;

                //this.mcbxDisplay.ExtralSelectionChanged += (s, e) => 
                //{
                //    try
                //    {
                //        var items = ((ListBox)e.OriginalSource).SelectedItems;
                //        var dc = DataContext as HInSXRTViewModel;
                //        if (dc != null && items != null)
                //        {
                //            List<string> displayModes = new List<string>();
                //            foreach (var item in items)
                //            {
                //                var data = item as MultiCbxBaseData;
                //                if (data != null)
                //                {
                //                    displayModes.Add(data.ViewName);
                //                }
                //            }
                //            dc.WaveformSingleViewModel.DisplayRaw = displayModes.Contains("RAW");
                //            dc.WaveformSingleViewModel.DisplaySpike = displayModes.Contains("SPK");
                //            dc.WaveformSingleViewModel.DisplayLFWave = displayModes.Contains("LFP");
                //            dc.WaveformSingleViewModel.DisplayHFWave = displayModes.Contains("HFP");
                //        }


                //    }
                //    catch (Exception)
                //    {

                //    }

                //};
            }
            catch (Exception)
            {

                ;
            }
            this.cmbDisplayWindow.ItemsSource = CONST.DisplayWindowMSOptions;

            List<int> listChannels = new List<int>();
            for (int i = 1; i <= 128; i++)
            {
                listChannels.Add(i);
            }
            //this.cmbSelectChannel.ItemsSource = listChannels;

            //this.numChannels.Maximum = ReadonlyCONST.DeviceChannelCount;

            //this.cmbSpikeScopeDisplayColumnn.ItemsSource = CONST.DisplayColumnOptions;

           // this.cmbWaveformDisplayColumn.ItemsSource = CONST.WaveformDisplayColumnOptions;
            //this.cmbWaveformDisplayTRIGColumn.ItemsSource = CONST.WaveformDisplayTRIGColumnOptions;

            this.cmbPlaybackSpeed.ItemsSource = CONST.PlaySpeedOptions;

            EventHelper.Subscribe<LayoutChangedEvent,int>((_) => {
                try
                {
                    switch (_)
                    {
                        case 2:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Collapsed;
                            Grid.SetColumnSpan(this.MainCol1Left, 1);



                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Collapsed;
                            //this.Grid_SingleChannel.Visibility = System.Windows.Visibility.Collapsed;
                            Grid.SetColumn(this.Grid_WaveformScope, 0);
                            Grid.SetColumnSpan(this.Grid_WaveformScope, 1);
                            Grid.SetRowSpan(this.Grid_WaveformScope, 2);
                            this.Grid_WaveformScope.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);

                            Grid_RasterPlot.Visibility = System.Windows.Visibility.Visible;
                            Grid.SetRow(this.Grid_RasterPlot, 0);
                            Grid.SetRowSpan(this.Grid_RasterPlot, 2);
                            Grid.SetColumn(this.Grid_RasterPlot, 1);
                            Grid.SetColumnSpan(this.Grid_RasterPlot, 1);
                            Grid_RasterPlotBorder.BorderThickness = new System.Windows.Thickness(2, 0, 0, 0);

                            Grid.SetRowSpan(this.Grid_CurrentChannel, 1);
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Visible;

                            RasterPlot.PopDown();

                            break;
                        case 3:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Visible;
                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Visible;
                            //this.Grid_SingleChannel.Visibility = System.Windows.Visibility.Collapsed;
                            //this.LeftRow1.Height = new System.Windows.GridLength(5, System.Windows.GridUnitType.Star);
                            //this.LeftRow2.Height = new System.Windows.GridLength(5, System.Windows.GridUnitType.Star);
                            Grid.SetColumn(this.Grid_WaveformScope, 1);
                            Grid.SetColumnSpan(this.Grid_WaveformScope, 1);
                            Grid.SetRowSpan(this.Grid_WaveformScope, 1);
                            this.Grid_WaveformScope.BorderThickness = new System.Windows.Thickness(2, 0, 0, 0);

                            Grid_RasterPlot.Visibility = System.Windows.Visibility.Visible;
                            Grid.SetRow(this.Grid_RasterPlot, 1);
                            Grid.SetRowSpan(this.Grid_RasterPlot, 1);
                            Grid.SetColumn(this.Grid_RasterPlot, 0);
                            Grid.SetColumnSpan(this.Grid_RasterPlot, 2);
                            Grid_RasterPlotBorder.BorderThickness = new System.Windows.Thickness(0, 2, 0, 0);

                         //   Grid.SetRow(this.Grid_SingleChannel, 1);
                           // Grid.SetRowSpan(this.Grid_SingleChannel, 1);

                            Grid.SetColumnSpan(this.MainCol1Left, 1);
                            Grid.SetRowSpan(this.Grid_CurrentChannel, 1);
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Visible;

                            break;
                        case 4:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Visible;
                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Visible;
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Visible;
                            this.MainCol3.Width = new GridLength(1, GridUnitType.Star);
                            this.MainCol2.Width = new GridLength(1, GridUnitType.Star);
                            this.MainCol1.Width = new GridLength(1, GridUnitType.Star);

                            RasterPlot.PopDown();

                            break;
                        case 22:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Visible;
                         //   this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Visible;
                          //  this.Grid_SingleChannel.Visibility = System.Windows.Visibility.Visible;
                            //this.LeftRow1.Height = new System.Windows.GridLength(6, System.Windows.GridUnitType.Star);
                            //this.LeftRow2.Height = new System.Windows.GridLength(4, System.Windows.GridUnitType.Star);
                            Grid.SetColumn(this.Grid_WaveformScope, 1);
                            Grid.SetColumnSpan(this.Grid_WaveformScope, 1);
                            Grid.SetRowSpan(this.Grid_WaveformScope, 2);

                        //    Grid.SetRow(this.Grid_SingleChannel, 1);
                        //    Grid.SetRowSpan(this.Grid_SingleChannel, 1);

                            this.Grid_WaveformScope.BorderThickness = new System.Windows.Thickness(2, 0, 0, 0);
                            Grid_RasterPlotBorder.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);
                            Grid_RasterPlot.Visibility = System.Windows.Visibility.Collapsed;

                            Grid.SetColumnSpan(this.MainCol1Left, 1);
                            Grid.SetRowSpan(this.Grid_CurrentChannel, 1);
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Visible;

                            RasterPlot.PopUp();

                            break;

                        case 11:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Collapsed;
                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Visible;
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Collapsed;
                            this.MainCol3.Width = new GridLength(0);
                            this.MainCol2.Width = new GridLength(10, GridUnitType.Star);
                            this.MainCol1.Width = new GridLength(0);
                            break;
                        case 12:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Collapsed;
                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Collapsed;
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Visible;
                            this.MainCol3.Width = new GridLength(10, GridUnitType.Star);
                            this.MainCol2.Width = new GridLength(0);
                            this.MainCol1.Width = new GridLength(0);
                            break;
                        case 13:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Visible;
                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Collapsed;
                        //    this.Grid_SingleChannel.Visibility = System.Windows.Visibility.Visible;
                            //this.LeftRow1.Height = System.Windows.GridLength.Auto;
                            //this.LeftRow2.Height = new System.Windows.GridLength(10, System.Windows.GridUnitType.Star);


                            Grid.SetColumnSpan(this.MainCol1Left, 2);
                            Grid.SetRowSpan(this.Grid_CurrentChannel, 1);
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Collapsed;
                            this.Grid_RasterPlot.Visibility = System.Windows.Visibility.Collapsed;

                       //     Grid.SetRow(this.Grid_SingleChannel, 0);
                        //    Grid.SetRowSpan(this.Grid_SingleChannel, 2);


                            Grid.SetColumn(this.Grid_WaveformScope, 1);
                            Grid.SetColumnSpan(this.Grid_WaveformScope, 1);
                            Grid.SetRowSpan(this.Grid_WaveformScope, 1);
                            this.Grid_WaveformScope.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);

                            //Grid_RasterPlot.Visibility = System.Windows.Visibility.Visible;
                            Grid.SetRow(this.Grid_RasterPlot, 1);
                            Grid.SetRowSpan(this.Grid_RasterPlot, 1);
                            Grid.SetColumn(this.Grid_RasterPlot, 1);
                            Grid.SetColumnSpan(this.Grid_RasterPlot, 1);
                            Grid_RasterPlotBorder.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);
                            break;
                        case 14:
                            this.MainCol1Left.Visibility = System.Windows.Visibility.Visible;
                            this.Grid_CurrentChannel.Visibility = System.Windows.Visibility.Collapsed;
                            this.Grid_WaveformScope.Visibility = System.Windows.Visibility.Collapsed;
                            var row = new RowDefinition() { Height = new GridLength(100) };
                            this.MainCol3.Width = new GridLength(0);
                            this.MainCol2.Width = new GridLength(0);
                            this.MainCol1.Width = new GridLength(10,GridUnitType.Star);
                            break;
                        default:
                            break;
                    }
                }
                catch(Exception ex)
                { 
                
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnProbeMapSpike_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SpikeMapWnd.PopUp();
        }

        #region 伪造trigger信号

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTrigger_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _autoGenerate = !_autoGenerate;
        }

        /// <summary>
        /// 
        /// </summary>
        private int _index = 0;
        private readonly Dictionary<int, bool> _levelDict = new Dictionary<int, bool>();
        private long _lastTriggerIndex = 0;
        private bool _autoGenerate = false;

        /// <summary>
        /// 
        /// </summary>
        public void GenerateTriggerLoop()
        {
            while (true)
            {
                try
                {
                    while (_autoGenerate)
                    {
                        Thread.Sleep(200);
                        //Thread.Sleep(50);

                        //
                        _index++;
                        if (!NeuralDataCollector.Instance.IsCollecting)
                        {
                            break;
                        }

                        //
                        var triggerChannels = new HashSet<int>();
                        for (int i = 0; i < ReadonlyCONST.TriggerChannels.Count; i++)
                        {
                            if (_index % (i * 2 + 2) == 0)
                            {
                                triggerChannels.Add(i);
                            }
                        }

                        //
                        if (triggerChannels.Count <= 0)
                        {
                            continue;
                        }

                        //
                        var pack = new TriggerPack
                        {
                            SampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate(),
                            Triggers = new Dictionary<int, List<TriggerEdge>>()
                        };

                        //
                        var offset = NeuralDataCollector.Instance.RecvdLength;

                        //
                        if (offset <= _lastTriggerIndex)
                        {
                            offset = _lastTriggerIndex + 300;
                        }
                        _lastTriggerIndex = offset;

                        //
                        foreach (var channel in triggerChannels)
                        {
                            var oldLevel = _levelDict.ContainsKey(channel) ? _levelDict[channel] : false;

                            //
                            pack.Triggers[channel] = new List<TriggerEdge>
                            {
                                new TriggerEdge { ToHigh = oldLevel, Index = offset}
                            };

                            //
                            _levelDict[channel] = !oldLevel;
                        }

                        //
                        TriggerDataPool.Instance.BufferTrigger(pack);
                    }
                }
                catch { }
                Thread.Sleep(1000);
                _lastTriggerIndex = 0;
            }
        }

        #endregion
        private void SpikeScopeChart_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                UpdateThreshold(e);
            }
            else
            {
                UpdateUVScale(e);
            }

            //
            e.Handled = true;
        }
        private void UpdateUVScale(MouseWheelEventArgs e)
        {
            var index = CONST.SpikeScopeVoltageScaleMSOptions.IndexOf(StairMedSettings.Instance.SpikeScopeVoltageRange);
            if (index < 0)
            {
                StairMedSettings.Instance.SpikeScopeVoltageRange = CONST.SpikeScopeVoltageScaleMSOptions[0];
            }
            else
            {
                index += (e.Delta < 0 ? 1 : -1);
                index = Math.Max(0, Math.Min(CONST.SpikeScopeVoltageScaleMSOptions.Count - 1, index));
                StairMedSettings.Instance.SpikeScopeVoltageRange = CONST.SpikeScopeVoltageScaleMSOptions[index];
            }
        }
        private void UpdateThreshold(MouseWheelEventArgs e)
        {
            var threshold = this.numThreshold.Value;
            threshold += (e.Delta > 0 ? 1 : -1);
            this.numThreshold.Value = threshold;
        }



            private void cmbDisplayMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ClearDisplayMode();
            //var selectedValue = this.cmbDisplayMode.SelectedValue?.ToString();
            //switch (selectedValue)
            //{
            //    case "RAW":
            //        StairMedSettings.Instance.DisplayRaw = true;
            //        break;
            //    case "SPK":
            //        StairMedSettings.Instance.DisplaySpike = true;
            //        break;
            //    case "LFP":
            //        StairMedSettings.Instance.DisplayLFWave = true;
            //        break;
            //    case "HFP":
            //        StairMedSettings.Instance.DisplayHFWave = true;
            //        break;
            //    case "FR":
            //        StairMedSettings.Instance.DisplaySpikeSum = true;
            //        break;
            //    default:
            //        StairMedSettings.Instance.DisplayRaw = true;
            //        break;

            //}
        }

        private void ClearDisplayMode()
        {
            //StairMedSettings.Instance.DisplayRaw = false;

            //StairMedSettings.Instance.DisplaySpike = false;

            //StairMedSettings.Instance.DisplayLFWave = false;

            //StairMedSettings.Instance.DisplayHFWave = false;

            //StairMedSettings.Instance.DisplaySpikeSum = false;
        }

        private int currentIndexGroup = 0;
        private int countPerPage = 8;
        private void btnPrev_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (currentIndexGroup > 0)
            {
                currentIndexGroup -= 1;
                ThumbnailListBoxScrollTo();
            }

        }

        private void btnNext_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            int totalPage = (int)(listBoxHeadstages.Items.Count / countPerPage);
            if (currentIndexGroup < totalPage)
            {
                currentIndexGroup += 1;
                ThumbnailListBoxScrollTo();
            }
        }

        private void ThumbnailListBoxScrollTo()
        {
            try
            {
                var divWidth = this.DivHeadstages.ActualWidth;
                Decorator decorator = (Decorator)VisualTreeHelper.GetChild(listBoxHeadstages, 0);
                ScrollViewer scrollViewer = (ScrollViewer)decorator.Child;
                if (scrollViewer != null)
                {
                    var leftOffset = currentIndexGroup * divWidth;
                    if (currentIndexGroup > 0)
                    {
                        leftOffset += (currentIndexGroup - 1) * countPerPage;
                    }
                    scrollViewer.ScrollToHorizontalOffset(leftOffset);
                }
            }
            catch
            {

            }
            
        }

        private void btnClean_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            StairMedSettings.Instance.IsClean = true;
            DateTime now = DateTime.Now;
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long timestamp = (long)(now.ToUniversalTime() - epoch).TotalMilliseconds;
            StairMedSettings.Instance.cleanTiem = timestamp + 2000;
          
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // 调用按钮点击事件处理函数以模拟点击效果，达到默认隐藏端口控制的目的
            btnMinimizePort_Click(btnMinimize, new RoutedEventArgs());
        }
        private void btnMinimizePort_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            grid_port.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnMaxPort_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            grid_port.Visibility = System.Windows.Visibility.Visible;
        }
        bool isFirst = true;
        private void cmbChangePlaySpeed(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox comboBox = sender as ComboBox;
                if (comboBox != null)
                {
                    //ComboBoxItem comboBoxItem = comboBox.SelectedValue as ComboBoxItem;
                    //if (comboBoxItem != null)
                    {
                        double p = 0;
                        if (!double.TryParse(comboBox.SelectedValue?.ToString(), out p))
                        {
                            p = 0;
                        }
                        if (p > 0 && !isFirst)
                        {
                            EventHelper.Publish<PlayNWBFileEvent, double>(p);
                        }
                    }
                }
            }
            catch (Exception)
            {


            }
            finally
            {
                isFirst = false;
            }
                        
            
        }
    }
}
