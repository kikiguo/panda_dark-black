﻿using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.FilterProcess;
using StairMed.Tools;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace StairMed.HInSX.App.Views.Regions
{
    /// <summary>
    /// HInSNeckView.xaml 的交互逻辑
    /// </summary>
    public partial class HInSNeckView : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public HInSNeckView()
        {
            InitializeComponent();

            //
            this.BindingCaptionClick();

            //
            //Task.Factory.StartNew(() =>
            //{
            //    while (true)
            //    {
            //        Thread.Sleep(100);
            //        UIHelper.Invoke(() =>
            //        {
            //            this.txtTime.Text = DateTime.Now.ToString("HH:mm:ss.f");
            //        });
            //    }
            //});


            //
            Task.Run(() =>
            {
                Thread.Sleep(500);
                var cudaSupport = XPUFactory.Instance.IsSupportCuda();
                if (!cudaSupport)
                {
                    UIHelper.BeginInvoke(() =>
                    {
                        //this.txtCUDA.Visibility = System.Windows.Visibility.Visible;
                        MsgBox.Error("不支持cuda，请换台电脑（需含NVIDIA显卡）", "CUDA支持");
                    });
                }
            });
        }
    }
}
