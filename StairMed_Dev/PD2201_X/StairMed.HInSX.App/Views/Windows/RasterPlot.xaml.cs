﻿using StairMed.AsyncFIFO.Logger;
using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// Interaction logic for RasterPlot.xaml
    /// </summary>
    public partial class RasterPlot : Window
    {
        private static Window _wnd = null;
        public RasterPlot()
        {
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.WindowState = WindowState.Normal;
            this.btnRestore.Visibility = Visibility.Collapsed;

            this.cmbWaveformDisplayColumn.ItemsSource = CONST.WaveformDisplayColumnOptions;
            this.cmbWaveformDisplayTRIGColumn.ItemsSource = CONST.WaveformDisplayTRIGColumnOptions;

            this.BindingDragMove();


            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
            this.Closed += (object sender, EventArgs e) =>
            {
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);

                //
                Application.Current.MainWindow.TakeToFront();
                _wnd = null;
            };

        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new RasterPlot().Show();
                }
            });
        }

        public static void PopDown()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.Close();
                }
            });
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
