﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.HInSX.App.Logger;
using StairMed.Tools;
using System.Diagnostics;
using System.Windows;
using System.Windows.Documents;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSXHardwareWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSXHardwareWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal HInSXHardwareWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();

            this.Closed += AboutWindow_Closed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutWindow_Closed(object sender, System.EventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            _wnd = null;
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var url = ((Hyperlink)sender).NavigateUri.AbsoluteUri;
                Process.Start("explorer.exe", url);
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new HInSXHardwareWnd().Show();
                }
            });
        }
    }
}
