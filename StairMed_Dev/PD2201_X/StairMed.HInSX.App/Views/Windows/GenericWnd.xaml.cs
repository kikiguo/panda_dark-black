﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Logger;
using System;
using System.Windows;
using System.Windows.Media.Media3D;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// SettingWnd.xaml 的交互逻辑
    /// </summary>
    public partial class GenericWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal GenericWnd(string winTitle,object obj, int width, int height)
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");
            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.btnRestore.Visibility = Visibility.Collapsed;
            TbWinTitle.Text = winTitle;
            this.contentControl.Content = obj;
            this.Closed += SettingWindow_Closed;
            this.Width = width;
            this.Height = height;
            EventHelper.Subscribe<GenericWndCloseEvent, string>((s) => {
                if (winTitle == s)
                    this.Close();
            }, Prism.Events.ThreadOption.UIThread);
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingWindow_Closed(object sender, EventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            _wnd = null;
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp(string winTitle, object obj, int width = 1200, int height = 700)
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new GenericWnd(winTitle,obj, width, height).Show();
                }
            });
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btnMin_Click(object sender, RoutedEventArgs e)
        {  
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
