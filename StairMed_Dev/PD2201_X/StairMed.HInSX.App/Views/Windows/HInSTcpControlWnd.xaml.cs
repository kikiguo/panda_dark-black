﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Event;
using StairMed.HInSX.App.Logger;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using System;
using System.Windows;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSTcpControlWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSTcpControlWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal HInSTcpControlWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                EventHelper.Publish<WindowLoadEvent, string>(this.GetType().Name);
            };
            this.Closed += (object sender, EventArgs e) =>
            {
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);
                _wnd = null;
                Application.Current.MainWindow.TakeToFront();
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new HInSTcpControlWnd().Show();
                }
            });
        }
    }
}
