﻿using StairMed.Controls;
using StairMed.HInSX.Command;
using System;
using System.Windows;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSSelectWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSSelectWnd
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Action<HInSXType> _succeedAction;

        /// <summary>
        /// 
        /// </summary>
        public HInSSelectWnd(Action<HInSXType> onSucceed)
        {
            _succeedAction = onSucceed;

            //
            InitializeComponent();
            this.BindingDragMove();

            //
            rbtn1024.IsChecked = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DelayClose();

            //
            var hinsType = HInSXType.H_Unknown;
            if (rbtn1024.IsChecked.HasValue && rbtn1024.IsChecked.Value)
            {
                hinsType = HInSXType.HInS_1024;
            }
            else
            {
                hinsType = HInSXType.HInS_2048;
            }

            //
            _succeedAction?.Invoke(hinsType);
        }
    }
}
