﻿using StairMed.Controls;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StairMed.UI.Logger;
using StairMed.ChipConst.Intan;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.UI.Views.Windows.Analyses;
using StairMed.HInSX.App.Views.Windows;
using ImTools;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// SpikeSample.xaml 的交互逻辑
    /// </summary>
    public partial class SpikeSampleWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        /// <summary>
        /// 
        /// </summary>
        internal SpikeSampleWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.btnRestore.Visibility = Visibility.Collapsed;
            this.Closed += SettingWindow_Closed;

            this.SpikeRate.ItemsSource = CONST.SpikeRate;
            this.LfpRate.ItemsSource = CONST.SpikeRate;
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingWindow_Closed(object sender, EventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            _wnd = null;
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new SpikeSampleWnd().Show();
                }
            });
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
