﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.HInSX.App.Logger;
using StairMed.Tools;
using System;
using System.Windows;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSRegisterWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSRegisterWnd
    {
        /// <summary>
        /// 单例窗口
        /// </summary>
        private static Window _wnd = null;

        /// <summary>
        /// 
        /// </summary>
        internal HInSRegisterWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            _wnd = this;
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.Closed += RWRegisterWindow_Closed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RWRegisterWindow_Closed(object sender, EventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            _wnd = null;
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PopUp()
        {
            UIHelper.BeginInvoke(() =>
            {
                if (_wnd != null)
                {
                    _wnd.TakeToFront();
                }
                else
                {
                    new HInSRegisterWnd().Show();
                }
            });
        }
    }
}
