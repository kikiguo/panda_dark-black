﻿using Prism.Regions;
using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.Enum;
using StairMed.HinSX.App.Views.Windows;
using StairMed.HInSX.App.Logger;
using StairMed.HInSX.App.Views.Regions;
using StairMed.HInSX.App.Views.Regions.Bodies;
using StairMed.MVVM.Helper;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSMainstayWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSMainstayWnd
    {
        public static RoutedCommand ShortCutCommand = new RoutedCommand();

        /// <summary>
        /// 
        /// </summary>
        public HInSMainstayWnd()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            RegionManager.SetRegionManager(this, RegionHelper.RegionManager);

            //
            InitializeComponent();
            this.WindowState = WindowState.Maximized;
            this.btnMaximize.Visibility = Visibility.Collapsed;

            //
            Application.Current.MainWindow = this;
            //
           this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                RegionHelper.RequestNavigate(RegionNames.HeadRegion, nameof(HInSHeadView));
                RegionHelper.RequestNavigate(RegionNames.NeckRegion, nameof(HInSNeckView));
                RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(HInSXRTView));
            };

            //
            this.BindingDragMove();

            //
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };

            //
            this.Closing += Window_Closing;

            ReadonlyCONST.DeviceType = DeviceType.HInSX;
            ReadonlyCONST.DeviceChannelCount = HInSXCONST.CHANNELS_COUNT;
            //ReadonlyCONST.ProbeMap = ProbeMapSettings.Instance.ProbeMap;
            //RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(HInSXRTView));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (NeuralDataCollector.Instance.IsCollecting)
            {
                Toast.Warn("请停止采集，再做关闭");
                e.Cancel = true;
                return;
            }

            //
            if (MsgBox.AskAnxious("Sure you want out?", "", this) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
                return;
            }

            //
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
