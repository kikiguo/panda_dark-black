﻿using StairMed.Controls;
using StairMed.Controls.Windows;
using StairMed.Enum;
using StairMed.HInSX.Command;
using StairMed.Tools;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.InteropServices;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSDetectionWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSDetectionWnd
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(HInSDetectionWnd);

        /// <summary>
        /// 
        /// </summary>
        //const int DetectionMS = 30 * 1000;
        const int DetectionMS = 5 * 1000;
        //const int DetectionMS = 500;

        /// <summary>
        /// 
        /// </summary>
        private readonly Action<HInSXType> _succeedAction;

        /// <summary>
        /// 
        /// </summary>
        private readonly Action _failedAction;

        /// <summary>
        /// 
        /// </summary>
        private HInSXDetection _detection = null;

        /// <summary>
        /// 设备类型是否靠谱
        /// </summary>
        private HInSXType _hinsType = HInSXType.H_Unknown;

        /// <summary>
        /// 网络连接是否正常
        /// </summary>
        private bool _tcpOk = false;

        /// <summary>
        /// USB连接是否正常
        /// </summary>
        private bool _usbOk = false;

        /// <summary>
        /// 停止检查
        /// </summary>
        private bool _stopCheck = false;

        /// <summary>
        /// 
        /// </summary>
        public HInSDetectionWnd(Action<HInSXType> onSucceed, Action onFailed)
        {
            _succeedAction = onSucceed;
            _failedAction = onFailed;

            //
            InitializeComponent();
            this.BindingDragMove();

            this.MaxHeight = SystemParameters.WorkArea.Height;
            this.WindowState = WindowState.Maximized;

            //
            this.Loaded += Wnd_Loaded;
            this.Closed += Wnd_Closed;
        }

        /// <summary>
        /// 窗口加载后开始检测
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wnd_Loaded(object sender, RoutedEventArgs e)
        {
            //
            _detection = new HInSXDetection();
            _detection.Start();
            InitStatus.Text = "---Panda Control---\r\n System checking ……";
            //
            Task.Run(() =>
            {
                ModifyLocalIPAddress("192.168.52.1", "255.255.255.0");
                //Thread.Sleep(30000);
                int pingSumCount = 5;//5
                int pingCount = 0;

                do
                {
                    if (_detection.InitPing() || pingCount >= pingSumCount)
                    {
                        break;
                    }
                    pingCount++;
                } while (true);
                _detection.EndInitPing();
                //EndInitPing()
                var ips = ToolMix.GetLocalIp();
                if ((!ips.Any(r => r.StartsWith("192.168.52."))) || (ips.Any(r => r.Equals("192.168.52.1"))))
                {
                    UIHelper.BeginInvoke(() =>
                    {
                        MsgBox.Warn($"Hardware detection failed！", "Connection  failed");
                        this.DelayClose();
                        _failedAction?.Invoke();
                    });
                    return;
                }

                //
                CheckDetectionResultPeriodical();

                //
                UIHelper.BeginInvoke(() =>
                {
                    if (!_tcpOk)
                    {
                        MsgBox.Warn($"Hardware detection failed");
                        //
                        this.DelayClose();
                        _failedAction?.Invoke();
                    }
                    else
                    {
                        switch (_hinsType)
                        {
                            case HInSXType.HInS_1024:
                                {
                                    MsgBox.Info($"Connected successfully");

                                    //
                                    this.DelayClose();
                                    _succeedAction?.Invoke(_hinsType);
                                }
                                break;
                            case HInSXType.HInS_2048:
                                {
                                    if (_usbOk)
                                    {
                                        MsgBox.Info($"Connected successfully");
                                    }
                                    else
                                    {
                                        MsgBox.Warn($"Hardware detection failed");
                                    }

                                    //
                                    this.DelayClose();
                                    _succeedAction?.Invoke(_hinsType);
                                }
                                break;
                            case HInSXType.H_Unknown:
                            default:
                                {
                                    MsgBox.Warn($"Hardware detection failed", "Connection  failed");

                                    //
                                    this.DelayClose();
                                    _failedAction?.Invoke();
                                }
                                break;
                        }

                    }
                });
            });
        }

        /// <summary>
        /// 周期性检查是否已检测到设备
        /// </summary>
        private void CheckDetectionResultPeriodical()
        {
            try
            {
                var watch = new Stopwatch();
                watch.Start();

                //
                while (!_stopCheck)
                {
                    Thread.Sleep(200);

                    //
                    if (!_stopCheck)
                    {
                        //网络是否可通
                        if (_detection.ConnectionState == ConnectState.Connected)
                        {
                            _tcpOk = true;
                        }

                        //网络可通的情况下，如果设备类型也没问题
                        if (_detection.HInSType != HInSXType.H_Unknown)
                        {
                            _hinsType = _detection.HInSType;
                        }

                        //
                        _usbOk = _detection.IsUsbOk;

                        //
                        if (_tcpOk && ((_hinsType == HInSXType.HInS_1024) || (_hinsType == HInSXType.HInS_2048 && _usbOk)))
                        {
                            break;
                        }

                        //
                        if (watch.ElapsedMilliseconds > DetectionMS)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}", ex);
            }
        }

        /// <summary>
        /// 关闭窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _stopCheck = true;
            this.Close();
        }

        /// <summary>
        /// 窗口关闭时，释放detection资源
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wnd_Closed(object sender, EventArgs e)
        {
            _stopCheck = true;
            if (_detection != null)
            {
                _detection.Dispose();
                _detection = null;
            }
        }

        /// <summary>
        /// 本地IP引入dll
        /// </summary>
        [DllImport("iphlpapi.dll", SetLastError = true)]
        private static extern int SetIpAndMask(string adapterName, uint ipAddress, uint ipMask);

        /// <summary>
        /// 修改本地IP
        /// </summary>
        /// <param name="newIpAddress"></param>
        static void ModifyLocalIPAddress(string newIpAddress, string subnetMask)
        {
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    networkInterface.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties ipProperties = networkInterface.GetIPProperties();

                    foreach (UnicastIPAddressInformation ipAddressInfo in ipProperties.UnicastAddresses)
                    {
                        if (ipAddressInfo.Address.AddressFamily == AddressFamily.InterNetwork &&
                            ipAddressInfo.Address.ToString().StartsWith("192.168.52."))
                        {
                            try
                            {
                                uint ip = BitConverter.ToUInt32(IPAddress.Parse(newIpAddress).GetAddressBytes(), 0);
                                uint mask = BitConverter.ToUInt32(IPAddress.Parse(subnetMask).GetAddressBytes(), 0);

                                int result = SetIpAndMask(networkInterface.Name, ip, mask);

                                if (result == 0)
                                {
                                    Console.WriteLine("IP address modified successfully.");
                                    return;
                                }
                                else
                                {
                                    Console.WriteLine("Failed to modify IP address. Error code: " + result);
                                }
                            }
                            catch (Exception ex)
                            {
                                // 处理异常
                                Console.WriteLine("Failed to modify IP address: " + ex.Message);
                            }
                        }
                    }
                }
            }

            Console.WriteLine("No active network adapter found.");
        }



    }
 }
