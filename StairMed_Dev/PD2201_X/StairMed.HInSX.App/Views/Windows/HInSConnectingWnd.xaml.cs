﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.Core.Helpers;
using StairMed.Enum;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Logger;
using StairMed.Tools;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSConnectingWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSConnectingWnd
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Action _cancel;

        /// <summary>
        /// 
        /// </summary>
        public HInSConnectingWnd(Action cancel)
        {
            _cancel = cancel;
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();

            //
            this.Closed += HInSConnectingWnd_Closed;

            //
            Task.Run(() =>
            {
                Thread.Sleep(1000);

                //
                var watch = new Stopwatch();
                watch.Start();

                //
                while (true)
                {
                    if (HInSXManager.Instance.StateInfo.ConnectState == ConnectState.Connected)
                    {
                        break;
                    }

                    //
                    if (watch.ElapsedMilliseconds > 25000)
                    {
                        break;
                    }

                    //
                    Thread.Sleep(1000);
                }

                //
                Thread.Sleep(100);
                UIHelper.BeginInvoke(() =>
                {
                    this.Close();
                });
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HInSConnectingWnd_Closed(object sender, System.EventArgs e)
        {
            LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
            Application.Current.MainWindow.TakeToFront();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            _cancel?.Invoke();
            this.Close();
        }
    }
}
