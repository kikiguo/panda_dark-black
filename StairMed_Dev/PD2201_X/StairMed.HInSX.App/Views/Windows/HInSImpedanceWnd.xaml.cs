﻿using StairMed.Controls;
using StairMed.Core.Consts;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Logger;
using StairMed.Event;
using StairMed.MVVM.Helper;
using System;
using System.Windows;

namespace StairMed.HInSX.App.Views.Windows
{
    /// <summary>
    /// HInSImpedanceWnd.xaml 的交互逻辑
    /// </summary>
    public partial class HInSImpedanceWnd
    {
        /// <summary>
        /// 
        /// </summary>
        internal HInSImpedanceWnd(long deviceId)
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [window]");

            //
            this.Owner = Application.Current.MainWindow;
            InitializeComponent();
            this.BindingDragMove();
            this.btnRestore.Visibility = Visibility.Collapsed;
            //
            this.txtDeviceId.Text = deviceId.ToString();
            this.txtDeviceName.Text = HInSXManager.Instance.DeviceInfo.Name;

            //
            this.Loaded += (object sender, RoutedEventArgs e) =>
            {
                EventHelper.Publish<WindowLoadEvent, string>(this.GetType().Name);
            };
            this.Closed += (object sender, EventArgs e) =>
            {
                LogTool.Logger.LogT($"{this.GetType().Name} closed [window]");
                EventHelper.Publish<WindowCloseEvent, string>(this.GetType().Name);
                Application.Current.MainWindow.TakeToFront();
            };
            this.StateChanged += (object? sender, EventArgs e) =>
            {
                this.btnRestore.Visibility = this.WindowState == WindowState.Normal ? Visibility.Collapsed : Visibility.Visible;
                this.btnMaximize.Visibility = this.WindowState == WindowState.Maximized ? Visibility.Collapsed : Visibility.Visible;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnMin_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Maximized;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Normal;
        }
    }
}
