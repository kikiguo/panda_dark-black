﻿using StairMed.Core.Consts;
using StairMed.Enum;
using System.Collections.Generic;
using System.Linq;

namespace StairMed.HInSX.App
{
    /// <summary>
    /// 
    /// </summary>
    internal class Initializer
    {
        /// <summary>
        /// 
        /// </summary>
        public static void Init()
        {
            //
            ReadonlyCONST.DeviceType = DeviceType.HInSX;

            //
            ReadonlyCONST.DeviceChannelCount = HInSXCONST.CHANNELS_COUNT;

            //
            ReadonlyCONST.TriggerChannels = Enumerable.Range(1, 5).ToList().AsReadOnly();

            //
            // ReadonlyCONST.SampleRates = new List<int> { 10 * 1000, 15 * 1000, 20 * 1000, 25 * 1000, 30 * 1000 }.AsReadOnly();
            ReadonlyCONST.SampleRates = new List<int> {25 * 1000}.AsReadOnly();

            //ReadonlyCONST.SampleRates = Enumerable.Range(10000, 20005).Where(r => r % 1000 == 0).ToList().AsReadOnly();
        }
    }
}
