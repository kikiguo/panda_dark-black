﻿using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Enum;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Logger;
using StairMed.MVVM.Base;
using StairMed.Tools;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    /// <summary>
    /// 
    /// </summary>
    internal class HInSRegisterWndViewModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private const string TAG = nameof(HInSRegisterWndViewModel);

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<HeadStage> _headstages = new ObservableCollection<HeadStage>();
        public ObservableCollection<HeadStage> Headstages
        {
            get { return _headstages; }
            set { Set(ref _headstages, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<Register> _registers = new ObservableCollection<Register>();
        public ObservableCollection<Register> Registers
        {
            get { return _registers; }
            set { Set(ref _registers, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private bool _firstCore = true;
        public bool FirstCore
        {
            get { return _firstCore; }
            set { Set(ref _firstCore, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _showLoading = false;
        public bool ShowLoading
        {
            get { return _showLoading; }
            set { Set(ref _showLoading, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSRegisterWndViewModel()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [windowviewmodel]");

            //
            for (int i = 0; i < 8; i++)
            {
                Headstages.Add(new HeadStage
                {
                    Name = ((char)('A' + i)).ToString(),
                    Selected = i == 0
                });
            }

            for (int i = 0; i < 64; i++)
            {
                Registers.Add(new Register
                {
                    Index = i,
                    Content = "0"
                });
            }
        }

        /// <summary>
        /// 写寄存器
        /// </summary>
        public ICommand WriteRegister => new DelegateCommand<object>((obj) =>
        {
            var registerIndex = (int)obj;
            var headstageIndex = GetHeadstageIndex();
            var coreIndex = FirstCore ? 0 : 1;

            //
            if (!int.TryParse(Registers[registerIndex].Content, out int result))
            {
                Toast.Error($"输入内容有误，无法转换为有效数字");
                return;
            }
            if (result < 0 || result > 255)
            {
                Toast.Error($"输入内容有误，应在0-255之间");
                return;
            }

            //
            var val = (byte)(result & 0xff);

            //
            ShowLoading = true;
            Task.Run(() =>
            {
                try
                {
                    if (HInSXManager.Instance.WriteRegister((uint)headstageIndex, (uint)coreIndex, (uint)registerIndex, val))
                    {
                        UIHelper.Invoke(() =>
                        {
                            Toast.Info($"写入成功，index:{registerIndex + 1}, val:{val}");
                            Registers[registerIndex].Content = val.ToString();
                        });
                    }
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(ReadAllRegister)}", ex);
                }

                UIHelper.BeginInvoke(() =>
                {
                    ShowLoading = false;
                });
            });
        });

        /// <summary>
        /// 读寄存器
        /// </summary>
        public ICommand ReadRegister => new DelegateCommand<object>((obj) =>
        {
            var registerIndex = (int)obj;
            var headstageIndex = GetHeadstageIndex();
            var coreIndex = FirstCore ? 0 : 1;

            //
            ShowLoading = true;
            Task.Run(() =>
            {
                try
                {
                    if (HInSXManager.Instance.ReadRegister((uint)headstageIndex, (uint)coreIndex, (uint)registerIndex, out byte val))
                    {
                        Toast.Info($"读取成功，index:{registerIndex + 1}, val:{val}");
                        UIHelper.Invoke(() =>
                        {
                            Registers[registerIndex].Content = val.ToString();
                        });
                    }
                    else
                    {
                        Toast.Info($"读取失败，index:{registerIndex + 1}");
                    }
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(ReadAllRegister)}", ex);
                }

                UIHelper.BeginInvoke(() =>
                {
                    ShowLoading = false;
                });
            });
        });

        /// <summary>
        /// 读取全部寄存器
        /// </summary>
        public ICommand ReadAllRegister => new DelegateCommand<object>((obj) =>
        {
            var headstageIndex = GetHeadstageIndex();
            var coreIndex = FirstCore ? 0 : 1;

            //
            ShowLoading = true;
            Task.Run(() =>
            {
                try
                {
                    foreach (var reg in Registers)
                    {
                        var registerIndex = reg.Index;
                        if (HInSXManager.Instance.ReadRegister((uint)headstageIndex, (uint)coreIndex, (uint)registerIndex, out byte val))
                        {
                            UIHelper.Invoke(() =>
                            {
                                reg.Content = val.ToString();
                            });
                        }
                        else
                        {
                            Toast.Info($"读取失败，index:{registerIndex + 1}");
                        }

                        //
                        if (HInSXManager.Instance.StateInfo.ConnectState != ConnectState.Connected)
                        {
                            break;
                        }
                    }
                    Toast.Info($"读取完毕");
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(ReadAllRegister)}", ex);
                }

                //
                UIHelper.BeginInvoke(() =>
                {
                    ShowLoading = false;
                });
            });
        });


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private int GetHeadstageIndex()
        {
            var headstageIndex = -1;
            for (int i = 0; i < Headstages.Count; i++)
            {
                if (Headstages[i].Selected)
                {
                    headstageIndex = i;
                    break;
                }
            }
            return headstageIndex;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class HeadStage : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { Set(ref _name, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _selected = false;
        public bool Selected
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    internal class Register : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        private string _content = "0";
        public string Content
        {
            get { return _content; }
            set { Set(ref _content, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _index = 0;
        public int Index
        {
            get { return _index; }
            set { Set(ref _index, value); }
        }

    }
}
