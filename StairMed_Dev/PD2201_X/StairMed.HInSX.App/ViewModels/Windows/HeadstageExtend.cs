﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    public class HeadstageExtend
    {
        public bool IsInserted { get; set; }
        public string HeadContent { get; set; }
    }
}
