﻿using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.Entity.Infos.Bases;
using StairMed.Event;
using StairMed.Event.RemoteTCPControl;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.HInSX.App.Views.Windows;
using StairMed.MVVM.Helper;
using StairMed.Probe;
using StairMed.Tools;
using StairMed.UI.ViewModels.UserControls.Waveforms;
using StairMed.UI.Views.Windows.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    internal class SpikeScopeMainWndViewModel : WaveformVMBase
    {
        const int GroupChannelCount = 1024;

        #region 绑定属性

        /// <summary>
        /// 
        /// </summary>
        public override bool NeedFilter => false;

        public HInSXSettings Setting { get; set; } = HInSXSettings.Instance;
        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private HInSXCollectParamInfo _chartDataParam;
        public override CollectParamInfo ChartDataParam
        {
            get { return _chartDataParam; }
            set { Set(ref _chartDataParam, (HInSXCollectParamInfo)value); }
        }

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private HInSXCollectParamInfo _deviceParam;
        public override CollectParamInfo DeviceParam
        {
            get { return _deviceParam; }
            set
            {
                Set(ref _deviceParam, (HInSXCollectParamInfo)value);
            }
        }

        /// <summary>
        /// 单个headstage上多少个通道
        /// </summary>
        private int _singleHeadstageChannels = 10;
        public int SingleHeadstageChannels
        {
            get { return _singleHeadstageChannels; }
            set { Set(ref _singleHeadstageChannels, value); }
        }

        /// <summary>
        /// headstage，每个具有128通道：与电极对应
        /// </summary>
        private ObservableCollection<HeadstageExtend> _headstages = new ObservableCollection<HeadstageExtend>();
        public ObservableCollection<HeadstageExtend> Headstages
        {
            get { return _headstages; }
            set
            {
                Set(ref _headstages, value);
            }
        }

        /// <summary>
        /// 选择第几个headstage显示
        /// </summary>
        private int _selectHeadstageIndex = 0;
        public int SelectHeadstageIndex
        {
            get { return _selectHeadstageIndex; }
            set
            {
                //切换Headstages时，会导致_selectHeadstageIndex = -1;
                //在InitWaveformUI中对此进行了判断
                //不可在此处理-1问题，否则在切换SelectHeadstageGroup时，会导致此tablecontrol无内容被选中
                if (_selectHeadstageIndex != value)
                {
                    Set(ref _selectHeadstageIndex, value);
                    InitWaveformUI();
                }
            }
        }



        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<string> _headstageGroups = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroups
        {
            get { return _headstageGroups; }
            set { Set(ref _headstageGroups, value); }
        }

        /// <summary>
        /// 每1024通道对应一个board
        /// </summary>
        private int _selectHeadstageGroup = 0;
        public int SelectHeadstageGroup
        {
            get { return _selectHeadstageGroup; }
            set
            {
                value = Math.Min(Math.Max(value, 0), HInSXCONST.CHANNELS_COUNT / GroupChannelCount - 1);
                if (_selectHeadstageGroup != value)
                {
                    Set(ref _selectHeadstageGroup, value);

                    //更新table control中各个headstage的标题名称
                    UpdateTableControlHeadstages();

                    //
                    if (SelectHeadstageIndex == 0)
                    {
                        InitWaveformUI();
                    }
                    else
                    {
                        SelectHeadstageIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSXManager HInSXManager { get; set; } = HInSXManager.Instance;

        #endregion

        private static readonly SpikeScopeMainWndViewModel _instance = new SpikeScopeMainWndViewModel();
        public static SpikeScopeMainWndViewModel Instance => _instance;
        /// <summary>
        /// 
        /// </summary>
        public SpikeScopeMainWndViewModel()
        {
            //
            DeviceId = HInSXManager.Instance.DeviceInfo.DeviceId;
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            ChartDataParam = HInSXManager.Instance.GetCollectionParam();
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            //
            UpdateTableControlHeadstageGroups();
            UpdateTableControlHeadstages();

            //
            SelectHeadstageGroup = 0;
            SelectHeadstageIndex = 0;

            //
            InitWaveformUI();

            //
            EventHelper.Subscribe<RemoteTCPControlSetSampleRateEvent, List<string>>((cmdParams) =>
            {
                if (int.TryParse(cmdParams[2], out int samplerate) && ReadonlyCONST.SampleRates.Contains(samplerate))
                {
                    DeviceParam.SampleRate = samplerate;
                }
            }, Prism.Events.ThreadOption.UIThread);

            //
            EventHelper.Subscribe<RemoteTCPControlRunModeCommandEvent, List<string>>((cmdParams) =>
            {
                if (cmdParams[2].Equals("start"))
                {
                    StartCollect.Execute(null);
                }
                else if (cmdParams[2].Equals("stop"))
                {
                    StopCollect.Execute(null);
                }
            }, Prism.Events.ThreadOption.UIThread);

            EventHelper.Subscribe<ChannelChangedEvent, int>((cmdParams) =>
            {
                this.SelectHeadstageIndex = cmdParams;
            }, Prism.Events.ThreadOption.UIThread);

            HInSXManager.Instance.OnHeadStageInsertedStateChanged += (int headstage, bool inserted) => {
                if (Headstages.Count > headstage)
                {
                    var newHeadstages = new ObservableCollection<HeadstageExtend>();
                    for (int i = 0; i < Headstages.Count; i++)
                    {
                        var item = Headstages[i];
                        if (i == headstage)
                            item.IsInserted = inserted;
                        newHeadstages.Add(item);
                    }
                    this.Headstages = newHeadstages;
                }
            };
            //System.Threading.Tasks.Task.Factory.StartNew(new Action(()=>{
            //    while (true)
            //    {
            //        var newHeadstages = new ObservableCollection<HeadstageExtend>();
            //        for (int i = 0; i < Headstages.Count; i++)
            //        {
            //            var item = Headstages[i];
            //            item.IsInserted = !item.IsInserted;
            //            newHeadstages.Add(item);
            //        }
            //        this.Headstages = newHeadstages;
            //        System.Threading.Thread.Sleep(5000);
            //    }
            //}));
        }

        /// <summary>
        /// 刷新波形列表显示的波形
        /// </summary>
        public void InitWaveformUI()
        {
            if (SelectHeadstageGroup >= 0 && SelectHeadstageGroup < HInSXCONST.CHANNELS_COUNT / GroupChannelCount && SelectHeadstageIndex >= 0 && SelectHeadstageIndex < GroupChannelCount / SingleHeadstageChannels)
            {
                var begin = SelectHeadstageIndex * SingleHeadstageChannels + SelectHeadstageGroup * GroupChannelCount;
                InitWaveformUI(DeviceId, Enumerable.Range(begin, SingleHeadstageChannels).ToList());
            }
        }

        /// <summary>
        /// 更新table control中各个headstage group的标题名称
        /// </summary>
        private void UpdateTableControlHeadstageGroups()
        {
            //大于1024通道时，设置group分组
            if (HInSXCONST.CHANNELS_COUNT > GroupChannelCount)
            {
                var headstageGroups = new ObservableCollection<string>();
                for (int i = 0; i < HInSXCONST.CHANNELS_COUNT / GroupChannelCount; i++)
                {
                    headstageGroups.Add($"{(char)('A' + i)}");
                }
                HeadstageGroups = headstageGroups;
            }
        }

        /// <summary>
        /// 更新table control中各个headstage的标题名称
        /// </summary>
        private void UpdateTableControlHeadstages()
        {
            //大于1024通道时，切换group则切换headstage tab的标题
            if (HInSXCONST.HInSXType != Command.HInSXType.HInS_1024)
            {
                var groupName = $"{(char)('A' + SelectHeadstageGroup)}";

                //
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{groupName}{i + 1}");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{groupName}{i + 1}"
                        });
                    }
                }

                //此动作会触发导致selectindex = -1
                for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                {
                    //Headstages[i] = $"{groupName}{i + 1}";
                    Headstages.Add(new HeadstageExtend { HeadContent = $"{groupName}{i + 1}" });
                }
            }
            else
            {
                //1024通道时，不需要变动headstage类型
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]",
                        });
                    }
                }
            }
        }

        /// <summary>
        /// 开始采集
        /// </summary>
        /// <returns></returns>
        protected override bool StartCollectImpl()
        {
            //ChangeWaveformName();
            if (HInSXManager.Instance.StartCollect(_deviceParam, out string errMsg))
            {
                return true;
            }

            //
            if (!string.IsNullOrWhiteSpace(errMsg))
            {
                Toast.Warn(errMsg);
            }

            //
            return false;
        }

        /// <summary>
        /// 停止采集
        /// </summary>
        /// <returns></returns>
        protected override bool StopCollectImpl()
        {
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            var ret = HInSXManager.Instance.StopCollect();
            if (ret)
            {
                NeuralDataRecorder.StopRecording();
            }
            return ret;
        }

        public ICommand ResetHinX => new DelegateCommand(() =>
        {
            HInSXManager.Instance.ResetMode();
        }
        );

        /// <summary>
        /// 通道映射关系
        /// </summary>
        public ICommand ViewProbeMap => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var imported = false;

                //
                new ElectrodeMapWnd(ProbeMapSettings.Instance.ProbeMap, (newProbeMap) =>
                {
                    ProbeMapSettings.Instance.ProbeMap = newProbeMap;
                    imported = true;
                }).ShowDialog();

                //
                if (imported)
                {
                    App.Reboot = true;
                    Application.Current.Shutdown();
                }
            });
        });

        /// <summary>
        /// 寄存器操作
        /// </summary>
        public ICommand ShowRegister => new DelegateCommand<object>((obj) =>
        {

            UIHelper.BeginInvoke(() =>
            {
                HInSRegisterWnd.PopUp();
            });
        });

        /// <summary>
        /// 查看硬件状态
        /// </summary>
        public ICommand ViewHardwareInfoCommand => new DelegateCommand<object>((obj) =>
        {
            HInSXManager.Instance.QueryRefreshStates();
            UIHelper.BeginInvoke(() =>
            {
                HInSXHardwareWnd.PopUp();
            });

        });

        #region 自动阈值

        /// <summary>
        /// Spkie阈值设置
        /// </summary>
        public ICommand ViewSpikeDetectionThresholdSetting => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                new AutoSpikeThresholdWnd((double multi) =>
                {
                    _multi = multi;

                    //准备采样所有通道
                    var collectParamInfo = HInSXManager.Instance.GetCollectionParam();
                    collectParamInfo.InitChannels(ReadonlyCONST.ProbeMap);
                    collectParamInfo.SelectAll();

                    //启动采样
                    if (!HInSXManager.Instance.StartCollect(_deviceParam, out string errMsg))
                    {
                        if (!string.IsNullOrWhiteSpace(errMsg))
                        {
                            Toast.Warn(errMsg);
                        }
                        else
                        {
                            Toast.Error($"启动采样失败");
                        }
                        return false;
                    }

                    //等待获取到足够长度的数据后，再对rms进行接收：【尿检接中尿】
                    Thread.Sleep(1000 + StairMedSolidSettings.Instance.RMSStatisticMS * 2);
                    RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
                    RMSCenter.Instance.HfpRmsUpdated += Instance_HfpRmsUpdated;
                    for (int i = 0; i < ReadonlyCONST.DeviceChannelCount; i++)
                    {
                        RMSCenter.Instance.RegisterInputChannel(i);
                    }

                    //等待所有通道都处理完
                    Thread.Sleep(5000);

                    //停止监听、停止采集、停止数据处理
                    RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
                    for (int i = 0; i < ReadonlyCONST.DeviceChannelCount; i++)
                    {
                        RMSCenter.Instance.UnregisterInputChannel(i);
                    }
                    Thread.Sleep(2000);
                    HInSXManager.Instance.StopCollect();

                    //
                    Toast.Info("设置成功");

                    //
                    return true;
                }).ShowDialog();
            });
        });

        private double _multi = 1.0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="rms"></param>
        private void Instance_HfpRmsUpdated(int channel, double rms)
        {
            SpikeThresholdCenter.Instance.SetThreshold(channel, Convert.ToInt32(_multi * rms));
        }

        #endregion

        /// <summary>
        /// 阻抗测试
        /// </summary>
        public ICommand ImpedanceTest => new DelegateCommand(() =>
        {
            UIHelper.BeginInvoke(() =>
            {
                new HInSImpedanceWnd(DeviceId).ShowDialog();
            });
        });

        /// <summary>
        /// 导入映射关系
        /// </summary>
        public ICommand ImportProbeMap => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            UIHelper.BeginInvoke(() =>
            {
                var dialog = new Microsoft.Win32.OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //
                try
                {
                    //
                    var xml = FileHelper.LoadFile(dialog.FileName, Encoding.UTF8);
                    if (string.IsNullOrWhiteSpace(xml))
                    {
                        Toast.Warn($"文件为空，无法导入");
                        return;
                    }

                    //
                    using (var reader = new StringReader(xml))
                    {
                        NTProbeMap newProbeMap = null;
                        try
                        {
                            newProbeMap = (NTProbeMap)new XmlSerializer(typeof(NTProbeMap)).Deserialize(reader);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogTool.Logger.Exp($"{TAG}, Deserialize", ex);
                        }

                        //
                        if (newProbeMap == null)
                        {
                            Toast.Warn($"文件有误，无法导入");
                            return;
                        }

                        //
                        if (newProbeMap.IsInValid())
                        {
                            Toast.Warn($"文件无效，导入失败");
                            return;
                        }

                        //
                        ReadonlyCONST.ProbeMap = newProbeMap;

                        //
                        Toast.Info($"导入成功");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogTool.Logger.Exp($"{TAG}, load", ex);
                    Toast.Error($"文件有误，无法导入");
                    return;
                }
            });
        });
    }
}
