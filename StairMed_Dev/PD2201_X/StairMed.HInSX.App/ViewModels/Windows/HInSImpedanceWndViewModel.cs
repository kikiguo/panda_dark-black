﻿using Microsoft.Win32;
using Prism.Commands;
using StairMed.ChipConst;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers;
using StairMed.Enum;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Logger;
using StairMed.MVVM.Base;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    /// <summary>
    /// 
    /// </summary>
    internal class HInSImpedanceWndViewModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(HInSImpedanceWndViewModel);

        /// <summary>
        /// 
        /// </summary>
        private long _deviceId = 1;
        public long DeviceId
        {
            get { return _deviceId; }
            set { Set(ref _deviceId, value); }
        }

        /// <summary>
        /// diagram中绘制阻值上限
        /// </summary>
        private int _impedanceMax = 10 * 1000 * 1000;
        public int ImpedanceMax
        {
            get { return _impedanceMax; }
            set { Set(ref _impedanceMax, value); }
        }

        /// <summary>
        /// diagram中绘制阻值下限
        /// </summary>
        private int _impedanceMin = 20 * 1000;
        public int ImpedanceMin
        {
            get { return _impedanceMin; }
            set { Set(ref _impedanceMin, value); }
        }

        /// <summary>
        /// 通道个数
        /// </summary>
        private int _channelCount = 128;
        public int ChannelCount
        {
            get { return _channelCount; }
            set { Set(ref _channelCount, value); }
        }

        /// <summary>
        /// headstage个数
        /// </summary>
        private int _headstageCount = HInSXCONST.HeadStageCount;
        public int HeadstageCount
        {
            get { return _headstageCount; }
            set { Set(ref _headstageCount, value); }
        }

        /// <summary>
        /// 当前选中的headstage
        /// </summary>
        private int _currentHeadstage = 0;
        public int CurrentHeadstage
        {
            get { return _currentHeadstage; }
            set
            {
                if (Set(ref _currentHeadstage, value))
                {
                    UpdateHeadstageImpedance();
                }
            }
        }


        /// <summary>
        /// 阻抗测试所使用的采样率
        /// </summary>
        private int _sampleRate = 20 * 1000;
        public int SampleRate
        {
            get { return _sampleRate; }
            set { Set(ref _sampleRate, value); }
        }

        /// <summary>
        /// 阻抗测试使用的测试频率
        /// </summary>
        private int _impedanceFreq = 1000;
        public int ImpedanceFreq
        {
            get { return _impedanceFreq; }
            set { Set(ref _impedanceFreq, value); }
        }

        /// <summary>
        /// 当前通道
        /// </summary>
        private int _currentChannel = 0;
        public int CurrentChannel
        {
            get { return _currentChannel; }
            set
            {
                if (Set(ref _currentChannel, value))
                {
                    UpdateChannelImpedance();
                }
            }
        }

        /// <summary>
        /// 当前选中通道的阻抗幅值
        /// </summary>
        private double _currentImpedance = 0;
        public double CurrentImpedance
        {
            get { return _currentImpedance; }
            set { Set(ref _currentImpedance, value); }
        }

        /// <summary>
        /// 当前选中通道的阻抗相位
        /// </summary>
        private double _currentPhase = 0;
        public double CurrentPhase
        {
            get { return _currentPhase; }
            set { Set(ref _currentPhase, value); }
        }

        /// <summary>
        /// 电阻阈值
        /// </summary>
        private double _threshold = 200 * 1000;
        public double Threshold
        {
            get { return _threshold; }
            set { Set(ref _threshold, value); }
        }

        /// <summary>
        /// 阻抗幅值
        /// </summary>
        private ObservableCollection<double> _impedances = new ObservableCollection<double>();
        public ObservableCollection<double> Impedances
        {
            get { return _impedances; }
            set { Set(ref _impedances, value); }
        }

        /// <summary>
        /// 阻抗相位
        /// </summary>
        private ObservableCollection<double> _phases = new ObservableCollection<double>();
        public ObservableCollection<double> Phases
        {
            get { return _phases; }
            set { Set(ref _phases, value); }
        }

        /// <summary>
        /// 所有headstage的阻抗幅值
        /// </summary>
        private readonly Dictionary<int, List<double>> _headstageImpedanceMagnitudeDict = new Dictionary<int, List<double>>();

        /// <summary>
        /// 所有headstage的阻抗相位
        /// </summary>
        private readonly Dictionary<int, List<double>> _headstageImpedancePhaseDict = new Dictionary<int, List<double>>();

        /// <summary>
        /// 伪造的幅值
        /// </summary>
        private readonly List<double> _demoMagnitudes = new List<double> { 2.57E+04, 2.59E+04, 2.58E+04, 2.52E+04, 2.50E+04, 4.12E+06, 2.53E+04, 2.51E+04, 2.46E+04, 2.38E+04, 4.20E+06, 2.41E+04, 2.39E+04, 2.40E+04, 2.39E+04, 2.40E+04, 3.47E+06, 3.36E+06, 2.27E+04, 2.28E+04, 2.26E+04, 2.27E+04, 2.28E+04, 2.28E+04, 2.25E+04, 2.21E+04, 3.95E+05, 2.42E+04, 2.21E+04, 2.21E+04, 2.17E+04, 2.28E+04, 2.19E+04, 2.16E+04, 2.18E+04, 2.14E+04, 3.67E+05, 2.66E+04, 2.20E+04, 2.25E+04, 2.27E+04, 2.25E+04, 2.24E+04, 2.26E+04, 2.21E+04, 2.25E+04, 3.65E+06, 2.41E+04, 2.36E+04, 2.40E+04, 2.44E+04, 2.44E+04, 2.43E+04, 2.42E+04, 2.41E+04, 2.52E+04, 4.05E+06, 8.13E+05, 2.58E+04, 3.40E+06, 2.94E+04, 2.59E+04, 2.62E+04, 4.30E+06, 2.05E+04, 1.95E+04, 1.97E+04, 1.96E+04, 2.02E+04, 2.07E+04, 2.06E+04, 2.04E+04, 2.09E+04, 2.04E+04, 2.14E+04, 2.12E+04, 2.11E+04, 2.13E+04, 4.43E+06, 2.23E+04, 2.30E+04, 2.20E+04, 2.21E+04, 2.27E+04, 2.26E+04, 2.29E+04, 2.33E+04, 2.33E+04, 2.35E+04, 2.36E+04, 5.00E+06, 2.33E+04, 2.34E+04, 2.39E+04, 2.47E+04, 2.48E+04, 2.60E+04, 2.41E+04, 2.38E+04, 5.11E+06, 2.27E+04, 2.31E+04, 2.34E+04, 2.27E+04, 2.33E+04, 2.24E+04, 2.25E+04, 2.24E+04, 2.22E+04, 2.21E+04, 2.38E+04, 2.26E+04, 2.30E+04, 2.23E+04, 2.17E+04, 2.14E+04, 2.16E+04, 2.16E+04, 2.12E+04, 3.96E+06, 2.15E+04, 2.12E+04, 2.12E+04, 2.10E+04, 2.04E+04, 3.92E+06, 1.99E+04, 1.98E+04, };

        /// <summary>
        /// 伪造的相位
        /// </summary>
        private readonly List<double> _demoPhases = new List<double> { -16, -15, -15, -14, -14, -79, -14, -13, -14, -13, -77, -12, -11, -10, -10, -9, -73, -71, -8, -7, -6, -6, -5, -5, -4, -4, -50, -9, -2, -3, -1, -3, -18, -17, -17, -17, -62, -27, -16, -15, -14, -14, -13, -13, -12, -12, -74, -10, -8, -8, -8, -7, -6, -6, -5, -5, -67, 1, -2, -70, 1, 0, -1, -61, -20, -19, -19, -18, -18, -18, -16, -16, -18, -17, -16, -16, -14, -13, -72, -14, -15, -12, -11, -10, -8, -8, -7, -5, -5, -4, -62, -4, -3, -3, -2, -1, -18, -17, -17, -78, -17, -17, -18, -15, -15, -14, -14, -14, -14, -14, -17, -13, -12, -12, -10, -9, -9, -9, -8, -67, -6, -4, -4, -5, -3, -63, -3, -3, };

        /// <summary>
        /// 
        /// </summary>
        public HInSImpedanceWndViewModel()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [windowviewmodel]");

            //
            for (int headstage = 0; headstage < HeadstageCount; headstage++)
            {
                var magnitudes = new List<double>();
                var phases = new List<double>();
                for (int channel = 0; channel < ChannelCount; channel++)
                {
                    ImpedanceCenter.Instance.GetImpedance(HInSXManager.Instance.DeviceInfo.DeviceId, channel + ChannelCount * headstage, out double magnitude, out double phase);
                    if (magnitude <= 0)
                    {
                        magnitudes.Add(ImpedanceMax * 10);
                        phases.Add(0);
                    }
                    else
                    {
                        magnitudes.Add(magnitude);
                        phases.Add(phase);
                    }
                }

                _headstageImpedanceMagnitudeDict[headstage] = magnitudes;
                _headstageImpedancePhaseDict[headstage] = phases;
            }

            //
            UpdateHeadstageImpedance();
        }


        /// <summary>
        /// 按headstage更新
        /// </summary>
        private void UpdateHeadstageImpedance()
        {
            if (_headstageImpedanceMagnitudeDict.ContainsKey(CurrentHeadstage))
            {
                Impedances.Clear();
                Phases.Clear();

                //
                var headstageImpedances = _headstageImpedanceMagnitudeDict[CurrentHeadstage];
                var headstagePhases = _headstageImpedancePhaseDict[CurrentHeadstage];

                //
                for (int i = 0; i < ChannelCount; i++)
                {
                    Impedances.Add(headstageImpedances[i]);
                    Phases.Add(headstagePhases[i]);
                }
            }
            else
            {
                for (int i = 0; i < ChannelCount; i++)
                {
                    Impedances.Add(ImpedanceMax * 10);
                    Phases.Add(0);
                }
            }

            //
            CurrentChannel = 0;

            //
            UpdateChannelImpedance();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateChannelImpedance()
        {
            if (CurrentChannel >= 0 && CurrentChannel < Impedances.Count && CurrentChannel < Phases.Count)
            {
                CurrentImpedance = Impedances[CurrentChannel];
                CurrentPhase = Phases[CurrentChannel];
            }
        }



        /// <summary>
        /// 读取阻抗
        /// </summary>
        public ICommand ReadImpedance => new DelegateCommand<string>((obj) =>
        {
            DeviceId = Convert.ToInt32(obj);

            //
            //if (!HInSXManager.Instance.IsHeadstageInserted(CurrentHeadstage))
            //{
            //    Toast.Warn($"Headstage未插入，无法进行阻抗测试");
            //    return;
            //}

            //
            Loading.Show(() =>
            {
                var applyNotch = StairMedSettings.Instance.NotchFilterType != NotchFilterType.None;
                var notchFreq = StairMedSettings.Instance.NotchFilterType == NotchFilterType._50Hz ? 50 : StairMedSettings.Instance.NotchFilterType == NotchFilterType._60Hz ? 60 : 0;

                //按需要顺序执行
                var inputChannels = new List<int>();
                for (int i = 0; i < ChannelCount; i++)
                {
                    inputChannels.Add(ReadonlyCONST.ConvertToInputChannel(i + ChannelCount * CurrentHeadstage));
                }

                //
                HInSXManager.Instance.ImpedanceTest(DeviceId, inputChannels, applyNotch, notchFreq, 10, ImpedanceFreq, SampleRate, (indexInInputChannel, magnitude, phase) =>
                {
                    //if (CurrentHeadstage == 2)
                    //{
                    //    magnitude = physicsChannel < _demoMagnitudes.Count ? _demoMagnitudes[physicsChannel] : _demoMagnitudes[^1];
                    //    phase = physicsChannel < _demoPhases.Count ? _demoPhases[physicsChannel] : _demoPhases[^1];
                    //}
                    //else
                    //{
                    //    magnitude = 12 * 1000 * 1000;
                    //    phase = 100;
                    //}

                    //
                    _impedances[indexInInputChannel] = magnitude;
                    _phases[indexInInputChannel] = phase;
                    CurrentChannel = indexInInputChannel;
                    CurrentImpedance = magnitude;

                    //
                    _headstageImpedanceMagnitudeDict[CurrentHeadstage][indexInInputChannel] = magnitude;
                    _headstageImpedancePhaseDict[CurrentHeadstage][indexInInputChannel] = phase;

                    //
                    ImpedanceCenter.Instance.SetImpedance(HInSXManager.Instance.DeviceInfo.DeviceId, indexInInputChannel + ChannelCount * CurrentHeadstage, magnitude, phase);
                });
            });
        });

        /// <summary>
        /// 保存至CSV
        /// </summary>
        public ICommand SaveToCSV => new DelegateCommand(() =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new SaveFileDialog
                {
                    Filter = "csv(*.csv)|*.csv",
                    FileName = $"Impedance-{StairMedSolidSettings.Instance.TestType}_{DateTime.Now:yyyyMMdd_HHmmss}",
                    OverwritePrompt = true,
                };
                if (dialog.ShowDialog() == true)
                {
                    var path = $"{dialog.FileName}";

                    //
                    var channelNames = new List<string>(ChannelCount);
                    for (int headstage = 0; headstage < HeadstageCount; headstage++)
                    {
                        if (headstage != CurrentHeadstage)
                        {
                            continue;
                        }
                        for (int channel = 0; channel < ChannelCount; channel++)
                        {
                            channelNames.Add($"{NeuralDataCollector.Instance.GetCollectingDataParam().GetNativeName(channel + headstage * ChannelCount)}");
                        }
                    }

                    //
                    var impedanceMagnitudes = new List<double>(ChannelCount);
                    var impedancePhases = new List<double>( ChannelCount);
                    for (int headstage = 0; headstage < HeadstageCount; headstage++)
                    {
                        if (headstage != CurrentHeadstage)
                        {
                            continue;
                        }
                        var headstageImpedanceMagnitudes = _headstageImpedanceMagnitudeDict[headstage];
                        var headstageImpedancePhases = _headstageImpedancePhaseDict[headstage];
                        for (int channel = 0; channel < ChannelCount; channel++)
                        {
                            impedanceMagnitudes.Add(headstageImpedanceMagnitudes[channel]);
                            impedancePhases.Add(headstageImpedancePhases[channel]);
                        }
                    }

                    //
                    FileHelper.SaveToFileWithBom(path, IntanImpedenceTest.ForamtToCSV(ImpedanceFreq, channelNames, impedanceMagnitudes, impedancePhases));

                    //
                    Toast.Info($"保存成功:{path}");
                }
            });
        });

        /// <summary>
        /// 保存至Png
        /// </summary>
        public ICommand SaveToImg => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var dialog = new SaveFileDialog
                {
                    Filter = "png(*.png)|*.png",
                    FileName = $"Impedance-{StairMedSolidSettings.Instance.TestType}_Headstage{CurrentHeadstage + 1}_{DateTime.Now:yyyyMMdd_HHmmss}",
                    OverwritePrompt = true,
                };
                if (dialog.ShowDialog() == true)
                {
                    var path = $"{dialog.FileName}";
                    FileHelper.SnapshotUIControl((FrameworkElement)obj, new PngBitmapEncoder(), path);
                    Toast.Info($"保存成功:{path}");
                }
            });
        });

        /// <summary>
        /// 复制图片
        /// </summary>
        public ICommand CopyAsImg => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var img = ToolMix.SnapshotUIControl((FrameworkElement)obj);
                if (img != null)
                {
                    Clipboard.SetImage(img);
                    Toast.Info("已复制");
                }
                else
                {
                    Toast.Warn("复制失败");
                }
            });
        });
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        public ICommand SaveCommand => new DelegateCommand<object>((_) =>
        {
            if (Settings.ImpedanceToCSV)
            {
                SaveToCSV.Execute(null);
            }
            if (Settings.ImpedanceToPNG)
            {
                SaveToImg.Execute(_);
            }
            if (Settings.ImpedanceToJPG)
            {
                CopyAsImg.Execute(_);
            }
        });
    }
}
