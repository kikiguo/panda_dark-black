﻿using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    public class HInSMainstayWndViewModel:ViewModelBase,IDisposable
    {
        private string _nowDate;

        public string NowDate
        {
            get { return _nowDate; }
            set { Set(ref _nowDate, value); }
        }

        private string _nowTime;

        public string NowTime
        {
            get { return _nowTime; }
            set { Set(ref _nowTime, value); }
        }

        Timer timer = new Timer(500);
        public HInSMainstayWndViewModel()
        {
            timer.Elapsed += (sender, e) =>
            {
                this.NowDate = DateTime.Today.ToString("yyyy/MM/dd");
                this.NowTime = DateTime.Now.ToString("HH:mm:ss");
            };
            timer.Start();
        }

        public void Dispose()
        {
            timer.Stop();
            timer.Dispose();
        }
    }
}
