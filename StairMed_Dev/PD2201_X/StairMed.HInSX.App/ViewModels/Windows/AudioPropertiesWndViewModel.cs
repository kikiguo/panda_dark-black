﻿using HDF5CSharp;
using MatFileHandler;
using Prism.Commands;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers.ISI;
using StairMed.DataCenter;
using StairMed.HInSX.App.Core;
using StairMed.MVVM.Base;
using StairMed.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows;
using StairMed.UI.ViewModels.Windows.Analyses;
using StairMed.DataCenter.Centers.Audio;
using Accord.Statistics.Kernels;
using Microsoft.VisualBasic;
using NAudio.Wave;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    internal class AudioPropertiesWndViewModel : AnalyseWndVMBase
    {
        public StairMedSettings Setting { get; set; } = StairMedSettings.Instance;



        /// <summary>
        /// 
        /// </summary>
        private readonly object _lock = new object();

        /// <summary>
        /// 实际通道号
        /// </summary>
        private int _inputChannel = 0;
        public int InputChannel
        {
            get { return _inputChannel; }
            set { Set(ref _inputChannel, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<float> _histograms = new List<float>();
        public List<float> Histograms
        {
            get { return _histograms; }
            set { Set(ref _histograms, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _mean = 0;
        public double Mean
        {
            get { return _mean; }
            set { Set(ref _mean, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _isiFreq = 0;
        public double ISIFreq
        {
            get { return _isiFreq; }
            set { Set(ref _isiFreq, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private double _stdDev = 0;
        public double StdDev
        {
            get { return _stdDev; }
            set { Set(ref _stdDev, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private long _isiCount = 0;
        public long ISICount
        {
            get { return _isiCount; }
            set { Set(ref _isiCount, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _binSizeMS = 10;
        public int BinSizeMS
        {
            get { return _binSizeMS; }
            set { Set(ref _binSizeMS, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private int _timeSpan = 1000;
        public int TimeSpan
        {
            get { return _timeSpan; }
            set { Set(ref _timeSpan, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _saveing = false;

        /// <summary>
        /// 
        /// </summary>
        private readonly HashSet<string> ISIChannelSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.ISIChannel),
        };

        /// <summary>
        /// 
        /// </summary>
        public AudioPropertiesWndViewModel()
        {
            //
            CareSettings.UnionWith(ISIChannelSettings);

            //
            AudioCenter.Instance.ISIUpdated -= Instance_ISIUpdated;
            AudioCenter.Instance.ISIUpdated += Instance_ISIUpdated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isiInfo"></param>
        private void Instance_ISIUpdated(int channel, ISIInfo isiInfo)
        {
            if (_saveing || channel != InputChannel)
            {
                return;
            }

            //
            if (channel == InputChannel)
            {
                lock (_lock)
                {
                    try
                    {
                        //
                     //   play(isiInfo.Histograms);
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="changes"></param>
        protected override void ReloadSettings(HashSet<string> changes)
        {
            base.ReloadSettings(changes);

            //
            if (changes == null || ISIChannelSettings.Overlaps(changes))
            {
                AudioCenter.Instance.UnregisterISIChannel(InputChannel);
                InputChannel = ReadonlyCONST.ConvertToInputChannel(Settings.ISIChannel);
                AudioCenter.Instance.RegisterISIChannel(InputChannel);

                //
                lock (_lock)
                {
                    ISICount = 0;
                    Mean = 0.0;
                    StdDev = 0.0;
                    ISIFreq = 0.0;
                    Histograms = new List<float>();
                    BinSizeMS = StairMedSettings.Instance.ISIBinSizeMS;
                    TimeSpan = StairMedSettings.Instance.ISITimeScaleMS;
                }
            }
        }

        /// <summary>
        /// 窗口关闭时，释放所有监听
        /// </summary>
        protected override void OnViewClose()
        {
            base.OnViewClose();

            //
            AudioCenter.Instance.ISIUpdated -= Instance_ISIUpdated;
            AudioCenter.Instance.UnregisterISIChannel(InputChannel);
        }



     
        public void play(List<float> chunk)
        {
     
            NAudio.Wave.WaveOut waveOut = new NAudio.Wave.WaveOut(WaveCallbackInfo.FunctionCallback());
            if (chunk == null) { return; }
            AudioWaveOut audioWaveOut = null;
            //从最新的数据开始加载
            for (int sampleIndex = 0; sampleIndex <chunk.Count; sampleIndex++)
            {
                if (chunk[sampleIndex] > 180)
                {
                    try
                    {
                        audioWaveOut = new AudioWaveOut(chunk[sampleIndex], chunk[sampleIndex] / 100,0);
                        waveOut.Stop();
                        waveOut.Dispose();
                        waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
                        waveOut.Init(audioWaveOut);
                        waveOut.Play();
                        break;
                    }
                    catch (Exception e)
                    {
                        waveOut.Stop();
                        waveOut.Dispose();
                        break;
                    }

                }
                if (chunk[sampleIndex] < -280)
                {
                    try
                    {
                        audioWaveOut = new AudioWaveOut(chunk[sampleIndex], chunk[sampleIndex] / 100, 0);
                        waveOut.Stop();
                        waveOut.Dispose();
                        waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
                        waveOut.Init(audioWaveOut);
                        waveOut.Play();
                        break;
                    }
                    catch (Exception e)
                    {
                        waveOut.Stop();
                        waveOut.Dispose();
                        break;
                    }

                }



            }



        }

      
    }



}
