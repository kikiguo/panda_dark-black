﻿using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    public class HInSAboutPageViewModel: ViewModelBase
    {
        private HInSXDeviceInfo _deviceInfo;
        public virtual HInSXDeviceInfo DeviceInfo
        {
            get { return _deviceInfo; }
            set { Set(ref _deviceInfo, value); }
        }

        public HInSAboutPageViewModel()
        {
            DeviceInfo = HInSXManager.Instance.DeviceInfo;
            HInSXManager.Instance.QueryVersions();
            
        }
    }
}
