﻿using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.DataCenter.Recorder;
using StairMed.Entity.Infos.Bases;
using StairMed.Event;
using StairMed.Event.RemoteTCPControl;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.HInSX.App.Views.Windows;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.Probe;
using StairMed.Tools;
using StairMed.UI.ViewModels.UserControls.Waveforms;
using StairMed.UI.Views.Windows.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    internal class RasterPlotViewModel : ViewModelBase
    {

#if true
        const int GroupChannelCount = 1024;

        #region 绑定属性

        /// <summary>
        /// 
        /// </summary>
        public bool NeedFilter => false;

        public HInSXSettings Setting { get; set; } = HInSXSettings.Instance;
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;
        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private HInSXCollectParamInfo _chartDataParam;
        public CollectParamInfo ChartDataParam
        {
            get { return _chartDataParam; }
            set { Set(ref _chartDataParam, (HInSXCollectParamInfo)value); }
        }

        /// <summary>
        /// 数据显示列数
        /// </summary>
        private List<int> _displayColumns;
        public List<int> DisplayColumns
        {
            get { return _displayColumns; }
            set { Set(ref _displayColumns, value); }
        }


        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private HInSXCollectParamInfo _deviceParam;
        public CollectParamInfo DeviceParam
        {
            get { return _deviceParam; }
            set
            {
                Set(ref _deviceParam, (HInSXCollectParamInfo)value);
            }
        }

        /// <summary>
        /// 单个headstage上多少个通道
        /// </summary>
        private int _singleHeadstageChannels = 10;
        public int SingleHeadstageChannels
        {
            get { return _singleHeadstageChannels; }
            set { Set(ref _singleHeadstageChannels, value); }
        }

        /// <summary>
        /// headstage，每个具有128通道：与电极对应
        /// </summary>
        private ObservableCollection<HeadstageExtend> _headstages = new ObservableCollection<HeadstageExtend>();
        public ObservableCollection<HeadstageExtend> Headstages
        {
            get { return _headstages; }
            set
            {
                Set(ref _headstages, value);
            }
        }
        private bool headstageIndexChangedFromSubscribe = false;
        /// <summary>
        /// 选择第几个headstage显示
        /// </summary>
        private int _selectHeadstageIndex = 0;
        public int SelectHeadstageIndex
        {
            get { return _selectHeadstageIndex; }
            set
            {
                //切换Headstages时，会导致_selectHeadstageIndex = -1;
                //在InitWaveformUI中对此进行了判断
                //不可在此处理-1问题，否则在切换SelectHeadstageGroup时，会导致此tablecontrol无内容被选中
                if (_selectHeadstageIndex != value)
                {
                    Set(ref _selectHeadstageIndex, value);
                    InitWaveformUI();
                    if (!headstageIndexChangedFromSubscribe)
                        EventHelper.Publish<ChannelChangedEventFromSub, int>(_selectHeadstageIndex);
                }
                headstageIndexChangedFromSubscribe = false;
            }
        }

        private ObservableCollection<string> _headstageGroupsSerial = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroupsSerial
        {
            get { return _headstageGroupsSerial; }
            set { Set(ref _headstageGroupsSerial, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<string> _headstageGroups = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroups
        {
            get { return _headstageGroups; }
            set { Set(ref _headstageGroups, value); }
        }

        /// <summary>
        /// 每1024通道对应一个board
        /// </summary>
        private int _selectHeadstageGroup = 0;
        public int SelectHeadstageGroup
        {
            get { return _selectHeadstageGroup; }
            set
            {
                value = Math.Min(Math.Max(value, 0), HInSXCONST.CHANNELS_COUNT / GroupChannelCount - 1);
                if (_selectHeadstageGroup != value)
                {
                    Set(ref _selectHeadstageGroup, value);

                    //更新table control中各个headstage的标题名称
                    UpdateTableControlHeadstages();

                    //
                    if (SelectHeadstageIndex == 0)
                    {
                        InitWaveformUI();
                    }
                    else
                    {
                        SelectHeadstageIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSXManager HInSXManager { get; set; } = HInSXManager.Instance;

        #endregion

        //private static readonly SpikeScopeMainWndViewModel _instance = new SpikeScopeMainWndViewModel();
        //public static SpikeScopeMainWndViewModel Instance => _instance;

        private long _deviceId;

        public long DeviceId
        {
            get { return _deviceId; }
            set { _deviceId = value; }
        }
        /// <summary>
        /// 调整配置时，动态刷新波形
        /// </summary>
        private readonly HashSet<string> CareSettings = new HashSet<string>
        {
            nameof(StairMedSettings.Instance.WaveformDisplayColumn),
        };


        /// <summary>
        /// 
        /// </summary>
        public RasterPlotViewModel()
        {
            //
            DeviceId = HInSXManager.Instance.DeviceInfo.DeviceId;
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            ChartDataParam = HInSXManager.Instance.GetCollectionParam();
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

            //
            //NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            //
            UpdateTableControlHeadstageGroups();
            UpdateTableControlHeadstages();

            //
            SelectHeadstageGroup = 0;
            SelectHeadstageIndex = 0;

            //
            InitWaveformUI();

            DisplayColumns = Enumerable.Range(0, Settings.WaveformDisplayColumn).ToList();
            Settings.OnSettingChanged += (changes) =>
            {
                if (CareSettings.Overlaps(changes))
                {

                    DisplayColumns = Enumerable.Range(0, Settings.WaveformDisplayColumn).ToList();

                    /* T02.DataContext = trigs;
                     T03.DataContext = trigs;*/
                }


            };

            //
            EventHelper.Subscribe<RemoteTCPControlSetSampleRateEvent, List<string>>((cmdParams) =>
            {
                if (int.TryParse(cmdParams[2], out int samplerate) && ReadonlyCONST.SampleRates.Contains(samplerate))
                {
                    DeviceParam.SampleRate = samplerate;
                }
            }, Prism.Events.ThreadOption.UIThread);

            //


            EventHelper.Subscribe<ChannelChangedEvent, int>((cmdParams) =>
            {
                headstageIndexChangedFromSubscribe = true;
                this.SelectHeadstageIndex = cmdParams;
            }, Prism.Events.ThreadOption.UIThread);

            HInSXManager.Instance.OnHeadStageInsertedStateChanged += (int headstage, bool inserted) => {
                if (Headstages.Count > headstage)
                {
                    var newHeadstages = new ObservableCollection<HeadstageExtend>();
                    for (int i = 0; i < Headstages.Count; i++)
                    {
                        var item = Headstages[i];
                        if (i == headstage)
                            item.IsInserted = inserted;
                        newHeadstages.Add(item);
                    }
                    this.Headstages = newHeadstages;
                }
            };
            //System.Threading.Tasks.Task.Factory.StartNew(new Action(()=>{
            //    while (true)
            //    {
            //        var newHeadstages = new ObservableCollection<HeadstageExtend>();
            //        for (int i = 0; i < Headstages.Count; i++)
            //        {
            //            var item = Headstages[i];
            //            item.IsInserted = !item.IsInserted;
            //            newHeadstages.Add(item);
            //        }
            //        this.Headstages = newHeadstages;
            //        System.Threading.Thread.Sleep(5000);
            //    }
            //}));
        }

        /// <summary>
        /// 刷新波形列表显示的波形
        /// </summary>
        public void InitWaveformUI()
        {
            if (SelectHeadstageGroup >= 0 && SelectHeadstageGroup < HInSXCONST.CHANNELS_COUNT / GroupChannelCount && SelectHeadstageIndex >= 0 && SelectHeadstageIndex < GroupChannelCount / SingleHeadstageChannels)
            {
                var begin = SelectHeadstageIndex * SingleHeadstageChannels + SelectHeadstageGroup * GroupChannelCount;
                InitWaveformUI(DeviceId, Enumerable.Range(begin, SingleHeadstageChannels).ToList());
            }
        }
        private WaveformViewModel _waveformViewModel = new WaveformViewModel();

        public WaveformViewModel WaveformViewModel
        {
            get { return _waveformViewModel; }
            set { _waveformViewModel = value; }
        }


        private WaveformTRIGModel _waveformTRIGModel = new WaveformTRIGModel();

        public WaveformTRIGModel WaveformTRIGModel
        {
            get { return _waveformTRIGModel; }
            set { _waveformTRIGModel = value; }
        }


        protected void InitWaveformUI(long deviceId, List<int> physicsChannels)
        {

            UIHelper.BeginInvoke(() =>
            {
                Task.Run(() =>
                {
                    _waveformViewModel.InitUIDataAsync(deviceId, physicsChannels);
                });
            });
        }

        /// <summary>
        /// 更新table control中各个headstage group的标题名称
        /// </summary>
        private void UpdateTableControlHeadstageGroups()
        {
            //大于1024通道时，设置group分组
            if (HInSXCONST.CHANNELS_COUNT > GroupChannelCount)
            {
                var headstageGroups = new ObservableCollection<string>();
                for (int i = 0; i < HInSXCONST.CHANNELS_COUNT / GroupChannelCount; i++)
                {
                    headstageGroups.Add($"{(char)('A' + i)}");
                }
                HeadstageGroups = headstageGroups;
            }
        }

        /// <summary>
        /// 更新table control中各个headstage的标题名称
        /// </summary>
        private void UpdateTableControlHeadstages()
        {
            //大于1024通道时，切换group则切换headstage tab的标题
            if (HInSXCONST.HInSXType != Command.HInSXType.HInS_1024)
            {
                var groupName = $"{(char)('A' + SelectHeadstageGroup)}";

                //
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{groupName}{i + 1}");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{groupName}{i + 1}"
                        });
                    }
                }

                //此动作会触发导致selectindex = -1
                for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                {
                    //Headstages[i] = $"{groupName}{i + 1}";
                    Headstages.Add(new HeadstageExtend { HeadContent = $"{groupName}{i + 1}" });
                }
            }
            else
            {
                //1024通道时，不需要变动headstage类型
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]",
                        });
                        HeadstageGroupsSerial.Add($"{(i + 1).ToString()}");
                    }
                }
            }
        }

        public ICommand PanleChangedCommand => new DelegateCommand<object>((obj) =>
        {
            try
            {
                int layout = 0;
                if (Int32.TryParse(obj?.ToString(), out layout))
                {
                    EventHelper.Publish<LayoutChangedEvent, int>(layout);
                }

            }
            catch (Exception)
            {

            }

        });
#endif
    }
}
