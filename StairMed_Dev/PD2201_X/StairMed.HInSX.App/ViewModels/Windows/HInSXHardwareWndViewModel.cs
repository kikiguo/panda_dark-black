﻿using StairMed.HInSX.App.Core;
using StairMed.MVVM.Base;
using System.Collections.Generic;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    /// <summary>
    /// 
    /// </summary>
    internal class HInSXHardwareWndViewModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(HInSXHardwareWndViewModel);

        /// <summary>
        /// 
        /// </summary>
        public HInSXManager HInSXManager { get; set; } = HInSXManager.Instance;

        /// <summary>
        /// 
        /// </summary>
        private List<int> _rows = new List<int>();
        public List<int> Rows
        {
            get { return _rows; }
            set { Set(ref _rows, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSXHardwareWndViewModel()
        {
            switch (HInSXCONST.HInSXType)
            {
                case Command.HInSXType.H_Unknown:
                    break;
                case Command.HInSXType.HInS_1024:
                    {
                        Rows = new List<int> { 0 };
                    }
                    break;
                case Command.HInSXType.HInS_2048:
                    {
                        Rows = new List<int> { 0, 1 };
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
