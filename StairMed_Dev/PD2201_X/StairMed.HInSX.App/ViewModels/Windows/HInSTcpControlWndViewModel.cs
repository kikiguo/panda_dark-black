﻿using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.Enum;
using StairMed.Event.RemoteTCPControl;
using StairMed.HInSX.App.Logger;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.TCPControl;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    /// <summary>
    /// 
    /// </summary>
    internal class HInSTcpControlWndViewModel : ViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(HInSTcpControlWndViewModel);

        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public CmdManager CommandManager { get; set; } = CmdManager.Instance;

        /// <summary>
        /// 
        /// </summary>
        public WaveformManager WaveformManager { get; set; } = WaveformManager.Instance;

        /// <summary>
        /// 
        /// </summary>
        public SpikeManager SpikeManager { get; set; } = SpikeManager.Instance;

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<PresentChannel> _presentChannels = new ObservableCollection<PresentChannel>();
        public ObservableCollection<PresentChannel> PresentChannels
        {
            get { return _presentChannels; }
            set { Set(ref _presentChannels, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<OutputChannel> _outputChannels = new ObservableCollection<OutputChannel>();
        public ObservableCollection<OutputChannel> OutputChannels
        {
            get { return _outputChannels; }
            set { Set(ref _outputChannels, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private NeuralDataType _dataType = NeuralDataType.WFP;
        public NeuralDataType DataType
        {
            get { return _dataType; }
            set { Set(ref _dataType, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private string _format = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public HInSTcpControlWndViewModel()
        {
            LogTool.Logger.LogT($"new {this.GetType().Name} init [windowviewmodel]");

            //
            var digit = ReadonlyCONST.DeviceChannelCount > 9999 ? 5 : (ReadonlyCONST.DeviceChannelCount > 999 ? 4 : (ReadonlyCONST.DeviceChannelCount > 99 ? 3 : (ReadonlyCONST.DeviceChannelCount > 9 ? 2 : 1)));
            _format = $"D{digit}";
            for (int i = 0; i < 128; i++)
            {
                PresentChannels.Add(new PresentChannel
                {
                    Channel = i,
                    Title = (i + 1).ToString(_format),
                });
            }

            //
            EventHelper.Subscribe<RemoteTCPControlSelectCommandEvent, List<string>>((cmdParams) =>
            {
                var dataType = ParseNeuralDataType(cmdParams[1]);

                //
                if (int.TryParse(cmdParams[2], out int channel))
                {
                    SelectChannel(dataType, channel - 1);
                }
                else if (cmdParams[2].Equals("all"))
                {
                    foreach (var item in PresentChannels)
                    {
                        SelectChannel(dataType, item.Channel);
                    }
                }
            }, Prism.Events.ThreadOption.UIThread);

            //
            EventHelper.Subscribe<RemoteTCPControlUnselectCommandEvent, List<string>>((cmdParams) =>
            {
                var dataType = ParseNeuralDataType(cmdParams[1]);

                //
                if (int.TryParse(cmdParams[2], out int channel))
                {
                    UnselectChannel(dataType, channel - 1);
                }
                else if (cmdParams[2].Equals("all"))
                {
                    foreach (var item in PresentChannels)
                    {
                        UnselectChannel(dataType, item.Channel);
                    }
                }
            }, Prism.Events.ThreadOption.UIThread);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdParam"></param>
        /// <returns></returns>
        private static NeuralDataType ParseNeuralDataType(string cmdParam)
        {
            var dataType = NeuralDataType.WFP;
            switch (cmdParam)
            {
                case "spike":
                    {
                        dataType = NeuralDataType.Spike;
                    }
                    break;
                case "wfp":
                    {
                        dataType = NeuralDataType.WFP;
                    }
                    break;
                case "lfp":
                    {
                        dataType = NeuralDataType.LFP;
                    }
                    break;
                case "hfp":
                    {
                        dataType = NeuralDataType.HFP;
                    }
                    break;
                default:
                    break;
            }

            return dataType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="channel"></param>
        private void SelectChannel(NeuralDataType dataType, int channel)
        {
            if (OutputChannels.FirstOrDefault(r => r.Channel == channel && r.DataType == dataType) != null)
            {
                return;
            }

            //
            var index = OutputChannels.Count(r => r.Channel < channel || (r.Channel == channel && r.DataType < dataType));
            OutputChannels.Insert(index, new OutputChannel
            {
                Channel = channel,
                DataType = dataType,
                Title = $"{(channel + 1).ToString(_format)} | {dataType}"
            });
            if (dataType == NeuralDataType.Spike)
            {
                SpikeManager.AddChannel(channel);
            }
            else
            {
                WaveformManager.AddChannel(channel, dataType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataType"></param>
        /// <param name="channel"></param>
        private void UnselectChannel(NeuralDataType dataType, int channel)
        {
            var outputChannel = OutputChannels.FirstOrDefault(r => r.Channel == channel && r.DataType == dataType);
            if (outputChannel == null)
            {
                return;
            }

            //
            OutputChannels.Remove(outputChannel);
            if (dataType == NeuralDataType.Spike)
            {
                SpikeManager.RemoveChannel(channel);
            }
            else
            {
                WaveformManager.RemoveChannel(outputChannel.Channel, outputChannel.DataType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ICommand StartListening => new DelegateCommand(() =>
        {
            if (CommandManager.Listening && WaveformManager.Listening && SpikeManager.Listening)
            {
                CommandManager.StopListen();
                WaveformManager.StopListen();
                SpikeManager.StopListen();
            }
            else
            {
                if (!CommandManager.Listening)
                {
                    CommandManager.StartListen();
                }
                if (!WaveformManager.Listening)
                {
                    WaveformManager.StartListen();
                }
                if (!SpikeManager.Listening)
                {
                    SpikeManager.StartListen();
                }
            }
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand SwitchNeuralDataType => new DelegateCommand<object>((obj) =>
        {
            DataType = (NeuralDataType)obj;
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand AddSelected => new DelegateCommand<object>((obj) =>
        {
            if (obj == null)
            {
                Toast.Warn($"未选择项目");
                return;
            }

            //
            SelectChannel(DataType, ((PresentChannel)obj).Channel);
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand AddAll => new DelegateCommand(() =>
        {
            foreach (var item in PresentChannels)
            {
                SelectChannel(DataType, item.Channel);
            }
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand RemoveSelected => new DelegateCommand<object>((obj) =>
        {
            if (obj == null)
            {
                Toast.Warn($"未选择项目");
                return;
            }

            //
            var outputChannel = (OutputChannel)obj;
            UnselectChannel(outputChannel.DataType, outputChannel.Channel);
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand RemoveAll => new DelegateCommand(() =>
        {
            var outputChannels = OutputChannels.ToList();
            foreach (var item in outputChannels)
            {
                UnselectChannel(item.DataType, item.Channel);
            }
        });
    }

    /// <summary>
    /// 
    /// </summary>
    public class PresentChannel
    {
        public string Title { get; set; }
        public int Channel { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OutputChannel
    {
        public string Title { get; set; }
        public int Channel { get; set; }
        public NeuralDataType DataType { get; set; }
    }
}
