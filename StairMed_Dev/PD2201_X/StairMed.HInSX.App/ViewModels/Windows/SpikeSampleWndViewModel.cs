﻿using Prism.Commands;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.HInSX.App.Core;
using StairMed.MVVM.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace StairMed.HInSX.App.ViewModels.Windows
{
    internal class SpikeSampleWndViewModel : ViewModelBase
    {
        public HInSXSettings Setting { get; set; } = HInSXSettings.Instance;

        private HInSXCollectParamInfo _deviceParam;
        public HInSXCollectParamInfo DeviceParam
        {
            get { return _deviceParam; }
            set
            {
                Set(ref _deviceParam, (HInSXCollectParamInfo)value);
            }
        }

        private bool _channelsCheckedAll;

        public bool ChannelsCheckedAll
        {
            get { return _channelsCheckedAll; }
            set { Set(ref _channelsCheckedAll, value); }
        }


        public SpikeSampleWndViewModel()
        {
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            //ChannelsCheckedAll = DeviceParam.Electrodes.Exists(_ => _.Enabled = false);
        }

        public ICommand ResetHinX => new DelegateCommand(() =>
        {
            HInSXManager.Instance.ResetMode();
        }
        );

        public ICommand ChannelSelectAllCommand => new DelegateCommand<object>((e) =>
        {
            //HInSXManager.Instance.ResetMode();
            try
            {
                bool isChecked = (bool)e;

                DeviceParam.Electrodes.ForEach(_ => _.Enabled = isChecked);

            }
            catch (Exception)
            {

                ;
            }
        }
        );

        private string _testPort;

        public string TestPort
        {
            get { return _testPort; }
            set { Set(ref _testPort, value); }
        }

        private string _testChannel;

        public string TestChannel
        {
            get { return _testChannel; }
            set { Set(ref _testChannel, value); }
        }
        public ICommand SendTestModeCommand => new DelegateCommand(() => {
            try
            {
                int port = 0;
                try
                {
                    port = Convert.ToInt32(TestPort);
                }
                catch (Exception)
                {
                    port = 0;
                }
                int channel = 0;
                try
                {
                    channel = Convert.ToInt32(TestChannel);
                }
                catch (Exception)
                {
                    channel = 0;
                }
                HInSXManager.Instance.SendArmTest(port, channel);
            }
            catch (Exception)
            {

            }

        });
    }
}
