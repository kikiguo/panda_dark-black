﻿using HandyControl.Expression.Shapes;
using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter;
using StairMed.DataCenter.Centers.Trigger;
using StairMed.DataCenter.Recorder;
using StairMed.Enum;
using StairMed.Event;
using StairMed.HinSX.App.Views.Windows;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Views.Regions.Bodies;
using StairMed.HInSX.App.Views.Windows;
using StairMed.MVVM.Base;
using StairMed.MVVM.Helper;
using StairMed.Tools;
using StairMed.UI.Views.UserControls.SideBars;
using StairMed.UI.Views.Windows.Analyses;
using StairMed.UI.Views.Windows.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace StairMed.HInSX.App.ViewModels.Regions
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSHeadViewModel : RegionViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public NeuralDataCollector NeuralDataCollector { get; set; } = NeuralDataCollector.Instance;
        public NeuralDataRecorder NeuralDataRecorder { get; set; } = NeuralDataRecorder.Instance;
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;
        /// <summary>
        /// 
        /// </summary>
        private BodyType _bodyType = BodyType.RealTime;
        public BodyType BodyType
        {
            get { return _bodyType; }
            set { Set(ref _bodyType, value); }
        }

        private long _deviceId = -1;
        public long DeviceId
        {
            get { return _deviceId; }
            set
            {
                Set(ref _deviceId, value);
            }
        }
        public HInSHeadViewModel()
        {
            DeviceId = HInSXManager.Instance.DeviceInfo.DeviceId;
            Task.Factory.StartNew(GenerateTriggerLoop, TaskCreationOptions.LongRunning);
        }
        private int realTimeSpikeScopeChannel = 0;
        private int realTimeWaveformChannel = 0;
        /// <summary>
        /// 
        /// </summary>
        public ICommand SwitchBody => new DelegateCommand<object>((obj) =>
        {
            var bodyType = (BodyType)obj;
            BodyType = bodyType;
            if (NeuralDataCollector.IsCollecting || NeuralDataRecorder.IsRecording)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    MsgBox.Error($"请停止采集后再做切换", "提示");
                }));
                return;
            }
            switch (bodyType)
            {
                case BodyType.RealTime:
                    {
                        Settings.SpikeScopeChannel = realTimeSpikeScopeChannel;
                        Settings.WaveformChannel = realTimeWaveformChannel;
                        Settings.MainViewType = 0;
                        ReadonlyCONST.DeviceType = DeviceType.HInSX;
                        ReadonlyCONST.ProbeMap = ProbeMapSettings.Instance.ProbeMap;
                        ReadonlyCONST.DeviceChannelCount = HInSXCONST.CHANNELS_COUNT;
                        PanleChangedCommand.Execute(4);
                        RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(HInSXRTView));
                    }
                    break;
                case BodyType.History:
                    {
#if true
                        realTimeSpikeScopeChannel = Settings.SpikeScopeChannel;
                        realTimeWaveformChannel = Settings.WaveformChannel;
                        Settings.MainViewType = 1;
                        PanleChangedCommand.Execute(4);
                        RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(PlayerView));
#else
                        string strAppName = CONST.AppInstallPath;
                        strAppName += "\\Playback\\Playback.exe";
                        Process.Start(strAppName);
#endif
                    }
                    break;
                default:
                    break;
            }
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand ShowAbout => new DelegateCommand<object>((obj) =>
        {
            HInSAboutWnd.PopUp();
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand Setting => new DelegateCommand<object>((obj) =>
        {
            HinSXSettingWnd.PopUp();
        });

        public ICommand SelectChannelsCommand => new DelegateCommand<object>((obj) =>
        {
            SelectChannelsWnd.PopUp();
        });

        public ICommand ISIMenuCommand => new DelegateCommand<object>((obj) =>
        {
            ISIWnd.PopUp();
        });


        public ICommand PSTHMenuCommand => new DelegateCommand<object>((obj) =>
        {
            PSTHWnd.PopUp();
        });

        public ICommand SpectralMenuCommand => new DelegateCommand<object>((obj) =>
        {
            SpectrumWnd.PopUp();
        });

        public ICommand RegisterMenuCommand => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                HInSRegisterWnd.PopUp();
            });
        });

        public ICommand ImpedanceMenuCommand => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                new HInSImpedanceWnd(DeviceId).ShowDialog();
            });
        });

        public ICommand SpikeMapMenuCommand => new DelegateCommand<object>((obj) =>
        {
            SpikeMapWnd.PopUp();
        });

        public ICommand SimulateMenuCommand => new DelegateCommand<object>((obj) =>
        {
            _autoGenerate = !_autoGenerate;
        });

        public ICommand RMSMenuCommand => new DelegateCommand<object>((obj) =>
        {
            RMSWnd.PopUp();
        });

        public ICommand FilterMenuCommand => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                new HInSImpedanceWnd(DeviceId).ShowDialog();
            });
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand RemoteTCPControlSetting => new DelegateCommand<object>((obj) =>
        {
            HInSTcpControlWnd.PopUp();

            //
            ReadonlyCONST.ProbeMap = ProbeMapSettings.Instance.ProbeMap;
            RegionHelper.RequestNavigate(RegionNames.BodyRegion, nameof(HInSXRTView));

            //
            BodyType = BodyType.RealTime;
        });

        /// <summary>
        /// 
        /// </summary>
        private int _index = 0;
        private readonly Dictionary<int, bool> _levelDict = new Dictionary<int, bool>();
        private long _lastTriggerIndex = 0;
        private bool _autoGenerate = false;

        /// <summary>
        /// 
        /// </summary>
        public void GenerateTriggerLoop()
        {
            while (true)
            {
                try
                {
                    while (_autoGenerate)
                    {
                        Thread.Sleep(200);
                        //Thread.Sleep(50);

                        //
                        _index++;
                        if (!NeuralDataCollector.Instance.IsCollecting)
                        {
                            break;
                        }

                        //
                        var triggerChannels = new HashSet<int>();
                        for (int i = 0; i < ReadonlyCONST.TriggerChannels.Count; i++)
                        {
                            if (_index % (i * 2 + 2) == 0)
                            {
                                triggerChannels.Add(i);
                            }
                        }

                        //
                        if (triggerChannels.Count <= 0)
                        {
                            continue;
                        }

                        //
                        var pack = new TriggerPack
                        {
                            SampleRate = NeuralDataCollector.Instance.GetCollectingSampleRate(),
                            Triggers = new Dictionary<int, List<TriggerEdge>>()
                        };

                        //
                        var offset = NeuralDataCollector.Instance.RecvdLength;

                        //
                        if (offset <= _lastTriggerIndex)
                        {
                            offset = _lastTriggerIndex + 300;
                        }
                        _lastTriggerIndex = offset;

                        //
                        foreach (var channel in triggerChannels)
                        {
                            var oldLevel = _levelDict.ContainsKey(channel) ? _levelDict[channel] : false;

                            //
                            pack.Triggers[channel] = new List<TriggerEdge>
                            {
                                new TriggerEdge { ToHigh = oldLevel, Index = offset}
                            };

                            //
                            _levelDict[channel] = !oldLevel;
                        }

                        //
                        TriggerDataPool.Instance.BufferTrigger(pack);
                    }
                }
                catch { }
                Thread.Sleep(1000);
                _lastTriggerIndex = 0;
            }
        }

      

        /// <summary>
        /// 
        /// </summary>
        public ICommand Log => new DelegateCommand<object>((obj) =>
        {
            try
            {
                Process.Start("explorer.exe", Path.Combine(CONST.AppInstallPath, "Log"));
            }
            catch { }
        });

        public ICommand StartCollectCommand => new DelegateCommand<object>((obj) => {
            EventHelper.Publish<StartCollectEvent, int>(1);
        });
        public ICommand StopCollectCommand => new DelegateCommand<object>((obj) => {
            EventHelper.Publish<StopCollectEvent, int>(1);
        });

        public ICommand PanleChangedCommand => new DelegateCommand<object>((obj) =>
        {
            try
            {
                int layout = 0;
                if (Int32.TryParse(obj?.ToString(), out layout))
                {
                    EventHelper.Publish<LayoutChangedEvent, int>(layout);
                }

            }
            catch (Exception)
            {

            }

        });


        public ICommand StartRecordCommand => new DelegateCommand<object>((obj) => {
            EventHelper.Publish<StartRecordEvent, int>(1);
        });
        public ICommand StopRecordCommand => new DelegateCommand<object>((obj) => {
            EventHelper.Publish<StopRecordEvent, int>(1);
        });
        public ICommand SelectNwbFileCommand => new DelegateCommand<object>((obj) => {
            EventHelper.Publish<SelectNWBFileEvent, int>(1);
        });
        public ICommand StartPlayCommand => new DelegateCommand<object>((obj) => {
            double p = 0;
            if (!double.TryParse(obj?.ToString(), out p))
            {
                p = 1;
            }
            EventHelper.Publish<PlayNWBFileEvent, double>(p);
        });
        public ICommand StopPlayCommand => new DelegateCommand<object>((obj) => {
            int p = 0;
            if (!Int32.TryParse(obj?.ToString(), out p))
            {
                p = 1;
            }
            EventHelper.Publish<StopPlayNWBFileEvent, int>(p);
        });

        public ICommand SavedataSettingCommand => new DelegateCommand<object>((obj) => {
            GenericWnd.PopUp("Save data setting",new SaveDataSettingPage());
        });

        public ICommand OpenPlaybackPannelCommand => new DelegateCommand<object>((obj) =>
        {
            bool toOpen = false;
            bool.TryParse(obj?.ToString(), out toOpen);
            EventHelper.Publish<OpenPlaybackPanelEvent, bool>(toOpen);
        });

        public ICommand WebsiteCommand => new DelegateCommand(() => { 
            Hyperlink link = new Hyperlink();
            link.NavigateUri = new Uri("https://www.stairmed.com/");
            System.Diagnostics.Process.Start("explorer", link.NavigateUri.AbsoluteUri);
    });

        public ICommand FilterSettingCommand => new DelegateCommand(() => {
            GenericWnd.PopUp("Filter Parameter", new FilterSettingsPage(), 1200, 904);
        });

        public ICommand SampleSettingCommand => new DelegateCommand(() =>
        {
            GenericWnd.PopUp("Sample Properties", new SampleSettings(), 1200, 904);
        });

        public ICommand AudioSettingCommand => new DelegateCommand(() => {
            GenericWnd.PopUp("Audio Properties", new AudioSettings(), 984, 562);
        });

        public ICommand Sampling => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                SpikeSampleWnd.PopUp();
            });
        });

        public ICommand Audio => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                AudioPropertiesWnd.PopUp();
            });
        });


    }
}
