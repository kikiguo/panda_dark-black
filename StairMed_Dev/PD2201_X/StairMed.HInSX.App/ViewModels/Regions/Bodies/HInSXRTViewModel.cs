﻿using HandyControl.Controls;
using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.Core.Settings;
using StairMed.DataCenter.Centers;
using StairMed.DataCenter.Centers.SpikeScope;
using StairMed.Entity.Infos;
using StairMed.Entity.Infos.Bases;
using StairMed.Enum;
using StairMed.Event;
using StairMed.Event.RemoteTCPControl;
using StairMed.HInSX.App.Core;
using StairMed.HInSX.App.Core.Entitys;
using StairMed.HInSX.App.ViewModels.Windows;
using StairMed.HInSX.App.Views.Windows;
using StairMed.MVVM.Helper;
using StairMed.Probe;
using StairMed.Tools;
using StairMed.UI.ViewModels.UserControls.Waveforms;
using StairMed.UI.Views.Windows.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;

namespace StairMed.HInSX.App.ViewModels.Regions.Bodies
{
    /// <summary>
    /// 
    /// </summary>
    internal class HInSXRTViewModel : WaveformVMBase
    {
        const int GroupChannelCount = 1024;
        private readonly object _lockObj = new object();

        #region 绑定属性

        /// <summary>
        /// 
        /// </summary>
        public override bool NeedFilter => false;

        public HInSXSettings Setting { get; set; } = HInSXSettings.Instance;
        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private HInSXCollectParamInfo _chartDataParam;
        public override CollectParamInfo ChartDataParam
        {
            get { return _chartDataParam; }
            set { Set(ref _chartDataParam, (HInSXCollectParamInfo)value); }
        }

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private HInSXCollectParamInfo _deviceParam;
        public override CollectParamInfo DeviceParam
        {
            get { return _deviceParam; }
            set
            {
                Set(ref _deviceParam, (HInSXCollectParamInfo)value);
            }
        }

        /// <summary>
        /// 单个headstage上多少个通道
        /// </summary>
        private int _singleHeadstageChannels = 10;
        public int SingleHeadstageChannels
        {
            get { return _singleHeadstageChannels; }
            set { Set(ref _singleHeadstageChannels, value); }
        }

        /// <summary>
        /// headstage，每个具有128通道：与电极对应
        /// </summary>
        private ObservableCollection<HeadstageExtend> _headstages = new ObservableCollection<HeadstageExtend>();
        public ObservableCollection<HeadstageExtend> Headstages
        {
            get { return _headstages; }
            set { 
                Set(ref _headstages, value);
            }
        }
        private bool headstageIndexChangedInSubWin = false;
        /// <summary>
        /// 选择第几个headstage显示
        /// </summary>
        private int _selectHeadstageIndex = 0;
        public int SelectHeadstageIndex
        {
            get { return _selectHeadstageIndex; }
            set
            {
                //切换Headstages时，会导致_selectHeadstageIndex = -1;
                //在InitWaveformUI中对此进行了判断
                //不可在此处理-1问题，否则在切换SelectHeadstageGroup时，会导致此tablecontrol无内容被选中
                if (_selectHeadstageIndex != value)
                {
                    Set(ref _selectHeadstageIndex, value);
                    InitWaveformUI();
                    Settings.SpikeScopeChannel = SingleHeadstageChannels * _selectHeadstageIndex;
                    Settings.WaveformChannel= SingleHeadstageChannels * _selectHeadstageIndex;
                    if (!headstageIndexChangedInSubWin)
                        EventHelper.Publish<ChannelChangedEvent, int>(_selectHeadstageIndex);
                }
                headstageIndexChangedInSubWin = false;
            }
        }

        private ObservableCollection<string> _headstageGroupsSerial = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroupsSerial
        {
            get { return _headstageGroupsSerial; }
            set { Set(ref _headstageGroupsSerial, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<string> _headstageGroups = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroups
        {
            get { return _headstageGroups; }
            set { Set(ref _headstageGroups, value); }
        }

        /// <summary>
        /// 每1024通道对应一个board
        /// </summary>
        private int _selectHeadstageGroup = 0;
        public int SelectHeadstageGroup
        {
            get { return _selectHeadstageGroup; }
            set
            {
                value = Math.Min(Math.Max(value, 0), HInSXCONST.CHANNELS_COUNT / GroupChannelCount - 1);
                if (_selectHeadstageGroup != value)
                {
                    Set(ref _selectHeadstageGroup, value);

                    //更新table control中各个headstage的标题名称
                    UpdateTableControlHeadstages();

                    //
                    if (SelectHeadstageIndex == 0)
                    {
                        InitWaveformUI();
                    }
                    else
                    {
                        SelectHeadstageIndex = 0;
                    }                    
                }
            }
        }
        private List<float> _waveFormTimestamp1;

        public List<float> WaveFormTimestamp1
        {
            get { return _waveFormTimestamp1; }
            set
            {
                if (_waveFormTimestamp1 != value)
                {
                    Set(ref _waveFormTimestamp1, value);

                }
            }
        }
        private List<float> _waveFormTimestamp2;

        public List<float> WaveFormTimestamp2
        {
            get { return _waveFormTimestamp2; }
            set
            {
                if (_waveFormTimestamp2 != value)
                {
                    Set(ref _waveFormTimestamp2, value);
                }
            }
        }
        private List<float> _waveFormTimestamp3;

        public List<float> WaveFormTimestamp3
        {
            get { return _waveFormTimestamp3; }
            set
            {
                if (_waveFormTimestamp3 != value)
                {
                    Set(ref _waveFormTimestamp3, value);
                }
            }
        }
        private List<float> _waveFormTimestamp4;

        public List<float> WaveFormTimestamp4
        {
            get { return _waveFormTimestamp4; }
            set
            {
                if (_waveFormTimestamp4 != value)
                {
                    Set(ref _waveFormTimestamp4, value);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSXManager HInSXManager { get; set; } = HInSXManager.Instance;

        #endregion

        public delegate void SpikeTimestampsUpdatedEventHandler(int collectId, int channel, int sampleRate, long recvd, Dictionary<int, List<float>> timestamps);
        public event SpikeTimestampsUpdatedEventHandler SpikeTimestampsUpdated;
        public int SortedIndex = -1;

        /// <summary>
        /// 
        /// </summary>
        public HInSXRTViewModel()
        {

            //
            DeviceId = HInSXManager.Instance.DeviceInfo.DeviceId;
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            ChartDataParam = HInSXManager.Instance.GetCollectionParam();
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            //
            UpdateTableControlHeadstageGroups();
            UpdateTableControlHeadstages();

            //
            SelectHeadstageGroup = 0;
            SelectHeadstageIndex = 0;

            //
            InitWaveformUI();

            //
            EventHelper.Subscribe<RemoteTCPControlSetSampleRateEvent, List<string>>((cmdParams) =>
            {
                if (int.TryParse(cmdParams[2], out int samplerate) && ReadonlyCONST.SampleRates.Contains(samplerate))
                {
                    DeviceParam.SampleRate = samplerate;
                }
            }, Prism.Events.ThreadOption.UIThread);

            //
            EventHelper.Subscribe<RemoteTCPControlRunModeCommandEvent, List<string>>((cmdParams) =>
            {
                if (cmdParams[2].Equals("start"))
                {
                    StartCollect.Execute(null);
                }
                else if (cmdParams[2].Equals("stop"))
                {
                    StopCollect.Execute(null);
                }
            }, Prism.Events.ThreadOption.UIThread);

            HInSXManager.Instance.OnHeadStageInsertedStateChanged += (int headstage, bool inserted) => {
                if (Headstages.Count > headstage)
                {
                    var newHeadstages = new ObservableCollection<HeadstageExtend>();
                    for (int i = 0; i < Headstages.Count; i++)
                    {
                        var item = Headstages[i];
                        if (i == headstage)
                            item.IsInserted = inserted;
                        newHeadstages.Add(item);
                    }
                    this.Headstages = newHeadstages;
                    StairMedSettings.Instance.InsertedHeadstages = newHeadstages.Where(_ => _.IsInserted).Select(_ => _.HeadContent).ToList();
                    EventHelper.Publish<HeadstageInsertedEvent>();
                }
            };
            //_listDisplayMode = new ObservableCollection<MultiCbxBaseData>()
            //{
            //    new MultiCbxBaseData(){ID=0,ViewName="RAW",IsCheck=true },
            //    new MultiCbxBaseData(){ID=1,ViewName="SPK",IsCheck=false },
            //    new MultiCbxBaseData(){ID=2,ViewName="LFP",IsCheck=false },
            //    new MultiCbxBaseData(){ID=3,ViewName="HFP",IsCheck=false },
            //};
            EventHelper.Subscribe<ChannelChangedEventFromSub, int>((cmdParams) =>
            {
                headstageIndexChangedInSubWin = true;
                this.SelectHeadstageIndex = cmdParams;
            }, Prism.Events.ThreadOption.UIThread);
            //System.Threading.Tasks.Task.Factory.StartNew(new Action(()=>{
            //    while (true)
            //    {
            //        var newHeadstages = new ObservableCollection<HeadstageExtend>();
            //        for (int i = 0; i < Headstages.Count; i++)
            //        {
            //            var item = Headstages[i];
            //            item.IsInserted = !item.IsInserted;
            //            newHeadstages.Add(item);
            //        }
            //        this.Headstages = newHeadstages;
            //        System.Threading.Thread.Sleep(5000);
            //    }
            //}));

            EventHelper.Subscribe<StartCollectEvent, int>((_) => StartCollectHandler(_));
            EventHelper.Subscribe<StopCollectEvent, int>((_) => { StopCollect?.Execute(null); });

            EventHelper.Subscribe<StartRecordEvent, int>((_) => { StartRecord?.Execute(null); });
            EventHelper.Subscribe<StopRecordEvent, int>((_) => { StopRecord?.Execute(null); });

            EventHelper.Subscribe<FileReadCompletedEvent>(() =>
            {
                ((DelegateCommand)StopPlay).Execute();
            }, Prism.Events.ThreadOption.UIThread);

            EventHelper.Subscribe<SelectNWBFileEvent,int>((_) =>
            {
                ((DelegateCommand)ChooseNWBFile).Execute();
            }, Prism.Events.ThreadOption.UIThread);

            EventHelper.Subscribe<PlayNWBFileEvent, double>((_) =>
            {
                ((DelegateCommand<object>)StartPlay).Execute(_);
            }, Prism.Events.ThreadOption.UIThread);

            EventHelper.Subscribe<StopPlayNWBFileEvent, int>((_) =>
            {
                ((DelegateCommand)StopPlay).Execute();
            }, Prism.Events.ThreadOption.UIThread);

            EventHelper.Subscribe<OpenPlaybackPanelEvent, bool>((_) => {
                this.ShowPlaybackPanel = _;
            }, Prism.Events.ThreadOption.UIThread);

#if true //for test
            WaveFormTimestamp1 = new List<float>();
            WaveFormTimestamp2 = new List<float>();
            WaveFormTimestamp3 = new List<float>();
            WaveFormTimestamp4 = new List<float>();
            Queue<float> waveFormTimestamp1Queue = new Queue<float>();
            Queue<float> waveFormTimestamp2Queue = new Queue<float>();
            Queue<float> waveFormTimestamp3Queue = new Queue<float>();
            Queue<float> waveFormTimestamp4Queue = new Queue<float>();
            // 只订阅一次事件
            SpikeScopeCenter.Instance.RecvdChannelSpikeScopes -= HandleSpikes; // 首先取消订阅以避免重复订阅
            SpikeScopeCenter.Instance.RecvdChannelSpikeScopes += HandleSpikes;

            void HandleSpikes(int collectId, int channel, int sampleRate, long recvd, List<SpikeScopeInfo> spikeScopes)
            {
                if (spikeScopes.Count < 4) return;

                Application.Current.Dispatcher.InvokeAsync(() =>
                {
                    lock (_lockObj)
                    {
                        foreach (var spikeScope in spikeScopes)
                        {
                            float tempTimestamp = spikeScope.Timestamps;
                            switch (spikeScope.Label)
                            {
                                case 0:
                                    waveFormTimestamp1Queue.Enqueue(tempTimestamp);
                                    waveFormTimestamp2Queue.Enqueue(0);
                                    break;
                                case 1:
                                    waveFormTimestamp2Queue.Enqueue(tempTimestamp);
                                    waveFormTimestamp1Queue.Enqueue(0);
                                    break;
                                case 2:
                                    waveFormTimestamp3Queue.Enqueue(tempTimestamp);
                                    waveFormTimestamp4Queue.Enqueue(0);
                                    break;
                                case 3:
                                    waveFormTimestamp4Queue.Enqueue(tempTimestamp);
                                    waveFormTimestamp3Queue.Enqueue(0);
                                    break;
                            }
                        }

                        ProcessTimestampQueues();
                    }
                });
            }
  
            void ProcessTimestampQueues()
            {
                if (waveFormTimestamp1Queue.Any())
                {
                    WaveFormTimestamp1 = waveFormTimestamp1Queue.ToList();
                    waveFormTimestamp1Queue.Clear();
                }

                if (waveFormTimestamp2Queue.Any())
                {
                    WaveFormTimestamp2 = waveFormTimestamp2Queue.ToList();
                    waveFormTimestamp2Queue.Clear();
                }

                if (waveFormTimestamp3Queue.Any())
                {
                    WaveFormTimestamp3 = waveFormTimestamp3Queue.ToList();
                    waveFormTimestamp3Queue.Clear();
                }

                if (waveFormTimestamp4Queue.Any())
                {
                    WaveFormTimestamp4 = waveFormTimestamp4Queue.ToList();
                    waveFormTimestamp4Queue.Clear();
                }
            }
#endif
        }


        /// <summary>
        /// 刷新波形列表显示的波形
        /// </summary>
        public void InitWaveformUI()
        {
            if (SelectHeadstageGroup >= 0 && SelectHeadstageGroup < HInSXCONST.CHANNELS_COUNT / GroupChannelCount && SelectHeadstageIndex >= 0 && SelectHeadstageIndex < GroupChannelCount / SingleHeadstageChannels)
            {
                var begin = SelectHeadstageIndex * SingleHeadstageChannels + SelectHeadstageGroup * GroupChannelCount;
                InitWaveformUI(DeviceId, Enumerable.Range(begin, SingleHeadstageChannels).ToList());
            }
        }

        /// <summary>
        /// 更新table control中各个headstage group的标题名称
        /// </summary>
        private void UpdateTableControlHeadstageGroups()
        {
            //大于1024通道时，设置group分组
            if (HInSXCONST.CHANNELS_COUNT > GroupChannelCount)
            {
                var headstageGroups = new ObservableCollection<string>();
                for (int i = 0; i < HInSXCONST.CHANNELS_COUNT / GroupChannelCount; i++)
                {
                    headstageGroups.Add($"{(char)('A' + i)}");
                }
                HeadstageGroups = headstageGroups;
            }
        }

        /// <summary>
        /// 更新table control中各个headstage的标题名称
        /// </summary>
        private void UpdateTableControlHeadstages()
        {
            //大于1024通道时，切换group则切换headstage tab的标题
            if (HInSXCONST.HInSXType != Command.HInSXType.HInS_1024)
            {
                var groupName = $"{(char)('A' + SelectHeadstageGroup)}";

                //
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{groupName}{i + 1}");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{groupName}{i + 1}"
                        });
                    }
                }

                //此动作会触发导致selectindex = -1
                for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                {
                    //Headstages[i] = $"{groupName}{i + 1}";
                    Headstages.Add(new HeadstageExtend { HeadContent = $"{groupName}{i + 1}" });
                }
            }
            else
            {
                //1024通道时，不需要变动headstage类型
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]",
                        });
                        HeadstageGroupsSerial.Add($"{(i + 1).ToString()}");
                    }
                }
            }
        }

        /// <summary>
        /// 开始采集
        /// </summary>
        /// <returns></returns>
        protected override bool StartCollectImpl()
        {
            //ChangeWaveformName();
            if (HInSXManager.Instance.StartCollect(_deviceParam, out string errMsg))
            {
                return true;
            }

            //
            if (!string.IsNullOrWhiteSpace(errMsg))
            {
                Toast.Warn(errMsg);
            }

            //
            return false;
        }

        /// <summary>
        /// 停止采集
        /// </summary>
        /// <returns></returns>
        protected override bool StopCollectImpl()
        {
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            var ret = HInSXManager.Instance.StopCollect();
            if (ret)
            {
                NeuralDataRecorder.StopRecording();
            }
            return ret;
        }

        public ICommand ResetHinX => new DelegateCommand(() =>
        {
            HInSXManager.Instance.ResetMode();
        }
        );

        /// <summary>
        /// 通道映射关系
        /// </summary>
        public ICommand ViewProbeMap => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                var imported = false;

                //
                new ElectrodeMapWnd(ProbeMapSettings.Instance.ProbeMap, (newProbeMap) =>
                {
                    ProbeMapSettings.Instance.ProbeMap = newProbeMap;
                    imported = true;
                }).ShowDialog();

                //
                if (imported)
                {
                    App.Reboot = true;
                    Application.Current.Shutdown();
                }
            });
        });

        /// <summary>
        /// 寄存器操作
        /// </summary>
        public ICommand ShowRegister => new DelegateCommand<object>((obj) =>
        {

            UIHelper.BeginInvoke(() =>
            {
                HInSRegisterWnd.PopUp();
            });
        });

        /// <summary>
        /// 查看硬件状态
        /// </summary>
        public ICommand ViewHardwareInfoCommand => new DelegateCommand<object>((obj) =>
        {
            HInSXManager.Instance.QueryRefreshStates();
            UIHelper.BeginInvoke(() =>
            {
                HInSXHardwareWnd.PopUp();
            });
           
        });

        #region 自动阈值

        /// <summary>
        /// Spkie阈值设置
        /// </summary>
        public ICommand ViewSpikeDetectionThresholdSetting => new DelegateCommand<object>((obj) =>
        {
            UIHelper.BeginInvoke(() =>
            {
                new AutoSpikeThresholdWnd((double multi) =>
                {
                    _multi = multi;

                    //准备采样所有通道
                    var collectParamInfo = HInSXManager.Instance.GetCollectionParam();
                    collectParamInfo.InitChannels(ReadonlyCONST.ProbeMap);
                    collectParamInfo.SelectAll();

                    //启动采样
                    if (!HInSXManager.Instance.StartCollect(_deviceParam, out string errMsg))
                    {
                        if (!string.IsNullOrWhiteSpace(errMsg))
                        {
                            Toast.Warn(errMsg);
                        }
                        else
                        {
                            Toast.Error($"启动采样失败");
                        }
                        return false;
                    }

                    //等待获取到足够长度的数据后，再对rms进行接收：【尿检接中尿】
                    Thread.Sleep(1000 + StairMedSolidSettings.Instance.RMSStatisticMS * 2);
                    RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
                    RMSCenter.Instance.HfpRmsUpdated += Instance_HfpRmsUpdated;
                    for (int i = 0; i < ReadonlyCONST.DeviceChannelCount; i++)
                    {
                        RMSCenter.Instance.RegisterInputChannel(i);
                    }

                    //等待所有通道都处理完
                    Thread.Sleep(5000);

                    //停止监听、停止采集、停止数据处理
                    RMSCenter.Instance.HfpRmsUpdated -= Instance_HfpRmsUpdated;
                    for (int i = 0; i < ReadonlyCONST.DeviceChannelCount; i++)
                    {
                        RMSCenter.Instance.UnregisterInputChannel(i);
                    }
                    Thread.Sleep(2000);
                    HInSXManager.Instance.StopCollect();

                    //
                    Toast.Info("设置成功");

                    //
                    return true;
                }).ShowDialog();
            });
        });

        private double _multi = 1.0;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="rms"></param>
        private void Instance_HfpRmsUpdated(int channel, double rms)
        {
            SpikeThresholdCenter.Instance.SetThreshold(channel, Convert.ToInt32(_multi * rms));
        }

        #endregion

        /// <summary>
        /// 阻抗测试
        /// </summary>
        public ICommand ImpedanceTest => new DelegateCommand(() =>
        {
            UIHelper.BeginInvoke(() =>
            {
                new HInSImpedanceWnd(DeviceId).ShowDialog();
            });
        });

        /// <summary>
        /// 导入映射关系
        /// </summary>
        public ICommand ImportProbeMap => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            UIHelper.BeginInvoke(() =>
            {
                var dialog = new Microsoft.Win32.OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //
                try
                {
                    //
                    var xml = FileHelper.LoadFile(dialog.FileName, Encoding.UTF8);
                    if (string.IsNullOrWhiteSpace(xml))
                    {
                        Toast.Warn($"文件为空，无法导入");
                        return;
                    }

                    //
                    using (var reader = new StringReader(xml))
                    {
                        NTProbeMap newProbeMap = null;
                        try
                        {
                            newProbeMap = (NTProbeMap)new XmlSerializer(typeof(NTProbeMap)).Deserialize(reader);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogTool.Logger.Exp($"{TAG}, Deserialize", ex);
                        }

                        //
                        if (newProbeMap == null)
                        {
                            Toast.Warn($"文件有误，无法导入");
                            return;
                        }

                        //
                        if (newProbeMap.IsInValid())
                        {
                            Toast.Warn($"文件无效，导入失败");
                            return;
                        }

                        //
                        ReadonlyCONST.ProbeMap = newProbeMap;

                        //
                        Toast.Info($"导入成功");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogTool.Logger.Exp($"{TAG}, load", ex);
                    Toast.Error($"文件有误，无法导入");
                    return;
                }
            });
        });

        public ICommand SpikeScopeCommand => new DelegateCommand(() => {
            SpikeScopeMainWnd.PopUp();
        });

        public ICommand ParameterPanelCommand => new DelegateCommand<object>((obj) => {
            ParameterPanelShowed = !ParameterPanelShowed;
        });

        private bool _parameterPanelShowed;

        public bool ParameterPanelShowed
        {
            get { return _parameterPanelShowed; }
            set { Set(ref _parameterPanelShowed, value); }
        }

        private string _waveformSingleDisplayMode;

        public string WaveformSingleDisplayMode
        {
            get { return _waveformSingleDisplayMode; }
            set 
            {
                Set(ref _waveformSingleDisplayMode, value);

                WaveformSingleViewModel.DisplayRaw = value == "RAW";
                WaveformSingleViewModel.DisplaySpike = value == "SPK";
                WaveformSingleViewModel.DisplayLFWave = value == "LFP";
                WaveformSingleViewModel.DisplayHFWave = value == "HFP";
                WaveformSingleViewModel.DisplaySpikeSum = value == "FR";
                StairMedSettings.Instance.DisplayTRIG = value == "TRIG";
            }
        }

        //private ObservableCollection<MultiCbxBaseData> _listDisplayMode;

        //public ObservableCollection<MultiCbxBaseData> ListDisplayMode
        //{
        //    get { return _listDisplayMode; }
        //    set 
        //    { 
        //        Set(ref _listDisplayMode, value);

        //    }
        //}

        //private ObservableCollection<MultiCbxBaseData> _selectedDisplayValues=new ObservableCollection<MultiCbxBaseData> {
        //    new MultiCbxBaseData{ ID=0,ViewName="channel name",IsCheck=true },
        //    new MultiCbxBaseData{ ID=1,ViewName="impedance",IsCheck=false },
        //};

        //public ObservableCollection<MultiCbxBaseData> SelectedDisplayValues
        //{
        //    get { return _selectedDisplayValues; }
        //    set { Set(ref _selectedDisplayValues, value); }
        //}
        public ICommand PanleChangedCommand => new DelegateCommand<object>((obj) =>
        {
            try
            {
                int layout = 0;
                if (Int32.TryParse(obj?.ToString(), out layout))
                {
                    EventHelper.Publish<LayoutChangedEvent, int>(layout);
                }

            }
            catch (Exception)
            {

            }

        });

        private string _testPort;

        public string TestPort
        {
            get { return _testPort; }
            set { Set(ref _testPort, value); }
        }

        private string _testChannel;

        public string TestChannel
        {
            get { return _testChannel; }
            set { Set(ref _testChannel, value); }
        }
        public ICommand SendTestModeCommand => new DelegateCommand(() => {
            try
            {
                int port = 0;
                try
                {
                    port = Convert.ToInt32(TestPort);
                }
                catch (Exception)
                {
                    port = 0;
                }
                int channel = 0;
                try
                {
                    channel = Convert.ToInt32(TestChannel);
                }
                catch (Exception)
                {
                    channel = 0;
                }
                HInSXManager.Instance.SendArmTest(port, channel);
            }
            catch (Exception)
            {

            }

        });

        #region player
        private bool _showPlaybackPanel;

        public bool ShowPlaybackPanel
        {
            get { return _showPlaybackPanel; }
            set { Set(ref _showPlaybackPanel, value); }
        }

        protected override void CollectPreParation()
        {
            this.ShowPlaybackPanel = false;
            ReadonlyCONST.DeviceType = DeviceType.HInSX;
            ReadonlyCONST.ProbeMap = ProbeMapSettings.Instance.ProbeMap;
            ReadonlyCONST.DeviceChannelCount = HInSXCONST.CHANNELS_COUNT;

            DeviceId = HInSXManager.Instance.DeviceInfo.DeviceId;
            DeviceParam = HInSXManager.Instance.GetCollectionParam();
            ChartDataParam = HInSXManager.Instance.GetCollectionParam();
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;
            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            SelectHeadstageIndex = 0;
            base.CollectPreParation();
        }

        public NWBPlayController PlayController { get; set; } = NWBPlayController.Instance;

        private NWBCollectParamInfo _deviceParamNWB = new NWBCollectParamInfo(1);
        public CollectParamInfo DeviceParamNWB
        {
            get { return _deviceParamNWB; }
            set { Set(ref _deviceParamNWB, (NWBCollectParamInfo)value); }
        }

        private List<int> _inputChannels = new List<int>(); //通道们
        private List<int> _physicsChannels = new List<int>(); //通道们

      

        public ICommand StopPlay => new DelegateCommand(() => {
            //PlayController.StopReadFile();

            var succeed = false;
            Loading.Show(() =>
            {
                succeed = PlayController.StopReadFile();
            }, () =>
            {
                if (succeed)
                {
                    Task.Run(() =>
                    {
                        Thread.Sleep(200);
                        Toast.Info($"采集数据个数：{NeuralDataCollector.RecvdLength:N0}");
                    });
                }
                else
                {
                    Toast.Error("停止采样失败");
                }
            });

        });

        public ICommand StartPlay => new DelegateCommand<object>((obj) =>
        {
            var speedX = Convert.ToDouble(obj.ToString());
            if (!NeuralDataCollector.IsCollecting)
            {
                if (string.IsNullOrWhiteSpace(PlayController.SourcePath))
                {
                    Toast.Warn("尚未选择文件");
                    return;
                }

                DeviceId = PlayController.DeviceId;
                ReadonlyCONST.DeviceType = PlayController.DeviceType;
                ReadonlyCONST.ProbeMap = PlayController.ProbeMap;
                ReadonlyCONST.DeviceChannelCount = PlayController.DeviceChannelCount;

                //
                SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

                //
                _physicsChannels = PlayController.GetPhysicsChannels();
                _inputChannels = PlayController.GetInputChannels();

                //
                var deviceParamNWB = new NWBCollectParamInfo(PlayController.DeviceChannelCount)
                {
                    SampleRate = PlayController.SampleRate,
                    DataDuration = PlayController.Duration,
                    CollectType = PlayController.CollectMode,
                };
                deviceParamNWB.ChooseChannels(_inputChannels);
                DeviceParamNWB = deviceParamNWB;

                //DeviceParam = new CollectParamInfo(PlayController.DeviceChannelCount)
                //{
                //    SampleRate = PlayController.SampleRate,
                //    CollectType = PlayController.CollectMode,
                //};

                ////
                //NeuralDataCollector.InitCollectParamInfo(DeviceParam);

                //
                //switch (PlayController.DeviceType)
                //{
                //    case DeviceType.HInSX:
                //        {
                //            ShowTab = true;
                //            var tabItems = new List<string>();
                //            for (int i = 0; i < PlayController.DeviceChannelCount / SingleHeadstageChannels; i++)
                //            {
                //                tabItems.Add($"{(char)('A' + i)}:[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                //            }
                //            TabItems = tabItems;
                //        }
                //        break;
                //    default:
                //        {
                //            ShowTab = false;
                //        }
                //        break;
                //}
                SelectHeadstageIndex = 0;


                //
                Loading.Show(() =>
                {
                    PlayController.StartReadFile(_deviceParamNWB, speedX);
                }, () =>
                {
                    if (DeviceParam.CollectType == Enum.CollectMode.Spike)
                    {
                        Settings.DisplayRaw = false;
                        Settings.DisplayWFWave = false;
                        Settings.DisplayHFWave = false;
                        Settings.DisplayLFWave = false;
                        Settings.DisplaySpike = true;
                    }
                    else
                    {
                        if (!Settings.DisplayRaw && !Settings.DisplayWFWave && !Settings.DisplayHFWave && !Settings.DisplayLFWave)
                        {
                            Settings.DisplayRaw = true;
                            Settings.DisplayWFWave = false;
                            Settings.DisplayHFWave = false;
                            Settings.DisplayLFWave = false;
                            Settings.DisplaySpike = false;
                            Settings.DisplaySpikeSum = false;
                        }
                    }
                    //ChartDataParam = NeuralDataCollector.GetCollectingDataParam();
                });
            }
            else
            {
                PlayController.SpeedUp(speedX);
            }
        });

        public ICommand ChooseNWBFile => new DelegateCommand(() =>
        {
            var dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = $"(*.nwb)|*.nwb",
                RestoreDirectory = true,
            };
            if (dialog.ShowDialog() != true)
            {
                return;
            }
            var dir= dialog.FileName.Split("\\");
            var filename = dir[dir.Length-1].Split("_");
            var timestring = string.Format("" +
                filename[3].Substring(0, 4) + "-"
                + filename[3].Substring(4, 2) +
                "-" + filename[3].Substring(6, 2) + " "
                + filename[4].Substring(0, 2) + ":"
                + filename[4].Substring(2, 2)
                + ":" + filename[4].Substring(4, 2));
            DateTime filetime = Convert.ToDateTime(timestring);

            Settings.FileTimeSpan = GetTimeStamp(filetime);
            Settings.FileDate = filetime.ToString("yyyy-MM-dd");
            Settings.FileTime = filetime.ToString("HH:mm:ss");

            //
            if (!PlayController.SetSourcePath(dialog.FileName))
            {
                Toast.Error("文件加载错误");
                return;
            }
         


            ShowPlaybackPanel = true;
            //

        });
        private int GetTimeStamp(DateTime dt)// 获取时间戳Timestamp  
        {
            DateTime dateStart = new DateTime(1970, 1, 1, 8, 0, 0);
            int timeStamp = Convert.ToInt32((dt - dateStart).TotalSeconds);
            return timeStamp;
        }



        public ICommand MovePlayedTimeLine => new DelegateCommand<object>((seconds) =>
        {
            var s = Convert.ToInt32(seconds);
            PlayController.MovePlayedLine(s);
        });
        #endregion

    }

    public class MultiSelectItem<T>
    {
        public T Item { get; set; }
        public bool IsSelected { get; set; }        
    }
}
