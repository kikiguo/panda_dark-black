﻿using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.DataFile.Converters;
using StairMed.Entity.Infos.Bases;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.HInSX.App.Core;
using StairMed.Probe;
using StairMed.Tools;
using StairMed.UI.ViewModels.UserControls.Waveforms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Serialization;
using StairMed.HInSX.App.Views.Windows;

namespace StairMed.HInSX.App.ViewModels.Regions.Bodies
{
    /// <summary>
    /// 
    /// </summary>
    internal class HistoryViewModel : WaveformVMBase
    {
        /// <summary>
        /// 
        /// </summary>
        public override bool NeedFilter => false;

        /// <summary>
        /// 
        /// </summary>
        public NWBPlayController PlayController { get; set; } = NWBPlayController.Instance;

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private NWBCollectParamInfo _chartDataParam = new NWBCollectParamInfo(1);
        public override CollectParamInfo ChartDataParam
        {
            get { return _chartDataParam; }
            set { Set(ref _chartDataParam, (NWBCollectParamInfo)value); }
        }

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private NWBCollectParamInfo _deviceParam = new NWBCollectParamInfo(1);
        public override CollectParamInfo DeviceParam
        {
            get { return _deviceParam; }
            set { Set(ref _deviceParam, (NWBCollectParamInfo)value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _showTab = false;
        public bool ShowTab
        {
            get { return _showTab; }
            set { Set(ref _showTab, value); }
        }

        /// <summary>
        /// 单个headstage上多少个通道
        /// </summary>
        private int _singleHeadstageChannels = 128;
        public int SingleHeadstageChannels
        {
            get { return _singleHeadstageChannels; }
            set { Set(ref _singleHeadstageChannels, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<string> _tabItems = new List<string>();
        public List<string> TabItems
        {
            get { return _tabItems; }
            set { Set(ref _tabItems, value); }
        }

        /// <summary>
        /// 选择第几个headstage显示
        /// </summary>
        private int _selectHeadstageIndex = 0;
        public int SelectHeadstageIndex
        {
            get { return _selectHeadstageIndex; }
            set
            {
                Set(ref _selectHeadstageIndex, value);

                //
                var begin = value * SingleHeadstageChannels;
                InitWaveformUI(-1, Enumerable.Range(begin, SingleHeadstageChannels).ToList());
            }
        }

        private List<int> _inputChannels = new List<int>(); //通道们
        private List<int> _physicsChannels = new List<int>(); //通道们

        /// <summary>
        /// 
        /// </summary>
        public HistoryViewModel()
        {
            DeviceParam = new NWBCollectParamInfo(0);
            ChartDataParam = new NWBCollectParamInfo(128);
            DeviceId = PlayController.DeviceId;

            //
            EventHelper.Subscribe<FileReadCompletedEvent>(() =>
            {
                ((DelegateCommand)StopCollect).Execute();
            }, Prism.Events.ThreadOption.UIThread);
        }

        /// <summary>
        /// 开始采集
        /// </summary>
        public ICommand StartPlay => new DelegateCommand<object>((obj) =>
        {
            var speedX = Convert.ToDouble(obj.ToString());
            ReadonlyCONST.DeviceType = PlayController.DeviceType;
            ReadonlyCONST.ProbeMap = PlayController.ProbeMap;
            ReadonlyCONST.DeviceChannelCount = PlayController.DeviceChannelCount;
            if (!NeuralDataCollector.IsCollecting)
            {
                if (string.IsNullOrWhiteSpace(PlayController.SourcePath))
                {
                    Toast.Warn("尚未选择文件");
                    return;
                }

                //
                Loading.Show(() =>
                {
                    PlayController.StartReadFile(_deviceParam, speedX);
                }, () =>
                {
                    if (DeviceParam.CollectType == CollectMode.Spike)
                    {
                        Settings.DisplayRaw = false;
                        Settings.DisplayWFWave = false;
                        Settings.DisplayHFWave = false;
                        Settings.DisplayLFWave = false;
                        Settings.DisplaySpike = true;
                    }
                    else
                    {
                        if (!Settings.DisplayRaw && !Settings.DisplayWFWave && !Settings.DisplayHFWave && !Settings.DisplayLFWave)
                        {
                            Settings.DisplayRaw = true;
                            Settings.DisplayWFWave = false;
                            Settings.DisplayHFWave = false;
                            Settings.DisplayLFWave = false;
                            Settings.DisplaySpike = false;
                            Settings.DisplaySpikeSum = false;
                        }
                    }
                    ChartDataParam = NeuralDataCollector.GetCollectingDataParam();
                });
            }
            else
            {
                PlayController.SpeedUp(speedX);
            }
        });

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override bool StartCollectImpl()
        {
            //Do nothing
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override bool StopCollectImpl()
        {
            return PlayController.StopReadFile();
        }

        /// <summary>
        /// 选择文件
        /// </summary>
        public ICommand ChooseNWBFile => new DelegateCommand(() =>
        {
            var dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = $"(*.nwb)|*.nwb",
                RestoreDirectory = true,
            };
            if (dialog.ShowDialog() != true)
            {
                return;
            }

            //
            if (!PlayController.SetSourcePath(dialog.FileName))
            {
                Toast.Error("文件加载错误");
                return;
            }

            //
            ReadonlyCONST.DeviceType = PlayController.DeviceType;
            ReadonlyCONST.ProbeMap = PlayController.ProbeMap;
            ReadonlyCONST.DeviceChannelCount = PlayController.DeviceChannelCount;

            //
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

            //
            _physicsChannels = PlayController.GetPhysicsChannels();
            _inputChannels = PlayController.GetInputChannels();

            //
            var deviceParam = new NWBCollectParamInfo(PlayController.DeviceChannelCount)
            {
                SampleRate = PlayController.SampleRate,
                DataDuration = PlayController.Duration,
                CollectType = PlayController.CollectMode,
            };
            deviceParam.ChooseChannels(_inputChannels);
            DeviceParam = deviceParam;

            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            //
            switch (PlayController.DeviceType)
            {
                case DeviceType.HInSX:
                    {
                        ShowTab = true;
                        var tabItems = new List<string>();
                        for (int i = 0; i < PlayController.DeviceChannelCount / SingleHeadstageChannels; i++)
                        {
                            tabItems.Add($"{(char)('A' + i)}:[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                        }
                        TabItems = tabItems;
                    }
                    break;
                default:
                    {
                        ShowTab = false;
                    }
                    break;
            }
            SelectHeadstageIndex = 0;
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand MovePlayedTimeLine => new DelegateCommand<object>((seconds) =>
        {
            var s = Convert.ToInt32(seconds);
            PlayController.MovePlayedLine(s);
        });

        /// <summary>
        /// 导出
        /// </summary>
        public ICommand ExportChannelData => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            //
            var dialog = new FolderBrowserDialog
            {
                Description = "请选择保存文件路径",
                SelectedPath = CONST.AppInstallPath,
            };

            //
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var dstFolder = dialog.SelectedPath;
                var succeed = false;
                Loading.Show(() =>
                {
                    succeed = new NWB2FilePerChannelBinaryConverter().Convert(PlayController.SourcePath, dstFolder);
                }, () =>
                {
                    if (succeed)
                    {
                        Toast.Info($"已成功转换");
                        try
                        {
                            Process.Start("explorer.exe", dstFolder);
                        }
                        catch { }
                    }
                    else
                    {
                        Toast.Warn($"转换失败");
                    }
                });
            }
        });

        /// <summary>
        /// 导出
        /// </summary>
        public ICommand ExportFirst128ChannelData => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            //
            var dstFile = $"{PlayController.SourcePath}_first128.nwb";
            var succeed = false;
            Loading.Show(() =>
            {
                succeed = new NWB2First128NWBConverter().Convert(PlayController.SourcePath, dstFile);
            }, () =>
            {
                if (succeed)
                {
                    Toast.Info($"已成功转换");
                    try
                    {
                        Process.Start("explorer.exe", Path.GetDirectoryName(dstFile));
                    }
                    catch { }
                }
                else
                {
                    Toast.Warn($"转换失败");
                }
            });

        });


        /// <summary>
        /// 导入映射关系
        /// </summary>
        public ICommand ImportProbeMap => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            UIHelper.BeginInvoke(() =>
            {
                var dialog = new Microsoft.Win32.OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //
                try
                {
                    //
                    var xml = FileHelper.LoadFile(dialog.FileName, Encoding.UTF8);
                    if (string.IsNullOrWhiteSpace(xml))
                    {
                        Toast.Warn($"文件为空，无法导入");
                        return;
                    }

                    //
                    using (var reader = new StringReader(xml))
                    {
                        NTProbeMap newProbeMap = null;
                        try
                        {
                            newProbeMap = (NTProbeMap)new XmlSerializer(typeof(NTProbeMap)).Deserialize(reader);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogTool.Logger.Exp($"{TAG}, Deserialize", ex);
                        }

                        //
                        if (newProbeMap == null)
                        {
                            Toast.Warn($"文件有误，无法导入");
                            return;
                        }

                        //
                        if (newProbeMap.IsInValid())
                        {
                            Toast.Warn($"文件无效，导入失败");
                            return;
                        }

                        //
                        ReadonlyCONST.ProbeMap = newProbeMap;

                        //
                        NWBPlayController.Instance.ImportedProbeMap = true;

                        //
                        Toast.Info($"导入成功");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogTool.Logger.Exp($"{TAG}, load", ex);
                    Toast.Error($"文件有误，无法导入");
                    return;
                }
            });
        });

        public ICommand SpikeScopeCommand => new DelegateCommand(() => {
            SpikeScopeMainWnd.PopUp();
        });
    }
}
