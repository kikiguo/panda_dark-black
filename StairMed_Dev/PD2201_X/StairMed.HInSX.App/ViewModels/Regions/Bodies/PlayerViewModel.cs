﻿using Prism.Commands;
using StairMed.Controls.Windows;
using StairMed.Core.Consts;
using StairMed.DataFile.Converters;
using StairMed.Entity.Infos.Bases;
using StairMed.Enum;
using StairMed.Event;
using StairMed.MVVM.Helper;
using StairMed.HInSX.App.Core;
using StairMed.Probe;
using StairMed.Tools;
using StairMed.UI.ViewModels.UserControls.Waveforms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Serialization;
using StairMed.HInSX.App.Views.Windows;
using System.Collections.ObjectModel;
using StairMed.HInSX.App.ViewModels.Windows;
using StairMed.Entity.Infos;
using Prism;
using StairMed.Core.Settings;

namespace StairMed.HInSX.App.ViewModels.Regions.Bodies
{
    /// <summary>
    /// 
    /// </summary>
    internal class PlayerViewModel : WaveformVMBase, IActiveAware
    {
        const int GroupChannelCount = 1024;

        #region 绑定属性


        public HInSXSettings Setting { get; set; } = HInSXSettings.Instance;



        /// <summary>
        /// headstage，每个具有128通道：与电极对应
        /// </summary>
        private ObservableCollection<HeadstageExtend> _headstages = new ObservableCollection<HeadstageExtend>();
        public ObservableCollection<HeadstageExtend> Headstages
        {
            get { return _headstages; }
            set
            {
                Set(ref _headstages, value);
            }
        }
        private bool headstageIndexChangedInSubWin = false;
        /// <summary>
        /// 选择第几个headstage显示
        /// </summary>
        private int _selectHeadstageIndex = 0;
        public int SelectHeadstageIndex
        {
            get { return _selectHeadstageIndex; }
            set
            {
                //切换Headstages时，会导致_selectHeadstageIndex = -1;
                //在InitWaveformUI中对此进行了判断
                //不可在此处理-1问题，否则在切换SelectHeadstageGroup时，会导致此tablecontrol无内容被选中
                if (_selectHeadstageIndex != value)
                {
                    Set(ref _selectHeadstageIndex, value);
                    InitWaveformUI();
                    Settings.SpikeScopeChannel = SingleHeadstageChannels * _selectHeadstageIndex;
                    Settings.WaveformChannel = SingleHeadstageChannels * _selectHeadstageIndex;
                    if (!headstageIndexChangedInSubWin)
                        EventHelper.Publish<ChannelChangedEvent, int>(_selectHeadstageIndex);
                }
                headstageIndexChangedInSubWin = false;
            }
        }

        private ObservableCollection<string> _headstageGroupsSerial = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroupsSerial
        {
            get { return _headstageGroupsSerial; }
            set { Set(ref _headstageGroupsSerial, value); }
        }


        /// <summary>
        /// 
        /// </summary>
        private ObservableCollection<string> _headstageGroups = new ObservableCollection<string>();
        public ObservableCollection<string> HeadstageGroups
        {
            get { return _headstageGroups; }
            set { Set(ref _headstageGroups, value); }
        }

        /// <summary>
        /// 每1024通道对应一个board
        /// </summary>
        private int _selectHeadstageGroup = 0;
        public int SelectHeadstageGroup
        {
            get { return _selectHeadstageGroup; }
            set
            {
                value = Math.Min(Math.Max(value, 0), HInSXCONST.CHANNELS_COUNT / GroupChannelCount - 1);
                if (_selectHeadstageGroup != value)
                {
                    Set(ref _selectHeadstageGroup, value);

                    //更新table control中各个headstage的标题名称
                    UpdateTableControlHeadstages();

                    //
                    if (SelectHeadstageIndex == 0)
                    {
                        InitWaveformUI();
                    }
                    else
                    {
                        SelectHeadstageIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public HInSXManager HInSXManager { get; set; } = HInSXManager.Instance;

        #endregion


        /// <summary>
        /// 
        /// </summary>
        public PlayerViewModel()
        {
            DeviceParam = new NWBCollectParamInfo(0);
            ChartDataParam = new NWBCollectParamInfo(128);
            DeviceId = PlayController.DeviceId;

            //
            EventHelper.Subscribe<FileReadCompletedEvent>(() =>
            {
                ((DelegateCommand)StopCollect).Execute();
            }, Prism.Events.ThreadOption.UIThread);



            //
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            //
            UpdateTableControlHeadstageGroups();
            UpdateTableControlHeadstages();

            //
            SelectHeadstageGroup = 0;
            SelectHeadstageIndex = 0;

            //
            InitWaveformUI();



            HInSXManager.Instance.OnHeadStageInsertedStateChanged += (int headstage, bool inserted) => {
                if (Headstages.Count > headstage)
                {
                    var newHeadstages = new ObservableCollection<HeadstageExtend>();
                    for (int i = 0; i < Headstages.Count; i++)
                    {
                        var item = Headstages[i];
                        if (i == headstage)
                            item.IsInserted = inserted;
                        newHeadstages.Add(item);
                    }
                    this.Headstages = newHeadstages;
                }
            };
            EventHelper.Subscribe<ChannelChangedEventFromSub, int>((cmdParams) =>
            {
                headstageIndexChangedInSubWin = true;
                this.SelectHeadstageIndex = cmdParams;
            }, Prism.Events.ThreadOption.UIThread);
            //System.Threading.Tasks.Task.Factory.StartNew(new Action(()=>{
            //    while (true)
            //    {
            //        var newHeadstages = new ObservableCollection<HeadstageExtend>();
            //        for (int i = 0; i < Headstages.Count; i++)
            //        {
            //            var item = Headstages[i];
            //            item.IsInserted = !item.IsInserted;
            //            newHeadstages.Add(item);
            //        }
            //        this.Headstages = newHeadstages;
            //        System.Threading.Thread.Sleep(5000);
            //    }
            //}));
        }

        /// <summary>
        /// 刷新波形列表显示的波形
        /// </summary>
        private void InitWaveformUI()
        {
            if (SelectHeadstageGroup >= 0 && SelectHeadstageGroup < HInSXCONST.CHANNELS_COUNT / GroupChannelCount && SelectHeadstageIndex >= 0 && SelectHeadstageIndex < GroupChannelCount / SingleHeadstageChannels)
            {
                var begin = SelectHeadstageIndex * SingleHeadstageChannels + SelectHeadstageGroup * GroupChannelCount;
                InitWaveformUI(DeviceId, Enumerable.Range(begin, SingleHeadstageChannels).ToList());
            }
        }

        /// <summary>
        /// 更新table control中各个headstage group的标题名称
        /// </summary>
        private void UpdateTableControlHeadstageGroups()
        {
            //大于1024通道时，设置group分组
            if (HInSXCONST.CHANNELS_COUNT > GroupChannelCount)
            {
                var headstageGroups = new ObservableCollection<string>();
                for (int i = 0; i < HInSXCONST.CHANNELS_COUNT / GroupChannelCount; i++)
                {
                    headstageGroups.Add($"{(char)('A' + i)}");
                }
                HeadstageGroups = headstageGroups;
            }
        }

        /// <summary>
        /// 更新table control中各个headstage的标题名称
        /// </summary>
        private void UpdateTableControlHeadstages()
        {
            //大于1024通道时，切换group则切换headstage tab的标题
            if (HInSXCONST.HInSXType != Command.HInSXType.HInS_1024)
            {
                var groupName = $"{(char)('A' + SelectHeadstageGroup)}";

                //
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{groupName}{i + 1}");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{groupName}{i + 1}"
                        });
                    }
                }

                //此动作会触发导致selectindex = -1
                for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                {
                    //Headstages[i] = $"{groupName}{i + 1}";
                    Headstages.Add(new HeadstageExtend { HeadContent = $"{groupName}{i + 1}" });
                }
            }
            else
            {
                //1024通道时，不需要变动headstage类型
                if (Headstages.Count == 0)
                {
                    for (int i = 0; i < GroupChannelCount / SingleHeadstageChannels; i++)
                    {
                        //Headstages.Add($"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                        Headstages.Add(new HeadstageExtend
                        {
                            HeadContent = $"{(char)('A' + i)}[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]",
                        });
                        HeadstageGroupsSerial.Add($"{(i + 1).ToString()}");
                    }
                }
            }
        }


        /// <summary>
        /// 寄存器操作
        /// </summary>
        public ICommand ShowRegister => new DelegateCommand<object>((obj) =>
        {

            UIHelper.BeginInvoke(() =>
            {
                HInSRegisterWnd.PopUp();
            });
        });

        public ICommand SpikeScopeCommand => new DelegateCommand(() => {
            SpikeScopeMainWnd.PopUp();
        });

        public ICommand ParameterPanelCommand => new DelegateCommand<object>((obj) => {
            ParameterPanelShowed = !ParameterPanelShowed;
        });

        private bool _parameterPanelShowed;

        public bool ParameterPanelShowed
        {
            get { return _parameterPanelShowed; }
            set { Set(ref _parameterPanelShowed, value); }
        }

        private string _waveformSingleDisplayMode;

        public string WaveformSingleDisplayMode
        {
            get { return _waveformSingleDisplayMode; }
            set
            {
                Set(ref _waveformSingleDisplayMode, value);

                WaveformSingleViewModel.DisplayRaw = value == "RAW";
                WaveformSingleViewModel.DisplaySpike = value == "SPK";
                WaveformSingleViewModel.DisplayLFWave = value == "LFP";
                WaveformSingleViewModel.DisplayHFWave = value == "HFP";
                WaveformSingleViewModel.DisplaySpikeSum = value == "FR";
                StairMedSettings.Instance.DisplayTRIG = value == "TRIG";
            }
        }

        //private WaveformSingleViewModel _waveformSingleViewModel2 = new WaveformSingleViewModel();

        //public WaveformSingleViewModel WaveformSingleViewModel2
        //{
        //    get { return _waveformSingleViewModel2; }
        //    set { Set(ref _waveformSingleViewModel2, value); }
        //}

        //private ObservableCollection<MultiCbxBaseData> _listDisplayMode;

        //public ObservableCollection<MultiCbxBaseData> ListDisplayMode
        //{
        //    get { return _listDisplayMode; }
        //    set
        //    {
        //        Set(ref _listDisplayMode, value);

        //    }
        //}

        public ICommand PanleChangedCommand => new DelegateCommand<object>((obj) =>
        {
            try
            {
                int layout = 0;
                if (Int32.TryParse(obj?.ToString(), out layout))
                {
                    EventHelper.Publish<LayoutChangedEvent, int>(layout);
                }

            }
            catch (Exception)
            {

            }

        });

        private string _testPort;

        public string TestPort
        {
            get { return _testPort; }
            set { Set(ref _testPort, value); }
        }

        private string _testChannel;

        public string TestChannel
        {
            get { return _testChannel; }
            set { Set(ref _testChannel, value); }
        }
        public ICommand SendTestModeCommand => new DelegateCommand(() => {
            try
            {
                int port = 0;
                try
                {
                    port = Convert.ToInt32(TestPort);
                }
                catch (Exception)
                {
                    port = 0;
                }
                int channel = 0;
                try
                {
                    channel = Convert.ToInt32(TestChannel);
                }
                catch (Exception)
                {
                    channel = 0;
                }
                HInSXManager.Instance.SendArmTest(port, channel);
            }
            catch (Exception)
            {

            }

        });






        /// <summary>
        /// 
        /// </summary>
        public override bool NeedFilter => false;

        /// <summary>
        /// 
        /// </summary>
        public NWBPlayController PlayController { get; set; } = NWBPlayController.Instance;

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private NWBCollectParamInfo _chartDataParam = new NWBCollectParamInfo(1);
        public override CollectParamInfo ChartDataParam
        {
            get { return _chartDataParam; }
            set { Set(ref _chartDataParam, (NWBCollectParamInfo)value); }
        }

        /// <summary>
        /// 波形数据对应的采集参数
        /// </summary>
        private NWBCollectParamInfo _deviceParam = new NWBCollectParamInfo(1);
        public override CollectParamInfo DeviceParam
        {
            get { return _deviceParam; }
            set { Set(ref _deviceParam, (NWBCollectParamInfo)value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private bool _showTab = false;
        public bool ShowTab
        {
            get { return _showTab; }
            set { Set(ref _showTab, value); }
        }

        /// <summary>
        /// 单个headstage上多少个通道
        /// </summary>
        private int _singleHeadstageChannels = 128;
        public int SingleHeadstageChannels
        {
            get { return _singleHeadstageChannels; }
            set { Set(ref _singleHeadstageChannels, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<string> _tabItems = new List<string>();
        public List<string> TabItems
        {
            get { return _tabItems; }
            set { Set(ref _tabItems, value); }
        }


        private List<int> _inputChannels = new List<int>(); //通道们
        private List<int> _physicsChannels = new List<int>(); //通道们

        public event EventHandler IsActiveChanged;



        /// <summary>
        /// 开始采集
        /// </summary>
        public ICommand StartPlay => new DelegateCommand<object>((obj) =>
        {
            var speedX = Convert.ToDouble(obj.ToString());
            ReadonlyCONST.DeviceType = PlayController.DeviceType;
            ReadonlyCONST.ProbeMap = PlayController.ProbeMap;
            ReadonlyCONST.DeviceChannelCount = PlayController.DeviceChannelCount;
            if (!NeuralDataCollector.IsCollecting)
            {
                if (string.IsNullOrWhiteSpace(PlayController.SourcePath))
                {
                    Toast.Warn("尚未选择文件");
                    return;
                }

                //
                Loading.Show(() =>
                {
                    PlayController.StartReadFile(_deviceParam, speedX);
                }, () =>
                {
                    if (DeviceParam.CollectType == CollectMode.Spike)
                    {
                        Settings.DisplayRaw = false;
                        Settings.DisplayWFWave = false;
                        Settings.DisplayHFWave = false;
                        Settings.DisplayLFWave = false;
                        Settings.DisplaySpike = true;
                    }
                    else
                    {
                        if (!Settings.DisplayRaw && !Settings.DisplayWFWave && !Settings.DisplayHFWave && !Settings.DisplayLFWave)
                        {
                            Settings.DisplayRaw = true;
                            Settings.DisplayWFWave = false;
                            Settings.DisplayHFWave = false;
                            Settings.DisplayLFWave = false;
                            Settings.DisplaySpike = false;
                            Settings.DisplaySpikeSum = false;
                        }
                    }
                    ChartDataParam = NeuralDataCollector.GetCollectingDataParam();
                });
            }
            else
            {
                PlayController.SpeedUp(speedX);
            }
        });

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override bool StartCollectImpl()
        {
            //Do nothing
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override bool StopCollectImpl()
        {
            return PlayController.StopReadFile();
        }

        /// <summary>
        /// 选择文件
        /// </summary>
        public ICommand ChooseNWBFile => new DelegateCommand(() =>
        {
            var dialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = $"(*.nwb)|*.nwb",
                RestoreDirectory = true,
            };
            if (dialog.ShowDialog() != true)
            {
                return;
            }

            //
            if (!PlayController.SetSourcePath(dialog.FileName))
            {
                Toast.Error("文件加载错误");
                return;
            }

            //
            ReadonlyCONST.DeviceType = PlayController.DeviceType;
            ReadonlyCONST.ProbeMap = PlayController.ProbeMap;
            ReadonlyCONST.DeviceChannelCount = PlayController.DeviceChannelCount;

            //
            SingleHeadstageChannels = ReadonlyCONST.ProbeMap.ChannelCount;

            //
            _physicsChannels = PlayController.GetPhysicsChannels();
            _inputChannels = PlayController.GetInputChannels();

            //
            var deviceParam = new NWBCollectParamInfo(PlayController.DeviceChannelCount)
            {
                SampleRate = PlayController.SampleRate,
                DataDuration = PlayController.Duration,
                CollectType = PlayController.CollectMode,
            };
            deviceParam.ChooseChannels(_inputChannels);
            DeviceParam = deviceParam;

            //
            NeuralDataCollector.InitCollectParamInfo(DeviceParam);

            //
            switch (PlayController.DeviceType)
            {
                case DeviceType.HInSX:
                    {
                        ShowTab = true;
                        var tabItems = new List<string>();
                        for (int i = 0; i < PlayController.DeviceChannelCount / SingleHeadstageChannels; i++)
                        {
                            tabItems.Add($"{(char)('A' + i)}:[{i * SingleHeadstageChannels + 1}-{(i + 1) * SingleHeadstageChannels}]");
                        }
                        TabItems = tabItems;
                    }
                    break;
                default:
                    {
                        ShowTab = false;
                    }
                    break;
            }
            SelectHeadstageIndex = 0;
        });

        /// <summary>
        /// 
        /// </summary>
        public ICommand MovePlayedTimeLine => new DelegateCommand<object>((seconds) =>
        {
            var s = Convert.ToInt32(seconds);
            PlayController.MovePlayedLine(s);
        });

        /// <summary>
        /// 导出
        /// </summary>
        public ICommand ExportChannelData => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            //
            var dialog = new FolderBrowserDialog
            {
                Description = "请选择保存文件路径",
                SelectedPath = CONST.AppInstallPath,
            };

            //
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var dstFolder = dialog.SelectedPath;
                var succeed = false;
                Loading.Show(() =>
                {
                    succeed = new NWB2FilePerChannelBinaryConverter().Convert(PlayController.SourcePath, dstFolder);
                }, () =>
                {
                    if (succeed)
                    {
                        Toast.Info($"已成功转换");
                        try
                        {
                            Process.Start("explorer.exe", dstFolder);
                        }
                        catch { }
                    }
                    else
                    {
                        Toast.Warn($"转换失败");
                    }
                });
            }
        });

        /// <summary>
        /// 导出
        /// </summary>
        public ICommand ExportFirst128ChannelData => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            //
            var dstFile = $"{PlayController.SourcePath}_first128.nwb";
            var succeed = false;
            Loading.Show(() =>
            {
                succeed = new NWB2First128NWBConverter().Convert(PlayController.SourcePath, dstFile);
            }, () =>
            {
                if (succeed)
                {
                    Toast.Info($"已成功转换");
                    try
                    {
                        Process.Start("explorer.exe", Path.GetDirectoryName(dstFile));
                    }
                    catch { }
                }
                else
                {
                    Toast.Warn($"转换失败");
                }
            });

        });


        /// <summary>
        /// 导入映射关系
        /// </summary>
        public ICommand ImportProbeMap => new DelegateCommand<object>((seconds) =>
        {
            if (NeuralDataCollector.IsCollecting)
            {
                return;
            }

            UIHelper.BeginInvoke(() =>
            {
                var dialog = new Microsoft.Win32.OpenFileDialog
                {
                    Filter = $"(*.xml)|*.xml",
                    RestoreDirectory = true,
                };
                if (dialog.ShowDialog() != true)
                {
                    return;
                }

                //
                try
                {
                    //
                    var xml = FileHelper.LoadFile(dialog.FileName, Encoding.UTF8);
                    if (string.IsNullOrWhiteSpace(xml))
                    {
                        Toast.Warn($"文件为空，无法导入");
                        return;
                    }

                    //
                    using (var reader = new StringReader(xml))
                    {
                        NTProbeMap newProbeMap = null;
                        try
                        {
                            newProbeMap = (NTProbeMap)new XmlSerializer(typeof(NTProbeMap)).Deserialize(reader);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogTool.Logger.Exp($"{TAG}, Deserialize", ex);
                        }

                        //
                        if (newProbeMap == null)
                        {
                            Toast.Warn($"文件有误，无法导入");
                            return;
                        }

                        //
                        if (newProbeMap.IsInValid())
                        {
                            Toast.Warn($"文件无效，导入失败");
                            return;
                        }

                        //
                        ReadonlyCONST.ProbeMap = newProbeMap;

                        //
                        NWBPlayController.Instance.ImportedProbeMap = true;

                        //
                        Toast.Info($"导入成功");
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogTool.Logger.Exp($"{TAG}, load", ex);
                    Toast.Error($"文件有误，无法导入");
                    return;
                }
            });
        });

        private bool isActive;
        public bool IsActive 
        {
            get
            {
                return isActive;
            }
            set
            {
                this.isActive = value;
                if (value)
                {
                    Settings.SpikeScopeChannel = SingleHeadstageChannels * _selectHeadstageIndex;
                    Settings.WaveformChannel = SingleHeadstageChannels * _selectHeadstageIndex;
                }
            }
        }
    }
}
