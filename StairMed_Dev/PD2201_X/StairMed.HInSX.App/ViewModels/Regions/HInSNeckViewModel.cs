﻿using StairMed.Core.Settings;
using StairMed.MVVM.Base;

namespace StairMed.HInSX.App.ViewModels.Regions
{
    /// <summary>
    /// 
    /// </summary>
    public class HInSNeckViewModel : RegionViewModelBase
    {
        /// <summary>
        /// 
        /// </summary>
        public StairMedSettings Settings { get; set; } = StairMedSettings.Instance;

        /// <summary>
        /// 
        /// </summary>
        public StairMedSolidSettings SolidSettings { get; set; } = StairMedSolidSettings.Instance;
    }
}
