﻿using StairMed.Array;
using StairMed.HInSX.Comm.Logger;
using StairMed.HInSX.Command;
using StairMed.Net.Nanomsg.Protocols;
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace StairMed.HInSX.Comm.TCP
{
    /// <summary>
    /// 
    /// </summary>
    public class NanoTcp5555 : NanoTcpBase
    {
        const string TAG = nameof(NanoTcp5555);

        const string URL = "tcp://192.168.52.1:5555";

        private static FmcComm fmcComm;

        public NanoTcp5555()
        {
#if true
            if (fmcComm == null)
            {
#if true
                fmcComm = FmcComm.CreateInstance();
#else
                fmcComm = new FmcComm();//FmcComm.CreateInstance();
#endif
            }
            fmcComm.add_XferCallback(CallbackHandler);
#else
            InitSocket(URL);
#endif
            }

        public override void ShutDownComponent()
        {
            base.ShutDownComponent();
            if (fmcComm != null)
            {
#if true
                //fmcComm.remove_XferCallback(CallbackHandler);
#else
                //Thread.Sleep(1000);
                fmcComm.Dispose();
#endif
            }
        }

        private void CallbackHandler(byte[] data,int len)
        {
            var output = new PooledArray<byte>(len);
            try
            {
                Buffer.BlockCopy(data, 0, output.Array, 0, len);
                DataFlowing = true;
                _helper?.HandleRevicedBuffer(output);
            }
            finally
            {

            }
        }
        public void SetHelper2(IHInSXCmdHelper helper)
        {
            _helper = helper;
            fmcComm.SetAsyncReceive(true);
        }
        public override PooledArray<byte> SendRecieve(byte[] bytes)
        {
#if true
            try
            {
                DataFlowing = true;
                byte[] buffer = new byte[1];
#if false
                buffer = fmcComm.SendReceive(bytes, bytes.Length);
#else
                unsafe
                {
                    fixed (byte* pDstArray = bytes)
                    {
                        buffer = fmcComm.SendReceive(pDstArray, bytes.Length);
                    }
                }
#endif
                int outlen = buffer.Length;
                var output = new PooledArray<byte>(outlen);
                try
                {
                    Buffer.BlockCopy(buffer, 0, output.Array, 0, outlen);
                }
                finally
                {

                }
                return output;
            }
            finally
            {
                DataFlowing = false;
            }
            return null;
#else
            return SendRecieve(bytes, URL);
#endif
            }

        public override bool Send(byte[] bytes)
        {
#if true
            try
            {
                DataFlowing = true;
                unsafe
                {
                    fixed (byte* pDstArray = bytes)
                    {
                        return fmcComm.Send(pDstArray, bytes.Length);
                    }
                }
            }
            finally
            {
                DataFlowing = false;
            }
            return false;
#else
            return Send(bytes, URL);
#endif
        }
        /// <summary>
        /// 封装一发一收模式
        /// </summary>
        /// <param name="func"></param>
        /// <param name="sendTimeoutMS"></param>
        /// <param name="recvdTimeoutMS"></param>
        /// <returns></returns>
        //public override PooledArray<byte> SendRecieve(byte[] bytes, int sendTimeoutMS = 1500, int recvdTimeoutMS = 1500)
        //{
        //    return SendRecieve(bytes, sendTimeoutMS, recvdTimeoutMS);
        //}

        //public override void ShutDown()
        //{
        //    ShutDown(_pairSocket);
        //}

        public override void CloseNanomsg()
        {
            base.CloseNanomsg();
            if (fmcComm != null)
            {
                fmcComm.Dispose();
            }
        }
    }
}
