﻿using StairMed.Array;
using StairMed.HInSX.Comm.Logger;
using StairMed.HInSX.Command;
using StairMed.Net.Nanomsg;
using StairMed.Net.Nanomsg.Protocols;
using System;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace StairMed.HInSX.Comm.TCP
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class NanoTcpBase
    {
        /// <summary>
        /// 
        /// </summary>
        const int RCV_MAX_LEN = 1024 * 1024 * 16;

        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(NanoTcpBase);

        /// <summary>
        /// 
        /// </summary>
        string URL = "tcp://192.168.52.1:5555";

        /// <summary>
        /// 通信接口
        /// </summary>
        private PairSocket _pairSocket = null;

        public IHInSXCmdHelper _helper = null;

        /// <summary>
        /// 
        /// </summary>

        /// <summary>
        /// 是否有数据流动
        /// </summary>
        public bool DataFlowing { get; set; } = false;


        public void SetHelper(IHInSXCmdHelper helper)
        {
            _helper = helper;
            Task.Run(() =>
            {
                while (true)
                {
                    var recvd = _pairSocket.Receive();
                    if (recvd != null && (recvd.Length >= 4))
                    {
                        DataFlowing = true;
                        _helper?.HandleRevicedBuffer(recvd);

                    }
                }
            }); 
        }

        public void InitSocket(string strUrl,  int sendTimeoutMS = 5000, int recvdTimeoutMS = 5000)
        {
            URL = strUrl;
            while (_pairSocket == null)
            {
                var socket = new PairSocket();
                if (socket.SocketID < 0)
                {
                    LogTool.Logger.LogW($"{TAG}, socket id < 0");
                    Thread.Sleep(10);
                }
                else
                {
                    _pairSocket = socket;
                }
            }
            _pairSocket.Connect(strUrl);
            NN.SetSockOpt(_pairSocket.SocketID, SocketOption.SNDTIMEO, sendTimeoutMS);
            NN.SetSockOpt(_pairSocket.SocketID, SocketOption.RCVTIMEO, recvdTimeoutMS);
            NN.SetSockOpt(_pairSocket.SocketID, SocketOption.RCVMAXSIZE, RCV_MAX_LEN);
        }

        /// <summary>
        /// 封装一发一收模式
        /// </summary>
        /// <param name="func"></param>
        /// <param name="sendTimeoutMS"></param>
        /// <param name="recvdTimeoutMS"></param>
        /// <returns></returns>
        //public abstract PooledArray<byte> SendRecieve(byte[] bytes);

        public abstract PooledArray<byte> SendRecieve(byte[] bytes);
        public abstract bool Send(byte[] bytes);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="func"></param>
        /// <param name="sendTimeoutMS"></param>
        /// <param name="recvdTimeoutMS"></param>
        /// <param name="socket"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public PooledArray<byte> SendRecieve(byte[] bytes, string url)
        {
            try
            {
                //
                var server = _pairSocket.Connect(url);         
                try
                {
                    //
                    DataFlowing = true;
                    if (!_pairSocket.Send(bytes))
                    {
                        LogTool.Logger.LogE($"{nameof(SendRecieve)} send failed");
                        return null;
                    }

                    //
                    var recvd = _pairSocket.Receive();
                    if (recvd == null)
                    {
                        return null;
                    }
                    return recvd;
                }
                catch (Exception ex)
                {
                    LogTool.Logger.Exp($"{TAG}, {nameof(SendRecieve)}(inner)", ex);
                }
                finally
                {
                    DataFlowing = false;
                    _pairSocket.Shutdown(server);
                }

                //
                return null;
            }
            catch (Exception ex)
            {
                LogTool.Logger.Exp($"{TAG}, {nameof(SendRecieve)}(outer)", ex);
            }

            //
            return null;
        }


        public bool Send(byte[] bytes, string url)
        {
            try
            {
                //
                var server = _pairSocket.Connect(url);
                try
                {
                    //
                    DataFlowing = true;
                    if (!_pairSocket.Send(bytes))
                    {
                        DataFlowing = false;
                        LogTool.Logger.LogE($"{nameof(Send)} send failed");
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    DataFlowing = false;
                    LogTool.Logger.Exp($"{TAG}, {nameof(Send)}(inner)", ex);
                    return false;
                }
                finally
                {
                    DataFlowing = false;
                    _pairSocket.Shutdown(server);
                }
            }
            catch (Exception ex)
            {
                DataFlowing = false;
                LogTool.Logger.Exp($"{TAG}, {nameof(SendRecieve)}(outer)", ex);
                return false;
            }

            //
            return true;
        }

        public void ShutDown()
        {
            if (_pairSocket != null)
                _pairSocket.Dispose();
            ShutDownComponent();
        }

        public virtual void ShutDownComponent()
        { 
        
        }
        public virtual void CloseNanomsg()
        {

        }
    }
}
