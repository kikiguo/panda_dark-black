﻿using StairMed.Array;
using StairMed.HInSX.Comm.Logger;
using StairMed.HInSX.Command;
using StairMed.Net.Nanomsg.Protocols;
using System;
using System.Threading;

namespace StairMed.HInSX.Comm.TCP
{
    /// <summary>
    /// 
    /// </summary>
    public class NanoTcp5556 : NanoTcpBase
    {
        const string TAG = nameof(NanoTcp5556);

        /// <summary>
        /// 
        /// </summary>
        const string URL = "tcp://192.168.52.1:5556";

        private static FmcData fmcData;

        /// <summary>
        /// 
        /// </summary>
        public NanoTcp5556()
        {
#if true
            if (fmcData == null)
            {
                fmcData = FmcData.CreateInstance();//new FmcData();
            }
            //fmcData.remove_XferCallback(AcqCallbackHandler);
            fmcData.add_XferCallback(AcqCallbackHandler);
#else
            InitSocket(URL);
#endif
        }

        public override void ShutDownComponent()
        {
            base.ShutDownComponent();
#if true
            if (fmcData != null)
            {
                //fmcData.remove_XferCallback(AcqCallbackHandler);
                //Thread.Sleep(1000);
                //fmcData.Dispose();
            }
#endif
        }

        private void AcqCallbackHandler(byte[] data, int len)
        {
            var output = new PooledArray<byte>(len);
            try
            {
                Buffer.BlockCopy(data, 0, output.Array, 0, len);
                DataFlowing = true;
                _helper?.HandleRevicedBuffer(output);
            }
            finally
            {

            }
        }
        public void SetHelper2(IHInSXCmdHelper helper)
        {
            _helper = helper;
            fmcData.SetAsyncReceive(true);
        }

        public override PooledArray<byte> SendRecieve(byte[] bytes)
        {
#if true
            try
            {
                DataFlowing = true;
                byte[] buffer = new byte[1];
                unsafe
                {
                    fixed (byte* pDstArray = bytes)
                    {
                        buffer = fmcData.SendReceive(pDstArray, bytes.Length);
                    }
                }
                int outlen = buffer.Length;
                var output = new PooledArray<byte>(outlen);
                try
                {
                    Buffer.BlockCopy(buffer, 0, output.Array, 0, outlen);
                }
                finally
                {

                }
                return output;
            }
            finally
            {
                DataFlowing = false;
            }
            return null;
#else
            return SendRecieve(bytes, URL);
#endif
        }

        public override bool Send(byte[] bytes)
        {
#if true
            try
            {
                DataFlowing = true;
                unsafe
                {
                    fixed (byte* pDstArray = bytes)
                    {
                        return fmcData.Send(pDstArray, bytes.Length);
                    }
                }
            }
            finally
            {
                DataFlowing = false;
            }
            return false;
#else
            return Send(bytes, URL);
#endif
        }

        /// <summary>
        /// 封装一发一收模式
        /// </summary>
        /// <param name="func"></param>
        /// <param name="sendTimeoutMS"></param>
        /// <param name="recvdTimeoutMS"></param>
        /// <returns></returns>
        //public override PooledArray<byte> SendRecieve(byte[] bytes, int sendTimeoutMS = 1500, int recvdTimeoutMS = 1500)
        //{
        //    return SendRecieve(bytes, sendTimeoutMS, recvdTimeoutMS);
        //}

        public override void CloseNanomsg()
        {
            base.CloseNanomsg();
            if (fmcData != null)
            {
                fmcData.Dispose();
            }
        }
    }
}
