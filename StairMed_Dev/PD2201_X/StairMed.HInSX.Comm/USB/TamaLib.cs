﻿using System.Runtime.InteropServices;

namespace StairMed.HInSX.Comm.USB
{
    /// <summary>
    /// 
    /// </summary>
    internal class TamaLib
    {
        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern int meditco_usb_get_device_list();

        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern TamaState meditco_usb_get_info(int device_id, ref MeditcoUsbDevInfo devices);

        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern TamaState meditco_usb_open(int device_id);

        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern TamaState meditco_usb_close(int device_id);

        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern int meditco_usb_get_image(int device_id, int ept, byte[] pdata, int data_size);

        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern TamaState meditco_usb_flush_rx_data(int device_id, int ept);

        [DllImport("tama_tsp_usb.dll", CharSet = CharSet.Auto)]
        internal static extern TamaState meditco_usb_send_cmd(int device_id, ushort cmd, byte[] inbuf, int inlen, byte[] outbuf, int outlen);
    }

    /// <summary>
    /// 
    /// </summary>
    public struct MeditcoUsbDevInfo
    {
        public int device_id;
        public int serial_id;
        public int hard_ver;
        public int firm_ver;
        public int board_temp;
        public int reserved1;
        public int reserved2;
        public int reserved3;
        public int reserved4;
        public int reserved5;
        public int reserved6;
        public int reserved7;
        public int reserved8;
        public int reserved9;
        public int reserved10;
    };

    /// <summary>
    /// 
    /// </summary>
    internal enum TamaState : int
    {
        STATUS_SUCCESSFUL = 0x0000,
        STATUS_INCOMPLETE = 0x0001,
        STATUS_INVALID_PARAM = 0x0002,
        STATUS_INVALID_BOARDINDEX = 0x0004,
        STATUS_INVALID_BARNUM = 0x0008,
        STATUS_INVALID_CHINDEX = 0x0010,
        STATUS_DMA_TIMEOUT = 0x0020,
        STATUS_DMA_CANCELLED = 0x0040,
        STATUS_BSCAN_NOT_READY = 0x0080,
        STATUS_OPCODE_ERROR = 0x0100
    }
}
