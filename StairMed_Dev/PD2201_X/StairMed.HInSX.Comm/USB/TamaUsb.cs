﻿using StairMed.Array;
using System;

namespace StairMed.HInSX.Comm.USB
{
    /// <summary>
    /// 
    /// </summary>
    public class TamaUsb
    {
        /// <summary>
        /// 
        /// </summary>
        const string TAG = nameof(TamaUsb);

        /// <summary>
        /// 
        /// </summary>
        private readonly int _deviceId = 0;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _ept = 8;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _frameWidth = 2048;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _frameHeight = 1024;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _usbStepSize = 16 * 1024;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _headSize = 64;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _frameSize = 0;

        /// <summary>
        /// 
        /// </summary>
        private readonly int _usbBufsize = 0;

        /// <summary>
        /// usb是否工作正常
        /// </summary>
        private bool _isUsbOk = false;
        public bool IsUsbOk
        {
            get { return _isUsbOk; }
            set
            {
                if (_isUsbOk != value)
                {
                    _isUsbOk = value;
                }
            }
        }

        /// <summary>
        /// USB是否已打开
        /// </summary>
        private bool _isUsbOpened = false;
        public bool IsUsbOpened
        {
            get { return _isUsbOpened; }
            set
            {
                if (_isUsbOpened != value)
                {
                    _isUsbOpened = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool DataFlowing { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usbDeviceId"></param>
        /// <param name="ept"></param>
        /// <param name="frameWidth"></param>
        /// <param name="frameHeight"></param>
        /// <param name="stepSize"></param>
        /// <param name="headSize"></param>
        public TamaUsb(int usbDeviceId = 0, int ept = 8, int frameWidth = 2048, int frameHeight = 1024, int stepSize = 16 * 1024, int headSize = 64)
        {
            _deviceId = usbDeviceId;
            _ept = ept;
            _frameWidth = frameWidth;
            _frameHeight = frameHeight;
            _usbStepSize = stepSize;
            _headSize = headSize;

            //
            _frameSize = _frameWidth * _frameHeight;
            if (((_frameSize + _headSize) % _usbStepSize) == 0)
            {
                _usbBufsize = _frameSize + _headSize;
            }
            else
            {
                _usbBufsize = (((_frameSize + _headSize) / _usbStepSize) + 1) * _usbStepSize;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool OpenUsb()
        {
            if (IsUsbOpened)
            {
                return true;
            }

            //
            var uiRet = TamaState.STATUS_OPCODE_ERROR;

            //
            try
            {
                var deviceCount = TamaLib.meditco_usb_get_device_list();
                Logger.LogTool.Logger.LogI($"{TAG}, find usb device count = {deviceCount}");

                //
                if (deviceCount < 1 || deviceCount > 4)
                {
                    uiRet = TamaState.STATUS_BSCAN_NOT_READY;
                }
                else
                {
                    uiRet = TamaLib.meditco_usb_open(_deviceId);
                }
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(OpenUsb)}", ex);
            }

            //
            Logger.LogTool.Logger.LogW($"{TAG}, open usb device status = {uiRet}");

            //
            IsUsbOpened = uiRet == TamaState.STATUS_SUCCESSFUL;
            IsUsbOk = uiRet == TamaState.STATUS_SUCCESSFUL;

            //
            return IsUsbOk;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool CloseUsb()
        {
            try
            {
                var ret = TamaLib.meditco_usb_close(_deviceId);
                Logger.LogTool.Logger.LogW($"{TAG}, close usb device status = {ret}");
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(CloseUsb)}", ex);
            }

            //
            IsUsbOpened = false;
            IsUsbOk = false;

            //
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendBytes"></param>
        /// <returns></returns>
        public PooledArray<byte> SendRecieve(byte[] sendBytes)
        {
            var frame = new PooledArray<byte>(_usbBufsize);

            //
            try
            {
                TamaLib.meditco_usb_get_image(_deviceId, _ept, frame.Array, _usbBufsize);
                DataFlowing = true;
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(SendRecieve)}", ex);
                IsUsbOk = false;
            }

            //
            return frame;

            //
            //if ((TamaState)TamaLib.meditco_usb_get_image(_deviceId, _ept, frame, _usbBufsize) == TamaState.STATUS_SUCCESSFUL)
            //{
            //    DataFlowing = true;
            //    return frame;
            //}

            ////
            //return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool GetUsbInfo(out MeditcoUsbDevInfo info)
        {
            info = new MeditcoUsbDevInfo();
            try
            {
                return TamaLib.meditco_usb_get_info(_deviceId, ref info) == TamaState.STATUS_SUCCESSFUL;
            }
            catch (Exception ex)
            {
                Logger.LogTool.Logger.Exp($"{TAG}, {nameof(GetUsbInfo)}", ex);
            }
            return false;
        }
    }
}
